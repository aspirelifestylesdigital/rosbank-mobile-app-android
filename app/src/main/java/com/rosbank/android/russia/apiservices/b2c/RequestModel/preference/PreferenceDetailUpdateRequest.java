package com.rosbank.android.russia.apiservices.b2c.RequestModel.preference;

import com.google.gson.annotations.Expose;

/**
 * Created by Thu Nguyen on 12/12/2016.
 */

public class PreferenceDetailUpdateRequest extends PreferenceDetailRequest{
    @Expose
    private String Value;
    @Expose
    private String Value1;
    @Expose
    private String Value2;
    @Expose
    private String Value3;
    @Expose
    private String Value4;
    @Expose
    private String Value5;
    public PreferenceDetailUpdateRequest(String Delete, String Type) {
        super(Delete, Type);
    }

    @Override
    public void fillData(Object... values) {
        if(values != null){
            Object[] valueArr = values.clone();

            if(valueArr.length > 0){
                Value = valueArr[0].toString();
            }
            if(valueArr.length > 1){
                Value1 = valueArr[1].toString();
            }
            if(valueArr.length > 2){
                Value2 = valueArr[2].toString();
            }
            if(valueArr.length > 3){
                Value3 = valueArr[3].toString();
            }
            if(valueArr.length > 4){
                Value4 = valueArr[4].toString();
            }
            if(valueArr.length > 5){
                Value5 = valueArr[5].toString();
            }
        }
    }
}
