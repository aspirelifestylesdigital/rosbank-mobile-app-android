package com.rosbank.android.russia.apiservices.ResponseModel;

import com.rosbank.android.russia.apiservices.apis.appimplement.BaseResponse;
import com.rosbank.android.russia.model.concierge.RestaurantBookingDetailData;

/**
 * Created by ThuNguyen on 10/26/2016.
 */

public class RestaurantBookingDetailResponse extends BaseResponse{
    RestaurantBookingDetailData Data;

    public RestaurantBookingDetailData getData() {
        return Data;
    }

    public void setData(RestaurantBookingDetailData data) {
        Data = data;
    }
}
