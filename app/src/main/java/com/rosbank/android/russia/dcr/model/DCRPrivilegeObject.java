package com.rosbank.android.russia.dcr.model;


import com.google.gson.annotations.Expose;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.application.AppContext;
import com.rosbank.android.russia.model.BaseModel;
import com.rosbank.android.russia.utils.CommonUtils;

import java.io.Serializable;
import java.util.List;

/**
 * Created by anh.trinh on 3/10/2017.
 */

public class DCRPrivilegeObject
        extends BaseModel
        implements Serializable {
    public static final String PRIVILEGE_TYPE_RESTAURANT = "privilege_type_restaurant";
    public static final String PRIVILEGE_TYPE_HOTEL = "privilege_type_hotel";
    public static final String PRIVILEGE_TYPE_SPA = "privilege_type_spa";
    @Expose
    String id;
    @Expose
    DCRCommonObject item_type;
    @Expose
    DCRCommonObject status;
    @Expose
    String name;
    @Expose
    String summary;
    @Expose
    String description;
    @Expose
    String direction;
    @Expose
    String latitude;
    @Expose
    String longitude;
    @Expose
    String website_url;
    @Expose
    String phone_number;
    @Expose
    String logo_url;
    @Expose
    String operating_hours_description;
    @Expose
    List<DCROpeningHourObject> opening_hours;
    @Expose
    String address_line_1;
    @Expose
    String address_line_2;
    @Expose
    String address_metropolitan;
    @Expose
    String address_neighbourhood;
    @Expose
    String address_post_code;
    @Expose
    DCRCommonObject address_city;
    @Expose
    DCRCommonObject address_state;
    @Expose
    DCRCommonObject address_country;
    @Expose
    DCRCommonObject internal_label;
    @Expose
    String video_1_url;
    @Expose
    String video_2_url;
    @Expose
    String video_3_url;
    @Expose
    List<DCRCommonObject> meals_served;
    @Expose
    String menu;
    @Expose
    String executive_chef;
    @Expose
    List<DCRCommonObject> dress_codes;
    @Expose
    List<DCRCommonObject> ambiance;
    @Expose
    List<DCRCommonObject> cuisines;
    @Expose
    List<DCRCommonObject> types_of_place;
    @Expose
    List<DCRCommonObject> occasions;
    @Expose
    DCRCommonObject price_range;
    @Expose
    DCRCommonObject price_currency;
    @Expose
    List<DCRCommonObject> accepted_payment_types;
    @Expose
    String terms_and_conditions;

    @Expose
    boolean master_item;
    @Expose
    List<DCRCommonObject> chain;
    @Expose
    String yelp_item_id;
    @Expose
    String last_published_date;
    @Expose
    String format;
    @Expose
    List<DCRRelatedItemObject> related_items;
   /* @Expose
    List<DCRCommonObject> related_items;*/

    @Expose
    List<DCRImageObject> images;
    @Expose
    DCRCommonObject metadata;
    @Expose
    String source_name;
    //Privilege Hotel
    @Expose
    DCRCommonObject international_star_rating;
    @Expose
    List<DCRCommonObject> types;
    @Expose
    List<DCRCommonObject> links;

    @Expose
    DCRCommonObject location_type;
    @Expose
    List<DCRCommonObject> styles;
    @Expose
    List<DCRCommonObject> sub_types;
    @Expose
    List<DCRCommonObject> atmosphere;
    @Expose
    List<DCRCommonObject> room_types;
    @Expose
    List<DCRCommonObject> people;
    @Expose
    List<DCRCommonObject> hotel_amenities;
    @Expose
    List<DCRCommonObject> sports_hobby_amenities;
    @Expose
    String notes_on_price;

    public DCRCommonObject getMetadata() {
        return metadata;
    }

    public void setMetadata(final DCRCommonObject metadata) {
        this.metadata = metadata;
    }

    public List<DCRCommonObject> getStyles() {
        return styles;
    }

    public void setStyles(final List<DCRCommonObject> styles) {
        this.styles = styles;
    }

    public String getSpa_service() {
        return spa_service;
    }

    public void setSpa_service(final String spa_service) {
        this.spa_service = spa_service;
    }

    @Expose
    String spa_service;


    public String getNotes_on_price() {
        return notes_on_price;
    }

    public void setNotes_on_price(final String notes_on_price) {
        this.notes_on_price = notes_on_price;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public DCRCommonObject getItem_type() {
        return item_type;
    }

    public void setItem_type(final DCRCommonObject item_type) {
        this.item_type = item_type;
    }

    public DCRCommonObject getStatus() {
        return status;
    }

    public void setStatus(final DCRCommonObject status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(final String summary) {
        this.summary = summary;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(final String direction) {
        this.direction = direction;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(final String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(final String longitude) {
        this.longitude = longitude;
    }

    public String getWebsite_url() {
        return website_url;
    }

    public void setWebsite_url(final String website_url) {
        this.website_url = website_url;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(final String phone_number) {
        this.phone_number = phone_number;
    }

    public String getLogo_url() {
        String url = "";
        if (CommonUtils.isStringValid(logo_url)) {
            url = logo_url;
        } else {
            if (getImages() != null && getImages().size() > 0) {
                for (DCRImageObject imageObject : getImages()
                        ) {
                    if (imageObject.getImageIsDefault()) {
                        url = imageObject.getUrl();
                    }
                }
                if (!CommonUtils.isStringValid(url)) {
                    url = getImages().get(0)
                            .getUrl();
                }
                if (!CommonUtils.isStringValid(url)) {
                    for (DCRImageObject imageObject : getImages()) {
                        url = imageObject.getLocalPath();
                        if (CommonUtils.isStringValid(url)) {
                            break;
                        }
                    }
                }
            }
        }
        return url;

    }

    public void setLogo_url(final String logo_url) {
        this.logo_url = logo_url;
    }

    public String getOperating_hours_description() {
        return operating_hours_description;
    }

    public void setOperating_hours_description(final String operating_hours_description) {
        this.operating_hours_description = operating_hours_description;
    }

    public List<DCROpeningHourObject> getOpening_hours() {
        return opening_hours;
    }

    public void setOpening_hours(final List<DCROpeningHourObject> opening_hours) {
        this.opening_hours = opening_hours;
    }

    public String getAddress_line_1() {
        return address_line_1;
    }

    public void setAddress_line_1(final String address_line_1) {
        this.address_line_1 = address_line_1;
    }

    public String getAddress_line_2() {
        return address_line_2;
    }

    public void setAddress_line_2(final String address_line_2) {
        this.address_line_2 = address_line_2;
    }

    public String getAddress_metropolitan() {
        return address_metropolitan;
    }

    public void setAddress_metropolitan(final String address_metropolitan) {
        this.address_metropolitan = address_metropolitan;
    }

    public String getAddress_neighbourhood() {
        return address_neighbourhood;
    }

    public void setAddress_neighbourhood(final String address_neighbourhood) {
        this.address_neighbourhood = address_neighbourhood;
    }

    public String getAddress_post_code() {
        return address_post_code;
    }

    public void setAddress_post_code(final String address_post_code) {
        this.address_post_code = address_post_code;
    }

    public DCRCommonObject getAddress_city() {
        return address_city;
    }

    public void setAddress_city(final DCRCommonObject address_city) {
        this.address_city = address_city;
    }

    public DCRCommonObject getAddress_state() {
        return address_state;
    }

    public void setAddress_state(final DCRCommonObject address_state) {
        this.address_state = address_state;
    }

    public DCRCommonObject getAddress_country() {
        return address_country;
    }

    public void setAddress_country(DCRCommonObject address_country) {
        this.address_country = address_country;
    }

    public DCRCommonObject getInternal_label() {
        return internal_label;
    }

    public void setInternal_label(final DCRCommonObject internal_label) {
        this.internal_label = internal_label;
    }

    public String getVideo_1_url() {
        return video_1_url;
    }

    public void setVideo_1_url(final String video_1_url) {
        this.video_1_url = video_1_url;
    }

    public String getVideo_2_url() {
        return video_2_url;
    }

    public void setVideo_2_url(final String video_2_url) {
        this.video_2_url = video_2_url;
    }

    public String getVideo_3_url() {
        return video_3_url;
    }

    public void setVideo_3_url(final String video_3_url) {
        this.video_3_url = video_3_url;
    }

    public List<DCRCommonObject> getMeals_served() {
        return meals_served;
    }

    public void setMeals_served(final List<DCRCommonObject> meals_served) {
        this.meals_served = meals_served;
    }

    public String getMenu() {
        return menu;
    }

    public void setMenu(final String menu) {
        this.menu = menu;
    }

    public String getExecutive_chef() {
        return executive_chef;
    }

    public void setExecutive_chef(final String executive_chef) {
        this.executive_chef = executive_chef;
    }

    public List<DCRCommonObject> getDress_codes() {
        return dress_codes;
    }

    public void setDress_codes(final List<DCRCommonObject> dress_codes) {
        this.dress_codes = dress_codes;
    }

    public List<DCRCommonObject> getAmbiance() {
        return ambiance;
    }

    public void setAmbiance(final List<DCRCommonObject> ambiance) {
        this.ambiance = ambiance;
    }

    public List<DCRCommonObject> getCuisines() {
        return cuisines;
    }

    public String getStringCuisines() {
        String cussines = "";
        if (getCuisines() != null && getCuisines().size() > 0) {
            for (DCRCommonObject commonObject : getCuisines()
                    ) {
                if (CommonUtils.isStringValid(cussines)) {
                    cussines += ", ";
                }
                cussines += commonObject.getName();

            }
        }
        return cussines;
    }

    public String getStringOperatingHours() {
        String operatingHours = "";
        if (getOpening_hours() != null && getOpening_hours().size() > 0) {
            for (DCROpeningHourObject openingHourObject : getOpening_hours()
                    ) {
                if (CommonUtils.isStringValid(operatingHours)) {
                    operatingHours += "\n";
                }
                operatingHours +=
                        openingHourObject.getOpening_from_day() + " - " + openingHourObject.getOpening_to_day() + ": " + openingHourObject.getOpening_from_hour() + " - " + openingHourObject.getOpening_to_hour();

            }
        }
        return operatingHours;
    }

    public String getStringPrivilegeDescription() {
        String privilegeDescription = "";
        if (getRelated_items() != null && getRelated_items().size() > 0) {
            for (DCRRelatedItemObject dcrRelatedItemObject : getRelated_items()
                    ) {
                if (dcrRelatedItemObject.getType() != null && dcrRelatedItemObject.getType().getCode().equals("privilege")) {
                    if (CommonUtils.isStringValid(privilegeDescription)) {
                        privilegeDescription += "\n\n";
                    }

                    if (CommonUtils.isStringValid(dcrRelatedItemObject.getName())) {
                        privilegeDescription += "<b>" + dcrRelatedItemObject.getName() + "</b>";
                    }
                    if (CommonUtils.isStringValid(dcrRelatedItemObject.getDescription())) {
                        privilegeDescription += "<br><br>" + dcrRelatedItemObject.getDescription();
                    }
                }
            }
        }
        return privilegeDescription;
    }

    public void setCuisines(final List<DCRCommonObject> cuisines) {
        this.cuisines = cuisines;
    }

    public List<DCRCommonObject> getTypes_of_place() {
        return types_of_place;
    }

    public void setTypes_of_place(final List<DCRCommonObject> types_of_place) {
        this.types_of_place = types_of_place;
    }

    public List<DCRCommonObject> getOccasions() {
        return occasions;
    }

    public void setOccasions(final List<DCRCommonObject> occasions) {
        this.occasions = occasions;
    }

    public DCRCommonObject getPrice_range() {
        return price_range;
    }

    public void setPrice_range(final DCRCommonObject price_range) {
        this.price_range = price_range;
    }

    public DCRCommonObject getPrice_currency() {
        return price_currency;
    }

    public void setPrice_currency(final DCRCommonObject price_currency) {
        this.price_currency = price_currency;
    }

    public List<DCRCommonObject> getAccepted_payment_types() {
        return accepted_payment_types;
    }

    public void setAccepted_payment_types(final List<DCRCommonObject> accepted_payment_types) {
        this.accepted_payment_types = accepted_payment_types;
    }

    public String getTerms_and_conditions() {
        return terms_and_conditions;
    }

    public void setTerms_and_conditions(final String terms_and_conditions) {
        this.terms_and_conditions = terms_and_conditions;
    }

    public boolean isMaster_item() {
        return master_item;
    }

    public void setMaster_item(final boolean master_item) {
        this.master_item = master_item;
    }

    public List<DCRCommonObject> getChain() {
        return chain;
    }

    public void setChain(final List<DCRCommonObject> chain) {
        this.chain = chain;
    }

    public String getYelp_item_id() {
        return yelp_item_id;
    }

    public void setYelp_item_id(final String yelp_item_id) {
        this.yelp_item_id = yelp_item_id;
    }

    public String getLast_published_date() {
        return last_published_date;
    }

    public void setLast_published_date(final String last_published_date) {
        this.last_published_date = last_published_date;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(final String format) {
        this.format = format;
    }

    public List<DCRRelatedItemObject> getRelated_items() {
        return related_items;
    }

    public void setRelated_items(final List<DCRRelatedItemObject> related_items) {
        this.related_items = related_items;
    }
   /* public List<DCRCommonObject> getRelated_items() {
        return related_items;
    }

    public void setRelated_items(final List<DCRCommonObject> related_items) {
        this.related_items = related_items;
    }*/

    public String getSource_name() {
        return source_name;
    }

    public void setSource_name(final String source_name) {
        this.source_name = source_name;
    }

    public DCRCommonObject getInternational_star_rating() {
        return international_star_rating;
    }

    public void setInternational_star_rating(final DCRCommonObject international_star_rating) {
        this.international_star_rating = international_star_rating;
    }

    public List<DCRCommonObject> getTypes() {
        return types;
    }

    public void setTypes(final List<DCRCommonObject> types) {
        this.types = types;
    }

    public List<DCRCommonObject> getLinks() {
        return links;
    }

    public void setLinks(final List<DCRCommonObject> links) {
        this.links = links;
    }

    public DCRCommonObject getLocation_type() {
        return location_type;
    }

    public void setLocation_type(final DCRCommonObject location_type) {
        this.location_type = location_type;
    }

    public List<DCRCommonObject> getSub_types() {
        return sub_types;
    }

    public void setSub_types(final List<DCRCommonObject> sub_types) {
        this.sub_types = sub_types;
    }

    public List<DCRCommonObject> getAtmosphere() {
        return atmosphere;
    }

    public void setAtmosphere(final List<DCRCommonObject> atmosphere) {
        this.atmosphere = atmosphere;
    }

    public List<DCRCommonObject> getRoom_types() {
        return room_types;
    }

    public void setRoom_types(final List<DCRCommonObject> room_types) {
        this.room_types = room_types;
    }

    public List<DCRCommonObject> getPeople() {
        return people;
    }

    public void setPeople(final List<DCRCommonObject> people) {
        this.people = people;
    }

    public List<DCRCommonObject> getHotel_amenities() {
        return hotel_amenities;
    }

    public void setHotel_amenities(final List<DCRCommonObject> hotel_amenities) {
        this.hotel_amenities = hotel_amenities;
    }

    public List<DCRCommonObject> getSports_hobby_amenities() {
        return sports_hobby_amenities;
    }

    public void setSports_hobby_amenities(final List<DCRCommonObject> sports_hobby_amenities) {
        this.sports_hobby_amenities = sports_hobby_amenities;
    }


    public List<DCRImageObject> getImages() {
        return images;
    }

    public void setImages(final List<DCRImageObject> images) {
        this.images = images;
    }

    public String getPricePoint() {
        String pricePoint = "";
        int Price_range = 0;
        if (getPrice_range() != null && CommonUtils.isStringValid(getPrice_range().getName()))
            try {
                Price_range = Integer.parseInt(getPrice_range().getName());
            } catch (NumberFormatException e) {

            }
        for (int i = 0; i < Price_range; i++) {
            pricePoint += "\u20BD";
        }
        /*if (getPrice_currency() != null && CommonUtils.isStringValid(getPrice_currency()
                                                                          .getName())) {
            pricePoint+= getPrice_currency()
                                 .getName();
        }


        if (getPrice_range() != null && CommonUtils.isStringValid(getPrice_range().getName())) {
            pricePoint+= getPrice_range()
                                 .getName();
        }*/
        return pricePoint;
    }

    public DCRPrivilegeObject() {
    }

    public static String getAlphabetRu(String string) {
        String[] strings = AppContext.getSharedInstance()
                .getResources()
                .getStringArray(R.array.ru_alphabet);
        String alpha = "#";
        for (String s : strings
                ) {
            if (string.toUpperCase().startsWith(s)) {
                alpha = s;
            }
        }
        return alpha;
    }

    public static String getAlphabetEn(String string) {
        String[] strings = AppContext.getSharedInstance()
                .getResources()
                .getStringArray(R.array.en_alphabet);
        String alpha = "#";
        for (String s : strings
                ) {
            if (string.toUpperCase().startsWith(s)) {
                alpha = s;
            }
        }
        return alpha;
    }

    public void setFullAddress(final String fullAddress) {
        this.fullAddress = fullAddress;
    }

    /*  public static int getAlphabetNumber(String string) {
            String[] strings = AppContext.getSharedInstance()
                                         .getResources()
                                         .getStringArray(R.array.russia_alphabet);
            int number =0;
            for (int i = 0; i < strings.length; i++) {
                if (string.startsWith(strings[i])) {
                    number = i;
                    break;
                }
            }
            return number;
        }
    */
    private String fullAddress = "";

    public String getAddress() {
        String address = "";
        if (CommonUtils.isStringValid(getAddress_line_1())) {
            address += getAddress_line_1();
        }

        if (CommonUtils.isStringValid(getAddress_line_2())) {
            if (CommonUtils.isStringValid(address)) {
                address += ", ";
            }
            address += getAddress_line_2();
        }

        if (CommonUtils.isStringValid(getAddress_metropolitan())) {
            if (CommonUtils.isStringValid(address)) {
                address += ", ";
            }
            address += getAddress_metropolitan();
        }

        if (CommonUtils.isStringValid(getAddress_neighbourhood())) {
            if (CommonUtils.isStringValid(address)) {
                address += ", ";
            }
            address += getAddress_neighbourhood();
        }

        if (getAddress_city() != null && CommonUtils.isStringValid(getAddress_city().getName())) {
            if (CommonUtils.isStringValid(address)) {
                address += ", ";
            }
            address += getAddress_city().getName();
        }

        if (getAddress_state() != null && CommonUtils.isStringValid(getAddress_state().getName())) {
            if (CommonUtils.isStringValid(address)) {
                address += ", ";
            }
            address += getAddress_state().getName();
        }

        if (getAddress_country() != null && CommonUtils.isStringValid(getAddress_country().getName())) {
            if (CommonUtils.isStringValid(address)) {
                address += ", ";
            }
            address += getAddress_country().getName();
        }

        if (CommonUtils.isStringValid(getAddress_post_code())) {
            if (CommonUtils.isStringValid(address)) {
                address += ", ";
            }
            address += getAddress_post_code();
        }
        if (CommonUtils.isStringValid(address)) {
            fullAddress = address;
        }
        return fullAddress;
    }

    public String getTreatmeants() {
        return "";
    }

}
