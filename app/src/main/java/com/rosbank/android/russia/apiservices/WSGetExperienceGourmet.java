package com.rosbank.android.russia.apiservices;

import com.rosbank.android.russia.apiservices.RequestModel.ExperienceGourmetRequest;
import com.rosbank.android.russia.apiservices.ResponseModel.GetExperienceGourmetResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.ApiProviderClient;
import com.rosbank.android.russia.model.UserItem;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Callback;

/**
 * chau.nguyen
*/
public class WSGetExperienceGourmet extends ApiProviderClient<GetExperienceGourmetResponse> {

    private int page = 1;
    private final int perPage = 10;

    public WSGetExperienceGourmet(){

    }

    public void setTotalItems(int number){
        page = number/perPage;
        if(page%perPage!=0){
            page += 1;
        }
    }

    @Override
    public void run(Callback<GetExperienceGourmetResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }

    @Override
    protected boolean isUseAuthenticateInApi() {
        return true;
    }

    @Override
    public void runApi() {

        ExperienceGourmetRequest request = new ExperienceGourmetRequest();
        String id = UserItem.getLoginedId();
        if(!id.isEmpty())
            request.setUserID(id);
        request.setPage(page);
        request.setRecordPerPage(perPage);

        serviceInterface.getExperiencesGourmet(request).enqueue(callback);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("User-Agent", "Retrofit-Sample-App");
        return headers;
    }

}
