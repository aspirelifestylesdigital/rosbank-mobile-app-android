package com.rosbank.android.russia.apiservices;

import com.rosbank.android.russia.apiservices.ResponseModel.GetCitiesResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.ApiProviderClient;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Callback;


public class WSGetCities
        extends ApiProviderClient<GetCitiesResponse> {
    private String mCountryCode;

    public WSGetCities(String countryCode) {
        this.mCountryCode = countryCode;
    }

    @Override
    public void run(Callback<GetCitiesResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }

    @Override
    protected boolean isUseAuthenticateInApi() {
        return true;
    }

    @Override
    public void runApi() {
        serviceInterface.getCities(mCountryCode)
                        .enqueue(callback);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("User-Agent",
                    "Retrofit-Sample-App");
        return headers;
    }

}
