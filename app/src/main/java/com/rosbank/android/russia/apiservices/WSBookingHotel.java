package com.rosbank.android.russia.apiservices;

import android.support.annotation.NonNull;

import com.rosbank.android.russia.apiservices.RequestModel.BookHotelRequest;
import com.rosbank.android.russia.apiservices.ResponseModel.BookHotelResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.ApiProviderClient;
import com.rosbank.android.russia.utils.StringUtil;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Callback;


public class WSBookingHotel
        extends ApiProviderClient<BookHotelResponse> {

    private BookHotelRequest request;

    public WSBookingHotel(){

    }
    public void booking(final BookHotelRequest request){
        this.request = request;

    }

    @Override
    public void run(Callback<BookHotelResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }

    @Override
    protected boolean isUseAuthenticateInApi() {
        return true;
    }

    @Override
    public void runApi() {
        RequestBody partBookingId = createPartFromString(request.getBookingId()==null?"":request.getBookingId());
        RequestBody partUserID = createPartFromString(request.getUserID()==null?"":request.getUserID());
        RequestBody partBookingItemId = createPartFromString(request.getBookingItemId()==null?"":request.getBookingItemId());
        RequestBody partPhone = createPartFromString(request.getPhone()==null?"":request.getPhone()+"");
        RequestBody partEmail = createPartFromString(request.getEmail()==null?"":request.getEmail()+"");
        RequestBody partBoth = createPartFromString(request.getBoth()==null?"":request.getBoth()+"");
        RequestBody partMobileNumber = createPartFromString(request.getMobileNumber()==null?"":request.getMobileNumber()+"");
        RequestBody partEmailAddress = createPartFromString(request.getEmailAddress()==null?"":request.getEmailAddress()+"");
        RequestBody partCheckInDate = createPartFromString(request.getCheckInDate()==null?"":request.getCheckInDate()+"");
        RequestBody partCheckOutDate = createPartFromString(request.getCheckOutDate()==null?"":request.getCheckOutDate()+"");
        RequestBody partRoomType = createPartFromString(request.getRoomType()==null?"":request.getRoomType()+"");
        RequestBody partGuestName = createPartFromString(request.getGuestName()==null?"":request.getGuestName()+"");
        RequestBody partLoyaltyProgrammeMembership = createPartFromString(request.getLoyaltyProgrammeMembership()==null?"":request.getLoyaltyProgrammeMembership()+"");
        RequestBody partCountry = createPartFromString(request.getCountry()==null?"":request.getCountry()+"");
        RequestBody partCity = createPartFromString(request.getCity()==null?"":request.getCity()+"");
        RequestBody partNumberOfAdults = createPartFromString(request.getNumberOfAdults()==null?"":request.getNumberOfAdults()+"");
        RequestBody partNumberOfKids = createPartFromString(request.getNumberOfKids()==null?"":request.getNumberOfKids()+"");
        RequestBody partSmokingPreference = createPartFromString(request.getSmokingPreference()==null?"":request.getSmokingPreference()+"");
        RequestBody partSpecialRequirements = createPartFromString(request.getSpecialRequirements()==null?"":request.getSpecialRequirements()+"");

        MultipartBody.Part partUploadPhoto = null;
        if (request.getUploadPhoto() != null) {
            File file = new File(request.getUploadPhoto());
            if (!StringUtil.isEmpty(request.getUploadPhoto()) && file.exists()) {
                RequestBody requestFile =
                        RequestBody.create(MediaType.parse("multipart/form-data"),
                                           file);
                partUploadPhoto =
                        MultipartBody.Part.createFormData("picture",
                                                          file.getName(),
                                                          requestFile);
            } else {

            }
        }


        serviceInterface.bookHotel(partBookingId,
                partUserID,
                partBookingItemId,
                partUploadPhoto,
                partPhone,
                partEmail,
                partBoth,
                partMobileNumber,
                partEmailAddress,
                partCheckInDate,
                partCheckOutDate,
                partRoomType,partGuestName,partLoyaltyProgrammeMembership,
                partCountry,
                partCity,
                partNumberOfAdults,
                partNumberOfKids,partSmokingPreference,
                partSpecialRequirements
        ).enqueue(callback);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("User-Agent", "Retrofit-Sample-App");
        return headers;
    }

    @NonNull
    private RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(
                MediaType.parse("multipart/form-data"), descriptionString);
    }


}
