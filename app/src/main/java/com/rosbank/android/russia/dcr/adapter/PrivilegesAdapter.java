package com.rosbank.android.russia.dcr.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.dcr.model.DCRPrivilegeObject;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.FontUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.rosbank.android.russia.utils.StringUtil;

import java.util.ArrayList;

/**
 * Created by anh.trinh on 9/20/2016.
 */
public class PrivilegesAdapter
        extends ArrayAdapter<DCRPrivilegeObject> implements Filterable {
    private final Context context;
    private ArrayList<DCRPrivilegeObject> values;
    private ArrayList<DCRPrivilegeObject> filterValueList;
    private CustomFilter mFilter;
    private String privilegeType = "";
    private Typeface type = Typeface.createFromAsset(getContext().getAssets(),FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);

    public PrivilegesAdapter(Context context, ArrayList<DCRPrivilegeObject> values, ArrayList<DCRPrivilegeObject> filterValues, String privilegeType) {
        super(context, -1, values);
        this.context = context;
        this.values = values;
        this.mFilter = new CustomFilter(this);
        filterValueList = filterValues;
        if (filterValueList == null) {
            filterValueList = new ArrayList<>();
        }
        if (this.values != null) {
            filterValueList.addAll(this.values);
        }
        this.privilegeType = privilegeType;
    }

    public void setValues(final ArrayList<DCRPrivilegeObject> values) {
        this.values = values;
        this.filterValueList = values;
        notifyDataSetChanged();
    }

    public void filter(String text) {
        filterValueList.clear();
        if (TextUtils.isEmpty(text) || TextUtils.isEmpty(text.trim())) {
            filterValueList.addAll(values);
        } else {
            for (DCRPrivilegeObject parcelable : values) {
                if (parcelable.getName().toString().toLowerCase().startsWith(text.toLowerCase().trim())) {
                    filterValueList.add(parcelable);
                }
            }
        }

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.layout_item_privilege, parent, false);
        TextView textView = (TextView) rowView.findViewById(R.id.tvPrivilegeName);

        //CommonUtils.setFontForViewRecursive(textView, FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
        //textView.setTypeface(type);
        ImageView imgPrivilege = (ImageView) rowView.findViewById(R.id.imgPrivilege);
        //set data
        DCRPrivilegeObject privilegeObject = filterValueList.get(position);
        if (privilegeObject != null) {
            if (privilegeObject.getName() != null) {
                String clearName = privilegeObject.getName();
                if (clearName.contains("\n")) {
                    textView.setText(clearName.replace("\n", " "));
                } else {
                    textView.setText(clearName);
                }
            }
            if (CommonUtils.isStringValid(privilegeObject
                    .getLogo_url())) {
                if (CommonUtils.isUrlLink(privilegeObject.getLogo_url())) {
                    Glide.with(context)
                            .load(privilegeObject
                                    .getLogo_url())
                            .thumbnail(0.5f)
                            .centerCrop()
                            .crossFade()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(imgPrivilege);
                } else {
                    Drawable drawable = getContext().getResources().getDrawable(getContext().getResources()
                            .getIdentifier(privilegeObject.getLogo_url(), "drawable", getContext().getPackageName()));
                    imgPrivilege.setImageDrawable(drawable);
                }
            } else {
                if (privilegeType.equals(DCRPrivilegeObject.PRIVILEGE_TYPE_RESTAURANT)) {
                    imgPrivilege.setImageResource(R.drawable.img_experiences_item_gourmet);
                }
                if (privilegeType.equals(DCRPrivilegeObject.PRIVILEGE_TYPE_HOTEL)) {
                    imgPrivilege.setImageResource(R.drawable.img_concierge_service_hotel);
                }
                if (privilegeType.equals(DCRPrivilegeObject.PRIVILEGE_TYPE_SPA)) {
                    imgPrivilege.setImageResource(R.drawable.img_experiences_item_lifestyle);
                }
            }
        }
        return rowView;
    }

    @Override
    public int getCount() {
        return filterValueList == null ? 0 : filterValueList.size();
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    private class CustomFilter extends Filter {
        private PrivilegesAdapter mAdapter;

        private CustomFilter(PrivilegesAdapter mAdapter) {
            super();
            this.mAdapter = mAdapter;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            filterValueList.clear();
            final FilterResults results = new FilterResults();
            if (constraint.length() == 0) {
                filterValueList.addAll(values);
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();
                for (final DCRPrivilegeObject obj : values) {
                    if (obj.getName().toString().toLowerCase().startsWith(filterPattern)) {
                        filterValueList.add(obj);
                    }
                }
            }
            results.values = filterValueList;
            results.count = filterValueList.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mAdapter.notifyDataSetChanged();
        }
    }
}

