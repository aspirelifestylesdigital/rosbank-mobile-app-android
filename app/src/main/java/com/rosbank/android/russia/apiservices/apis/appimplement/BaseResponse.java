package com.rosbank.android.russia.apiservices.apis.appimplement;

import com.google.gson.annotations.Expose;

public class BaseResponse {
    public static final int STATUS_OK = 200;
    public static final String BODY_NULL = "NULL";

    @Expose
    public int Status;
    @Expose
    public String Message;

    public Boolean isSuccess(){
        if(STATUS_OK == Status){
            return true;
        }
        return false;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    public String getMessage() {
        if(Message==null || BODY_NULL.equalsIgnoreCase(Message))
            return "Respone body is null.";
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    /*
    public static String INVALID_SESSION = "400";
    public String handlerName;

    private String mStatus;
    private String mMessage;

    public BaseResponse(){

    }



    public Boolean isInvalidSessionToken(){
        if(mStatus!=null && INVALID_SESSION.equals(mStatus)){
            return true;
        }
        return false;
    }

    public String getStatus(){return  mStatus;}

    public void setStatus(String status) {
        this.mStatus = status;
    }
    public void setError(String message) {
        this.mMessage = message;
    }

    public String getMessage(){return mMessage;}

    public void setHandlerName(String handlerName) {
        this.handlerName = handlerName;
    }*/
}
