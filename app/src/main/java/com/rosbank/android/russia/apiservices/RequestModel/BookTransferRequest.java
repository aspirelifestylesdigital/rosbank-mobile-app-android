package com.rosbank.android.russia.apiservices.RequestModel;


import com.google.gson.annotations.Expose;

public class BookTransferRequest
        extends BaseRequest {
    @Expose
    private String BookingId = "";
    @Expose
    private String UserID = "";
    @Expose
    private String UploadPhoto = "";
    @Expose
    private boolean Phone = false;
    @Expose
    private Boolean Email = false;
    @Expose
    private Boolean Both = false;
    @Expose
    private String MobileNumber = "";
    @Expose
    private String EmailAddress = "";
    @Expose
    private String TransportType = "";
    @Expose
    private String EpochPickupDate = "";
    @Expose
    private String PickupTime = "";
    @Expose
    private String PickupLocation = "";
    @Expose
    private String DropOffLocation = "";

    public Integer getNbOfPassenger() {
        return NbOfPassenger;
    }

    public void setNbOfPassenger(final Integer nbOfPassenger) {
        NbOfPassenger = nbOfPassenger;
    }

    public String getTransportType() {
        return TransportType;
    }

    public void setTransportType(final String transportType) {
        TransportType = transportType;
    }

    @Expose
    private Integer NbOfPassenger = 0;
    @Expose
    private String SpecialRequirements = "";

    public String getSpecialRequirements() {
        return SpecialRequirements;
    }

    public void setSpecialRequirements(final String specialRequirements) {
        SpecialRequirements = specialRequirements;
    }

    public String getBookingId() {
        return BookingId;
    }

    public void setBookingId(final String bookingId) {
        BookingId = bookingId;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(final String userID) {
        UserID = userID;
    }

    public String getUploadPhoto() {
        return UploadPhoto;
    }

    public void setUploadPhoto(final String uploadPhoto) {
        UploadPhoto = uploadPhoto;
    }

    public Boolean getPhone() {
        return Phone;
    }

    public void setPhone(final Boolean phone) {
        Phone = phone;
    }

    public Boolean getEmail() {
        return Email;
    }

    public void setEmail(final Boolean email) {
        Email = email;
    }

    public Boolean getBoth() {
        return Both;
    }

    public void setBoth(final Boolean both) {
        Both = both;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(final String mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public String getEmailAddress() {
        return EmailAddress;
    }

    public void setEmailAddress(final String emailAddress) {
        EmailAddress = emailAddress;
    }

    public String getEpochPickupDate() {
        return EpochPickupDate;
    }

    public void setEpochPickupDate(final String epochPickupDate) {
        EpochPickupDate = epochPickupDate;
    }

    public String getPickupTime() {
        return PickupTime;
    }

    public void setPickupTime(final String pickupTime) {
        PickupTime = pickupTime;
    }

    public String getPickupLocation() {
        return PickupLocation;
    }

    public void setPickupLocation(final String pickupLocation) {
        PickupLocation = pickupLocation;
    }

    public String getDropOffLocation() {
        return DropOffLocation;
    }

    public void setDropOffLocation(final String dropOffLocation) {
        DropOffLocation = dropOffLocation;
    }
   }
