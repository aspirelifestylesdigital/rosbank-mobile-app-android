package com.rosbank.android.russia.fragment;

import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.utils.Fontfaces;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by nga.nguyent on 10/25/2016.
 */

public class NotificationsFragment extends BaseFragment {
    @BindView(R.id.tvNotificationTitle)
    TextView tvNotificationTitle;
    @BindView(R.id.imgArrowDown)
    ImageView imgArrowDown;
    @BindView(R.id.rclView)
    RecyclerView rclView;
    @BindView(R.id.llTop)
    LinearLayout llTop;
    //
    @BindView(R.id.llNotificationSetting)
    LinearLayout llNotificationSetting;
    @BindView(R.id.tvNotificationSettingMessage)
    TextView tvNotificationSettingMessage;
    @BindView(R.id.swtAnnouncements)
    Switch swtAnnouncements;
    @BindView(R.id.swtRecommendations)
    Switch swtRecommendations;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;
    @BindView(R.id.btnCancel)
    Button btnCancel;

    private static final float INITIAL_POSITION = 0.0f;
    private static final float ROTATED_POSITION = 180f;
    private final boolean HONEYCOMB_AND_ABOVE = Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;

    private boolean expandView = false;

    @Override
    protected int layoutId() {
        return R.layout.fragment_notifications;
    }

    @Override
    protected void initView() {
        tvNotificationTitle.setTypeface(Fontfaces.getFont(getContext(), Fontfaces.FONTLIST.AVENIR_NEXT_LT_MEDIUM));
        tvNotificationSettingMessage.setTypeface(Fontfaces.getFont(getContext(), Fontfaces.FONTLIST.Avenirnext_demibold));
        swtAnnouncements.setTypeface(Fontfaces.getFont(getContext(), Fontfaces.FONTLIST.AVENIR_NEXT_LT_MEDIUM));
        swtRecommendations.setTypeface(Fontfaces.getFont(getContext(), Fontfaces.FONTLIST.AVENIR_NEXT_LT_MEDIUM));
        btnSubmit.setTypeface(Fontfaces.getFont(getContext(), Fontfaces.FONTLIST.AvenirLTStd_Heavy));
        btnCancel.setTypeface(Fontfaces.getFont(getContext(), Fontfaces.FONTLIST.AvenirLTStd_Heavy));
    }

    @Override
    protected void bindData() {

    }

    @Override
    protected boolean onBack() {
        return false;
    }

    @OnClick(R.id.llTop)
    void onNotificationTitleClick(){
        menuChangeState();
    }

    private void menuChangeState(){
        expandView = !expandView;
        if (expandView) {
            imgArrowDown.setRotation(ROTATED_POSITION);
            llNotificationSetting.setVisibility(View.VISIBLE);
            Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.slide_in_top);
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {

                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            llNotificationSetting.startAnimation(animation);
        } else {
            imgArrowDown.setRotation(INITIAL_POSITION);
            Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.slide_out_top);
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    llNotificationSetting.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            llNotificationSetting.startAnimation(animation);
        }
    }

}
