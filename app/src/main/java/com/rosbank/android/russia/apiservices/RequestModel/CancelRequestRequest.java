package com.rosbank.android.russia.apiservices.RequestModel;

import com.google.gson.annotations.Expose;

/**
 * Created by ThuNguyen on 10/26/2016.
 */

public class CancelRequestRequest extends BaseRequest{
    @Expose
    private String BookingId;
    @Expose
    private String UserID;

    public String getBookingId() {
        return BookingId;
    }

    public void setBookingId(String bookingId) {
        BookingId = bookingId;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }
}
