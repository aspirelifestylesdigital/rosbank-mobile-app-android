package com.rosbank.android.russia.fragment;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.adapter.SelectionCountryOrCityAdapter;
import com.rosbank.android.russia.apiservices.ResponseModel.GetCitiesResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.GetCountriesResponse;
import com.rosbank.android.russia.apiservices.WSGetCities;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.model.CountryObject;
import com.rosbank.android.russia.utils.CommonUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by anh.trinh on 9/13/2016.
 */
public class SelectionCountryOrCityFragment
        extends BaseFragment implements Callback {

    @Override
    public void onResponse(final Call call,
                           final Response response) {
        if (response.body() instanceof GetCountriesResponse) {
            hideDialogProgress();
            if(getActivity()==null) {
                return;
            }
            GetCountriesResponse getCountriesResponse = ((GetCountriesResponse) response.body());
            int code = getCountriesResponse.getStatus();
            if (code == 200) {
                data.clear();
                data.addAll(getCountriesResponse.getData());
                adapter = new SelectionCountryOrCityAdapter(getActivity(),
                                                     data, filterData,
                                                     dataSelected);
                mListview.setAdapter(adapter);
            }else{
//                showToast(getCountriesResponse.getMessage());
            }
        }
        if (response.body() instanceof GetCitiesResponse) {
            hideDialogProgress();
            if(getActivity()==null) {
                return;
            }
            GetCitiesResponse citiesResponse = ((GetCitiesResponse) response.body());
            int code = citiesResponse.getStatus();
            if (code == 200) {
                data.clear();
                data.addAll(citiesResponse.getData());
                adapter = new SelectionCountryOrCityAdapter(getActivity(),
                                                        data, filterData,
                                                        dataSelected);
                mListview.setAdapter(adapter);
            }else{
//                showToast(citiesResponse.getMessage());
            }
        }
    }

    @Override
    public void onFailure(final Call call,
                          final Throwable t) {
        hideDialogProgress();
        if(getActivity()==null) {
            return;
        }
//        showToast(getString(R.string.text_server_error_message));
        /*Bundle bundle = new Bundle();
        bundle.putString(ErrorHomeFragment.PRE_ERROR_MESSAGE,
                         getString(R.string.text_server_error_message));
        ErrorHomeFragment fragment = new ErrorHomeFragment();
        fragment.setArguments(bundle);
        pushFragment(fragment,
                     true,
                     true);*/
    }

    public interface SelectionCallback {
        void onFinishSelection(Bundle bundle);
    }

    public static final String PRE_SELECTION_DATA_FILTER = "selection_data_filter";
    public static final String PRE_SELECTION_DATA = "selection_data";
    public static final String PRE_SELECTION_TYPE = "selection_type";
    public static final String PRE_SELECTION_COUNTRY = "Country";
    public static final String PRE_SELECTION_CITY = "City";
    @BindView(R.id.listview)
    ListView mListview;
    @BindView(R.id.search_country)
    SearchView mSearchView;

    String typeSelection;
    ArrayList<Parcelable> data = new ArrayList<Parcelable>();
    ArrayList<Parcelable> filterData = new ArrayList<Parcelable>();
    SelectionCountryOrCityAdapter adapter;
    Parcelable dataSelected = null;
    CountryObject countryFilter = null;

    SelectionCallback selectionCallback;

    public void setSelectionCallBack(SelectionCallback selectionCallBack) {
        this.selectionCallback = selectionCallBack;
    }


    @Override
    protected int layoutId() {
        return R.layout.fragment_selection_country;
    }

    @Override
    protected void initView() {
        mListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> adapterView,
                                    final View view,
                                    final int i,
                                    final long l) {
                if (selectionCallback != null) {
                    Bundle bundle = new Bundle();
                    bundle.putString(PRE_SELECTION_TYPE,
                                     typeSelection);
                    if(dataSelected == null || (filterData.get(i) != null &&
                            !dataSelected.toString().equalsIgnoreCase(filterData.get(i).toString()))) {
                    bundle.putParcelable(PRE_SELECTION_DATA,
                                     filterData.get(i));
                    }
                    selectionCallback.onFinishSelection(bundle);
                }
                onBackPress();

            }
        });
        mSearchView.setOnQueryTextListener(new android.support.v7.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(final String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(final String newText) {
                ImageView searchViewIcon = (ImageView)mSearchView.findViewById(android.support.v7.appcompat.R.id.search_mag_icon);
                searchViewIcon.setVisibility(TextUtils.isEmpty(newText) ? View.VISIBLE : View.GONE);
                adapter.getFilter().filter(newText);
                return false;
            }
        });
        mSearchView.setIconifiedByDefault(false);
        mSearchView.setQueryHint(getString(R.string.text_selection_country_hint));
        CommonUtils.clearFocusLineSearchView(mSearchView);

        if (getArguments() != null) {
            typeSelection = getArguments().getString(PRE_SELECTION_TYPE,
                                                     "");
            dataSelected = getArguments().getParcelable(PRE_SELECTION_DATA);
            String[] resource;
            if (typeSelection.equals(PRE_SELECTION_COUNTRY)) {

                /*showDialogProgress();
                WSGetCountries wsGetCountries = new WSGetCountries();
                wsGetCountries.run(this);*/
                data.clear();
                data.addAll(CountryObject.getCountryObjectList());
                adapter = new SelectionCountryOrCityAdapter(getActivity(),
                        data, filterData,
                        dataSelected);
                mListview.setAdapter(adapter);
                //edtFilter.setVisibility(View.VISIBLE);

            } else {
                if (typeSelection.equals(PRE_SELECTION_CITY)) {
                    countryFilter = getArguments().getParcelable(PRE_SELECTION_DATA_FILTER);
                    if(countryFilter!=null && CommonUtils.isStringValid(countryFilter.getCountryCode())) {
                        showDialogProgress();
                        WSGetCities wsGetCities = new WSGetCities(countryFilter.getCountryCode());
                        wsGetCities.run(this);
                    }
                }
            }
        }


        }

    @Override
    protected void bindData() {

    }

    @Override
    public boolean onBack() {
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (typeSelection.equals(PRE_SELECTION_COUNTRY)) {
            setTitle(getString(R.string.text_title_country));
        }
        else {
            setTitle(getString(R.string.text_title_city));
        }

        hideToolbarMenuIcon();
        showLogoApp(false);

        /*if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showLogoApp(false);
            ((HomeActivity) getActivity()).hideToolbarMenuIcon();
            ((HomeActivity) getActivity()).setTitle(getString(R.string.text_title_filter));
        }*/
    }

    @Override
    public void onPause() {
        super.onPause();
        showToolbarMenuIcon();
        /*if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showToolbarMenuIcon();
        }*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater,
                                           container,
                                           savedInstanceState);
        ButterKnife.bind(this,
                         rootView);
        return rootView;
    }

}
