package com.rosbank.android.russia.fragment;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;

/**
 * Created by nga.nguyent on 10/27/2016.
 */

public class LiveChat extends BaseFragment{
    @Override
    protected int layoutId() {
        return R.layout.fragment_livechat;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void bindData() {

    }

    @Override
    protected boolean onBack() {
        return false;
    }
}
