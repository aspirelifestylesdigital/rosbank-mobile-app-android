package com.rosbank.android.russia.apiservices.RequestModel;


import com.google.gson.annotations.Expose;

import java.util.ArrayList;

public class GetListMyRequestRequest
        extends BaseRequest {
    @Expose
    private String UserID;
    @Expose
    private Integer Page;
    @Expose
    private Integer RecordPerPage;
    @Expose
    private String SortBy;

    public ArrayList<String> getTypeOfRequests() {
        return TypeOfRequests;
    }

    public void setTypeOfRequests(final ArrayList<String> typeOfRequests) {
        TypeOfRequests = typeOfRequests;
    }

    public Integer getPage() {
        return Page;
    }

    public void setPage(final Integer page) {
        Page = page;
    }

    public Integer getRecordPerPage() {
        return RecordPerPage;
    }

    public void setRecordPerPage(final Integer recordPerPage) {
        RecordPerPage = recordPerPage;
    }

    public String getSortBy() {
        return SortBy;
    }

    public void setSortBy(final String sortBy) {
        SortBy = sortBy;
    }

    @Expose
    private ArrayList<String> TypeOfRequests;
    public String getUserID() {
        return UserID;
    }

    public void setUserID(final String userID) {
        UserID = userID;
    }



}
