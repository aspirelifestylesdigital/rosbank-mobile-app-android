package com.rosbank.android.russia.fragment;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.adapter.MyRequestAdapter;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CICallback;
import com.rosbank.android.russia.apiservices.b2c.B2CWSGetRecentRequest;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.B2CGetRecentRequestRequest;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.model.concierge.MyRequestObject;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.FontUtils;
import com.rosbank.android.russia.widgets.MyRequestsHeaderView;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by anh.trinh on 9/13/2016.
 */
public class MyRequestHistoryFragment
        extends BaseFragment implements MyRequestsHeaderView.MyRequestHeaderViewListener, B2CICallback {

    @BindView(R.id.header)
    MyRequestsHeaderView mHeader;
    @BindView(R.id.tv_request_title)
    TextView mTvRequestTitle;
    @BindView(R.id.listview_request)
    RecyclerView mListviewRequest;
    @BindView(R.id.tv_no_result)
    TextView mTvNoResult;
    String mUserID = "";
    Integer mPage = 1;
    Integer mPerPage = 20;
    String mSortBy = "history";
    Integer mTotal = -1;
    boolean isNeedLoadMore = true;
    private int firstVisibleItem, visibleItemCount, totalItemCount;
    private boolean loading = false;
    String mFilterGroupType = MyRequestsHeaderView.PRE_REQUEST_TYPE_ALL_HISTORY;
    AppConstant.BOOKING_FILTER_TYPE mFilterType = AppConstant.BOOKING_FILTER_TYPE.All;

    private List<MyRequestObject> data = new ArrayList<>();
    MyRequestAdapter adapter;

    @Override
    protected int layoutId() {
        return R.layout.fragment_my_request_history;
    }

    @Override
    protected void initView() {
        CommonUtils.setFontForTextView(mTvRequestTitle,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        CommonUtils.setFontForTextView(mTvNoResult,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
        mHeader.setRequestHeaderListener(this);
        mTvRequestTitle.setText(R.string.text_my_request_view_all_request);
        mTvNoResult.setText(R.string.text_my_request_view_all_request_no_result);
        adapter = new MyRequestAdapter(data,
                                       new MyRequestAdapter.MyRequestListener() {
                                           @Override
                                           public void onAmendClick(final MyRequestObject object) {

                                           }

                                           @Override
                                           public void onCancelClick(final MyRequestObject object) {
                                               CancelRequestFragment.openCancelRequest(MyRequestHistoryFragment.this, object);
                                           }

                                           @Override
                                           public void onCalendarClick(final MyRequestObject object) {

                                           }
                                       },getContext());
        LinearLayoutManager layoutManager = new LinearLayoutManager(view.getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mListviewRequest.setLayoutManager(layoutManager);
        mListviewRequest.setAdapter(adapter);
        mListviewRequest.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView,
                                             int newState) {
                super.onScrollStateChanged(recyclerView,
                                           newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView,
                                   int dx,
                                   int dy) {
                if (dy > 0) {
                    totalItemCount = layoutManager.getItemCount();
                    visibleItemCount = layoutManager.getChildCount();
                    firstVisibleItem = layoutManager.findFirstVisibleItemPosition();

                    if (!loading && (firstVisibleItem + visibleItemCount) >= totalItemCount && isNeedLoadMore) {
                        loading = true;
                        requestLoadMore();
                    }
                }
            }
        });
        // Load recent list
        resetAndLoadRecentList();
    }

    @Override
    public void onResume() {
        super.onResume();
        /*mUserID = SharedPreferencesUtils.getPreferences(AppConstant.USER_ID_PRE,
                                                        "");
        if (!mUserID.equals("")) {
            data.clear();
            mPage =1;
            showDialogProgress();
            GetListMyRequestRequest getListMyRequestRequest = new GetListMyRequestRequest();
            getListMyRequestRequest.setUserID(mUserID);
            getListMyRequestRequest.setPage(mPage);
            getListMyRequestRequest.setRecordPerPage(mPerPage);
            getListMyRequestRequest.setSortBy(mSortBy);
            WSGetMyRequests wsGetMyRequests = new WSGetMyRequests();
            wsGetMyRequests.getMyRequests(getListMyRequestRequest);
            wsGetMyRequests.run(this);
        }*/
    }
    @Subscribe
    public void updateRecentList(AppConstant.CONCIERGE_EDIT_TYPE requestType){
        data.clear();
        mPage = 1;
        B2CWSGetRecentRequest b2CWSGetRecentRequest = new B2CWSGetRecentRequest(this);
        b2CWSGetRecentRequest.setPage(mPage);
        b2CWSGetRecentRequest.run(null);
    }
    private void resetAndLoadRecentList(){
        data.clear();
        mPage = 1;
        getRecentRequest();
    }
    private void getRecentRequest(){
        if(mPage == 1) {
            showDialogProgress();
        }
        B2CWSGetRecentRequest b2CWSGetRecentRequest = new B2CWSGetRecentRequest(this);
        b2CWSGetRecentRequest.setPage(mPage);
        b2CWSGetRecentRequest.run(null);
    }
    private void requestLoadMore() {
        scrollShowLoadMore();
        mPage++;
        getRecentRequest();


    }
    private void scrollShowLoadMore() {
        mListviewRequest.post(new Runnable() {
            @Override
            public void run() {
                adapter.addViewLoading();
            }
        });
    }

    /**
     * use post Runnable when hide View load more
     */
    private void scrollHideLoadMore() {
        mListviewRequest.post(new Runnable() {
            @Override
            public void run() {
                adapter.removeViewLoading();
            }
        });
    }

    private void resetLoadingMore() {
        loading = false;
        scrollHideLoadMore();
    }
    @Override
    protected void bindData() {
    }

    @Override
    public boolean onBack() {
        return false;
    }


    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater,
                                           container,
                                           savedInstanceState);
        ButterKnife.bind(this,
                         rootView);
        return rootView;
    }

    @Override
    public void onRadioCheckChange(final String requestType) {
        switch (requestType) {
            case MyRequestsHeaderView.PRE_REQUEST_TYPE_ALL:
                mTvRequestTitle.setText(R.string.text_my_request_view_all_request);
                //  mTvNoResult.setText(R.string.text_my_request_view_all_request_no_result);
                mFilterType = AppConstant.BOOKING_FILTER_TYPE.All;
                break;
            case MyRequestsHeaderView.PRE_REQUEST_TYPE_GOURMET:
                mTvRequestTitle.setText(R.string.text_my_request_gourmet_request);
                //  mTvNoResult.setText(R.string.text_my_request_gourmet_request_no_result);
                mFilterType = AppConstant.BOOKING_FILTER_TYPE.Restaurant;
                break;
            case MyRequestsHeaderView.PRE_REQUEST_TYPE_HOTEL:
                mTvRequestTitle.setText(R.string.text_my_request_hotel_request);
                //mTvNoResult.setText(R.string.text_my_request_hotel_request_no_result);
                mFilterType = AppConstant.BOOKING_FILTER_TYPE.Hotel;
                break;
            case MyRequestsHeaderView.PRE_REQUEST_TYPE_EVENT:
                mTvRequestTitle.setText(R.string.text_my_request_event_request);
                //    mTvNoResult.setText(R.string.text_my_request_event_request_no_result);
                mFilterType = AppConstant.BOOKING_FILTER_TYPE.Entertainment;
                break;
            case MyRequestsHeaderView.PRE_REQUEST_TYPE_GOLF:
                mTvRequestTitle.setText(R.string.text_my_request_golf_request);
                //    mTvNoResult.setText(R.string.text_my_request_golf_request_no_result);
                mFilterType = AppConstant.BOOKING_FILTER_TYPE.Golf;
                break;
            case MyRequestsHeaderView.PRE_REQUEST_TYPE_CAR:
                mTvRequestTitle.setText(R.string.text_my_request_car_request);
                //   mTvNoResult.setText(R.string.text_my_request_car_request_no_result);
                mFilterType = AppConstant.BOOKING_FILTER_TYPE.Car;
                break;
            case MyRequestsHeaderView.PRE_REQUEST_TYPE_SPA:
                mTvRequestTitle.setText(R.string.text_my_request_spa_request);
                //   mTvNoResult.setText(R.string.text_my_request_golf_request_no_result);
                mFilterType = AppConstant.BOOKING_FILTER_TYPE.Spa;
                break;
            case MyRequestsHeaderView.PRE_REQUEST_TYPE_OTHER:
                mTvRequestTitle.setText(R.string.text_my_request_other_request);
                //     mTvNoResult.setText(R.string.text_my_request_other_request_no_result);
                mFilterType = AppConstant.BOOKING_FILTER_TYPE.Other;
                break;
        }
        adapter.filter(mFilterType,mFilterGroupType);
        if (adapter.getItemCount() > 0) {
            mListviewRequest.smoothScrollToPosition(0);
            mListviewRequest.setVisibility(View.VISIBLE);
            mTvNoResult.setVisibility(View.GONE);
            if (!loading && (firstVisibleItem + visibleItemCount) >= totalItemCount && isNeedLoadMore) {
                loading = true;
                requestLoadMore();
            }
        } else {
            if(isNeedLoadMore){
                loading = true;
                requestLoadMore();
            }else {
                mListviewRequest.setVisibility(View.GONE);
                mTvNoResult.setVisibility(View.VISIBLE);
            }
        }

    }

    @Override
    public void onB2CResponse(B2CBaseResponse response) {

    }

    @Override
    public void onB2CResponseOnList(List<B2CBaseResponse> responseList) {
        hideDialogProgress();
       // resetLoadingMore();
        loading= false;
        isNeedLoadMore = responseList != null && responseList.size() >= B2CGetRecentRequestRequest.PER_PAGE;
        data.addAll(MyRequestObject.cast(responseList));
        // adapter.setUpdateData(data);
        adapter.filter(mFilterType,
                       mFilterGroupType);
        if (adapter.getItemCount() > 0) {
            mListviewRequest.setVisibility(View.VISIBLE);
            mTvNoResult.setVisibility(View.GONE);
            if (!loading && (firstVisibleItem + visibleItemCount) >= totalItemCount && isNeedLoadMore) {
                loading = true;
                requestLoadMore();
            }else{
                scrollHideLoadMore();
            }
        } else {
            if(isNeedLoadMore){
                loading = true;
                requestLoadMore();
            }else {
                mListviewRequest.setVisibility(View.GONE);
                mTvNoResult.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onB2CFailure(String errorMessage, String errorCode) {
        hideDialogProgress();
     //   resetLoadingMore();
        loading = false;
        isNeedLoadMore = false;
        if(mPage > 1){
            mPage --;
        }
        if (adapter.getItemCount() > 0) {
            mListviewRequest.setVisibility(View.VISIBLE);
            mTvNoResult.setVisibility(View.GONE);
            if (!loading && (firstVisibleItem + visibleItemCount) >= totalItemCount && isNeedLoadMore) {
                loading = true;
                requestLoadMore();
            }else{
                scrollHideLoadMore();
            }
        } else {
            if(isNeedLoadMore){
                loading = true;
                requestLoadMore();
            }else {
                mListviewRequest.setVisibility(View.GONE);
                mTvNoResult.setVisibility(View.VISIBLE);
            }
        }
    }
}
