package com.rosbank.android.russia.fragment;

import android.annotation.SuppressLint;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.activitys.HomeActivity;
import com.rosbank.android.russia.application.AppContext;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.model.UserItem;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.FontUtils;

import butterknife.BindView;

/*
 * Created by chau.nguyen on 10/05/2016.
 */
public class ThankYouFragment extends BaseFragment {

    @BindView(R.id.tvThankYouSubBold)
    TextView tvSubBold;

    @BindView(R.id.tvThankYouSubMain)
    TextView tvSubMain;

    @BindView(R.id.tvThankYouSign)
    TextView tvSign;

    @BindView(R.id.tvThankYouEnd)
    TextView tvEnd;

    String trackedProductCategory;
    String trackedProductName;

    public ThankYouFragment() {

    }

    public static final String LINE_BOLD = "line_bold";
    public static final String LINE_MAIN = "line_main";
    public static final String LINE_SIGN = "line_sign";
    public static final String LINE_END = "line_end";
    public static final String TYPE_CANCEL = "cancel";

    /*
    ThankYouFragment fr = new ThankYouFragment();
    Bundle bundle = new Bundle();
    bundle.putString(ThankYouFragment.LINE_BOLD," ");
    bundle.putString(ThankYouFragment.LINE_MAIN," ");
    bundle.putString(ThankYouFragment.LINE_SIGN," ");
    bundle.putString(ThankYouFragment.LINE_END," ");
    fr.setArguments(bundle);
    pushFragment(fr,true,true);
    */

    @Override
    protected int layoutId() {
        return R.layout.fragment_thank_you;
    }

    @SuppressLint("StringFormatMatches")
    @Override
    protected void initView() {
        if (!TextUtils.isEmpty(trackedProductCategory)) {
            AppContext.getSharedInstance().track(trackedProductCategory, trackedProductName);
        }
        CommonUtils.setFontForViewRecursive(tvSubBold,
                FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
        CommonUtils.setFontForViewRecursive(tvSubMain,
                FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
        CommonUtils.setFontForViewRecursive(tvSign,
                FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
        CommonUtils.setFontForViewRecursive(tvEnd,
                FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);

        if (getActivity() != null) {
            if (UserItem.isVip()) {
                tvSubMain.setText(getActivity().getString(R.string.thank_you_sub_main_line2, getActivity().getString(R.string.hermitage_phone_number)));
                tvEnd.setText(getActivity().getResources().getString(R.string.thank_you_sub_end_line4_hermitage));
            } else {
                tvSubMain.setText(getActivity().getString(R.string.thank_you_sub_main_line2, getActivity().getString(R.string.rosbank_phone_number)));
                tvEnd.setText(getActivity().getResources().getString(R.string.thank_you_sub_end_line4_rosbank));
            }
        }

        if (getArguments() == null)
            return;

        String bold = getArguments().getString(LINE_BOLD, "");
        String main = getArguments().getString(LINE_MAIN, "");
        String sign = getArguments().getString(LINE_SIGN, "");
        //String end = getArguments().getString(LINE_END,"");


        if (!getArguments().getString(TYPE_CANCEL, "").isEmpty()) {
            tvSubBold.setVisibility(View.GONE);
        } else {
            if (!bold.isEmpty())
                tvSubBold.setText(bold);
        }

        if (!main.isEmpty())
            tvSubMain.setText(main);

        if (!sign.isEmpty())
            tvSign.setText(sign);

       /* if(!end.isEmpty())
            tvEnd.setText(end);*/

    }

    @Override
    protected void bindData() {

    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showLogoApp(false);
            ((HomeActivity) getActivity()).setTitle(getString(R.string.thank_you_title));
        }
    }

    @Override
    public boolean onBack() {
        return false;
    }

    public void setTrackedProductCategory(String trackedProductCategory) {
        this.trackedProductCategory = trackedProductCategory;
    }

    public void setTrackedProductName(String trackedProductName) {
        this.trackedProductName = trackedProductName;
    }
}
