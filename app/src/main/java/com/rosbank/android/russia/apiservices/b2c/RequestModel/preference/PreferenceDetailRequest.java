package com.rosbank.android.russia.apiservices.b2c.RequestModel.preference;

import com.google.gson.annotations.Expose;

/**
 * Created by ThuNguyen on 12/12/2016.
 */

public abstract class PreferenceDetailRequest {
    @Expose
    private String Delete;
    @Expose
    private String Type;
    @Expose
    private String MyPreferencesId;

    public PreferenceDetailRequest(String Delete){
        this.Delete = Delete;
    }
    public PreferenceDetailRequest(String Delete, String Type){
        this.Delete = Delete;
        this.Type = Type;
    }
    public void setMyPreferencesId(String preferencesId){
        MyPreferencesId = preferencesId;
    }
    public abstract void fillData(Object... values);
}
