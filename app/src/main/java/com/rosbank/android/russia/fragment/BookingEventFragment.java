package com.rosbank.android.russia.fragment;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.activitys.HomeActivity;
import com.rosbank.android.russia.adapter.TabBookingEventAdapter;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.model.concierge.MyRequestObject;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.FontUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by anh.trinh on 9/13/2016.
 */
public class BookingEventFragment
        extends BaseFragment {
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.view_pager)
    ViewPager mViewPager;
    private TabBookingEventAdapter mAdapter;


    @Override
    protected int layoutId() {
        return R.layout.fragment_booking_event;
    }

    @Override
    protected void initView() {
        tabLayout.addTab(tabLayout.newTab()
                                  .setText(getString(R.string.text_concierge_booking_hotel_tab1)));
        tabLayout.addTab(tabLayout.newTab()
                                  .setText(getString(R.string.text_concierge_booking_hotel_tab2)));
        CommonUtils.setFontForViewRecursive(tabLayout,
                FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        mAdapter = new TabBookingEventAdapter(getContext(),
                                              getChildFragmentManager(),
                                              getArguments());

        mViewPager.setAdapter(mAdapter);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab LayoutTab) {
                mViewPager.setCurrentItem(LayoutTab.getPosition());
                mAdapter.makeDefaultDateTime(LayoutTab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab LayoutTab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab LayoutTab) {

            }
        });
        Bundle bundle = getArguments();
        if (bundle != null) {
            MyRequestObject
                    myRequestObject = bundle.getParcelable(AppConstant.PRE_REQUEST_OBJECT_DATA);
            tabLayout.setVisibility(View.GONE);
            mViewPager.beginFakeDrag();
            /*if (myRequestObject != null && myRequestObject.getBookingRequestType()
                                                          .equals(MyRequestObject.HOTEL_REQUEST)) {


                mViewPager.setCurrentItem(0);

            } else {
                if (myRequestObject != null && myRequestObject.getBookingRequestType()
                                                              .equals(MyRequestObject.HOTEL_RECOMMEND_REQUEST)) {
                    mViewPager.setCurrentItem(1);
                }
            }*/
        }


    }

    @Override
    protected void bindData() {


    }

    @Override
    public void onResume() {

        super.onResume();
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showLogoApp(false);
            ((HomeActivity) getActivity()).setTitle(getString(R.string.text_event_title));
        }
    }

    @Override
    public boolean onBack() {
        return false;
    }


    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater,
                                           container,
                                           savedInstanceState);
        ButterKnife.bind(this,
                         rootView);
        return rootView;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode,
                                         permissions,
                                         grantResults);
        int currentItemPos = mViewPager.getCurrentItem();
        mAdapter.getItem(currentItemPos)
                .onRequestPermissionsResult(requestCode,
                                            permissions,
                                            grantResults);
    }

    @Override
    public void onActivityResult(int requestCode,
                                 int resultCode,
                                 Intent data) {
        super.onActivityResult(requestCode,
                               resultCode,
                               data);
        int currentItemPos = mViewPager.getCurrentItem();
        mAdapter.getItem(currentItemPos)
                .onActivityResult(requestCode,
                                  resultCode,
                                  data);
    }
}
