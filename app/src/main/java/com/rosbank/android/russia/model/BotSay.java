package com.rosbank.android.russia.model;

import com.rosbank.android.russia.utils.StringUtil;
import com.google.gson.annotations.Expose;

import java.util.List;

/**
 * Created by nga.nguyent on 10/17/2016.
 */

public class BotSay {
    @Expose
    String type;
    @Expose
    String category;
    @Expose
    String text;
    @Expose
    List<ChatButton>buttons;
    @Expose
    BotDataArray data;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<ChatButton> getButtons() {
        return buttons;
    }

    public void setButtons(List<ChatButton> buttons) {
        this.buttons = buttons;
    }

    public BotDataArray getData() {
        return data;
    }

    public void setData(BotDataArray data) {
        this.data = data;
    }

    public boolean isGoodKeyword(){
        /**
         * normal - ok
         * nonrelated - not contain keyword
         * */
        return  "normal".equalsIgnoreCase(type);
    }

    public boolean isOpenChatOk(){
        return "greeting".equalsIgnoreCase(type);
    }

    public boolean isRecommend(){
        return "recommend".equalsIgnoreCase(type);
    }

    public boolean isImagesData(){
        return "array".equalsIgnoreCase(type);
    }

    public boolean isEmptyImagesData(){
        if(data==null || data.getListData()==null || data.getListData().size()==0 )
            return true;
        return false;
    }

    public boolean isNoButtons(){
        if(buttons==null || buttons.size()==0)
            return true;
        return false;
    }

    /**
     * user send text
     * */
    private final String USER_KEY = "user_send_msg";
    public void setUserMessage(String text){
        type = USER_KEY;
        this.text = text;
    }

    public boolean isUserData(){
        return USER_KEY.equalsIgnoreCase(type);
    }

    public boolean isHelloMessage(){
        if(StringUtil.isEmpty(text))
            return false;
        if(text.contains("I am Alexis, your virtual dedicated concierge"))
            return true;
        return false;
    }

    public void removeEmptyParameter(){
        text = text.replace("{{username}}", "");
    }

    public boolean isFaceWithConsultantMessage(){
        if(StringUtil.isEmpty(text))
            return false;
        if(text.contains("Would you like to speak to our concierge consultant?"))
            return true;
        return false;
    }

}
