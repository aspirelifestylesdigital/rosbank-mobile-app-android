package com.rosbank.android.russia.apiservices;

import com.rosbank.android.russia.apiservices.RequestModel.ChangePasswordRequest;
import com.rosbank.android.russia.apiservices.ResponseModel.ChangePasswordResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.ApiProviderClient;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Callback;


public class WSChangePassword
        extends ApiProviderClient<ChangePasswordResponse> {

    ChangePasswordRequest request;

    public WSChangePassword(){

    }

    public ChangePasswordRequest getRequest() {
        return request;
    }

    public void setRequest(ChangePasswordRequest request) {
        this.request = request;
    }

    @Override
    public void run(Callback<ChangePasswordResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }

    @Override
    protected boolean isUseAuthenticateInApi() {
        return true;
    }

    @Override
    public void runApi() {
        if(request==null)
            return;

        serviceInterface.changePassWord(request).enqueue(callback);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("User-Agent", "Retrofit-Sample-App");
        return headers;
    }

}
