package com.rosbank.android.russia.fragment;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.adapter.MyRequestAdapter;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CICallback;
import com.rosbank.android.russia.apiservices.b2c.B2CWSGetRecentRequest;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.B2CGetRecentRequestRequest;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.model.concierge.MyRequestObject;
import com.rosbank.android.russia.model.concierge.OtherBookingDetailData;
import com.rosbank.android.russia.model.concierge.SpaBookingDetailData;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.DateTimeUtil;
import com.rosbank.android.russia.utils.FontUtils;
import com.rosbank.android.russia.utils.SelfPermissionUtils;
import com.rosbank.android.russia.widgets.MyRequestsHeaderView;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import butterknife.BindView;

/**
 * Created by anh.trinh on 9/13/2016.
 */
public class MyRequestUpComingFragment
        extends BaseFragment
        implements MyRequestsHeaderView.MyRequestHeaderViewListener, B2CICallback {
    @BindView(R.id.header)
    MyRequestsHeaderView mHeader;
    @BindView(R.id.tv_request_title)
    TextView mTvRequestTitle;
    @BindView(R.id.listview_request)
    RecyclerView mListviewRequest;
    @BindView(R.id.tv_no_result)
    TextView mTvNoResult;
    String mUserID = "";
    Integer mPage = 1;
    Integer mPerPage = 20;
    String mSortBy = "upcoming";
    Integer mTotal = -1;
    Long mEventId;
    boolean isNeedLoadMore = true;
    private int firstVisibleItem, visibleItemCount, totalItemCount;
    private boolean loading = false;
    String mFilterGroupType = MyRequestsHeaderView.PRE_REQUEST_TYPE_ALL_UPCOMING;
    AppConstant.BOOKING_FILTER_TYPE mFilterType = AppConstant.BOOKING_FILTER_TYPE.All;

    private List<MyRequestObject> data = new ArrayList<>();
    MyRequestAdapter adapter;
    MyRequestObject myRequestObject= null;

    @Override
    protected int layoutId() {
        return R.layout.fragment_my_request_upcoming;
    }

    @Override
    protected void initView() {
        CommonUtils.setFontForTextView(mTvRequestTitle,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        CommonUtils.setFontForTextView(mTvNoResult,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
        mHeader.setRequestHeaderListener(this);
        mTvRequestTitle.setText(R.string.text_my_request_view_all_request);
        adapter = new MyRequestAdapter(data,
                                       new MyRequestAdapter.MyRequestListener() {
                                           @Override
                                           public void onAmendClick(final MyRequestObject object) {
                                               if(object instanceof OtherBookingDetailData || object instanceof SpaBookingDetailData) {
                                               amendRequest(object);
                                               }else{
                                                   if(DateTimeUtil.shareInstance().checkLaterThan24Hours(object.getRequestStartDateEpoch())){
                                                       amendRequest(object);
                                                   }else{
                                                       showDialogMessage("", getString(R.string.text_amend_request_less_than_24_hours_error));
                                                   }
                                               }
                                           }

                                           @Override
                                           public void onCancelClick(final MyRequestObject object) {
                                               if(object instanceof OtherBookingDetailData|| object instanceof SpaBookingDetailData) {
                                                   CancelRequestFragment.openCancelRequest(MyRequestUpComingFragment.this, object);
                                               }else{
                                                   if(DateTimeUtil.shareInstance().checkLaterThan24Hours(object.getRequestStartDateEpoch())){
                                               CancelRequestFragment.openCancelRequest(MyRequestUpComingFragment.this, object);
                                                   }else{
                                                       showDialogMessage("", getString(R.string.text_cancel_request_less_than_24_hours_error));
                                                   }
                                               }

                                           }

                                           @Override
                                           public void onCalendarClick(final MyRequestObject object) {
                                               if (SelfPermissionUtils.getInstance()
                                                                      .checkReadWriteCalendarPermission(getActivity(),
                                                                                                        AppConstant.PERMISSION_REQUEST_CALENDAR)) {

                                                   addCalendar(object);


                                               }else{
                                                   myRequestObject = object;
                                               }
                                           }
                                       },
                                       getContext());
        LinearLayoutManager layoutManager = new LinearLayoutManager(view.getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mListviewRequest.setLayoutManager(layoutManager);
        mListviewRequest.setAdapter(adapter);
        mListviewRequest.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView,
                                             int newState) {
                super.onScrollStateChanged(recyclerView,
                                           newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView,
                                   int dx,
                                   int dy) {
                Log.d("ThuNguyen", "RecyclerView onScrolled()");
                //if (dy > 0) {
                    totalItemCount = layoutManager.getItemCount();
                    visibleItemCount = layoutManager.getChildCount();
                    firstVisibleItem = layoutManager.findFirstVisibleItemPosition();
                    Log.d("ThuNguyen", "RecyclerView totalItemCount = " + totalItemCount + ",visibleItemCount=" + visibleItemCount + ",firstVisibleItem" + firstVisibleItem);
                    if (!loading && (firstVisibleItem + visibleItemCount) >= totalItemCount && isNeedLoadMore) {
                        Log.d("ThuNguyen", "RecyclerView loadmore onScrolled()");
                        loading = true;
                        requestLoadMore();
                    }
                //}
            }
        });
        /*mListviewRequest.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Log.d("ThuNguyen", "RecyclerView addOnGlobalLayoutListener()");
                totalItemCount = layoutManager.getItemCount();
                visibleItemCount = layoutManager.getChildCount();
                firstVisibleItem = layoutManager.findFirstVisibleItemPosition();
                //Log.d("ThuNguyen", "RecyclerView totalItemCount = " + totalItemCount + ",visibleItemCount=" + visibleItemCount + ",firstVisibleItem" + firstVisibleItem);
                if (!loading && (firstVisibleItem + visibleItemCount) >= totalItemCount && isNeedLoadMore) {
                    Log.d("ThuNguyen", "RecyclerView loadmore onGlobalLayout()");
                    loading = true;
                    requestLoadMore();
                }
            }
        });*/
        // Load recent list
        resetAndLoadRecentList();
    }

    @Override
    public void onResume() {
        super.onResume();
        /*mUserID = SharedPreferencesUtils.getPreferences(AppConstant.USER_ID_PRE,
                                                        "");
        if (!mUserID.equals("")) {
            data.clear();
            mPage = 1;
            showDialogProgress();
            GetListMyRequestRequest getListMyRequestRequest = new GetListMyRequestRequest();
            getListMyRequestRequest.setUserID(mUserID);
            getListMyRequestRequest.setPage(mPage);
            getListMyRequestRequest.setRecordPerPage(mPerPage);
            getListMyRequestRequest.setSortBy(mSortBy);
            WSGetMyRequests wsGetMyRequests = new WSGetMyRequests();
            wsGetMyRequests.getMyRequests(getListMyRequestRequest);
            wsGetMyRequests.run(this);
        }*/

    }
    @Subscribe
    public void updateRecentList(AppConstant.CONCIERGE_EDIT_TYPE requestType){
        data.clear();
        mPage = 1;
        B2CWSGetRecentRequest b2CWSGetRecentRequest = new B2CWSGetRecentRequest(this);
        b2CWSGetRecentRequest.setPage(mPage);
        b2CWSGetRecentRequest.run(null);
    }
    private void resetAndLoadRecentList(){
        data.clear();
        mPage = 1;
        getRecentRequest();
    }
    private void getRecentRequest(){
        if(mPage == 1) {
            showDialogProgress();
        }
        B2CWSGetRecentRequest b2CWSGetRecentRequest = new B2CWSGetRecentRequest(this);
        b2CWSGetRecentRequest.setPage(mPage);
        b2CWSGetRecentRequest.run(null);
    }

    private void addCalendar(MyRequestObject object) {
        String title = CommonUtils.isStringValid(object.getItemTitle())?object.getItemTitle():"";
        String description= CommonUtils.isStringValid(object.getItemDescription())?object.getItemDescription():"";
        long dateStart = object.getRequestStartDateEpoch();
        long dateEnd= object.getRequestEndDateEpoch();
        if(dateEnd == 0 && dateStart > 0){
            dateEnd = dateStart + 60*60*1000; // Extra 1 hour in miliseconds
        }
//        if (CommonUtils.isStringValid(object.getEpochCheckOutDate())){
//            dateEnd = String.valueOf(Long.parseLong(object.getEpochCheckOutDate()));
//        }else{
//            if(CommonUtils.isStringValid(dateStart)) {
//                dateEnd = String.valueOf(Long.parseLong(dateStart) + 60 * 60);
//            }
//        }
        try {
            ContentResolver cr = getActivity()
                                         .getContentResolver();
            ContentValues values = new ContentValues();
            if(dateStart > 0) {
                values.put(CalendarContract.Events.DTSTART,
                        dateStart);
            }
            if(dateEnd > 0) {
                values.put(CalendarContract.Events.DTEND,
                        dateEnd);
            }
            values.put(CalendarContract.Events.TITLE, title);
            values.put(CalendarContract.Events.DESCRIPTION,description);
            values.put(CalendarContract.Events.EVENT_TIMEZONE, TimeZone.getDefault().getID());
            values.put(CalendarContract.Events.CALENDAR_ID, 1);
            values.put(CalendarContract.Events.HAS_ALARM, 1);
            String eventUriString = "content://com.android.calendar/events";

            Uri uri = cr.insert(Uri.parse(eventUriString),
                                values);

            long eventID = Long.parseLong(uri.getLastPathSegment());

            values = new ContentValues();
            values.put(CalendarContract.Reminders.MINUTES, 30);
            values.put(CalendarContract.Reminders.EVENT_ID, eventID);
            values.put(CalendarContract.Reminders.METHOD, CalendarContract.Reminders.METHOD_ALERT);
            String reminderUriString = "content://com.android.calendar/reminders";

            uri = cr.insert(Uri.parse(reminderUriString),
                            values);

            CommonUtils.showActionAlertOnlyYES(getActivity(), null, getString(R.string.text_add_event_calendar_success), getString(R.string.text_add_event_calendar_ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();

                }
            });
        } catch (Exception e) {
            String msg = e.getMessage();
        }

    }
    private void requestLoadMore() {
            scrollShowLoadMore();
            mPage++;
            getRecentRequest();
    }

    private void scrollShowLoadMore() {
        mListviewRequest.post(new Runnable() {
            @Override
            public void run() {
                adapter.addViewLoading();
            }
        });
    }

    /**
     * use post Runnable when hide View load more
     */
    private void scrollHideLoadMore() {
        mListviewRequest.post(new Runnable() {
            @Override
            public void run() {
                adapter.removeViewLoading();
            }
        });
    }

    private void resetLoadingMore() {
        loading = false;
        scrollHideLoadMore();
    }

    @Override
    protected void bindData() {

    }

    @Override
    public boolean onBack() {
        return false;
    }

    @Override
    public void onRadioCheckChange(final String requestType) {
        switch (requestType) {
            case MyRequestsHeaderView.PRE_REQUEST_TYPE_ALL:
                mTvRequestTitle.setText(R.string.text_my_request_view_all_request);
                //    mTvNoResult.setText(R.string.text_my_request_view_all_request_no_result);
                mFilterType = AppConstant.BOOKING_FILTER_TYPE.All;
                break;
            case MyRequestsHeaderView.PRE_REQUEST_TYPE_GOURMET:
                mTvRequestTitle.setText(R.string.text_my_request_gourmet_request);
                //     mTvNoResult.setText(R.string.text_my_request_gourmet_request_no_result);
                mFilterType = AppConstant.BOOKING_FILTER_TYPE.Restaurant;
                break;
            case MyRequestsHeaderView.PRE_REQUEST_TYPE_HOTEL:
                mTvRequestTitle.setText(R.string.text_my_request_hotel_request);
                //    mTvNoResult.setText(R.string.text_my_request_hotel_request_no_result);
                mFilterType = AppConstant.BOOKING_FILTER_TYPE.Hotel;
                break;
            case MyRequestsHeaderView.PRE_REQUEST_TYPE_EVENT:
                mTvRequestTitle.setText(R.string.text_my_request_event_request);
                //     mTvNoResult.setText(R.string.text_my_request_event_request_no_result);
                mFilterType = AppConstant.BOOKING_FILTER_TYPE.Entertainment;
                break;
            case MyRequestsHeaderView.PRE_REQUEST_TYPE_GOLF:
                mTvRequestTitle.setText(R.string.text_my_request_golf_request);
                //   mTvNoResult.setText(R.string.text_my_request_golf_request_no_result);
                mFilterType = AppConstant.BOOKING_FILTER_TYPE.Golf;
                break;
            case MyRequestsHeaderView.PRE_REQUEST_TYPE_CAR:
                mTvRequestTitle.setText(R.string.text_my_request_car_request);
                //    mTvNoResult.setText(R.string.text_my_request_car_request_no_result);
                mFilterType = AppConstant.BOOKING_FILTER_TYPE.Car;
                break;
            case MyRequestsHeaderView.PRE_REQUEST_TYPE_SPA:
                mTvRequestTitle.setText(R.string.text_my_request_spa_request);
                //   mTvNoResult.setText(R.string.text_my_request_golf_request_no_result);
                mFilterType = AppConstant.BOOKING_FILTER_TYPE.Spa;
                break;
            case MyRequestsHeaderView.PRE_REQUEST_TYPE_OTHER:
                mTvRequestTitle.setText(R.string.text_my_request_other_request);
                //    mTvNoResult.setText(R.string.text_my_request_other_request_no_result);
                mFilterType = AppConstant.BOOKING_FILTER_TYPE.Other;
                break;
        }
        adapter.filter(mFilterType,
                       mFilterGroupType);
        if (adapter.getItemCount() > 0) {
            mListviewRequest.smoothScrollToPosition(0);
            mListviewRequest.setVisibility(View.VISIBLE);
            mTvNoResult.setVisibility(View.GONE);
            if (!loading && (firstVisibleItem + visibleItemCount) >= totalItemCount && isNeedLoadMore) {
                loading = true;
                requestLoadMore();
            }
        } else {
            if(isNeedLoadMore){
                loading = true;
                requestLoadMore();
            }else {
                mListviewRequest.setVisibility(View.GONE);
                mTvNoResult.setVisibility(View.VISIBLE);
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode){
            case AppConstant.PERMISSION_REQUEST_CALENDAR:
                if (grantResults.length > 0
                            && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(myRequestObject!=null){
                        addCalendar(myRequestObject);
                    }

                }
                break;
        }
    }

    private void amendRequest(MyRequestObject object){
        BaseFragment fragment = null;
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstant.PRE_REQUEST_OBJECT_DATA,object);

        switch (object.getBookingRequestEnum()) {
            case BOOK_HOTEL:
            case RECOMMEND_HOTEL:
                if(object.isBookNormalType()) {
                    fragment = new BookHotelFragment();
                }else {
                    fragment = new BookHotelRecomendFragment();
                }
                break;
            case RESERVE_TABLE:
            case RECOMMEND_RESTAURANT:
                if(object.isBookNormalType()) {
                    fragment = new ConciergeGourmetTab1Fragment();
                }else {
                    fragment = new ConciergeGourmetTab2Fragment();
                }
                break;
            case T_CAR_RENTAL:
            case T_CAR_TRANSFER:
                if(object.isBookNormalType()) {
                    fragment = new BookingRentalFragment();
                }else {
                    fragment = new BookingTransferFragment();
                }
                break;
            case GOLF:
                fragment = new BookingGolfFragment();
                break;
            case EVENT:
            case RECOMMEND_EVENT:
                if(object.isBookNormalType()){
                    fragment = new BookEventFragment();
                }else{
                    fragment = new BookEventRecommendationFragment();
                }
                break;
            case OTHER_REQUEST:
            case SPA:
                fragment = new BookingOtherFragment();
                break;
        }
        if(fragment != null){
            fragment.setArguments(bundle);
            pushFragment(fragment,
                    true,
                    true);
        }
    }


    @Override
    public void onB2CResponse(B2CBaseResponse response) {
        if(getActivity()==null) {
            return;
        }

    }

    @Override
    public void onB2CResponseOnList(List<B2CBaseResponse> responseList) {
        hideDialogProgress();
        //resetLoadingMore();
        loading = false;
        isNeedLoadMore = responseList != null && responseList.size() >= B2CGetRecentRequestRequest.PER_PAGE;
        data.addAll(MyRequestObject.cast(responseList));
        // adapter.setUpdateData(data);
        adapter.filter(mFilterType,
                       mFilterGroupType);
        if (adapter.getItemCount() > 0) {
            mListviewRequest.setVisibility(View.VISIBLE);
            mTvNoResult.setVisibility(View.GONE);
            if (!loading && (firstVisibleItem + visibleItemCount) >= totalItemCount && isNeedLoadMore) {
                loading = true;
                requestLoadMore();
            }else {
                scrollHideLoadMore();
            }
        } else {
            if(isNeedLoadMore){
                loading = true;
                requestLoadMore();
            }else {
                mListviewRequest.setVisibility(View.GONE);
                mTvNoResult.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onB2CFailure(String errorMessage, String errorCode) {
        hideDialogProgress();
       // resetLoadingMore();
        loading = false;
        isNeedLoadMore = false;
        if(mPage > 1){
            mPage --;
        }
        if (adapter.getItemCount() > 0) {
            mListviewRequest.setVisibility(View.VISIBLE);
            mTvNoResult.setVisibility(View.GONE);
            if (!loading && (firstVisibleItem + visibleItemCount) >= totalItemCount && isNeedLoadMore) {
                loading = true;
                requestLoadMore();
            }else {
                scrollHideLoadMore();
            }
        } else {
            if(isNeedLoadMore){
                loading = true;
                requestLoadMore();
            }else {
                mListviewRequest.setVisibility(View.GONE);
                mTvNoResult.setVisibility(View.VISIBLE);
            }
        }
    }

}
