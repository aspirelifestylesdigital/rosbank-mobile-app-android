package com.rosbank.android.russia.apiservices;

import com.rosbank.android.russia.apiservices.RequestModel.CancelRequestRequest;
import com.rosbank.android.russia.apiservices.ResponseModel.CancelRequestResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.ApiProviderClient;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Callback;


public class WSCancelRequest
        extends ApiProviderClient<CancelRequestResponse> {

    private CancelRequestRequest cancelRequestRequest;

    public void setCancelRequest(final CancelRequestRequest request){
        this.cancelRequestRequest = request;
    }

    @Override
    public void run(Callback<CancelRequestResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }

    @Override
    protected boolean isUseAuthenticateInApi() {
        return true;
    }

    @Override
    public void runApi() {
        serviceInterface.cancelRequest(cancelRequestRequest).enqueue(callback);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("User-Agent", "Retrofit-Sample-App");
        return headers;
    }

}
