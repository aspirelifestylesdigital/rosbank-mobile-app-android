package com.rosbank.android.russia.adapter;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.interfaces.OnRecyclerViewItemClickListener;
import com.rosbank.android.russia.model.KeyValueObject;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.FontUtils;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by nga.nguyent on 2/26/2016.
 */
public class SelectDataAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public enum CHOICEMODE{
        SINGLE,
        MULTI
    }

    private List<KeyValueObject> data;
    private HashMap<String, KeyValueObject> selectedItems = new HashMap<>();
    private OnRecyclerViewItemClickListener itemClickListener;
    private CHOICEMODE choiceMode = CHOICEMODE.MULTI;

    public void setChoiceMode(CHOICEMODE choiceMode) {
        this.choiceMode = choiceMode;
    }

    public CHOICEMODE getChoiceMode() {
        return choiceMode;
    }

    public void addData(List<KeyValueObject> data) {
        if(this.data==null){
            this.data = new ArrayList<>();
        }
        this.data.addAll(data);
    }

    public void clearData(){
        if(data!=null){
            data.clear();
        }
    }

    public void setItemClickListener(OnRecyclerViewItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(parent.getContext(), R.layout.layout_item_cuisinetype, null);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        KeyValueObject item = data.get(position);
        //
        ((ViewHolder)holder).setData(position, item);
    }

    @Override
    public int getItemCount() {
        if(data==null)
            return 0;
        return data.size();
    }

    public List<KeyValueObject> getCuisinesSelected(){
        List<KeyValueObject>selected = new ArrayList<>();

        Set<Map.Entry<String, KeyValueObject>> sets = selectedItems.entrySet();
        Iterator<Map.Entry<String, KeyValueObject>> iter = sets.iterator();
        while(iter.hasNext()){
            Map.Entry<String, KeyValueObject> ele = iter.next();

            selected.add(ele.getValue());
        }

        return selected;
    }

    public void setCuisineSelected(List<KeyValueObject>list){
        if(list==null)
            return;

        for (int i = 0; i < list.size(); i++) {
            KeyValueObject cuis = list.get(i);
            selectedItems.put(cuis.getKey(), cuis);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.tvName)
        TextView tvName;
        @BindView(R.id.imgCheckbox)
        ImageView imgCheck;
        int pos;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            imgCheck.setImageResource(R.drawable.ic_tick);
            imgCheck.setVisibility(View.INVISIBLE);
        }

        public void setData(int position, KeyValueObject item){
            this.pos = position;
            CommonUtils.setFontForViewRecursive(tvName,
                    FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
            tvName.setText(item.getValue());

            if(selectedItems.containsKey(item.getKey())){
                //remove
                imgCheck.setVisibility(View.VISIBLE);
            }
            else{
                imgCheck.setVisibility(View.INVISIBLE);
            }

        }

        @OnClick(R.id.llItemMain)
        void OnItemClick(View view){
            if(choiceMode==CHOICEMODE.SINGLE){
                selectedItems.clear();
            }
            //
            checkTick();
            //
            if(itemClickListener!=null){
                itemClickListener.onItemClick(pos, view);
            }
        }

        private void checkTick(){
            KeyValueObject cuis = data.get(pos);
            if(selectedItems.containsKey(cuis.getKey())){
                //remove
                selectedItems.remove(cuis.getKey());
                imgCheck.setVisibility(View.INVISIBLE);
            }
            else{
                //checked
                selectedItems.put(cuis.getKey(), cuis);
                imgCheck.setVisibility(View.VISIBLE);
            }
        }

    }

}
