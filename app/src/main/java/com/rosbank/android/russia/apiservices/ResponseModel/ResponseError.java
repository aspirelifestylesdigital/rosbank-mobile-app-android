package com.rosbank.android.russia.apiservices.ResponseModel;

//import retrofit.RetrofitError;

/**
 * Created by anh.tran on 5/20/2015.
 */
public class ResponseError {
//    public RetrofitError.Kind errorKind;
    public String errorMessage;
    public String status = "";

    public String handlerName;
    public int task;
    public long time;
    public ResponseError(String msg, String handler, int _task){
//        errorKind = error;
        errorMessage = msg;
        handlerName = handler;
        task = _task;
    }

//    public ResponseError(String _status, String msg, String handler, int _task){
//        status = _status;
//        errorMessage = msg;
//        handlerName = handler;
//        task = _task;
//    }
    public ResponseError(String _status, String msg, String handler, int _task, long _time){
        status = _status;
        errorMessage = msg;
        handlerName = handler;
        task = _task;
        time = _time;
    }
    public String getErrorMessage(){return  errorMessage;}
    public String getStatus(){return  status;}
}
