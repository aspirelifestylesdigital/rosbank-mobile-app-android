package com.rosbank.android.russia.model.concierge;
import android.text.TextUtils;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CGetRecentRequestResponse;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.application.AppContext;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.DateTimeUtil;

/**
 * Created by ThuNguyen on 10/26/2016.
 */

public class HotelBookingDetailData extends MyRequestObject {

    private String CheckInDate;
    private String CheckOutDate;
    private String HotelName;
    private String RoomType;
    private String LoyaltyProgrammeMembership;
    private String MemberNo;
    private Boolean SaveMembershipDetails;
    private String MinPrice;
    private String MaxPrice;
    private int NumberOfAdults;
    private int NumberOfKids;
    private int NumberOfChildren;
    private Boolean SmokingPreference;

    public Boolean getWithBreakfast() {
        return WithBreakfast;
    }

    public void setWithBreakfast(final Boolean withBreakfast) {
        WithBreakfast = withBreakfast;
    }

    public Boolean getWithWifi() {
        return WithWifi;
    }

    public void setWithWifi(final Boolean withWifi) {
        WithWifi = withWifi;
    }

    private Boolean WithBreakfast;
    private Boolean WithWifi;
    private String CreateDate;
    private long createDateEpoc;
    private long checkInDateEpoc;
    private long checkOutDateEpoc;
    private String AdditionalPreference;

    public int getNumbersOfRoom() {
        return NumbersOfRoom;
    }

    public void setNumbersOfRoom(final int numbersOfRoom) {
        NumbersOfRoom = numbersOfRoom;
    }

    private int NumbersOfRoom;
    public HotelBookingDetailData(B2CGetRecentRequestResponse recentRequestResponse) {
        super(recentRequestResponse);

        CheckInDate = recentRequestResponse.getCHECKIN();
        if(CheckInDate.contains(" ")){
            CheckInDate = CheckInDate.substring(0,CheckInDate.indexOf(" "))+" 23:59:59";
        }
        CheckOutDate = recentRequestResponse.getCHECKOUT();
        if(!TextUtils.isEmpty(recentRequestResponse.getNUMBEROFADULTS())) {
            try {
                NumberOfAdults = Integer.parseInt(recentRequestResponse.getNUMBEROFADULTS());
            }catch (Exception e){}

        }
        if(!TextUtils.isEmpty(recentRequestResponse.getNUMBEROFCHILDREN())) {
            try {
                NumberOfKids = Integer.parseInt(recentRequestResponse.getNUMBEROFCHILDREN());
                NumberOfChildren = Integer.parseInt(recentRequestResponse.getNUMBEROFCHILDREN());
            }catch (Exception e){}

        }
        // Booking history
        CreateDate = recentRequestResponse.getCREATEDDATE();
        createDateEpoc = DateTimeUtil.shareInstance().convertToTimeStampFromGMT(CreateDate, AppConstant.DATE_FORMAT_YYYY_MM_DD_T_HH_MM_SS);
        checkInDateEpoc = DateTimeUtil.shareInstance().convertToTimeStamp(CheckInDate, AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS);
        checkOutDateEpoc = DateTimeUtil.shareInstance().convertToTimeStamp(CheckOutDate, AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS);

        // Item title
        ItemTitle = conciergeRequestDetail.getHotelName();
        if(TextUtils.isEmpty(ItemTitle)){
          //  ItemTitle = "- Get hotel recommendation -";
            ItemTitle = AppContext.getSharedInstance().getResources().getString(R.string.text_get_hotel_recommendation);
        }
        // Item description
        // Check-in date
        ItemDescription =  AppContext.getSharedInstance().getResources().getString(R.string.text_check_in_date) + DateTimeUtil.shareInstance().convertTime(CheckInDate, AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS, AppConstant.DATE_FORMAT_CONCIERGE_BOOKING).toUpperCase();
        ItemDescription += "\n"+ AppContext.getSharedInstance().getResources().getString(R.string.text_check_out_date) + DateTimeUtil.shareInstance().convertTime(CheckOutDate, AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS, AppConstant.DATE_FORMAT_CONCIERGE_BOOKING).toUpperCase();
    }

    @Override
    protected void initRequestProperties(){
        // Initialize the mapping properties
        HotelName = conciergeRequestDetail.getHotelName();
        RoomType = conciergeRequestDetail.getRoomPreference();
        LoyaltyProgrammeMembership = conciergeRequestDetail.getMembership();
        MemberNo = conciergeRequestDetail.getHotelLoyaltyMembershipNumber();
        MinPrice = conciergeRequestDetail.getMinPrice();
        MaxPrice = conciergeRequestDetail.getBudgetRangepernightCurrency();
        if(!TextUtils.isEmpty(conciergeRequestDetail.getNumbersOfRoom())) {
            try {
                NumbersOfRoom = Integer.parseInt(conciergeRequestDetail.getNumbersOfRoom());
            }catch (Exception e){}

        }
        SmokingPreference = conciergeRequestDetail.getSmokingPref() != null && (conciergeRequestDetail.getSmokingPref().equalsIgnoreCase("Yes") || conciergeRequestDetail.getSmokingPref().equalsIgnoreCase("true"));
        WithBreakfast = conciergeRequestDetail.getWithBreakfast() != null && (conciergeRequestDetail.getWithBreakfast().equalsIgnoreCase("yes") || conciergeRequestDetail.getWithBreakfast().equalsIgnoreCase("true"));
        WithWifi = conciergeRequestDetail.getWithWifi() != null && (conciergeRequestDetail.getWithWifi().equalsIgnoreCase("yes") || conciergeRequestDetail.getWithWifi().equalsIgnoreCase("true"));

        AdditionalPreference = conciergeRequestDetail.getAdditionalPreference();
        // Booking type group
        BookingFilterGroupType = AppConstant.BOOKING_FILTER_TYPE.Hotel;
    }

    @Override
    public boolean isBookNormalType() {
        return !TextUtils.isEmpty(HotelName);
    }

    @Override
    public long getRequestStartDateEpoch() {
        return checkInDateEpoc;
    }

    @Override
    public long getRequestEndDateEpoch() {
        return checkOutDateEpoc;
    }

    @Override
    public String getReservationName() {
        if(CommonUtils.isStringValid(conciergeRequestDetail.getGuestName())){
            return conciergeRequestDetail.getGuestName();
        }else {
            return getFullName();
        }
    }
    public long getCreateDateEpoc() {
        return createDateEpoc;
    }
    public long getCheckInDateEpoc() {
        return checkInDateEpoc;
    }

    public long getCheckOutDateEpoc() {
        return checkOutDateEpoc;
    }

    public String getGuestDisplay(){
        String guests = "";
        if(NumberOfAdults >= 2){
            guests += NumberOfAdults +AppContext.getSharedInstance().getResources().getString(R.string.text_adults) ;
        }else{
            guests += NumberOfAdults + AppContext.getSharedInstance().getResources().getString(R.string.text_adult);
        }

        if(NumberOfChildren > 0){
            if(NumberOfChildren == 1){
                guests += AppContext.getSharedInstance().getResources().getString(R.string.text_one_child);
            }else{
                guests += " & " + NumberOfChildren + AppContext.getSharedInstance().getResources().getString(R.string.text_children);
            }
        }
        return guests;
    }

    public int getNumberOfChildren() {
        return NumberOfChildren;
    }

    public void setNumberOfChildren(int numberOfChildren) {
        NumberOfChildren = numberOfChildren;
    }

    public String getHotelName() {
        return HotelName;
    }

    public void setHotelName(String hotelName) {
        HotelName = hotelName;
    }

    public String getRoomType() {
        return RoomType;
    }

    public void setRoomType(String roomType) {
        RoomType = roomType;
    }

    public String getLoyaltyProgrammeMembership() {
        return LoyaltyProgrammeMembership;
    }

    public void setLoyaltyProgrammeMembership(String loyaltyProgrammeMembership) {
        LoyaltyProgrammeMembership = loyaltyProgrammeMembership;
    }

    public String getMemberNo() {
        return MemberNo;
    }

    public Boolean getSaveMembershipDetails() {
        return SaveMembershipDetails;
    }

    public void setSaveMembershipDetails(Boolean saveMembershipDetails) {
        SaveMembershipDetails = saveMembershipDetails;
    }

    public int getNumberOfAdults() {
        return NumberOfAdults;
    }

    public void setNumberOfAdults(int numberOfAdults) {
        NumberOfAdults = numberOfAdults;
    }

    public int getNumberOfKids() {
        return NumberOfKids;
    }

    public void setNumberOfKids(int numberOfKids) {
        NumberOfKids = numberOfKids;
    }

    public boolean getSmokingPreference() {
        return SmokingPreference;
    }

    public void setSmokingPreference(boolean smokingPreference) {
        SmokingPreference = smokingPreference;
    }

    public String getCheckInDate() {
        return CheckInDate;
    }

    public void setCheckInDate(String checkInDate) {
        CheckInDate = checkInDate;
    }

    public String getCheckOutDate() {
        return CheckOutDate;
    }

    public void setCheckOutDate(String checkOutDate) {
        CheckOutDate = checkOutDate;
    }

    public String getMinPrice() {
        return MinPrice;
    }

    public void setMinPrice(String minPrice) {
        MinPrice = minPrice;
    }

    public String getMaxPrice() {
        return MaxPrice;
    }

    public void setMaxPrice(String maxPrice) {
        MaxPrice = maxPrice;
    }

    public void setAdditionalPreference(String additionalPreference) {
        AdditionalPreference = additionalPreference;
    }

    public String getAdditionalPreference() {
        return AdditionalPreference;
    }
}
