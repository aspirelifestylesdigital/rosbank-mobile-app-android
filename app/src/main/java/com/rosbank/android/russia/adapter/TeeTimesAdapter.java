package com.rosbank.android.russia.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.FontUtils;

import java.util.List;

/**
 * Created by ThuNguyen on 9/20/2016.
 */
public class TeeTimesAdapter
        extends ArrayAdapter<String> {
    private final Context context;
    private final List<String> values;
    private final String selectedData;


    public TeeTimesAdapter(Context context, List<String> values, String selectedData) {
        super(context, -1, values);
        this.context = context;
        this.values = values;
        this.selectedData =selectedData;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                                                           .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.item_selection, parent, false);
        TextView textView = (TextView) rowView.findViewById(R.id.tv_selection_name);
        ImageView imgTick = (ImageView) rowView.findViewById(R.id.ic_selection_tick);
        CommonUtils.setFontForViewRecursive(textView,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
        textView.setText(values.get(position));
        if(selectedData!=null && selectedData.replace(" ", "").equalsIgnoreCase(values.get(position).replace(" ", ""))){
            imgTick.setVisibility(View.VISIBLE);
        }else{
            imgTick.setVisibility(View.GONE);
        }
        return rowView;
    }

    @Override
    public int getCount() {
        return values.size();
    }
}

