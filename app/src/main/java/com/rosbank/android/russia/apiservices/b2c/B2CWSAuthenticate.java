package com.rosbank.android.russia.apiservices.b2c;

import android.text.TextUtils;

import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CICallback;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CAuthenticateResponse;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.utils.SharedPreferencesUtils;

import java.util.List;


public class B2CWSAuthenticate{
    private static final String REFRESH_TOKEN_ERROR_CODE = "ENR73-2";
    B2CICallback callback;
    B2CWSRequestToken b2CWSRequestToken;
    B2CWSRefreshToken b2CWSRefreshToken;
    B2CWSAccessToken b2CWSAccessToken;
    public void run(B2CICallback callback) {
        this.callback = callback;

        String accessToken = SharedPreferencesUtils.getPreferences(AppConstant.PRE_B2C_ACCESS_TOKEN, "");
        if(TextUtils.isEmpty(accessToken)) {
            // Begin with request token first
            b2CWSRequestToken = new B2CWSRequestToken(requestTokenCallback);
            b2CWSRequestToken.run(null);
        }else{
            // Begin with refresh token only
            b2CWSRefreshToken = new B2CWSRefreshToken(refreshTokenCallback);
            b2CWSRefreshToken.run(null);
        }
    }

    private B2CICallback requestTokenCallback = new B2CICallback() {
        @Override
        public void onB2CResponse(B2CBaseResponse response) {
            if(response != null){
                if(response.isSuccess()) {
                    // Continue with access token
                    b2CWSAccessToken = new B2CWSAccessToken(accessTokenCallback);
                    b2CWSAccessToken.run(null);
                }else{
                    if(callback != null){
                        callback.onB2CResponse(new B2CAuthenticateResponse(response.getMessage(), false));
                    }
                }
            }else{
                if(callback != null){
                    callback.onB2CResponse(new B2CAuthenticateResponse(b2CWSRequestToken.getMessageErrorFromLog(), false));
                }
            }
        }

        @Override
        public void onB2CResponseOnList(List<B2CBaseResponse> responseList) {

        }

        @Override
        public void onB2CFailure(String errorMessage, String errorCode) {
            if(callback != null){
                callback.onB2CResponse(new B2CAuthenticateResponse(errorMessage, false));
            }
        }
    };
    private B2CICallback accessTokenCallback = new B2CICallback() {
        @Override
        public void onB2CResponse(B2CBaseResponse response) {
            if(callback != null) {
                if (response != null) {
                    callback.onB2CResponse(new B2CAuthenticateResponse(response.getMessage(), response.isSuccess()));
                } else {
                    callback.onB2CFailure(b2CWSAccessToken.getMessageErrorFromLog(), b2CWSAccessToken.getErrorCodeFromLog());
                }
            }
        }

        @Override
        public void onB2CResponseOnList(List<B2CBaseResponse> responseList) {

        }

        @Override
        public void onB2CFailure(String errorMessage, String errorCode) {
            if(callback != null){
                callback.onB2CFailure(errorMessage, errorCode);
            }
        }

    };
    private B2CICallback refreshTokenCallback = new B2CICallback() {
        @Override
        public void onB2CResponse(B2CBaseResponse response) {
            if(!TextUtils.isEmpty(response.getErrorCode())){
                // Clear all the token so that it needs to be requested token
                SharedPreferencesUtils.setPreferences(AppConstant.PRE_B2C_REQUEST_TOKEN, "");
                SharedPreferencesUtils.setPreferences(AppConstant.PRE_B2C_ACCESS_TOKEN, "");
                SharedPreferencesUtils.setPreferences(AppConstant.PRE_B2C_REFRESH_TOKEN, "");

                run(callback);
            }else{
                if(response.isSuccess()){
                    // Calculate the expiration time again
                    long expirationTimeDuration = SharedPreferencesUtils.getPreferences(AppConstant.PRE_B2C_EXPIRATION_TIME_DURATION, 0L);
                    SharedPreferencesUtils.setPreferences(AppConstant.PRE_B2C_EXPIRED_AT, System.currentTimeMillis() + expirationTimeDuration);
                }
                if(callback != null){
                    callback.onB2CResponse(new B2CAuthenticateResponse(response.getMessage(), response.isSuccess()));
                }
            }
        }

        @Override
        public void onB2CResponseOnList(List<B2CBaseResponse> responseList) {

        }

        @Override
        public void onB2CFailure(String errorMessage, String errorCode) {
            if(!TextUtils.isEmpty(errorCode)){
                // Clear all the token so that it needs to be requested token
                SharedPreferencesUtils.setPreferences(AppConstant.PRE_B2C_REQUEST_TOKEN, "");
                SharedPreferencesUtils.setPreferences(AppConstant.PRE_B2C_ACCESS_TOKEN, "");
                SharedPreferencesUtils.setPreferences(AppConstant.PRE_B2C_REFRESH_TOKEN, "");

                run(callback);
            }else {
                if (callback != null) {
                    callback.onB2CFailure(errorMessage, errorCode);
                }
            }
        }
    };
}
