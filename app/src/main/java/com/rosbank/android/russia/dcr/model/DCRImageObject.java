package com.rosbank.android.russia.dcr.model;


import com.google.gson.annotations.Expose;

import java.io.Serializable;


public class DCRImageObject
        implements Serializable {
    public DCRImageObject(boolean isdefault){
        this.isdefault = isdefault;
    }
    @Expose
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }

    public boolean getImageIsDefault() {
        return isdefault;
    }

    public void setIsDefault(final boolean isdefault) {
        this.isdefault = isdefault;
    }

    @Expose
    private boolean isdefault;

    public String getLocalPath() {
        return localPath;
    }

    public void setLocalPath(final String localPath) {
        this.localPath = localPath;
    }

    private String localPath;

    @Override
    public boolean equals(Object obj) {
        if(obj != null && obj instanceof DCRImageObject){
            DCRImageObject commonObject = (DCRImageObject)obj;
            return (url != null && url.equals(commonObject.getUrl()));
        }
        return false;
    }
    public DCRImageObject() {
        this.url = "";
        this.isdefault = false;
        this.localPath = "";
    }


}
