package com.rosbank.android.russia.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;


public class LoyaltyObject
        implements Parcelable {
    @Expose
    private String Name;

    public String getName() {
        return Name;
    }

    public void setName(final String name) {
        Name = name;
    }

    public String getNumber() {
        return Number;
    }

    public void setNumber(final String number) {
        Number = number;
    }

    @Expose
    private String Number;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel parcel,
                              final int i) {
        parcel.writeString(this.Name);
        parcel.writeString(this.Number);

    }
    public LoyaltyObject() {

    }
    protected LoyaltyObject(Parcel in) {
        this.Name = in.readString();
        this.Number = in.readString();
    }
    public static final Creator<LoyaltyObject> CREATOR = new Creator<LoyaltyObject>() {
        @Override
        public LoyaltyObject createFromParcel(Parcel source) {
            return new LoyaltyObject(source);
        }

        @Override
        public LoyaltyObject[] newArray(int size) {
            return new LoyaltyObject[size];
        }
    };
}
