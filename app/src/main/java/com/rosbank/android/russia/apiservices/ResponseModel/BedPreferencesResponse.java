package com.rosbank.android.russia.apiservices.ResponseModel;

import com.rosbank.android.russia.apiservices.apis.appimplement.BaseResponse;
import com.rosbank.android.russia.model.CommonObject;

import java.util.List;


public class BedPreferencesResponse
        extends BaseResponse {
    private List<CommonObject> Data;

    public List<CommonObject> getData() {
        return Data;
    }

    public void setData(List<CommonObject> data) {
        this.Data = data;
    }
}
