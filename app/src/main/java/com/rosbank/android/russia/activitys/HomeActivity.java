package com.rosbank.android.russia.activitys;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.RelativeLayout;

import com.liveperson.monitoring.model.EngagementDetails;
import com.liveperson.monitoring.model.LPMonitoringIdentity;
import com.liveperson.monitoring.sdk.api.LivepersonMonitoring;
import com.liveperson.monitoring.sdk.callbacks.MonitoringErrorType;
import com.liveperson.monitoring.sdk.responses.LPEngagementResponse;
import com.liveperson.sdk.MonitoringParams;
import com.liveperson.sdk.callbacks.EngagementCallback;
import com.rosbank.android.russia.R;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.application.AppContext;
import com.rosbank.android.russia.application.coreactivitys.BaseActivityNavigationView;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.fragment.AboutLandingFragment;
import com.rosbank.android.russia.fragment.AccountLandingFragment;
import com.rosbank.android.russia.fragment.ConciergeServiceLandingFragment;
import com.rosbank.android.russia.fragment.ErrorFragment;
import com.rosbank.android.russia.fragment.ExperiencesLandingFragment;
import com.rosbank.android.russia.fragment.HomeFragment;
import com.rosbank.android.russia.fragment.MyRequestsFragment;
import com.rosbank.android.russia.helper.DoubleClickListener;
import com.rosbank.android.russia.helper.TagClickListener;
import com.rosbank.android.russia.liveperson.ChatActivity;
import com.rosbank.android.russia.model.UserItem;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.EntranceLock;
import com.rosbank.android.russia.utils.FontUtils;
import com.rosbank.android.russia.utils.IntentUtil;
import com.rosbank.android.russia.utils.Logger;
import com.rosbank.android.russia.utils.ShakeEventListener;
import com.rosbank.android.russia.utils.SharedPreferencesUtils;
import com.rosbank.android.russia.views.AnimatorUtils;
import com.rosbank.android.russia.views.ClipRevealFrame;
import com.liveperson.infra.ConversationViewParams;
import com.liveperson.infra.LPAuthenticationParams;
import com.liveperson.infra.messaging_ui.fragment.ConversationFragment;
import com.liveperson.messaging.sdk.api.LivePerson;
import com.liveperson.messaging.sdk.api.model.ConsumerProfile;
import com.ogaclejapan.arclayout.ArcLayout;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeActivity
        extends BaseActivityNavigationView
        implements View.OnClickListener {

    @BindView(R.id.root_layout)
    View rootLayout;

    @BindView(R.id.menu_layout)
    ClipRevealFrame menuLayout;
    @BindView(R.id.arc_layout)
    ArcLayout arcLayout;
    @BindView(R.id.flTooltip)
    View mFlTooltip;
    @BindView(R.id.ivCallTooltip)
    protected View ivCallTooltip;
    @BindView(R.id.ivLiveChat)
    protected View ivLiveChat;
    @BindView(R.id.ivSendRequestTooltip)
    protected View ivSendRequestTooltip;

    @BindView(R.id.btnCall)
    protected View btnCall;
    @BindView(R.id.btnLiveChat)
    protected View btnLiveChat;
    @BindView(R.id.btnSendConcierge)
    protected View btnSendConcierge;


    private MenuItem mSignInMenuItem;
    private MenuItem mSignOutMenuItem;
    private static final int MENU_ITEMS = 10;
    private final ArrayList<View> mMenuItems = new ArrayList<>(MENU_ITEMS);

    private SensorManager mSensorManager;
    ShakeEventListener mSensorListener;
    EntranceLock entranceLock = new EntranceLock();

    private static final String TAG = HomeActivity.class.getSimpleName();

    public static final String CAMPAIGN_SAVED = "CAMPAIGN_SAVED";
    public static final String CAMPAIGN_ID = "campaignID";
    public static final String ENGAGEMENT_ID = "engagementID";
    public static final String ENGAGEMENT_CONTEXT_ID = "engagementContextID";
    public static final String SESSION_ID = "sessionID";
    public static final String VISITOR_ID = "visitorID";

    @Override
    protected int activityLayoutId() {
        return R.layout.activity_home;
    }

    @Override
    protected int layoutContainerId() {
        return R.id.home_container;
    }

    @Override
    protected void initView() {

        ButterKnife.bind(this);
        if (!isCampaignSaved()) {
            setMonitoringParams(false);
        }
        CommonUtils.setFontForTextView(mTvTitle,
                FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        CommonUtils.setFontForTextView(mTvRightTitle,
                FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);

        mIvLogoAppToolbar.setOnClickListener(new DoubleClickListener() {
            @Override
            public void onSingleClick(final View v) {

            }

            @Override
            public void onDoubleClick(final View v) {

                Intent intent = new Intent(getBaseContext(),
                        SplashAcitivy.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.push_down_in,
                        R.anim.push_down_out);
            }
        });

        fab.bringToFront();

        fab.setOnClickListener(new TagClickListener() {
            @Override
            public void onSingleClick(View v) {
                //showToast("Single");
                //Logger.sout("SingleClick");
                closeNavMenu();
                onFabClick(false);
            }

            @Override
            public void onDoubleClick(View v) {
                //showToast("double");
                Logger.sout("DoubleClick");
            }
        });

        for (int i = 0, size = arcLayout.getChildCount(); i < size; i++) {
            arcLayout.getChildAt(i)
                    .setOnClickListener(this);
        }


        final Menu navMenu = navigationView.getMenu();
        navigationView.getViewTreeObserver()
                .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        // Remember to remove the installed OnGlobalLayoutListener
                        navigationView.getViewTreeObserver()
                                .removeOnGlobalLayoutListener(this);
                        // Loop through and find each MenuItem View

                        for (int i = 0; i < navMenu.size(); i++) {
                            final MenuItem item = navMenu.getItem(i);
                            if (i == 6) {
                                mSignInMenuItem = item;
                            }
                            if (i == 7) {
                                mSignOutMenuItem = item;
                            }

                            navigationView.findViewsWithText(mMenuItems,
                                    item.getTitle(),
                                    View.FIND_VIEWS_WITH_TEXT);
                            CommonUtils.applyFontToMenuItem(getBaseContext(), item, FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD);

                        }

                        showOrHideSignInOutItem();
                    }
                });

        navigationView.getMenu()
                .getItem(0)
                .setChecked(true);
        pushFragment(new HomeFragment(),
                true,
                false,
                false);

        /* do this in onCreate */
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        mSensorListener = new ShakeEventListener();
        mSensorListener.setOnShakeListener(new ShakeEventListener.OnShakeListener() {
            @Override
            public void onShake() {
                onFabClick(false);
            }
        });
        getSupportFragmentManager().addOnBackStackChangedListener(new android.support.v4.app.FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                if (getCurrentFragment() instanceof HomeFragment) {
                    navigationView.getMenu()
                            .getItem(0)
                            .setChecked(true);
                }
                if (getCurrentFragment() instanceof AboutLandingFragment) {
                    navigationView.getMenu()
                            .getItem(7)
                            .setChecked(true);
                }
            }
        });
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        // Show tutorial at the first time
        if (!SharedPreferencesUtils.getPreferences(AppConstant.PRE_B2C_TOOLTIP_SHOW,
                false)) {
            fab.performClick();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (UserItem.isVip()) {
            mIvLogoAppToolbar.setImageDrawable(getResources().getDrawable(R.drawable.hermitage_logo_2));
        } else {
            mIvLogoAppToolbar.setImageDrawable(getResources().getDrawable(R.drawable.rosbank_logo_2));
        }
        //update status signin/signout
        showOrHideSignInOutItem();
        //
        updateStatusHomePage();

        //
        if (mSensorManager != null) {
            mSensorManager.registerListener(mSensorListener,
                    mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                    SensorManager.SENSOR_DELAY_NORMAL);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mSensorManager != null) {
            mSensorManager.unregisterListener(mSensorListener);
        }
    }

    @Override
    public void hideFloatingActionButton() {
        super.hideFloatingActionButton();
        if (mSensorManager != null) {
            mSensorManager.unregisterListener(mSensorListener);
        }
    }

    @Override
    public void showFloatingActionButton() {
        super.showFloatingActionButton();
        if (mSensorManager != null) {
            mSensorManager.registerListener(mSensorListener,
                    mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                    SensorManager.SENSOR_DELAY_NORMAL);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode,
                permissions,
                grantResults);

        switch (requestCode) {
            case AppConstant.PERMISSION_REQUEST_CALL_REQUEST:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    callPhone();
                }
                break;
        }

    }

    @Override
    public void onBackPressed() {
        if (fab.isSelected()) {
            onFabClick(false);
        } else {
            if (BaseFragment.isTakeMoreAction) {
                super.onBackPressed();
            }

        }
    }

    public void showFab(Boolean isShown) {
        fab.setVisibility(isShown ? View.VISIBLE : View.GONE);
    }

    public boolean isChatVisible() {
        ConversationFragment fragment = (ConversationFragment) getSupportFragmentManager().findFragmentByTag(ConversationFragment.class.getSimpleName());
        return fragment != null && fragment.isVisible();
    }

    public void showOrHideSignInOutItem() {
        if (mSignOutMenuItem != null && mSignInMenuItem != null) {
            if (!UserItem.isLogined()) {
                mSignOutMenuItem.setVisible(false);
                mSignInMenuItem.setVisible(true);
            } else {
                mSignOutMenuItem.setVisible(true);
                mSignInMenuItem.setVisible(false);
            }
        }
    }

    @Override
    protected void onToolbarOpened(View drawerView) {
        showOrHideSignInOutItem();
        // Hide the sticky if it opens before
        if (!isChatVisible()) {
            closeFab();
            fab.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    protected void onToolbarClosed(View drawerView) {
        if (!isChatVisible()) {
            fab.setVisibility(View.VISIBLE);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        if (entranceLock.isClickContinuous()) {
            return true;
        }
        // Handle navigation view item clicks here.
        int id = item.getItemId();
//        View menuItemViewBackground = View.inflate(this, R.layout.menu_item_layout, null);
//        item.setActionView(menuItemViewBackground);
        Fragment currentFragment = getCurrentFragment();
        if (id == R.id.nav_home) {
            AppContext.getSharedInstance()
                    .track(AppConstant.ANALYTIC_EVENT_CATEGORY_CONCIERGE,
                            AppConstant.ANALYTIC_EVENT_ACTION_OPEN_MENU,
                            AppConstant.ANALYTIC_MENU_ITEM.HOME.getValue());
            if ((currentFragment instanceof HomeFragment)) {
                ((HomeFragment) currentFragment).scrollToTop();
            } else {
                //clearAllFragments();
                pushFragment(new HomeFragment(),
                        true,
                        false,
                        false);
            }
        } else if (id == R.id.nav_account) {
            AppContext.getSharedInstance()
                    .track(AppConstant.ANALYTIC_EVENT_CATEGORY_CONCIERGE,
                            AppConstant.ANALYTIC_EVENT_ACTION_OPEN_MENU,
                            AppConstant.ANALYTIC_MENU_ITEM.ACCOUNT.getValue());
            if (UserItem.isLogined()) {
                if (!(currentFragment instanceof AccountLandingFragment)) {
                    pushFragment(new AccountLandingFragment(),
                            true,
                            true,
                            false);
                }
            } else {
                showDialogRequiredSignIn();
            }
        } else if (id == R.id.nav_my_requests) {
            AppContext.getSharedInstance()
                    .track(AppConstant.ANALYTIC_EVENT_CATEGORY_CONCIERGE,
                            AppConstant.ANALYTIC_EVENT_ACTION_OPEN_MENU,
                            AppConstant.ANALYTIC_MENU_ITEM.MY_REQUESTS.getValue());
            if (UserItem.isLogined()) {
                if (!(currentFragment instanceof MyRequestsFragment)) {
                    pushFragment(new MyRequestsFragment(),
                            true,
                            true,
                            false);
                }
            } else {
                showDialogRequiredSignIn();
            }

        } /*else if (id == R.id.nav_notifications) {
            pushFragment(new NotificationsFragment(),
                         true,
                         true);
        }*/ else if (id == R.id.nav_send_concierge_requests) {
            AppContext.getSharedInstance()
                    .track(AppConstant.ANALYTIC_EVENT_CATEGORY_CONCIERGE,
                            AppConstant.ANALYTIC_EVENT_ACTION_OPEN_MENU,
                            AppConstant.ANALYTIC_MENU_ITEM.SEND_CONCIERGE_REQUEST.getValue());
            if (UserItem.isLogined()) {
                if (!(currentFragment instanceof ConciergeServiceLandingFragment)) {
                    pushFragment(new ConciergeServiceLandingFragment(),
                            true,
                            true,
                            false);
                }
            } else {
                showDialogRequiredSignIn();
            }
        }/* else if (id == R.id.nav_city_guides) {

        }*/ else if (id == R.id.nav_experiences) {
            if (!(currentFragment instanceof ExperiencesLandingFragment)) {
                pushFragment(new ExperiencesLandingFragment(),
                        true,
                        true,
                        false);
            }
        } else if (id == R.id.nav_about) {
            AppContext.getSharedInstance()
                    .track(AppConstant.ANALYTIC_EVENT_CATEGORY_CONCIERGE,
                            AppConstant.ANALYTIC_EVENT_ACTION_OPEN_MENU,
                            AppConstant.ANALYTIC_MENU_ITEM.ABOUT.getValue());
            if (!(currentFragment instanceof AboutLandingFragment)) {
                pushFragment(new AboutLandingFragment(),
                        true,
                        true,
                        false);
            }
        } else if (id == R.id.nav_sign_in) {
            AppContext.getSharedInstance()
                    .track(AppConstant.ANALYTIC_EVENT_CATEGORY_CONCIERGE,
                            AppConstant.ANALYTIC_EVENT_ACTION_OPEN_MENU,
                            AppConstant.ANALYTIC_MENU_ITEM.SIGNIN.getValue());
            Intent intent = new Intent(this,
                    LoginAcitivy.class);
            startActivity(intent);

        } else if (id == R.id.nav_sign_out) {
            AppContext.getSharedInstance()
                    .track(AppConstant.ANALYTIC_EVENT_CATEGORY_CONCIERGE,
                            AppConstant.ANALYTIC_EVENT_ACTION_OPEN_MENU,
                            AppConstant.ANALYTIC_MENU_ITEM.SIGNOUT.getValue());
            signOut();
            //AppContext.getSharedInstance().changeLocale("en");

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.END);

        return true;
    }

    @OnClick({R.id.ic_back})
    public void onViewClick(View view) {
        switch (view.getId()) {
            case R.id.ic_back:
                onBackPressed();
                break;
        }
    }

    public void showLogoApp(boolean isShowLogo) {
        if (isShowLogo) {
            mIvLogoAppToolbar.setVisibility(View.VISIBLE);
            mIvBack.setVisibility(View.GONE);
            mTvTitle.setText("");
            mTvTitle.setVisibility(View.GONE);
        } else {
            mIvLogoAppToolbar.setVisibility(View.GONE);
            mIvBack.setVisibility(View.VISIBLE);
            mTvTitle.setVisibility(View.VISIBLE);
        }
    }

    public void showRightTextTitle(boolean isShow,
                                   View.OnClickListener onClickListener) {
        if (isShow) {
            toolbar.setNavigationIcon(null);
            mTvRightTitle.setVisibility(View.VISIBLE);
            mTvRightTitle.setOnClickListener(onClickListener);
        } else {
            toolbar.setNavigationIcon(R.drawable.menu);
            mTvRightTitle.setVisibility(View.GONE);
        }
    }

    public void showDialogRequiredSignIn() {
        CommonUtils.showDialogRequiredSignIn(this,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog,
                                        final int which) {
                        Intent intent = new Intent(getBaseContext(),
                                LoginAcitivy.class);
                        startActivity(intent);
                    }
                },
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog,
                                        final int which) {
                        if (getCurrentFragment() instanceof HomeFragment) {
                            navigationView.getMenu()
                                    .getItem(0)
                                    .setChecked(true);
                        }
                        if (getCurrentFragment() instanceof AboutLandingFragment) {
                            navigationView.getMenu()
                                    .getItem(7)
                                    .setChecked(true);
                        }

                    }
                });


    }

    private void signOut() {

        UserItem.logOut();
        //
        showOrHideSignInOutItem();
        //
        //updateStatusHomePage();
        Intent intent = new Intent(this,
                LoginAcitivy.class);
        startActivity(intent);

        //
        finish();
    }

    public void setVisibilityForLogoAppToolbar(int visibility) {
        mIvLogoAppToolbar.setVisibility(visibility);
    }

    private void updateStatusHomePage() {
        Fragment current = getCurrentFragment();
        if (current != null && current instanceof HomeFragment) {
            ((HomeFragment) current).updateWelcomText();
        }
    }

    private void callPhone() {
        // open Action view for phone call, dont need CALL REQUEST permission
        IntentUtil call = IntentUtil.getInstance(this);
        if (UserItem.isVip()) {
            call.openCall(this.getString(R.string.hermitage_phone_number));
        } else {
            call.openCall(this.getString(R.string.rosbank_phone_number));
        }
    }

    /**
     * =============================================
     */
    private void onFabClick(boolean isOpenChat) {
        View v = fab;
        int x = (v.getLeft() + v.getRight()) / 2;
        int y = (v.getTop() + v.getBottom()) / 2;
        int w = rootLayout.getWidth() - x;
        int h = rootLayout.getHeight() - y;
        float radiusOfFab = 1f * v.getWidth() / 2f;
        float radiusFromFabToRoot = (float) Math.hypot(
                Math.max(x,
                        w),
                Math.max(y,
                        h));

        if (v.isSelected()) {
            rotateClose();
            hideMenu(x,
                    y,
                    radiusFromFabToRoot,
                    radiusOfFab, isOpenChat);
        } else {
            rotateOpen();
            showMenu(x,
                    y,
                    radiusOfFab,
                    radiusFromFabToRoot);
            //
                /*expandView = false;
                menuChangeState();*/
        }
        v.setSelected(!v.isSelected());
    }

    private void closeFab() {
        View v = fab;
        int x = (v.getLeft() + v.getRight()) / 2;
        int y = (v.getTop() + v.getBottom()) / 2;
        int w = rootLayout.getWidth() - x;
        int h = rootLayout.getHeight() - y;
        float radiusOfFab = 1f * v.getWidth() / 2f;
        float radiusFromFabToRoot = (float) Math.hypot(
                Math.max(x,
                        w),
                Math.max(y,
                        h));
        if (v.isSelected()) {
            rotateClose();
            hideMenu(x,
                    y,
                    radiusFromFabToRoot,
                    radiusOfFab, false);
            v.setSelected(false);
        }
    }

    private void rotateOpen() {
        /*RotateAnimation rotate = new RotateAnimation(0, 360, Animation.RELATIVE_TO_SELF,
                0.5f,  Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(500);

        fab.startAnimation(rotate);*/

        /*ObjectAnimator imageViewObjectAnimator = ObjectAnimator.ofFloat(fab,
                                                                        getString(R.string.text_rotation),
                                                                        0f,
                                                                        360f);
        imageViewObjectAnimator.setDuration(400); // miliseconds
        imageViewObjectAnimator.start();*/
    }

    private void rotateClose() {
        /*RotateAnimation rotate = new RotateAnimation(0, -360, Animation.RELATIVE_TO_SELF,
                0.5f,  Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(500);

        fab.startAnimation(rotate);*/
       /* ObjectAnimator imageViewObjectAnimator = ObjectAnimator.ofFloat(fab,
                                                                        getString(R.string.text_rotation),
                                                                        0f,
                                                                        -360f);
        imageViewObjectAnimator.setDuration(400); // miliseconds
        imageViewObjectAnimator.start();*/
    }

    private void calculateTheTooltipPosition(View anchor,
                                             View tooltip,
                                             int extraHeight, int extraPadding) {
        int[] anchorCoordinate = new int[2];
        anchor.getLocationOnScreen(anchorCoordinate);

        int anchorWidth = anchor.getWidth();
        int anchorHeight = anchor.getHeight();

        int tooltipWidth = tooltip.getWidth();
        int tooltipHeight = tooltip.getHeight();

        int tooltipPosX = (anchorCoordinate[0] + anchorWidth / 2) - tooltipWidth;
        int tooltipPosY = anchorCoordinate[1] - 3 * anchorHeight / 2 - tooltipHeight - extraHeight;

        RelativeLayout.LayoutParams layoutParams =
                new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.leftMargin = tooltipPosX - extraPadding;
        layoutParams.topMargin = tooltipPosY;
        tooltip.setLayoutParams(layoutParams);
    }

    private void showMenu(int cx,
                          int cy,
                          float startRadius,
                          float endRadius) {

        menuLayout.setVisibility(View.VISIBLE);

        List<Animator> animList = new ArrayList<>();

        Animator revealAnim = createCircularReveal(menuLayout,
                cx,
                cy,
                startRadius,
                endRadius);
        revealAnim.setInterpolator(new AccelerateDecelerateInterpolator());
        revealAnim.setDuration(200);
        animList.add(revealAnim);

        //animList.add(createShowItemAnimator(fab));

        for (int i = 0, len = arcLayout.getChildCount(); i < len; i++) {
            animList.add(createShowItemAnimator(arcLayout.getChildAt(i)));
        }

        AnimatorSet animSet = new AnimatorSet();
        animSet.playSequentially(animList);
        animSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                if (!SharedPreferencesUtils.getPreferences(AppConstant.PRE_B2C_TOOLTIP_SHOW,
                        false)) {
                    // Check tooltip show for the first time
                    mFlTooltip.setVisibility(View.VISIBLE);
                    calculateTheTooltipPosition(btnCall,
                            ivCallTooltip, 0, (int) getResources().getDimension(R.dimen.margin_20dp));
                    /*calculateTheTooltipPosition(btnLiveChat,
                                                ivLiveChat
                            ,
                                                0,(int) getResources().getDimension(R.dimen.margin_20dp));*/
                    calculateTheTooltipPosition(btnSendConcierge,
                            ivSendRequestTooltip, 0, 0);

                    calculateTheTooltipPosition(btnLiveChat,
                            ivLiveChat, 0, (int) getResources().getDimension(R.dimen.margin_10dp));

                    SharedPreferencesUtils.setPreferences(AppConstant.PRE_B2C_TOOLTIP_SHOW,
                            true);
                } else {
                    mFlTooltip.setVisibility(View.GONE);
                }
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        animSet.start();
    }

    private void hideMenu(int cx,
                          int cy,
                          float startRadius,
                          float endRadius, boolean isOpenChat) {
        mFlTooltip.setVisibility(View.GONE);
        List<Animator> animList = new ArrayList<>();
        for (int i = arcLayout.getChildCount() - 1; i >= 0; i--) {
            animList.add(createHideItemAnimator(arcLayout.getChildAt(i)));
        }

        //animList.add(createHideItemAnimator(imgConcierge));

        Animator revealAnim = createCircularReveal(menuLayout,
                cx,
                cy,
                startRadius,
                endRadius);
        revealAnim.setInterpolator(new AccelerateDecelerateInterpolator());
        revealAnim.setDuration(200);
        revealAnim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                menuLayout.setVisibility(View.INVISIBLE);
                if (isOpenChat) startChat();
            }
        });

        animList.add(revealAnim);

        AnimatorSet animSet = new AnimatorSet();
        animSet.playSequentially(animList);
        animSet.start();

    }

    private Animator createShowItemAnimator(View item) {
        View v = fab;
        float dx = v.getX() - item.getX();
        float dy = v.getY() - item.getY();

        item.setScaleX(0f);
        item.setScaleY(0f);
        item.setTranslationX(dx);
        item.setTranslationY(dy);

        Animator anim = ObjectAnimator.ofPropertyValuesHolder(
                item,
                AnimatorUtils.scaleX(0f,
                        1f),
                AnimatorUtils.scaleY(0f,
                        1f),
                AnimatorUtils.translationX(dx,
                        0f),
                AnimatorUtils.translationY(dy,
                        0f)
        );

        anim.setInterpolator(new DecelerateInterpolator());
        anim.setDuration(50);
        return anim;
    }

    private Animator createHideItemAnimator(final View item) {
        View v = fab;
        final float dx = v.getX() - item.getX();
        final float dy = v.getY() - item.getY();

        Animator anim = ObjectAnimator.ofPropertyValuesHolder(
                item,
                AnimatorUtils.scaleX(1f,
                        0f),
                AnimatorUtils.scaleY(1f,
                        0f),
                AnimatorUtils.translationX(0f,
                        dx),
                AnimatorUtils.translationY(0f,
                        dy)
        );

        anim.setInterpolator(new DecelerateInterpolator());
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                item.setTranslationX(0f);
                item.setTranslationY(0f);
            }
        });
        anim.setDuration(50);
        return anim;
    }

    private Animator createCircularReveal(final ClipRevealFrame view,
                                          int x,
                                          int y,
                                          float startRadius,
                                          float endRadius) {
        final Animator reveal;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            reveal = ViewAnimationUtils.createCircularReveal(view,
                    x,
                    y,
                    startRadius,
                    endRadius);
        } else {
            view.setClipOutLines(true);
            view.setClipCenter(x,
                    y);
            reveal = ObjectAnimator.ofFloat(view,
                    getString(R.string.text_clip_radius)
                    ,
                    startRadius,
                    endRadius);
            reveal.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    view.setClipOutLines(false);
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
        }
        return reveal;
    }

    /**
     * =============================================
     */

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSendConcierge:
                AppContext.getSharedInstance()
                        .track(AppConstant.ANALYTIC_EVENT_CATEGORY_CONCIERGE,
                                AppConstant.ANALYTIC_EVENT_ACTION_SEND_REQUEST,
                                "");
                onFabClick(false);
                if (UserItem.isLogined()) {
                    pushFragment(new ConciergeServiceLandingFragment(),
                            false,
                            true,
                            true);
                } else {
                    showDialogRequiredSignIn();
                }
                break;
            case R.id.btnLiveChat:
                if (UserItem.isLogined()) {
                    //initConversationFragment();
                    onFabClick(true);
                } else {
                    onFabClick(false);
                    showDialogRequiredSignIn();
                }
                break;
            case R.id.btnCall:
                AppContext.getSharedInstance()
                        .track(AppConstant.ANALYTIC_EVENT_CATEGORY_CONCIERGE,
                                AppConstant.ANALYTIC_EVENT_ACTION_CALL_CONCIERGE,
                                "");

                menuLayout.setVisibility(View.INVISIBLE);
                if (UserItem.isLogined()) {
                    fab.setSelected(!fab.isSelected());
                    callPhone();
                } else {
                    onFabClick(false);
                    showDialogRequiredSignIn();
                }
                break;
            case R.id.btnSurprised:
                onFabClick(false);
                if (UserItem.isLogined()) {
                    //showToast("Surprised");
                    pushFragment(new ErrorFragment(),
                            false,
                            true,
                            true);
                } else {
                    showDialogRequiredSignIn();
                }
                break;
        }
    }

    private void openChatWindow() {
        startActivity(new Intent(this, ChatActivity.class));
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    private void startChat() {
        if (isCampaignSaved()|| isEngagementExists()) {
            startActivity(new Intent(this, ChatActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
        } else {
            setMonitoringParams(true);
        }
    }

    private void setMonitoringParams(Boolean isShowLoading) {
        if (isShowLoading) showDialogProgress();
        JSONArray entryPoints = new JSONArray();
        entryPoints.put("sec://rosbank");

        MonitoringParams monitoringParams = new MonitoringParams("", entryPoints, null);

        List<LPMonitoringIdentity> identityList = new ArrayList<>();
        identityList.add(new LPMonitoringIdentity("", ""));

        LivepersonMonitoring.getEngagement(HomeActivity.this, identityList, monitoringParams, new EngagementCallback() {
            @Override
            public void onSuccess(LPEngagementResponse lpEngagementResponse) {

                List<EngagementDetails> engagementDetails = lpEngagementResponse.getEngagementDetailsList();

                if (engagementDetails != null && !engagementDetails.isEmpty()) {
                    saveCampaignInfo(true, lpEngagementResponse);
                } else {
                    saveCampaignInfo(false, null);
                }
                hideDialogProgress();
                if (isShowLoading) openChatWindow();
            }

            @Override
            public void onError(MonitoringErrorType monitoringErrorType, Exception e) {
                if(!isEngagementExists()){
                    saveCampaignInfo(false, null);
                }
                hideDialogProgress();
                if (isShowLoading) openChatWindow();
            }
        });
    }

    private void saveCampaignInfo(Boolean isSuccess, LPEngagementResponse lpEngagementResponse) {
        SharedPreferencesUtils.setPreferences(CAMPAIGN_SAVED, isSuccess);
        if (isSuccess && lpEngagementResponse != null) {
            List<EngagementDetails> engagementDetails = lpEngagementResponse.getEngagementDetailsList();
            SharedPreferencesUtils.setPreferences(CAMPAIGN_ID, engagementDetails.get(0).getCampaignId());
            SharedPreferencesUtils.setPreferences(ENGAGEMENT_ID, engagementDetails.get(0).getEngagementId());
            SharedPreferencesUtils.setPreferences(ENGAGEMENT_CONTEXT_ID, engagementDetails.get(0).getContextId());
            SharedPreferencesUtils.setPreferences(SESSION_ID, lpEngagementResponse.getSessionId());
            SharedPreferencesUtils.setPreferences(VISITOR_ID, lpEngagementResponse.getVisitorId());
        }
    }


    public boolean isCampaignSaved() {
        return SharedPreferencesUtils.getPreferences(CAMPAIGN_SAVED, false);
    }

    public boolean isEngagementExists(){
        return !Objects.equals(SharedPreferencesUtils.getPreferences(ENGAGEMENT_ID, "0"), "0");
    }
}