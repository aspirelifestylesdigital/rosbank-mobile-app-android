package com.rosbank.android.russia.apiservices.b2c.ResponseModel;

import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.google.gson.annotations.Expose;


public class B2CGetPreferenceResponse
        extends B2CBaseResponse {
    @Expose
    private String MYPREFERENCESID;
    @Expose
    private String TYPE;
    @Expose
    private String VALUE;
    @Expose
    private String VALUE1;
    @Expose
    private String VALUE2;
    @Expose
    private String VALUE3;
    @Expose
    private String VALUE4;
    @Expose
    private String VALUE5;
    @Expose
    private String VALUE6;

    public String getMYPREFERENCESID() {
        return MYPREFERENCESID;
    }

    public void setMYPREFERENCESID(String MYPREFERENCESID) {
        this.MYPREFERENCESID = MYPREFERENCESID;
    }

    public String getTYPE() {
        return TYPE;
    }

    public void setTYPE(String TYPE) {
        this.TYPE = TYPE;
    }

    public String getVALUE() {
        return VALUE;
    }

    public void setVALUE(String VALUE) {
        this.VALUE = VALUE;
    }

    public String getVALUE1() {
        return VALUE1;
    }

    public void setVALUE1(String VALUE1) {
        this.VALUE1 = VALUE1;
    }

    public String getVALUE2() {
        return VALUE2;
    }

    public void setVALUE2(String VALUE2) {
        this.VALUE2 = VALUE2;
    }

    public String getVALUE3() {
        return VALUE3;
    }

    public void setVALUE3(String VALUE3) {
        this.VALUE3 = VALUE3;
    }

    public String getVALUE4() {
        return VALUE4;
    }

    public void setVALUE4(String VALUE4) {
        this.VALUE4 = VALUE4;
    }

    public String getVALUE5() {
        return VALUE5;
    }

    public void setVALUE5(String VALUE5) {
        this.VALUE5 = VALUE5;
    }

    public String getVALUE6() {
        return VALUE6;
    }
}
