package com.rosbank.android.russia.dcr.api;

import com.rosbank.android.russia.apiservices.ResponseModel.AuthenticateResponse;
import com.rosbank.android.russia.dcr.api.ResponseModel.DCRAutocompleteSearchResponse;
import com.rosbank.android.russia.dcr.api.ResponseModel.DCRGetPrivilegeDetailResponse;
import com.rosbank.android.russia.dcr.api.ResponseModel.DCRGetPrivilegesResponse;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Created by anh.trinh on 03/10/2017.
 */
public interface DCRApiInterface {

    /**
     * DEFINE API NAME
     * ======================================================
     */
    String WS_AUTHENTICATE = "api/v1/Authenticate";
    String WS_GET_PRIVILEGE = "api/v1/items/search";
    String WS_GET_PRIVILEGE_DETAIL = "api/v1/items";
    String WS_ITEM_AUTOCOMPLETE_SEARCH = "api/v1/items/autocomplete";
    /**
     * =====================================================
     */
    String WS_API_QUERY_USERID = "UserID";
    String WS_API_QUERY_COUNTRY = "Country";
    String WS_API_QUERY_BOOKINGID = "BookingId";
    String WS_QUERY_RESTAURANT_ID = "RestaurantId";
   // String WS_SUBJECTTION_KEY = "51e4675983a04e06b3f1faf974b9e86f";
    //String WS_SUBJECTTION_KEY = "828da96478e84e0bba15b78afbabaf40";
    String WS_API_QUERY_LANGUAGE = "language";
    String WS_API_QUERY_ITEM_TYPE = "item_type";
    String WS_API_QUERY_TERM = "term";
    String WS_API_QUERY_TERM_OPERATOR = "term_operator";
    String WS_API_QUERY_EXAC_MATCH = "exact_match";
    String WS_API_QUERY_SORT = "sort";
    String WS_API_QUERY_FACET = "facets";
    String WS_API_QUERY_PAGE = "page";
    String WS_API_QUERY_SIZE = "size";
    String WS_API_QUERY_IDS = "ids";

    /**
     * DEFINE API METHOD
     * ======================================================
     */
    @POST(WS_AUTHENTICATE)
    Call<AuthenticateResponse> authenticate(@Query("UserID") String username,
                                            @Query("Password") String secret);

    /*api/v1/items/search?language=en&item_type=privilege&term=training%20sessions&term_
    operator=OR&exact_match=false&sort=Name&facets=Type HTTP/1.1
    Host: aspiredigitalapitest.azure-api.net/dcr
    Ocp-Apim-Subscription-Key: KhXfp555U_hCIUxdJns0A23h59lhm9qqU4jfF-VMd7e*/
    @GET(WS_GET_PRIVILEGE)
    Call<DCRGetPrivilegesResponse> getPrivileges(@QueryMap Map<String, String> map);

    @GET(WS_GET_PRIVILEGE_DETAIL)
    Call<DCRGetPrivilegeDetailResponse> getPrivilegeDetail(@QueryMap Map<String, String> map);


    @GET(WS_ITEM_AUTOCOMPLETE_SEARCH)
    Call<DCRAutocompleteSearchResponse> getAutocompleteSearch(@QueryMap Map<String, String> map);

       /**
     * DEFINE QUERY_KEY
     * ======================================================
     */

/***********************************************************************************************************/
/***********************************************************************************************************/
/***********************************************************************************************************/
}
