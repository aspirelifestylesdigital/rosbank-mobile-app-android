package com.rosbank.android.russia.apiservices.ResponseModel;

import com.rosbank.android.russia.apiservices.apis.appimplement.BaseResponse;
import com.rosbank.android.russia.model.BookingObject;


public class BookTransferResponse
        extends BaseResponse {
    private BookingObject Data;

    public BookingObject getData() {
        return Data;
    }

    public void setData(BookingObject data) {
        this.Data = data;
    }
}
