package com.rosbank.android.russia.apiservices.ResponseModel;

import com.rosbank.android.russia.apiservices.apis.appimplement.BaseResponse;
import com.rosbank.android.russia.model.CityObject;

import java.util.List;


public class GetCitiesResponse
        extends BaseResponse {
    private List<CityObject> Data;

    public List<CityObject> getData() {
        return Data;
    }

    public void setData(List<CityObject> data) {
        this.Data = data;
    }
}
