package com.rosbank.android.russia.apiservices;

import com.rosbank.android.russia.apiservices.ResponseModel.BookRestaurantResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.ApiProviderClient;
import com.rosbank.android.russia.utils.StringUtil;

import org.json.JSONArray;

import android.support.annotation.NonNull;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Callback;


public class WSBookingRestaurantRecommend
        extends ApiProviderClient<BookRestaurantResponse> {

    private String BookingId = "";
    private String UserID = "";
    private String UploadPhoto = "";
    private Boolean Phone = false;
    private Boolean Email = false;
    private Boolean Both = false;
    private String MobileNumber = "";
    private String EmailAddress = "";
    private String ReservationDate = "";
    private String ReservationTime = "";
    private String Occasion = "";
    private List<String> Cuisines = new ArrayList<>();
    private String MinimumPrice;
    private String MaximumPrice;
    private String Country = "";
    private String City = "";
    private Integer NumberOfAdults = 0;
    private Integer NumberOfKids = 0;
    private String SpecialRequirements = "";

    public WSBookingRestaurantRecommend(){

    }

    @Override
    public void run(Callback<BookRestaurantResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }

    @Override
    protected boolean isUseAuthenticateInApi() {
        return true;
    }

    @Override
    public void runApi() {

        JSONArray arr = new JSONArray(Cuisines);

        RequestBody partBookingId = createPartFromString(BookingId);
        RequestBody partUserID = createPartFromString(UserID);
        RequestBody partPhone = createPartFromString(Phone+"");
        RequestBody partEmail = createPartFromString(Email+"");
        RequestBody partBoth = createPartFromString(Both+"");
        RequestBody partMobileNumber = createPartFromString(MobileNumber);
        RequestBody partEmailAddress = createPartFromString(EmailAddress);
        RequestBody partReservationDate = createPartFromString(ReservationDate);
        RequestBody partReservationTime = createPartFromString(ReservationTime);
        RequestBody partOccasion = createPartFromString(Occasion);
       // RequestBody partCuisines = createPartFromString(arr.toString());
        RequestBody partMinPrice = createPartFromString(MinimumPrice);
        RequestBody partMaxPrice = createPartFromString(MaximumPrice);
        RequestBody partCountry = createPartFromString(Country);
        RequestBody partCity = createPartFromString(City);
        RequestBody partNumberOfAdults = createPartFromString(NumberOfAdults+"");
        RequestBody partNumberOfKids = createPartFromString(NumberOfKids+"");
        RequestBody partSpecialRequirements = createPartFromString(SpecialRequirements+"");

        MultipartBody.Part partUploadPhoto = null;
        File file = new File(UploadPhoto);
        if(!StringUtil.isEmpty(UploadPhoto) && file.exists()){
            RequestBody requestFile =
                    RequestBody.create(MediaType.parse("multipart/form-data"), file);
            partUploadPhoto =
                    MultipartBody.Part.createFormData("picture", file.getName(), requestFile);
        }
        else{

        }

        HashMap<String, RequestBody> map = new HashMap<>();
        map.put("BookingId", partBookingId);
        map.put("UserID", partUserID);
        map.put("Phone", partPhone);
        map.put("Email", partEmail);
        map.put("Both", partBoth);
        map.put("MobileNumber", partMobileNumber);
        map.put("EmailAddress", partEmailAddress);
        map.put("ReservationDate", partReservationDate);
        map.put("ReservationTime", partReservationTime);
        map.put("Occasion", partOccasion);
       // map.put("Cuisines", partCuisines);
        map.put("MinimumPrice", partMinPrice);
        map.put("MaximumPrice", partMaxPrice);
        map.put("Country", partCountry);
        map.put("City", partCity);
        map.put("NumberOfAdults", partNumberOfAdults);
        map.put("NumberOfKids", partNumberOfKids);
        map.put("SpecialRequirements", partSpecialRequirements);

        RequestBody rb;
        for(int i=0;i<Cuisines.size();i++)
        {
            rb=RequestBody.create(MediaType.parse("text/plain"), Cuisines.get(i));
            map.put("Cuisines["+i+"]", rb);
        }

        serviceInterface.bookRestaurantRecommend(map, partUploadPhoto).enqueue(callback);

        /*serviceInterface.bookRestaurant(partBookingId,
                partUserID,
                partBookingItemId,
                partUploadPhoto,
                partPhone,
                partEmail,
                partBoth,
                partMobileNumber,
                partEmailAddress,
                partReservationDate,
                partReservationTime,
                partReservationName,
                partCountry,
                partCity,
                partNumberOfAdults,
                partNumberOfKids,
                partSpecialRequirements
        ).enqueue(callback);*/
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        Map<String, String> headers = new HashMap<>();
        //headers.put("User-Agent", "Retrofit-Sample-App");
        return headers;
    }

    @NonNull
    private RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(
                MediaType.parse("multipart/form-data"), descriptionString);
    }

    public String getBookingId() {
        return BookingId;
    }

    public void setBookingId(String bookingId) {
        BookingId = bookingId;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public String getUploadPhoto() {
        return UploadPhoto;
    }

    public void setUploadPhoto(String uploadPhoto) {
        UploadPhoto = uploadPhoto;
    }

    public Boolean getPhone() {
        return Phone;
    }

    public void setPhone(Boolean phone) {
        Phone = phone;
    }

    public Boolean getEmail() {
        return Email;
    }

    public void setEmail(Boolean email) {
        Email = email;
    }

    public Boolean getBoth() {
        return Both;
    }

    public void setBoth(Boolean both) {
        Both = both;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public String getEmailAddress() {
        return EmailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        EmailAddress = emailAddress;
    }

    public String getReservationDate() {
        return ReservationDate;
    }

    public void setReservationDate(String reservationDate) {
        ReservationDate = reservationDate;
    }

    public String getReservationTime() {
        return ReservationTime;
    }

    public void setReservationTime(String reservationTime) {
        ReservationTime = reservationTime;
    }

    public String getOccasion() {
        return Occasion;
    }

    public void setOccasion(String occasion) {
        Occasion = occasion;
    }

    public List<String> getCuisines() {
        return Cuisines;
    }

    public void setCuisines(List<String> cuisines) {
        Cuisines = cuisines;
    }

    public String getMinimumPrice() {
        return MinimumPrice;
    }

    public void setMinimumPrice(String minimumPrice) {
        MinimumPrice = minimumPrice;
    }

    public String getMaximumPrice() {
        return MaximumPrice;
    }

    public void setMaximumPrice(String maximumPrice) {
        MaximumPrice = maximumPrice;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public Integer getNumberOfAdults() {
        return NumberOfAdults;
    }

    public void setNumberOfAdults(Integer numberOfAdults) {
        NumberOfAdults = numberOfAdults;
    }

    public Integer getNumberOfKids() {
        return NumberOfKids;
    }

    public void setNumberOfKids(Integer numberOfKids) {
        NumberOfKids = numberOfKids;
    }

    public String getSpecialRequirements() {
        return SpecialRequirements;
    }

    public void setSpecialRequirements(String specialRequirements) {
        SpecialRequirements = specialRequirements;
    }
}
