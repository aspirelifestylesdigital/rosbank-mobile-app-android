package com.rosbank.android.russia.dcr.widgets;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.activitys.HomeActivity;
import com.rosbank.android.russia.dcr.adapter.DCRGalleryAdapter;
import com.rosbank.android.russia.dcr.model.DCRImageObject;
import com.rosbank.android.russia.utils.CommonUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/*
 * Created by anh.trinh on 10/20/2016.
 */
public class GalleryView extends RelativeLayout {
    private static final int LAYOUT_RESOURCE_ID = R.layout.gallery_view;

    @BindView(R.id.pager)
    ViewPager viewPager;


    @BindView(R.id.btn_left)
    FrameLayout frameBtnLeft;

    @BindView(R.id.btn_right)
    FrameLayout frameBtnRight;

    private List<DCRImageObject> listObj;
    private int heightTabLayout = 0;
    /**
     * @param context
     *         The context which view is running on.
     * @return New GalleryView object.
     * @since v0.1
     */
    public static GalleryView newInstance(final Context context) {
        if (context == null) {
            return null;
        }

        return new GalleryView(context);
    }

    /**
     * The constructor with current context.
     *
     * @param context
     *         The context which view is running on.
     * @since v0.1
     */
    public GalleryView(final Context context) {
        super(context);
        this.initialize();
    }

    /**
     * The constructor with current context.
     *
     * @param context
     *         The context which view is running on.
     * @param attrs
     *         The initialize attributes set.
     * @since v0.1
     */
    public GalleryView(final Context context,
                            final AttributeSet attrs) {
        super(context,
              attrs);
        this.initialize();
    }

    /**
     * The constructor with current context.
     *
     * @param context
     *         The context which view is running on.
     * @param attrs
     *         The initialize attributes set.
     * @param defStyleAttr
     *         The default style.
     * @since v0.1
     */
    public GalleryView(final Context context,
                            final AttributeSet attrs,
                            final int defStyleAttr) {
        super(context,
              attrs,
              defStyleAttr);

        this.initialize();
    }

    /**
     * @see View#setEnabled(boolean)
     * @since v0.1
     */
    @Override
    public void setEnabled(final boolean isEnabled) {
        this.setAlpha(isEnabled ?
                      1f :
                      0.5f);
        super.setEnabled(isEnabled);
    }

    /**
     * Reset injected resources created by ButterKnife.
     *
     * @since v0.1
     */
    public void resetInjectedResource() {
        ButterKnife.bind(this);
    }

    /**
     * Initialize UI sub views.
     *
     * @since v0.1
     */
    private void initialize() {

        final LayoutInflater layoutInflater = LayoutInflater.from(this.getContext());
        layoutInflater.inflate(LAYOUT_RESOURCE_ID,
                               this,
                               true);

        ButterKnife.bind(this,
                         this);

    }
    public void setData(List<DCRImageObject> data){
        this.listObj = data;
        resetData();

    }

    @OnClick(R.id.btn_right)
    public void eventBtnRight() {
        viewPager.setCurrentItem(getItem(+1), true);
    }

    @OnClick(R.id.btn_left)
    public void eventBtnLeft() {
        viewPager.setCurrentItem(getItem(-1), true);
    }

    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }

    private void resetData(){
     //   int height = CommonUtils.getScreenHeight((HomeActivity)getContext()) - heightTabLayout;
        int height = (int) getResources().getDimension(R.dimen.img_height_concierge_landing_item);
        viewPager.getLayoutParams().height = height;
        viewPager.requestLayout();

        DCRGalleryAdapter adapter = new DCRGalleryAdapter((HomeActivity)getContext(), listObj,
                                                          CommonUtils.getScreenWidth((HomeActivity)getContext()), height);
        frameBtnLeft.setVisibility(INVISIBLE);
        if(listObj ==null || listObj.size()==1){
            frameBtnRight.setVisibility(INVISIBLE);
        }

        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                if(position==0){
                    frameBtnLeft.setVisibility(INVISIBLE);
                }else{
                    frameBtnLeft.setVisibility(VISIBLE);
                }
                if(listObj.size()>1 ) {
                    if (position == listObj.size() - 1) {
                        frameBtnRight.setVisibility(INVISIBLE);
                    } else {
                        frameBtnRight.setVisibility(VISIBLE);
                    }
                }

            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int arg0) {

            }
        });

    }


    public void setHeightTabLayout(int heightTabLayout) {
        this.heightTabLayout = heightTabLayout;
    }
}