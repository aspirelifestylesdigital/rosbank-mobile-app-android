package com.rosbank.android.russia.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;


public class CommonObject
        implements Parcelable {
    public CommonObject(String value){
        Value = value;
    }
    @Expose
    private String Key;

    public String getKey() {
        return Key;
    }

    public void setKey(final String key) {
        Key = key;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(final String value) {
        Value = value;
    }

    @Expose
    private String Value;

    @Override
    public boolean equals(Object obj) {
        if(obj != null && obj instanceof CommonObject){
            CommonObject commonObject = (CommonObject)obj;
            return (Value != null && Value.equals(commonObject.getValue())) ||
                    (Key != null && Key.equals(commonObject.getKey()));
        }
        return false;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel parcel,
                              final int i) {
        parcel.writeString(this.Key);
        parcel.writeString(this.Value);

    }
    protected CommonObject(Parcel in) {
        this.Key = in.readString();
        this.Value = in.readString();
    }
    public CommonObject() {
        this.Key = "";
        this.Value = "";
    }

    public static final Creator<CommonObject> CREATOR = new Creator<CommonObject>() {
        @Override
        public CommonObject createFromParcel(Parcel source) {
            return new CommonObject(source);
        }

        @Override
        public CommonObject[] newArray(int size) {
            return new CommonObject[size];
        }
    };
}
