package com.rosbank.android.russia.apiservices;

import com.rosbank.android.russia.apiservices.RequestModel.SaveMyCurrencyRequest;
import com.rosbank.android.russia.apiservices.ResponseModel.SaveMyCurrencyResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.ApiProviderClient;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Callback;


public class WSSaveMyCurrency
        extends ApiProviderClient<SaveMyCurrencyResponse> {

    private SaveMyCurrencyRequest saveMyCurrencyRequest;

    public WSSaveMyCurrency(){

    }
    public void saveMyCurrency(final SaveMyCurrencyRequest request){
        this.saveMyCurrencyRequest = request;


    }




    @Override
    public void run(Callback<SaveMyCurrencyResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }

    @Override
    protected boolean isUseAuthenticateInApi() {
        return true;
    }

    @Override
    public void runApi() {

        serviceInterface.saveMyCurrency(saveMyCurrencyRequest).enqueue(callback);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("User-Agent", "Retrofit-Sample-App");
        return headers;
    }

}
