package com.rosbank.android.russia.processing.cancelrequest;

import android.text.TextUtils;
import android.view.View;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.apiservices.ResponseModel.OtherBookingDetailResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.rosbank.android.russia.apiservices.b2c.B2CWSGetRequestDetail;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.B2CUpsertConciergeRequestRequest;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CGetRecentRequestResponse;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.application.AppContext;
import com.rosbank.android.russia.model.concierge.OtherBookingDetailData;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.DateTimeUtil;
import com.rosbank.android.russia.utils.SharedPreferencesUtils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by ThuNguyen on 10/26/2016.
 */

public class OtherCancelRequestDetail extends BaseCancelRequestDetail{
    private OtherBookingDetailData otherBookingDetailData;
    public OtherCancelRequestDetail() {
    }

    public OtherCancelRequestDetail(View view) {
        super(view);
    }

    @Override
    public void getRequestDetail() {
        showProgressDialog();
        B2CWSGetRequestDetail b2CWSGetRequestDetail = new B2CWSGetRequestDetail(this);
        b2CWSGetRequestDetail.setValue(myRequestObject.getEpcCaseId(), myRequestObject.getBookingItemID());
        b2CWSGetRequestDetail.run(null);
        /*WSGetOtherBookingDetail wsGetOtherBookingDetail = new WSGetOtherBookingDetail();
        wsGetOtherBookingDetail.setBookingId(myRequestObject.getBookingId());
        wsGetOtherBookingDetail.run(this);
        showProgressDialog();*/
    }

    @Override
    public B2CUpsertConciergeRequestRequest getB2CUpsertConciergeRequestRequest() {
        B2CUpsertConciergeRequestRequest  upsertConciergeRequestRequest = new B2CUpsertConciergeRequestRequest();
        upsertConciergeRequestRequest.setFunctionality(AppConstant.CONCIERGE_FUNCTIONALITY_TYPE.OTHER.getValue());
        upsertConciergeRequestRequest.setRequestType(AppConstant.CONCIERGE_REQUEST_TYPE.OTHER_REQUEST.getValue());
        upsertConciergeRequestRequest.setEmail1((SharedPreferencesUtils.getPreferences(AppConstant.USER_EMAIL_PRE, "")));
        upsertConciergeRequestRequest.setSituation("other");
        if(!TextUtils.isEmpty(otherBookingDetailData.getMobileNumber())) {
            upsertConciergeRequestRequest.setPhoneNumber(otherBookingDetailData.getMobileNumber());
        }
        upsertConciergeRequestRequest.setPrefResponse(otherBookingDetailData.getPrefResponse());

        if(myRequestObject != null){
            upsertConciergeRequestRequest.setTransactionID(myRequestObject.getBookingItemID());
        }
        // Request details
        upsertConciergeRequestRequest.setRequestDetails(otherBookingDetailData.getRequestDetaiString());
        return upsertConciergeRequestRequest;
    }

    @Override
    protected void updateUI() {
        super.updateUI();
        long epochCreatedDate = otherBookingDetailData.getCreateDateEpoc();
        tvRequestedDate.setText(DateTimeUtil.shareInstance().formatTimeStamp(epochCreatedDate, "dd MMMM yyyy"));
        tvRequestedTime.setText(DateTimeUtil.shareInstance().formatTimeStamp(epochCreatedDate, "hh:mm aa"));
        String spaName = otherBookingDetailData.getSpaName();
        // Request type
       // tvRequestType.setText("OTHER REQUEST");
        if(CommonUtils.isStringValid(spaName)){
            tvRequestType.setText(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_title_spa_request));
            // Request name
            if(!TextUtils.isEmpty(myRequestObject.getItemTitle()) && !myRequestObject.getItemTitle().equalsIgnoreCase("null")){
                tvRequestName.setText(myRequestObject.getItemTitle());
            }else {
                tvRequestName.setText(otherBookingDetailData.getWhatCanWeDo());
            }

            // Request detail header
            tvRequestDetailHeader.setText(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_spa_request_detail));

            if(otherBookingDetailData != null){
                // Case Id
                tvCaseId.setText(otherBookingDetailData.getBookingItemID());

                // Other request
                addRequestDetailItem(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_title_spa_request), otherBookingDetailData.getWhatCanWeDo());
            }

        }else{
            tvRequestType.setText(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_title_other_request));
            // Request name
            /*if(!TextUtils.isEmpty(myRequestObject.getItemTitle()) && !myRequestObject.getItemTitle().equalsIgnoreCase("null")){
                tvRequestName.setText(myRequestObject.getItemTitle());
            }else {*/
                tvRequestName.setText(otherBookingDetailData.getWhatCanWeDo());
            //}

            // Request detail header
            tvRequestDetailHeader.setText(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_other_request_detail));

            if(otherBookingDetailData != null){
                // Case Id
                tvCaseId.setText(otherBookingDetailData.getBookingItemID());

                // Other request
                addRequestDetailItem(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_other_request), otherBookingDetailData.getWhatCanWeDo());
            }
        }
    }
    @Override
    public void onResponse(Call call, Response response) {
        hideProgressDialog();
        if(response != null && response.body() instanceof OtherBookingDetailResponse){
            otherBookingDetailData = ((OtherBookingDetailResponse)response.body()).getData();
            updateUI();
        }
    }

    @Override
    public void onFailure(Call call, Throwable t) {
        hideProgressDialog();
    }

    @Override
    public void onB2CResponse(final B2CBaseResponse response) {
        hideProgressDialog();
        if(response instanceof B2CGetRecentRequestResponse){
            otherBookingDetailData = new OtherBookingDetailData((B2CGetRecentRequestResponse)response);
            updateUI();
        }
    }

    @Override
    public void onB2CResponseOnList(final List<B2CBaseResponse> responseList) {

    }

    @Override
    public void onB2CFailure(final String errorMessage,
                             final String errorCode) {
        hideProgressDialog();
    }
}
