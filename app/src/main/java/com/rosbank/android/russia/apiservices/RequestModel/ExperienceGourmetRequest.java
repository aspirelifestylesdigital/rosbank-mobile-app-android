package com.rosbank.android.russia.apiservices.RequestModel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

import java.util.List;

public class ExperienceGourmetRequest extends BaseRequest implements Parcelable {
    @Expose
    private String UserID;
    @Expose
    private int Page;
    @Expose
    private int RecordPerPage;
    @Expose
    private String Zipcode;
    @Expose
    private List<String> PriceRange;
    @Expose
    private List<String> Cuisines;
    @Expose
    private String Rating;
    @Expose
    private List<String> Occasions;

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public int getPage() {
        return Page;
    }

    public void setPage(int page) {
        Page = page;
    }

    public int getRecordPerPage() {
        return RecordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        RecordPerPage = recordPerPage;
    }

    public String getZipcode() {
        return Zipcode;
    }

    public void setZipcode(String zipcode) {
        Zipcode = zipcode;
    }

    public String getRating() {
        return Rating;
    }

    public void setRating(String rating) {
        Rating = rating;
    }

    public List<String> getPriceRange() {
        return PriceRange;
    }

    public void setPriceRange(List<String> priceRange) {
        PriceRange = priceRange;
    }

    public List<String> getCuisines() {
        return Cuisines;
    }

    public void setCuisines(List<String> cuisines) {
        Cuisines = cuisines;
    }

    public List<String> getOccasions() {
        return Occasions;
    }

    public void setOccasions(List<String> occasions) {
        Occasions = occasions;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.UserID);
        dest.writeInt(this.Page);
        dest.writeInt(this.RecordPerPage);
        dest.writeString(this.Zipcode);
        dest.writeStringList(this.PriceRange);
        dest.writeStringList(this.Cuisines);
        dest.writeString(this.Rating);
        dest.writeStringList(this.Occasions);
    }

    public ExperienceGourmetRequest() {
    }

    protected ExperienceGourmetRequest(Parcel in) {
        this.UserID = in.readString();
        this.Page = in.readInt();
        this.RecordPerPage = in.readInt();
        this.Zipcode = in.readString();
        this.PriceRange = in.createStringArrayList();
        this.Cuisines = in.createStringArrayList();
        this.Rating = in.readString();
        this.Occasions = in.createStringArrayList();
    }

    public static final Parcelable.Creator<ExperienceGourmetRequest> CREATOR = new Parcelable.Creator<ExperienceGourmetRequest>() {
        @Override
        public ExperienceGourmetRequest createFromParcel(Parcel source) {
            return new ExperienceGourmetRequest(source);
        }

        @Override
        public ExperienceGourmetRequest[] newArray(int size) {
            return new ExperienceGourmetRequest[size];
        }
    };
}