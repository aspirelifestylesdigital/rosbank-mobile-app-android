package com.rosbank.android.russia.fragment;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.activitys.HomeActivity;
import com.rosbank.android.russia.adapter.TabMyRequestAdapter;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.FontUtils;

import butterknife.BindView;

/**
 * Created by anh.trinh on 9/13/2016.
 */
public class MyRequestsFragment
        extends BaseFragment {
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.view_pager)
    ViewPager mViewPager;
    private TabMyRequestAdapter mAdapter;
    @Override
    protected int layoutId() {
        return R.layout.fragment_my_request;
    }

    @Override
    protected void initView() {
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.text_my_request_tab1)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.text_my_request_tab2)));
        CommonUtils.setFontForViewRecursive(tabLayout,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        mAdapter = new TabMyRequestAdapter(getContext(),
                                              getChildFragmentManager());

        mViewPager.setAdapter(mAdapter);
        mViewPager.setOffscreenPageLimit(2);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab LayoutTab) {
                mViewPager.setCurrentItem(LayoutTab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab LayoutTab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab LayoutTab) {

            }
        });

    }

    @Override
    protected void bindData() {

    }
    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showLogoApp(false);
            ((HomeActivity) getActivity()).setTitle(getString(R.string.text_title_my_requests));
        }
    }

    @Override
    public boolean onBack() {
        return false;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        int currentItemPos = mViewPager.getCurrentItem();
        mAdapter.getItem(currentItemPos).onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        int currentItemPos = mViewPager.getCurrentItem();
        mAdapter.getItem(currentItemPos).onActivityResult(requestCode, resultCode, data);
    }


}
