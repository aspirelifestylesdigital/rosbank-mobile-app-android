package com.rosbank.android.russia.fragment;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.FontUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.rosbank.android.russia.application.AppConstant.CLIENT_CODE_REGULAR;
import static com.rosbank.android.russia.fragment.ForgotPasswordFragment.CLIENT_CODE;

/**
 * Created by anh.trinh on 9/13/2016.
 */
public class ForgotPasswordSuccessFragment
        extends BaseFragment {

    @BindView(R.id.ic_back)
    ImageView mIcBack;
    @BindView(R.id.tv_tool_bar_title)
    TextView mTvToolBarTitle;
    @BindView(R.id.img_mc_logo)
    ImageView mImgMcLogo;
    @BindView(R.id.tv_app_name)
    TextView mTvAppName;
    @BindView(R.id.tv_success_message)
    TextView mTvSuccessMessage;
    @BindView(R.id.tv_message_great)
    TextView mTvGreatMessage;
    @BindView(R.id.btn_check_mail)
    Button mBtnCheckMail;
    @BindView(R.id.container)
    LinearLayout mContainer;

    @Override
    protected int layoutId() {
        return R.layout.fragment_forgot_password_success;
    }

    @Override
    protected void initView() {
        mTvToolBarTitle.setText(R.string.text_title_forgot_pass);
        CommonUtils.setFontForViewRecursive(mContainer,
                FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
        CommonUtils.setFontForViewRecursive(mBtnCheckMail,
                FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        CommonUtils.setFontForViewRecursive(mTvAppName,
                FontUtils.FONT_FILE_NAME_GOTHAM_ROUND_MEDIUM);
        CommonUtils.setFontForViewRecursive(mTvToolBarTitle,
                FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);

        if (getArguments() != null && !getArguments().isEmpty()) {
            mImgMcLogo.setImageDrawable(getArguments().getString(CLIENT_CODE, CLIENT_CODE_REGULAR).equalsIgnoreCase(CLIENT_CODE_REGULAR) ?
                    ContextCompat.getDrawable(getActivity(), R.drawable.rosbank_logo_2) :
                    ContextCompat.getDrawable(getActivity(), R.drawable.hermitage_logo_2)
            );
        }
    }

    @Override
    protected void bindData() {

    }

    @Override
    public boolean onBack() {
        return false;
    }


    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater,
                container,
                savedInstanceState);
        ButterKnife.bind(this,
                rootView);
        return rootView;
    }

    @OnClick({R.id.ic_back, R.id.btn_check_mail})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ic_back:

                onBackPress();
                break;
            case R.id.btn_check_mail:
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_APP_EMAIL);
                startActivity(Intent.createChooser(intent, ""));

                break;
        }
    }

}
