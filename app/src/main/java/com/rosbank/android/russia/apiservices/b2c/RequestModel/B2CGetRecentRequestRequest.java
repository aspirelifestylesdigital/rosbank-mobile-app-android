package com.rosbank.android.russia.apiservices.b2c.RequestModel;

import com.rosbank.android.russia.BuildConfig;
import com.rosbank.android.russia.apiservices.RequestModel.BaseRequest;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.model.UserItem;
import com.rosbank.android.russia.utils.SharedPreferencesUtils;
import com.google.gson.annotations.Expose;

/**
 * Created by ThuNguyen on 10/26/2016.
 */

public class B2CGetRecentRequestRequest extends BaseRequest{
    public static final int PER_PAGE = 10;
    @Expose
    private String AccessToken;
    @Expose
    private String ConsumerKey;
    @Expose
    private String Functionality;
    @Expose
    private String OnlineMemberId;
    @Expose
    private String RowEnd;
    @Expose
    private String RowStart;

    public B2CGetRecentRequestRequest(){
        AccessToken = SharedPreferencesUtils.getPreferences(AppConstant.PRE_B2C_ACCESS_TOKEN, "");
        //ConsumerKey = BuildConfig.B2C_CONSUMER_KEY;
        ConsumerKey = UserItem.isVip() ? BuildConfig.B2C_CONSUMER_KEY_PRIVATE : BuildConfig.B2C_CONSUMER_KEY_REGULAR;
        Functionality = "GetRecentRequests";
        OnlineMemberId = SharedPreferencesUtils.getPreferences(AppConstant.PRE_B2C_ONLINE_MEMBER_ID, "");
    }
    public B2CGetRecentRequestRequest build(int page){
        int startRow = (page - 1) * PER_PAGE + 1;
        int endRow = startRow + PER_PAGE - 1;
        RowStart = String.valueOf(startRow);
        RowEnd = String.valueOf(endRow);
        return this;
    }

    public void setAccessToken(String accessToken) {
        AccessToken = accessToken;
    }
}
