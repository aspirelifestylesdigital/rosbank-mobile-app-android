package com.rosbank.android.russia.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.activitys.HomeActivity;
import com.rosbank.android.russia.adapter.SelectionRentalAdapter;
import com.rosbank.android.russia.apiservices.ResponseModel.RentalResponse;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.model.RentalObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by anh.trinh on 9/13/2016.
 */
public class SelectionRentalFragment
        extends BaseFragment implements Callback {


    @Override
    public void onResponse(final Call call,
                           final Response response) {
        if (response.body() instanceof RentalResponse) {
            hideDialogProgress();
            if(getActivity()==null) {
                return;
            }

            RentalResponse rentalResponse = ((RentalResponse) response.body());
            int code = rentalResponse.getStatus();
            if (code == 200) {
                data.clear();
                data.addAll(rentalResponse.getData());
                adapter = new SelectionRentalAdapter(getActivity(),
                                                     data,
                                                     dataSelected);
                mListview.setAdapter(adapter);
            }else{
//                showToast(rentalResponse.getMessage());
            }
            }
    }

    @Override
    public void onFailure(final Call call,
                          final Throwable t) {
        hideDialogProgress();
        if(getActivity()==null) {
            return;
        }

//        showToast(getString(R.string.text_server_error_message));
      /*  Bundle bundle = new Bundle();
        bundle.putString(ErrorHomeFragment.PRE_ERROR_MESSAGE,
                         getString(R.string.text_server_error_message));
        ErrorHomeFragment fragment = new ErrorHomeFragment();
        fragment.setArguments(bundle);
        pushFragment(fragment,
                     true,
                     true);*/

    }

    public interface SelectionCallback {
        void onFinishSelection(Bundle bundle);
    }

    public static final String PRE_SELECTION_DATA = "selection_data";
    public static final String PRE_SELECTION_TYPE = "selection_type";
    public static final String PRE_SELECTION_VERHICLE_TYPE = "VERHICLE TYPE";
    public static final String PRE_SELECTION_RENTAL_CONUNTRY = "RENTAL COMPANY";
    public static final String PRE_SELECTION_TITLE = "Title";
    @BindView(R.id.listview)
    ListView mListview;
    String typeSelection;
    ArrayList<RentalObject> data = new ArrayList<RentalObject>();
    SelectionRentalAdapter adapter;
    ArrayList<RentalObject> dataSelected = new ArrayList<RentalObject>();
    String title;
    SelectionCallback selectionCallback;
    public void setSelectionCallBack(SelectionCallback selectionCallBack) {
        this.selectionCallback = selectionCallBack;
    }


    @Override
    protected int layoutId() {
        return R.layout.fragment_selection_rental;
    }

    @Override
    protected void initView() {

        if (getArguments() != null) {
            title = getArguments().getString(PRE_SELECTION_TITLE, "");
            typeSelection = getArguments().getString(PRE_SELECTION_TYPE,
                                                     "");
            dataSelected = getArguments().getParcelableArrayList(PRE_SELECTION_DATA);
            if(dataSelected == null){
                dataSelected = new ArrayList<>();
            }
            String[] resource;
            if (typeSelection == PRE_SELECTION_VERHICLE_TYPE) {
                /*if (getActivity() instanceof HomeActivity) {
                    ((HomeActivity) getActivity()).setTitle(getString(R.string.text_title_vehicle_type));

                }*/
                /*showDialogProgress();
                WSGetRentalVehicleTypePreferences wsGetRentalVehicleTypePreferences = new WSGetRentalVehicleTypePreferences();
                wsGetRentalVehicleTypePreferences.run(this);*/
                data.clear();
                data.addAll(getCarRentalList());
                adapter = new SelectionRentalAdapter(getActivity(),
                        data,
                        dataSelected);
                mListview.setAdapter(adapter);

            } else {
                if (typeSelection == PRE_SELECTION_RENTAL_CONUNTRY) {
                    /*if (getActivity() instanceof HomeActivity) {
                        ((HomeActivity) getActivity()).setTitle(getString(R.string.text_title_rental_company));

                    }*/
                    /*showDialogProgress();
                    WSGetRentalCompanyPreferences wsGetRentalCompanyPreferences = new WSGetRentalCompanyPreferences();
                    wsGetRentalCompanyPreferences.run(this);*/
                    data.clear();
                    data.addAll(getCarCompanyList());
                    adapter = new SelectionRentalAdapter(getActivity(),
                            data,
                            dataSelected);
                    mListview.setAdapter(adapter);
                }
            }

        }

    }

    @Override
    protected void bindData() {

    }

    @Override
    public boolean onBack() {
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle(getString(R.string.text_title_preference));
        if(!TextUtils.isEmpty(title)){
            setTitle(title);
        }
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showLogoApp(false);
            ((HomeActivity) getActivity()).showRightTextTitle(true,
                                                              new View.OnClickListener() {
                                                                  @Override
                                                                  public void onClick(final View view) {

                                                                      if (selectionCallback != null) {
                                                                          Bundle bundle = new Bundle();
                                                                          bundle.putString(PRE_SELECTION_TYPE,
                                                                                           typeSelection);
                                                                          bundle.putParcelableArrayList(PRE_SELECTION_DATA,adapter.getSelectedData());
                                                                          selectionCallback.onFinishSelection(bundle);
                                                                      }
                                                                      onBackPress();
                                                                  }
                                                              });


        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater,
                                           container,
                                           savedInstanceState);
        ButterKnife.bind(this,
                         rootView);
        return rootView;
    }

    @Override
    public void onPause() {
        super.onPause();
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showToolbarMenuIcon();
            ((HomeActivity) getActivity()).hideRightText();


        }
    }
    private List<RentalObject> getCarRentalList(){
        List<RentalObject> carRentalObjList = new ArrayList<>();
        String[]carRentalArr = getResources().getStringArray(R.array.preferred_vehicle_array);
        for(int index = 0; index < carRentalArr.length; index++){
            RentalObject rentalObject = new RentalObject();
            rentalObject.setKey(String.valueOf(index));
            rentalObject.setValue(carRentalArr[index]);
            carRentalObjList.add(rentalObject);
        }
        return carRentalObjList;
    }
    private List<RentalObject> getCarCompanyList(){
        List<RentalObject> companyObjList = new ArrayList<>();
        String[]companyArr = getResources().getStringArray(R.array.preferred_rental_company);
        for(int index = 0; index < companyArr.length; index++){
            RentalObject rentalObject = new RentalObject();
            rentalObject.setKey(String.valueOf(index));
            rentalObject.setValue(companyArr[index]);
            companyObjList.add(rentalObject);
        }
        return companyObjList;
    }
}
