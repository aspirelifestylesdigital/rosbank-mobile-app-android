package com.rosbank.android.russia.fragment;

import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.activitys.HomeActivity;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;

import butterknife.BindView;

/*
 * Created by chau.nguyen on 10/05/2016.
 */
public class LiveChatFragment
        extends BaseFragment {


    public static final String AboutWebTitle = "webTitle";
    public static final String AboutWebLink = "webLink";

    @BindView(R.id.webView_about_common)
    WebView aboutWebView;

    String titleName = "", linkWeb = "";


    public LiveChatFragment() {

    }

    @Override
    protected int layoutId() {
        return R.layout.fragment_about_common_web;
    }

    @Override
    protected void initView() {
        aboutWebView.getSettings()
                    .setJavaScriptEnabled(true);
    }

    @Override
    protected void bindData() {
        titleName = getArguments().getString(AboutWebTitle,
                                             "");
        linkWeb = getArguments().getString(AboutWebLink,
                                           "");

            aboutWebView.loadUrl(linkWeb);
            aboutWebView.setWebViewClient(new WebViewClient() {
                @Override
                public void onPageStarted(WebView view,
                                          String url,
                                          Bitmap favicon) {
                    showDialogProgress();
                    super.onPageStarted(view,
                                        url,
                                        favicon);
                }

                @Override
                public void onPageFinished(WebView view,
                                           String url) {
                    hideDialogProgress();
                    super.onPageFinished(view,
                                         url);
                }
            });

    }



    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showLogoApp(false);
            ((HomeActivity) getActivity()).setTitle(titleName);
            ((HomeActivity) getActivity()).hideFloatingActionButton();

        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showFloatingActionButton();

        }
    }

    @Override
    public void onBackPress() {
        if (aboutWebView.canGoBack()) {
            aboutWebView.goBack();
//            Log.e("URL Back",aboutWebView.getUrl());
        } else {
            onBackPress();
        }

    }

    @Override
    public boolean onBack() {
        if (aboutWebView.canGoBack()) {
            return true;
        } else {
            return false;
        }

    }



}

