package com.rosbank.android.russia.apiservices.RequestModel;


import com.google.gson.annotations.Expose;

public class GetMyProfileRequest
        extends BaseRequest {
    @Expose
    private String UserID;

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

}
