package com.rosbank.android.russia.apiservices.RequestModel;


import com.rosbank.android.russia.model.LoyaltyObject;
import com.google.gson.annotations.Expose;

import java.util.ArrayList;

public class UpdateHotelPreferencesRequest
        extends BaseRequest {
    @Expose
    private String UserID;
    @Expose
    private ArrayList<String> RoomTypes;
    @Expose
    private ArrayList<String> BedTypes;
    @Expose
    private String Rating;
    @Expose
    private String SmokingRoom;
    @Expose
    private ArrayList<LoyaltyObject> LoyaltyPrograms;

    public String getUserID() {
        return UserID;
    }

    public void setUserID(final String userID) {
        UserID = userID;
    }

    public ArrayList<String> getRoomTypes() {
        return RoomTypes;
    }

    public void setRoomTypes(final ArrayList<String> roomTypes) {
        RoomTypes = roomTypes;
    }

    public ArrayList<String> getBedTypes() {
        return BedTypes;
    }

    public void setBedTypes(final ArrayList<String> bedTypes) {
        BedTypes = bedTypes;
    }

    public ArrayList<LoyaltyObject> getLoyaltyPrograms() {
        return LoyaltyPrograms;
    }

    public void setLoyaltyPrograms(final ArrayList<LoyaltyObject> loyaltyPrograms) {
        LoyaltyPrograms = loyaltyPrograms;
    }



    public void setRating(final String rating) {
        Rating = rating;
    }



    public String getSmokingRoom() {
        return SmokingRoom;
    }

    public void setSmokingRoom(final String smokingRoom) {
        SmokingRoom = smokingRoom;
    }

    public String getRating() {
        return Rating;
    }



}
