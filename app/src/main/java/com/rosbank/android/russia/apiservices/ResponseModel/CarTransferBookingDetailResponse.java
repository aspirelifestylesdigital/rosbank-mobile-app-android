package com.rosbank.android.russia.apiservices.ResponseModel;

import com.rosbank.android.russia.apiservices.apis.appimplement.BaseResponse;
import com.rosbank.android.russia.model.concierge.CarBookingDetailData;

/**
 * Created by ThuNguyen on 10/26/2016.
 */

public class CarTransferBookingDetailResponse extends BaseResponse {
    private CarBookingDetailData Data;

    public CarBookingDetailData getData() {
        return Data;
    }

    public void setData(CarBookingDetailData data) {
        Data = data;
    }
}
