package com.rosbank.android.russia.fragment;

import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.apiservices.ResponseModel.UpdateGourmetResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CICallback;
import com.rosbank.android.russia.apiservices.b2c.B2CWSGetPreference;
import com.rosbank.android.russia.apiservices.b2c.B2CWSUpsertPreferenceRequest;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.B2CUpsertPreferenceRequest;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.preference.PreferenceDetailDiningRequest;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.preference.PreferenceDetailRequest;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.model.KeyValueObject;
import com.rosbank.android.russia.model.preference.DiningPreferenceDetailData;
import com.rosbank.android.russia.model.preference.PreferenceData;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.EntranceLock;
import com.rosbank.android.russia.utils.FontUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nga.nguyent on 9/27/2016.
 */
public class MyPreferencesGourmetFragment
        extends BaseFragment
        implements SelectDataFragment.OnSelectDataListener,
                   Callback {

    @BindView(R.id.tvTitle)
    protected TextView tvTitle;
    @BindView(R.id.tvCuisinePre)
    protected TextView tvCuisinePre;
    @BindView(R.id.tvCuisineChoosed)
    protected TextView tvCuisineChoosed;
    @BindView(R.id.tvOtherCuisine)
    protected TextView tvOtherCuisine;
    @BindView(R.id.edtOtherCuisine)
    protected EditText edtOtherCuisine;
    @BindView(R.id.tvFoodAllergyTitle)
    protected TextView tvFoodAllergy;
    @BindView(R.id.edtFoodAllergy)
    protected EditText edtFoodAllergy;
    @BindView(R.id.btnSave)
    Button btnSave;
    @BindView(R.id.btnCancel)
    Button btnCancel;
    @BindView(R.id.container)
    LinearLayout mContainer;
    List<KeyValueObject> cuisinePreferences;

    DiningPreferenceDetailData diningPreferenceDetailData;
    EntranceLock entranceLock = new EntranceLock();

    @Override
    protected int layoutId() {
        return R.layout.fragment_preference_gourmet;
    }

    @Override
    protected void initView() {
        ButterKnife.bind(this,
                         view);

        CommonUtils.setFontForViewRecursive(mContainer,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
        CommonUtils.setFontForViewRecursive(tvTitle,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        CommonUtils.setFontForViewRecursive(btnSave,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        CommonUtils.setFontForViewRecursive(btnCancel,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);

        showDialogProgress();
        B2CWSGetPreference b2CWSGetPreference = new B2CWSGetPreference(getPreferenceCallback);
        b2CWSGetPreference.run(null);
    }

    private void updateUI() {
        if (getActivity() == null) {
            return;
        }
        cuisinePreferences = new ArrayList<>();
        if (diningPreferenceDetailData != null) {
            if (!TextUtils.isEmpty(diningPreferenceDetailData.getCuisinePreference())) {
                String[] selectedCuisineArr = diningPreferenceDetailData.getCuisinePreference()
                                                                        .split("\\s*,\\s*");

                if (selectedCuisineArr != null && selectedCuisineArr.length > 0) {

                    for (String selectedCuisine : selectedCuisineArr) {
                        String cussineRu = CommonUtils.getRusianCussineString(selectedCuisine);
                        if (CommonUtils.isStringValid(cussineRu)) {
                            if (!diningPreferenceDetailData.getCuisinePreference()
                                                           .contains(cussineRu)) {
                                KeyValueObject keyValueObject = new KeyValueObject(cussineRu,
                                                                                   cussineRu);
                                cuisinePreferences.add(keyValueObject);
                            }
                        } else {
                            KeyValueObject keyValueObject = new KeyValueObject(selectedCuisine,
                                                                               selectedCuisine);
                            cuisinePreferences.add(keyValueObject);
                        }
                    }
                }
            }

        }
        if(cuisinePreferences== null|| cuisinePreferences.size()==0){
            KeyValueObject keyValueObject = new KeyValueObject("Русская",
                                                               "Русская");
            cuisinePreferences.add(keyValueObject);
        }
        updateCuisinePreferences();
    }

    @Override
    protected void bindData() {
        updateCuisinePreferences();
    }

    @Override
    protected boolean onBack() {
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        showLogoApp(false);
        setTitle(getString(R.string.title_mypreference));
        hideRightText();
        showToolbarMenuIcon();
    }

    @OnClick({R.id.tvCuisineChoosed,
              R.id.edtOtherCuisine,
              R.id.btnSave,
              R.id.btnCancel})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvCuisineChoosed:
                //showToast("Cuisine");
                SelectDataFragment cuisine = new SelectDataFragment();
                cuisine.setOnSelectListener(this);
                if (cuisinePreferences != null) {
                    cuisine.setCuisineSelected(cuisinePreferences);
                }
                cuisine.setHeaderTitle(getString(R.string.cuisine_preferences));
                pushFragment(cuisine,
                             true,
                             true);
                break;
            case R.id.edtOtherCuisine:
                //showToast("Other");
                break;
            case R.id.btnSave:
                //showToast("Save");
                CommonUtils.hideSoftKeyboard(getActivity());
                if (!entranceLock.isClickContinuous()) {
                    saveGourmet();
                }
                break;
            case R.id.btnCancel:
                //showToast("Cancel");
                if (!entranceLock.isClickContinuous()) {
                    onBackPress();
                }
                break;
        }
    }

    @Override
    public void onSelectedData(SelectDataFragment.APIDATA APIDATA,
                               List<KeyValueObject> list) {
        cuisinePreferences = list;
        updateCuisinePreferences();
    }

    private void updateCuisinePreferences() {
        String text = "";
        if (cuisinePreferences != null) {
            for (int i = 0; i < cuisinePreferences.size(); i++) {
                if (text.length() > 0) {
                    text += ", ";
                }
                text += cuisinePreferences.get(i)
                                          .getValue();
            }
        }

        if (text.length() == 0) {
            text = getString(R.string.hint_choose_cuisine);
            tvCuisineChoosed.setTextColor(ContextCompat.getColor(getContext(),
                                                                 R.color.hint_color));
        } else {
            tvCuisineChoosed.setTextColor(ContextCompat.getColor(getContext(),
                                                                 R.color.text_color_active));
        }

        tvCuisineChoosed.setText(text);
        edtOtherCuisine.setText(diningPreferenceDetailData != null ?
                                diningPreferenceDetailData.getOtherPreference() :
                                "");
        edtFoodAllergy.setText(diningPreferenceDetailData != null ?
                               diningPreferenceDetailData.getFoodAllergies() :
                               "");
    }

    private void saveGourmet() {

        /*if(!UserItem.isLogined())
        {
            showToast(getString(R.string.text_message_need_login_update_gourmet));
            return;
        }

        showDialogProgress();

        String textOther = edtOtherContent.getText().toString();
        List<String>ids = getCuisineIds();
        String userId = UserItem.getLoginedId();

        UpdategGourmetRequest request = new UpdategGourmetRequest();
        if(!StringUtil.isEmpty(textOther))
            request.setOtherCuisine(textOther);
        if(ids.size()>0)
            request.setCuisines(ids);
        request.setUserID(userId);

        WSUpdateGourmetPreference ws = new WSUpdateGourmetPreference();
        ws.setRequest(request);
        ws.run(this);*/
        AppConstant.PREFERENCE_EDIT_TYPE preferenceEditType = diningPreferenceDetailData != null ?
                                                              AppConstant.PREFERENCE_EDIT_TYPE.UPDATE :
                                                              AppConstant.PREFERENCE_EDIT_TYPE.ADD;
        PreferenceDetailRequest preferenceDetailRequest =
                new PreferenceDetailDiningRequest("false");
        switch (preferenceEditType) {
            case ADD:
                break;
            case UPDATE:
                preferenceDetailRequest.setMyPreferencesId(diningPreferenceDetailData.getPreferenceId());
                break;
        }
        preferenceDetailRequest.fillData(CommonUtils.getStringArrWithSeparator(cuisinePreferences,
                                                                               ","),
                                         edtOtherCuisine.getText()
                                                        .toString()
                                                        .trim(),
                                         edtFoodAllergy.getText()
                                                       .toString()
                                                       .trim());

        B2CUpsertPreferenceRequest upsertPreferenceRequest = new B2CUpsertPreferenceRequest();
        upsertPreferenceRequest.setPreferenceDetails(preferenceDetailRequest);
        B2CWSUpsertPreferenceRequest b2CWSUpsertPreferenceRequest =
                new B2CWSUpsertPreferenceRequest(preferenceEditType,
                                                 upsertPreferenceCallback);
        b2CWSUpsertPreferenceRequest.setRequest(upsertPreferenceRequest);
        b2CWSUpsertPreferenceRequest.run(null);
        showDialogProgress();
    }

    private List<String> getCuisineIds() {
        List<String> ids = new ArrayList<>();

        if (cuisinePreferences != null) {
            for (int i = 0; i < cuisinePreferences.size(); i++) {
                KeyValueObject cuisine = cuisinePreferences.get(i);
                //
                ids.add(cuisine.getKey());
            }
        }

        return ids;
    }

    @Override
    public void onResponse(Call call,
                           Response response) {
        if (getActivity() == null) {
            return;
        }
        if (response.body() instanceof UpdateGourmetResponse) {
            UpdateGourmetResponse rsp = (UpdateGourmetResponse) response.body();
            if (rsp.isSuccess()) {
                onBackPress();
            } else {
//                showToast(getString(R.string.text_message_update_gourmet_fail));
            }
        }
        hideDialogProgress();
    }

    @Override
    public void onFailure(Call call,
                          Throwable t) {
        hideDialogProgress();
        if (getActivity() == null) {
            return;
        }
    }

    private B2CICallback getPreferenceCallback = new B2CICallback() {
        @Override
        public void onB2CResponse(B2CBaseResponse response) {
            hideDialogProgress();
            if (getActivity() == null) {
                return;
            }
        }

        @Override
        public void onB2CResponseOnList(List<B2CBaseResponse> responseList) {
            hideDialogProgress();
            if (getActivity() == null) {
                return;
            }
            diningPreferenceDetailData =
                    (DiningPreferenceDetailData) PreferenceData.findFromList(responseList,
                                                                             AppConstant.PREFERENCE_TYPE.DINING);
            updateUI();
        }

        @Override
        public void onB2CFailure(String errorMessage,
                                 String errorCode) {
            hideDialogProgress();
            if (getActivity() == null) {
                return;
            }
        }
    };

    private B2CICallback upsertPreferenceCallback = new B2CICallback() {
        @Override
        public void onB2CResponse(B2CBaseResponse response) {
            hideDialogProgress();
            if (getActivity() == null) {
                return;
            }
            if (response != null && response.isSuccess()) {
                onBackPress();
            } else {
//                showToast(getString(R.string.text_server_error_message));
                showDialogMessage("",
                                  getString(R.string.text_concierge_request_error));
            }
        }

        @Override
        public void onB2CResponseOnList(List<B2CBaseResponse> responseList) {
            if (getActivity() == null) {
                return;
            }
        }

        @Override
        public void onB2CFailure(String errorMessage,
                                 String errorCode) {
            hideDialogProgress();
            if (getActivity() == null) {
                return;
            }
//            showToast(getString(R.string.text_server_error_message));
            showDialogMessage("",
                              getString(R.string.text_concierge_request_error));
        }
    };
}
