package com.rosbank.android.russia.apiservices;

import com.rosbank.android.russia.apiservices.RequestModel.UpdateTransportPreferencesRequest;
import com.rosbank.android.russia.apiservices.ResponseModel.UpdateTransportPreferencesResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.ApiProviderClient;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Callback;


public class WSCreateUpdateTransportPreferences
        extends ApiProviderClient<UpdateTransportPreferencesResponse> {
    private UpdateTransportPreferencesRequest request;

    public WSCreateUpdateTransportPreferences(){

    }
    public void updateTransportPreferencesRequest(final UpdateTransportPreferencesRequest request){
        this.request = request;
    }

    @Override
    public void run(Callback<UpdateTransportPreferencesResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }

    @Override
    protected boolean isUseAuthenticateInApi() {
        return true;
    }

    @Override
    public void runApi() {

        serviceInterface.updateTransportPreferencesResponse(request).enqueue(callback);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("User-Agent", "Retrofit-Sample-App");
        return headers;
    }

}
