package com.rosbank.android.russia.apiservices.RequestModel;


import com.rosbank.android.russia.model.LoyaltyObject;
import com.google.gson.annotations.Expose;

import java.util.ArrayList;

public class UpdateTransportPreferencesRequest
        extends BaseRequest {
    public String getUserID() {
        return UserID;
    }

    public void setUserID(final String userID) {
        UserID = userID;
    }

    public ArrayList<String> getVehicles() {
        return Vehicles;
    }

    public void setVehicles(final ArrayList<String> vehicles) {
        Vehicles = vehicles;
    }

    public ArrayList<String> getCompanies() {
        return Companies;
    }

    public void setCompanies(final ArrayList<String> companies) {
        Companies = companies;
    }

    public String getOthersRentalCompany() {
        return OthersRentalCompany;
    }

    public void setOthersRentalCompany(final String othersRentalCompany) {
        OthersRentalCompany = othersRentalCompany;
    }

    public ArrayList<LoyaltyObject> getLoyaltyPrograms() {
        return LoyaltyPrograms;
    }

    public void setLoyaltyPrograms(final ArrayList<LoyaltyObject> loyaltyPrograms) {
        LoyaltyPrograms = loyaltyPrograms;
    }

    @Expose
    private String UserID;
    @Expose
    private ArrayList<String> Vehicles;
    @Expose
    private ArrayList<String> Companies;
    @Expose
    private String OthersRentalCompany;
    @Expose
    private ArrayList<LoyaltyObject> LoyaltyPrograms;

}
