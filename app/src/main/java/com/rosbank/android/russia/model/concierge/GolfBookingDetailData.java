package com.rosbank.android.russia.model.concierge;

import android.text.TextUtils;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CGetRecentRequestResponse;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.application.AppContext;
import com.rosbank.android.russia.utils.DateTimeUtil;

/**
 * Created by ThuNguyen on 10/26/2016.
 */

public class GolfBookingDetailData extends MyRequestObject {
    public String getGolfCourseName() {
        return GolfCourseName;
    }

    public void setGolfCourseName(final String golfCourseName) {
        GolfCourseName = golfCourseName;
    }

    public String getNoofHrs() {
        return NoofHrs;
    }

    public void setNoofHrs(final String noofHrs) {
        NoofHrs = noofHrs;
    }

    public int getNumberOfAdults() {
        return NumberOfAdults;
    }

    public void setNumberOfAdults(int numberOfAdults) {
        NumberOfAdults = numberOfAdults;
    }

    private String GolfCourseName;
    private String StartDate;
    private String NoofHrs;
    private int NumberOfAdults;
    private long startDateEpoch;
    private String CreateDate;
    private long createDateEpoc;
    private String PreferredGolfHandicap;

    public GolfBookingDetailData(B2CGetRecentRequestResponse recentRequestResponse) {
        super(recentRequestResponse);
        StartDate = conciergeRequestDetail.getDateofplay()+" "+conciergeRequestDetail.getTeetime();
        if(!TextUtils.isEmpty(recentRequestResponse.getNUMBEROFADULTS())) {
            try {
                NumberOfAdults = Integer.parseInt(recentRequestResponse.getNUMBEROFADULTS());
            }catch (Exception e){}

        }
        CreateDate = recentRequestResponse.getCREATEDDATE();
        createDateEpoc = DateTimeUtil.shareInstance().convertToTimeStampFromGMT(CreateDate, AppConstant.DATE_FORMAT_YYYY_MM_DD_T_HH_MM_SS);
        // Booking history
        startDateEpoch = DateTimeUtil.shareInstance().convertToTimeStamp(StartDate, AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS);

        // Item title
        ItemTitle = conciergeRequestDetail.getGolfCourseName();
        GolfCourseName = ItemTitle;

        // Item description
        ItemDescription = AppContext.getSharedInstance().getResources().getString(R.string.text_date_of_play) + DateTimeUtil.shareInstance().convertTime(StartDate, AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS,
                AppContext.getSharedInstance().getResources().getString(R.string.text_my_request_reservation_date_format));

    }


    @Override
    protected void initRequestProperties() {
        PreferredGolfHandicap = conciergeRequestDetail.getPreferredGolfHandicap();

        // Booking type group
        BookingFilterGroupType = AppConstant.BOOKING_FILTER_TYPE.Golf;
    }

    public long getRequestStartDateEpoch() {
        return startDateEpoch;
    }

    @Override
    public long getRequestEndDateEpoch() {
        return 0;
    }

    @Override
    public boolean isBookNormalType() {
        return true;
    }

    public long getStartDateEpoch() {
        return startDateEpoch;
    }

    @Override
    public String getReservationName() {
        return conciergeRequestDetail.getGuestName();
    }
    public long getCreateDateEpoc() {
        return createDateEpoc;
    }

    public void setPreferredGolfHandicap(String preferredGolfHandicap) {
        PreferredGolfHandicap = preferredGolfHandicap;
    }

    public String getPreferredGolfHandicap() {
        return PreferredGolfHandicap;
    }
}
