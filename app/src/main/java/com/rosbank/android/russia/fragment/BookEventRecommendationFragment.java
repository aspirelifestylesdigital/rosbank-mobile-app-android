package com.rosbank.android.russia.fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.activitys.HomeActivity;
import com.rosbank.android.russia.apiservices.ResponseModel.BookHotelResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.HotelBookingDetailResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CICallback;
import com.rosbank.android.russia.apiservices.b2c.B2CWSGetRequestDetail;
import com.rosbank.android.russia.apiservices.b2c.B2CWSUpsertConciergeRequest;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.B2CUpsertConciergeRequestRequest;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CGetRecentRequestResponse;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CUpsertConciergeRequestResponse;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.application.AppContext;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.model.concierge.EventBookingDetailData;
import com.rosbank.android.russia.model.concierge.MyRequestObject;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.DateTimeUtil;
import com.rosbank.android.russia.utils.EntranceLock;
import com.rosbank.android.russia.utils.FontUtils;
import com.rosbank.android.russia.utils.NetworkUtil;
import com.rosbank.android.russia.utils.PhotoPickerUtils;
import com.rosbank.android.russia.utils.StringUtil;
import com.rosbank.android.russia.widgets.AttachePhotoView;
import com.rosbank.android.russia.widgets.ContactView;
import com.rosbank.android.russia.widgets.CountryCityRequestView;
import com.rosbank.android.russia.widgets.CustomErrorView;
import com.rosbank.android.russia.widgets.NumberPickerView;

import org.greenrobot.eventbus.EventBus;

import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by anh.trinh on 9/13/2016.
 */
public class BookEventRecommendationFragment
        extends BaseFragment
        implements ContactView.ContactViewListener,
        SelectionFragment.SelectionCallback,
        SelectionCountryOrCityFragment.SelectionCallback,
        Callback, B2CICallback {
    @BindView(R.id.tv_event_category)
    TextView tvEventCategory;
    @BindView(R.id.event_category_error)
    CustomErrorView eventCategoryError;
    @BindView(R.id.tv_preferred_date)
    TextView tvEventDate;
    @BindView(R.id.countryCityView)
    CountryCityRequestView countryCityRequestView;
    @BindView(R.id.picker_no_of_tickets)
    NumberPickerView pickerTickets;
    @BindView(R.id.edt_any_special_requirements)
    EditText mEdtAnySpecialRequirements;
    @BindView(R.id.special_req_error)
    CustomErrorView specialReqError;
    @BindView(R.id.photo_attached)
    AttachePhotoView mPhotoAttached;
    /* @BindView(R.id.img_attached)
     ImageView mImgAttached;*/
    @BindView(R.id.btn_cancel)
    Button mBtnCancel;
    @BindView(R.id.btn_submit)
    Button mBtnSubmit;
    @BindView(R.id.layout_bottom_button)
    LinearLayout mLayoutBottomButton;
    @BindView(R.id.container)
    LinearLayout mLayoutContainer;
    @BindView(R.id.scrollView)
    ScrollView mScrollView;
    @BindView(R.id.contact_view)
    ContactView mContactView;

    private Calendar pickCal;

    private MyRequestObject myRequestObject = null;
    private B2CUpsertConciergeRequestRequest upsertConciergeRequestRequest;
    private String uploadedPhotoPath = "";
    private EntranceLock entranceLock = new EntranceLock();
    private boolean isDateTimeSelectedByUser = false;
    private boolean isSubmitClicked = false;

    @Override
    protected int layoutId() {
        return R.layout.fragment_concierge_book_event_recommend;
    }

    @Override
    protected void initView() {

        CommonUtils.setFontForViewRecursive(mLayoutContainer,
                FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
        CommonUtils.setFontForViewRecursive(mBtnSubmit,
                FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        CommonUtils.setFontForViewRecursive(mBtnCancel,
                FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        pickerTickets.setMinimum(1);
        makeDefaultDateTime();
        specialReqError.setMessage(getString(R.string.special_requirement_error));

        mPhotoAttached.setParentFragment(this);
        mPhotoAttached.setAttachePhotoCallback(new AttachePhotoView.IAttachePhotoCallback() {
            @Override
            public void onPhotoChanged() {
                // Reset the photo path once the user change photo
                uploadedPhotoPath = "";
            }
        });
        mContactView.setContactViewListener(this);
        if (getArguments() != null) {
            myRequestObject = (MyRequestObject) getArguments().getSerializable(AppConstant.PRE_REQUEST_OBJECT_DATA);
        }
        if (myRequestObject != null) {
            isDateTimeSelectedByUser = true;
            showDialogProgress();
            B2CWSGetRequestDetail b2CWSGetRequestDetail = new B2CWSGetRequestDetail(this);
            b2CWSGetRequestDetail.setValue(myRequestObject.getEpcCaseId(), myRequestObject.getBookingItemID());
            b2CWSGetRequestDetail.run(null);
        }
    }

    public void makeDefaultDateTime() {
        if (!isDateTimeSelectedByUser) {
            pickCal = Calendar.getInstance();
            pickCal.add(Calendar.DAY_OF_YEAR, 1);
            pickCal.add(Calendar.MINUTE, AppConstant.MINUTE_EXTRA);
            showDateTime();
        }

    }

    private void showDateTime() {
        tvEventDate.setText(DateTimeUtil.shareInstance().formatTimeStamp(pickCal.getTimeInMillis(), AppConstant.DATE_FORMAT_CONCIERGE_BOOKING));
    }

    @Override
    protected void bindData() {
    }

    @Override
    public boolean onBack() {
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((HomeActivity) getActivity()).showLogoApp(false);
        ((HomeActivity) getActivity()).setTitle(getString(R.string.text_event_title));
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater,
                container,
                savedInstanceState);
        ButterKnife.bind(this,
                rootView);
        return rootView;
    }

    @OnTouch({
            R.id.tv_event_category, R.id.edt_any_special_requirements})
    boolean onInputTouch(final View v,
                         final MotionEvent event) {

        switch (v.getId()) {
            case R.id.tv_event_category:
                eventCategoryError.setVisibility(View.GONE);
                break;
            case R.id.edt_any_special_requirements:
                specialReqError.setVisibility(View.GONE);
                break;
        }
        return false;
    }

    @OnClick({
            R.id.tv_preferred_date,
            R.id.btn_cancel,
            R.id.btn_submit,
            R.id.tv_event_category})
    public void onClick(View view) {
        CommonUtils.hideSoftKeyboard(getContext());
        switch (view.getId()) {
            case R.id.tv_preferred_date:
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view,
                                                  int year,
                                                  int monthOfYear,
                                                  int dayOfMonth) {
                                if (CommonUtils.validateFutureDate(dayOfMonth, monthOfYear, year, view)) {
                                    pickCal.set(year, monthOfYear, dayOfMonth);
                                    showDateTime();
                                }
                            }
                        },
                        pickCal.get(Calendar.YEAR),
                        pickCal.get(Calendar.MONTH),
                        pickCal.get(Calendar.DAY_OF_MONTH));
                Calendar c = Calendar.getInstance();
                c.add(Calendar.DAY_OF_YEAR, 1);
                datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis() - 1000);
                datePickerDialog.show();
                break;
            case R.id.tv_event_category:
                Bundle bundle1 = new Bundle();
                bundle1.putString(SelectionFragment.PRE_SELECTION_TYPE,
                        SelectionFragment.PRE_SELECTION_EVENT_CATEGORY);
                if (!tvEventCategory.getText().toString().equalsIgnoreCase(getString(R.string.text_concierge_booking_event_category_hint))) {
                    bundle1.putString(SelectionFragment.PRE_SELECTION_DATA,
                            tvEventCategory.getText().toString());
                }
                SelectionFragment fragment1 = new SelectionFragment();
                fragment1.setSelectionCallBack(this);
                fragment1.setArguments(bundle1);
                pushFragment(fragment1,
                        true,
                        true);
                break;
            case R.id.btn_cancel:
                if (!entranceLock.isClickContinuous()) {
                    onBackPress();
                }
                break;
            case R.id.btn_submit:
                AppContext.getSharedInstance().track(AppConstant.ANALYTIC_EVENT_CATEGORY_CONCIERGE_REQUEST, AppConstant.ANALYTIC_EVENT_ACTION_SEND_CONCIERGE_REQUEST, AppConstant.ANALYTIC_BOOK_ITEM.ENTERTAINMENT.getValue());
                if (validationData() && !entranceLock.isClickContinuous()) {
                    if (NetworkUtil.getNetworkStatus(getContext())) {
                        isSubmitClicked = true;
                        processBooking();
                    } else {
                        showInternetProblemDialog();
                    }
                }

                break;
        }
    }

    @Override
    public void onResponse(final Call call,
                           final Response response) {
        if (response.body() instanceof BookHotelResponse) {
            hideDialogProgress();
            if (getActivity() == null) {
                return;
            }
            BookHotelResponse bookHotelResponse = ((BookHotelResponse) response.body());
            int code = bookHotelResponse.getStatus();
            if (code == 200) {
                onBackPress();

                ThankYouFragment thankYouFragment = new ThankYouFragment();
                if (myRequestObject != null) {
                   /* Bundle bundle = new Bundle();
                    bundle.putString(ThankYouFragment.LINE_MAIN,
                                     getString(R.string.text_message_thank_you_amend));
                    thankYouFragment.setArguments(bundle);*/
                    EventBus.getDefault().post(AppConstant.CONCIERGE_EDIT_TYPE.AMEND);
                } else {
                    AppContext.getSharedInstance().track(AppConstant.TRACK_EVENT_PRODUCT_CATEGORY.RECOMMEND_EVENT.getValue(), AppConstant.TRACK_EVENT_PRODUCT_NAME.GENERIC_RECOMMEND.getValue());
                    EventBus.getDefault().post(AppConstant.CONCIERGE_EDIT_TYPE.ADD);
                }
                pushFragment(thankYouFragment,
                        true,
                        true);

            } else {
//                showToast(bookHotelResponse.getMessage());
            }
        }
        if (response.body() instanceof HotelBookingDetailResponse) {
            hideDialogProgress();
            if (getActivity() == null) {
                return;
            }
            HotelBookingDetailResponse hotelBookingDetailResponse =
                    ((HotelBookingDetailResponse) response.body());
            int code = hotelBookingDetailResponse.getStatus();
            if (code == 200) {
            } else {
//                showToast(hotelBookingDetailResponse.getMessage());
            }
        }


    }

    @Override
    public void onFailure(final Call call,
                          final Throwable t) {
        hideDialogProgress();
        if (getActivity() == null) {
            return;
        }
//        showToast(getString(R.string.text_server_error_message));
    }

    @Override
    public void onFinishSelection(final Bundle bundle) {
        String typeSelection = bundle.getString(SelectionFragment.PRE_SELECTION_TYPE,
                "");
        if (typeSelection.equals(SelectionFragment.PRE_SELECTION_COUNTRY_CODE)) {
            mContactView.setCountryNo(bundle.getString(SelectionFragment.PRE_SELECTION_DATA,
                    ""));
        } else if (typeSelection.equalsIgnoreCase(SelectionFragment.PRE_SELECTION_EVENT_CATEGORY)) {
            String selectedEventCategory = bundle.getString(SelectionFragment.PRE_SELECTION_DATA, "");
            if (!TextUtils.isEmpty(selectedEventCategory)) {
                tvEventCategory.setText(selectedEventCategory);
                tvEventCategory.setTextColor(ContextCompat.getColor(getContext(), R.color.text_color_active));
            } else {
                tvEventCategory.setText(R.string.text_concierge_booking_event_category_hint);
                tvEventCategory.setTextColor(ContextCompat.getColor(getContext(), R.color.hint_color));
            }
        }
        /*if (typeSelection.equals(SelectionCountryOrCityFragment.PRE_SELECTION_CITY)) {
            mCitySelected =
                    bundle.getParcelable(SelectionCountryOrCityFragment.PRE_SELECTION_DATA);
            if (mCitySelected != null && CommonUtils.isStringValid(mCitySelected.getCityName())) {
            }
        }*/


    }

    private boolean validationData() {
        boolean result = true;
        if (tvEventCategory.getText().toString().equals(getString(R.string.text_concierge_booking_event_category_hint))) {
            eventCategoryError.fillData(getString(R.string.text_sign_up_error_required_field));
            eventCategoryError.setVisibility(View.VISIBLE);
            result = false;
        } else {
            eventCategoryError.setVisibility(View.GONE);
        }
        if (!countryCityRequestView.isValidated()) {
            result = false;
        }
        if (StringUtil.containSpecialCharacter(mEdtAnySpecialRequirements.getText().toString())) {
            specialReqError.setVisibility(View.VISIBLE);
            result = false;
        } else {
            specialReqError.setVisibility(View.GONE);
        }

        if (!mContactView.validationContact()) {
            result = false;
        }
        return result;
    }

    private void processBooking() {
        upsertConciergeRequestRequest = new B2CUpsertConciergeRequestRequest();
        upsertConciergeRequestRequest.setFunctionality(AppConstant.CONCIERGE_FUNCTIONALITY_TYPE.ENTERTAINMENT_RECOMMEND.getValue());
        upsertConciergeRequestRequest.setRequestType(AppConstant.CONCIERGE_REQUEST_TYPE.RECOMMEND_EVENT.getValue());
       /* upsertConciergeRequestRequest.setCity(edtCity.getText().toString().trim());
        upsertConciergeRequestRequest.setCountry(mTvSelectCountry.getText().toString().trim());*/

        upsertConciergeRequestRequest.setDelivery(DateTimeUtil.shareInstance().formatTimeStamp(pickCal.getTimeInMillis(), AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS));
        upsertConciergeRequestRequest.setEventDate(DateTimeUtil.shareInstance().formatTimeStamp(pickCal.getTimeInMillis(), AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS));

        // Image path
        if (!TextUtils.isEmpty(uploadedPhotoPath)) {
            upsertConciergeRequestRequest.setAttachmentPath(uploadedPhotoPath);
        } else {
            if (mPhotoAttached.getPhotos() != null && mPhotoAttached.getPhotos().size() > 0) {
                String pathImage = mPhotoAttached.getPhotos()
                        .get(0);
                upsertConciergeRequestRequest.setAttachmentPath(pathImage);
            }
        }
        upsertConciergeRequestRequest.updateDataFromCountryCityView(countryCityRequestView);
        upsertConciergeRequestRequest.updateDataFromContactView(mContactView);

        if (myRequestObject != null) {
            upsertConciergeRequestRequest.setTransactionID(myRequestObject.getBookingItemID());
        }
        // Request details
        upsertConciergeRequestRequest.setRequestDetails(combineRequestDetails());

        upsertConciergeRequestRequest.setNumberOfAdults(String.valueOf(pickerTickets.getNumber()));
        upsertConciergeRequestRequest.setSituation(tvEventCategory.getText().toString());

        showDialogProgress();
        B2CWSUpsertConciergeRequest b2CWSUpsertConciergeRequest = new B2CWSUpsertConciergeRequest(myRequestObject == null ? AppConstant.CONCIERGE_EDIT_TYPE.ADD : AppConstant.CONCIERGE_EDIT_TYPE.AMEND, this);
        b2CWSUpsertConciergeRequest.setRequest(upsertConciergeRequestRequest);
        b2CWSUpsertConciergeRequest.run(null);

    }

    private String combineRequestDetails() {
        String requestDetails = "";
        if (!TextUtils.isEmpty(tvEventCategory.getText().toString())) {
            requestDetails +=
                    AppConstant.B2C_REQUEST_DETAIL_EVENT_CATEGORY + tvEventCategory.getText()
                            .toString();
        }

        if (!TextUtils.isEmpty(mEdtAnySpecialRequirements.getText().toString().trim())) {
            if (!TextUtils.isEmpty(requestDetails)) {
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_SPECIAL_REQUIREMENTS + StringUtil.removeAllSpecialCharacterAndBreakLine(mEdtAnySpecialRequirements.getText().toString().trim());
        }

        if (!TextUtils.isEmpty(countryCityRequestView.getCountry())) {
            if (!TextUtils.isEmpty(requestDetails)) {
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_COUNTRY + countryCityRequestView.getCountry();
        }
        if (!TextUtils.isEmpty(countryCityRequestView.getState())) {
            if (!TextUtils.isEmpty(requestDetails)) {
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_COUNTRY + countryCityRequestView.getState();
        }
        if (!TextUtils.isEmpty(mContactView.getReservationName())) {
            if (!TextUtils.isEmpty(requestDetails)) {
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_RESERVATION_NAME + mContactView.getReservationName();
        }
        if (!TextUtils.isEmpty(requestDetails)) {
            requestDetails += " | ";
        }
        requestDetails += AppConstant.B2C_REQUEST_DETAIL_NO_TICKET + pickerTickets.getNumber();

        return requestDetails;
       /* String requestDetails = "";
        if(!TextUtils.isEmpty(mEdtAnySpecialRequirements.getText().toString().trim())){
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_SPECIAL_REQ + mEdtAnySpecialRequirements.getText().toString().trim();
        }
        if (!TextUtils.isEmpty(requestDetails)) {
            requestDetails += " | ";
        }
        requestDetails += AppConstant.B2C_REQUEST_DETAIL_EVENT_CATEGORY + tvEventCategory.getText().toString();

        requestDetails += " | " + AppConstant.B2C_REQUEST_DETAIL_PREF_DATE + DateTimeUtil.shareInstance().formatTimeStamp(pickCal.getTimeInMillis(), AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS);
        requestDetails += " | " + AppConstant.B2C_REQUEST_DETAIL_NO_TICKET + pickerTickets.getNumber();
        return requestDetails;*/
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode,
                permissions,
                grantResults);

        switch (requestCode) {
            case AppConstant.PERMISSION_REQUEST_READ_EXTERNAL:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mPhotoAttached.openGallery();
                }
                break;
            case AppConstant.PERMISSION_REQUEST_OPEN_CAMERA:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mPhotoAttached.openCamera();
                }
                break;
            case AppConstant.PERMISSION_REQUEST_WRITE_EXTERNAL:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mPhotoAttached.openCamera();
                }
                break;
        }

    }

    @Override
    public void onActivityResult(int requestCode,
                                 int resultCode,
                                 Intent data) {
        super.onActivityResult(requestCode,
                resultCode,
                data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == PhotoPickerUtils.PICK_IMAGE_REQUEST_CODE || requestCode == PhotoPickerUtils.TAKE_PICTURE_REQUEST_CODE) {
                mPhotoAttached.onActivityResult(requestCode,
                        data);
            }
        }
    }

    @Override
    public void onSelectCountryClick() {
        Bundle bundle1 = new Bundle();
        bundle1.putString(SelectionFragment.PRE_SELECTION_TYPE,
                SelectionFragment.PRE_SELECTION_COUNTRY_CODE);
        bundle1.putString(SelectionFragment.PRE_SELECTION_DATA,
                mContactView.getSelectedCountryCode());
        SelectionFragment fragment1 = new SelectionFragment();
        fragment1.setSelectionCallBack(this);
        fragment1.setArguments(bundle1);
        pushFragment(fragment1,
                true,
                true);
    }

    private void setDetailData(EventBookingDetailData data) {
        countryCityRequestView.setData(data);
        tvEventCategory.setText(data.getConciergeRequestDetail().getEventCategory());
        tvEventCategory.setTextColor(ContextCompat.getColor(getContext(), R.color.text_color_active));
        pickerTickets.setNumber(data.getNoOfTickets());
        mEdtAnySpecialRequirements.setText(data.getConciergeRequestDetail().getSpecialRequirement());

        pickCal = Calendar.getInstance();
        pickCal.setTimeInMillis(data.getPreferredDateEpoc());
        showDateTime();
        // Contact view
        mContactView.updateContactView(data);
    }


    @Override
    public void onB2CResponse(B2CBaseResponse response) {
        hideDialogProgress();
        if (getActivity() == null) {
            return;
        }
        if (response instanceof B2CUpsertConciergeRequestResponse) {
            if (response != null && response.isSuccess()) {
                onBackPress();

                ThankYouFragment thankYouFragment = new ThankYouFragment();
                if (myRequestObject != null) {
//                    Bundle bundle = new Bundle();
//                    bundle.putString(ThankYouFragment.LINE_MAIN,
//                            getString(R.string.text_message_thank_you_amend));
//                    thankYouFragment.setArguments(bundle);
                    EventBus.getDefault().post(AppConstant.CONCIERGE_EDIT_TYPE.AMEND);
                } else {
                    // Track product
                    AppContext.getSharedInstance().track(AppConstant.TRACK_EVENT_PRODUCT_CATEGORY.RECOMMEND_EVENT.getValue(), AppConstant.TRACK_EVENT_PRODUCT_NAME.GENERIC_RECOMMEND.getValue());
                    EventBus.getDefault().post(AppConstant.CONCIERGE_EDIT_TYPE.ADD);
                }
                pushFragment(thankYouFragment,
                        true,
                        true);
            } else {
                showDialogMessage(getString(R.string.text_title_dialog_error), getString(R.string.text_concierge_request_error));
                if (upsertConciergeRequestRequest != null) {
                    uploadedPhotoPath = upsertConciergeRequestRequest.getAttachmentPath();
                }
            }
        } else if (response instanceof B2CGetRecentRequestResponse) {
            setDetailData(new EventBookingDetailData((B2CGetRecentRequestResponse) response));
        }
    }

    @Override
    public void onB2CResponseOnList(List<B2CBaseResponse> responseList) {
        if (getActivity() == null) {
            return;
        }

    }

    @Override
    public void onB2CFailure(String errorMessage, String errorCode) {
        hideDialogProgress();
        if (getActivity() == null) {
            return;
        }
        if (isSubmitClicked) {
            isSubmitClicked = false;
            showDialogMessage(getString(R.string.text_title_dialog_error), getString(R.string.text_concierge_request_error));
        }
        // Store the uploaded successful photo path
        // So that the upsert failed in the first time, user tried to process for the second time
        // then that is no need to upload again
        if (upsertConciergeRequestRequest != null) {
            uploadedPhotoPath = upsertConciergeRequestRequest.getAttachmentPath();
        }
    }
}
