package com.rosbank.android.russia.fragment;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.activitys.HomeActivity;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.EntranceLock;
import com.rosbank.android.russia.utils.FontUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by anh.trinh on 9/26/2016.
 */
public class AccountLandingFragment
        extends BaseFragment {

    @BindView(R.id.img_account)
    ImageView mImgAccount;
    @BindView(R.id.tv_my_profile)
    TextView mTvMyProfile;
    @BindView(R.id.line_1)
    View line1;
  /*  @BindView(R.id.tv_my_preference)
    TextView tvMyPreference;*/
    @BindView(R.id.line_2)
    View line2;
    @BindView(R.id.tv_my_currency)
    TextView mTvMyCurrency;
    @BindView(R.id.line_3)
    View line3;
    @BindView(R.id.layout_contain)
    RelativeLayout mLayoutContain;

    EntranceLock entranceLock = new EntranceLock();
    @Override
    protected int layoutId() {
        return R.layout.fragment_account_landing;
    }

    @Override
    protected void initView() {
        CommonUtils.setFontForViewRecursive(mLayoutContain,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);

    }

    @Override
    protected void bindData() {

    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showLogoApp(false);
            ((HomeActivity) getActivity()).setTitle(getString(R.string.text_title_account));
        }
    }

    @Override
    public boolean onBack() {
        return false;
    }


    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater,
                                           container,
                                           savedInstanceState);
        ButterKnife.bind(this,
                         rootView);
        return rootView;
    }

    @OnClick({R.id.tv_my_profile,
             /* R.id.tv_my_preference,*/
              R.id.tv_my_currency,
            R.id.tvChangePassword})
    public void onClick(View view) {
        if(!entranceLock.isClickContinuous()) {
        switch (view.getId()) {
            case R.id.tv_my_profile:
                pushFragment(new MyProfileFragment(), true, true);
                break;
/*            case R.id.tv_my_preference:
                pushFragment(new MyPreferencesFragment(), true, true);
                break;*/
            case R.id.tv_my_currency:
                pushFragment(new SelectionMyCurrencyFragment(), true, true);
                break;
            case R.id.tvChangePassword:
                pushFragment(new ChangePasswordFragment(), true, true);
                break;
        }
    }
    }
}
