package com.rosbank.android.russia.fragment;

import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.activitys.HomeActivity;
import com.rosbank.android.russia.adapter.ExperienceAdapter;
import com.rosbank.android.russia.apiservices.RequestModel.ExperienceGourmetRequest;
import com.rosbank.android.russia.apiservices.ResponseModel.GetExperienceEntertainmentResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.GetExperienceGourmetResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.GetExperienceLifeStyleResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.GetExperienceTravelResponse;
import com.rosbank.android.russia.apiservices.WSGetExperienceEntertainment;
import com.rosbank.android.russia.apiservices.WSGetExperienceGourmet;
import com.rosbank.android.russia.apiservices.WSGetExperienceGourmetFilter;
import com.rosbank.android.russia.apiservices.WSGetExperienceLifeStyle;
import com.rosbank.android.russia.apiservices.WSGetExperienceTravel;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.interfaces.OnRecyclerViewItemClickListener;

import retrofit2.Call;
import retrofit2.Callback;

import com.rosbank.android.russia.model.GourmetResponeData;
import com.rosbank.android.russia.model.Restaurant;
import com.rosbank.android.russia.utils.Fontfaces;
//import com.rosbank.android.russia.utils.GpsTrackerManager;
import com.rosbank.android.russia.utils.SelfPermissionUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Response;

import static com.rosbank.android.russia.fragment.ExperienceSearchFragment.ApiRequest.GOURMET;
import static com.rosbank.android.russia.fragment.ExperienceSearchFragment.ApiRequest.GOURMET_FILTER;

/*
 * Created by chau.nguyen on 10/05/2016.
 */
public class ExperienceSearchFragment extends BaseFragment implements Callback {

    @BindView(R.id.tvExperienceFilter)
    TextView tvFilter;

    @BindView(R.id.tvExperienceNoOffers)
    TextView tvNoOffers;

    @BindView(R.id.rcvExperienceSearch)
    RecyclerView rcvResult;

    public static final String TitleSearch = "titleSearch";
    private String titleBar = "";
    private List<Parcelable> data;
    ExperienceAdapter adapter;

    private int firstVisibleItem, visibleItemCount, totalItemCount;
    private boolean loading = false;

   // private GpsTrackerManager gpsManager;
    private Location locationUser;

    ExperienceGourmetRequest requestFilter;

    public enum ApiRequest {
        GOURMET,
        ENTERTAINMENT,
        TRAVEL,
        LIFESTYLE,
        GOURMET_FILTER
    }

    private ApiRequest apiRequest = ApiRequest.ENTERTAINMENT;

    public interface EventFilterPage {
        void callFilter(ExperienceGourmetRequest request);
        void callNoFilter();
    }

    public ExperienceSearchFragment() {
        if (data != null) {
            data.clear();
        }
        this.data = new ArrayList<>();
    }

    @Override
    protected int layoutId() {
        return R.layout.fragment_experiences_common_landing;
    }


    @Override
    protected void initView() {
        tvFilter.setTypeface(Fontfaces.getFont(getActivity(), Fontfaces.FONTLIST.AVENIR_NEXT_LT_MEDIUM));
        tvNoOffers.setTypeface(Fontfaces.getFont(getActivity(), Fontfaces.FONTLIST.Avenirnext_demibold));

        adapter = new ExperienceAdapter(data, new OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(int position, View view) {
                //after click search Item
                ExperiencesDetailFragment detailFr = new ExperiencesDetailFragment();
                Bundle bundleDetail = new Bundle();
                bundleDetail.putSerializable(ExperiencesDetailFragment.EXP_DETAIL_TYPE, apiRequest);

                if (data.get(position) instanceof Restaurant) {
                    Restaurant rest = (Restaurant) data.get(position);
                    bundleDetail.putString(ExperiencesDetailFragment.EXP_DETAIL_ID_RESTAURANT, rest.getIDStr());
                } else {
                    bundleDetail.putParcelable(ExperiencesDetailFragment.EXP_DETAIL_DATA, data.get(position));
                }
                detailFr.setArguments(bundleDetail);
                pushFragment(detailFr, true, true);
            }
        }, apiRequest);

        LinearLayoutManager layoutManager = new LinearLayoutManager(view.getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rcvResult.setLayoutManager(layoutManager);
        rcvResult.setAdapter(adapter);
        rcvResult.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    totalItemCount = layoutManager.getItemCount();
                    visibleItemCount = layoutManager.getChildCount();
                    firstVisibleItem = layoutManager.findFirstVisibleItemPosition();

                    if (!loading && (firstVisibleItem + visibleItemCount) >= totalItemCount) {
                        loading = true;
                        requestLoadMore();
                    }
                }
            }
        });
        loading = true;
        switch (apiRequest) {
            case GOURMET:
                showDialogProgress();
                WSGetExperienceGourmet eat = new WSGetExperienceGourmet();
                eat.run(this);
                break;
            case ENTERTAINMENT:
                tvFilter.setVisibility(View.GONE);
                showDialogProgress();
                WSGetExperienceEntertainment play = new WSGetExperienceEntertainment();
                play.run(this);
                break;
            case LIFESTYLE:
                tvFilter.setVisibility(View.GONE);
                showDialogProgress();
                WSGetExperienceLifeStyle life = new WSGetExperienceLifeStyle();
                life.run(this);
                break;
            case TRAVEL:
                tvFilter.setVisibility(View.GONE);
                showDialogProgress();
                WSGetExperienceTravel go = new WSGetExperienceTravel();
                go.run(this);
                break;
            default:
                break;
        }
    }

    //======= Config Scroll load more ==================

    /**
     * use post Runnable when add View load more
     * don't call adapter.addViewLoading with have error
     */
    private void scrollShowLoadMore() {
        rcvResult.post(new Runnable() {
            @Override
            public void run() {
                adapter.addViewLoading();
            }
        });
    }

    /**
     * use post Runnable when hide View load more
     */
    private void scrollHideLoadMore() {
        rcvResult.post(new Runnable() {
            @Override
            public void run() {
                adapter.removeViewLoading();
            }
        });
    }

    private void resetLoadingMore() {
        loading = false;
        scrollHideLoadMore();
    }

    private void requestLoadMore() {
        if (apiRequest == ApiRequest.GOURMET) {
            scrollShowLoadMore();
            WSGetExperienceGourmet requestMore = new WSGetExperienceGourmet();
            requestMore.setTotalItems(adapter.getRealSize());
            requestMore.run(ExperienceSearchFragment.this);
        } else if (apiRequest == ApiRequest.GOURMET_FILTER) {
            if (requestFilter != null) {
                WSGetExperienceGourmetFilter filter = new WSGetExperienceGourmetFilter();
                filter.setRequest(requestFilter);
                filter.setTotalItems(adapter.getRealSize());
                filter.run(ExperienceSearchFragment.this);
            }
        } else {
            //not process load more

        }

    }
    //======= End Config Scroll load more ==================

    //======= Location User GPS ======================
    private void isHaveLocation() {
        if (locationUser == null) {
          //  locationUser = gpsManager.getLastLocation();
            adapter.setLocationUser(locationUser);
        }
    }

    private boolean isGourmet() {
        return (apiRequest == ApiRequest.GOURMET || apiRequest == ApiRequest.GOURMET_FILTER);
    }

    @Override
    public void onPause() {
        super.onPause();
        /*if (gpsManager != null) {
            gpsManager.stopGpsTrackerService();
        }*/
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case AppConstant.PERMISSION_REQUEST_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //    gpsManager.initialize();
                    isHaveLocation();
                }
                break;
        }

    }
    //======= End Location User GPS ==================

    @Override
    protected void bindData() {
        titleBar = getArguments().getString(TitleSearch, "");
        showResult();
//        doSearch();
    }

    /*private void doSearch() {
        if (data != null && data.size() > 0) {
            showResult();
        } else {
            showNoOffers();
        }
    }*/

    private void showResult() {
        tvNoOffers.setVisibility(View.GONE);
        rcvResult.setVisibility(View.VISIBLE);
    }

    private void showNoOffers() {
        tvNoOffers.setVisibility(View.VISIBLE);
        rcvResult.setVisibility(View.GONE);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showLogoApp(false);
            ((HomeActivity) getActivity()).setTitle(titleBar);
        }

        if (isGourmet()) {
          /*  gpsManager = GpsTrackerManager.getInstance(getContext());
            if (SelfPermissionUtils.getInstance().checkOrRequestLocationPermission(getActivity(), AppConstant.PERMISSION_REQUEST_LOCATION))
                gpsManager.initialize();*/
        }
    }

    @Override
    public boolean onBack() {
        return false;
    }

    @OnClick(R.id.tvExperienceFilter)
    public void eventFilter() {
        ExperiencesFilterFragment filterFragment = new ExperiencesFilterFragment();
        filterFragment.setFilterCallSearch(new EventFilterPage() {
            @Override
            public void callFilter(ExperienceGourmetRequest request) {
                showDialogProgress();
                requestFilter = request;
                apiRequest = ApiRequest.GOURMET_FILTER;
                adapter.setClearAll();
                WSGetExperienceGourmetFilter filter = new WSGetExperienceGourmetFilter();
                filter.setRequest(requestFilter);
                filter.run(ExperienceSearchFragment.this);
            }

            @Override
            public void callNoFilter() {
                showDialogProgress();
                apiRequest = ApiRequest.GOURMET;
                adapter.setClearAll();
                WSGetExperienceGourmet eat = new WSGetExperienceGourmet();
                eat.run(ExperienceSearchFragment.this);
            }
        });
        pushFragment(filterFragment, true, true);
    }

    private void loadResultFilter(GetExperienceGourmetResponse eat){
        if (eat.isSuccess()) {
            if(eat.getData().getNbOfRecordsReturn() > 0){
                GourmetResponeData rests = eat.getData();
                isHaveLocation();
                adapter.setUpdateData(rests.getListData());
            }else if(adapter.getItemCount() == 0){
                showNoOffers();
            }
        } else {
//            showToast(eat.getMessage());
        }
    }
    /**
     * Call Server Response
     */
    @Override
    public void onResponse(Call call, Response response) {
        if(getActivity()==null) {
            return;
        }
        if (response.body() instanceof GetExperienceGourmetResponse) {
            hideDialogProgress();
            resetLoadingMore();
            GetExperienceGourmetResponse eat = ((GetExperienceGourmetResponse) response.body());
            if(apiRequest == GOURMET){
                if (eat.isSuccess()) {
                    if(eat.getData().getNbOfRecordsReturn() > 0){
                        GourmetResponeData rests = eat.getData();
                        isHaveLocation();
                        adapter.setUpdateData(rests.getListData());
                    }else if(adapter.getItemCount() == 0){
                        showNoOffers();
                    }
                } else {
//                    showToast(eat.getMessage());
                }
            } else if (apiRequest == GOURMET_FILTER) {
                loadResultFilter(eat);
            }
        } else if (response.body() instanceof GetExperienceEntertainmentResponse) {
            hideDialogProgress();
            GetExperienceEntertainmentResponse entertainment = ((GetExperienceEntertainmentResponse) response.body());
            if (entertainment.isSuccess()) {
                adapter.setUpdateData(entertainment.getData());
            } else {
//                showToast(entertainment.getMessage());
            }
        } else if (response.body() instanceof GetExperienceLifeStyleResponse) {
            hideDialogProgress();
            GetExperienceLifeStyleResponse life = ((GetExperienceLifeStyleResponse) response.body());
            if (life.isSuccess()) {
                adapter.setUpdateData(life.getData());
            } else {
//                showToast(life.getMessage());
            }
        } else if (response.body() instanceof GetExperienceTravelResponse) {
            hideDialogProgress();
            GetExperienceTravelResponse travel = ((GetExperienceTravelResponse) response.body());
            if (travel.isSuccess()) {
                adapter.setUpdateData(travel.getData());
            } else {
//                showToast(travel.getMessage());
            }
        }
    }

    @Override
    public void onFailure(Call call, Throwable t) {
        hideDialogProgress();
        if(getActivity()==null) {
            return;
        }
//        showToast(getActivity().getString(R.string.text_server_error_message));
        showNoOffers();
    }

    public void setApiRequest(ApiRequest apiRequest) {
        this.apiRequest = apiRequest;
    }
}
