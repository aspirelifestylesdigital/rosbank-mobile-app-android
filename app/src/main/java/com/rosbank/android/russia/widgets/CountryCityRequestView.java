package com.rosbank.android.russia.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.activitys.HomeActivity;
import com.rosbank.android.russia.fragment.SelectionCountryOrCityFragment;
import com.rosbank.android.russia.fragment.SelectionFragment;
import com.rosbank.android.russia.model.CountryObject;
import com.rosbank.android.russia.model.concierge.MyRequestObject;
import com.rosbank.android.russia.utils.CommonUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;

/**
 * Created by ThuNguyen on 1/19/2017.
 */

public class CountryCityRequestView extends FrameLayout implements
        SelectionCountryOrCityFragment.SelectionCallback, SelectionFragment.SelectionCallback{
    @BindView(R.id.tv_please_select_country)
    TextView tvCountry;
    @BindView(R.id.country_error)
    CustomErrorView countryErrorView;
    @BindView(R.id.edt_city)
    EditText edtCity;
    @BindView(R.id.city_error)
    CustomErrorView cityErrorView;

    @BindView(R.id.layout_state)
    View stateView;
    @BindView(R.id.tv_please_select_state)
    TextView tvState;
    @BindView(R.id.state_error)
    CustomErrorView stateErrorView;

    private String countryHint;
    private String cityHint;
    private String stateHint;

    private CountryObject selectedCountryObject;

    public CountryCityRequestView(Context context) {
        this(context, null);
    }

    public CountryCityRequestView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CountryCityRequestView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.CountryCityRequest, defStyleAttr, 0);
        countryHint = a.getString(R.styleable.CountryCityRequest_country_hint);
        cityHint = a.getString(R.styleable.CountryCityRequest_city_hint);
        stateHint = a.getString(R.styleable.CountryCityRequest_state_hint);
        a.recycle();

        init(context);
    }

    private void init(Context context){
        LayoutInflater.from(context).inflate(R.layout.ctrl_country_city_request, this, true);
        ButterKnife.bind(this, this);

        // Set default
        tvCountry.setText(countryHint);
        edtCity.setHint(cityHint);
        tvState.setText(stateHint);

        // Set default error
        countryErrorView.fillData(getResources().getString(R.string.text_sign_up_error_required_field));
        cityErrorView.fillData(getResources().getString(R.string.text_sign_up_error_required_field));
        stateErrorView.fillData(getResources().getString(R.string.text_sign_up_error_required_field));

        countryErrorView.setVisibility(GONE);
        cityErrorView.setVisibility(GONE);
        stateErrorView.setVisibility(GONE);

        selectedCountryObject = CountryObject.getDefaultCountry();
        tvCountry.setText(selectedCountryObject.getCountryName());
        CommonUtils.makeFirstCharacterUpperCase(edtCity);
        decideVisibilityOfStateTextFromCountry();

    }
    @OnTouch({R.id.edt_city})
    boolean onInputTouch(final View v,
                         final MotionEvent event) {

        switch (v.getId()) {
            case R.id.edt_city:
                cityErrorView.setVisibility(View.GONE);
                break;
        }
        return false;
    }
    @OnClick({R.id.tv_please_select_country, R.id.tv_please_select_state})
    void onClick(View view){
        switch (view.getId()){
            case R.id.tv_please_select_country:
                Bundle bundleCountry = new Bundle();
                bundleCountry.putString(SelectionCountryOrCityFragment.PRE_SELECTION_TYPE,
                        SelectionCountryOrCityFragment.PRE_SELECTION_COUNTRY);
                bundleCountry.putParcelable(SelectionCountryOrCityFragment.PRE_SELECTION_DATA,
                        selectedCountryObject);

                SelectionCountryOrCityFragment selectionCountryOrCityFragment =
                        new SelectionCountryOrCityFragment();
                selectionCountryOrCityFragment.setArguments(bundleCountry);
                selectionCountryOrCityFragment.setSelectionCallBack(this);
                ((HomeActivity)getContext()).pushFragment(selectionCountryOrCityFragment,
                        true,
                        true);
                break;
            case R.id.tv_please_select_state:
                Bundle bundle = new Bundle();
                bundle.putString(SelectionFragment.PRE_SELECTION_TYPE,
                        SelectionFragment.PRE_SELECTION_STATE);
                if(!tvState.getText().toString().equals(stateHint)) {
                    bundle.putString(SelectionFragment.PRE_SELECTION_DATA,
                            tvState.getText().toString());
                }
                if(selectedCountryObject != null){
                    if(selectedCountryObject.getCountryCode().equalsIgnoreCase("usa")){
                        bundle.putString(SelectionFragment.PRE_SELECTION_SPECIFIC_STATE,
                                "usa");
                    }else if(selectedCountryObject.getCountryCode().equalsIgnoreCase("can")){
                        bundle.putString(SelectionFragment.PRE_SELECTION_SPECIFIC_STATE,
                                "can");
                    }
                }
                SelectionFragment selectionFragment = new SelectionFragment();
                selectionFragment.setSelectionCallBack(new SelectionFragment.SelectionCallback() {
                    @Override
                    public void onFinishSelection(Bundle bundle) {
                        String selectedState = bundle.getString(SelectionFragment.PRE_SELECTION_DATA);
                        decorateStateText(selectedState);
                    }
                });
                selectionFragment.setArguments(bundle);
                ((HomeActivity)getContext()).pushFragment(selectionFragment,
                        true,
                        true);
                break;
        }
    }
    public boolean isValidated(boolean isShowError){
        boolean isValidated = true;

        // Check country
        if(tvCountry.getText().toString().equals(countryHint)){
            isValidated = false;
            if(isShowError)
            countryErrorView.setVisibility(VISIBLE);
        }else{
            countryErrorView.setVisibility(GONE);
        }

        // Check city
        if(edtCity.getText().toString().length() == 0){
            isValidated = false;
            if(isShowError)
            cityErrorView.setVisibility(VISIBLE);
        }else{
            cityErrorView.setVisibility(GONE);
        }
        if(stateView.getVisibility() == VISIBLE){
            if(tvState.getText().toString().equals(stateHint)){
                isValidated = false;
                if(isShowError)
                stateErrorView.setVisibility(VISIBLE);
            }else{
                stateErrorView.setVisibility(GONE);
            }
        }
        return isValidated;
    }
    public boolean isValidated(){
       return isValidated(true);
    }
    public void setCountry(String country){
        selectedCountryObject = CountryObject.getCountryObjectFromName(country);
        tvCountry.setText(country);
    }
    public void setState(String state){
        decorateStateText(state);
    }
    public String getCountry(){
        if(tvCountry.getText().toString().equalsIgnoreCase(countryHint)){
            return "";
        }
        return tvCountry.getText().toString();
    }
    public void setCity(String city){
        edtCity.setText(city);
    }
    public String getCity(){
        return edtCity.getText().toString();
    }
    public String getState(){
        if(tvState.getText().toString().equalsIgnoreCase(stateHint)){
            return "";
        }
        return tvState.getText().toString();
    }
    public void setData(MyRequestObject myRequestObject){
        setCountry(myRequestObject.getCountry());
        setCity(myRequestObject.getCity());
        decideVisibilityOfStateTextFromCountry();
        decorateStateText(myRequestObject.getState());
    }
    private void decorateStateText(String state){
        if(TextUtils.isEmpty(state)){
            tvState.setText(stateHint);
            tvState.setTextColor(ContextCompat.getColor(getContext(), R.color.hint_color));
        }else{
            tvState.setText(state);
            tvState.setTextColor(ContextCompat.getColor(getContext(), R.color.text_color_active));
            stateErrorView.setVisibility(GONE);
        }
    }
    private void decideVisibilityOfStateTextFromCountry(){
        if(selectedCountryObject != null &&
                (selectedCountryObject.getCountryCode().equalsIgnoreCase("usa") || selectedCountryObject.getCountryCode().equalsIgnoreCase("can"))){
            stateView.setVisibility(VISIBLE);
        }else{
            stateView.setVisibility(GONE);
        }
        decorateStateText("");
    }
    @Override
    public void onFinishSelection(Bundle bundle) {
        selectedCountryObject =
                bundle.getParcelable(SelectionCountryOrCityFragment.PRE_SELECTION_DATA);
        if (selectedCountryObject != null && CommonUtils.isStringValid(selectedCountryObject.getCountryCode())) {
            tvCountry.setText(selectedCountryObject.getCountryName());
            tvCountry.setTextColor(ContextCompat.getColor(getContext(), R.color.text_color_active));
            countryErrorView.setVisibility(GONE);
        }else{
            selectedCountryObject = null;
            tvCountry.setText(countryHint);
            tvCountry.setTextColor(ContextCompat.getColor(getContext(), R.color.hint_color));
        }
        decideVisibilityOfStateTextFromCountry();
    }
}
