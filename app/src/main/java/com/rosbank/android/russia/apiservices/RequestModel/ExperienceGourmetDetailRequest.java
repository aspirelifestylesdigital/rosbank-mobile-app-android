package com.rosbank.android.russia.apiservices.RequestModel;

import com.google.gson.annotations.Expose;

public class ExperienceGourmetDetailRequest extends BaseRequest {
    @Expose
    private String RestaurantId;
    @Expose
    private String UserID;

    public String getRestaurantId() {
        return RestaurantId;
    }

    public void setRestaurantId(String restaurantId) {
        RestaurantId = restaurantId;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }
}