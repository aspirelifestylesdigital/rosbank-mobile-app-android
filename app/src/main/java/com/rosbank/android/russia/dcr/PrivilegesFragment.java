package com.rosbank.android.russia.dcr;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.activitys.HomeActivity;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.dcr.adapter.PrivilegesAdapter;
import com.rosbank.android.russia.dcr.api.DCRICallback;
import com.rosbank.android.russia.dcr.api.DCRWSGetPrivileges;
import com.rosbank.android.russia.dcr.api.RequestModel.DCRGetPrivilegesRequest;
import com.rosbank.android.russia.dcr.api.ResponseModel.DCRBaseResponse;
import com.rosbank.android.russia.dcr.api.ResponseModel.DCRGetPrivilegesResponse;
import com.rosbank.android.russia.dcr.model.DCRCommonObject;
import com.rosbank.android.russia.dcr.model.DCRImageObject;
import com.rosbank.android.russia.dcr.model.DCROpeningHourObject;
import com.rosbank.android.russia.dcr.model.DCRPrivilegeObject;
import com.rosbank.android.russia.dcr.model.DCRRelatedItemObject;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.FontUtils;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/*
 * Created by anh.trinh on 11/03/2017.
 */
public class PrivilegesFragment
        extends BaseFragment
        implements DCRICallback {


    @BindView(R.id.list_privileges)
    ListView lvPrivileges;

    @BindView(R.id.side_index)
    LinearLayout linearLayoutSideIndex;

    public static final String PRIVILEGE_TYPE = "privilege_type";
    @BindView(R.id.scrollIndex)
    ScrollView scrollIndex;

    private String privilegeType = "";
    Map<String, Integer> mapIndex;
    ArrayList<DCRPrivilegeObject> dcrPrivilegeObjectList;
    PrivilegesAdapter privilegeAdapter;
    private int page = 1;
    private int perPage = 20;
    private boolean isLoading = false;
    private boolean isLoadMore = true;
    DCRGetPrivilegesRequest dcrGetPrivilegesRequest = new DCRGetPrivilegesRequest();
    DCRWSGetPrivileges dcrwsGetPrivileges = new DCRWSGetPrivileges(this);

    public PrivilegesFragment() {

    }

    @Override
    protected int layoutId() {
        return R.layout.fragment_list_privileges;
    }

    @Override
    protected void initView() {
        if (getArguments() != null) {
            privilegeType = getArguments().getString(PRIVILEGE_TYPE,
                                                     "");
        }
        privilegeAdapter = new PrivilegesAdapter(getContext(),
                                                 dcrPrivilegeObjectList,
                                                 dcrPrivilegeObjectList,privilegeType);
        lvPrivileges.setAdapter(privilegeAdapter);
        if (dcrPrivilegeObjectList == null) {
            dcrPrivilegeObjectList = new ArrayList<DCRPrivilegeObject>();
        } else {
            dcrPrivilegeObjectList.clear();
        }
        linearLayoutSideIndex.removeAllViews();
        showDialogProgress();
        page = 1;
        getData();
        lvPrivileges.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent,
                                    final View view,
                                    final int position,
                                    final long id) {
                showPrivilegeDetail(dcrPrivilegeObjectList.get(position));
            }
        });
        lvPrivileges.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(final AbsListView view,
                                             final int scrollState) {

            }

            @Override
            public void onScroll(final AbsListView view,
                                 final int firstVisibleItem,
                                 final int visibleItemCount,
                                 final int totalItemCount) {
                int lastInScreen = firstVisibleItem + visibleItemCount;
                if ((lastInScreen == totalItemCount) && isLoadMore && !isLoading) {
                    getData();
                }
            }
        });



    }

    @Override
    protected void bindData() {



    }

    private void getData() {
        if (CommonUtils.isStringValid(privilegeType)) {
            if (privilegeType.equals(DCRPrivilegeObject.PRIVILEGE_TYPE_SPA)) {
                isLoadMore = false;
                hideDialogProgress();
                if (dcrPrivilegeObjectList == null) {
                    dcrPrivilegeObjectList = new ArrayList<>();
                }
                dcrPrivilegeObjectList.addAll(getPrivilegeSpaData());
                initData();
                return;
            }else {
                isLoading = true;
                if (privilegeType.equals(DCRPrivilegeObject.PRIVILEGE_TYPE_RESTAURANT)) {
                /*GET
                        /api/v1/items/search?language=en&item_type=dining&term=lugo&term_operator=OR&exact_
                match=false&sort=Price|Name&facets=City|Country&filter_lat=123123&filter_long=123123&
               filter_distance =3&filter_distance_unit=km*/
                    dcrGetPrivilegesRequest.setItem_type("dining");
                }
                if (privilegeType.equals(DCRPrivilegeObject.PRIVILEGE_TYPE_HOTEL)) {
           /*   /api/v1/items/search?language=en&item_type=accommodation&term=hyatt&term_operator=OR&
              exact_match=false&sort=Price|Name&facets=City|Country&filter_lat=123123&filter_long=123123&
               filter_distance =3&filter_distance_unit=km&filter_price_from=1&filter_price_to=3&filter_price_currency=USD*/
                    dcrGetPrivilegesRequest.setItem_type("accommodation");

                }
                dcrGetPrivilegesRequest.setSort("Name");
                dcrGetPrivilegesRequest.setPage(String.valueOf(page));
                dcrGetPrivilegesRequest.setSize(String.valueOf(perPage));
                dcrwsGetPrivileges.setRequest(dcrGetPrivilegesRequest);
                dcrwsGetPrivileges.run(null);
//                Log.e("LoadMore",
//                      "Page " + page + " PerPage " + perPage);
                page++;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showLogoApp(false);
            if (privilegeType.equals(DCRPrivilegeObject.PRIVILEGE_TYPE_RESTAURANT)) {
                ((HomeActivity) getActivity()).setTitle(getString(R.string.privileges_title_restaurant));
            }
            if (privilegeType.equals(DCRPrivilegeObject.PRIVILEGE_TYPE_HOTEL)) {
                ((HomeActivity) getActivity()).setTitle(getString(R.string.privileges_title_hotel));
            }
            if (privilegeType.equals(DCRPrivilegeObject.PRIVILEGE_TYPE_SPA)) {
                ((HomeActivity) getActivity()).setTitle(getString(R.string.privileges_title_spa));
            }
        }
    }

    @Override
    public boolean onBack() {
        return false;
    }

    private boolean isRussian(List<DCRPrivilegeObject> privilegeObjects) {
        /*boolean isRussian = false;
        if (privilegeObjects != null) {
            for (int i = 0; i < privilegeObjects.size(); i++) {
                DCRPrivilegeObject privilegeObject = privilegeObjects.get(i);
                String index = DCRPrivilegeObject.getAlphabetRu(privilegeObject.getName());
                if (CommonUtils.isStringValid(index) && !index.equals("#")) {
                    isRussian = true;
                    break;
                }
            }
        }
        return isRussian;*/
       //return SharedPreferencesUtils.getPreferences(AppConstant.LANGUAGE_SETTINGS, AppConstant.RUSSIAN_LANGUAGE).equalsIgnoreCase(AppConstant.RUSSIAN_LANGUAGE);
        return true;
    }

    private void getIndexList(List<DCRPrivilegeObject> privilegeObjects) {
        mapIndex = new LinkedHashMap<String, Integer>();
        if (isRussian(privilegeObjects)) {
            for (int i = 0; i < privilegeObjects.size(); i++) {
                DCRPrivilegeObject privilegeObject = privilegeObjects.get(i);
                String index = DCRPrivilegeObject.getAlphabetRu(privilegeObject.getName());
                if (CommonUtils.isStringValid(index) && mapIndex.get(index) == null) {
                    mapIndex.put(index,
                                 i);
                }
            }
        } else {
            for (int i = 0; i < privilegeObjects.size(); i++) {
                DCRPrivilegeObject privilegeObject = privilegeObjects.get(i);
                String index = DCRPrivilegeObject.getAlphabetEn(privilegeObject.getName());
                if (CommonUtils.isStringValid(index) && mapIndex.get(index) == null) {
                    mapIndex.put(index,
                                 i);
                }
            }
        }
    }

    private void displayIndex() {
        linearLayoutSideIndex.removeAllViews();
        TextView textView;
        // List<String> indexList = new ArrayList<String>(mapIndex.keySet());
        String[] indexList;
        if (isRussian(dcrPrivilegeObjectList)) {
            indexList = getResources().getStringArray(R.array.ru_alphabet);
        } else {
            indexList = getResources().getStringArray(R.array.en_alphabet);
        }
        for (String index : indexList) {
            textView = (TextView) getActivity().getLayoutInflater().inflate(R.layout.slide_index_item, null);
            textView.setText(index);
            //CommonUtils.setFontForViewRecursive(textView, FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    if (mapIndex.get(index) != null) {
                        lvPrivileges.setSelection(mapIndex.get(index));
                    } else {
                        if (mapIndex.get(index.toLowerCase()) != null) {
                            lvPrivileges.setSelection(mapIndex.get(index.toLowerCase()));
                        }
                    }

                }
            });
            linearLayoutSideIndex.addView(textView);
        }
    }

    private void initData() {
        getIndexList(dcrPrivilegeObjectList);
        displayIndex();
        if(dcrPrivilegeObjectList!=null && dcrPrivilegeObjectList.size()>9){
            //scrollIndex.setVisibility(View.VISIBLE);
        }else{
            scrollIndex.setVisibility(View.GONE);
        }
        privilegeAdapter.setValues(dcrPrivilegeObjectList);
    }


    @Override
    public void onDCRResponse(final DCRBaseResponse response) {
        hideDialogProgress();
        isLoading = false;
        if (getActivity() == null) {
            return;
        }
        if (response instanceof DCRGetPrivilegesResponse) {
            DCRGetPrivilegesResponse dcrGetPrivilegesResponse = (DCRGetPrivilegesResponse) response;

            if (dcrGetPrivilegesResponse.getData() == null || dcrGetPrivilegesResponse.getData()
                                                                                      .size() == 0) {
                isLoadMore = false;
            }
            dcrPrivilegeObjectList.addAll(dcrGetPrivilegesResponse.getData());
            initData();
        }

    }

    @Override
    public void onDCRResponseOnList(final List<DCRBaseResponse> responseList) {
        hideDialogProgress();
        isLoading = false;
    }

    @Override
    public void onDCRFailure(final String errorMessage,
                             final String errorCode) {
        hideDialogProgress();
        isLoading = false;
    }

    private void showPrivilegeDetail(DCRPrivilegeObject dcrPrivilegeObject) {
        Bundle bundle = new Bundle();
        bundle.putString(PrivilegeDetailBaseFragment.PRIVILEGE_TYPE,privilegeType);
        bundle.putSerializable(PrivilegeDetailBaseFragment.PRIVILEGE_DATA,
                               dcrPrivilegeObject);
        if (privilegeType.equals(DCRPrivilegeObject.PRIVILEGE_TYPE_HOTEL)) {
            PrivilegeDetailHotelFragment fragment =
                    new PrivilegeDetailHotelFragment();
            fragment.setArguments(bundle);

            pushFragment(fragment,
                         true,
                         true);
        } else {
            if (privilegeType.equals(DCRPrivilegeObject.PRIVILEGE_TYPE_RESTAURANT)) {
                PrivilegeDetailRestaurantFragment fragment =
                        new PrivilegeDetailRestaurantFragment();
                fragment.setArguments(bundle);

                pushFragment(fragment,
                             true,
                             true);
            } else {
                if (privilegeType.equals(DCRPrivilegeObject.PRIVILEGE_TYPE_SPA)) {
                    PrivilegeDetailSpaFragment fragment =
                            new PrivilegeDetailSpaFragment();
                    fragment.setArguments(bundle);

                    pushFragment(fragment,
                                 true,
                                 true);
                }
            }

        }
    }

    private List<DCRPrivilegeObject> getPrivilegeSpaData() {
        List<DCRPrivilegeObject> dcrPrivilegeObjects = new ArrayList<>();
        DCRPrivilegeObject object2 = new DCRPrivilegeObject();
        object2.setId("34");
        object2.setStatus(new DCRCommonObject("Active"));
        object2.setItem_type(new DCRCommonObject("Complimentary"));
        object2.setName("Float Studio");
        object2.setWebsite_url("http://floatstudio.ru/");
        object2.setLatitude("55.740572");
        object2.setLongitude("37.633522");
        object2.setPhone_number(" +7 499 110 52 10");
        List<DCRCommonObject> types2 = new ArrayList<>();
        DCRCommonObject type21 = new DCRCommonObject("Massage");
        DCRCommonObject type22 = new DCRCommonObject("Day Spa");
        DCRCommonObject type23 = new DCRCommonObject("Medispas");
        types2.add(type21);
        types2.add(type22);
        types2.add(type23);
        object2.setTypes(types2);
        object2.setAddress_line_1("ул. Большая Татарская, д. 7");
        object2.setAddress_post_code("115184");
        object2.setAddress_city(new DCRCommonObject("Москва"));
        object2.setAddress_country(new DCRCommonObject("Российская Федерация"));
        object2.setSummary("Флоат Студия - это современное пространство расслабления и оздоровления в самом центре Москвы.");
        object2.setDescription("Флоатинг – это уникальный терапевтический метод достижения глубинной релаксации в специальной флоат-комнате. Процедура проходит в бассейне с солевым раствором, который позволяет телу «парить» на поверхности. Флоатинг активно используется не только в качестве релакс-процедуры, он благотворно влияет на весь организм целиком и имеет мощный косметологический эффект. В то время как каждая мышца тела отдыхает, ваше давление и сердечный ритм приходят в норму, органы насыщаются кислородом, а состояние кожи заметно улучшается."
                    +"<br><br>"+"Yслуги - Уход за лицом, Комплексный уход за лицом/телом, Мануальная терапия, Оздоровительные процедуры\n");
        object2.setDirection("https://goo.gl/maps/jqXjwdqQxsG2");
        object2.setSpa_service("Уход за лицом, Комплексный уход за лицом/телом, Мануальная терапия, Оздоровительные процедуры");
        List<DCROpeningHourObject> dcrOpeningHourObjects2 = new ArrayList<>();
       /* DCROpeningHourObject openingHourObject21 = new DCROpeningHourObject("Пн",
                                                                            "10:00 AM",
                                                                            "Вс",

                                                                            "00:00  AM");*/
        DCROpeningHourObject openingHourObject22 = new DCROpeningHourObject("Пн",
                                                                            "10:00 ",
                                                                            "Вс",

                                                                            "22:00 ");
       // dcrOpeningHourObjects2.add(openingHourObject21);
        dcrOpeningHourObjects2.add(openingHourObject22);
        object2.setOpening_hours(dcrOpeningHourObjects2);
        // set image
        List<DCRImageObject> dcrImageObjects2 = new ArrayList<>();
        DCRImageObject imageObject21 = new DCRImageObject();
        imageObject21.setLocalPath("float_studio_1");
        DCRImageObject imageObject22 = new DCRImageObject();
        imageObject22.setLocalPath("float_studio_2");
        dcrImageObjects2.add(imageObject21);
        dcrImageObjects2.add(imageObject22);
        object2.setImages(dcrImageObjects2);

        DCRRelatedItemObject p = new DCRRelatedItemObject("Скидка 10% на все услуги");
        DCRCommonObject type = new DCRCommonObject("privilege");
        type.setCode("privilege");
        p.setType(type);
        p.setDescription(getString(R.string.privilege_fulfillment_instruction) + "<br><br>"
                + "Для того, чтобы воспользоваться скидкой, пожалуйста, обратитесь в Консьерж-службу");
        List<DCRRelatedItemObject> relatedItems = new ArrayList<>();
        relatedItems.add(p);
        object2.setRelated_items(relatedItems);

        dcrPrivilegeObjects.add(object2);

        //---------------------------------------------
        DCRPrivilegeObject object1 = new DCRPrivilegeObject();
        object1.setId("32");
        object1.setStatus(new DCRCommonObject("Active"));
        object1.setItem_type(new DCRCommonObject("Complimentary"));
        object1.setName("Mahash SPA в отеле Intercontinental");
        object1.setWebsite_url("http://www.mahash.ru/mahash/index.php");
        object1.setLatitude("55.767371");
        object1.setLongitude("37.601051");
        object1.setPhone_number(" +7 499 951 95 03");
        List<DCRCommonObject> types = new ArrayList<>();
        DCRCommonObject type1 = new DCRCommonObject("Salons");
        DCRCommonObject type2 = new DCRCommonObject("Massage");
        DCRCommonObject type3 = new DCRCommonObject("Day Spa");
        types.add(type1);
        types.add(type2);
        types.add(type3);
        object1.setTypes(types);
        object1.setAddress_line_1("ул. Тверская, д. 22, гостиница Intercontinental, -1 этаж");
        object1.setAddress_post_code("127006");
        object1.setAddress_city(new DCRCommonObject("Москва"));
        object1.setAddress_country(new DCRCommonObject("Российская Федерация"));
        object1.setSummary("MAHASH – это ведущий международный бренд, представляющий СПА салоны, салоны красоты и собственную органическую марку косметики в Москве и Европе.");
        object1.setDescription("MAHASH – это всегда шанс оторваться от действительности, чтобы найти себя или вернуться к себе в душевном состоянии, в образе или в стиле. Место, где можно расслабиться и отдохнуть. Источник восстановления внутренних сил человека, целебной энергетики и мудрости, накопленных веками аюрведических и восточных практик, слитых воедино в эстетике растительной косметики." +
                "<br><br>"+"Yслуги - услугУход за лицом, Комплексный уход за лицом/телом, Обертывание, Мануальная терапия"
                );
        object1.setDirection("https://goo.gl/maps/a8GR7E58GZq");
        /*object1.setInternal_label(new DCRCommonObject("Iconic"));*/
        object1.setSpa_service("Уход за лицом, Комплексный уход за лицом/телом, Обертывание, Мануальная терапия");
        List<DCROpeningHourObject> dcrOpeningHourObjects1 = new ArrayList<>();
        DCROpeningHourObject openingHourObject1 = new DCROpeningHourObject("Пн",
                                                                           "10:00 ",
                                                                           "Вс",
                                                                           "24:00 ");
        /*DCROpeningHourObject openingHourObject2 = new DCROpeningHourObject("Пн",
                                                                           "10:00 AM",
                                                                           "Вс",
                                                                           "10:00 PM");*/
        dcrOpeningHourObjects1.add(openingHourObject1);
       // dcrOpeningHourObjects1.add(openingHourObject2);
        object1.setOpening_hours(dcrOpeningHourObjects1);
        // set images
        List<DCRImageObject> dcrImageObjects = new ArrayList<>();
        DCRImageObject imageObject1 = new DCRImageObject();
        imageObject1.setLocalPath("mahash_spa_1");
        DCRImageObject imageObject2 = new DCRImageObject();
        imageObject2.setLocalPath("mahash_spa_2");
        dcrImageObjects.add(imageObject1);
        dcrImageObjects.add(imageObject2);
        object1.setImages(dcrImageObjects);
        dcrPrivilegeObjects.add(object1);

        DCRRelatedItemObject p1 = new DCRRelatedItemObject("Скидка 30%  на все СПА-процедуры для участников программы MAHASH Corporate Wellness");
        p1.setType(type);
        p1.setDescription(getString(R.string.privilege_fulfillment_instruction) + "<br><br>"
                + "Для того, чтобы воспользоваться скидкой, пожалуйста, обратитесь в Консьерж-службу");
        List<DCRRelatedItemObject> relatedItems2 = new ArrayList<>();
        relatedItems2.add(p1);

        DCRRelatedItemObject p2 = new DCRRelatedItemObject("Скидка 15% на все услуги салона при единоразовом посещении");
        p2.setType(type);
        p2.setDescription(getString(R.string.privilege_fulfillment_instruction) + "<br><br>"
                + "Для того, чтобы воспользоваться скидкой, пожалуйста, обратитесь в Консьерж-службу");
        relatedItems2.add(p2);

        object1.setRelated_items(relatedItems2);

        return dcrPrivilegeObjects;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater,
                                           container,
                                           savedInstanceState);
        ButterKnife.bind(this,
                         rootView);
        return rootView;
    }
}
