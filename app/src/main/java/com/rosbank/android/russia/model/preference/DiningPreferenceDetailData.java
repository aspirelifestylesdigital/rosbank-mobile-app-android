package com.rosbank.android.russia.model.preference;

import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CGetPreferenceResponse;
import com.rosbank.android.russia.application.AppConstant;

/**
 * Created by ThuNguyen on 12/13/2016.
 */

public class DiningPreferenceDetailData extends PreferenceData{
    private String cuisinePreference;
    private String otherPreference;
    private String foodAllergies;
    public DiningPreferenceDetailData(B2CGetPreferenceResponse response){
        super(response);
        preferenceType = AppConstant.PREFERENCE_TYPE.DINING;
        if(response != null){
            cuisinePreference = response.getVALUE();
            otherPreference = response.getVALUE1();
            foodAllergies = response.getVALUE2();
        }
    }

    public String getCuisinePreference() {
        return cuisinePreference;
    }

    public void setCuisinePreference(String cuisinePreference) {
        this.cuisinePreference = cuisinePreference;
    }

    public String getOtherPreference() {
        return otherPreference;
    }

    public void setOtherPreference(String otherPreference) {
        this.otherPreference = otherPreference;
    }

    public void setFoodAllergies(String foodAllergies) {
        this.foodAllergies = foodAllergies;
    }

    public String getFoodAllergies() {
        return foodAllergies;
    }
}
