package com.rosbank.android.russia.apiservices.apis.appimplement;

import com.rosbank.android.russia.apiservices.RequestModel.BookHotelRequest;
import com.rosbank.android.russia.apiservices.RequestModel.CancelRequestRequest;
import com.rosbank.android.russia.apiservices.RequestModel.ChangePasswordRequest;
import com.rosbank.android.russia.apiservices.RequestModel.ExperienceGourmetRequest;
import com.rosbank.android.russia.apiservices.RequestModel.FavouriteRequest;
import com.rosbank.android.russia.apiservices.RequestModel.ForgotPassRequest;
import com.rosbank.android.russia.apiservices.RequestModel.GetListMyRequestRequest;
import com.rosbank.android.russia.apiservices.RequestModel.GetMyProfileRequest;
import com.rosbank.android.russia.apiservices.RequestModel.HomeGourmetRequest;
import com.rosbank.android.russia.apiservices.RequestModel.SaveMyCurrencyRequest;
import com.rosbank.android.russia.apiservices.RequestModel.SignInRequest;
import com.rosbank.android.russia.apiservices.RequestModel.SignUpRequest;
import com.rosbank.android.russia.apiservices.RequestModel.UpdateHotelPreferencesRequest;
import com.rosbank.android.russia.apiservices.RequestModel.UpdateMyProfileRequest;
import com.rosbank.android.russia.apiservices.RequestModel.UpdateTransportPreferencesRequest;
import com.rosbank.android.russia.apiservices.RequestModel.UpdategGolfRequest;
import com.rosbank.android.russia.apiservices.RequestModel.UpdategGourmetRequest;
import com.rosbank.android.russia.apiservices.RequestModel.UpdategOtherPreferencesRequest;
import com.rosbank.android.russia.apiservices.ResponseModel.AddWishListResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.AuthenticateResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.BedPreferencesResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.BookGolfResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.BookHotelRecommendResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.BookHotelResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.BookRentalResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.BookRestaurantResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.BookTransferResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.CancelRequestResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.CarRentalBookingDetailResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.CarTransferBookingDetailResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.ChangePasswordResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.ChatbotResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.CuisineGourmetResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.CuisineTypeResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.GetCitiesResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.GetCountriesResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.GetExperienceEntertainmentResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.GetExperienceGourmetDetailResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.GetExperienceGourmetResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.GetExperienceLifeStyleResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.GetExperienceTravelResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.GetListMyRequestResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.GetMyCurrencyResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.GetMyPreferenceResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.GolfBookingDetailResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.HomeGourmetResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.HotelBookingDetailResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.HotelRecommendationBookingDetailResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.OccasionResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.OtherBookingDetailResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.PreferrelTeeTimeResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.RatingPreferencesResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.RemoveWishListResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.RentalResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.RestaurantBookingDetailResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.RestaurantRecommendationBookingDetailResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.RoomTypePreferencesResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.SaveMyCurrencyResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.SignInResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.SignUpResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.SmokingRoomPreferencesResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.TransportTypesResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.UpdateGolfResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.UpdateGourmetResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.UpdateHotelPreferencesResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.UpdateOtherPreferencesResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.UpdateTransportPreferencesResponse;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.B2CUpsertPreferenceRequest;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.B2CChangePasswordRequest;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.B2CForgotPasswordRequest;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.B2CGetConciergeRequestDetailRequest;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.B2CGetPreferenceRequest;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.B2CGetRecentRequestRequest;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.B2CGetUserDetailsRequest;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.B2CLoginRequest;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.B2COtherBookingRequest;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.B2CRegistrationRequest;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.B2CUpdateRegistrationRequest;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.B2CUpsertConciergeRequestRequest;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CAccessTokenResponse;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CUpsertPreferenceResponse;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CBookingResponse;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CChangePasswordResponse;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CForgotPasswordResponse;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CGetPreferenceResponse;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CGetRecentRequestResponse;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CGetUserDetailsResponse;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CLoginResponse;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CRefreshTokenResponse;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CRegistrationResponse;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CRequestTokenResponse;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CUpdateRegistrationResponse;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CUploadFileResponse;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CUpsertConciergeRequestResponse;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by nga.nguyent on 9/15/2016.
 */
public interface ApiInterface {

    /**
     * DEFINE API NAME
     * ======================================================
     */
    String WS_AUTHENTICATE = "/api/identity/Authenticate";
    String WS_ACCOUNT_SIGN_IN = "/api/identity/SignIn";
    String WS_ACCOUNT_SIGN_UP = "/api/identity/signup";
    String WS_HOME_GOURMET = "/api/Restaurants/GetRestaurants";
    String WS_ACCOUNT_FORGOT_SECRET = "/api/identity/ForgotPassword";
    String WS_ADD_TO_WISHLIST = "/api/mywishlist/AddToWishlist";
    String WS_REMOVE_TO_WISHLIST = "/api/mywishlist/RemoveMyWishlist";
    String WS_GET_MY_PROFILE = "/api/identity/GetMyProfile";
    String WS_UPDATE_MY_PROFILE = "/api/identity/UpdateMyProfile";
    String WS_GET_RENTAL_VEHICLE_TYPE_PREFERENCES = "/api/common/RentalVehicleTypePreferences";
    String WS_GET_RENTAL_COMPANY_PREFERENCES = "/api/common/RentalCompanyPreferences";
    String WS_GET_CUISINETYPE = "/api/common/CuisinePreferences";
    String WS_CREATE_UPDATE_TRANSPORT_PREFERENCES =
            "/api/identity/CreateUpdateTransportPreferences";
    String WS_GET_MY_PREFERENCES = "/api/identity/GetMyPreference";
    String WS_UPDATE_GOURMET = "/api/identity/CreateUpdateGourmetPreferences";
    String WS_GET_PRERERRED_TEE_TIMES = "/api/common/TeeTimePreferences";
    String WS_UPDATE_GOLF = "/api/identity/CreateUpdateGolfPreferences";

    String WS_GET_RENTAL_RATING_PREFERENCES = "/api/common/RatingPreferences";
    String WS_GET_RENTAL_ROOM_TYPE_PREFERENCES = "/api/common/RoomTypePreferences";
    String WS_GET_RENTAL_BED_TYPE_PREFERENCES = "/api/common/BedPreferences";
    String WS_GET_RENTAL_SMOKING_ROOM_PREFERENCES = "/api/common/SmokingRoomPreferences";
    String WS_UPDATE_HOTEL_PREFERENCES = "/api/identity/CreateUpdateHotelPreferences";
    String WS_UPDATE_OTHER_PREFERENCE = "/api/identity/CreateUpdateOthersPreferences";

    String WS_GET_MY_CURRENCY = "/api/identity/GetMyCurrency";
    String WS_SAVE_MY_CURRENCY = "/api/identity/SaveMyCurrency";

    String WS_CHANGE_SECRET = "/api/identity/ChangePassword";
    String WS_BOOK_HOTEL = "/api/Travel/BookHotel";
    String WS_BOOK_HOTEL_RECOMMEND = "/api/Travel/BookHotelRecommendation";
    String WS_GET_HOTEL_BOOKING_DETAIL = "/api/Travel/GetHotelBookingDetail";
    String WS_GET_HOTEL_RECOMMENDATION_BOOKING_DETAIL = "/api/Travel/GetHotelRecommendationBookingDetail";
    String WS_BOOK_RESTAURANT = "/api/Restaurants/BookRestaurant";
    String WS_BOOK_RESTAURANT_RECOMMEND = "/api/Restaurants/BookRestaurantRecommendation";
    String WS_GET_RESTAURANT_BOOKING_DETAIL = "/api/Restaurants/GetRestaurantBookingDetail";
    String WS_GET_RESTAURANT_RECOMMENDATION_BOOKING_DETAIL = "/api/Restaurants/GetRestaurantRecommendationBookingDetail";

    String WS_GET_COUNTRIES = "/api/Geos/GetCountries";
    String WS_GET_CITIES = "/api/Geos/GetCities";
    String WS_GET_OCCASION = "/api/Restaurants/getoccasions";
    String WS_GET_CUISINE_GOURMET = "/api/Restaurants/GetCuisines";
    String WS_GET_TRANSPORT_TYPES = "/api/common/TransportTypes";
    String WS_BOOK_RENTAL = "/api/Travel/BookCarRental";
    String WS_GET_CAR_RENTAL_BOOKING_DETAIL = "/api/Travel/GetCarRentalBookingDetail";
    String WS_BOOK_TRANSFER = "/api/Travel/BookCarTransfer";
    String WS_GET_CAR_TRANSFER_BOOKING_DETAIL = "/api/Travel/GetCarTransferBookingDetail";
    String WS_CHATBOT = "https://chatbot.s3corp.com.vn:3005/Program-O/chatbot/conversation_start.php";
    String WS_BOOK_GOLF = "/api/LifeStyle/BookGolf";
    String WS_GET_GOLF_BOOKING_DETAIL = "/api/LifeStyle/GetGolfBookingDetail";
    String WS_BOOK_OTHER = "ManageRequest/CreateConciergeRequest";
    String WS_GET_OTHER_BOOKING_DETAIL = "/api/Others/GetOtherBookingDetail";

    String WS_GET_EXPERIENCES_ENTERTAINMENT = "/api/statictemp/GetEntertainments";
    String WS_GET_EXPERIENCES_LIFESTYLE= "/api/statictemp/getlifestyles";
    String WS_GET_EXPERIENCES_TRAVEL= "/api/statictemp/GetTravels";
    String WS_GET_EXPERIENCES_GOURMET_DETAIL= "/api/Restaurants/GetRestaurantDetail";
    String WS_GET_MY_REQUESTS = "/api/identity/GetMyRequests";
    String WS_CANCEL_MY_REQUEST = "/api/identity/CancelMyRequest";

    /**
     * =====================================================
     */
    String WS_API_QUERY_USERID = "UserID";
    String WS_API_QUERY_COUNTRY = "Country";
    String WS_API_QUERY_BOOKINGID = "BookingId";
    String WS_QUERY_FORMAT = "format";
    String WS_QUERY_UID = "uid";
    String WS_QUERY_USNAME = "usName";
    String WS_QUERY_SAY = "say";
    String WS_QUERY_BOT_ID = "bot_id";
    String WS_QUERY_COUNTRIES = "countries";
    String WS_QUERY_CITIES = "cities";
    String WS_QUERY_LOCATION_REQUEST = "location_request";
    String WS_QUERY_LAT = "lat";
    String WS_QUERY_LONG = "long";
    String WS_QUERY_CONVO_ID = "convo_id";
    String WS_QUERY_LOCATION = "location";
    String WS_QUERY_RESTAURANT_ID = "RestaurantId";


    /**
     * DEFINE API METHOD
     * ======================================================
     */

    @POST(WS_AUTHENTICATE)
    Call<AuthenticateResponse> authenticate(@Query("UserID") String username,
                                            @Query("Password") String secret);

    @Headers("Content-Type: application/json")
    @POST(WS_ACCOUNT_SIGN_IN)
    Call<SignInResponse> userLogin(
                                          @Body SignInRequest request);

    @Headers("Content-Type: application/json")
    @POST(WS_ACCOUNT_SIGN_UP)
    Call<SignUpResponse> userSignUp(@Body SignUpRequest request);

    @POST(WS_HOME_GOURMET)
    Call<HomeGourmetResponse> getHomeGourmet(@Body HomeGourmetRequest request);

    @Headers("Content-Type: application/json")
    @POST(WS_ACCOUNT_FORGOT_SECRET)
    Call<BaseResponse> userForgotSecret(
                                                 @Body ForgotPassRequest request);

    @Headers("Content-Type: application/json")
    @POST(WS_ADD_TO_WISHLIST)
    Call<AddWishListResponse> addWishList(
                                                 @Body FavouriteRequest request);

    @Headers("Content-Type: application/json")
    @POST(WS_REMOVE_TO_WISHLIST)
    Call<RemoveWishListResponse> removeWishList(
                                                       @Body FavouriteRequest request);

    @Headers("Content-Type: application/json")
    @POST(WS_GET_MY_PROFILE)
    Call<SignUpResponse> getMyProfile(
                                             @Body GetMyProfileRequest request);

    @Headers("Content-Type: application/json")
    @POST(WS_UPDATE_MY_PROFILE)
    Call<BaseResponse> updateMyProfile(@Body UpdateMyProfileRequest request);

    @Headers("Content-Type: application/json")
    @GET(WS_GET_RENTAL_VEHICLE_TYPE_PREFERENCES)
    Call<RentalResponse> getRentalVehicleTypePreferences();

    @Headers("Content-Type: application/json")
    @GET(WS_GET_RENTAL_COMPANY_PREFERENCES)
    Call<RentalResponse> getRentalCompanyPreferences();

    @Headers("Content-Type: application/json")
    @GET(WS_GET_CUISINETYPE)
    Call<CuisineTypeResponse> getCuisineType();

    @Headers("Content-Type: application/json")
    @POST(WS_CREATE_UPDATE_TRANSPORT_PREFERENCES)
    Call<UpdateTransportPreferencesResponse> updateTransportPreferencesResponse(@Body
                                                                                UpdateTransportPreferencesRequest request);

    @Headers("Content-Type: application/json")
    @GET(WS_GET_MY_PREFERENCES)
    Call<GetMyPreferenceResponse> getMyPreferences(@Query(WS_API_QUERY_USERID) String userId);

    @Headers("Content-Type: application/json")
    @POST(WS_UPDATE_GOURMET)
    Call<UpdateGourmetResponse> updateGourmetPreferences(@Body UpdategGourmetRequest request);

    @Headers("Content-Type: application/json")
    @GET(WS_GET_PRERERRED_TEE_TIMES)
    Call<PreferrelTeeTimeResponse> getPreferredTeeTimes();

    @Headers("Content-Type: application/json")
    @POST(WS_UPDATE_GOLF)
    Call<UpdateGolfResponse> updateGolfPreferences(@Body UpdategGolfRequest request);


    @Headers("Content-Type: application/json")
    @GET(WS_GET_RENTAL_RATING_PREFERENCES)
    Call<RatingPreferencesResponse> getRatingPreferences();

    @Headers("Content-Type: application/json")
    @GET(WS_GET_RENTAL_ROOM_TYPE_PREFERENCES)
    Call<RoomTypePreferencesResponse> getRoomTypePreferences();

    @Headers("Content-Type: application/json")
    @GET(WS_GET_RENTAL_BED_TYPE_PREFERENCES)
    Call<BedPreferencesResponse> getBedTypePreferences();

    @Headers("Content-Type: application/json")
    @GET(WS_GET_RENTAL_SMOKING_ROOM_PREFERENCES)
    Call<SmokingRoomPreferencesResponse> getSmookingRoomPreferences();

    @Headers("Content-Type: application/json")
    @POST(WS_UPDATE_HOTEL_PREFERENCES)
    Call<UpdateHotelPreferencesResponse> updateHotelPreferencesResponse(@Body
                                                                        UpdateHotelPreferencesRequest request);

    @Headers("Content-Type: application/json")
    @POST(WS_UPDATE_OTHER_PREFERENCE)
    Call<UpdateOtherPreferencesResponse> updateOtherPreferences(@Body
                                                                UpdategOtherPreferencesRequest request);


    @Headers("Content-Type: application/json")
    @GET(WS_GET_MY_CURRENCY)
    Call<GetMyCurrencyResponse> getMyCurrency(@Query(WS_API_QUERY_USERID) String userId);

    @Headers("Content-Type: application/json")
    @POST(WS_SAVE_MY_CURRENCY)
    Call<SaveMyCurrencyResponse> saveMyCurrency(@Body SaveMyCurrencyRequest request);


    @Headers("Content-Type: application/json")
    @POST(WS_CHANGE_SECRET)
    Call<ChangePasswordResponse> changePassWord(@Body ChangePasswordRequest request);

    @Headers("Content-Type: application/json")
    @POST(WS_BOOK_HOTEL)
    Call<BookHotelResponse> bookHotel(@Body BookHotelRequest request);

    @Multipart
    @POST(WS_BOOK_HOTEL)
    Call<BookHotelResponse> bookHotel(@Part("BookingId") RequestBody BookingId,
                                      @Part("UserID") RequestBody UserID,
                                      @Part("BookingItemId") RequestBody BookingItemId,
                                      @Part MultipartBody.Part UploadPhoto,
                                      @Part("Phone") RequestBody Phone,
                                      @Part("Email") RequestBody Email,
                                      @Part("Both") RequestBody Both,
                                      @Part("MobileNumber") RequestBody MobileNumber,
                                      @Part("EmailAddress") RequestBody EmailAddress,
                                      @Part("CheckInDate") RequestBody CheckInDate,
                                      @Part("CheckOutDate") RequestBody CheckOutDate,
                                      @Part("RoomType") RequestBody RoomType,
                                      @Part("GuestName") RequestBody GuestName,
                                      @Part("LoyaltyProgrammeMembership")
                                      RequestBody LoyaltyProgrammeMembership,
                                      @Part("Country") RequestBody Country,
                                      @Part("City") RequestBody City,
                                      @Part("NumberOfAdults") RequestBody NumberOfAdults,
                                      @Part("NumberOfKids") RequestBody NumberOfKids,
                                      @Part("SmokingPreference") RequestBody SmokingPreference,
                                      @Part("SpecialRequirements") RequestBody SpecialRequirements
                                     );
    @Headers("Content-Type: application/json")
    @GET(WS_GET_HOTEL_BOOKING_DETAIL)
    Call<HotelBookingDetailResponse> getHotelBookingDetail(@Query(WS_API_QUERY_BOOKINGID) String bookingId);

    @Multipart
    @POST(WS_BOOK_HOTEL_RECOMMEND)
    Call<BookHotelRecommendResponse> bookHotelRecommend(@Part("BookingId") RequestBody BookingId,
                                                        @Part("UserID") RequestBody UserID,
                                                        @Part MultipartBody.Part UploadPhoto,
                                                        @Part("Phone") RequestBody Phone,
                                                        @Part("Email") RequestBody Email,
                                                        @Part("Both") RequestBody Both,
                                                        @Part("MobileNumber")
                                                        RequestBody MobileNumber,
                                                        @Part("EmailAddress")
                                                        RequestBody EmailAddress,
                                                        @Part("CheckInDate")
                                                        RequestBody CheckInDate,
                                                        @Part("CheckOutDate")
                                                        RequestBody CheckOutDate,
                                                        @Part("RoomType") RequestBody RoomType,
                                                        @Part("StarRating") RequestBody StarRating,
                                                        @Part("MinimumPrice")
                                                        RequestBody MinimumPrice,
                                                        @Part("MaximumPrice")
                                                        RequestBody MaximumPrice,
                                                        @Part("Country") RequestBody Country,
                                                        @Part("City") RequestBody City,
                                                        @Part("NumberOfAdults")
                                                        RequestBody NumberOfAdults,
                                                        @Part("NumberOfKids")
                                                        RequestBody NumberOfKids,
                                                        @Part("SmokingPreference")
                                                        RequestBody SmokingPreference,
                                                        @Part("SpecialRequirements")
                                                        RequestBody SpecialRequirements
                                                       );
    @Headers("Content-Type: application/json")
    @GET(WS_GET_HOTEL_RECOMMENDATION_BOOKING_DETAIL)
    Call<HotelRecommendationBookingDetailResponse> getHotelRecommendationBookingDetail(@Query(WS_API_QUERY_BOOKINGID) String bookingId);
    @Multipart
    @POST(WS_BOOK_RENTAL)
    Call<BookRentalResponse> bookRental(@PartMap()
                                               Map<String, RequestBody> partMap,
                                               @Part MultipartBody.Part file);
    @Headers("Content-Type: application/json")
    @GET(WS_GET_CAR_RENTAL_BOOKING_DETAIL)
    Call<CarRentalBookingDetailResponse> getCarRentalBookingDetail(@Query(WS_API_QUERY_BOOKINGID) String bookingId);
    @Multipart
    @POST(WS_BOOK_TRANSFER)
    Call<BookTransferResponse> bookTransfer(@Part("BookingId") RequestBody BookingId,
                                            @Part("UserID") RequestBody UserID,
                                            @Part MultipartBody.Part UploadPhoto,
                                            @Part("Phone") RequestBody Phone,
                                            @Part("Email") RequestBody Email,
                                            @Part("Both") RequestBody Both,
                                            @Part("MobileNumber")
                                            RequestBody MobileNumber,
                                            @Part("EmailAddress")
                                            RequestBody EmailAddress,
                                            @Part("TransportType")
                                            RequestBody TransportType,
                                            @Part("EpochPickupDate") RequestBody EpochPickupDate,
                                            @Part("PickupTime")
                                            RequestBody PickupTime,
                                            @Part("PickupLocation")
                                            RequestBody PickupLocation,
                                            @Part("DropOffLocation")
                                            RequestBody DropOffLocation,
                                            @Part("NbOfPassenger")
                                            RequestBody NbOfPassenger,
                                            @Part("SpecialRequirements")
                                            RequestBody SpecialRequirements
                                           );
    @Headers("Content-Type: application/json")
    @GET(WS_GET_CAR_TRANSFER_BOOKING_DETAIL)
    Call<CarTransferBookingDetailResponse> getCarTransferBookingDetail(@Query(WS_API_QUERY_BOOKINGID) String bookingId);
    @Multipart
    @POST(WS_BOOK_GOLF)
    Call<BookGolfResponse> bookGolf(@Part("BookingId") RequestBody BookingId,
                                    @Part("UserID") RequestBody UserID,
                                    @Part MultipartBody.Part UploadPhoto,
                                    @Part("Phone") RequestBody Phone,
                                    @Part("Email") RequestBody Email,
                                    @Part("Both") RequestBody Both,
                                    @Part("MobileNumber")
                                    RequestBody MobileNumber,
                                    @Part("EmailAddress")
                                    RequestBody EmailAddress,
                                    @Part("GolfCourseName")
                                    RequestBody GolfCourseName,
                                    @Part("Date") RequestBody Date,
                                    @Part("StartTime")
                                    RequestBody StartTime,
                                    @Part("NbOfHours")
                                    RequestBody NbOfHours,
                                    @Part("Country")
                                    RequestBody Country,
                                    @Part("City")
                                    RequestBody City,
                                    @Part("NumberOfAdults")
                                    RequestBody NumberOfAdults,
                                    @Part("SpecialRequirements")
                                    RequestBody SpecialRequirements
                                   );
    @Headers("Content-Type: application/json")
    @GET(WS_GET_GOLF_BOOKING_DETAIL)
    Call<GolfBookingDetailResponse> getGolfBookingDetail(@Query(WS_API_QUERY_BOOKINGID) String bookingId);

    @Headers("Content-Type: application/json")
    @POST(WS_BOOK_OTHER)
    Call<B2CBookingResponse> bookOther(@Body B2COtherBookingRequest request);

    //@Headers("Content-Type: application/json")
    @GET(WS_GET_OTHER_BOOKING_DETAIL)
    Call<OtherBookingDetailResponse> getOtherBookingDetail(@Query(WS_API_QUERY_BOOKINGID) String bookingId);
    /*@Headers("Content-Type: application/json")
    @POST(WS_BOOK_RESTAURANT)
    Call<BookRestaurantResponse> bookRestaurant(@Body BookRestaurantRequest request);*/

    @Headers("Content-Type: application/json")
    @GET(WS_GET_COUNTRIES)
    Call<GetCountriesResponse> getCountries();

    @Headers("Content-Type: application/json")
    @GET(WS_GET_RESTAURANT_BOOKING_DETAIL)
    Call<RestaurantBookingDetailResponse> getRestaurantBookingDetail(@Query(WS_API_QUERY_BOOKINGID) String bookingId);

    @Headers("Content-Type: application/json")
    @GET(WS_GET_RESTAURANT_RECOMMENDATION_BOOKING_DETAIL)
    Call<RestaurantRecommendationBookingDetailResponse> getRestaurantRecommendationBookingDetail(@Query(WS_API_QUERY_BOOKINGID) String bookingId);

    @Multipart
    @POST(WS_BOOK_RESTAURANT)
    Call<BookRestaurantResponse> bookRestaurant(@Part("BookingId") RequestBody BookingId,
                                                @Part("UserID") RequestBody UserID,
                                                @Part("BookingItemId") RequestBody BookingItemId,
                                                @Part MultipartBody.Part UploadPhoto,
                                                @Part("Phone") RequestBody Phone,
                                                @Part("Email") RequestBody Email,
                                                @Part("Both") RequestBody Both,
                                                @Part("MobileNumber") RequestBody MobileNumber,
                                                @Part("EmailAddress") RequestBody EmailAddress,
                                                @Part("ReservationDate")
                                                RequestBody ReservationDate,
                                                @Part("ReservationTime")
                                                RequestBody ReservationTime,
                                                @Part("ReservationName")
                                                RequestBody ReservationName,
                                                @Part("Country") RequestBody Country,
                                                @Part("City") RequestBody City,
                                                @Part("NumberOfAdults") RequestBody NumberOfAdults,
                                                @Part("NumberOfKids") RequestBody NumberOfKids,
                                                @Part("SpecialRequirements")
                                                RequestBody SpecialRequirements
                                               );
    @Headers("Content-Type: application/json")
    @GET(WS_GET_CITIES)
    Call<GetCitiesResponse> getCities(@Query(WS_API_QUERY_COUNTRY) String countryCode);
    @Headers("Content-Type: application/json")
    @GET(WS_GET_OCCASION)
    Call<OccasionResponse> getOccasionList();

    @Headers("Content-Type: application/json")
    @GET(WS_GET_CUISINE_GOURMET)
    Call<CuisineGourmetResponse> getCuisineGourmet();

    @Multipart
    @POST(WS_BOOK_RESTAURANT_RECOMMEND)
    Call<BookRestaurantResponse> bookRestaurantRecommend(
                                                                @PartMap()
                                                                Map<String, RequestBody> partMap,
                                                                @Part MultipartBody.Part file);

    @Headers("Content-Type: application/json")
    @GET(WS_GET_TRANSPORT_TYPES)
    Call<TransportTypesResponse> getTransportTypes();

    @POST(WS_HOME_GOURMET)
    Call<GetExperienceGourmetResponse> getExperiencesGourmet(@Body ExperienceGourmetRequest request);

    @Headers("Content-Type: application/json")
    @GET(WS_GET_EXPERIENCES_GOURMET_DETAIL)
    Call<GetExperienceGourmetDetailResponse> getExperiencesGourmetDetail(@Query(WS_QUERY_RESTAURANT_ID) String restaurantId,
                                                                         @Query(WS_API_QUERY_USERID) String UserID);

    @Headers("Content-Type: application/json")
    @GET(WS_GET_EXPERIENCES_ENTERTAINMENT)
    Call<GetExperienceEntertainmentResponse> getExperiencesEntertainment();

    @Headers("Content-Type: application/json")
    @GET(WS_GET_EXPERIENCES_LIFESTYLE)
    Call<GetExperienceLifeStyleResponse> getExperiencesLifeStyle();

    @Headers("Content-Type: application/json")
    @GET(WS_GET_EXPERIENCES_TRAVEL)
    Call<GetExperienceTravelResponse> getExperiencesTravel();


    @GET
    Call<ChatbotResponse> chatBot(@Url String url);
    /*Call<ChatbotResponse> chatBot(@Url String url,
                                  @Query(WS_QUERY_CONVO_ID) String convoid,
                                  @Query(WS_QUERY_FORMAT) String format,
                               @Query(WS_QUERY_UID) String uid,
                               @Query(WS_QUERY_USNAME) String usname,
                               @Query(WS_QUERY_SAY) String say,
                               @Query(WS_QUERY_BOT_ID) String botid,
                                  @Query(WS_QUERY_COUNTRIES) String country,
                                  @Query(WS_QUERY_CITIES) String cities,
                                  @Query(WS_QUERY_LOCATION_REQUEST) int locationRequest,
                                  @Query(WS_QUERY_LAT) String lati,
                                  @Query(WS_QUERY_LONG) String longi);*/

    @Headers("Content-Type: application/json")
    @POST(WS_GET_MY_REQUESTS)
    Call<GetListMyRequestResponse> getMyRequest(
                                             @Body GetListMyRequestRequest request);

    @Headers("Content-Type: application/json")
    @POST(WS_CANCEL_MY_REQUEST)
    Call<CancelRequestResponse> cancelRequest(@Body CancelRequestRequest request);

//============================================================================================================
/***********************************************************************************************************/
/***********************************************************************************************************/
/***********************************************************************************************************/
    /**
     * ======================================================
     * FOR BC2 API INFO
     * ======================================================
     */

    /**
     * DEFINE API NAME
     * ======================================================
     */
    // For OAuth
    String WS_B2C_REQUEST_TOKEN = "OAuth/AuthoriseRequestToken";
    @Headers("Content-Type: application/json")
    @GET(WS_B2C_REQUEST_TOKEN)
    Call<B2CRequestTokenResponse> requestToken(
            @Query(WS_QUERY_CALLBACK_URL) String callBackUrl,
            @Query(WS_QUERY_MEMBER_DEVICE_ID) String memberDeviceId,
            @Query(WS_QUERY_ONLINE_MEMBER_ID) String onLineMemberId);

    String WS_B2C_ACCESS_TOKEN = "OAuth/GetAccessToken";
    @Headers("Content-Type: application/json")
    @GET(WS_B2C_ACCESS_TOKEN)
    Call<B2CAccessTokenResponse> accessToken(
            @Query(WS_QUERY_CONSUMER_KEY) String consumerKey,
            @Query(WS_QUERY_CALLBACK_URL) String callBackUrl,
            @Query(WS_QUERY_REQUEST_TOKEN) String requestToken,
            @Query(WS_QUERY_ONLINE_MEMBER_ID) String onLineMemberId);

    String WS_B2C_REFRESH_TOKEN = "OAuth/RefereshToken";
    @Headers("Content-Type: application/json")
    @GET(WS_B2C_REFRESH_TOKEN)
    Call<B2CRefreshTokenResponse> refreshToken(
            @Query(WS_QUERY_CONSUMER_KEY) String consumerKey,
            @Query(WS_QUERY_ACCESS_TOKEN) String accessToken,
            @Query(WS_QUERY_REFRESH_TOKEN) String refreshToken);

    // For Manage Users
    String WS_B2C_REGISTRATION = "ManageUsers/Registration";
    @Headers("Content-Type: application/json")
    @POST(WS_B2C_REGISTRATION)
    Call<B2CRegistrationResponse> registration(
            @Body B2CRegistrationRequest request);

    String WS_B2C_UPDATE_REGISTRATION = "ManageUsers/UpdateRegistration";
    @Headers("Content-Type: application/json")
    @POST(WS_B2C_UPDATE_REGISTRATION)
    Call<B2CUpdateRegistrationResponse> updateRegistration(
            @Body B2CUpdateRegistrationRequest request);
    String WS_B2C_GET_USER_DETAILS = "ManageUsers/GetUserDetails";
    @Headers("Content-Type: application/json")
    @POST(WS_B2C_GET_USER_DETAILS)
    Call<B2CGetUserDetailsResponse> getUserDetails(
            @Body B2CGetUserDetailsRequest request);

    String WS_B2C_CHANGE_SECRET_WORD = "ManageUsers/ChangePassword";
    @Headers("Content-Type: application/json")
    @POST(WS_B2C_CHANGE_SECRET_WORD)
    Call<B2CChangePasswordResponse> changePassword(
            @Body B2CChangePasswordRequest request);

    String WS_B2C_LOGIN = "ManageUsers/Login";
    @Headers("Content-Type: application/json")
    @POST(WS_B2C_LOGIN)
    Call<B2CLoginResponse> login(
            @Body B2CLoginRequest request);

    String WS_B2C_FORGOT_SECRET_WORD = "ManageUsers/ForgotPassword";
    @Headers("Content-Type: application/json")
    @POST(WS_B2C_FORGOT_SECRET_WORD)
    Call<B2CForgotPasswordResponse> forgotPassword(
            @Body B2CForgotPasswordRequest request);

    String WS_B2C_VERIFICATION = "ManageUsers/Verification";
    String WS_B2C_USER_AUTHENTICATION = "ManageUsers/UserAuthentication";

    String WS_B2C_ADD_PREFERENCE = "ManageUsers/AddPreference";
    @Headers("Content-Type: application/json")
    @POST(WS_B2C_ADD_PREFERENCE)
    Call<B2CUpsertPreferenceResponse> addPreferences(
            @Body B2CUpsertPreferenceRequest request);

    String WS_B2C_GET_PREFERENCE = "ManageUsers/GetPreference";
    @Headers("Content-Type: application/json")
    @POST(WS_B2C_GET_PREFERENCE)
    Call<List<B2CGetPreferenceResponse>> getPreferences(
            @Body B2CGetPreferenceRequest request);

    String WS_B2C_UPDATE_PREFERENCE = "ManageUsers/UpdatePreference";
    @Headers("Content-Type: application/json")
    @POST(WS_B2C_UPDATE_PREFERENCE)
    Call<B2CUpsertPreferenceResponse> updatePreferences(
            @Body B2CUpsertPreferenceRequest request);

    String WS_B2C_GET_USER_DETAILS_BY_BIN = "ManageUsers/GetUserDetailsByBIN";
    String WS_B2C_GET_EPC_MEMBER_ID = "ManageUsers/GetePCMemberID";
    String WS_B2C_UPDATE_MEMBER_USER_PROFILE = "ManageUsers/UpdateMemberUserProfile";

    // For Manage Request
    String WS_B2C_UPLOAD_MEDIA_FILES = "ManageRequest/UploadMediaFiles";
    @Multipart
    @POST(WS_B2C_UPLOAD_MEDIA_FILES)
    Call<B2CUploadFileResponse> uploadMediaFile(
            @Query(WS_QUERY_CONSUMER_KEY) String consumerKey,
            @Query(WS_QUERY_MEMBER_ID) String memberId,
            @Part MultipartBody.Part UploadPhoto);

    String WS_B2C_CREATE_CONCIERGE_REQUEST = "ManageRequest/CreateConciergeRequest";
    @Headers("Content-Type: application/json")
    @POST(WS_B2C_CREATE_CONCIERGE_REQUEST)
    Call<B2CUpsertConciergeRequestResponse> createConciergeRequest(
            @Body B2CUpsertConciergeRequestRequest request);

    String WS_B2C_UPDATE_CONCIERGE_REQUEST = "ManageRequest/UpdateConciergeRequest";
    @Headers("Content-Type: application/json")
    @POST(WS_B2C_UPDATE_CONCIERGE_REQUEST)
    Call<B2CUpsertConciergeRequestResponse> updateConciergeRequest(
            @Body B2CUpsertConciergeRequestRequest request);

    String WS_B2C_GET_RECENT_REQUEST = "ManageRequest/GetRecentRequestList";
    @Headers("Content-Type: application/json")
    @POST(WS_B2C_GET_RECENT_REQUEST)
    Call<List<B2CGetRecentRequestResponse>> getRecentRequests(
            @Body B2CGetRecentRequestRequest request);
    String WS_B2C_GET_REQUEST_DETAIL = "ManageRequest/GetConciergeRequestDetails";
    @Headers("Content-Type: application/json")
    @POST(WS_B2C_GET_REQUEST_DETAIL)
    Call<B2CGetRecentRequestResponse> getRequestDetail(
            @Body B2CGetConciergeRequestDetailRequest request);
    /**
     * DEFINE QUERY_KEY
     * ======================================================
     */
    String WS_QUERY_CALLBACK_URL = "CallBackURL";
    String WS_QUERY_MEMBER_DEVICE_ID = "MemberDeviceId";
    String WS_QUERY_ONLINE_MEMBER_ID = "OnlineMemberId";
    String WS_QUERY_MEMBER_ID = "memberId";
    String WS_QUERY_REQUEST_TOKEN = "RequestToken";
    String WS_QUERY_ACCESS_TOKEN = "AccessToken";
    String WS_QUERY_REFRESH_TOKEN = "RefreshToken";
    String WS_QUERY_CONSUMER_KEY = "ConsumerKey";
    String WS_QUERY_CONSUMER_SECRET = "ConsumerSecret";
/***********************************************************************************************************/
/***********************************************************************************************************/
/***********************************************************************************************************/
}
