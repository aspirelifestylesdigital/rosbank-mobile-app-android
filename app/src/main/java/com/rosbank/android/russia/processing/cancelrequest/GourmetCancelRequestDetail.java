package com.rosbank.android.russia.processing.cancelrequest;

import android.text.TextUtils;
import android.view.View;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.apiservices.ResponseModel.RestaurantBookingDetailResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.rosbank.android.russia.apiservices.b2c.B2CWSGetRequestDetail;

import com.rosbank.android.russia.apiservices.b2c.RequestModel.B2CUpsertConciergeRequestRequest;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CGetRecentRequestResponse;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.application.AppContext;
import com.rosbank.android.russia.model.CountryObject;
import com.rosbank.android.russia.model.concierge.RestaurantBookingDetailData;
import com.rosbank.android.russia.utils.DateTimeUtil;
import com.rosbank.android.russia.utils.SharedPreferencesUtils;

import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by ThuNguyen on 10/26/2016.
 */

public class GourmetCancelRequestDetail extends BaseCancelRequestDetail {
    private RestaurantBookingDetailData restaurantBookingDetailData;

    public GourmetCancelRequestDetail() {
    }

    public GourmetCancelRequestDetail(View view) {
        super(view);
    }

    @Override
    protected void updateUI() {
        super.updateUI();
        long epochCreatedDate = restaurantBookingDetailData.getCreateDateEpoc();
        tvRequestedDate.setText(DateTimeUtil.shareInstance().formatTimeStamp(epochCreatedDate, "dd MMMM yyyy"));
        tvRequestedTime.setText(DateTimeUtil.shareInstance().formatTimeStamp(epochCreatedDate, "hh:mm aa"));
        // Request type
        tvRequestType.setText(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_title_dinning_request));
        // Request name
        String requestName = restaurantBookingDetailData.getNameOfRestaurant();
        if (TextUtils.isEmpty(requestName) || requestName.equalsIgnoreCase("null")) {
            requestName = myRequestObject.getItemTitle();
        }
        if (TextUtils.isEmpty(requestName) || requestName.equalsIgnoreCase("null")) {
            tvRequestName.setText(restaurantBookingDetailData.getCity());
        } else {
            tvRequestName.setText(requestName + ", " + restaurantBookingDetailData.getCity());
        }

        // Request detail header
        tvRequestDetailHeader.setText(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_dining_request_detail));

        if (restaurantBookingDetailData != null) {
            // Case Id
            tvCaseId.setText(restaurantBookingDetailData.getBookingItemID());

            // Restaurant name
            String restaurantName = restaurantBookingDetailData.getNameOfRestaurant();
            if (TextUtils.isEmpty(restaurantName) || restaurantName.equalsIgnoreCase("null")) {
                //addRequestDetailItem("Name of Restaurant", "N.A");
            } else {
                addRequestDetailItem(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_name_of_restaurant), restaurantName);
            }
            // Reservation name
            String reservationName = restaurantBookingDetailData.getReservationName();
            if (TextUtils.isEmpty(reservationName) || reservationName.equalsIgnoreCase("null")) {
                //addRequestDetailItem("Reservation Name", "N.A");
            } else {
                addRequestDetailItem(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_reservation_name), reservationName);
            }
            // Reservation date
            long reservationDate = restaurantBookingDetailData.getReservationDateEpoc();
            addRequestDetailItem(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_reservation_date), DateTimeUtil.shareInstance().formatTimeStamp(reservationDate, "dd MMMM yyyy"));
        }
    }

    @Override
    public void getRequestDetail() {
        showProgressDialog();
        B2CWSGetRequestDetail b2CWSGetRequestDetail = new B2CWSGetRequestDetail(this);
        b2CWSGetRequestDetail.setValue(myRequestObject.getEpcCaseId(), myRequestObject.getBookingItemID());
        b2CWSGetRequestDetail.run(null);
        /*WSGetRestaurantBookingDetail wsGetRestaurantBookingDetail = new WSGetRestaurantBookingDetail();
        wsGetRestaurantBookingDetail.setBookingId(myRequestObject.getBookingId());
        wsGetRestaurantBookingDetail.run(this);
        showProgressDialog();*/
    }

    @Override
    public void onResponse(Call call, Response response) {
        hideProgressDialog();
        if (response != null && response.body() instanceof RestaurantBookingDetailResponse) {
            restaurantBookingDetailData = ((RestaurantBookingDetailResponse) response.body()).getData();
            updateUI();
        }
    }

    @Override
    public void onFailure(Call call, Throwable t) {
        hideProgressDialog();
    }

    @Override
    public void onB2CResponse(final B2CBaseResponse response) {
        hideProgressDialog();
        if (response instanceof B2CGetRecentRequestResponse) {
            restaurantBookingDetailData = new RestaurantBookingDetailData((B2CGetRecentRequestResponse) response);
            updateUI();
        }
    }

    @Override
    public void onB2CResponseOnList(final List<B2CBaseResponse> responseList) {

    }

    @Override
    public void onB2CFailure(final String errorMessage,
                             final String errorCode) {
        hideProgressDialog();
    }

    @Override
    public B2CUpsertConciergeRequestRequest getB2CUpsertConciergeRequestRequest() {
        B2CUpsertConciergeRequestRequest upsertConciergeRequestRequest = new B2CUpsertConciergeRequestRequest();
        upsertConciergeRequestRequest.setFunctionality(AppConstant.CONCIERGE_FUNCTIONALITY_TYPE.DINING.getValue());
        upsertConciergeRequestRequest.setRequestType(AppConstant.CONCIERGE_REQUEST_TYPE.RESERVE_TABLE.getValue());
        // Event date
        upsertConciergeRequestRequest.setEventDate(DateTimeUtil.shareInstance().formatTimeStamp(restaurantBookingDetailData.getReservationDateEpoc(), AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS));
        upsertConciergeRequestRequest.setDelivery(DateTimeUtil.shareInstance().formatTimeStamp(restaurantBookingDetailData.getReservationDateEpoc(), AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS));
        if (!TextUtils.isEmpty(restaurantBookingDetailData.getOccasion())) {
            upsertConciergeRequestRequest.setSituation(restaurantBookingDetailData.getOccasion());
        } else {
            upsertConciergeRequestRequest.setSituation("Situation");
        }
        if (!TextUtils.isEmpty(restaurantBookingDetailData.getCity())) {
            upsertConciergeRequestRequest.setCity(restaurantBookingDetailData.getCity());
        }
        if (!TextUtils.isEmpty(restaurantBookingDetailData.getCountry())) {
            if (restaurantBookingDetailData.getCountry() == null || restaurantBookingDetailData.getCountry().equals("Not Available") || CountryObject.getCountryObjectFromName(restaurantBookingDetailData.getCountry()) == null) {
                upsertConciergeRequestRequest.setCountry(Locale.getDefault().getISO3Language().toUpperCase());
            } else {
                upsertConciergeRequestRequest.setCountry(CountryObject.getCountryObjectFromName(restaurantBookingDetailData.getCountry()).getCountryCode());
            }
        }
        upsertConciergeRequestRequest.setNumberOfAdults(String.valueOf(restaurantBookingDetailData.getNumberOfAdults()));
        upsertConciergeRequestRequest.setNumberOfKids(String.valueOf(restaurantBookingDetailData.getNumberOfKids()));
        upsertConciergeRequestRequest.setNumberOfChildren(String.valueOf(restaurantBookingDetailData.getNumberOfChildren()));
        upsertConciergeRequestRequest.setEmail1((SharedPreferencesUtils.getPreferences(AppConstant.USER_EMAIL_PRE,
                "")));
        upsertConciergeRequestRequest.setEmailAddress1((SharedPreferencesUtils.getPreferences(AppConstant.USER_EMAIL_PRE,
                "")));
        upsertConciergeRequestRequest.setPhoneNumber(restaurantBookingDetailData.getMobileNumber());
        upsertConciergeRequestRequest.setPrefResponse(restaurantBookingDetailData.getPrefResponse());

        if (myRequestObject != null) {
            upsertConciergeRequestRequest.setTransactionID(myRequestObject.getBookingItemID());
        }
        // Request details
        upsertConciergeRequestRequest.setRequestDetails(restaurantBookingDetailData.getRequestDetaiString());
        return upsertConciergeRequestRequest;
    }

    private String combineRequestDetails() {
        String requestDetails = "";
        if (!TextUtils.isEmpty(restaurantBookingDetailData.getConciergeRequestDetail().getSpecialRequirement())) {
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_SPECIAL_REQ + restaurantBookingDetailData.getConciergeRequestDetail().getSpecialRequirement();
        }
        if (!TextUtils.isEmpty(restaurantBookingDetailData.getNameOfRestaurant())) {
            if (!TextUtils.isEmpty(requestDetails)) {
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_RESTAURANT_NAME + restaurantBookingDetailData.getNameOfRestaurant();
        }
        if (!TextUtils.isEmpty(restaurantBookingDetailData.getReservationName())) {
            if (!TextUtils.isEmpty(requestDetails)) {
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_RESERVATION_NAME + restaurantBookingDetailData.getReservationName();
        }
        if (!TextUtils.isEmpty(restaurantBookingDetailData.getCountry())) {
            if (!TextUtils.isEmpty(requestDetails)) {
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_COUNTRY + restaurantBookingDetailData.getCountry();
        }
        if (!TextUtils.isEmpty(restaurantBookingDetailData.getOccasion())) {
            if (!TextUtils.isEmpty(requestDetails)) {
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_PREFERED + restaurantBookingDetailData.getOccasion();
        }
        return requestDetails;
    }
}
