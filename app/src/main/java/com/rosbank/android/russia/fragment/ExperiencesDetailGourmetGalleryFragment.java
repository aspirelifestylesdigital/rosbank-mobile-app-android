package com.rosbank.android.russia.fragment;

import android.support.v4.view.ViewPager;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.rosbank.android.russia.BuildConfig;
import com.rosbank.android.russia.R;
import com.rosbank.android.russia.adapter.ExperiencesDetailGalleryAdapter;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.model.Galleries;
import com.rosbank.android.russia.model.Restaurant;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.Fontfaces;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;


/*
 * Created by chau.nguyen on 10/20/2016.
 */
public class ExperiencesDetailGourmetGalleryFragment extends BaseFragment {

    @BindView(R.id.pager)
    ViewPager viewPager;

    @BindView(R.id.tvTitleImg)
    TextView tvTitle;

    @BindView(R.id.tvNumberPage)
    TextView tvNumberPage;

    @BindView(R.id.btn_left)
    FrameLayout frameBtnLeft;

    @BindView(R.id.btn_right)
    FrameLayout frameBtnRight;

    private List<Galleries> listObj;
    private Restaurant restaurant;
    private int heightTabLayout = 0;

    public ExperiencesDetailGourmetGalleryFragment() {
        listObj = new ArrayList<>();
    }

    @Override
    protected int layoutId() {
        return R.layout.fragment_experiences_detail_gourmet_gallery;
    }

    @Override
    protected void initView() {
        tvTitle.setTypeface(Fontfaces.getFont(getActivity(), Fontfaces.FONTLIST.AVENIR_NEXT_LT_MEDIUM));
        tvNumberPage.setTypeface(Fontfaces.getFont(getActivity(), Fontfaces.FONTLIST.AVENIR_NEXT_LT_MEDIUM));

        if (restaurant != null) {

            if (restaurant.getGalleries().size() > 0) {
                for (Galleries obj : restaurant.getGalleries()) {
                    String link = BuildConfig.WS_ROOT_URL + obj.getBackgroundImageUrl();
                    obj.setBackgroundImageUrl(link);
                    listObj.add(obj);
                }
            } else {
                Galleries galleries = new Galleries();
                galleries.setCaptionTitle(restaurant.getRestaurantName());
                galleries.setBackgroundImageUrl(restaurant.getBackgroundImageUrl());
                listObj.add(galleries);
            }

            int height = CommonUtils.getScreenHeight(getActivity()) - heightTabLayout;
            viewPager.getLayoutParams().height = height;
            viewPager.requestLayout();

            ExperiencesDetailGalleryAdapter adapter = new ExperiencesDetailGalleryAdapter(getActivity(), listObj,
                    CommonUtils.getScreenWidth(getActivity()), height);

            viewPager.setAdapter(adapter);
            setTextGallery(0);
            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                @Override
                public void onPageSelected(int position) {
                    setTextGallery(position);
                }

                @Override
                public void onPageScrolled(int arg0, float arg1, int arg2) {

                }

                @Override
                public void onPageScrollStateChanged(int arg0) {

                }
            });

        }
    }

    private void setTextGallery(int position) {
        Galleries galleries = listObj.get(position);
        tvTitle.setText(galleries.getCaptionTitle());
        tvNumberPage.setText((position + 1) + "/" + listObj.size());
    }

    @OnClick(R.id.btn_right)
    public void eventBtnRight() {
        viewPager.setCurrentItem(getItem(+1), true);
    }

    @OnClick(R.id.btn_left)
    public void eventBtnLeft() {
        viewPager.setCurrentItem(getItem(-1), true);
    }

    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }

    @Override
    protected void bindData() {

    }

    @Override
    protected boolean onBack() {
        return false;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public void setHeightTabLayout(int heightTabLayout) {
        this.heightTabLayout = heightTabLayout;
    }
}