package com.rosbank.android.russia.model.preference;

import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CGetPreferenceResponse;
import com.rosbank.android.russia.application.AppConstant;

/**
 * Created by ThuNguyen on 12/13/2016.
 */

public class OtherPreferenceDetailData extends PreferenceData {
    private String comment;

    public OtherPreferenceDetailData(B2CGetPreferenceResponse response) {
        super(response);
        preferenceType = AppConstant.PREFERENCE_TYPE.OTHER;
        if (response != null) {
            comment = response.getVALUE();
        }
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
