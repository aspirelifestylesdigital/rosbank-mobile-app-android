package com.rosbank.android.russia.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.FontUtils;

import java.util.ArrayList;

/**
 * Created by anh.trinh on 9/20/2016.
 */
public class SelectionAdapter extends ArrayAdapter<String> implements Filterable {
    private final Context context;
    private  ArrayList<String> data;
    private  ArrayList<String> values;
    private final String selectedData;
    private CustomFilter mFilter;


    public SelectionAdapter(Context context, ArrayList<String> values,String selectedData) {
        super(context, -1, values);
        this.context = context;
        this.values = values;
        this.mFilter = new CustomFilter(this);
        this.selectedData =selectedData;
        this.data  = new ArrayList<>();
        this.data.addAll(values);
    }
    @Override
    public Filter getFilter() {
        return mFilter;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.item_selection, parent, false);
        TextView textView = (TextView) rowView.findViewById(R.id.tv_selection_name);
        ImageView imgTick = (ImageView) rowView.findViewById(R.id.ic_selection_tick);
        CommonUtils.setFontForViewRecursive(textView,
                FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
        textView.setText(values.get(position));
        if(selectedData.equals(values.get(position))){
            imgTick.setVisibility(View.VISIBLE);
        }else{
            imgTick.setVisibility(View.GONE);
        }
        return rowView;
    }

    @Override
    public int getCount() {
        return values.size();
    }
    private class CustomFilter extends Filter {
        private SelectionAdapter mAdapter;

        private CustomFilter(SelectionAdapter mAdapter) {
            super();
            this.mAdapter = mAdapter;
        }
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            values.clear();
            final FilterResults results = new FilterResults();
            if (constraint.length() == 0) {
                values.addAll(data);
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();
                for (final String obj : data) {
                    if (obj.toLowerCase().contains(filterPattern)) {
                        values.add(obj);
                    }
                }
            }
            results.values = values;
            results.count = values.size();
            return results;
        }
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mAdapter.notifyDataSetChanged();
        }
    }
}

