package com.rosbank.android.russia.apiservices;

import com.rosbank.android.russia.apiservices.RequestModel.FavouriteRequest;
import com.rosbank.android.russia.apiservices.ResponseModel.AddWishListResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.ApiProviderClient;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Callback;


public class WSAddToWishList
        extends ApiProviderClient<AddWishListResponse> {
    //
    private String userId;
    private String itemId;

    public WSAddToWishList(){

    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    @Override
    public void run(Callback<AddWishListResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }

    @Override
    protected boolean isUseAuthenticateInApi() {
        return true;
    }

    @Override
    public void runApi() {
        FavouriteRequest request = new FavouriteRequest();
        request.setUserID(userId);
        request.setItemId(itemId);
        serviceInterface.addWishList(request).enqueue(callback);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("User-Agent", "Retrofit-Sample-App");
        return headers;
    }

}
