package com.rosbank.android.russia.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

import java.util.List;

/**
 * Created by nga.nguyent on 9/22/2016.
 * modified chau.nguyen on 10/19/2016
 */
public class Restaurant implements Parcelable {
    public static final String VOTED = "voted";

    @Expose
    private String RestaurantName;
    @Expose
    private String RestaurantDescription;
    @Expose
    private String ShortDescription;
    @Expose
    private List<String> Cuisines;
    @Expose
    private String PriceRange;
    @Expose
    private String Rating;
    @Expose
    private String InternalLabel;
    @Expose
    private String InternalIcon;
    @Expose
    private String Address;
    @Expose
    private String Zipcode;
    @Expose
    private String LongLat;
    @Expose
    private String Location;
    @Expose
    private String Long;
    @Expose
    private String Lat;
    @Expose
    private String HoursOfOperation;
    @Expose
    private String ExecutiveChef;
    @Expose
    private List<String> TypeOfPayments;
    @Expose
    private String CityName;
    @Expose
    private String CountryName;
    @Expose
    private String CountryCode;
    @Expose
    private String Telephone;
    @Expose
    private String TermsCondition;
    @Expose
    private String DressCode;
    @Expose
    private String Email;
    @Expose
    private List<Menu> Menu;
    @Expose
    private List<Branches> Branches;
    @Expose
    private List<Galleries> Galleries;
    @Expose
    private List<String> Occations;
    @Expose
    private boolean IsInterested;
    @Expose
    private String BannerURL;
    @Expose
    private ID ID;
    @Expose
    private String IDStr;
    @Expose
    private String AddWishListStatusToCSS;
    @Expose
    private String BackgroundImageUrl;
    @Expose
    private String LogoSource;
    @Expose
    private Link Link;

    public String getBackgroundImageUrl() {
        return BackgroundImageUrl;
    }

    public void setBackgroundImageUrl(String backgroundImageUrl) {
        BackgroundImageUrl = backgroundImageUrl;
    }


    private Restaurant(Parcel in) {
        RestaurantName = in.readString();
        RestaurantDescription = in.readString();
        ShortDescription = in.readString();
        Cuisines = in.createStringArrayList();
        PriceRange = in.readString();
        Rating = in.readString();
        InternalLabel = in.readString();
        InternalIcon = in.readString();
        Address = in.readString();
        Zipcode = in.readString();
        LongLat = in.readString();
        Location = in.readString();
        Long = in.readString();
        Lat = in.readString();
        HoursOfOperation = in.readString();
        ExecutiveChef = in.readString();
        TypeOfPayments = in.createStringArrayList();
        CityName = in.readString();
        CountryName = in.readString();
        CountryCode = in.readString();
        Telephone = in.readString();
        TermsCondition = in.readString();
        DressCode = in.readString();
        Email = in.readString();
        Occations = in.createStringArrayList();
        IsInterested = in.readByte() != 0;
        BannerURL = in.readString();
        IDStr = in.readString();
        AddWishListStatusToCSS = in.readString();
        LogoSource = in.readString();
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(RestaurantName);
        dest.writeString(RestaurantDescription);
        dest.writeString(ShortDescription);
        dest.writeStringList(Cuisines);
        dest.writeString(PriceRange);
        dest.writeString(Rating);
        dest.writeString(InternalLabel);
        dest.writeString(InternalIcon);
        dest.writeString(Address);
        dest.writeString(Zipcode);
        dest.writeString(LongLat);
        dest.writeString(Location);
        dest.writeString(Long);
        dest.writeString(Lat);
        dest.writeString(HoursOfOperation);
        dest.writeString(ExecutiveChef);
        dest.writeStringList(TypeOfPayments);
        dest.writeString(CityName);
        dest.writeString(CountryName);
        dest.writeString(CountryCode);
        dest.writeString(Telephone);
        dest.writeString(TermsCondition);
        dest.writeString(DressCode);
        dest.writeString(Email);
        dest.writeStringList(Occations);
        dest.writeByte((byte) (IsInterested ? 1 : 0));
        dest.writeString(BannerURL);
        dest.writeString(IDStr);
        dest.writeString(AddWishListStatusToCSS);
        dest.writeString(LogoSource);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Restaurant> CREATOR = new Creator<Restaurant>() {
        @Override
        public Restaurant createFromParcel(Parcel in) {
            return new Restaurant(in);
        }

        @Override
        public Restaurant[] newArray(int size) {
            return new Restaurant[size];
        }
    };

    public com.rosbank.android.russia.model.Link getLink() {
        return Link;
    }

    public void setLink(com.rosbank.android.russia.model.Link link) {
        Link = link;
    }

    public String getRestaurantName() {
        return RestaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        RestaurantName = restaurantName;
    }

    public String getRestaurantDescription() {
        return RestaurantDescription;
    }

    public void setRestaurantDescription(String restaurantDescription) {
        RestaurantDescription = restaurantDescription;
    }

    public String getShortDescription() {
        return ShortDescription;
    }

    public void setShortDescription(String shortDescription) {
        ShortDescription = shortDescription;
    }

    public List<String> getCuisines() {
        return Cuisines;
    }

    public void setCuisines(List<String> cuisines) {
        Cuisines = cuisines;
    }

    public String getPriceRange() {
        return PriceRange;
    }

    public void setPriceRange(String priceRange) {
        PriceRange = priceRange;
    }

    public String getRating() {
        return Rating;
    }

    public void setRating(String rating) {
        Rating = rating;
    }

    public String getInternalLabel() {
        return InternalLabel;
    }

    public void setInternalLabel(String internalLabel) {
        InternalLabel = internalLabel;
    }

    public String getInternalIcon() {
        return InternalIcon;
    }

    public void setInternalIcon(String internalIcon) {
        InternalIcon = internalIcon;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getZipcode() {
        return Zipcode;
    }

    public void setZipcode(String zipcode) {
        Zipcode = zipcode;
    }

    public String getLongLat() {
        return LongLat;
    }

    public void setLongLat(String longLat) {
        LongLat = longLat;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public String getLong() {
        return Long;
    }

    public void setLong(String aLong) {
        Long = aLong;
    }

    public String getLat() {
        return Lat;
    }

    public void setLat(String lat) {
        Lat = lat;
    }

    public String getHoursOfOperation() {
        return HoursOfOperation;
    }

    public void setHoursOfOperation(String hoursOfOperation) {
        HoursOfOperation = hoursOfOperation;
    }

    public String getExecutiveChef() {
        return ExecutiveChef;
    }

    public void setExecutiveChef(String executiveChef) {
        ExecutiveChef = executiveChef;
    }

    public List<String> getTypeOfPayments() {
        return TypeOfPayments;
    }

    public void setTypeOfPayments(List<String> typeOfPayments) {
        TypeOfPayments = typeOfPayments;
    }

    public String getCityName() {
        return CityName;
    }

    public void setCityName(String cityName) {
        CityName = cityName;
    }

    public String getCountryName() {
        return CountryName;
    }

    public void setCountryName(String countryName) {
        CountryName = countryName;
    }

    public String getCountryCode() {
        return CountryCode;
    }

    public void setCountryCode(String countryCode) {
        CountryCode = countryCode;
    }

    public String getTelephone() {
        return Telephone;
    }

    public void setTelephone(String telephone) {
        Telephone = telephone;
    }

    public String getTermsCondition() {
        return TermsCondition;
    }

    public void setTermsCondition(String termsCondition) {
        TermsCondition = termsCondition;
    }

    public String getDressCode() {
        return DressCode;
    }

    public void setDressCode(String dressCode) {
        DressCode = dressCode;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public List<com.rosbank.android.russia.model.Menu> getMenu() {
        return Menu;
    }

    public void setMenu(List<com.rosbank.android.russia.model.Menu> menu) {
        Menu = menu;
    }

    public List<com.rosbank.android.russia.model.Branches> getBranches() {
        return Branches;
    }

    public void setBranches(List<com.rosbank.android.russia.model.Branches> branches) {
        Branches = branches;
    }

    public List<com.rosbank.android.russia.model.Galleries> getGalleries() {
        return Galleries;
    }

    public void setGalleries(List<com.rosbank.android.russia.model.Galleries> galleries) {
        Galleries = galleries;
    }

    public List<String> getOccations() {
        return Occations;
    }

    public void setOccations(List<String> occations) {
        Occations = occations;
    }

    public boolean isInterested() {
        return IsInterested;
    }

    public void setInterested(boolean interested) {
        IsInterested = interested;
    }

    public String getBannerURL() {
        return BannerURL;
    }

    public void setBannerURL(String bannerURL) {
        BannerURL = bannerURL;
    }

    public com.rosbank.android.russia.model.ID getID() {
        return ID;
    }

    public void setID(com.rosbank.android.russia.model.ID ID) {
        this.ID = ID;
    }

    public String getIDStr() {
        return IDStr;
    }

    public void setIDStr(String IDStr) {
        this.IDStr = IDStr;
    }

    public String getAddWishListStatusToCSS() {
        return AddWishListStatusToCSS;
    }

    public String getLogoSource() {
        return LogoSource;
    }

    public void setLogoSource(String logoSource) {
        LogoSource = logoSource;
    }

    public void setAddWishListStatusToCSS(String addWishListStatusToCSS) {
        AddWishListStatusToCSS = addWishListStatusToCSS;
        if (AddWishListStatusToCSS != null) {
            AddWishListStatusToCSS = AddWishListStatusToCSS.trim();
        }
    }

    public boolean isVoted() {
        if (AddWishListStatusToCSS == null)
            return false;
        AddWishListStatusToCSS = AddWishListStatusToCSS.trim();
        if (AddWishListStatusToCSS.equalsIgnoreCase(VOTED))
            return true;
        return false;
    }

    /**
     * show list array Cuisines
     * @return African, Amalfitano
     */
    public String getShowCuisines(){
        String content = "";
        if (Cuisines != null){
            for (String cuisine: Cuisines) {
                if(content.length() > 0){
                    content += ", ";
                }
                content += cuisine;
            }
        }
        return content;
    }
}
