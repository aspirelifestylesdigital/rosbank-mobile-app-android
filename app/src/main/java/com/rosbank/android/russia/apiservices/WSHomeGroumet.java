package com.rosbank.android.russia.apiservices;

import com.rosbank.android.russia.apiservices.RequestModel.HomeGourmetRequest;
import com.rosbank.android.russia.apiservices.ResponseModel.HomeGourmetResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.ApiProviderClient;
import com.rosbank.android.russia.model.UserItem;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Callback;


public class WSHomeGroumet
        extends ApiProviderClient<HomeGourmetResponse> {
    //
    private int page = 1;
    private int perPage = 3;

    public WSHomeGroumet(){

    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPerPage() {
        return perPage;
    }

    public void setPerPage(int perPage) {
        this.perPage = perPage;
    }

    public void setTotalItems(int number){
        page = number/perPage;
        page += 1;
        if(number%perPage!=0){
            page += 1;
        }
    }

    @Override
    public void run(Callback<HomeGourmetResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }

    @Override
    protected boolean isUseAuthenticateInApi() {
        return true;
    }

    @Override
    public void runApi() {
        // no need to check logged-in user
        /*if(!UserItem.isLogined())
            return;*/

        String id = UserItem.getLoginedId();
        HomeGourmetRequest request = new HomeGourmetRequest();
        request.setPage(page);
        request.setRecordPerPage(perPage);
        request.setUserID(id);

        serviceInterface.getHomeGourmet(request).enqueue(callback);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        return headers;
    }

}

