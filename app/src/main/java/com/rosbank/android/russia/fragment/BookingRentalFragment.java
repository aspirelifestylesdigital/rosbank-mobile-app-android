package com.rosbank.android.russia.fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.ToggleButton;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.activitys.HomeActivity;
import com.rosbank.android.russia.apiservices.ResponseModel.BookRentalResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.CarRentalBookingDetailResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CICallback;
import com.rosbank.android.russia.apiservices.b2c.B2CWSGetPreference;
import com.rosbank.android.russia.apiservices.b2c.B2CWSGetRequestDetail;
import com.rosbank.android.russia.apiservices.b2c.B2CWSUpsertConciergeRequest;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.B2CUpsertConciergeRequestRequest;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CGetRecentRequestResponse;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CUpsertConciergeRequestResponse;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.application.AppContext;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.model.RentalObject;
import com.rosbank.android.russia.model.concierge.CarBookingDetailData;
import com.rosbank.android.russia.model.concierge.MyRequestObject;
import com.rosbank.android.russia.model.preference.CarPreferenceDetailData;
import com.rosbank.android.russia.model.preference.PreferenceData;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.DateTimeUtil;
import com.rosbank.android.russia.utils.EntranceLock;
import com.rosbank.android.russia.utils.FontUtils;
import com.rosbank.android.russia.utils.NetworkUtil;
import com.rosbank.android.russia.utils.PhotoPickerUtils;
import com.rosbank.android.russia.utils.StringUtil;
import com.rosbank.android.russia.widgets.AttachePhotoView;
import com.rosbank.android.russia.widgets.ContactView;
import com.rosbank.android.russia.widgets.CustomErrorView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by anh.trinh on 9/13/2016.
 */
public class BookingRentalFragment
        extends BaseFragment
        implements ContactView.ContactViewListener,
                   SelectionRentalFragment.SelectionCallback,
                   SelectionFragment.SelectionCallback,
                   Callback, B2CICallback {

    @BindView(R.id.edt_driver_name)
    EditText mEdtDriverName;
    @BindView(R.id.driver_name_error)
    CustomErrorView mDriverNameError;
    @BindView(R.id.edt_age_of_driver)
    EditText mEdtAgeOfDriver;
    @BindView(R.id.age_of_driver_error)
    CustomErrorView mAgeOfDriverError;
    @BindView(R.id.layout_age_of_driver)
    RelativeLayout mLayoutAgeOfDriver;
    @BindView(R.id.tb_switch_international_license)
    ToggleButton mTbSwitchInternationalLicense;
    @BindView(R.id.tv_pick_up_date)
    TextView mTvPickUpDate;
    @BindView(R.id.tv_pick_up_time)
    TextView mTvPickUpTime;
    @BindView(R.id.ervPickupTime)
    CustomErrorView ervPickupTime;
    @BindView(R.id.edt_pick_up_location)
    EditText mEdtPickUpLocation;
    @BindView(R.id.pick_up_location_error)
    CustomErrorView mPickUpLocationError;
    @BindView(R.id.tv_drop_off_date)
    TextView mTvDropOffDate;
    @BindView(R.id.tv_drop_off_time)
    TextView mTvDropOffTime;
    @BindView(R.id.ervDropoffTime)
    CustomErrorView ervDropoffTime;
    @BindView(R.id.edt_drop_off_loction)
    EditText mEdtDropOffLoction;
    @BindView(R.id.drop_off_loction_error)
    CustomErrorView mDropOffLoctionError;
    @BindView(R.id.tv_please_select_preferred_verhicle)
    TextView mTvPleaseSelectPreferredVerhicle;
    @BindView(R.id.edt_car_rental_company)
    EditText edtCarRentalCompany;
    @BindView(R.id.edt_maximum_budget)
    EditText mEdtMaximumPrice;
    @BindView(R.id.tv_any_special_requirements_title)
    TextView mTvAnySpecialRequirementsTitle;
    @BindView(R.id.edt_any_special_requirements)
    EditText mEdtAnySpecialRequirements;
    @BindView(R.id.special_req_error)
    CustomErrorView specialReqError;
    @BindView(R.id.photo_attached)
    AttachePhotoView mPhotoAttached;
    @BindView(R.id.btn_submit)
    Button mBtnSubmit;
    @BindView(R.id.btn_cancel)
    Button mBtnCancel;
    @BindView(R.id.layout_bottom_button)
    LinearLayout mLayoutBottomButton;
    @BindView(R.id.container)
    LinearLayout mLayoutContainer;
    @BindView(R.id.scrollView)
    ScrollView mScrollView;
    @BindView(R.id.contact_view)
    ContactView mContactView;

    private Calendar pickCal;
    private Calendar dropCal;

    ArrayList<RentalObject> mVehicleSelected = new ArrayList<>();
    private MyRequestObject myRequestObject = null;
    private B2CUpsertConciergeRequestRequest upsertConciergeRequestRequest;
    private String uploadedPhotoPath = "";
    private EntranceLock entranceLock = new EntranceLock();
    private boolean isDateTimeSelectedByUser = false;
    private boolean isSubmitClicked = false;

    @Override
    protected int layoutId() {
        return R.layout.fragment_concierge_book_rental;
    }

    @Override
    protected void initView() {
        CommonUtils.setFontForViewRecursive(mLayoutContainer,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
        CommonUtils.setFontForViewRecursive(mBtnSubmit,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        CommonUtils.setFontForViewRecursive(mBtnCancel,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        CommonUtils.makeFirstCharacterUpperCase(mEdtDriverName, mEdtPickUpLocation, mEdtDropOffLoction, edtCarRentalCompany);
        specialReqError.setMessage(getString(R.string.special_requirement_error));

        mPhotoAttached.setAttachePhotoCallback(new AttachePhotoView.IAttachePhotoCallback() {
            @Override
            public void onPhotoChanged() {
                // Reset the photo path once the user change photo
                uploadedPhotoPath = "";
            }
        });
        makeDefaultDateTime();

        mPhotoAttached.setParentFragment(this);
        mContactView.setContactViewListener(this);
//        mEdtDriverName.setText(SharedPreferencesUtils.getPreferences(AppConstant.USER_FULL_NAME,
//                                                                     ""));
        if (getArguments() != null) {
            myRequestObject = (MyRequestObject)getArguments().getSerializable(AppConstant.PRE_REQUEST_OBJECT_DATA);
        }
        if (myRequestObject != null) {
            isDateTimeSelectedByUser = true;
            showDialogProgress();
            B2CWSGetRequestDetail b2CWSGetRequestDetail = new B2CWSGetRequestDetail(this);
            b2CWSGetRequestDetail.setValue(myRequestObject.getEpcCaseId(), myRequestObject.getBookingItemID());
            b2CWSGetRequestDetail.run(null);
        }else{ // Load default preferences
            showDialogProgress();
            B2CWSGetPreference b2CWSGetPreference = new B2CWSGetPreference(getPreferenceCallback);
            b2CWSGetPreference.run(null);
        }

        // Error time
        ervPickupTime.setMessage(getString(R.string.text_error_message_invalid_time));
        ervDropoffTime.setMessage(getString(R.string.text_error_message_drop_off_time));

    }
    public void makeDefaultDateTime(){
        if(!isDateTimeSelectedByUser) {
            pickCal = Calendar.getInstance();
            pickCal.add(Calendar.DAY_OF_YEAR, 1);
            pickCal.add(Calendar.MINUTE, AppConstant.MINUTE_EXTRA);
            dropCal = Calendar.getInstance();
            dropCal.add(Calendar.DAY_OF_YEAR, 1);
            dropCal.add(Calendar.MINUTE, AppConstant.MINUTE_EXTRA);
            showDateTime();
        }
    }
    private void showDateTime(){
        mTvPickUpDate.setText(DateTimeUtil.shareInstance().formatTimeStamp(pickCal.getTimeInMillis(), AppConstant.DATE_FORMAT_CONCIERGE_BOOKING));
        mTvPickUpTime.setText(DateTimeUtil.shareInstance().formatTimeStamp(pickCal.getTimeInMillis(), AppConstant.TIME_FORMAT_CONCIERGE_BOOKING).toUpperCase());
        mTvDropOffDate.setText(DateTimeUtil.shareInstance().formatTimeStamp(dropCal.getTimeInMillis(), AppConstant.DATE_FORMAT_CONCIERGE_BOOKING));
        mTvDropOffTime.setText(DateTimeUtil.shareInstance().formatTimeStamp(dropCal.getTimeInMillis(), AppConstant.TIME_FORMAT_CONCIERGE_BOOKING).toUpperCase());
    }
    @Override
    protected void bindData() {
    }

    @Override
    public boolean onBack() {
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (myRequestObject != null) {
            ((HomeActivity) getActivity()).showLogoApp(false);
            ((HomeActivity) getActivity()).setTitle(getString(R.string.text_title_car_rental));
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater,
                                           container,
                                           savedInstanceState);
        ButterKnife.bind(this,
                         rootView);
        return rootView;
    }

    @OnClick({R.id.tv_please_select_preferred_verhicle,
              R.id.btn_submit,
              R.id.btn_cancel,
              R.id.tv_pick_up_date,R.id.tv_drop_off_date,
              R.id.tv_pick_up_time,
              R.id.tv_drop_off_time})
    public void onClick(View view) {
        CommonUtils.hideSoftKeyboard(getContext());
        switch (view.getId()) {
            case R.id.tv_pick_up_date:
                ervPickupTime.hide();
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                     new DatePickerDialog.OnDateSetListener() {

                         @Override
                         public void onDateSet(DatePicker view,
                                               int year,
                                               int monthOfYear,
                                               int dayOfMonth) {
                             if(CommonUtils.validateFutureDate(dayOfMonth, monthOfYear, year, view)) {
                                 pickCal.set(year, monthOfYear, dayOfMonth);
                                 if (DateTimeUtil.shareInstance().checkLaterThan24Hours(pickCal)) {
                                     isDateTimeSelectedByUser = true;
                                     ervPickupTime.setVisibility(View.GONE);
                                     if (dropCal.getTimeInMillis() < pickCal.getTimeInMillis()) {
                                         dropCal.set(year, monthOfYear, dayOfMonth, pickCal.get(Calendar.HOUR_OF_DAY), pickCal.get(Calendar.MINUTE));
                                     }
                                     ervDropoffTime.setVisibility(View.GONE);
                                 } else {
                                     ervPickupTime.setVisibility(View.VISIBLE);
                                 }
                                 showDateTime();
                             }
                         }

                     },
                     pickCal.get(Calendar.YEAR),
                     pickCal.get(Calendar.MONTH),
                     pickCal.get(Calendar.DAY_OF_MONTH));

                Calendar c = Calendar.getInstance();
                c.add(Calendar.DAY_OF_YEAR, 1);
                c.set(Calendar.HOUR_OF_DAY, 0);
                c.set(Calendar.MINUTE, 0);
                Calendar pickOnlyDate = Calendar.getInstance();
                pickOnlyDate.set(pickCal.get(Calendar.YEAR), pickCal.get(Calendar.MONTH), pickCal.get(Calendar.DAY_OF_MONTH), 0, 0);
                datePickerDialog.getDatePicker().setMinDate(Math.min(c.getTimeInMillis(), pickOnlyDate.getTimeInMillis()));
                datePickerDialog.show();
                break;

            case R.id.tv_drop_off_date:
                ervDropoffTime.hide();
                DatePickerDialog datePickerDialogOut = new DatePickerDialog(getContext(),
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view,
                                              int year,
                                              int monthOfYear,
                                              int dayOfMonth) {
                            if(CommonUtils.validateFutureDate(dayOfMonth, monthOfYear, year, view)) {
                                dropCal.set(year, monthOfYear, dayOfMonth);
                                showDateTime();
                                if (DateTimeUtil.shareInstance().checkLaterThan24Hours(dropCal)) {
                                    ervDropoffTime.setVisibility(View.GONE);
                                    isDateTimeSelectedByUser = true;
                                } else {
                                    ervDropoffTime.setVisibility(View.VISIBLE);
                                }
                            }
                        }
                    },
                    dropCal.get(Calendar.YEAR),
                    dropCal.get(Calendar.MONTH),
                    dropCal.get(Calendar.DAY_OF_MONTH));
                pickOnlyDate = Calendar.getInstance();
                pickOnlyDate.set(pickCal.get(Calendar.YEAR), pickCal.get(Calendar.MONTH), pickCal.get(Calendar.DAY_OF_MONTH), 0, 0);
                Calendar dropOnlyDate = Calendar.getInstance();
                dropOnlyDate.set(dropCal.get(Calendar.YEAR), dropCal.get(Calendar.MONTH), dropCal.get(Calendar.DAY_OF_MONTH), 0, 0);
                datePickerDialogOut.getDatePicker().setMinDate(Math.min(pickOnlyDate.getTimeInMillis(), dropOnlyDate.getTimeInMillis()));
                datePickerDialogOut.show();
                break;
            case R.id.tv_pick_up_time:
                ervPickupTime.hide();
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(getContext(),
                   new TimePickerDialog.OnTimeSetListener() {
                       @Override
                       public void onTimeSet(TimePicker timePicker,
                                             int selectedHour,
                                             int selectedMinute) {
                           pickCal.set(Calendar.HOUR_OF_DAY, selectedHour);
                           pickCal.set(Calendar.MINUTE, selectedMinute);
                           if(DateTimeUtil.shareInstance().checkLaterThan24Hours(pickCal)) {
                               isDateTimeSelectedByUser = true;
                               ervPickupTime.setVisibility(View.GONE);
                               if(dropCal.getTimeInMillis() < pickCal.getTimeInMillis()){
                                   dropCal.set(pickCal.get(Calendar.YEAR), pickCal.get(Calendar.MONTH), pickCal.get(Calendar.DAY_OF_MONTH),
                                           pickCal.get(Calendar.HOUR_OF_DAY), pickCal.get(Calendar.MINUTE));
                               }
                               ervDropoffTime.setVisibility(View.GONE);
                           }else{
                               ervPickupTime.setVisibility(View.VISIBLE);
                           }
                           showDateTime();
                       }
                   },
                   pickCal.get(Calendar.HOUR_OF_DAY),
                   pickCal.get(Calendar.MINUTE),
                        false);
                mTimePicker.setTitle(getString(R.string.text_concierge_booking_rental_pick_up_time).replace("*", ""));
                mTimePicker.show();
                break;
            case R.id.tv_drop_off_time:
                ervDropoffTime.hide();
                TimePickerDialog mTimePickerDrop;
                mTimePickerDrop = new TimePickerDialog(getContext(),
                   new TimePickerDialog.OnTimeSetListener() {
                       @Override
                       public void onTimeSet(TimePicker timePicker,
                                             int selectedHour,
                                             int selectedMinute) {
                           dropCal.set(Calendar.HOUR_OF_DAY, selectedHour);
                           dropCal.set(Calendar.MINUTE, selectedMinute);
                           if(DateTimeUtil.shareInstance().checkLaterThan24Hours(dropCal)) {
                               isDateTimeSelectedByUser = true;
                               ervDropoffTime.setVisibility(View.GONE);
                               if(dropCal.getTimeInMillis() < pickCal.getTimeInMillis()){
                                   pickCal.set(dropCal.get(Calendar.YEAR), dropCal.get(Calendar.MONTH), dropCal.get(Calendar.DAY_OF_MONTH),
                                           dropCal.get(Calendar.HOUR_OF_DAY), dropCal.get(Calendar.MINUTE));
                               }
                           }else{
                               ervDropoffTime.setVisibility(View.VISIBLE);
                           }
                           showDateTime();
                       }
                   },
                   dropCal.get(Calendar.HOUR_OF_DAY),
                   dropCal.get(Calendar.MINUTE),
                   false);
                mTimePickerDrop.setTitle(getString(R.string.text_concierge_booking_rental_drop_off_time).replace("*",""));
                mTimePickerDrop.show();
                break;
            case R.id.tv_please_select_preferred_verhicle:
                Bundle bundle = new Bundle();
                bundle.putString(SelectionRentalFragment.PRE_SELECTION_TYPE,
                                 SelectionRentalFragment.PRE_SELECTION_VERHICLE_TYPE);
                bundle.putString(SelectionRentalFragment.PRE_SELECTION_TITLE, getString(R.string.text_concierge_booking_rental_preferred_verhicle));
                if (mVehicleSelected != null) {
                    bundle.putParcelableArrayList(SelectionRentalFragment.PRE_SELECTION_DATA,
                                                  mVehicleSelected);
                }
                SelectionRentalFragment fragment = new SelectionRentalFragment();
                fragment.setSelectionCallBack(this);
                fragment.setArguments(bundle);
                pushFragment(fragment,
                             true,
                             true);
                break;
//            case R.id.tv_please_select_car_rental_company:
//                Bundle bundleCompany = new Bundle();
//                bundleCompany.putString(SelectionRentalFragment.PRE_SELECTION_TYPE,
//                                        SelectionRentalFragment.PRE_SELECTION_RENTAL_CONUNTRY);
//                if (mCompanySelected != null) {
//                    bundleCompany.putParcelableArrayList(SelectionRentalFragment.PRE_SELECTION_DATA,
//                                                         mCompanySelected);
//                }
//                SelectionRentalFragment fragmentCompany = new SelectionRentalFragment();
//                fragmentCompany.setSelectionCallBack(this);
//                fragmentCompany.setArguments(bundleCompany);
//                pushFragment(fragmentCompany,
//                             true,
//                             true);
//                break;
            case R.id.btn_submit:
                AppContext.getSharedInstance().track(AppConstant.ANALYTIC_EVENT_CATEGORY_CONCIERGE_REQUEST, AppConstant.ANALYTIC_EVENT_ACTION_SEND_CONCIERGE_REQUEST, AppConstant.ANALYTIC_BOOK_ITEM.TRANSPORTATION.getValue());
                if (validationData() && !entranceLock.isClickContinuous()) {
                    if(NetworkUtil.getNetworkStatus(getContext())) {
                        isSubmitClicked = true;
                    processBookingRental();
                    }else{
                        showInternetProblemDialog();
                    }
                }
                break;
            case R.id.btn_cancel:
                if(!entranceLock.isClickContinuous()) {
                    onBackPress();
                }
                break;
        }
    }

    @OnTouch({R.id.edt_driver_name,
              R.id.edt_age_of_driver,
              R.id.edt_pick_up_location,
              R.id.edt_drop_off_loction,
                R.id.edt_any_special_requirements})
    boolean onInputTouch(final View v,
                         final MotionEvent event) {

        switch (v.getId()) {
            case R.id.edt_driver_name:
                mDriverNameError.setVisibility(View.GONE);
                break;
            case R.id.edt_age_of_driver:
                mAgeOfDriverError.setVisibility(View.GONE);
                break;
            case R.id.edt_pick_up_location:
                mPickUpLocationError.setVisibility(View.GONE);
                break;
            case R.id.edt_drop_off_loction:
                mDropOffLoctionError.setVisibility(View.GONE);
                break;
            case R.id.edt_any_special_requirements:
                specialReqError.setVisibility(View.GONE);
                break;
        }
        return false;
    }

    @Override
    public void onFinishSelection(final Bundle bundle) {

        String typeSelection = bundle.getString(SelectionFragment.PRE_SELECTION_TYPE,
                                                "");

        if (typeSelection.equals(SelectionFragment.PRE_SELECTION_COUNTRY_CODE)) {
            mContactView.setCountryNo(bundle.getString(SelectionFragment.PRE_SELECTION_DATA, ""));
        }
        if (typeSelection.equals(SelectionRentalFragment.PRE_SELECTION_VERHICLE_TYPE)) {
            mVehicleSelected =
                    bundle.getParcelableArrayList(SelectionRentalFragment.PRE_SELECTION_DATA);
            updateVehicle();
        }
//        if (typeSelection.equals(SelectionRentalFragment.PRE_SELECTION_RENTAL_CONUNTRY)) {
//            mCompanySelected =
//                    bundle.getParcelableArrayList(SelectionRentalFragment.PRE_SELECTION_DATA);
//            updateRentalCompany();
//        }

    }

    private void updateVehicle() {
        if (mVehicleSelected == null || mVehicleSelected.size() == 0) {
            mTvPleaseSelectPreferredVerhicle.setText(getString(R.string.text_please_select_vehicle));
            mTvPleaseSelectPreferredVerhicle.setTextColor(ContextCompat.getColor(getContext(), R.color.hint_color));
            return;
        }
        String vehicle = "";
        if (mVehicleSelected != null) {
            for (int i = 0; i < mVehicleSelected.size(); i++) {
                RentalObject rentalObject = mVehicleSelected.get(i);
                if (i == 0) {
                    vehicle += rentalObject.getValue();
                } else {
                    vehicle += ", " + rentalObject.getValue();

                }
            }
            mTvPleaseSelectPreferredVerhicle.setText(vehicle);
            mTvPleaseSelectPreferredVerhicle.setTextColor(ContextCompat.getColor(getContext(), R.color.text_color_active));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode,
                                         permissions,
                                         grantResults);

        switch (requestCode) {
            case AppConstant.PERMISSION_REQUEST_READ_EXTERNAL:
                if (grantResults.length > 0
                            && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mPhotoAttached.openGallery();
                }
                break;
            case AppConstant.PERMISSION_REQUEST_OPEN_CAMERA:
                if (grantResults.length > 0
                            && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mPhotoAttached.openCamera();
                }
                break;
            case AppConstant.PERMISSION_REQUEST_WRITE_EXTERNAL:
                if (grantResults.length > 0
                            && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mPhotoAttached.openCamera();
                }
                break;
        }

    }

    @Override
    public void onActivityResult(int requestCode,
                                 int resultCode,
                                 Intent data) {
        super.onActivityResult(requestCode,
                               resultCode,
                               data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == PhotoPickerUtils.PICK_IMAGE_REQUEST_CODE || requestCode == PhotoPickerUtils.TAKE_PICTURE_REQUEST_CODE) {
                mPhotoAttached.onActivityResult(requestCode,
                                                data);
            }
        }
    }

    private boolean validationData() {
        boolean result = true;
        if (!CommonUtils.isStringValid(mEdtDriverName.getText()
                                                     .toString()
                                                     .trim())) {
            mDriverNameError.fillData(getString(R.string.text_sign_up_error_required_field));
            mDriverNameError.setVisibility(View.VISIBLE);
            result = false;
        }else if(StringUtil.containSpecialCharacter(mEdtDriverName.getText().toString().trim())) {
            mDriverNameError.fillData(getString(R.string.special_requirement_error));
            mDriverNameError.setVisibility(View.VISIBLE);
            result = false;
        }else{
            mDriverNameError.setVisibility(View.GONE);
        }
        if (!CommonUtils.isStringValid(mEdtAgeOfDriver.getText()
                                                      .toString()
                                                      .trim())) {
            mAgeOfDriverError.fillData(getString(R.string.text_sign_up_error_required_field));
            mAgeOfDriverError.setVisibility(View.VISIBLE);
            result = false;
        }else{
            mAgeOfDriverError.setVisibility(View.GONE);
        }
        if(StringUtil.containSpecialCharacter(mEdtAnySpecialRequirements.getText().toString())){
            specialReqError.setVisibility(View.VISIBLE);
            result = false;
        }else{
            specialReqError.setVisibility(View.GONE);
        }
        if (!CommonUtils.isStringValid(mEdtPickUpLocation.getText()
                                                         .toString()
                                                         .trim())) {
            mPickUpLocationError.fillData(getString(R.string.text_sign_up_error_required_field));
            mPickUpLocationError.setVisibility(View.VISIBLE);
            result = false;
        }else{
            mPickUpLocationError.setVisibility(View.GONE);
        }
        if (!CommonUtils.isStringValid(mEdtDropOffLoction.getText()
                                                         .toString()
                                                         .trim())) {
            mDropOffLoctionError.fillData(getString(R.string.text_sign_up_error_required_field));
            mDropOffLoctionError.setVisibility(View.VISIBLE);
            result = false;
        }else{
            mDropOffLoctionError.setVisibility(View.GONE);
        }
        if(!DateTimeUtil.shareInstance().checkLaterThan24Hours(pickCal)){
            result = false;
            ervPickupTime.setVisibility(View.VISIBLE);
        }else{
            ervPickupTime.setVisibility(View.GONE);
        }
        if(!DateTimeUtil.shareInstance().checkLaterThan24Hours(dropCal)){
            result = false;
            ervDropoffTime.setVisibility(View.VISIBLE);
        }else{
            ervDropoffTime.setVisibility(View.GONE);
        }
        if (!mContactView.validationContact()) {
            result = false;
        }

        return result;
    }

    private void processBookingRental() {
        upsertConciergeRequestRequest = new B2CUpsertConciergeRequestRequest();
        upsertConciergeRequestRequest.setFunctionality(AppConstant.CONCIERGE_FUNCTIONALITY_TYPE.CAR_RENTAL.getValue());
        upsertConciergeRequestRequest.setRequestType(AppConstant.CONCIERGE_REQUEST_TYPE.T_CAR_RENTAL.getValue());
        upsertConciergeRequestRequest.updateDataFromContactView(mContactView);
        // Pickup date
        upsertConciergeRequestRequest.setPickupDate(DateTimeUtil.shareInstance().formatTimeStamp(pickCal.getTimeInMillis(), AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS));
        upsertConciergeRequestRequest.setPickUp(DateTimeUtil.shareInstance().formatTimeStamp(pickCal.getTimeInMillis(), AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS));
        upsertConciergeRequestRequest.setSituation("Car Rental");
        // Image path
        if(!TextUtils.isEmpty(uploadedPhotoPath)){
            upsertConciergeRequestRequest.setAttachmentPath(uploadedPhotoPath);
        }else {
            if (mPhotoAttached.getPhotos() != null && mPhotoAttached.getPhotos().size() > 0) {
                String pathImage = mPhotoAttached.getPhotos()
                        .get(0);
                upsertConciergeRequestRequest.setAttachmentPath(pathImage);
            }
        }
        if(myRequestObject != null){
            upsertConciergeRequestRequest.setTransactionID(myRequestObject.getBookingItemID());
        }
        // Request details
        upsertConciergeRequestRequest.setRequestDetails(combineRequestDetails());


        showDialogProgress();
        B2CWSUpsertConciergeRequest b2CWSUpsertConciergeRequest = new B2CWSUpsertConciergeRequest(myRequestObject == null ? AppConstant.CONCIERGE_EDIT_TYPE.ADD : AppConstant.CONCIERGE_EDIT_TYPE.AMEND, this);
        b2CWSUpsertConciergeRequest.setRequest(upsertConciergeRequestRequest);
        b2CWSUpsertConciergeRequest.run(null);

    }
    private String combineRequestDetails(){

        String requestDetails = "";
        if(!TextUtils.isEmpty(mEdtAnySpecialRequirements.getText().toString().trim())){
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_SPECIAL_REQUIREMENTS + StringUtil.removeAllSpecialCharacterAndBreakLine(mEdtAnySpecialRequirements.getText().toString().trim());
        }
        if(!TextUtils.isEmpty(mContactView.getReservationName())){
            if(!TextUtils.isEmpty(requestDetails)){
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_RESERVATION_NAME+ StringUtil.removeAllSpecialCharacterAndBreakLine(mContactView.getReservationName());
        }
        // International license
        if(!StringUtil.isEmpty(requestDetails)){
            requestDetails += " | ";
        }
        requestDetails += AppConstant.B2C_REQUEST_DETAIL_INT_LICENSE + (mTbSwitchInternationalLicense.isChecked() ? "Yes" : "No");

        // Name of driver
        if(!StringUtil.isEmpty(requestDetails)){
            requestDetails += " | ";
        }
        requestDetails += AppConstant.B2C_REQUEST_DETAIL_DRIVER_NAME + StringUtil.removeAllSpecialCharacterAndBreakLine(mEdtDriverName.getText().toString().trim());
        // Age of driver
        if(!StringUtil.isEmpty(requestDetails)){
            requestDetails += " | ";
        }
        requestDetails += AppConstant.B2C_REQUEST_DETAIL_DRIVER_AGE + mEdtAgeOfDriver.getText().toString().trim();

        // Preferred vehicle
        if (mVehicleSelected != null && mVehicleSelected.size() > 0) {
            if(!StringUtil.isEmpty(requestDetails)){
                requestDetails += " | ";
            }

            String vehicles = mVehicleSelected.get(0).getValue();
            for (int i = 1; i < mVehicleSelected.size(); i++) {
                vehicles += "," + mVehicleSelected.get(i).getValue();
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_PREF_VEHICLE + vehicles;
        }
        // Pickup location
        if(!StringUtil.isEmpty(requestDetails)){
            requestDetails += " | ";
        }
        requestDetails += AppConstant.B2C_REQUEST_DETAIL_PICK_LOCATION + mEdtPickUpLocation.getText().toString().trim();
        // Dropoff Time - 10 AM "
        // Drop-off day
        if(!StringUtil.isEmpty(requestDetails)){
            requestDetails += " | ";
        }
        requestDetails += AppConstant.B2C_REQUEST_DETAIL_DROP_OF_DATE + DateTimeUtil.shareInstance().formatTimeStamp(dropCal.getTimeInMillis(), AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS);

        // Drop-off location
        requestDetails += " | " + AppConstant.B2C_REQUEST_DETAIL_DROP_LOCATION + StringUtil.removeAllSpecialCharacterAndBreakLine(mEdtDropOffLoction.getText().toString().trim());

        // Preferred car rental company
        if (!TextUtils.isEmpty(edtCarRentalCompany.getText().toString())) {
            if(!StringUtil.isEmpty(requestDetails)){
                requestDetails += " | ";
            }

            requestDetails += AppConstant.B2C_REQUEST_DETAIL_PREF_CAR_RENTAL_COMPANY + StringUtil.removeAllSpecialCharacterAndBreakLine(edtCarRentalCompany.getText().toString());
        }

        if(mEdtMaximumPrice.getText().toString().trim().length() > 0){
            if (!TextUtils.isEmpty(requestDetails)) {
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_BUDGET_RANGE_PER_DAY + mEdtMaximumPrice.getText().toString().trim();
        }

        return requestDetails;
    }
    private void setDetailData(CarBookingDetailData data) {
        if(getActivity()==null) {
            return;
        }
        mEdtDriverName.setText(data.getDriverName());
        mEdtAgeOfDriver.setText(data.getDriverAge());
        mTbSwitchInternationalLicense.setChecked(data.getInternationalLicense());
        mEdtPickUpLocation.setText(data.getConciergeRequestDetail().getPickupLocation());
        mEdtDropOffLoction.setText(data.getConciergeRequestDetail().getDropoffLocation());

        // Preferred vehicle
        String prefVehicle = data.getConciergeRequestDetail().getPreferredVehicle();
        if(!TextUtils.isEmpty(prefVehicle)){
            String[] vehicles = prefVehicle.split("\\s*,\\s*");
            for(String vehicle : vehicles){
                mVehicleSelected.add(new RentalObject(vehicle));
            }
            if(mVehicleSelected.size() > 0){
                updateVehicle();
            }
        }
        // Rental company
        edtCarRentalCompany.setText(data.getConciergeRequestDetail().getPreferredcarrentalcompany());

        if(!TextUtils.isEmpty(data.getConciergeRequestDetail().getBudgetRangeCurrencyperday())) {
            mEdtMaximumPrice.setText(data.getConciergeRequestDetail().getBudgetRangeCurrencyperday());
        }
        mEdtAnySpecialRequirements.setText(data.getConciergeRequestDetail().getSpecialRequirement());

        pickCal = Calendar.getInstance();
        pickCal.setTimeInMillis(data.getPickupDateEpoc());
        dropCal = Calendar.getInstance();
        dropCal.setTimeInMillis(data.getDropOffDateEpoc());
        showDateTime();

        // Contact view
        mContactView.updateContactView(data);
    }
    private void updateUIFromDefaultPreference(CarPreferenceDetailData carPreferenceDetailData){
        if(getActivity()==null) {
            return;
        }
        if(carPreferenceDetailData != null){
            // Preferred vehicles
            if(!TextUtils.isEmpty(carPreferenceDetailData.getPreferredRentalVehicle())){
                String[] selectedRentalVehicleArr = carPreferenceDetailData.getPreferredRentalVehicle().split("\\s*,\\s*");
                if(selectedRentalVehicleArr != null && selectedRentalVehicleArr.length > 0){
                    mVehicleSelected = new ArrayList<>();
                    for(String selectedRentalVehicle : selectedRentalVehicleArr){
                        String vehicleRu =CommonUtils.getRusiaPreferredVehicleString(selectedRentalVehicle);
                        if(CommonUtils.isStringValid(vehicleRu)){
                            if(!carPreferenceDetailData.getPreferredRentalVehicle().contains(vehicleRu)) {
                                mVehicleSelected.add(new RentalObject(vehicleRu));
                            }
                        }else {
                            mVehicleSelected.add(new RentalObject(selectedRentalVehicle));
                        }
                    }
                }
                updateVehicle();
            }
            // Rental company
            edtCarRentalCompany.setText(carPreferenceDetailData.getPreferredRentalCompany());
        }
    }
    @Override
    public void onB2CResponse(B2CBaseResponse response) {
        hideDialogProgress();
        if(getActivity()==null) {
            return;
        }
        if(response instanceof B2CUpsertConciergeRequestResponse){
            if(response != null && response.isSuccess()){
                onBackPress();
                ThankYouFragment thankYouFragment = new ThankYouFragment();
                if (myRequestObject != null) {
//                    Bundle bundle = new Bundle();
//                    bundle.putString(ThankYouFragment.LINE_MAIN,
//                            getString(R.string.text_message_thank_you_amend));
//                    thankYouFragment.setArguments(bundle);
                    EventBus.getDefault().post(AppConstant.CONCIERGE_EDIT_TYPE.AMEND);
                } else {
                    // Track product
                    AppContext.getSharedInstance().track(AppConstant.TRACK_EVENT_PRODUCT_CATEGORY.CAR_RENTAL.getValue(), AppConstant.TRACK_EVENT_PRODUCT_NAME.GENERIC_BOOK.getValue());
                    EventBus.getDefault().post(AppConstant.CONCIERGE_EDIT_TYPE.ADD);
                }
                pushFragment(thankYouFragment,
                        true,
                        true);
            }else{
                //showDialogMessage("Error", response.getMessage());
                showDialogMessage(getString(R.string.text_title_dialog_error), getString(R.string.text_concierge_request_error));
                if(upsertConciergeRequestRequest != null){
                    uploadedPhotoPath = upsertConciergeRequestRequest.getAttachmentPath();
                }
            }
        }else if(response instanceof B2CGetRecentRequestResponse){
            setDetailData(new CarBookingDetailData((B2CGetRecentRequestResponse)response));
        }
    }

    @Override
    public void onB2CResponseOnList(List<B2CBaseResponse> responseList) {
        if(getActivity()==null) {
            return;
        }
    }

    @Override
    public void onB2CFailure(String errorMessage, String errorCode) {
        hideDialogProgress();
        if(getActivity()==null) {
            return;
        }
        //showDialogMessage("Error", errorMessage);
        if(isSubmitClicked) {
            isSubmitClicked = false;
            showDialogMessage(getString(R.string.text_title_dialog_error), getString(R.string.text_concierge_request_error));
        }
        // Store the uploaded successful photo path
        // So that the upsert failed in the first time, user tried to process for the second time
        // then that is no need to upload again
        if(upsertConciergeRequestRequest != null){
            uploadedPhotoPath = upsertConciergeRequestRequest.getAttachmentPath();
        }
    }
    @Override
    public void onResponse(final Call call,
                           final Response response) {
        if (response.body() instanceof BookRentalResponse) {
            hideDialogProgress();
            if(getActivity()==null) {
                return;
            }
            BookRentalResponse bookRentalResponse = ((BookRentalResponse) response.body());
            int code = bookRentalResponse.getStatus();
            if (code == 200) {
                onBackPress();
                ThankYouFragment thankYouFragment = new ThankYouFragment();
                if (myRequestObject != null) {
                    Bundle bundle = new Bundle();
                    bundle.putString(ThankYouFragment.LINE_MAIN,
                                     getString(R.string.text_message_thank_you_amend));
                    thankYouFragment.setArguments(bundle);
                }
                pushFragment(thankYouFragment,
                             true,
                             true);

            } else {
//                showToast(bookRentalResponse.getMessage());
            }
        }
        if (response.body() instanceof CarRentalBookingDetailResponse) {
            hideDialogProgress();
            if(getActivity()==null) {
                return;
            }
            CarRentalBookingDetailResponse carRentalBookingDetailResponse =
                    ((CarRentalBookingDetailResponse) response.body());
            int code = carRentalBookingDetailResponse.getStatus();
            if (code == 200) {
                setDetailData(carRentalBookingDetailResponse.getData());
            } else {
//                showToast(carRentalBookingDetailResponse.getMessage());
            }
        }


    }

    @Override
    public void onFailure(final Call call,
                          final Throwable t) {
        hideDialogProgress();
        if(getActivity()==null) {
            return;
        }
//        showToast(getString(R.string.text_server_error_message));
    }


    @Override
    public void onSelectCountryClick() {
        Bundle bundle1 = new Bundle();
        bundle1.putString(SelectionFragment.PRE_SELECTION_TYPE,
                          SelectionFragment.PRE_SELECTION_COUNTRY_CODE);
        bundle1.putString(SelectionFragment.PRE_SELECTION_DATA,
                          mContactView.getSelectedCountryCode());
        SelectionFragment fragment1 = new SelectionFragment();
        fragment1.setSelectionCallBack(this);
        fragment1.setArguments(bundle1);
        pushFragment(fragment1,
                     true,
                     true);
    }
    private B2CICallback getPreferenceCallback = new B2CICallback() {
        @Override
        public void onB2CResponse(B2CBaseResponse response) {
            hideDialogProgress();
            if(getActivity()==null) {
                return;
            }
        }

        @Override
        public void onB2CResponseOnList(List<B2CBaseResponse> responseList) {
            if(getActivity() != null) {
            hideDialogProgress();
                if(getActivity()==null) {
                    return;
                }
            CarPreferenceDetailData carPreferenceDetailData = (CarPreferenceDetailData) PreferenceData.findFromList(responseList, AppConstant.PREFERENCE_TYPE.CAR);
            updateUIFromDefaultPreference(carPreferenceDetailData);
        }
        }

        @Override
        public void onB2CFailure(String errorMessage, String errorCode) {
            hideDialogProgress();
            if(getActivity()==null) {
                return;
            }
        }
    };
}
