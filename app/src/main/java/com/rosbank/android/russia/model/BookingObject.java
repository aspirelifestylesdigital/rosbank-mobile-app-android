package com.rosbank.android.russia.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;


public class BookingObject
        implements Parcelable {
    @Expose
    private String BookingId;

    public String getBookingId() {
        return BookingId;
    }

    public void setBookingId(final String bookingId) {
        BookingId = bookingId;
    }

    public String getBookingName() {
        return BookingName;
    }

    public void setBookingName(final String bookingName) {
        BookingName = bookingName;
    }

    @Expose
    private String BookingName;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel parcel,
                              final int i) {
        parcel.writeString(this.BookingId);
        parcel.writeString(this.BookingName);

    }
    protected BookingObject(Parcel in) {
        this.BookingId = in.readString();
        this.BookingName = in.readString();
    }
    public BookingObject() {
        this.BookingId = "";
        this.BookingName = "";
    }

    public static final Creator<BookingObject> CREATOR = new Creator<BookingObject>() {
        @Override
        public BookingObject createFromParcel(Parcel source) {
            return new BookingObject(source);
        }

        @Override
        public BookingObject[] newArray(int size) {
            return new BookingObject[size];
        }
    };
}
