package com.rosbank.android.russia.apiservices.RequestModel;


import com.google.gson.annotations.Expose;

public class UpdateMyProfileRequest
        extends BaseRequest {
    @Expose
    private String Salutation;
    @Expose
    private String FirstName;
    @Expose
    private String LastName;
    @Expose
    private String MobileNumber;


    @Expose
    private String UserID;


    public String getSalutation() {
        return Salutation;
    }

    public void setSalutation(final String salutation) {
        Salutation = salutation;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(final String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(final String lastName) {
        LastName = lastName;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(final String userID) {
        UserID = userID;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(final String mobileNumber) {
        MobileNumber = mobileNumber;
    }



}
