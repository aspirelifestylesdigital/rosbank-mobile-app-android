package com.rosbank.android.russia.apiservices;

import com.rosbank.android.russia.apiservices.RequestModel.SignInRequest;
import com.rosbank.android.russia.apiservices.ResponseModel.SignInResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.ApiProviderClient;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Callback;


public class WSUserLogin
        extends ApiProviderClient<SignInResponse> {
    //
    private String mUser;
    private String mPassword;

    public WSUserLogin(){

    }

    public void login(String user, String psw){
        mUser = user;
        mPassword = psw;
    }

    @Override
    public void run(Callback<SignInResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }

    @Override
    protected boolean isUseAuthenticateInApi() {
        return true;
    }

    @Override
    public void runApi() {
        SignInRequest request = new SignInRequest();
        request.setEmail(mUser);
        request.setPassword(mPassword);
        serviceInterface.userLogin(request).enqueue(callback);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("User-Agent", "Retrofit-Sample-App");
        return headers;
    }

}
