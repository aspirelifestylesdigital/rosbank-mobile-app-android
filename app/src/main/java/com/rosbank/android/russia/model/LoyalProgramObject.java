package com.rosbank.android.russia.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;


public class LoyalProgramObject
        implements Parcelable {
    public static final String PRE_TEMP_CATEGORY_CAR_RENTAL = "Car Rental and Transfers";
    public static final String PRE_TEMP_CATEGORY_HOTEL = "Hotel";
    @Expose
    private String ID;
    @Expose
    private String CategoryId;
    @Expose
    private String Category;
    @Expose
    private String Name;
    @Expose
    private String MembershipNumber;
    @Expose
    private String TempCategory;//Car Rental and Transfers //Hotel
    @Expose
    private String Index;

    public String getID() {
        return ID;
    }

    public void setID(final String ID) {
        this.ID = ID;
    }

    public String getCategoryId() {
        return CategoryId;
    }

    public void setCategoryId(final String categoryId) {
        CategoryId = categoryId;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(final String category) {
        Category = category;
    }

    public String getName() {
        return Name;
    }

    public void setName(final String name) {
        Name = name;
    }

    public String getMembershipNumber() {
        return MembershipNumber;
    }

    public void setMembershipNumber(final String membershipNumber) {
        MembershipNumber = membershipNumber;
    }

    public String getTempCategory() {
        return TempCategory;
    }

    public void setTempCategory(final String tempCategory) {
        TempCategory = tempCategory;
    }

    public String getIndex() {
        return Index;
    }

    public void setIndex(final String index) {
        Index = index;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel parcel,
                              final int i) {
        parcel.writeString(this.ID);
        parcel.writeString(this.CategoryId);
        parcel.writeString(this.Category);
        parcel.writeString(this.Name);
        parcel.writeString(this.MembershipNumber);
        parcel.writeString(this.TempCategory);
        parcel.writeString(this.Index);



    }
    public LoyalProgramObject() {

    }
    protected LoyalProgramObject(Parcel in) {
        this.ID = in.readString();
        this.CategoryId = in.readString();
        this.Category = in.readString();
        this.Name = in.readString();
        this.MembershipNumber = in.readString();
        this.TempCategory = in.readString();
        this.Index = in.readString();
    }
    public static final Creator<LoyalProgramObject> CREATOR = new Creator<LoyalProgramObject>() {
        @Override
        public LoyalProgramObject createFromParcel(Parcel source) {
            return new LoyalProgramObject(source);
        }

        @Override
        public LoyalProgramObject[] newArray(int size) {
            return new LoyalProgramObject[size];
        }
    };
}
