package com.rosbank.android.russia.apiservices.ResponseModel;


import com.rosbank.android.russia.apiservices.apis.appimplement.BaseResponse;
import com.rosbank.android.russia.model.AuthorizeToken;
import com.google.gson.annotations.Expose;

public class AuthenticateResponse
        extends BaseResponse {

    @Expose
    public AuthorizeToken Data;

    public AuthorizeToken getData() {
        return Data;
    }

    public void setData(AuthorizeToken data) {
        this.Data = data;
    }

}
