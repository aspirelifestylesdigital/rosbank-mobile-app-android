package com.rosbank.android.russia.utils;

import android.Manifest;
import android.app.Activity;
import android.app.Fragment;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v13.app.FragmentCompat;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

/**
 * Created by ThuNguyen on 5/10/2016.
 */
public class SelfPermissionUtils {

    private static SelfPermissionUtils instance;

    private SelfPermissionUtils() {
    }

    public static SelfPermissionUtils getInstance(){
        if(instance==null){
            instance = new SelfPermissionUtils();
        }
        return instance;
    }

    public boolean checkOrRequestCameraPermission(Activity activity, int requestCode){
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }

        if(ContextCompat.checkSelfPermission(activity,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED){

            // No explanation needed, we can request the permission.
            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.CAMERA},
                    requestCode);
            return false;
        }
        return true;
    }

    public boolean checkOrRequestCameraPermission(android.support.v4.app.Fragment fragment, int requestCode){
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }

        if(ContextCompat.checkSelfPermission(fragment.getContext(),
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED){

            // No explanation needed, we can request the permission.
            ActivityCompat.requestPermissions(fragment.getActivity(),
                    new String[]{Manifest.permission.CAMERA},
                    requestCode);
            return false;
        }
        return true;
    }

    public boolean checkOrRequestWriteExternalStoragePermission(Activity activity, int requestCode){
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }

        if(ContextCompat.checkSelfPermission(activity,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED){

            // No explanation needed, we can request the permission.
            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    requestCode);
            return false;
        }
        return true;
    }

    public boolean checkOrRequestWriteExternalStoragePermission(android.support.v4.app.Fragment fragment, int requestCode){
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }

        if(ContextCompat.checkSelfPermission(fragment.getContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED){

            // No explanation needed, we can request the permission.
            ActivityCompat.requestPermissions(fragment.getActivity(),
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    requestCode);
            return false;
        }
        return true;
    }

    public boolean checkOrRequestReadExternalStoragePermission(Activity activity, int requestCode){
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }

        if(ContextCompat.checkSelfPermission(activity,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED){

            // No explanation needed, we can request the permission.
            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    requestCode);
            return false;
        }
        return true;
    }

    public boolean checkOrRequestReadExternalStoragePermission(android.support.v4.app.Fragment fragment, int requestCode){
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }

        if(ActivityCompat.checkSelfPermission(fragment.getContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED){

            // No explanation needed, we can request the permission.
            ActivityCompat.requestPermissions(fragment.getActivity(),
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    requestCode);
            return false;
        }
        return true;
    }

    public boolean checkOrRequestBluetoothPermission(Activity activity, int requestCode){
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }

        if(ContextCompat.checkSelfPermission(activity,
                Manifest.permission.BLUETOOTH)
                != PackageManager.PERMISSION_GRANTED){

            // No explanation needed, we can request the permission.
            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_PRIVILEGED},
                    requestCode);
            return false;
        }
        return true;
    }

    public boolean checkOrRequestLocationPermission(Activity activity, int requestCode){
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }

        if(ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){

            // No explanation needed, we can request the permission.
            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    requestCode);

            return false;
        }
        return true;
    }

    public boolean checCallPhonePermission(Activity activity, int requestCode){
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }

        if(ActivityCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED){

            // No explanation needed, we can request the permission.
            ActivityCompat.requestPermissions(activity,
                    new String[]{ Manifest.permission.CALL_PHONE },
                    requestCode);

            return false;
        }
        return true;
    }

    public boolean checCallPhonePermission(Fragment fragment, int requestCode){
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }

        //use:android.support.v13.app
        //FragmentCompat.OnRequestPermissionsResultCallback;
        if(ActivityCompat.checkSelfPermission(fragment.getActivity(), Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED){

            // No explanation needed, we can request the permission.
            FragmentCompat.requestPermissions(fragment,
                    new String[]{ Manifest.permission.CALL_PHONE },
                    requestCode);

            return false;
        }
        return true;
    }
    public boolean checkReadWriteCalendarPermission(Activity activity, int requestCode){
       if(ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED
                   && ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED){

            // No explanation needed, we can request the permission.
            ActivityCompat.requestPermissions(activity,
                                              new String[]{Manifest.permission.WRITE_CALENDAR, Manifest.permission.READ_CALENDAR},
                                              requestCode);

            return false;
        }
        return true;
    }

}
