package com.rosbank.android.russia.interfaces;

import android.view.View;

/**
 * Created by apple on 3/23/16.
 */
public interface OnRecyclerViewItemClickListener {
    void onItemClick(int position, View view);
}
