package com.rosbank.android.russia.model;

import com.google.gson.annotations.Expose;

/**
 * Created by nga.nguyent on 9/29/2016.
 */

public class KeyValueObject {
    @Expose
    String Key;
    @Expose
    String Value;

    public KeyValueObject(){}
    public KeyValueObject(String Key, String Value){
        this.Key = Key;
        this.Value = Value;
    }
    public String getKey() {
        return Key;
    }

    public void setKey(String key) {
        Key = key;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }

    @Override
    public String toString() {
        return Value;
    }

    @Override
    public boolean equals(Object obj) {
        return Value.equals(obj.toString());
    }
}
