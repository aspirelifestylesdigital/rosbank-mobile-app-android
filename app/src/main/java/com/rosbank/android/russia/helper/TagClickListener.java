package com.rosbank.android.russia.helper;

import android.os.Handler;
import android.view.View;

import com.rosbank.android.russia.utils.Logger;

/**
 * Created by anh.trinh on 9/19/2016.
 */
public abstract class TagClickListener implements View.OnClickListener {

    private static final long DOUBLE_CLICK_TIME_DELTA = 300;//milliseconds
    int click = 0;
    long lastClickTime = 0;
    View view;

    Handler handler = new Handler();
    Runnable r = new Runnable() {
        @Override
        public void run() {
            long clickTime = System.currentTimeMillis();
            long distance = clickTime - lastClickTime;
            Logger.sout(distance + "");
            if (distance >= DOUBLE_CLICK_TIME_DELTA || click==1 ){
                if(click<=1)
                    onSingleClick(view);
                else
                    onDoubleClick(view);

                //reset time
                click = 0;
            }
        }
    };

    @Override
    public void onClick(View v) {
        view = v;
        lastClickTime = System.currentTimeMillis();
        click++;
        handler.postDelayed(r, DOUBLE_CLICK_TIME_DELTA);
    }

    public abstract void onSingleClick(View v);
    public abstract void onDoubleClick(View v);
}