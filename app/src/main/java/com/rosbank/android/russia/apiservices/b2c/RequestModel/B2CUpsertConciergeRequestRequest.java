package com.rosbank.android.russia.apiservices.b2c.RequestModel;

import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.rosbank.android.russia.BuildConfig;
import com.rosbank.android.russia.apiservices.RequestModel.BaseRequest;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.model.CountryObject;
import com.rosbank.android.russia.model.UserItem;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.SharedPreferencesUtils;
import com.rosbank.android.russia.utils.StringUtil;
import com.rosbank.android.russia.widgets.ContactView;
import com.rosbank.android.russia.widgets.CountryCityRequestView;

import java.util.Locale;

/**
 * Created by ThuNguyen on 10/26/2016.
 */

public class B2CUpsertConciergeRequestRequest extends BaseRequest {
    @Expose
    protected String AccessToken;
    @Expose
    private String AttachmentPath;
    @Expose
    private String ConsumerKey;
    @Expose
    private String TransactionID;
    // Event Date  in Reserve Table
    @Expose
    private String Delivery;
    @Expose
    private String EPCClientCode;
    @Expose
    private String EPCProgramCode;
    @Expose
    private String EditType;
    @Expose
    private String Email1;
    // Email1 in Reserve Table
    @Expose
    private String EmailAddress1;
    @Expose
    private String FirstName;
    @Expose
    private String Functionality;
    @Expose
    private String LastName;
    @Expose
    private String OnlineMemberId;
    @Expose
    private String PhoneNumber;
    @Expose
    private String PrefResponse;
    @Expose
    private String RequestDetails;
    @Expose
    private String RequestType;
    @Expose
    private String Salutation;
    @Expose
    private String Source;
    @Expose
    private String VerificationCode;
    @Expose
    private String City;
    @Expose
    private String Country;
    @Expose
    private String NumberOfAdults;
    @Expose
    private String NumberOfKids;
    @Expose
    private String NumberOfChildren;
    @Expose
    private String StartDate;
    @Expose
    private String EndDate;
    @Expose
    private String PickupDate;
    @Expose
    private String EventDate;
    @Expose
    private String Situation;
    @Expose
    private String NoofHrs;
    @Expose
    private String CheckIn;
    @Expose
    private String CheckOut;
    @Expose
    private String Checkindate;
    @Expose
    private String Checkoutdate;
    @Expose
    private String EventName;
    @Expose
    private String PickUp;

    @Expose
    private String State;

    public B2CUpsertConciergeRequestRequest(String test) {

    }

    public B2CUpsertConciergeRequestRequest() {
        AccessToken = SharedPreferencesUtils.getPreferences(AppConstant.PRE_B2C_ACCESS_TOKEN, "");
        ConsumerKey = UserItem.isVip() ? BuildConfig.B2C_CONSUMER_KEY_PRIVATE : BuildConfig.B2C_CONSUMER_KEY_REGULAR;
        OnlineMemberId = SharedPreferencesUtils.getPreferences(AppConstant.PRE_B2C_ONLINE_MEMBER_ID, "");
        EPCClientCode = UserItem.isVip() ? BuildConfig.B2C_EPC_CLIENT_CODE_PRIVATE : BuildConfig.B2C_EPC_CLIENT_CODE_REGULAR;
        EPCProgramCode = UserItem.isVip() ? BuildConfig.B2C_EPC_PROGRAM_CODE_PRIVATE : BuildConfig.B2C_EPC_PROGRAM_CODE_REGULAR;
        VerificationCode = UserItem.isVip() ? BuildConfig.B2C_VERIFICATION_CODE_PRIVATE : BuildConfig.B2C_VERIFICATION_CODE_REGULAR;
        Salutation = CommonUtils.convertSalutationToEnglish(SharedPreferencesUtils.getPreferences(AppConstant.USER_SALUTATION, ""));
        FirstName = SharedPreferencesUtils.getPreferences(AppConstant.USER_FIRST_NAME, "");
        LastName = SharedPreferencesUtils.getPreferences(AppConstant.USER_LAST_NAME, "");
        Source = "Mobile App";
    }

    public void updateDataFromContactView(ContactView contactView) {
        if (contactView.isEmailChecked()) {
            setEmail1(contactView.getEmail());
            setEmailAddress1(contactView.getEmail());
            setPhoneNumber(SharedPreferencesUtils.getPreferences(AppConstant.USER_MOBILE_NUMBER, ""));
            setPrefResponse("Email");
        } else if (contactView.isPhoneChecked()) {
            setEmail1(SharedPreferencesUtils.getPreferences(AppConstant.USER_EMAIL_PRE, ""));
            setEmailAddress1(SharedPreferencesUtils.getPreferences(AppConstant.USER_EMAIL_PRE, ""));
            setPhoneNumber(contactView.getFullPhone());
            setPrefResponse("Phone");
        } else {
            setEmail1(contactView.getEmail());
            setEmailAddress1(contactView.getEmail());
            setPhoneNumber(contactView.getFullPhone());
            setPrefResponse("Email,Phone");
        }
        String[] firstLastName = StringUtil.getKeyValueFromSplitor(contactView.getReservationName(), " ");
        if (firstLastName.length > 0) {
            if (CommonUtils.isStringValid(firstLastName[0])) {
                setFirstName(firstLastName[0]);
            }
            if (CommonUtils.isStringValid(firstLastName[1])) {
                setLastName(firstLastName[1]);
            }

        }
    }

    public void updateDefaultPersonalInformation() {
        setEmailAddress1(SharedPreferencesUtils.getPreferences(AppConstant.USER_EMAIL_PRE, ""));
        setEmail1(SharedPreferencesUtils.getPreferences(AppConstant.USER_EMAIL_PRE, ""));
        setPhoneNumber(SharedPreferencesUtils.getPreferences(AppConstant.USER_MOBILE_NUMBER, ""));
        setPrefResponse("Email,Phone");
        String[] firstLastName = StringUtil.getKeyValueFromSplitor(SharedPreferencesUtils.getPreferences(AppConstant.USER_FULL_NAME, ""), " ");
        setFirstName(firstLastName[0]);
        setLastName(firstLastName[1]);
    }

    public void updateDataFromCountryCityView(CountryCityRequestView countryCityRequestView) {

        if (countryCityRequestView.getCountry() == null || countryCityRequestView.getCountry().equals("Not Available") || CountryObject.getCountryObjectFromName(countryCityRequestView.getCountry()) == null) {
            setCountry(Locale.getDefault().getISO3Language().toUpperCase());
            setState(Locale.getDefault().getISO3Language().toUpperCase());
        } else {
            setCountry(CountryObject.getCountryObjectFromName(countryCityRequestView.getCountry()).getCountryCode());
            setState(countryCityRequestView.getState().isEmpty() ? CountryObject.getCountryObjectFromName(countryCityRequestView.getCountry()).getCountryCode() : countryCityRequestView.getState());
        }
        setCity(countryCityRequestView.getCity());
    }

    public String getNumberOfChildren() {
        return NumberOfChildren;
    }

    public void setNumberOfChildren(String numberOfChildren) {
        NumberOfChildren = numberOfChildren;
    }

    public String getCheckindate() {
        return Checkindate;
    }

    public void setCheckindate(String checkindate) {
        Checkindate = checkindate;
    }

    public String getCheckoutdate() {
        return Checkoutdate;
    }

    public void setCheckoutdate(String checkoutdate) {
        Checkoutdate = checkoutdate;
    }

    public String getPickUp() {
        return PickUp;
    }

    public void setPickUp(final String pickUp) {
        PickUp = pickUp;
    }

    public String getAccessToken() {
        return AccessToken;
    }

    public void setAccessToken(String accessToken) {
        AccessToken = accessToken;
    }

    public String getAttachmentPath() {
        return AttachmentPath;
    }

    public void setAttachmentPath(String attachmentPath) {
        AttachmentPath = attachmentPath;
    }

    public String getConsumerKey() {
        return ConsumerKey;
    }

    public void setConsumerKey(String consumerKey) {
        ConsumerKey = consumerKey;
    }

    public String getDelivery() {
        return Delivery;
    }

    public void setDelivery(String delivery) {
        Delivery = delivery;
    }

    public String getEPCClientCode() {
        return EPCClientCode;
    }

    public void setEPCClientCode(String EPCClientCode) {
        this.EPCClientCode = EPCClientCode;
    }

    public String getEPCProgramCode() {
        return EPCProgramCode;
    }

    public void setEPCProgramCode(String EPCProgramCode) {
        this.EPCProgramCode = EPCProgramCode;
    }

    public String getEditType() {
        return EditType;
    }

    public void setEditType(String editType) {
        EditType = editType;
    }

    public String getEmail1() {
        return Email1;
    }

    public void setEmail1(String email1) {
        Email1 = email1;
        EmailAddress1 = email1;
    }

    public String getEmailAddress1() {
        return EmailAddress1;
    }

    public void setEmailAddress1(final String emailAddress1) {
        EmailAddress1 = emailAddress1;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getFunctionality() {
        return Functionality;
    }

    public void setFunctionality(String functionality) {
        Functionality = functionality;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getOnlineMemberId() {
        return OnlineMemberId;
    }

    public void setOnlineMemberId(String onlineMemberId) {
        OnlineMemberId = onlineMemberId;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        if (!TextUtils.isEmpty(phoneNumber)) {
            PhoneNumber = phoneNumber;
        } else {
            PhoneNumber = UserItem.getLoginedMobileBumber();
        }
    }

    public String getPrefResponse() {
        return PrefResponse;
    }

    public void setPrefResponse(String prefResponse) {
        PrefResponse = prefResponse;
    }

    public String getRequestDetails() {
        return RequestDetails;
    }

    public void setRequestDetails(String requestDetails) {
        RequestDetails = requestDetails;
    }

    public String getRequestType() {
        return RequestType;
    }

    public void setRequestType(String requestType) {
        RequestType = requestType;
    }

    public String getSalutation() {
        return Salutation;
    }

    public void setSalutation(String salutation) {
        Salutation = salutation;
    }

    public String getSource() {
        return Source;
    }

    public void setSource(String source) {
        Source = source;
    }

    public String getVeriCode() {
        return VerificationCode;
    }

    public void setVeriCode(String veriCode) {
        VerificationCode = veriCode;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = StringUtil.removeAllSpecialCharacterAndBreakLine(city);
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getNumberOfAdults() {
        return NumberOfAdults;
    }

    public void setNumberOfAdults(String numberOfAdults) {
        NumberOfAdults = numberOfAdults;
    }

    public String getNumberOfKids() {
        return NumberOfKids;
    }

    public void setNumberOfKids(String numberOfKids) {
        NumberOfKids = numberOfKids;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    public String getPickupDate() {
        return PickupDate;
    }

    public void setPickupDate(String pickupDate) {
        PickupDate = pickupDate;
    }

    public String getTransactionID() {
        return TransactionID;
    }

    public void setTransactionID(String transactionID) {
        TransactionID = transactionID;
    }

    public String getEventDate() {
        return EventDate;
    }

    public void setEventDate(String eventDate) {
        EventDate = eventDate;
    }

    public String getSituation() {
        return Situation;
    }

    public void setSituation(String situation) {
        Situation = StringUtil.removeAllSpecialCharacterAndBreakLine(situation);
    }

    public String getNoofHrs() {
        return NoofHrs;
    }

    public void setNoofHrs(String noofHrs) {
        NoofHrs = noofHrs;
    }

    public String getCheckOut() {
        return CheckOut;
    }

    public void setCheckOut(final String checkOut) {
        CheckOut = checkOut;
    }

    public String getCheckIn() {
        return CheckIn;
    }

    public void setCheckIn(final String checkIn) {
        CheckIn = checkIn;
    }

    public String getEventName() {
        return EventName;
    }

    public void setEventName(final String eventName) {
        EventName = eventName;
    }


}
