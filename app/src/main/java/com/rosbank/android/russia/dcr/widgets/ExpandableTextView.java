package com.rosbank.android.russia.dcr.widgets;
/*
 * Copyright (C) 2011 The Android Open Source Project
 * Copyright 2014 Manabu Shimobe
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.SparseBooleanArray;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.FontUtils;

public class ExpandableTextView extends FrameLayout implements View.OnClickListener {

    private static final String TAG = ExpandableTextView.class.getSimpleName();

    /* The default number of lines */
    private static final int MAX_COLLAPSED_LINES = 2;

    /* The default animation duration */
    private static final int DEFAULT_ANIM_DURATION = 300;

    /* The default alpha value when the animation starts */
    private static final float DEFAULT_ANIM_ALPHA_START = 0.7f;

    protected TextView mTv;

    protected View mAnchorView;
    protected View mRootAnchorView;
    protected TextView mTvExpand; // Button to expand/collapse
    protected ImageView mIvExpand;

    private boolean mRelayout;

    private boolean mCollapsed = true; // Show short version as default.

    private int mCollapsedHeight;
    private int mExpandedHeight;

    private int mTextHeightWithMaxLines;

    private int mMaxCollapsedLines;

    private int mMarginBetweenTxtAndBottom;

    private Drawable mExpandDrawable;

    private Drawable mCollapseDrawable;

    private String mExpandText;
    private String mCollapseText;
    private boolean isAnchorAbove;
    private int mAnimationDuration;

    private float mAnimAlphaStart;

    private boolean mAnimating;
    private boolean mInflateInnerImage;
    private float mTextSize;

    /* Listener for callback */
    private OnExpandStateChangeListener mListener;

    /* For saving collapsed status when used in ListView */
    private SparseBooleanArray mCollapsedStatus;
    private int mPosition;

    public ExpandableTextView(Context context) {
        this(context, null);
    }

    public ExpandableTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public ExpandableTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    public void setmRelayout(boolean mRelayout) {
        this.mRelayout = mRelayout;
    }

    @Override
    public void onClick(View view) {
        if (mAnchorView.getVisibility() != View.VISIBLE) {
            return;
        }

        mCollapsed = !mCollapsed;
        renderAnchorView();

        if (mCollapsedStatus != null) {
            mCollapsedStatus.put(mPosition, mCollapsed);
        }

        // mark that the animation is in progress
        mAnimating = true;
        // notify the listener
        if (mListener != null) {
            mListener.onBeginExpandOrCollapse(mTv);
        }
        Animation animation;
        if (mCollapsed) {
            animation = new ExpandCollapseAnimation(this, getHeight(), mCollapsedHeight);
        } else {
            animation = new ExpandCollapseAnimation(this, getHeight(), getHeight() +
                    mTextHeightWithMaxLines - mTv.getHeight());
        }

        animation.setFillAfter(true);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                applyAlphaAnimation(mTv, mAnimAlphaStart);
            }
            @Override
            public void onAnimationEnd(Animation animation) {
                // clear animation here to avoid repeated applyTransformation() calls
                clearAnimation();
                // clear the animation flag
                mAnimating = false;
                if(mCollapsed){
                    mTv.setMaxLines(mMaxCollapsedLines);

                }else{
                    mTv.setMaxLines(Integer.MAX_VALUE);
                }
                // notify the listener
                if (mListener != null) {
                    mListener.onExpandStateChanged(mTv, !mCollapsed, -getDifferentCollapseExpand());
                }

                if(mInflateInnerImage){
                    mInflateInnerImage = false;
                    inflateAfterInnerImageRendered();
                }
            }
            @Override
            public void onAnimationRepeat(Animation animation) { }
        });

        clearAnimation();
        startAnimation(animation);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        // while an animation is in progress, intercept all the touch events to children to
        // prevent extra clicks during the animation
        return mAnimating;
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        findViews();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if(!mCollapsed){
            mExpandedHeight = getMeasuredHeight();
        }
        // If no change, measure and return
        if (!mRelayout || getVisibility() == View.GONE) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            return;
        }
        mRelayout = false;

        // Setup with optimistic case
        // i.e. Everything fits. No button needed
        mAnchorView.setVisibility(View.GONE);
        mTv.setMaxLines(Integer.MAX_VALUE);

        // Measure
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        // If the text fits in collapsed mode, we are done.
        if (mTv.getLineCount() <= mMaxCollapsedLines) {
            return;
        }

        // Saves the text height w/ max lines
        mTextHeightWithMaxLines = getRealTextViewHeight(mTv);

        // Doesn't fit in collapsed mode. Collapse text view as needed. Show
        // button.
        if (mCollapsed) {
            mTv.setMaxLines(mMaxCollapsedLines);
        }
        mAnchorView.setVisibility(View.VISIBLE);
        // Re-measure with new setup
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        if (mCollapsed) {
            // Gets the margin between the TextView's bottom and the ViewGroup's bottom
            mTv.post(new Runnable() {
                @Override
                public void run() {
                    mMarginBetweenTxtAndBottom = getHeight() - mTv.getHeight();
                }
            });
            // Saves the collapsed height of this ViewGroup
            mCollapsedHeight = getMeasuredHeight();
        }
    }

    public void setOnExpandStateChangeListener(@Nullable OnExpandStateChangeListener listener) {
        mListener = listener;
    }

    public void setmMaxCollapsedLines(int mMaxCollapsedLines) {
        this.mMaxCollapsedLines = mMaxCollapsedLines;
    }

    public void setText(@Nullable CharSequence text) {
        mRelayout = true;
        mTv.setText(text);
        setVisibility(TextUtils.isEmpty(text) ? View.GONE : View.VISIBLE);
    }
    public void setText(@Nullable Spanned text) {
        mRelayout = true;
        mTv.setText(text);
        setVisibility(TextUtils.isEmpty(text) ? View.GONE : View.VISIBLE);
    }
    public void setText(@Nullable CharSequence text, @NonNull SparseBooleanArray collapsedStatus, int position) {
        mCollapsedStatus = collapsedStatus;
        mPosition = position;
        boolean isCollapsed = collapsedStatus.get(position, true);
        clearAnimation();
        mCollapsed = isCollapsed;
        renderAnchorView();
        setText(text);
        getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
        requestLayout();
    }

    @Nullable
    public CharSequence getText() {
        if (mTv == null) {
            return "";
        }
        return mTv.getText();
    }
    public TextView getTextView(){
        return mTv;
    }
    private void init(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.ExpandableTextView);
        mMaxCollapsedLines = typedArray.getInt(R.styleable.ExpandableTextView_maxCollapsedLines, MAX_COLLAPSED_LINES);
        mAnimationDuration = typedArray.getInt(R.styleable.ExpandableTextView_animDuration, DEFAULT_ANIM_DURATION);
        mAnimAlphaStart = typedArray.getFloat(R.styleable.ExpandableTextView_animAlphaStart, DEFAULT_ANIM_ALPHA_START);
        mExpandDrawable = typedArray.getDrawable(R.styleable.ExpandableTextView_expandDrawable);
        mCollapseDrawable = typedArray.getDrawable(R.styleable.ExpandableTextView_collapseDrawable);
        mExpandText = typedArray.getString(R.styleable.ExpandableTextView_expandText);
        mCollapseText = typedArray.getString(R.styleable.ExpandableTextView_collapseText);
        isAnchorAbove = typedArray.getBoolean(R.styleable.ExpandableTextView_isAnchorAbove, false);

        mTextSize = typedArray.getDimensionPixelSize(R.styleable.ExpandableTextView_textSize, (int)getResources().getDimension(R.dimen.text_size_14sp));
        if (mExpandDrawable == null) {
            mExpandDrawable = getDrawable(getContext(), R.drawable.ic_expand_down);
        }
        if (mCollapseDrawable == null) {
            mCollapseDrawable = getDrawable(getContext(), R.drawable.ic_expand_up);
        }

        typedArray.recycle();

        // default visibility is gone
        setVisibility(GONE);
    }

    private void findViews() {
        if(isAnchorAbove) {
            LayoutInflater.from(getContext()).inflate(R.layout.ctrl_expandable_hiding_textview, this, true);
        }else{
            LayoutInflater.from(getContext()).inflate(R.layout.ctrl_expandable_textview, this, true);
        }
        mTv = (TextView) findViewById(R.id.expandable_text);
        mTv.setEllipsize(TextUtils.TruncateAt.END);
        mTv.setTextSize(TypedValue.COMPLEX_UNIT_PX, mTextSize);
        mTv.setOnClickListener(this);
        mTvExpand = (TextView) findViewById(R.id.tvExpand);
        mRootAnchorView = findViewById(R.id.rlAnchor);
        mAnchorView = findViewById(R.id.llAnchor);
        mIvExpand = (ImageView)findViewById(R.id.ivUpDown);
        // Set font
//        CommonUtils.setFontForTextView(mTv,
//                FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
        CommonUtils.setFontForTextView(mTvExpand,
                                       FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
        // Render anchor view
        renderAnchorView();

        mAnchorView.setOnClickListener(this);

//        // Rule for the anchor and the displayed text
//        RelativeLayout.LayoutParams tvExpandParam = (RelativeLayout.LayoutParams)mTvExpand.getLayoutParams();
//        RelativeLayout.LayoutParams anchorViewParam = (RelativeLayout.LayoutParams)mRootAnchorView.getLayoutParams();
//        if(isAnchorAbove){
//            tvExpandParam.addRule(RelativeLayout.BELOW, R.id.rlAnchor);
//            tvExpandParam.topMargin = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, getResources().getDisplayMetrics());
//        }else{
//            anchorViewParam.addRule(RelativeLayout.BELOW, R.id.expandable_text);
//            anchorViewParam.topMargin = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, getResources().getDisplayMetrics());
//        }
    }
    public int getDifferentCollapseExpand(){
        return (mExpandedHeight - mCollapsedHeight);
    }
    public void renderAnchorView(){
        mTvExpand.setText(mCollapsed ? mCollapseText : mExpandText);
        mIvExpand.setImageResource(mCollapsed ? R.drawable.down : R.drawable.up);
    }
    public void inflateAfterInnerImageRendered(){
        /*if(!mAnimating) {
            mRelayout = true;
            ViewGroup.LayoutParams params = getLayoutParams();
            if (params instanceof LinearLayout.LayoutParams) {
                params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            } else if (params instanceof RelativeLayout.LayoutParams) {
                params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            } else if (params instanceof FrameLayout.LayoutParams) {
                params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            }
            setLayoutParams(params);

            requestLayout();
        }else{
            mInflateInnerImage = true;
        }*/
        mRelayout = true;
        setVisibility(VISIBLE);
        requestLayout();
    }
    private static boolean isPostHoneycomb() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;
    }

    private static boolean isPostLolipop() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private static void applyAlphaAnimation(View view, float alpha) {
        if (isPostHoneycomb()) {
            view.setAlpha(alpha);
        } else {
            AlphaAnimation alphaAnimation = new AlphaAnimation(alpha, alpha);
            // make it instant
            alphaAnimation.setDuration(0);
            alphaAnimation.setFillAfter(true);
            view.startAnimation(alphaAnimation);
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private static Drawable getDrawable(@NonNull Context context, @DrawableRes int resId) {
        Resources resources = context.getResources();
        if (isPostLolipop()) {
            return resources.getDrawable(resId, context.getTheme());
        } else {
            return resources.getDrawable(resId);
        }
    }

    private static int getRealTextViewHeight(@NonNull TextView textView) {
        int textHeight = textView.getLayout().getLineTop(textView.getLineCount());
        int padding = textView.getCompoundPaddingTop() + textView.getCompoundPaddingBottom();
        return textHeight + padding;
    }

    class ExpandCollapseAnimation extends Animation {
        private final View mTargetView;
        private final int mStartHeight;
        private final int mEndHeight;

        public ExpandCollapseAnimation(View view, int startHeight, int endHeight) {
            mTargetView = view;
            mStartHeight = startHeight;
            mEndHeight = endHeight;
            setDuration(mAnimationDuration);
        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            final int newHeight = (int)((mEndHeight - mStartHeight) * interpolatedTime + mStartHeight);
            mTv.getLayoutParams().height = newHeight - mMarginBetweenTxtAndBottom;
            mTv.setMaxHeight(newHeight - mMarginBetweenTxtAndBottom);
            if (Float.compare(mAnimAlphaStart, 1.0f) != 0) {
                applyAlphaAnimation(mTv, mAnimAlphaStart + interpolatedTime * (1.0f - mAnimAlphaStart));
            }
            mTargetView.getLayoutParams().height = newHeight;
            mTargetView.requestLayout();
        }

        @Override
        public void initialize( int width, int height, int parentWidth, int parentHeight ) {
            super.initialize(width, height, parentWidth, parentHeight);
        }

        @Override
        public boolean willChangeBounds( ) {
            return true;
        }


    }

    public interface OnExpandStateChangeListener {
        /**
         * Called when the expand/collapse animation has been finished
         *
         * @param textView - TextView being expanded/collapsed
         * @param isExpanded - true if the TextView has been expanded
         * @param differentHeight - different height between expanded and collapsed
         */
        void onExpandStateChanged(TextView textView,
                                  boolean isExpanded,
                                  int differentHeight);
        void onBeginExpandOrCollapse(TextView textView);
    }
}
