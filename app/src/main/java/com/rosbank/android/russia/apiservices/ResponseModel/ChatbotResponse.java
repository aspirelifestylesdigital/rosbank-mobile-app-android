package com.rosbank.android.russia.apiservices.ResponseModel;

import com.rosbank.android.russia.model.BotSay;
import com.google.gson.annotations.Expose;

/**
 * Created by nga.nguyent on 10/17/2016.
 */

public class ChatbotResponse{
    @Expose
    String status;
    @Expose
    String usersay;
    @Expose
    String convo_id;
    @Expose
    BotSay botsay;

    public Boolean isSuccess(){
        if("200".equalsIgnoreCase(status)){
            return true;
        }
        return false;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUsersay() {
        return usersay;
    }

    public void setUsersay(String usersay) {
        this.usersay = usersay;
    }

    public String getConvo_id() {
        return convo_id;
    }

    public void setConvo_id(String convo_id) {
        this.convo_id = convo_id;
    }

    public BotSay getBotsay() {
        return botsay;
    }

    public void setBotsay(BotSay botsay) {
        this.botsay = botsay;
    }
}
