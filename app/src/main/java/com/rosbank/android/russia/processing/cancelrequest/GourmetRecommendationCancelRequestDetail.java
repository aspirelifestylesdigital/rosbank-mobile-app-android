package com.rosbank.android.russia.processing.cancelrequest;

import android.text.TextUtils;
import android.view.View;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.apiservices.ResponseModel.RestaurantRecommendationBookingDetailResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.rosbank.android.russia.apiservices.b2c.B2CWSGetRequestDetail;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.B2CUpsertConciergeRequestRequest;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CGetRecentRequestResponse;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.application.AppContext;
import com.rosbank.android.russia.model.CountryObject;
import com.rosbank.android.russia.model.concierge.RestaurantBookingDetailData;
import com.rosbank.android.russia.utils.DateTimeUtil;
import com.rosbank.android.russia.utils.SharedPreferencesUtils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by ThuNguyen on 10/26/2016.
 */

public class GourmetRecommendationCancelRequestDetail extends BaseCancelRequestDetail {
    private RestaurantBookingDetailData restaurantRecommendationBookingDetailData;

    public GourmetRecommendationCancelRequestDetail() {
    }

    public GourmetRecommendationCancelRequestDetail(View view) {
        super(view);
    }

    @Override
    public void getRequestDetail() {
        showProgressDialog();
        B2CWSGetRequestDetail b2CWSGetRequestDetail = new B2CWSGetRequestDetail(this);
        b2CWSGetRequestDetail.setValue(myRequestObject.getEpcCaseId(), myRequestObject.getBookingItemID());
        b2CWSGetRequestDetail.run(null);
        /*WSGetRestaurantRecommendationBookingDetail wsGetRestaurantRecommendationBookingDetail = new WSGetRestaurantRecommendationBookingDetail();
        wsGetRestaurantRecommendationBookingDetail.setBookingId(myRequestObject.getBookingId());
        wsGetRestaurantRecommendationBookingDetail.run(this);
        showProgressDialog();*/
    }

    @Override
    protected void updateUI() {
        super.updateUI();
        long epochCreatedDate = restaurantRecommendationBookingDetailData.getCreateDateEpoc();
        tvRequestedDate.setText(DateTimeUtil.shareInstance().formatTimeStamp(epochCreatedDate, "dd MMMM yyyy"));
        tvRequestedTime.setText(DateTimeUtil.shareInstance().formatTimeStamp(epochCreatedDate, "hh:mm aa"));
        // Request type
        tvRequestType.setText(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_title_dinning_recommend_request));
        // Request name
        if (TextUtils.isEmpty(restaurantRecommendationBookingDetailData.getOccasion()) || restaurantRecommendationBookingDetailData.getOccasion().equalsIgnoreCase("null")) {
            tvRequestName.setText(restaurantRecommendationBookingDetailData.getCity());
        } else {
            tvRequestName.setText(restaurantRecommendationBookingDetailData.getOccasion() + ", " + restaurantRecommendationBookingDetailData.getCity());
        }
        // Request detail header
        tvRequestDetailHeader.setText(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_dining_recommend_request_details));

        if (restaurantRecommendationBookingDetailData != null) {
            // Case ID
            tvCaseId.setText(restaurantRecommendationBookingDetailData.getBookingItemID());

            // Reservation date
            long reservationDate = restaurantRecommendationBookingDetailData.getReservationDateEpoc();
            addRequestDetailItem(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_dining_recommend_reservation_date), DateTimeUtil.shareInstance().formatTimeStamp(reservationDate, "dd MMMM yyyy"));
        }
    }

    @Override
    public void onResponse(Call call, Response response) {
        hideProgressDialog();
        if (response != null && response.body() instanceof RestaurantRecommendationBookingDetailResponse) {
            // restaurantRecommendationBookingDetailData = ((RestaurantRecommendationBookingDetailResponse)response.body()).getData();
            updateUI();
        }
    }

    @Override
    public void onFailure(Call call, Throwable t) {
        hideProgressDialog();
    }

    @Override
    public void onB2CResponse(final B2CBaseResponse response) {
        hideProgressDialog();
        if (response instanceof B2CGetRecentRequestResponse) {
            restaurantRecommendationBookingDetailData = new RestaurantBookingDetailData((B2CGetRecentRequestResponse) response);
            updateUI();
        }
    }

    @Override
    public void onB2CResponseOnList(final List<B2CBaseResponse> responseList) {

    }

    @Override
    public void onB2CFailure(final String errorMessage,
                             final String errorCode) {
        hideProgressDialog();
    }

    @Override
    public B2CUpsertConciergeRequestRequest getB2CUpsertConciergeRequestRequest() {
        B2CUpsertConciergeRequestRequest upsertConciergeRequestRequest = new B2CUpsertConciergeRequestRequest();
        upsertConciergeRequestRequest.setFunctionality(AppConstant.CONCIERGE_FUNCTIONALITY_TYPE.DINING_RECOMMEND.getValue());
        upsertConciergeRequestRequest.setRequestType(AppConstant.CONCIERGE_REQUEST_TYPE.RECOMMEND_RESTAURANT.getValue());
        // Event date
        upsertConciergeRequestRequest.setEventDate(DateTimeUtil.shareInstance().formatTimeStamp(restaurantRecommendationBookingDetailData.getReservationDateEpoc(), AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS));
        upsertConciergeRequestRequest.setDelivery(DateTimeUtil.shareInstance().formatTimeStamp(restaurantRecommendationBookingDetailData.getReservationDateEpoc(), AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS));
        if (!TextUtils.isEmpty(restaurantRecommendationBookingDetailData.getOccasion())) {
            upsertConciergeRequestRequest.setSituation(restaurantRecommendationBookingDetailData.getOccasion());
        }else{
            upsertConciergeRequestRequest.setSituation("Situation");
        }
        if (!TextUtils.isEmpty(restaurantRecommendationBookingDetailData.getCity())) {
            upsertConciergeRequestRequest.setCity(restaurantRecommendationBookingDetailData.getCity());
        }
        if (!TextUtils.isEmpty(restaurantRecommendationBookingDetailData.getState())) {
            upsertConciergeRequestRequest.setState(restaurantRecommendationBookingDetailData.getState());
        }else {
            upsertConciergeRequestRequest.setState(restaurantRecommendationBookingDetailData.getCity());
        }
        if (!TextUtils.isEmpty(restaurantRecommendationBookingDetailData.getCountry())) {
            upsertConciergeRequestRequest.setCountry(CountryObject.getCountryObjectFromName(restaurantRecommendationBookingDetailData.getCountry()).getCountryCode());
        }

        upsertConciergeRequestRequest.setNumberOfAdults(String.valueOf(restaurantRecommendationBookingDetailData.getNumberOfAdults()));
        upsertConciergeRequestRequest.setNumberOfKids(String.valueOf(restaurantRecommendationBookingDetailData.getNumberOfKids()));
        upsertConciergeRequestRequest.setNumberOfChildren(String.valueOf(restaurantRecommendationBookingDetailData.getNumberOfChildren()));

        upsertConciergeRequestRequest.setEmail1((SharedPreferencesUtils.getPreferences(AppConstant.USER_EMAIL_PRE,
                "")));
        upsertConciergeRequestRequest.setEmailAddress1((SharedPreferencesUtils.getPreferences(AppConstant.USER_EMAIL_PRE,
                "")));

        upsertConciergeRequestRequest.setPhoneNumber(restaurantRecommendationBookingDetailData.getMobileNumber());

        upsertConciergeRequestRequest.setPrefResponse(restaurantRecommendationBookingDetailData.getPrefResponse());

        if (myRequestObject != null) {
            upsertConciergeRequestRequest.setTransactionID(myRequestObject.getBookingItemID());
        }
        // Request details
        upsertConciergeRequestRequest.setRequestDetails(restaurantRecommendationBookingDetailData.getRequestDetaiString());
        return upsertConciergeRequestRequest;
    }

    private String combineRequestDetails() {
        String requestDetails = "";
        if (!TextUtils.isEmpty(restaurantRecommendationBookingDetailData.getConciergeRequestDetail().getSpecialRequirement())) {
            requestDetails +=
                    AppConstant.B2C_REQUEST_DETAIL_SPECIAL_REQ + restaurantRecommendationBookingDetailData.getConciergeRequestDetail().getSpecialRequirement();
        }
        if (!TextUtils.isEmpty(restaurantRecommendationBookingDetailData.getReservationName())) {
            if (!TextUtils.isEmpty(requestDetails)) {
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_RESERVATION_NAME + restaurantRecommendationBookingDetailData.getReservationName();
        }
        if (!TextUtils.isEmpty(restaurantRecommendationBookingDetailData.getCuisine())) {
            if (!TextUtils.isEmpty(requestDetails)) {
                requestDetails += " | ";
            }
            requestDetails +=
                    AppConstant.B2C_REQUEST_DETAIL_TYPE_OF_CUISINE + restaurantRecommendationBookingDetailData.getCuisine();
        }
        if (!TextUtils.isEmpty(restaurantRecommendationBookingDetailData.getOccasion())) {
            if (!TextUtils.isEmpty(requestDetails)) {
                requestDetails += " | ";
            }
            requestDetails +=
                    AppConstant.B2C_REQUEST_DETAIL_AMBIANCE_OCCASION + restaurantRecommendationBookingDetailData.getOccasion();
        }
        if (!TextUtils.isEmpty(restaurantRecommendationBookingDetailData.getCountry())) {
            if (!TextUtils.isEmpty(requestDetails)) {
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_COUNTRY + restaurantRecommendationBookingDetailData.getCountry();
        }
        if (!TextUtils.isEmpty(restaurantRecommendationBookingDetailData.getMaxPrice())) {
            if (!TextUtils.isEmpty(requestDetails)) {
                requestDetails += " | ";
            }
            requestDetails +=
                    AppConstant.B2C_REQUEST_DETAIL_BUDGET_RANGE_CURRENCY + restaurantRecommendationBookingDetailData.getMaxPrice();
        }

        return requestDetails;
       /* String requestDetails = "";
        if(!TextUtils.isEmpty(restaurantRecommendationBookingDetailData.getConciergeRequestDetail().getSpecialRequirement())){
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_SPECIAL_REQ + restaurantRecommendationBookingDetailData.getConciergeRequestDetail().getSpecialRequirement();
        }
        if(!TextUtils.isEmpty(restaurantRecommendationBookingDetailData.getMaxPrice())){
            if(!TextUtils.isEmpty(requestDetails)){
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_MAX_PRICE + restaurantRecommendationBookingDetailData.getMaxPrice();
        }
        if(!TextUtils.isEmpty(requestDetails)){
            requestDetails += " | ";
        }
        requestDetails += AppConstant.B2C_REQUEST_DETAIL_CUISINE + restaurantRecommendationBookingDetailData.getCuisine();
        return requestDetails;*/
    }
}
