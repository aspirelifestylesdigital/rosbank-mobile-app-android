package com.rosbank.android.russia.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RelativeLayout;

import com.rosbank.android.russia.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;

/**
 * @author anh.trinh
 * @since v0.1
 */
public class MyRequestsHeaderView
        extends RelativeLayout {

    private static final int LAYOUT_RESOURCE_ID = R.layout.my_request_header_view;
    public static final String PRE_REQUEST_TYPE_ALL_UPCOMING = "REQUEST_TYPE_ALL_UPCOMING";
    public static final String PRE_REQUEST_TYPE_ALL_HISTORY = "REQUEST_TYPE_ALL_HISTORY";
    public static final String PRE_REQUEST_TYPE_ALL = "REQUEST_TYPE_ALL";
    public static final String PRE_REQUEST_TYPE_GOURMET = "REQUEST_TYPE_GOURMET";
    public static final String PRE_REQUEST_TYPE_HOTEL = "REQUEST_TYPE_HOTEL";
    public static final String PRE_REQUEST_TYPE_EVENT = "REQUEST_TYPE_EVENT";
    public static final String PRE_REQUEST_TYPE_CAR = "REQUEST_TYPE_CAR";
    public static final String PRE_REQUEST_TYPE_GOLF = "REQUEST_TYPE_GOLF";
    public static final String PRE_REQUEST_TYPE_SPA = "REQUEST_TYPE_SPA";
    public static final String PRE_REQUEST_TYPE_OTHER = "REQUEST_TYPE_OTHER";
    @BindView(R.id.rbt_all_request)

    RadioButton mRbtAllRequest;
    @BindView(R.id.rdb_dinning_request)
    RadioButton mRdbDinningRequest;
    @BindView(R.id.rdb_hotel_request)
    RadioButton mRdbHotelRequest;
    @BindView(R.id.rdb_event_request)
    RadioButton mRdbEventRequest;
    @BindView(R.id.rdb_car_request)
    RadioButton mRdbCarRequest;
    @BindView(R.id.rdb_golf_request)
    RadioButton mRdbGolfRequest;
    @BindView(R.id.rdb_spa_request)
    RadioButton mRdbSpaRequest;
    @BindView(R.id.rdb_other_request)
    RadioButton mRdbOtherRequest;


    public interface MyRequestHeaderViewListener {
        void onRadioCheckChange(String requestType);
    }

    MyRequestHeaderViewListener myRequestHeaderViewListener;

    public void setRequestHeaderListener(MyRequestHeaderViewListener listener) {
        this.myRequestHeaderViewListener = listener;
    }

    /**
     * @param context
     *         The context which view is running on.
     * @return New ItemListCollections object.
     * @since v0.1
     */
    public static MyRequestsHeaderView newInstance(final Context context) {
        if (context == null) {
            return null;
        }

        return new MyRequestsHeaderView(context);
    }

    /**
     * The constructor with current context.
     *
     * @param context
     *         The context which view is running on.
     * @since v0.1
     */
    public MyRequestsHeaderView(final Context context) {
        super(context);
        this.initialize();
    }

    /**
     * The constructor with current context.
     *
     * @param context
     *         The context which view is running on.
     * @param attrs
     *         The initialize attributes set.
     * @since v0.1
     */
    public MyRequestsHeaderView(final Context context,
                                final AttributeSet attrs) {
        super(context,
              attrs);
        this.initialize();
    }

    /**
     * The constructor with current context.
     *
     * @param context
     *         The context which view is running on.
     * @param attrs
     *         The initialize attributes set.
     * @param defStyleAttr
     *         The default style.
     * @since v0.1
     */
    public MyRequestsHeaderView(final Context context,
                                final AttributeSet attrs,
                                final int defStyleAttr) {
        super(context,
              attrs,
              defStyleAttr);

        this.initialize();
    }

    /**
     * @see View#setEnabled(boolean)
     * @since v0.1
     */
    @Override
    public void setEnabled(final boolean isEnabled) {
        this.setAlpha(isEnabled ?
                      1f :
                      0.5f);
        super.setEnabled(isEnabled);
    }

    /**
     * Reset injected resources created by ButterKnife.
     *
     * @since v0.1
     */
    public void resetInjectedResource() {
        ButterKnife.bind(this);
    }

    /**
     * Initialize UI sub views.
     *
     * @since v0.1
     */
    private void initialize() {

        final LayoutInflater layoutInflater = LayoutInflater.from(this.getContext());
        layoutInflater.inflate(LAYOUT_RESOURCE_ID,
                               this,
                               true);

        ButterKnife.bind(this,
                         this);

    }

    @OnCheckedChanged({R.id.rbt_all_request,
                       R.id.rdb_dinning_request,
                       R.id.rdb_hotel_request,
                       R.id.rdb_event_request,
                       R.id.rdb_car_request,
                       R.id.rdb_golf_request,R.id.rdb_spa_request,
                       R.id.rdb_other_request})
    public void onCheckedChange(CompoundButton buttonView,
                                boolean isChecked) {
        if (isChecked) {
            switch (buttonView.getId()) {
                case R.id.rbt_all_request:
                   /* mRdbCarRequest.setChecked(false);
                    mRdbDinningRequest.setChecked(false);
                    mRdbHotelRequest.setChecked(false);
                    mRdbGolfRequest.setChecked(false);
                    mRdbEventRequest.setChecked(false);
                    mRdbOtherRequest.setChecked(false);*/
                    if (myRequestHeaderViewListener != null) {
                        myRequestHeaderViewListener.onRadioCheckChange(PRE_REQUEST_TYPE_ALL);
                    }
                    break;
                case R.id.rdb_dinning_request:
                  //  mRbtAllRequest.setChecked(false);
                    if (myRequestHeaderViewListener != null) {
                        myRequestHeaderViewListener.onRadioCheckChange(PRE_REQUEST_TYPE_GOURMET);
                    }
                    break;
                case R.id.rdb_hotel_request:
                   // mRbtAllRequest.setChecked(false);
                    if (myRequestHeaderViewListener != null) {
                        myRequestHeaderViewListener.onRadioCheckChange(PRE_REQUEST_TYPE_HOTEL);
                    }
                    break;
                case R.id.rdb_event_request:
                  //  mRbtAllRequest.setChecked(false);
                    if (myRequestHeaderViewListener != null) {
                        myRequestHeaderViewListener.onRadioCheckChange(PRE_REQUEST_TYPE_EVENT);
                    }
                    break;
                case R.id.rdb_car_request:
                   // mRbtAllRequest.setChecked(false);
                    if (myRequestHeaderViewListener != null) {
                        myRequestHeaderViewListener.onRadioCheckChange(PRE_REQUEST_TYPE_CAR);
                    }
                    break;
                case R.id.rdb_golf_request:
                   // mRbtAllRequest.setChecked(false);
                    if (myRequestHeaderViewListener != null) {
                        myRequestHeaderViewListener.onRadioCheckChange(PRE_REQUEST_TYPE_GOLF);
                    }
                    break;
                case R.id.rdb_spa_request:
                    // mRbtAllRequest.setChecked(false);
                    if (myRequestHeaderViewListener != null) {
                        myRequestHeaderViewListener.onRadioCheckChange(PRE_REQUEST_TYPE_SPA);
                    }
                    break;
                case R.id.rdb_other_request:
                   // mRbtAllRequest.setChecked(false);
                    if (myRequestHeaderViewListener != null) {
                        myRequestHeaderViewListener.onRadioCheckChange(PRE_REQUEST_TYPE_OTHER);
                    }
                    break;
            }
        }

    }


}
