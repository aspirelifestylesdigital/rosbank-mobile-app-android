package com.rosbank.android.russia.fragment;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.activitys.HomeActivity;
import com.rosbank.android.russia.apiservices.ResponseModel.BookOtherResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.OtherBookingDetailResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CICallback;
import com.rosbank.android.russia.apiservices.b2c.B2CWSGetPreference;
import com.rosbank.android.russia.apiservices.b2c.B2CWSGetRequestDetail;
import com.rosbank.android.russia.apiservices.b2c.B2CWSUpsertConciergeRequest;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.B2CUpsertConciergeRequestRequest;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CGetRecentRequestResponse;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CUpsertConciergeRequestResponse;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.application.AppContext;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.dcr.model.DCRCommonObject;
import com.rosbank.android.russia.dcr.model.DCRPrivilegeObject;
import com.rosbank.android.russia.model.concierge.MyRequestObject;
import com.rosbank.android.russia.model.concierge.OtherBookingDetailData;
import com.rosbank.android.russia.model.preference.OtherPreferenceDetailData;
import com.rosbank.android.russia.model.preference.PreferenceData;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.EntranceLock;
import com.rosbank.android.russia.utils.FontUtils;
import com.rosbank.android.russia.utils.NetworkUtil;
import com.rosbank.android.russia.utils.PhotoPickerUtils;
import com.rosbank.android.russia.utils.StringUtil;
import com.rosbank.android.russia.widgets.AttachePhotoView;
import com.rosbank.android.russia.widgets.ContactView;
import com.rosbank.android.russia.widgets.CustomErrorView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTouch;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by anh.trinh on 9/13/2016.
 */
public class BookingOtherFragment
        extends BaseFragment
        implements SelectionFragment.SelectionCallback,
                   ContactView.ContactViewListener,
                   Callback, B2CICallback {

    @BindView(R.id.tv_other_request_title)
    TextView mTvOtherRequestTitle;
    @BindView(R.id.edt_other_request)
    EditText mEdtOtherRequest;
    @BindView(R.id.other_request_error)
    CustomErrorView mOtherRequestError;
    @BindView(R.id.layout_other_request)
    RelativeLayout mLayoutOtherRequest;
    @BindView(R.id.contact_view)
    ContactView mContactView;
    @BindView(R.id.photo_attached)
    AttachePhotoView mPhotoAttached;
    @BindView(R.id.btn_submit)
    Button mBtnSubmit;
    @BindView(R.id.btn_cancel)
    Button mBtnCancel;
    @BindView(R.id.layout_bottom_button)
    LinearLayout layoutBottomButton;
    @BindView(R.id.container)
    LinearLayout mLayoutContainer;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    @BindView(R.id.framePriDetailImgTitle)
    LinearLayout framePriDetailImgTitle;
    @BindView(R.id.imgLogo)
    ImageView mImageViewLogo;
    @BindView(R.id.tvPrivilegeName)
    TextView tvPrivilegeName;
    @BindView(R.id.tvImageCopyRight)
    TextView tvImageCopyRight;
    @BindView(R.id.tvPrivilegeAddress)
    TextView tvPrivilegeAddress;
    @BindView(R.id.tvFillTheMandatory)
    TextView tvFillTheMandatory;

    MyRequestObject myRequestObject;
    DCRPrivilegeObject mDcrPrivilegeObject;
    private B2CUpsertConciergeRequestRequest upsertConciergeRequestRequest;
    private String uploadedPhotoPath = "";
    private EntranceLock entranceLock = new EntranceLock();
    private boolean isSubmitClicked = false;
    private String titlePage;

    @Override
    protected int layoutId() {
        return R.layout.fragment_concierge_book_other;
    }

    @Override
    protected void initView() {
        titlePage = getString(R.string.text_concierge_booking_other_title);
        CommonUtils.setFontForViewRecursive(mLayoutContainer,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
        CommonUtils.setFontForViewRecursive(mBtnSubmit,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        CommonUtils.setFontForViewRecursive(mBtnCancel,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        //CommonUtils.makeFirstCharacterUpperCase(mEdtOtherRequest);
        mPhotoAttached.setParentFragment(this);
        mPhotoAttached.setAttachePhotoCallback(new AttachePhotoView.IAttachePhotoCallback() {
            @Override
            public void onPhotoChanged() {
                // Reset the photo path once the user change photo
                uploadedPhotoPath = "";
            }
        });
        tvFillTheMandatory.setVisibility(View.GONE);
        mContactView.setContactViewListener(this);
        if (getArguments() != null) {
            myRequestObject = (MyRequestObject)getArguments().getSerializable(AppConstant.PRE_REQUEST_OBJECT_DATA);
            mDcrPrivilegeObject =
                    (DCRPrivilegeObject) getArguments().getSerializable(AppConstant.PRE_SPA_DATA);
            setPrivilegeData(mDcrPrivilegeObject);
        }
        if (myRequestObject != null) {
            getOtherDetails(myRequestObject.getBookingItemID());
        }else{ // Load default preferences
            showDialogProgress();
            B2CWSGetPreference b2CWSGetPreference = new B2CWSGetPreference(getPreferenceCallback);
            b2CWSGetPreference.run(null);
        }
    }
    private void setPrivilegeData(DCRPrivilegeObject dcrPrivilegeObject) {
        boolean    isFromPrivilege = false;
        if (dcrPrivilegeObject != null) {
            isFromPrivilege = true;
            tvFillTheMandatory.setVisibility(View.VISIBLE);
            if(CommonUtils.isStringValid(dcrPrivilegeObject
                                                 .getLogo_url())) {
                if(CommonUtils.isUrlLink(dcrPrivilegeObject.getLogo_url())) {
                    Glide.with(getContext())
                         .load(dcrPrivilegeObject
                                       .getLogo_url())
                         .thumbnail(0.5f)
                         .centerCrop()
                         .crossFade()
                         .diskCacheStrategy(DiskCacheStrategy.ALL)
                         .into(mImageViewLogo);
                }else{
                    Drawable drawable = getContext().getResources().getDrawable(getContext().getResources()
                                                                                            .getIdentifier(dcrPrivilegeObject.getLogo_url(), "drawable", getContext().getPackageName()));
                    mImageViewLogo.setImageDrawable(drawable);
                }
                mImageViewLogo.setVisibility(View.VISIBLE);
            } else {
                mImageViewLogo.setVisibility(View.GONE);
            }
            if (CommonUtils.isStringValid(dcrPrivilegeObject.getName())) {
                tvPrivilegeName.setText(dcrPrivilegeObject.getName());
                titlePage =  dcrPrivilegeObject.getName();
                tvImageCopyRight.setTypeface(tvImageCopyRight.getTypeface(),
                                             Typeface.ITALIC);
                tvImageCopyRight.setText(getString(R.string.privilege_detail_text_copy_right_by_format,
                                                   dcrPrivilegeObject.getName()));

            }

        }
        setPrivilegeLayout(isFromPrivilege);
    }

    private void setPrivilegeLayout(boolean isPrivilege) {
        if (isPrivilege) {
            framePriDetailImgTitle.setVisibility(View.VISIBLE);

        } else {
            framePriDetailImgTitle.setVisibility(View.GONE);
        }
    }
    @Override
    protected void bindData() {

    }

    private void getOtherDetails(String bookingId){
        showDialogProgress();
        B2CWSGetRequestDetail b2CWSGetRequestDetail = new B2CWSGetRequestDetail(this);
        b2CWSGetRequestDetail.setValue(myRequestObject.getEpcCaseId(), bookingId);
        b2CWSGetRequestDetail.run(null);
    }

    @Override
    public boolean onBack() {
        return false;
    }


    @OnClick({R.id.btn_submit,
              R.id.btn_cancel})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_submit:
                AppContext.getSharedInstance().track(AppConstant.ANALYTIC_EVENT_CATEGORY_CONCIERGE_REQUEST, AppConstant.ANALYTIC_EVENT_ACTION_SEND_CONCIERGE_REQUEST, AppConstant.ANALYTIC_BOOK_ITEM.OTHERS.getValue());
                if (validationData() && !entranceLock.isClickContinuous()) {
                    if(NetworkUtil.getNetworkStatus(getContext())) {
                        isSubmitClicked = true;
                    processBooking();
                    }else{
                        showInternetProblemDialog();
                    }
                }
                break;
            case R.id.btn_cancel:
                if(!entranceLock.isClickContinuous()) {
                    onBackPress();
                }
                break;
        }
    }

    @OnTouch({R.id.edt_other_request})
    boolean onInputTouch(final View v,
                         final MotionEvent event) {

        switch (v.getId()) {
            case R.id.edt_other_request:
                mOtherRequestError.setVisibility(View.GONE);
                break;
        }
        return false;
    }

    @Override
    public void onSelectCountryClick() {
        Bundle bundle1 = new Bundle();
        bundle1.putString(SelectionFragment.PRE_SELECTION_TYPE,
                          SelectionFragment.PRE_SELECTION_COUNTRY_CODE);
        bundle1.putString(SelectionFragment.PRE_SELECTION_DATA,
                          mContactView.getSelectedCountryCode());
        SelectionFragment fragment1 = new SelectionFragment();
        fragment1.setSelectionCallBack(this);
        fragment1.setArguments(bundle1);
        pushFragment(fragment1,
                     true,
                     true);
    }

    @Override
    public void onFinishSelection(final Bundle bundle) {

        String typeSelection = bundle.getString(SelectionFragment.PRE_SELECTION_TYPE,
                                                "");

        if (typeSelection.equals(SelectionFragment.PRE_SELECTION_COUNTRY_CODE)) {
            mContactView.setCountryNo(bundle.getString(SelectionFragment.PRE_SELECTION_DATA, ""));
        }
    }

    private boolean validationData() {
        boolean result = true;
        if (!CommonUtils.isStringValid(mEdtOtherRequest.getText()
                                                       .toString()
                                                       .trim())) {
            mOtherRequestError.fillData(getString(R.string.text_sign_up_error_required_field));
            mOtherRequestError.setVisibility(View.VISIBLE);
            result = false;
        }
        if (!mContactView.validationContact()) {
            result = false;
        }

        return result;
    }

    private void processBooking() {
        upsertConciergeRequestRequest = new B2CUpsertConciergeRequestRequest();
        upsertConciergeRequestRequest.setFunctionality(AppConstant.CONCIERGE_FUNCTIONALITY_TYPE.OTHER.getValue());
        upsertConciergeRequestRequest.setRequestType(AppConstant.CONCIERGE_REQUEST_TYPE.OTHER_REQUEST.getValue());
        upsertConciergeRequestRequest.updateDataFromContactView(mContactView);
        upsertConciergeRequestRequest.setSituation("other");

        // Image path
        if(!TextUtils.isEmpty(uploadedPhotoPath)){
            upsertConciergeRequestRequest.setAttachmentPath(uploadedPhotoPath);
        }else {
            if (mPhotoAttached.getPhotos() != null && mPhotoAttached.getPhotos().size() > 0) {
                String pathImage = mPhotoAttached.getPhotos()
                        .get(0);
                upsertConciergeRequestRequest.setAttachmentPath(pathImage);
            }
        }
        if(myRequestObject != null){
            upsertConciergeRequestRequest.setTransactionID(myRequestObject.getBookingItemID());
        }
        // Request details
        upsertConciergeRequestRequest.setRequestDetails(combineRequestDetails());

        showDialogProgress();
        B2CWSUpsertConciergeRequest b2CWSUpsertConciergeRequest = new B2CWSUpsertConciergeRequest(myRequestObject == null ? AppConstant.CONCIERGE_EDIT_TYPE.ADD : AppConstant.CONCIERGE_EDIT_TYPE.AMEND, this);
        b2CWSUpsertConciergeRequest.setRequest(upsertConciergeRequestRequest);
        b2CWSUpsertConciergeRequest.run(null);

    }
    private String combineRequestDetails(){
        String requestDetails = "";
        if(!TextUtils.isEmpty(mEdtOtherRequest.getText().toString().trim())){
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_SPECIAL_REQUIREMENTS + StringUtil.removeAllSpecialCharacterAndBreakLine(mEdtOtherRequest.getText().toString().trim());
        }
        if(!TextUtils.isEmpty(mContactView.getReservationName())){
            if(!TextUtils.isEmpty(requestDetails)){
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_RESERVATION_NAME + StringUtil.removeAllSpecialCharacterAndBreakLine(mContactView.getReservationName());
        }
        if(!TextUtils.isEmpty(tvPrivilegeName.getText().toString().trim())){
            if(!TextUtils.isEmpty(requestDetails)){
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_SPA_NAME + StringUtil.removeAllSpecialCharacterAndBreakLine(tvPrivilegeName.getText().toString().trim());
        }
        // Add Privilege Data
        if (mDcrPrivilegeObject != null) {
            if (!TextUtils.isEmpty(mDcrPrivilegeObject.getAddress())) {
                if (!TextUtils.isEmpty(requestDetails)) {
                    requestDetails += " | ";
                }
                requestDetails +=
                        AppConstant.B2C_REQUEST_DETAIL_FULL_ADDRESS + mDcrPrivilegeObject.getAddress();
            }
            if (!TextUtils.isEmpty(mDcrPrivilegeObject.getId())) {
                if (!TextUtils.isEmpty(requestDetails)) {
                    requestDetails += " | ";
                }
                requestDetails +=
                        AppConstant.B2C_REQUEST_DETAIL_PRIVILEGE_ID + mDcrPrivilegeObject.getId();
            }
            if (mDcrPrivilegeObject.getAddress_country() != null && CommonUtils.isStringValid(mDcrPrivilegeObject.getAddress_country()
                                                                                                                 .getName())) {
                if (!TextUtils.isEmpty(requestDetails)) {
                    requestDetails += " | ";
                }
                requestDetails +=
                        AppConstant.B2C_REQUEST_DETAIL_COUNTRY + mDcrPrivilegeObject.getAddress_country()
                                                                                    .getName();
            } else {
                if (!TextUtils.isEmpty(requestDetails)) {
                    requestDetails += " | ";
                }
                requestDetails +=
                        AppConstant.B2C_REQUEST_DETAIL_COUNTRY + AppConstant.B2C_REQUEST_DETAIL_NOT_AVAILABLE;
            }
            if (mDcrPrivilegeObject.getAddress_city() != null && CommonUtils.isStringValid(mDcrPrivilegeObject.getAddress_city()
                                                                                                              .getName())) {
                if (!TextUtils.isEmpty(requestDetails)) {
                    requestDetails += " | ";
                }
                requestDetails +=
                        AppConstant.B2C_REQUEST_DETAIL_CITY + mDcrPrivilegeObject.getAddress_city()
                                                                                 .getName();
            } else {
                if (!TextUtils.isEmpty(requestDetails)) {
                    requestDetails += " | ";
                }
                requestDetails +=
                        AppConstant.B2C_REQUEST_DETAIL_CITY + AppConstant.B2C_REQUEST_DETAIL_NOT_AVAILABLE;
            }
            if (mDcrPrivilegeObject.getAddress_state() != null && CommonUtils.isStringValid(mDcrPrivilegeObject.getAddress_state()
                                                                                                               .getName())) {
                if (!TextUtils.isEmpty(requestDetails)) {
                    requestDetails += " | ";
                }
                requestDetails +=
                        AppConstant.B2C_REQUEST_DETAIL_STATE + mDcrPrivilegeObject.getAddress_state()
                                                                                  .getName();
            } else {
                if (!TextUtils.isEmpty(requestDetails)) {
                    requestDetails += " | ";
                }
                requestDetails +=
                        AppConstant.B2C_REQUEST_DETAIL_STATE + AppConstant.B2C_REQUEST_DETAIL_NOT_AVAILABLE;
            }
        }
        return requestDetails;
    }
    @Override
    public void onB2CResponse(B2CBaseResponse response) {
        hideDialogProgress();
        if(getActivity()==null) {
            return;
        }
        if(response instanceof B2CUpsertConciergeRequestResponse){
            if(response != null && response.isSuccess()){
                onBackPress();
                ThankYouFragment thankYouFragment = new ThankYouFragment();
                if (myRequestObject != null) {
//                    Bundle bundle = new Bundle();
//                    bundle.putString(ThankYouFragment.LINE_MAIN,
//                            getString(R.string.text_message_thank_you_amend));
//                    thankYouFragment.setArguments(bundle);
                    EventBus.getDefault().post(AppConstant.CONCIERGE_EDIT_TYPE.AMEND);
                } else {
                    // Track product
                    //AppContext.getSharedInstance().track(AppConstant.TRACK_EVENT_PRODUCT_CATEGORY.OTHER.getValue(), AppConstant.TRACK_EVENT_PRODUCT_NAME.GENERIC_BOOK.getValue());
                    thankYouFragment.setTrackedProductCategory(AppConstant.TRACK_EVENT_PRODUCT_CATEGORY.OTHER.getValue());
                    thankYouFragment.setTrackedProductName(AppConstant.TRACK_EVENT_PRODUCT_NAME.GENERIC_BOOK.getValue());
                    EventBus.getDefault().post(AppConstant.CONCIERGE_EDIT_TYPE.ADD);
                }
                pushFragment(thankYouFragment,
                        true,
                        true);
            }else {
                showDialogMessage(getString(R.string.text_title_dialog_error), getString(R.string.text_concierge_request_error));

            }
        }else if(response instanceof B2CGetRecentRequestResponse){
            showBookingDetails(new OtherBookingDetailData((B2CGetRecentRequestResponse)response));
        }
    }

    @Override
    public void onB2CResponseOnList(List<B2CBaseResponse> responseList) {
        if(getActivity()==null) {
            return;
        }
    }

    @Override
    public void onB2CFailure(String errorMessage, String errorCode) {
        hideDialogProgress();
        if(getActivity()==null) {
            return;
        }
        if(isSubmitClicked) {
            isSubmitClicked = false;
            showDialogMessage(getString(R.string.text_title_dialog_error), getString(R.string.text_concierge_request_error));
        }
        // Store the uploaded successful photo path
        // So that the upsert failed in the first time, user tried to process for the second time
        // then that is no need to upload again
        if(upsertConciergeRequestRequest != null){
            uploadedPhotoPath = upsertConciergeRequestRequest.getAttachmentPath();
        }
    }
    @Override
    public void onResponse(final Call call,
                           final Response response) {
        if (response.body() instanceof BookOtherResponse) {
            hideDialogProgress();
            if(getActivity()==null) {
                return;
            }
            BookOtherResponse bookOtherResponse = ((BookOtherResponse) response.body());
            int code = bookOtherResponse.getStatus();
            if (code == 200) {
                onBackPress();
                ThankYouFragment thankYouFragment = new ThankYouFragment();

                pushFragment(thankYouFragment,
                             true,
                             true);

            } else {
//                showToast(bookOtherResponse.getMessage());
            }
        } else if(response.body() instanceof OtherBookingDetailResponse){
            hideDialogProgress();
            if(getActivity()==null) {
                return;
            }
            showBookingDetails(((OtherBookingDetailResponse) response.body()).getData());
        }


    }

    @Override
    public void onFailure(final Call call,
                          final Throwable t) {
        hideDialogProgress();
        if(getActivity()==null) {
            return;
        }
//        showToast(getString(R.string.text_server_error_message));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode,
                                         permissions,
                                         grantResults);

        switch (requestCode) {
            case AppConstant.PERMISSION_REQUEST_READ_EXTERNAL:
                if (grantResults.length > 0
                            && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mPhotoAttached.openGallery();
                }
                break;
            case AppConstant.PERMISSION_REQUEST_OPEN_CAMERA:
                if (grantResults.length > 0
                            && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mPhotoAttached.openCamera();
                }
                break;
            case AppConstant.PERMISSION_REQUEST_WRITE_EXTERNAL:
                if (grantResults.length > 0
                            && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mPhotoAttached.openCamera();
                }
                break;
        }

    }

    @Override
    public void onActivityResult(int requestCode,
                                 int resultCode,
                                 Intent data) {
        super.onActivityResult(requestCode,
                               resultCode,
                               data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == PhotoPickerUtils.PICK_IMAGE_REQUEST_CODE || requestCode == PhotoPickerUtils.TAKE_PICTURE_REQUEST_CODE) {
                mPhotoAttached.onActivityResult(requestCode,
                                                data);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showLogoApp(false);
            ((HomeActivity) getActivity()).setTitle(titlePage);
        }
    }

    private void showBookingDetails(OtherBookingDetailData data){
        mEdtOtherRequest.setText(data.getWhatCanWeDo());
        if(CommonUtils.isStringValid(data.getPrivilegeId())){
            mDcrPrivilegeObject = new DCRPrivilegeObject();
            mDcrPrivilegeObject.setId(data.getPrivilegeId());
            if(CommonUtils.isStringValid(data.getSpaName())){
                mDcrPrivilegeObject.setName(data.getSpaName());
            }
            if(CommonUtils.isStringValid(data.getCountry())){
                mDcrPrivilegeObject.setAddress_country(new DCRCommonObject(data.getCountry()));
            }
            if(CommonUtils.isStringValid(data.getCity())){
                mDcrPrivilegeObject.setAddress_city(new DCRCommonObject(data.getCity()));
            }
            if(CommonUtils.isStringValid(data.getState())){
                mDcrPrivilegeObject.setAddress_state(new DCRCommonObject(data.getState()));
            }
            if(CommonUtils.isStringValid(data.getFullAddress())){
                mDcrPrivilegeObject.setFullAddress(data.getFullAddress());
                tvPrivilegeAddress.setText(data.getFullAddress());
                tvPrivilegeAddress.setVisibility(View.VISIBLE);
            }else{
                tvPrivilegeAddress.setVisibility(View.GONE);
            }
            setPrivilegeData(mDcrPrivilegeObject);

        }

        // Contact view
        mContactView.updateContactView(data);
    }
    private void updateUIFromDefaultPreference(OtherPreferenceDetailData otherPreferenceDetailData){
        if(otherPreferenceDetailData != null){
            mEdtOtherRequest.setText(otherPreferenceDetailData.getComment());
        }
    }
    private B2CICallback getPreferenceCallback = new B2CICallback() {
        @Override
        public void onB2CResponse(B2CBaseResponse response) {

            hideDialogProgress();
            if(getActivity()==null) {
                return;
            }
        }

        @Override
        public void onB2CResponseOnList(List<B2CBaseResponse> responseList) {
            if(getActivity() != null) {
            hideDialogProgress();
                if(getActivity()==null) {
                    return;
                }
            OtherPreferenceDetailData otherPreferenceDetailData = (OtherPreferenceDetailData) PreferenceData.findFromList(responseList, AppConstant.PREFERENCE_TYPE.OTHER);
            updateUIFromDefaultPreference(otherPreferenceDetailData);
        }
        }

        @Override
        public void onB2CFailure(String errorMessage, String errorCode) {

            hideDialogProgress();
            if(getActivity()==null) {
                return;
            }
        }
    };
}
