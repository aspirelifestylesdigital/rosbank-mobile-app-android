package com.rosbank.android.russia.apiservices.ResponseModel;

import com.rosbank.android.russia.model.concierge.MyRequestObject;
import com.google.gson.annotations.Expose;

import java.util.ArrayList;


public class MyRequestResponseData {
    @Expose
    public Integer TotalRecords;
    @Expose
    public Integer CurrentPage;
    @Expose
    public Integer NbOfRecordsReturn;

    public ArrayList<MyRequestObject> getListData() {
        return ListData;
    }

    public void setListData(final ArrayList<MyRequestObject> listData) {
        ListData = listData;
    }

    public Integer getTotalRecords() {
        return TotalRecords;
    }

    public void setTotalRecords(final Integer totalRecords) {
        TotalRecords = totalRecords;
    }

    public Integer getCurrentPage() {
        return CurrentPage;
    }

    public void setCurrentPage(final Integer currentPage) {
        CurrentPage = currentPage;
    }

    public Integer getNbOfRecordsReturn() {
        return NbOfRecordsReturn;
    }

    public void setNbOfRecordsReturn(final Integer nbOfRecordsReturn) {
        NbOfRecordsReturn = nbOfRecordsReturn;
    }

    @Expose
    public ArrayList<MyRequestObject> ListData;


}
