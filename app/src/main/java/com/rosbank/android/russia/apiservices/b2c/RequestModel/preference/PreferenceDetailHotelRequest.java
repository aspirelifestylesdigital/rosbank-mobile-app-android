package com.rosbank.android.russia.apiservices.b2c.RequestModel.preference;

import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.utils.StringUtil;
import com.google.gson.annotations.Expose;

import java.util.List;

/**
 * Created by Thu Nguyen on 12/12/2016.
 */

public class PreferenceDetailHotelRequest extends PreferenceDetailAddRequest{
    @Expose
    private String Preferredstarrating;
    @Expose
    private String RoomPreference;
    @Expose
    private String BedPreference;
    @Expose
    private String SmokingPreference;
    @Expose
    private String NameofLoyaltyProgram;
    @Expose
    private String MembershipNo;
    @Expose
    private String AdditionalPreference;

    public PreferenceDetailHotelRequest(String Delete) {
        super(Delete, AppConstant.PREFERENCE_TYPE.HOTEL.getValue());
    }

    @Override
    public void fillData(Object... values) {
        List<String> valueList = StringUtil.removeAllSpecialCharactersAndFillInList(values);
        if(valueList != null){
            if(valueList.size() > 0){
                Preferredstarrating = valueList.get(0);
            }
            if(valueList.size() > 1){
                RoomPreference = valueList.get(1);
            }
            if(valueList.size() > 2){
                BedPreference = valueList.get(2);
            }
            if(valueList.size() > 3){
                SmokingPreference = valueList.get(3);
            }
            if(valueList.size() > 4){
                NameofLoyaltyProgram = valueList.get(4);
            }
            if(valueList.size() > 5){
                MembershipNo = valueList.get(5);
            }
            if(valueList.size() > 6){
                AdditionalPreference = valueList.get(6);
            }
        }
    }
}
