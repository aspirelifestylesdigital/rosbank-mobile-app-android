package com.rosbank.android.russia.application.coreactivitys;

import com.rosbank.android.russia.R;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public abstract class AbstractActivity
        extends AppCompatActivity{

    protected FrameLayout parentContainer;
    protected Dialog dialogProgress;
    protected AlertDialog dialogMessage;

    /**
     * layout_id in res or if not use layout return 0
     * */
    protected abstract int activityLayoutId();
    /**
     * return id of layout parentContainer, layout will contain fragment.
     * */
    protected abstract int layoutContainerId();
    /**
     * initView as: textsize, textcolor, bindData,... after load view in onCreate.
     * */
    protected abstract void initView();

    public abstract void openNavMenu();

    public abstract void closeNavMenu();

    public abstract void setTitle(String title);

    public abstract void showFloatingActionButton();

    public abstract void hideFloatingActionButton();

    public void pushFragment(BaseFragment fragment, boolean addBackStack, boolean animation) {
        pushFragment(fragment, false, addBackStack, animation);
    }
    public void pushFragment(BaseFragment fragment,
                             Boolean clearStack,
                             Boolean addToStack,
                             boolean animation) {
        if (clearStack) {
            clearAllFragments();
        }

        if (fragment != null) {
            replaceFragment(fragment, addToStack, animation);
        }

    }

    private FragmentManager fragmentManager(){
        return getSupportFragmentManager();
    }


    protected void replaceFragment(BaseFragment fragment, Boolean addToBackStack, boolean animation) {

        int idContainer = layoutContainerId();
        if(idContainer<=0)
            return;

        FragmentManager fm = fragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        //ft.setCustomAnimations(R.anim.slide_from_left, R.anim.slide_to_left);
        if (animation) {
            ft.setCustomAnimations(R.anim.slide_in_right,
                    R.anim.slide_out_left,
                    R.anim.slide_in_left,
                    R.anim.slide_out_right);

            /*ft.setCustomAnimations(R.animator.slide_in_right,
                    R.animator.slide_out_left,
                    R.animator.slide_in_left,
                    R.animator.slide_out_right);*/
        } else {
            /*ft.setCustomAnimations(R.anim.fade_in,
                    R.anim.fade_out);*/
            //ft.setCustomAnimations(R.animator.fade_in, R.animator.fade_out);
        }

        ft.replace(idContainer, fragment, fragment.getClass().getName());

        if (addToBackStack) {
            ft.addToBackStack(fragment.getClass().getName());
        }

        ft.commit();
    }

    public Fragment getCurrentFragment() {
        int idContainer = layoutContainerId();
        if(idContainer<=0)
            return null;

        FragmentManager fragmentManager = fragmentManager();
        Fragment fragment = fragmentManager.findFragmentById(idContainer);
        return fragment;
    }

    public void clearAllFragments() {
        FragmentManager fm = fragmentManager();


        if (fm.getBackStackEntryCount() > 0) {
            fm.popBackStackImmediate(null,
                                     FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    public void closeFragment(Fragment fragment) {
        fragmentManager().beginTransaction().remove(fragment).commitAllowingStateLoss();
    }

    public void closeFragment(Fragment fragment, int inAnimator, int outAnimator) {
        FragmentTransaction transaction = fragmentManager().beginTransaction();
        transaction.setCustomAnimations(inAnimator, outAnimator);
        transaction.remove(fragment).commitAllowingStateLoss();
    }

    public Boolean isFragmentOpened(String tag) {
        FragmentManager fm = fragmentManager();
        Fragment frag = fm.findFragmentByTag(tag);
        if (frag != null && frag.isAdded()) {
            return true;
        }
        return false;
    }

    public Fragment getCurrentFragmentByTag(String tag) {
        FragmentManager fm = fragmentManager();
        Fragment frag = fm.findFragmentByTag(tag);
        return frag;
    }

    public void showToast(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    private void getKeyhash(){
        /*
        * get SHA for facebook test.
        * */
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    getPackageName(),
                    PackageManager.GET_SIGNATURES);

            for (Signature signature : info.signatures) {

                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());

                System.out.println("KeyHash:" + Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }

        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }
    public void showDialogProgress() {

        if (isDialogProgressShowing()) {
            return;
        }

        dialogProgress = new Dialog(this);
        dialogProgress.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogProgress.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialogProgress.setContentView(R.layout.dialog_loading);
        dialogProgress.getWindow().getAttributes().width = WindowManager.LayoutParams.WRAP_CONTENT;
        ProgressBar progressBar = (ProgressBar) dialogProgress.findViewById(R.id.progressBar);
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(android.R.color.transparent), android.graphics.PorterDuff.Mode.SRC_ATOP);
        /*progressBar.getProgressDrawable().setColorFilter(
                getResources().getColor(R.color.app_color_light_bg), android.graphics.PorterDuff.Mode.SRC_IN);*/
        dialogProgress.setCanceledOnTouchOutside(false);

        dialogProgress.show();


    }

    public void hideDialogProgress() {
        if (dialogProgress == null)
            return;

        dialogProgress.dismiss();
        dialogProgress = null;
    }

    public boolean isDialogProgressShowing() {
        if (dialogProgress != null && dialogProgress.isShowing()) {
            return true;
        }
        return false;
    }

    public void showDialogMessage(String title, String message){
        if (isDialogMessageShowing()) {
            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                dialogMessage = null;
            }
        });
        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                dialogMessage = null;
            }
        });
        dialogMessage = builder.show();
    }
    public void showDialogMessage(String title, String message, DialogInterface.OnClickListener okClickListener, DialogInterface.OnDismissListener onDismissListener){
        if (isDialogMessageShowing()) {
            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(getString(android.R.string.ok),okClickListener);
        builder.setCancelable(false);
        builder.setOnDismissListener(onDismissListener);
        dialogMessage = builder.show();
    }
    public void hideDialogMessage(){
        if(isDialogMessageShowing()) {
            dialogMessage.dismiss();
            dialogMessage = null;
        }
    }

    public boolean isDialogMessageShowing(){
        if(dialogMessage!=null && dialogMessage.isShowing())
            return true;
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        //
        Fragment fragment = getCurrentFragment();
        fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
