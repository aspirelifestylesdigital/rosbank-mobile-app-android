package com.rosbank.android.russia.dcr.api.RequestModel;


import com.rosbank.android.russia.utils.RemoveNullStringFactory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Map;

public class DCRBaseRequest {
    @Expose
    protected String language;

    public String mAuthorizeToken;
    public DCRBaseRequest(){
        language = "ru-RU";
    }
    public void setAuthorizeToken(String token) {
        this.mAuthorizeToken = token;
    }

    public String getAuthorizeToken() {
        return mAuthorizeToken;
    }

    public Map<String, String> toMap(){
        Gson gson = new GsonBuilder()
                .registerTypeAdapterFactory(new RemoveNullStringFactory())
                .create();
        Type type = new TypeToken<DCRBaseRequest>(){}.getType();
        String jsonStr = gson.toJson(this);
        type = new TypeToken<Map<String, String>>(){}.getType();
        Map<String, String> myMap = gson.fromJson(jsonStr, type);
        return myMap;
    }

}
