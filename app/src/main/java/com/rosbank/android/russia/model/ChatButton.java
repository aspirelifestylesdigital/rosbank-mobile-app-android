package com.rosbank.android.russia.model;

import com.google.gson.annotations.Expose;

/**
 * Created by nga.nguyent on 10/17/2016.
 */

public class ChatButton {
    @Expose
    String action;
    @Expose
    String type;
    @Expose
    String name;

    //private variable for state of button is pressed or not.
    private boolean selected;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isYes(){
        return "api_confirm".equalsIgnoreCase(type) && "Yes".equalsIgnoreCase(name);
    }

    public boolean isNo(){
        return "api_confirm".equalsIgnoreCase(type) && "No".equalsIgnoreCase(name);
    }

    public boolean isCallUs(){
        return "callus".equalsIgnoreCase(type);
    }

    public boolean isLiveChat(){
        return "livechat".equalsIgnoreCase(type);
    }

    /**
     * Recommend buttons
     * */
    public boolean isAvailableTables(){
        return "availabletables".equalsIgnoreCase(action);
    }

    public boolean isAuthenticVietnameseFood(){
        return "authenticfood".equalsIgnoreCase(action);
    }

    public boolean isConciergeRecommendation(){
        return "conciergerecommendation".equalsIgnoreCase(action);
    }

    public boolean isMichelinStars(){
        return "michelinstars".equalsIgnoreCase(action);
    }

    public boolean isNearbyRestaurants(){
        return "nearbyrestaurants".equalsIgnoreCase(action);
    }

    public boolean isNewRestaurants(){
        return "newrestaurants".equalsIgnoreCase(action);
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
