package com.rosbank.android.russia.utils;

import android.os.Bundle;
import android.text.TextUtils;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class StringUtil {

    /**
     * Format the number to display
     * Ex: 1 - 01
     */
    public static String Format2Digit(int value) {
        return String.format("%02d", value);
    }

    /**
     * Format the number to display
     * Ex:  1 - 1
     * 12 - 12
     */
    public static String Format1Digit(int value) {
        return String.format("%d", value);
    }

    /**
     * Doing duplicate text loop with number
     */
    public static String Duplicate(String text, int number) {
        if (number <= 0) {
            return text;
        }
        String val = text + Duplicate(text, number - 1);
//		String.format( "%s", text)
        return val;
    }

    /**
     * check value is empty [null or empty] return true.
     */
    public static boolean isEmpty(String value) {

        if (value == null /*|| value.equalsIgnoreCase("null")*/) {
            return true;
        }

        return TextUtils.isEmpty(value);

    }

    /**
     * comapre ignore case
     */
    public static boolean compare(String text1, String text2) {

        if (text1 == null || text2 == null) {
            return false;
        }

        return text1.compareToIgnoreCase(text2) == 0 ? true : false;
    }

    public static String removeSpace(String text) {
        if (isEmpty(text)) {
            return text;
        }

        text = text.replaceAll("\\s+", "");
        text = text.replaceAll("\\s", "");

        return text;
    }

    public static String upperFirstCharacter(String s) {

        String tmp = s.toLowerCase();

        if (tmp.length() > 0) {
            String first = tmp.substring(0, 1).toUpperCase();
            tmp = first + tmp.substring(1);
        }

        return tmp;

    }

    private JSONObject convertBundleToJson(Bundle bundle) {
        JSONObject json = new JSONObject();
        Set<String> keys = bundle.keySet();
        for (String key : keys) {
            try {
                // json.put(key, bundle.get(key)); see edit below
                String obj = (String) bundle.get(key);

                if (obj.indexOf(":") != -1) {
                    json.put(key, new JSONObject((String) obj));
                } else {
                    json.put(key, obj);
                }

            } catch (JSONException e) {
                //Handle exception here
            }
        }
        return json;
    }


    /**
     * equal ignore case
     */
    public static boolean equals(String text1, String text2) {

        boolean empty1 = isEmpty(text1);
        boolean empty2 = isEmpty(text2);

        if (empty1 && empty2) {
            return true;
        }

        if (empty1 && !empty2) {
            return false;
        }

        if (!empty1 && empty2) {
            return false;
        }

        return text1.equalsIgnoreCase(text2);
    }

    public static String[] getKeyValueFromSplitor(String fullName, String split) {
        String[] firstLastName = new String[2];
        if (!TextUtils.isEmpty(fullName)) {
            fullName = fullName.trim();
        }
        if (!TextUtils.isEmpty(fullName)) {
            int firstSpaceIndex = fullName.indexOf(split);
            if (firstSpaceIndex > -1) {
                firstLastName[0] = fullName.substring(0, firstSpaceIndex);
                firstLastName[1] = fullName.substring(firstSpaceIndex + 1).trim();
            }
        }
        return firstLastName;
    }

    public static void makeFirstLetterUpperCase(EditText editText) {
        int selection = editText.getSelectionStart();
        String text = editText.getText().toString();
        if (text.length() >= 1 && Character.isLetter(text.charAt(0)) && Character.getType(text.charAt(0)) == Character.LOWERCASE_LETTER) {
            String newText = text.substring(0, 1).toUpperCase() + text.substring(1);
            editText.setText(newText);
            editText.setSelection(selection);
        }
    }

    public static String removeAllSpecialCharacterAndBreakLine(String original) {
        if (TextUtils.isEmpty(original)) {
            return "";
        }
        // !@#$%&*()-+_=[]:;"',.?/
        String[] unsupportedCharacterArr = new String[]{"`", "~", "^", "{", "}", "<", ">", "|", "'"};
        // Replace unicode
        //String result = original.replaceAll("[^\\x00-\\x7F]", "");

        String result = original.replaceAll("[\\u0080-\\u0400]", "").replaceAll("[\\u0500-\\uFFFF]", "");

        // Replace break line
        result = result.replace("\n", " ").replace("\r", " ");
        // Remove unsupported characters
        for (String unsupportedCharacter : unsupportedCharacterArr) {
            result = result.replace(unsupportedCharacter, "");
        }
        return result;
        //	return original;
    }

    public static String removeAllSpecialCharacter(String original) {
        if (TextUtils.isEmpty(original)) {
            return "";
        }
        // !@#$%&*()-+_=[]:;"',.?/
        String[] unsupportedCharacterArr = new String[]{"`", "~", "^", "{", "}", "<", ">", "|", "'"};
        // Replace unicode
        //String result = original.replaceAll("[^\\x00-\\x7F]", "");

        String result = original.replaceAll("[\\u0080-\\u0400]", "").replaceAll("[\\u0500-\\uFFFF]", "");

        // Remove unsupported characters
        for (String unsupportedCharacter : unsupportedCharacterArr) {
            result = result.replace(unsupportedCharacter, "");
        }
        return result;
        //	return original;
    }

    public static boolean containSpecialCharacter(String original) {
        String filterSpecialCharacter = removeAllSpecialCharacter(original);
        return !original.equals(filterSpecialCharacter);
        //return false;
    }

    public static List<String> removeAllSpecialCharactersAndFillInList(Object... values) {
        List<String> resultList = new ArrayList<>();
        for (Object value : values) {
            resultList.add(removeAllSpecialCharacterAndBreakLine(value.toString()));
        }
        return resultList;
    }

    //temporary sick approach
    public static String getEventName(String details) {
        HashMap<String, String> requestDetails = new HashMap<>();
        String[] data = details.split(" \\| |\\|");
        for (String detail : data) {
            requestDetails.put(detail.split(" - ")[0], detail.split(" - ")[1]);
        }
        if (!isEmpty(requestDetails.get("EventName"))) {
            return requestDetails.get("EventName");
        } else {
            return "";
        }
    }

    //temporary sick approach
    public static String getEventCategory(String details) {
        HashMap<String, String> requestDetails = new HashMap<>();
        String[] data = details.split(" \\| |\\|");
        for (String detail : data) {
            requestDetails.put(detail.split(" - ")[0], detail.split(" - ")[1]);
        }
        if (!isEmpty(requestDetails.get("EventCategory"))) {
            return requestDetails.get("EventCategory");
        } else if (!isEmpty(requestDetails.get("Event Category"))) {
            return requestDetails.get("Event Category");
        } else {
            return "";
        }
    }
}
