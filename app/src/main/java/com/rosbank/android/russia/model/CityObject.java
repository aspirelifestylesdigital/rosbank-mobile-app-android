package com.rosbank.android.russia.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;


public class CityObject
        implements Parcelable {
    @Expose
    private CountryObject Country;
    @Expose
    private String States;
    @Expose
    private String CityName;

    public CountryObject getCountry() {
        return Country;
    }

    public void setCountry(final CountryObject country) {
        Country = country;
    }

    public String getStates() {
        return States;
    }

    public void setStates(final String states) {
        States = states;
    }

    public String getCityName() {
        return CityName;
    }

    public void setCityName(final String cityName) {
        CityName = cityName;
    }


    @Override
    public String toString() {
        return CityName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel parcel,
                              final int i) {

        parcel.writeString(this.States);
        parcel.writeString(this.CityName);

    }
    protected CityObject(Parcel in) {
        this.CityName = in.readString();
        this.States = in.readString();
    }
    public CityObject() {
        this.CityName = "";
        this.States = "";
    }

    public static final Creator<CityObject> CREATOR = new Creator<CityObject>() {
        @Override
        public CityObject createFromParcel(Parcel source) {
            return new CityObject(source);
        }

        @Override
        public CityObject[] newArray(int size) {
            return new CityObject[size];
        }
    };
}
