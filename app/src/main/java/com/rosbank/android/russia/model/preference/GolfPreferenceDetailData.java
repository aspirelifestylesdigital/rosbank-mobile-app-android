package com.rosbank.android.russia.model.preference;

import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CGetPreferenceResponse;
import com.rosbank.android.russia.application.AppConstant;

/**
 * Created by ThuNguyen on 12/13/2016.
 */

public class GolfPreferenceDetailData extends PreferenceData{
    private String courseName;
    private String teeTimes;
    private String preferredGolfHandicap;
    public GolfPreferenceDetailData(B2CGetPreferenceResponse response){
        super(response);
        preferenceType = AppConstant.PREFERENCE_TYPE.GOLF;
        if(response != null){
            courseName = response.getVALUE();
            teeTimes = response.getVALUE1();
            preferredGolfHandicap = response.getVALUE2();
        }
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getTeeTimes() {
        return teeTimes;
    }

    public void setTeeTimes(String teeTimes) {
        this.teeTimes = teeTimes;
    }

    public void setPreferredGolfHandicap(String preferredGolfHandicap) {
        this.preferredGolfHandicap = preferredGolfHandicap;
    }

    public String getPreferredGolfHandicap() {
        return preferredGolfHandicap;
    }
}
