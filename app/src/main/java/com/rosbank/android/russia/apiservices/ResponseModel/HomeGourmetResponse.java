package com.rosbank.android.russia.apiservices.ResponseModel;

import com.rosbank.android.russia.apiservices.apis.appimplement.BaseResponse;
import com.rosbank.android.russia.model.GourmetResponeData;
import com.google.gson.annotations.Expose;


public class HomeGourmetResponse
        extends BaseResponse {
    @Expose
    private GourmetResponeData Data;

    public GourmetResponeData getData() {
        return Data;
    }

    public void setData(GourmetResponeData data) {
        this.Data = data;
    }
}
