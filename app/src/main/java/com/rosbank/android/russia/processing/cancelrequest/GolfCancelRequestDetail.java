package com.rosbank.android.russia.processing.cancelrequest;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.apiservices.ResponseModel.GolfBookingDetailResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.rosbank.android.russia.apiservices.b2c.B2CWSGetRequestDetail;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.B2CUpsertConciergeRequestRequest;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CGetRecentRequestResponse;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.application.AppContext;
import com.rosbank.android.russia.model.CountryObject;
import com.rosbank.android.russia.model.concierge.GolfBookingDetailData;
import com.rosbank.android.russia.utils.DateTimeUtil;
import com.rosbank.android.russia.utils.SharedPreferencesUtils;
import com.rosbank.android.russia.utils.StringUtil;

import android.text.TextUtils;
import android.view.View;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by ThuNguyen on 10/26/2016.
 */

public class GolfCancelRequestDetail extends BaseCancelRequestDetail{
    private GolfBookingDetailData golfBookingDetailData;
    public GolfCancelRequestDetail() {
    }

    public GolfCancelRequestDetail(View view) {
        super(view);
    }

    @Override
    public void getRequestDetail() {
        showProgressDialog();
        B2CWSGetRequestDetail b2CWSGetRequestDetail = new B2CWSGetRequestDetail(this);
        b2CWSGetRequestDetail.setValue(myRequestObject.getEpcCaseId(), myRequestObject.getBookingItemID());
        b2CWSGetRequestDetail.run(null);
        /*WSGetGolfBookingDetail wsGetGolfBookingDetail = new WSGetGolfBookingDetail();
        wsGetGolfBookingDetail.setBookingId(myRequestObject.getBookingId());
        wsGetGolfBookingDetail.run(this);
        showProgressDialog();*/
    }
    @Override
    protected void updateUI() {
        super.updateUI();
        long epochCreatedDate = golfBookingDetailData.getCreateDateEpoc();
        tvRequestedDate.setText(DateTimeUtil.shareInstance().formatTimeStamp(epochCreatedDate, "dd MMMM yyyy"));
        tvRequestedTime.setText(DateTimeUtil.shareInstance().formatTimeStamp(epochCreatedDate, "hh:mm aa"));
        // Request type
        tvRequestType.setText(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_title_golf_request));
        // Request name
        String requestName = golfBookingDetailData.getGolfCourseName();
        if(TextUtils.isEmpty(requestName) || requestName.equalsIgnoreCase("null")){
            requestName = myRequestObject.getItemTitle();
        }
        if(TextUtils.isEmpty(requestName) || requestName.equalsIgnoreCase("null")){
            tvRequestName.setText(golfBookingDetailData.getCity());
        }else{
            tvRequestName.setText(requestName + ", " + golfBookingDetailData.getConciergeRequestDetail().getCity());
        }

        // Request detail header
        tvRequestDetailHeader.setText(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_golf_request_detail));

        if(golfBookingDetailData != null){
            // Case Id
            tvCaseId.setText(golfBookingDetailData.getBookingItemID());
            // Golf course name
            String golfCourseName = golfBookingDetailData.getGolfCourseName();
            if(TextUtils.isEmpty(golfCourseName) || golfCourseName.equalsIgnoreCase("null")){
                //addRequestDetailItem("Name of Golf Course", "N.A");
            }else{
                addRequestDetailItem(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_name_of_golf_course), golfCourseName);
            }
            // Date
            long date = golfBookingDetailData.getStartDateEpoch();
          addRequestDetailItem(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_date), DateTimeUtil.shareInstance().formatTimeStamp(date, "dd MMMM yyyy"));
            // City
            addRequestDetailItem(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_city), golfBookingDetailData.getConciergeRequestDetail().getCity());

        }
    }
    @Override
    public void onResponse(Call call, Response response) {
        hideProgressDialog();
        if(response != null && response.body() instanceof GolfBookingDetailResponse){
            golfBookingDetailData = ((GolfBookingDetailResponse)response.body()).getData();
            updateUI();
        }
    }

    @Override
    public void onFailure(Call call, Throwable t) {
        hideProgressDialog();
    }

    @Override
    public void onB2CResponse(final B2CBaseResponse response) {
        hideProgressDialog();
        if(response instanceof B2CGetRecentRequestResponse){
            golfBookingDetailData = new GolfBookingDetailData((B2CGetRecentRequestResponse)response);
            updateUI();
        }

    }

    @Override
    public void onB2CResponseOnList(final List<B2CBaseResponse> responseList) {

    }

    @Override
    public void onB2CFailure(final String errorMessage,
                             final String errorCode) {
        hideProgressDialog();
    }
    @Override
    public B2CUpsertConciergeRequestRequest getB2CUpsertConciergeRequestRequest() {
        B2CUpsertConciergeRequestRequest upsertConciergeRequestRequest = new B2CUpsertConciergeRequestRequest();
        upsertConciergeRequestRequest.setFunctionality(AppConstant.CONCIERGE_FUNCTIONALITY_TYPE.GOLF.getValue());
        upsertConciergeRequestRequest.setRequestType(AppConstant.CONCIERGE_REQUEST_TYPE.GOLF.getValue());
        upsertConciergeRequestRequest.setEmail1((SharedPreferencesUtils.getPreferences(AppConstant.USER_EMAIL_PRE, "")));
        upsertConciergeRequestRequest.setSituation("golf");
        if(!TextUtils.isEmpty(golfBookingDetailData.getMobileNumber())) {
            upsertConciergeRequestRequest.setPhoneNumber(golfBookingDetailData.getMobileNumber());
        }
        upsertConciergeRequestRequest.setPrefResponse(golfBookingDetailData.getPrefResponse());

        // Pickup date
        upsertConciergeRequestRequest.setStartDate(DateTimeUtil.shareInstance().formatTimeStamp(golfBookingDetailData.getStartDateEpoch(), AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS));
        if(!TextUtils.isEmpty(golfBookingDetailData.getCity())) {
            upsertConciergeRequestRequest.setCity(golfBookingDetailData.getCity());
        }
        if(!TextUtils.isEmpty(golfBookingDetailData.getState())) {
            upsertConciergeRequestRequest.setState(golfBookingDetailData.getState());
        }else {
            upsertConciergeRequestRequest.setState(golfBookingDetailData.getCity());
        }
        if(!TextUtils.isEmpty(golfBookingDetailData.getCountry())) {
            upsertConciergeRequestRequest.setCountry(CountryObject.getCountryObjectFromName(golfBookingDetailData.getCountry()).getCountryCode());
        }
        if(golfBookingDetailData.getConciergeRequestDetail()!=null && !TextUtils.isEmpty(golfBookingDetailData.getConciergeRequestDetail().getNoofBallers())) {
            upsertConciergeRequestRequest.setNumberOfAdults(golfBookingDetailData.getConciergeRequestDetail()
                                                                                 .getNoofBallers());
        }

        if(myRequestObject != null){
            upsertConciergeRequestRequest.setTransactionID(myRequestObject.getBookingItemID());
        }
        // Request details
        upsertConciergeRequestRequest.setRequestDetails(golfBookingDetailData.getRequestDetaiString());
        upsertConciergeRequestRequest.setNumberOfAdults(String.valueOf(golfBookingDetailData.getNumberOfAdults()));
        return upsertConciergeRequestRequest;
    }
    private String combineRequestDetails(){
        String requestDetails = "";
        if(!TextUtils.isEmpty(golfBookingDetailData.getConciergeRequestDetail().getSpecialRequirement())){
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_SPECIAL_REQ + StringUtil.removeAllSpecialCharacterAndBreakLine(golfBookingDetailData.getConciergeRequestDetail().getSpecialRequirement());
        }
        if(!TextUtils.isEmpty(golfBookingDetailData.getConciergeRequestDetail().getGuestName())){
            if(!TextUtils.isEmpty(requestDetails)){
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_GUEST_NAME + StringUtil.removeAllSpecialCharacterAndBreakLine(golfBookingDetailData.getConciergeRequestDetail().getGuestName());
        }
        if(!TextUtils.isEmpty(golfBookingDetailData.getConciergeRequestDetail().getGuestName())) {
            if (!TextUtils.isEmpty(requestDetails)) {
                requestDetails += " | ";
            }
            requestDetails +=
                    AppConstant.B2C_REQUEST_DETAIL_DATE_OF_PLAY + golfBookingDetailData.getConciergeRequestDetail().getDateofplay();
        }
        if(!TextUtils.isEmpty(golfBookingDetailData.getConciergeRequestDetail().getTeetime())) {
            if (!TextUtils.isEmpty(requestDetails)) {
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_TEE_TIME + golfBookingDetailData.getConciergeRequestDetail().getTeetime();
        }

        if (!TextUtils.isEmpty(golfBookingDetailData.getConciergeRequestDetail().getCountry())) {
            if (!TextUtils.isEmpty(requestDetails)) {
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_COUNTRY + golfBookingDetailData.getConciergeRequestDetail().getCountry();
        }
        if(!TextUtils.isEmpty(golfBookingDetailData.getConciergeRequestDetail().getCity())){
            if(!TextUtils.isEmpty(requestDetails)){
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_CITY+ StringUtil.removeAllSpecialCharacterAndBreakLine(golfBookingDetailData.getConciergeRequestDetail().getCity());
        }
        if(!TextUtils.isEmpty(golfBookingDetailData.getGolfCourseName())){
            if(!TextUtils.isEmpty(requestDetails)){
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_GOLF_COURSE + StringUtil.removeAllSpecialCharacterAndBreakLine(golfBookingDetailData.getGolfCourseName());
        }
        if(!TextUtils.isEmpty(requestDetails)){
            requestDetails += " | ";
        }
        requestDetails += AppConstant.B2C_REQUEST_DETAIL_NO_BALLERS + golfBookingDetailData.getConciergeRequestDetail().getNoballers();

        if(!TextUtils.isEmpty(requestDetails)){
            requestDetails += " | ";
        }
        if(!TextUtils.isEmpty(golfBookingDetailData.getConciergeRequestDetail().getHolepreference())) {
            requestDetails +=
                    AppConstant.B2C_REQUEST_DETAIL_HOLES + golfBookingDetailData.getConciergeRequestDetail()
                                                                                .getHolepreference();
        }
               return requestDetails;
    }
}
