package com.rosbank.android.russia.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rosbank.android.russia.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author anh.trinh
 * @since v0.1
 */
public class NumberPickerView
        extends RelativeLayout {

    private static final int LAYOUT_RESOURCE_ID = R.layout.number_picker_view; // 1s

    @BindView(R.id.ic_minus)
    ImageButton icMinus;
    @BindView(R.id.ic_plus)
    ImageButton icPlus;
    @BindView(R.id.tv_number)
    TextView tvNumber;

    private int number =0;
    private int minimum=0;
    private int maximum =100;


    /**
     * @param context
     *         The context which view is running on.
     * @return New ItemListCollections object.
     * @since v0.1
     */
    public static NumberPickerView newInstance(final Context context) {
        if (context == null) {
            return null;
        }

        return new NumberPickerView(context);
    }

    /**
     * The constructor with current context.
     *
     * @param context
     *         The context which view is running on.
     * @since v0.1
     */
    public NumberPickerView(final Context context) {
        super(context);
        this.initialize();
    }

    /**
     * The constructor with current context.
     *
     * @param context
     *         The context which view is running on.
     * @param attrs
     *         The initialize attributes set.
     * @since v0.1
     */
    public NumberPickerView(final Context context,
                            final AttributeSet attrs) {
        super(context,
              attrs);
        this.initialize();
    }

    /**
     * The constructor with current context.
     *
     * @param context
     *         The context which view is running on.
     * @param attrs
     *         The initialize attributes set.
     * @param defStyleAttr
     *         The default style.
     * @since v0.1
     */
    public NumberPickerView(final Context context,
                            final AttributeSet attrs,
                            final int defStyleAttr) {
        super(context,
              attrs,
              defStyleAttr);

        this.initialize();
    }

    /**
     * @see View#setEnabled(boolean)
     * @since v0.1
     */
    @Override
    public void setEnabled(final boolean isEnabled) {
        this.setAlpha(isEnabled ?
                      1f :
                      0.5f);
        super.setEnabled(isEnabled);
    }

    /**
     * Reset injected resources created by ButterKnife.
     *
     * @since v0.1
     */
    public void resetInjectedResource() {
        ButterKnife.bind(this);
    }

    /**
     * Initialize UI sub views.
     *
     * @since v0.1
     */
    private void initialize() {

        final LayoutInflater layoutInflater = LayoutInflater.from(this.getContext());
        layoutInflater.inflate(LAYOUT_RESOURCE_ID,
                               this,
                               true);

        ButterKnife.bind(this,
                         this);
    }
    @OnClick({R.id.ic_minus,
              R.id.ic_plus})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ic_minus:
                if(this.number>this.minimum){
                    setNumber(number-1);
                }
                break;
            case R.id.ic_plus:
                if(this.number<this.maximum){
                    setNumber(number+1);
                }
                break;
        }
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(final int number) {
        if(number<this.minimum){
            return;
        }
        this.number = number;
        tvNumber.setText(String.valueOf(this.number));
    }

    public int getMinimum() {
        return minimum;
    }

    public void setMinimum(final int minimum) {
        this.minimum = minimum;
        setNumber(this.minimum);
    }

    public int getMaximum() {
        return maximum;
    }

    public void setMaximum(final int maximum) {
        this.maximum = maximum;
    }
}
