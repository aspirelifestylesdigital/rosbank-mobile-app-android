package com.rosbank.android.russia.apiservices;

import com.rosbank.android.russia.apiservices.RequestModel.ForgotPassRequest;
import com.rosbank.android.russia.apiservices.apis.appimplement.ApiProviderClient;
import com.rosbank.android.russia.apiservices.apis.appimplement.BaseResponse;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Callback;


public class WSUserForgotPassword
        extends ApiProviderClient<BaseResponse> {
    //
    private String mEmail;

    public WSUserForgotPassword(){

    }

    public void login(String email){
        mEmail = email;
    }

    @Override
    public void run(Callback<BaseResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }

    @Override
    protected boolean isUseAuthenticateInApi() {
        return true;
    }

    @Override
    public void runApi() {
        ForgotPassRequest request = new ForgotPassRequest();
        request.setEmail(mEmail);
        serviceInterface.userForgotSecret(request).enqueue(callback);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("User-Agent", "Retrofit-Sample-App");
        return headers;
    }

}
