package com.rosbank.android.russia.apiservices.RequestModel;


import com.google.gson.annotations.Expose;

public class SignUpRequest
        extends BaseRequest {
    @Expose
    private String Salutation;
    @Expose
    private String FirstName;
    @Expose
    private String LastName;
    @Expose
    private String MobileNumber;
    @Expose
    private String EmailAddress;
    @Expose
    private String CreditCardNumber;
    @Expose
    private String Password;
    @Expose
    private String ConfirmPassword;
    @Expose
    private Boolean HasAcceptTermAndPolicy;

    public Boolean getAcceptTermAndPolicy() {
        return HasAcceptTermAndPolicy;
    }

    public void setAcceptTermAndPolicy(final Boolean acceptTermAndPolicy) {
        HasAcceptTermAndPolicy = acceptTermAndPolicy;
    }

    public String getSalutation() {
        return Salutation;
    }

    public void setSalutation(final String salutation) {
        Salutation = salutation;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(final String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(final String lastName) {
        LastName = lastName;
    }

    public String getPhone() {
        return MobileNumber;
    }

    public void setPhone(final String phone) {
        MobileNumber = phone;
    }

    public String getConfirmPassword() {
        return ConfirmPassword;
    }

    public void setConfirmPassword(final String confirmPassword) {
        ConfirmPassword = confirmPassword;
    }

    public String getEmail() {
        return EmailAddress;
    }

    public void setEmail(String email) {
        EmailAddress = email;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getCreditCardNumber() {
        return CreditCardNumber;
    }

    public void setCreditCardNumber(final String creditCardNumber) {
        CreditCardNumber = creditCardNumber;
    }
}
