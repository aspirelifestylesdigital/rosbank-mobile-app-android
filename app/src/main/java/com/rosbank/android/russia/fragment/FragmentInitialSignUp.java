package com.rosbank.android.russia.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CICallback;
import com.rosbank.android.russia.application.coreactivitys.AbstractActivity;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentInitialSignUp extends BaseFragment implements SelectionFragment.SelectionCallback, Callback, B2CICallback {

    @BindView(R.id.btn_go_to_sign_up)
    Button btnNext;
    @BindView(R.id.tv_tool_bar_title)
    TextView mTvToolBarTitle;

    @Override
    protected int layoutId() {
        return R.layout.fragment_sign_up_initial;
    }

    @Override
    protected void initView() {
        mTvToolBarTitle.setText(R.string.text_sign_up_enter_code_title);
    }


    @OnClick({R.id.btn_go_to_sign_up, R.id.ic_back})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_go_to_sign_up:
                pushFragment(new SignUpFragment(),
                        true,
                        true);
                break;
            case R.id.ic_back:
                onBackPress();
                break;
        }
    }


    @Override
    public void onB2CResponse(B2CBaseResponse response) {

    }

    @Override
    public void onB2CResponseOnList(List<B2CBaseResponse> responseList) {

    }

    @Override
    public void onB2CFailure(String errorMessage, String errorCode) {

    }


    public void onBackPress() {
        AbstractActivity activity = (AbstractActivity) getActivity();
        if (activity != null) {
            activity.onBackPressed();
        }
    }


    @Override
    protected void bindData() {

    }

    @Override
    protected boolean onBack() {
        return false;
    }

    @Override
    public void onFinishSelection(Bundle bundle) {

    }

    @Override
    public void onResponse(Call call, Response response) {

    }

    @Override
    public void onFailure(Call call, Throwable t) {

    }
}
