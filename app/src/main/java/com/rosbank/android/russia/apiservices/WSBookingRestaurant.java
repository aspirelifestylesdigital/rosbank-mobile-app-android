package com.rosbank.android.russia.apiservices;

import android.support.annotation.NonNull;

import com.rosbank.android.russia.apiservices.ResponseModel.BookRestaurantResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.ApiProviderClient;
import com.rosbank.android.russia.utils.StringUtil;
import com.google.gson.annotations.Expose;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Callback;


public class WSBookingRestaurant
        extends ApiProviderClient<BookRestaurantResponse> {

    //private BookRestaurantRequest request;
    private String photoPath = "";

    @Expose
    private String BookingId = "";
    @Expose
    private String UserID = "";
    @Expose
    private String BookingItemId = "";
    @Expose
    private String UploadPhoto = "";
    @Expose
    private Boolean Phone = false;
    @Expose
    private Boolean Email = false;
    @Expose
    private Boolean Both = false;
    @Expose
    private String MobileNumber = "";
    @Expose
    private String EmailAddress = "";
    @Expose
    private String ReservationDate = "";
    @Expose
    private String ReservationTime = "";
    @Expose
    private String ReservationName = "";
    @Expose
    private String Country = "";
    @Expose
    private String City = "";
    @Expose
    private Integer NumberOfAdults = 0;
    @Expose
    private Integer NumberOfKids = 0;
    @Expose
    private String SpecialRequirements = "";

    public WSBookingRestaurant(){

    }

    /*public void setRequest(final BookRestaurantRequest request){
        this.request = request;

    }*/

    @Override
    public void run(Callback<BookRestaurantResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }

    @Override
    protected boolean isUseAuthenticateInApi() {
        return true;
    }

    @Override
    public void runApi() {
        //serviceInterface.bookRestaurant(request).enqueue(callback);
        RequestBody partBookingId = createPartFromString(BookingId);
        RequestBody partUserID = createPartFromString(UserID);
        RequestBody partBookingItemId = createPartFromString(BookingItemId);

        RequestBody partPhone = createPartFromString(Phone+"");
        RequestBody partEmail = createPartFromString(Email+"");
        RequestBody partBoth = createPartFromString(Both+"");
        RequestBody partMobileNumber = createPartFromString(MobileNumber);
        RequestBody partEmailAddress = createPartFromString(EmailAddress);
        RequestBody partReservationDate = createPartFromString(ReservationDate);
        RequestBody partReservationTime = createPartFromString(ReservationTime);
        RequestBody partReservationName = createPartFromString(ReservationName);
        RequestBody partCountry = createPartFromString(Country);
        RequestBody partCity = createPartFromString(City);
        RequestBody partNumberOfAdults = createPartFromString(NumberOfAdults+"");
        RequestBody partNumberOfKids = createPartFromString(NumberOfKids+"");
        RequestBody partSpecialRequirements = createPartFromString(SpecialRequirements+"");

        MultipartBody.Part partUploadPhoto = null;

        File file = new File(photoPath);
        if(!StringUtil.isEmpty(photoPath) && file.exists()){
            RequestBody requestFile =
                    RequestBody.create(MediaType.parse("multipart/form-data"), file);
            partUploadPhoto =
                    MultipartBody.Part.createFormData("picture", file.getName(), requestFile);
        }
        else{

        }


        serviceInterface.bookRestaurant(partBookingId,
                partUserID,
                partBookingItemId,
                partUploadPhoto,
                partPhone,
                partEmail,
                partBoth,
                partMobileNumber,
                partEmailAddress,
                partReservationDate,
                partReservationTime,
                partReservationName,
                partCountry,
                partCity,
                partNumberOfAdults,
                partNumberOfKids,
                partSpecialRequirements
        ).enqueue(callback);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("User-Agent", "Retrofit-Sample-App");
        return headers;
    }

    @NonNull
    private RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(
                MediaType.parse("multipart/form-data"), descriptionString);
    }

    /*public BookRestaurantRequest getRequest() {
        return request;
    }*/

    public String getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    public String getBookingId() {
        return BookingId;
    }

    public void setBookingId(String bookingId) {
        BookingId = bookingId;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public String getBookingItemId() {
        return BookingItemId;
    }

    public void setBookingItemId(String bookingItemId) {
        BookingItemId = bookingItemId;
    }

    public String getUploadPhoto() {
        return UploadPhoto;
    }

    public void setUploadPhoto(String uploadPhoto) {
        UploadPhoto = uploadPhoto;
    }

    public Boolean getPhone() {
        return Phone;
    }

    public void setPhone(Boolean phone) {
        Phone = phone;
    }

    public Boolean getEmail() {
        return Email;
    }

    public void setEmail(Boolean email) {
        Email = email;
    }

    public Boolean getBoth() {
        return Both;
    }

    public void setBoth(Boolean both) {
        Both = both;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public String getEmailAddress() {
        return EmailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        EmailAddress = emailAddress;
    }

    public String getReservationDate() {
        return ReservationDate;
    }

    public void setReservationDate(String reservationDate) {
        ReservationDate = reservationDate;
    }

    public String getReservationTime() {
        return ReservationTime;
    }

    public void setReservationTime(String reservationTime) {
        ReservationTime = reservationTime;
    }

    public String getReservationName() {
        return ReservationName;
    }

    public void setReservationName(String reservationName) {
        ReservationName = reservationName;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public Integer getNumberOfAdults() {
        return NumberOfAdults;
    }

    public void setNumberOfAdults(Integer numberOfAdults) {
        NumberOfAdults = numberOfAdults;
    }

    public Integer getNumberOfKids() {
        return NumberOfKids;
    }

    public void setNumberOfKids(Integer numberOfKids) {
        NumberOfKids = numberOfKids;
    }

    public String getSpecialRequirements() {
        return SpecialRequirements;
    }

    public void setSpecialRequirements(String specialRequirements) {
        SpecialRequirements = specialRequirements;
    }
}
