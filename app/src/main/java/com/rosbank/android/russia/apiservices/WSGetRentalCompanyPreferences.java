package com.rosbank.android.russia.apiservices;

import com.rosbank.android.russia.apiservices.ResponseModel.RentalResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.ApiProviderClient;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Callback;


public class WSGetRentalCompanyPreferences
        extends ApiProviderClient<RentalResponse> {

    public WSGetRentalCompanyPreferences(){

    }
    @Override
    public void run(Callback<RentalResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }

    @Override
    protected boolean isUseAuthenticateInApi() {
        return true;
    }

    @Override
    public void runApi() {
        serviceInterface.getRentalCompanyPreferences().enqueue(callback);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("User-Agent", "Retrofit-Sample-App");
        return headers;
    }

}
