package com.rosbank.android.russia.apiservices.apis.appimplement.b2c;

import java.util.List;

/**
 * Created by ThuNguyen on 11/3/2016.
 */

public interface B2CICallback {
    void onB2CResponse(B2CBaseResponse response);
    void onB2CResponseOnList(List<B2CBaseResponse> responseList);
    void onB2CFailure(String errorMessage, String errorCode);
}
