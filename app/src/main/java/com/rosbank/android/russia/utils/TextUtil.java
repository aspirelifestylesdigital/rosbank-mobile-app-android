package com.rosbank.android.russia.utils;

import android.graphics.Color;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;

/**
 * Created by ThuNguyen on 11/9/2016.
 */

public class TextUtil {
    public static final SpannableString makeBoldStarOnText(String text){
        SpannableString spannableString = new SpannableString(text);
        ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(Color.RED);
        int starStart = text.indexOf("*");
        int endStart = starStart + 1;
        spannableString.setSpan(foregroundColorSpan, starStart, endStart, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannableString;
    }
}
