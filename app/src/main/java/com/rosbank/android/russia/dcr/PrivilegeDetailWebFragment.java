package com.rosbank.android.russia.dcr;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.activitys.HomeActivity;
import com.rosbank.android.russia.activitys.LoginAcitivy;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.dcr.model.DCRPrivilegeObject;
import com.rosbank.android.russia.fragment.BookHotelFragment;
import com.rosbank.android.russia.fragment.BookingOtherFragment;
import com.rosbank.android.russia.fragment.ConciergeGourmetTab1Fragment;
import com.rosbank.android.russia.model.UserItem;
import com.rosbank.android.russia.utils.CommonUtils;

import butterknife.BindView;
import butterknife.OnClick;

/*
 * Created by chau.nguyen on 10/05/2016.
 */
public class PrivilegeDetailWebFragment
        extends BaseFragment {

    public static final String AboutWebTitle = "webTitle";
    public static final String AboutWebLink = "webLink";
    public static final String PRIVILEGE_DATA = "data";

    @BindView(R.id.webView_about_common)
    WebView aboutWebView;

    String titleName = "", linkWeb = "";
    private String privilegeType = "";
    DCRPrivilegeObject mDcrPrivilegeObject;

    public PrivilegeDetailWebFragment() {

    }

    @Override
    protected int layoutId() {
        return R.layout.fragment_privilege_detail_web;
    }

    @Override
    protected void initView() {
        aboutWebView.getSettings()
                .setJavaScriptEnabled(true);
    }

    @Override
    protected void bindData() {
        if (getArguments() != null) {
            privilegeType = getArguments().getString(PrivilegesFragment.PRIVILEGE_TYPE,
                    "");
            mDcrPrivilegeObject =
                    (DCRPrivilegeObject) getArguments().getSerializable(PRIVILEGE_DATA);
        }
        titleName = getArguments().getString(AboutWebTitle,
                "");
        linkWeb = getArguments().getString(AboutWebLink,
                "");

        aboutWebView.loadUrl(linkWeb);

        aboutWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view,
                                      String url,
                                      Bitmap favicon) {
                showDialogProgress();
                super.onPageStarted(view,
                        url,
                        favicon);
            }

            @Override
            public void onPageFinished(WebView view,
                                       String url) {
                hideDialogProgress();
                super.onPageFinished(view,
                        url);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showLogoApp(false);
            ((HomeActivity) getActivity()).setTitle(titleName);
        }
    }

    @Override
    public boolean onBack() {
        return false;
    }

    private void bookPrivilegeDetail(DCRPrivilegeObject dcrPrivilegeObject) {
        if (UserItem.isLogined()) {
            if (privilegeType.equals(DCRPrivilegeObject.PRIVILEGE_TYPE_HOTEL)) {
                BaseFragment fragment = new BookHotelFragment();
                Bundle bundle = new Bundle();
                if (dcrPrivilegeObject != null) {
                    bundle.putSerializable(AppConstant.PRE_HOTEL_DATA,
                            dcrPrivilegeObject);
                }
                fragment.setArguments(bundle);
                pushFragment(fragment,
                        true,
                        true);
            } else {
                if (privilegeType.equals(DCRPrivilegeObject.PRIVILEGE_TYPE_RESTAURANT)) {
                    BaseFragment fragment = new ConciergeGourmetTab1Fragment();
                    Bundle bundle = new Bundle();
                    if (dcrPrivilegeObject != null) {
                        bundle.putSerializable(AppConstant.PRE_RESTAURANT_DATA,
                                dcrPrivilegeObject);
                    }
                    fragment.setArguments(bundle);
                    pushFragment(fragment,
                            true,
                            true);
                } else {
                    if (privilegeType.equals(DCRPrivilegeObject.PRIVILEGE_TYPE_SPA)) {
                        BaseFragment fragment = new BookingOtherFragment();
                        Bundle bundle = new Bundle();
                        if (dcrPrivilegeObject != null) {
                            bundle.putSerializable(AppConstant.PRE_SPA_DATA,
                                    dcrPrivilegeObject);
                        }
                        fragment.setArguments(bundle);
                        pushFragment(fragment,
                                true,
                                true);
                    }
                }
            }

        } else {
            CommonUtils.showDialogRequiredSignIn(getContext(),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(final DialogInterface dialog,
                                            final int which) {
                            Intent intent = new Intent(getContext(),
                                    LoginAcitivy.class);
                            startActivity(intent);
                        }
                    },
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(final DialogInterface dialog,
                                            final int which) {

                        }
                    });
        }


    }

    @OnClick({R.id.btnPrivilegeDetailBook})
    public void onPrivilegeDetailBookClick(View view) {
        bookPrivilegeDetail(mDcrPrivilegeObject);
    }
}


