package com.rosbank.android.russia.fragment;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.adapter.ExperiencesDetailAdapter;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.interfaces.OnRecyclerViewItemClickListener;
import com.rosbank.android.russia.model.ExperiencesDetailObject;
import com.rosbank.android.russia.model.Restaurant;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/*
 * Created by chau.nguyen on 10/20/2016.
 */
public class ExperiencesDetailGourmetPhoneFragment extends BaseFragment {

    @BindView(R.id.rcvDetailCommon)
    RecyclerView rcv;
    List<ExperiencesDetailObject> listExpDetailObj;

    private Restaurant restaurant;

    public ExperiencesDetailGourmetPhoneFragment() {
        listExpDetailObj = new ArrayList<>();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int layoutId() {
        return R.layout.fragment_experiences_detail_gourmet_common;
    }

    @Override
    protected void initView() {
        if (restaurant != null) {

            /*if(!restaurant.getTelephone().isEmpty()){
                listExpDetailObj.add(new ExperiencesDetailObject(restaurant.getTelephone(), ExperiencesDetailAdapter.ROW_SHOW.TELEPHONE));
            }*/

            if(!restaurant.getLink().getUrl().isEmpty()){
                listExpDetailObj.add(new ExperiencesDetailObject(restaurant.getLink().getUrl(), ExperiencesDetailAdapter.ROW_SHOW.WEBSITE));
            }

            String email = restaurant.getEmail();
            if(!email.isEmpty()){
                listExpDetailObj.add(new ExperiencesDetailObject(email, ExperiencesDetailAdapter.ROW_SHOW.EMAIL));
            }

            ExperiencesDetailAdapter adapter = new ExperiencesDetailAdapter(listExpDetailObj, new OnRecyclerViewItemClickListener() {
                @Override
                public void onItemClick(int position, View view) {
                    /*if (view.getTag() instanceof ExperiencesDetailObject) {

                        ExperiencesDetailObject obj = (ExperiencesDetailObject) view.getTag();

                    }*/
                }
            });

            LinearLayoutManager layoutManager = new LinearLayoutManager(view.getContext());
            layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            rcv.setLayoutManager(layoutManager);
            rcv.setAdapter(adapter);
        }
    }

    @Override
    protected void bindData() {

    }

    @Override
    protected boolean onBack() {
        return false;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }
}