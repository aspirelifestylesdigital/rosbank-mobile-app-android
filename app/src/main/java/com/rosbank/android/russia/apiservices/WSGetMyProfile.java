package com.rosbank.android.russia.apiservices;

import com.rosbank.android.russia.apiservices.RequestModel.GetMyProfileRequest;
import com.rosbank.android.russia.apiservices.ResponseModel.SignUpResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.ApiProviderClient;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Callback;


public class WSGetMyProfile
        extends ApiProviderClient<SignUpResponse> {
    //
    private String mUserID;

    public WSGetMyProfile(){

    }

    public void getMyProfile(String userId){
        mUserID = userId;
    }

    @Override
    public void run(Callback<SignUpResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }

    @Override
    protected boolean isUseAuthenticateInApi() {
        return true;
    }

    @Override
    public void runApi() {
        GetMyProfileRequest request = new GetMyProfileRequest();
        request.setUserID(mUserID);
        serviceInterface.getMyProfile(request).enqueue(callback);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("User-Agent", "Retrofit-Sample-App");
        return headers;
    }

}
