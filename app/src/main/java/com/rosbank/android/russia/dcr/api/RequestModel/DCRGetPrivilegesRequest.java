package com.rosbank.android.russia.dcr.api.RequestModel;


public class DCRGetPrivilegesRequest extends DCRBaseRequest {
    public String item_type;
    public String term;
    public String term_operator;
    public String exact_match;
    public String sort;
    public String facets;
    public String page;
    public String size;

    public DCRGetPrivilegesRequest() {

    }

    public String getLanguage() {
        return language;
    }

    public DCRGetPrivilegesRequest setLanguage(String language) {
        this.language = language;
        return this;
    }

    public String getItem_type() {
        return item_type;
    }

    public DCRGetPrivilegesRequest setItem_type(String item_type) {
        this.item_type = item_type;
        return this;
    }

    public String getTerm() {
        return term;
    }

    public DCRGetPrivilegesRequest setTerm(String term) {
        this.term = term;
        return this;
    }

    public String getTerm_operator() {
        return term_operator;
    }

    public DCRGetPrivilegesRequest setTerm_operator(String term_operator) {
        this.term_operator = term_operator;
        return this;
    }

    public String getExact_match() {
        return exact_match;
    }

    public DCRGetPrivilegesRequest setExact_match(String exact_match) {
        this.exact_match = exact_match;
        return this;
    }

    public String getSort() {
        return sort;
    }

    public DCRGetPrivilegesRequest setSort(String sort) {
        this.sort = sort;
        return this;
    }

    public String getFacets() {
        return facets;
    }

    public DCRGetPrivilegesRequest setFacets(String facets) {
        this.facets = facets;
        return this;
    }

    public String getPage() {
        return page;
    }

    public DCRGetPrivilegesRequest setPage(String page) {
        this.page = page;
        return this;
    }

    public String getSize() {
        return size;
    }

    public DCRGetPrivilegesRequest setSize(String size) {
        this.size = size;
        return this;
    }
}
