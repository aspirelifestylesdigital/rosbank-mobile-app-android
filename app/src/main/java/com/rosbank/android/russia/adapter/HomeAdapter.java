package com.rosbank.android.russia.adapter;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.interfaces.OnRecyclerViewItemClickListener;
import com.rosbank.android.russia.model.ConciergeRequestObject;
import com.rosbank.android.russia.model.Restaurant;
import com.rosbank.android.russia.model.UserItem;
import com.rosbank.android.russia.utils.EntranceLock;
import com.rosbank.android.russia.utils.Fontfaces;
import com.rosbank.android.russia.utils.NetworkUtil;
import com.rosbank.android.russia.utils.StringUtil;
import com.rosbank.android.russia.views.LoadingViewHolder;
import com.rosbank.android.russia.widgets.CustomErrorView;

import android.content.Context;
import android.os.Build;
import android.os.CountDownTimer;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;

import static android.view.View.GONE;

//import com.bumptech.glide.Glide;

/**
 * Created by nga.nguyent on 2/26/2016.
 */
public class HomeAdapter
        extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    enum ITEMTYPE {
        PARENT(0),
        CHILD(1),
        LOADING(2);

        ITEMTYPE(int val) {
            value = val;
        }

        public int getValue() {
            return value;
        }

        int value;
    }

    public interface OnHomePageClickListener {
        void onExpandClick(boolean expanded);

        void onFavouriteClick(int position);

        void onChatbotSubmit(String text);
        void showDialogNoConnection();
    }

    private Context context;
    private LayoutInflater mInflater;
    private List<Restaurant> data;
    private List<ConciergeRequestObject> conciergeRequestObjectList;

    private OnHomePageClickListener onmenuClick;
    private MyParentViewHolder parentView;
    private OnRecyclerViewItemClickListener onItemClickListener;
    private boolean expandView = true;

    public HomeAdapter(Context context) {
        this.context = context;
        mInflater = LayoutInflater.from(context);
    }

    public void setData(List<ConciergeRequestObject> data) {
        /*if(this.data==null){
            this.data = new ArrayList<>();
        }
        this.data.addAll(data);*/
        conciergeRequestObjectList = data;
    }

    public void updateWelcom() {
        if (parentView != null) {
            parentView.updateWelcom();
        }
    }
    public void resetPlaceHolder() {
        if (parentView != null) {
            parentView.resetPlaceHolder();
        }
    }

    public void setVisibilityForArrow(int visibility) {
        parentView.setVisibilityForArrow(visibility);
    }

    public void enforceArrowDance() {
        if (parentView != null) {
            parentView.makeArrowDance();
        }
    }
    public void enforceRotatePlaceHolder() {
        if (parentView != null) {
            parentView.rotate(0);
        }
    }

    public MyParentViewHolder getParentItemView() {
        return parentView;
    }

    public ConciergeRequestObject getItemAtIndex(int position) {
        /*if(position>=data.size())
            return null;
        return data.get(position);*/
        return conciergeRequestObjectList.get(position);
    }

    public void setOnmenuClick(OnHomePageClickListener onmenuClick) {
        this.onmenuClick = onmenuClick;
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void menuClick() {
        if (parentView != null) {
            parentView.onMenuClick();
        }
    }

    public void addLoadingKey() {
        //add key loading...
        if (!isShowLoading()) {
            data.add(null);
            notifyItemInserted(getItemCount() - 1);
        }
    }

    public void removeLoadingKey() {
        if (isShowLoading()) {
            data.remove(data.size() - 1);
            notifyItemRemoved(data.size());
        }
    }

    public boolean isShowLoading() {
        //becase position 0 always parentview
        if (data.size() > 1 && data.get(data.size() - 1) == null) {
            return true;
        }
        return false;
    }

    public void clear() {
        data.clear();
    }

    @Override
    public int getItemCount() {
        /*if(data==null)
            return 0;

        if(!expandView)
            return 1;

        return data.size();*/
        return conciergeRequestObjectList.size();
    }

    public int getRealSize() {
        if (data == null) {
            return 0;
        }
        if (data.size() <= 1) {
            return 0;
        }

        int n = data.size();
        n -= 1;//parent item
        if (isShowLoading()) {
            n -= 1;//loading item
        }
        if (n < 0) {
            n = 0;
        }
        return n;
    }

    @Override
    public int getItemViewType(int position) {
        //return super.getItemViewType(position);
        if (position == 0) {
            return ITEMTYPE.PARENT.value;
        }
//        else if(position==data.size()-1 && data.get( data.size() - 1  ) == null ){
//            return ITEMTYPE.LOADING.value;
//        }
        return ITEMTYPE.CHILD.value;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        if (viewType == ITEMTYPE.PARENT.getValue()) {
            View view = mInflater.inflate(R.layout.layout_item_home_parent,
                                          parent,
                                          false);
            MyParentViewHolder holder = new MyParentViewHolder(view);
            parentView = holder;
            return holder;
        } else if (viewType == ITEMTYPE.CHILD.getValue()) {
            View view = mInflater.inflate(R.layout.layout_item_home_child,
                                          parent,
                                          false);
            MyChildViewHolder holder = new MyChildViewHolder(view);
            return holder;
        } else {
            View view = mInflater.inflate(R.layout.layout_footer_loadmore,
                                          parent,
                                          false);
            LoadingViewHolder holder = new LoadingViewHolder(view);
            return holder;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder,
                                 int position) {
        if (holder instanceof MyParentViewHolder) {
            ((MyParentViewHolder) holder).setData();
        } else if (holder instanceof MyChildViewHolder) {
            ConciergeRequestObject rest = conciergeRequestObjectList.get(position);
            ((MyChildViewHolder) holder).setData(position,
                                                 rest);
        } else {
            //loading
        }
    }

    public class MyChildViewHolder
            extends RecyclerView.ViewHolder {
        @BindView(R.id.layoutExploreTitle)
        FrameLayout layoutExploreRecommendation;
        @BindView(R.id.imgBackground)
        public ImageView imgBackground;
        @BindView(R.id.imgFavorite)
        public ImageButton imgFavourite;
        @BindView(R.id.tvGourmetName)
        public TextView tvGroumetName;
        @BindView(R.id.tvRestaurantName)
        public TextView tvRestaurantName;
        @BindView(R.id.tvExploreRecommend)
        public TextView tvExploreRecommend;
        @BindView(R.id.loutFavorite)
        FrameLayout layoutFavourite;
        @BindView(R.id.tvConciergeRequest)
        TextView tvConciergeRequest;
        private int pos;

        public MyChildViewHolder(View view) {
            super(view);

            ButterKnife.bind(this,
                             view);

            tvExploreRecommend.setTypeface(Fontfaces.getFont(context,
                                                             Fontfaces.FONTLIST.AvenirLTStd_Light));
            tvGroumetName.setTypeface(Fontfaces.getFont(context,
                                                        Fontfaces.FONTLIST.AvenirLTStd_Black));
            tvRestaurantName.setTypeface(Fontfaces.getFont(context,
                                                           Fontfaces.FONTLIST.AvenirLTStd_Light));

            imgBackground.setClickable(true);
            imgBackground.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onItemClick(pos,
                                                        itemView);
                    }
                }
            });
            //imgBackground.getLayoutParams().height = (int)(CommonUtils.getScreenWidth((Activity) context) * 0.5);

            layoutFavourite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onmenuClick != null) {
                        onmenuClick.onFavouriteClick(pos);
                    }
                }
            });

        }

        public void setData(int position,
                            ConciergeRequestObject rest) {
            this.pos = position;
            if (position == 1) {
                layoutExploreRecommendation.setVisibility(View.VISIBLE);
            } else {
                layoutExploreRecommendation.setVisibility(GONE);
            }
            tvConciergeRequest.setText(rest.getBookingName());
            imgBackground.setImageResource(rest.getImageRes());

            /*int n = getParentItemList().get(0).getChildItemList().size();
            if(position==n){
                layoutLoading.setVisibility(View.VISIBLE);
            }
            else{
                layoutLoading.setVisibility(View.GONE);
            }*/

            /*if(!UserItem.isLogined()){
                imgFavourite.setVisibility(GONE);
                imgFavourite.setImageResource(R.drawable.icon_like);
                layoutFavourite.setClickable(false);
            }
            else {
                layoutFavourite.setClickable(true);
                imgFavourite.setVisibility(View.VISIBLE);
                if (rest.isVoted()) {
                    imgFavourite.setImageResource(R.drawable.icon_liked);
                } else {
                    imgFavourite.setImageResource(R.drawable.icon_like);
                }
            }
            tvGroumetName.setText(context.getString(R.string.text_gourname));
            tvRestaurantName.setText(rest.getRestaurantName());

//            Glide
//                    .with(context)
//                    .load(rest.getBackgroundImageUrl())
//                    .override(CommonUtils.getScreenWidth((Activity) context),(int)(CommonUtils.getScreenWidth((Activity)context)*0.5))
//                    .centerCrop()
//                    //.placeholder(R.drawable.loading_spinner)
//                    .crossFade()
//                    .into(imgBackground);*/

        }

    }

    public class MyParentViewHolder
            extends RecyclerView.ViewHolder {
        private static final float INITIAL_POSITION = 0.0f;
        private static final float ROTATED_POSITION = 180f;
        private final boolean HONEYCOMB_AND_ABOVE =
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;
        @BindView(R.id.tvWelcome)
        public TextView tvWelcome;
        //        @BindView(R.id.tvWelcomeName)
//        public TextView tvWelcomeName;
        @BindView(R.id.tvHello)
        public TextView tvHello;
        @BindView(R.id.tvPlaceHolder)
        public TextView tvPlaceHolder;
        @BindView(R.id.etMessage)
        public EditText edtMessage;
        @BindView(R.id.etMessageError)
        public CustomErrorView etMessageError;
        @BindView(R.id.btnSubmit)
        public Button btnSubmit;
        @BindView(R.id.btnCancel)
        public Button btnCancel;
        @BindView(R.id.rlArrow)
        public View rlArrow;
        @BindView(R.id.imgArrowDown)
        public ImageButton imgArrowDown;
        private EntranceLock entranceLock = new EntranceLock();

        public MyParentViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,
                             view);
            tvWelcome.setTypeface(Fontfaces.getFont(context,
                                                    Fontfaces.FONTLIST.AVENIR_NEXT_LT_MEDIUM));
            //tvWelcomeName.setTypeface(Fontfaces.getFont(context, Fontfaces.FONTLIST.AVENIR_NEXT_LT_MEDIUM));
            tvHello.setTypeface(Fontfaces.getFont(context,
                                                  Fontfaces.FONTLIST.AvenirLTStd_Light));
            tvPlaceHolder.setTypeface(Fontfaces.getFont(context,
                                                        Fontfaces.FONTLIST.AVENIR_NEXT_LT_MEDIUM));
            btnSubmit.setTypeface(Fontfaces.getFont(context,
                                                    Fontfaces.FONTLIST.Avenirnext_demibold));
            btnCancel.setTypeface(Fontfaces.getFont(context,
                                                    Fontfaces.FONTLIST.Avenirnext_demibold));
            etMessageError.fillData(context.getString(R.string.text_sign_up_error_required_field));
            //edtMessage.setImeOptions(EditorInfo.IME_ACTION_DONE);
            //edtMessage.setImeActionLabel("AAAA", KeyEvent.KEYCODE_ENTER);

            updateWelcom();

            rlArrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /*expandView = !expandView;

                    menuChangeState();*/

                    if (onmenuClick != null) {
                        onmenuClick.onExpandClick(expandView);
                    }
                }
            });
            makeArrowDance();

            rotate(0);
            edtMessage.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(final View v,
                                          final boolean hasFocus) {
                    if(!hasFocus && edtMessage.getText().toString().trim().equals("")) {
                        tvPlaceHolder.setVisibility(View.VISIBLE);
                    }
                }
            });
        }

        public void rotate(int count) {

            //
            final int nextCount = count < 3 ?
                                  count + 1 :
                                  0;
            String[] strings = {context.getString(R.string.text_can_you_recommend),
                                context.getString(R.string.text_book_me_a_private_jet),
                                context.getString(R.string.text_book_a_private_chef),
                                context.getString(R.string.text_are_there_any_hidden)};
            tvPlaceHolder.setText(strings[count]);
            tvPlaceHolder.setAnimation(null);
            final Animation animationFadeIn = AnimationUtils.loadAnimation(context,
                                                                           R.anim.fade_in);
            final Animation animationFadeOut = AnimationUtils.loadAnimation(context,
                                                                            R.anim.fade_out);



            animationFadeIn.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {

                    new CountDownTimer(4000,
                                       1000) {

                        public void onTick(long millisUntilFinished) {

                        }

                        public void onFinish() {
                            if(tvPlaceHolder.getVisibility()== View.VISIBLE) {
                                tvPlaceHolder.setAnimation(null);
                                tvPlaceHolder.startAnimation(animationFadeOut);
                            }
                        }
                    }.start();

                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            animationFadeOut.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {

                    if (nextCount >= 0) {
                        {
                            if (tvPlaceHolder.getVisibility() == View.VISIBLE) {
                                rotate(nextCount);
                            }
                        }
                    }

                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            animationFadeIn.setRepeatCount(0);
            animationFadeOut.setRepeatCount(0);
            tvPlaceHolder.setAnimation(null);
            tvPlaceHolder.startAnimation(animationFadeIn);
        }
        public void resetPlaceHolder() {
            edtMessage.setText("");
            edtMessage.clearFocus();
        }
        public void updateWelcom() {
            if (UserItem.isLogined()) {
                //tvWelcomeName.setText(UserItem.getLoginedFirstName() + " " + UserItem.getLoginedLastName());
                tvWelcome.setText(context.getString(R.string.text_welcome) + " " + UserItem.getLoginedFirstName() + " " + UserItem.getLoginedLastName());
                tvWelcome.setVisibility(View.VISIBLE);
            } else {
                String wl = context.getString(R.string.text_welcome)
                                   .replace(",",
                                            "");
                tvWelcome.setText(wl);
                tvWelcome.setVisibility(View.VISIBLE);
            }
        }

        public void setData() {
            updateWelcom();
            /*if (UserItem.isLogined()) {
                tvWelcomeName.setText(UserItem.getLoginedFirstName() + " " + UserItem.getLoginedLastName());
            } else {
                String wl = context.getString(R.string.text_welcome).replace(",", "");
                tvWelcome.setText(wl);
                tvWelcomeName.setText("");
            }*/
        }

        public void makeArrowDance() {
            Animation mAnimation = new TranslateAnimation(
                                                                 TranslateAnimation.ABSOLUTE,
                                                                 0f,
                                                                 TranslateAnimation.ABSOLUTE,
                                                                 0f,
                                                                 TranslateAnimation.RELATIVE_TO_PARENT,
                                                                 0f,
                                                                 TranslateAnimation.RELATIVE_TO_PARENT,
                                                                 -0.4f);
            mAnimation.setDuration(500);
            mAnimation.setRepeatCount(-1);
            mAnimation.setRepeatMode(Animation.REVERSE);
            mAnimation.setInterpolator(new LinearInterpolator());
            imgArrowDown.setAnimation(mAnimation);
        }

        public void resetDance() {
            imgArrowDown.setAnimation(null);
        }

        public void setVisibilityForArrow(int visibility) {
            rlArrow.setVisibility(visibility);
            if(visibility == View.VISIBLE){
                makeArrowDance();
            }
        }

        private void menuChangeState() {
            if (expandView) {
                imgArrowDown.setRotation(ROTATED_POSITION);
            } else {
                imgArrowDown.setRotation(INITIAL_POSITION);
            }

            notifyDataSetChanged();
        }

        @OnClick(R.id.btnSubmit)
        void onSubmitClick() {
            String text = edtMessage.getText()
                                    .toString()
                                    .trim();
            if (StringUtil.isEmpty(text)) {
                tvPlaceHolder.setAnimation(null);
                etMessageError.setVisibility(View.VISIBLE);
                tvPlaceHolder.setVisibility(GONE);


                return;
            }
            if (onmenuClick != null && !entranceLock.isClickContinuous()) {
                onmenuClick.onChatbotSubmit(text);
            }
            if(!NetworkUtil.getNetworkStatus(context)){
                onmenuClick.showDialogNoConnection();
            }else {
              //  edtMessage.setText("");
            }
        }

        @OnClick(R.id.btnCancel)
        void onCancelClick() {
            if (!entranceLock.isClickContinuous()) {
                resetPlaceHolder();
                tvPlaceHolder.setVisibility(View.VISIBLE);
                etMessageError.setVisibility(GONE);
                rotate(0);
            }
        }

        @OnTouch({R.id.etMessage})
        boolean onInputTouchEmail(final View v,
                                  final MotionEvent event) {
            tvPlaceHolder.setAnimation(null);
            tvPlaceHolder.setVisibility(View.GONE);
            etMessageError.setVisibility(GONE);

            return false;
        }

        public void onMenuClick() {

        }

        public void clearInputText() {
            edtMessage.setText("");
        }

    }

}
