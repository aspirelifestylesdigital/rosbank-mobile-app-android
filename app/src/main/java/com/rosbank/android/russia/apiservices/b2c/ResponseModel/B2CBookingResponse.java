package com.rosbank.android.russia.apiservices.b2c.ResponseModel;

import com.google.gson.annotations.Expose;

import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;

/**
 * Created by anh.tran on 11/4/2016.
 */

public class B2CBookingResponse extends B2CBaseResponse {
    @Expose
    private String transactionId;
    //@Expose
   // private Status status;

//    public B2CBookingResponse.Status getStatus() {
//        return status;
//    }
//
//    public void setStatus(B2CBookingResponse.Status status) {
//        this.status = status;
//    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

//    @Override
//    public boolean isSuccess(){
//        if(status!=null) {
//            return status.getSuccess();
//        } else  {
//            return super.isSuccess();
//        }
//    }
//
//    @Override
//    public String getMessage(){
//        if(status!=null) {
//            return status.getMessage();
//        } else {
//            return super.getMessage();
//        }
//    }

    public class Status {
        @Expose
        private Boolean success;
        @Expose

        private String message;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Boolean getSuccess() {
            return success;
        }

        public void setSuccess(Boolean success) {
            this.success = success;
        }
    }
}
