package com.rosbank.android.russia.apiservices;

import com.rosbank.android.russia.apiservices.ResponseModel.GetExperienceGourmetDetailResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.ApiProviderClient;
import com.rosbank.android.russia.model.UserItem;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Callback;

/**
 * chau.nguyen
*/
public class WSGetExperienceGourmetDetail extends ApiProviderClient<GetExperienceGourmetDetailResponse> {

    String RestaurantId;

    public WSGetExperienceGourmetDetail(String restaurantId) {
        RestaurantId = restaurantId;
    }

    @Override
    public void run(Callback<GetExperienceGourmetDetailResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }

    @Override
    protected boolean isUseAuthenticateInApi() {
        return true;
    }

    @Override
    public void runApi() {

        String id = UserItem.getLoginedId();
        serviceInterface.getExperiencesGourmetDetail(RestaurantId,id).enqueue(callback);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("User-Agent", "Retrofit-Sample-App");
        return headers;
    }

}
