package com.rosbank.android.russia.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.LinkMovementMethod;
import android.text.method.PasswordTransformationMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.activitys.HomeActivity;
import com.rosbank.android.russia.activitys.LoginAcitivy;
import com.rosbank.android.russia.apiservices.ResponseModel.SignUpResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CICallback;
import com.rosbank.android.russia.apiservices.b2c.B2CWSRegistration;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.B2CRegistrationRequest;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.model.UserItem;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.EntranceLock;
import com.rosbank.android.russia.utils.FontUtils;
import com.rosbank.android.russia.utils.NetworkUtil;
import com.rosbank.android.russia.utils.StringUtil;
import com.rosbank.android.russia.widgets.CustomErrorView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;
import static com.rosbank.android.russia.application.AppConstant.*;

/**
 * Created by anh.trinh on 9/14/2016.
 */
public class SignUpFragment
        extends BaseFragment
        implements SelectionFragment.SelectionCallback,
        Callback, B2CICallback {
    /*@BindView(R.id.toolbar)
    Toolbar mToolbar;*/
    @BindView(R.id.ic_back)
    ImageView mIcBack;
    @BindView(R.id.tv_tool_bar_title)
    TextView mTvToolBarTitle;
    @BindView(R.id.tv_register)
    TextView mTvRegister;
    @BindView(R.id.tv_salutation)
    TextView mTvSalutation;
    @BindView(R.id.tv_mandatory)
    TextView mTvMandatory;
    @BindView(R.id.layout_salutation)
    RelativeLayout mLayoutSalutation;
    @BindView(R.id.tv_selected_salutation)
    TextView mTvSelectedSalutation;
    @BindView(R.id.tv_title_first_name)
    TextView mTvTitleFirstName;
    @BindView(R.id.edt_first_name)
    EditText mEdtFirstName;
    @BindView(R.id.tv_title_last_name)
    TextView mTvTitleLastName;
    @BindView(R.id.edt_last_name)
    EditText mEdtLastName;
    @BindView(R.id.tv_mobile_no)
    TextView mTvMobileNo;
    @BindView(R.id.tv_country_no)
    TextView mTvCountryNo;
    @BindView(R.id.edt_phone_number)
    EditText mEdtPhoneNumber;
    @BindView(R.id.layout_mobile)
    RelativeLayout mLayoutMobile;
    @BindView(R.id.tv_title_zip_code)
    TextView mTvZipCode;
    @BindView(R.id.tv_we_will_need)
    TextView mTvWeWillNeed;
    @BindView(R.id.tv_title_email)
    TextView mTvTitleEmail;
    @BindView(R.id.edt_email)
    EditText mEdtEmail;
    @BindView(R.id.tv_the_email_address)
    TextView mTvTheEmailAddress;
    @BindView(R.id.tv_title_password)
    TextView mTvTitlePassword;
    @BindView(R.id.edt_password)
    EditText mEdtPassword;
    @BindView(R.id.tv_note_rule_pass)
    TextView mTvNoteRulePass;
    @BindView(R.id.tv_title_retype_pass)
    TextView mTvTitleRetypePass;
    @BindView(R.id.edt_retype_pass)
    EditText mEdtRetypePass;
    @BindView(R.id.cb_read_term_of_service)
    CheckBox mCbReadTermOfService;
    @BindView(R.id.btn_cancel)
    Button mBtnCancel;
    @BindView(R.id.btn_submit)
    Button mBtnSubmit;
    @BindView(R.id.layout_bottom_button)
    LinearLayout mLayoutBottomButton;
    @BindView(R.id.container)
    LinearLayout mLayoutContainer;
    @BindView(R.id.scrollView)
    ScrollView mScrollView;
    @BindView(R.id.salutation_error)
    CustomErrorView mSalutationError;
    @BindView(R.id.tv_title_last_six_diits)
    TextView mTvTitleLastSixDiits;
    @BindView(R.id.edt_last_six_digits)
    EditText mEdtLastSixDigits;
    @BindView(R.id.tv_title_sign_up_please_verify)
    TextView mTvTitleSignUpPleaseVerify;
    @BindView(R.id.layout_selected_salutation)
    RelativeLayout mLayoutSelectedSalutation;
    @BindView(R.id.first_name_error)
    CustomErrorView mFirstNameError;
    @BindView(R.id.layout_first_name)
    RelativeLayout mLayoutFirstName;
    @BindView(R.id.last_name_error)
    CustomErrorView mLastNameError;
    @BindView(R.id.layout_last_name)
    RelativeLayout mLayoutLastName;
    @BindView(R.id.phone_error)
    CustomErrorView mPhoneError;
    @BindView(R.id.email_error)
    CustomErrorView mEmailError;
    @BindView(R.id.layout_email)
    RelativeLayout mLayoutEmail;
    @BindView(R.id.last_six_digits_error)
    CustomErrorView mLastSixDigitsError;
    @BindView(R.id.layout_last_six_digits)
    RelativeLayout mLayoutLastSixDigits;
    @BindView(R.id.password_error)
    CustomErrorView mPasswordError;
    @BindView(R.id.layout_password)
    RelativeLayout mLayoutPassword;
    @BindView(R.id.retype_pass_error)
    CustomErrorView mRetypePassError;
    @BindView(R.id.layout_rety_pass)
    RelativeLayout mLayoutRetyPass;
    @BindView(R.id.cbShowPassword)
    CheckBox cbShowPassword;
    @BindView(R.id.layout_zip_code)
    RelativeLayout mLayoutZipCode;
    @BindView(R.id.zip_code_error)
    CustomErrorView mZipCodeErrorView;
    @BindView(R.id.edt_zip_code)
    EditText mEdtZipCode;
    @BindView(R.id.tv_term_of_service)
    TextView mTvTermOfService;
    @BindView(R.id.tv_title_entry_code)
    TextView mTvEntryCodeTitle;
    @BindView(R.id.layout_entry_code)
    RelativeLayout mLayoutEntryCode;
    @BindView(R.id.edt_entry_code)
    EditText mEdtEntryCode;
    @BindView(R.id.entry_code_error)
    CustomErrorView mEntryCodeError;

    String SalutationSelected = "";
    String CountryCodeSelected = "";
    boolean isCapitalizeText = false;
    EntranceLock entranceLock = new EntranceLock();
    boolean isFieldRequired, isPasswordIncorrectFormat, isPasswordNotMatch, isEmailIncorrectFormat,
            isMobileIncorrectFormat, isDigitCardInvalid, isEntryCodeIncorrect;

    @Override
    protected int layoutId() {
        return R.layout.fragment_sign_up;
    }

    @Override
    protected void initView() {
        mTvToolBarTitle.setText(getString(R.string.text_title_sign_up));

        String s = getString(R.string.text_sign_up_terms_of_service);
        String termAndCondition = getString(R.string.text_terms_and_conditions);
        int termStart = s.indexOf(termAndCondition);
        int termEnd = termStart + termAndCondition.length();
        String privacyPolicy = getString(R.string.text_privacy_policy);
        int privacyStart = s.indexOf(privacyPolicy);
        int privacyEnd = privacyStart + privacyPolicy.length();

        SpannableString ss = new SpannableString(s);
        ClickableSpan span1 = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                ((LoginAcitivy)getActivity()).showWebStatic(getString(R.string.text_title_terms_of_use), getString(R.string.text_path_terms_of_use));
            }

            @Override
            public void updateDrawState(final TextPaint textPaint) {
                textPaint.setColor(getResources().getColor(R.color.app_color_light_bg));
                textPaint.setUnderlineText(true);
            }
        };

        ClickableSpan span2 = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                ((LoginAcitivy)getActivity()).showWebStatic(getString(R.string.text_title_privacy_policy), getString(R.string.text_path_privacy_policy));
            }

            @Override
            public void updateDrawState(final TextPaint textPaint) {
                textPaint.setColor(getResources().getColor(R.color.app_color_light_bg));
                textPaint.setUnderlineText(true);
            }
        };

        ss.setSpan(span1, termStart, termEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(span2, privacyStart, privacyEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        mTvTermOfService.setText(ss);
        mTvTermOfService.setMovementMethod(LinkMovementMethod.getInstance());

        cbShowPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (!isChecked) {
                    // show password
                    mEdtPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    mEdtRetypePass.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    // hide password
                    mEdtPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    mEdtRetypePass.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
                mEdtPassword.setSelection(mEdtPassword.getText().length());
                mEdtRetypePass.setSelection(mEdtRetypePass.getText().length());
            }
        });
        CommonUtils.setFontForViewRecursive(mLayoutContainer,
                FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
        CommonUtils.setFontForViewRecursive(mTvToolBarTitle,
                FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        CommonUtils.setFontForViewRecursive(mBtnSubmit,
                FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        CommonUtils.setFontForViewRecursive(mBtnCancel,
                FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        CommonUtils.setFontForViewList(FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD, cbShowPassword);

        mSalutationError.fillData(getString(R.string.text_sign_up_error_required_field));
        mFirstNameError.fillData(getString(R.string.text_sign_up_error_required_field));
        mLastNameError.fillData(getString(R.string.text_sign_up_error_required_field));
        mPhoneError.fillData(getString(R.string.text_sign_up_error_required_field));
        mEmailError.fillData(getString(R.string.text_sign_up_error_required_field));
        mLastSixDigitsError.fillData(getString(R.string.text_sign_up_error_required_field));
        mPasswordError.fillData(getString(R.string.text_sign_up_error_required_field));
        mRetypePassError.fillData(getString(R.string.text_sign_up_error_required_field));
        mZipCodeErrorView.fillData(getString(R.string.text_sign_up_error_required_field));
        mEntryCodeError.fillData(getString(R.string.text_sign_up_error_required_field));

        mEdtFirstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                Log.d("ThuNguyen", "afterTextChanged");
                StringUtil.makeFirstLetterUpperCase(mEdtFirstName);
            }
        });
        mEdtLastName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                Log.d("ThuNguyen", "afterTextChanged");
                StringUtil.makeFirstLetterUpperCase(mEdtLastName);
            }
        });
        // Make country code be default as Russian
        CountryCodeSelected = getString(R.string.russian_country_code);
        mTvCountryNo.setText("+" + getCountryCode(CountryCodeSelected));
        mTvCountryNo.setTextColor(ContextCompat.getColor(getContext(), R.color.text_color_active));
    }

    @Override
    protected void bindData() {

    }

    @Override
    public boolean onBack() {

        return false;
    }

    @Override
    public void onBackPress() {
        super.onBackPress();
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater,
                container,
                savedInstanceState);
        ButterKnife.bind(this,
                rootView);
        return rootView;
    }

    @OnClick({R.id.ic_back,
            R.id.layout_selected_salutation,
            R.id.tv_country_no,
            R.id.cb_read_term_of_service,
            R.id.btn_cancel,
            R.id.btn_submit})
    public void onClick(View view) {
        Activity activity = getActivity();
        if (!entranceLock.isClickContinuous()) {
            switch (view.getId()) {
                case R.id.ic_back:
                    onBackPress();
                    break;
                case R.id.layout_selected_salutation:
                    mSalutationError.setVisibility(GONE);
                    Bundle bundle = new Bundle();
                    bundle.putString(SelectionFragment.PRE_SELECTION_TYPE,
                            SelectionFragment.PRE_SELECTION_SALUTATION);
                    bundle.putString(SelectionFragment.PRE_SELECTION_DATA, SalutationSelected);
                    SelectionFragment fragment = new SelectionFragment();
                    fragment.setSelectionCallBack(this);
                    fragment.setArguments(bundle);
                    pushFragment(fragment, true, true);

                    break;
                case R.id.tv_country_no:
                    mPhoneError.setVisibility(GONE);
                    Bundle bundle1 = new Bundle();
                    bundle1.putString(SelectionFragment.PRE_SELECTION_TYPE,
                            SelectionFragment.PRE_SELECTION_COUNTRY_CODE);
                    bundle1.putString(SelectionFragment.PRE_SELECTION_DATA, CountryCodeSelected);
                    SelectionFragment fragment1 = new SelectionFragment();
                    fragment1.setSelectionCallBack(this);
                    fragment1.setArguments(bundle1);
                    pushFragment(fragment1, true, true);

                    break;
                case R.id.cb_read_term_of_service:
                    break;
                case R.id.btn_cancel:
                    onBackPress();
                    break;
                case R.id.btn_submit:
                    processSubmit();
                    break;
            }
        }
    }

    @OnTouch({R.id.edt_email, R.id.edt_first_name, R.id.edt_last_name, R.id.edt_last_six_digits
            , R.id.edt_phone_number, R.id.edt_password, R.id.edt_retype_pass, R.id.edt_zip_code, R.id.edt_entry_code})
    boolean onInputTouch(final View v,
                         final MotionEvent event) {

        switch (v.getId()) {
            case R.id.edt_email:
                mEmailError.setVisibility(GONE);
                break;
            case R.id.edt_first_name:
                mFirstNameError.setVisibility(GONE);
                break;
            case R.id.edt_last_name:
                mLastNameError.setVisibility(GONE);
                break;
            case R.id.edt_last_six_digits:
                mLastSixDigitsError.setVisibility(GONE);
                break;
            case R.id.edt_phone_number:
                mPhoneError.setVisibility(GONE);
                break;
            case R.id.edt_password:
                mPasswordError.setVisibility(GONE);
                break;
            case R.id.edt_retype_pass:
                mRetypePassError.setVisibility(GONE);
                break;
            case R.id.edt_zip_code:
                mZipCodeErrorView.setVisibility(GONE);
                break;
            case R.id.edt_entry_code:
                mEntryCodeError.setVisibility(GONE);
                break;
        }
        return false;
    }

    @Override
    public void onFinishSelection(final Bundle bundle) {

        String typeSelection = bundle.getString(SelectionFragment.PRE_SELECTION_TYPE,
                "");

        if (typeSelection.equals(SelectionFragment.PRE_SELECTION_SALUTATION)) {
            SalutationSelected = bundle.getString(SelectionFragment.PRE_SELECTION_DATA,
                    "");
            if (!TextUtils.isEmpty(SalutationSelected)) {
                mTvSelectedSalutation.setText(SalutationSelected);
                mTvSelectedSalutation.setTextColor(ContextCompat.getColor(getContext(), R.color.text_color_active));
            } else {
                mTvSelectedSalutation.setTextColor(ContextCompat.getColor(getContext(), R.color.hint_color));
                mTvSelectedSalutation.setText(R.string.text_sign_up_please_select_salutation);
            }
        }
        if (typeSelection.equals(SelectionFragment.PRE_SELECTION_COUNTRY_CODE)) {
            CountryCodeSelected = bundle.getString(SelectionFragment.PRE_SELECTION_DATA,
                    "");
            if (!TextUtils.isEmpty(CountryCodeSelected)) {
                mTvCountryNo.setText("+" + getCountryCode(CountryCodeSelected));
                mTvCountryNo.setTextColor(ContextCompat.getColor(getContext(), R.color.text_color_active));
            } else {
                mTvCountryNo.setText(R.string.text_sign_up_cc);
                mTvCountryNo.setTextColor(ContextCompat.getColor(getContext(), R.color.hint_color));
            }
        }

    }

    private String getCountryCode(String countryCode) {
        if (countryCode == null) {
            return "";
        } else {
            return countryCode.substring(countryCode.lastIndexOf("(") + 1,
                    countryCode.lastIndexOf(")"));
        }
    }

    private boolean validationSignUpInfo() {
        // Reset variable checking
        isFieldRequired = isPasswordIncorrectFormat = isPasswordNotMatch = false;
        isEmailIncorrectFormat = isDigitCardInvalid = isMobileIncorrectFormat = false;
        isEntryCodeIncorrect = false;
        if (SalutationSelected.equals("")) {
            mSalutationError.setVisibility(View.VISIBLE);
            isFieldRequired = true;
        } else {
            mSalutationError.setVisibility(GONE);
        }
        if (mEdtFirstName.getText().toString().trim().equals("")) {
            mFirstNameError.setVisibility(View.VISIBLE);
            isFieldRequired = true;
        } else {
            mFirstNameError.setVisibility(GONE);
        }
        if (mEdtLastName.getText().toString().trim().equals("")) {
            mLastNameError.setVisibility(View.VISIBLE);
            isFieldRequired = true;
        } else {
            mLastNameError.setVisibility(GONE);
        }
        if (mEdtEmail.getText().toString().trim().equals("")) {
            mEmailError.fillData(getString(R.string.text_sign_up_error_required_field));
            mEmailError.setVisibility(View.VISIBLE);
            isFieldRequired = true;
        } else if (!CommonUtils.emailValidator(mEdtEmail.getText().toString().trim())) {
            isEmailIncorrectFormat = true;
            mEmailError.fillData(getString(R.string.text_sign_up_error_email));
            mEmailError.setVisibility(View.VISIBLE);
        } else {
            mEmailError.setVisibility(GONE);
        }

        if (mTvCountryNo.getText().toString().trim().equals(getString(R.string.text_sign_up_cc))) {
            //mPhoneError.fillData(getString(R.string.text_sign_up_error_country_code));
            mPhoneError.setVisibility(View.VISIBLE);
            isFieldRequired = true;
        } else if (mEdtPhoneNumber.getText().toString().trim().equals("")) {
            //mPhoneError.fillData(getString(R.string.text_sign_up_error_phone));
            mPhoneError.setVisibility(View.VISIBLE);
            isFieldRequired = true;
        } else {
            mPhoneError.setVisibility(GONE);
        }
        if (mEdtEntryCode.getText().toString().trim().equals("")) {
            mEntryCodeError.setVisibility(View.VISIBLE);
            isFieldRequired = true;
        }else if(!mEdtEntryCode.getText().toString().trim().equals(CLIENT_CODE_REGULAR) &&
                !mEdtEntryCode.getText().toString().trim().equals(CLIENT_CODE_PRIVATE)){
            isEntryCodeIncorrect = true;
            mEntryCodeError.fillData(getString(R.string.text_sign_in_invalid_entry_code));
            mEntryCodeError.setVisibility(View.VISIBLE);
    }else {
            mEntryCodeError.setVisibility(GONE);
        }



       /* if(mEdtLastSixDigits.getText().toString().trim().equals("")){
            mLastSixDigitsError.setVisibility(View.VISIBLE);
            isFieldRequired = true;
        }else if(!mEdtLastSixDigits.getText().toString().equalsIgnoreCase(BuildConfig.B2C_BIN)){
            mLastSixDigitsError.setVisibility(View.VISIBLE);
            mLastSixDigitsError.fillData(getString(R.string.text_sign_up_error_last_six_digits_of_your_card));
            isDigitCardInvalid = true;
        }else{
            mLastSixDigitsError.setVisibility(GONE);
        }*/

        if (mEdtPassword.getText().toString().trim().length() == 0) {
            mPasswordError.setVisibility(View.VISIBLE);
            mPasswordError.fillData(getString(R.string.text_sign_up_error_required_field));
            //mPasswordError.fillData(getString(R.string.text_sign_up_error_pass));
            isFieldRequired = true;
        } else if (!CommonUtils.secretValidator(mEdtPassword.getText().toString())) {
            mPasswordError.setVisibility(View.VISIBLE);
            mPasswordError.fillData(getString(R.string.text_sign_up_error_pass));
            isPasswordIncorrectFormat = true;
        } else {
            mPasswordError.setVisibility(GONE);
        }

        if (mEdtRetypePass.getText().toString().trim().length() == 0) {
            mRetypePassError.setVisibility(View.VISIBLE);
            mRetypePassError.fillData(getString(R.string.text_sign_up_error_required_field));
            isFieldRequired = true;
        } else if (!CommonUtils.secretValidator(mEdtRetypePass.getText().toString())) {
            mRetypePassError.setVisibility(View.VISIBLE);
            mRetypePassError.fillData(getString(R.string.text_sign_up_error_pass));
            isPasswordIncorrectFormat = true;
        } else if (!mEdtRetypePass.getText().toString().equals(mEdtPassword.getText().toString())) {
            mRetypePassError.setVisibility(View.VISIBLE);
            mRetypePassError.fillData(getString(R.string.text_sign_up_error_do_not_match_pass));
            if (mPasswordError.getVisibility() == GONE) {
                mPasswordError.setVisibility(View.VISIBLE);
                mPasswordError.fillData(getString(R.string.text_sign_up_error_do_not_match_pass));
            }
            isPasswordNotMatch = true;
        } else {
            mRetypePassError.setVisibility(GONE);
        }
        return !(isFieldRequired || isPasswordNotMatch || isPasswordIncorrectFormat
                || isEmailIncorrectFormat || isDigitCardInvalid || isMobileIncorrectFormat||isEntryCodeIncorrect);
    }

    private void processSubmit() {
        if (validationSignUpInfo()) {
            if (mCbReadTermOfService.isChecked()) {
                if (!NetworkUtil.getNetworkStatus(getContext())) {
                    showDialogMessage(getString(R.string.text_title_dialog_error),
                            getString(R.string.text_message_dialog_error_no_internet));
                } else {
                    showDialogProgress();
                    B2CRegistrationRequest registrationRequest = new B2CRegistrationRequest();
                    registrationRequest.build(mEdtEmail.getText().toString().trim(),
                            mEdtFirstName.getText().toString().trim(),
                            mEdtLastName.getText().toString().trim(),
                            mTvCountryNo.getText().toString().trim() + mEdtPhoneNumber.getText().toString().trim(),
                            mEdtPassword.getText().toString().trim(),
                            SalutationSelected,
                            mEdtZipCode.getText().toString().trim(),
                            mEdtEntryCode.getText().toString().trim());
                    B2CWSRegistration b2CWSRegistration = new B2CWSRegistration(this);
                    b2CWSRegistration.setRegistrationRequest(registrationRequest);
                    b2CWSRegistration.run(null);
                }
            } else {
                Activity activity = getActivity();
                if (activity instanceof LoginAcitivy) {
                    Bundle bundle = new Bundle();
                    bundle.putString(ErrorFragment.PRE_ERROR_MESSAGE, getString(R.string.text_sign_up_error_message_unread_term_and_policy));
                    ErrorFragment fragment = new ErrorFragment();
                    fragment.setArguments(bundle);
                    pushFragment(fragment, true, true);
                }
            }
        }
    }



    @Override
    public void onResponse(final Call call,
                           final Response response) {
        if (response.body() instanceof SignUpResponse) {
            hideDialogProgress();
            if (getActivity() == null) {
                return;
            }

            SignUpResponse signUpResponse = ((SignUpResponse) response.body());
            int code = signUpResponse.getStatus();
            if (code == 200) {
                if (signUpResponse.getData() != null &&
                        signUpResponse.getData().getUserItem() != null &&
                        UserItem.isLoginSuccess(signUpResponse.getData()
                                .getUserItem())
                    /*signUpResponse.getData().getUserItem().getEmail() != null && signUpResponse.getData().getUserItem().getUserID() != null*/
                        ) {
                    /*SharedPreferencesUtils.setPreferences(AppConstant.USER_EMAIL_PRE,
                                                          signUpResponse.getData()
                                                                        .getUserItem()
                                                                        .getEmail());
                    SharedPreferencesUtils.setPreferences(AppConstant.USER_ID_PRE,
                                                          signUpResponse.getData()
                                                                        .getUserItem()
                                                                        .getUserID());*/
                    UserItem.savePreference(signUpResponse.getData()
                            .getUserItem(),mEdtEntryCode.getText().toString().trim());

                }
                Intent intent = new Intent(getActivity(),
                        HomeActivity.class);

                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

                getActivity().finish();
            } else {
//               showToast(((SignUpResponse)response.body()).getMessage());

            }
        }
    }

    @Override
    public void onFailure(final Call call,
                          final Throwable t) {
        hideDialogProgress();
        if (getActivity() == null) {
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putString(ErrorFragment.PRE_ERROR_MESSAGE,
                getString(R.string.text_sign_up_error_message));
        pushFragment(new ErrorFragment(),
                true,
                true);
    }

    @Override
    public void onB2CResponse(B2CBaseResponse response) {
        hideDialogProgress();
        if (getActivity() == null) {
            return;
        }
        if (response != null) {
            if (response.isSuccess()) {
                UserItem.saveClientCode(mEdtEntryCode.getText().toString().trim());
                Intent intent = new Intent(getActivity(),
                        HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

                getActivity().finish();
            } else {
//                showToast(response.getMessage());
                showDialogMessage("", getString(R.string.text_concierge_request_error));
            }
        } else {
            //showToast("User profile has been registered");
        }
    }

    @Override
    public void onB2CResponseOnList(List<B2CBaseResponse> responseList) {
        if (getActivity() == null) {
            return;
        }

    }

    @Override
    public void onB2CFailure(String errorMessage, String errorCode) {
        hideDialogProgress();
        if (getActivity() == null) {
            return;
        }
        if (!TextUtils.isEmpty(errorCode) && errorCode.equals("ENR68-2")) {
            showDialogMessage("", getString(R.string.text_sign_up_duplicate_email_error));
        } else {
            showDialogMessage("", getString(R.string.text_concierge_request_error));
        }
    }
}
