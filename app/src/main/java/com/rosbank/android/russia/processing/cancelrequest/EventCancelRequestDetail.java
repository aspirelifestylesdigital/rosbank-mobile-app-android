package com.rosbank.android.russia.processing.cancelrequest;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.rosbank.android.russia.apiservices.b2c.B2CWSGetRequestDetail;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.B2CUpsertConciergeRequestRequest;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CGetRecentRequestResponse;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.application.AppContext;
import com.rosbank.android.russia.model.CountryObject;
import com.rosbank.android.russia.model.concierge.EventBookingDetailData;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.DateTimeUtil;
import com.rosbank.android.russia.utils.SharedPreferencesUtils;

import android.text.TextUtils;
import android.view.View;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by ThuNguyen on 10/26/2016.
 */

public class EventCancelRequestDetail extends BaseCancelRequestDetail {
    private EventBookingDetailData eventBookingDetailData;

    public EventCancelRequestDetail() {
    }

    public EventCancelRequestDetail(View view) {
        super(view);
    }

    @Override
    public void getRequestDetail() {
        showProgressDialog();
        B2CWSGetRequestDetail b2CWSGetRequestDetail = new B2CWSGetRequestDetail(this);
        b2CWSGetRequestDetail.setValue(myRequestObject.getEpcCaseId(), myRequestObject.getBookingItemID());
        b2CWSGetRequestDetail.run(null);

    }

    @Override
    public B2CUpsertConciergeRequestRequest getB2CUpsertConciergeRequestRequest() {
        B2CUpsertConciergeRequestRequest upsertConciergeRequestRequest = new B2CUpsertConciergeRequestRequest();
        upsertConciergeRequestRequest.setFunctionality(AppConstant.CONCIERGE_FUNCTIONALITY_TYPE.ENTERTAINMENT.getValue());
        upsertConciergeRequestRequest.setRequestType(AppConstant.CONCIERGE_REQUEST_TYPE.EVENT.getValue());

        upsertConciergeRequestRequest.setEventDate(DateTimeUtil.shareInstance().formatTimeStamp(System.currentTimeMillis(), AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS));
        upsertConciergeRequestRequest.setDelivery(eventBookingDetailData.getEventDateTime());
        upsertConciergeRequestRequest.setEventName(eventBookingDetailData.getEventName());

        if (!TextUtils.isEmpty(eventBookingDetailData.getCity())) {
            upsertConciergeRequestRequest.setCity(eventBookingDetailData.getCity());
        }
        if (!TextUtils.isEmpty(eventBookingDetailData.getCountry())) {
            upsertConciergeRequestRequest.setCountry(CountryObject.getCountryObjectFromName(eventBookingDetailData.getCountry()).getCountryCode());
        }
        if (!TextUtils.isEmpty(eventBookingDetailData.getState())) {
            upsertConciergeRequestRequest.setState(eventBookingDetailData.getState());
        } else {
            upsertConciergeRequestRequest.setState(eventBookingDetailData.getCity());
        }
        upsertConciergeRequestRequest.setNumberOfAdults(String.valueOf(eventBookingDetailData.getNoOfTickets()));
        upsertConciergeRequestRequest.setNumberOfChildren("0");
        upsertConciergeRequestRequest.setEmail1((SharedPreferencesUtils.getPreferences(AppConstant.USER_EMAIL_PRE,
                "")));
        if (!TextUtils.isEmpty(eventBookingDetailData.getMobileNumber())) {
            upsertConciergeRequestRequest.setPhoneNumber(eventBookingDetailData.getMobileNumber());
        }
        upsertConciergeRequestRequest.setPrefResponse(eventBookingDetailData.getPrefResponse());

        if (eventBookingDetailData.getEventCategory() != null && !eventBookingDetailData.getEventCategory().isEmpty()){
            upsertConciergeRequestRequest.setSituation(eventBookingDetailData.getEventCategory());
        }else {
            upsertConciergeRequestRequest.setSituation("Book an Event");
        }

        if (myRequestObject != null) {
            upsertConciergeRequestRequest.setTransactionID(myRequestObject.getBookingItemID());
        }
        // Request details
        upsertConciergeRequestRequest.setRequestDetails(eventBookingDetailData.getRequestDetaiString());
        return upsertConciergeRequestRequest;
    }

    private String combineRequestDetails() {
        String requestDetails = "";
        if (!TextUtils.isEmpty(eventBookingDetailData.getConciergeRequestDetail()
                .getSpecialRequirement())) {
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_SPECIAL_REQ + eventBookingDetailData.getConciergeRequestDetail()
                    .getSpecialRequirement();
        }
        if (!TextUtils.isEmpty(eventBookingDetailData.getConciergeRequestDetail().getEventCategory())) {
            if (!TextUtils.isEmpty(requestDetails)) {
                requestDetails += " | ";
            }
            requestDetails +=
                    AppConstant.B2C_REQUEST_DETAIL_EVENT_CATEGORY + eventBookingDetailData.getConciergeRequestDetail().getEventCategory();
        }
        if (!TextUtils.isEmpty(eventBookingDetailData.getCountry())) {
            if (!TextUtils.isEmpty(requestDetails)) {
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_COUNTRY + eventBookingDetailData.getCountry();
        }
        if (!TextUtils.isEmpty(eventBookingDetailData.getReservationName())) {
            if (!TextUtils.isEmpty(requestDetails)) {
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_RESERVATION_NAME + eventBookingDetailData.getReservationName();
        }
        if (!TextUtils.isEmpty(requestDetails)) {
            requestDetails += " | ";
        }
        requestDetails += AppConstant.B2C_REQUEST_DETAIL_NO_TICKET + eventBookingDetailData.getNoOfTickets();

        return requestDetails;
        /*String requestDetails = "";
        if(!TextUtils.isEmpty(eventBookingDetailData.getConciergeRequestDetail()
                                                          .getSpecialRequirement())){
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_SPECIAL_REQ + eventBookingDetailData.getConciergeRequestDetail()
                                                                                                 .getSpecialRequirement();
        }
        if(!TextUtils.isEmpty(eventBookingDetailData.getEventName())){
            if(!TextUtils.isEmpty(requestDetails)){
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_EVENT_NAME + eventBookingDetailData.getEventName();
        }

        if (!TextUtils.isEmpty(requestDetails)) {
            requestDetails += " | ";
        }
        requestDetails += AppConstant.B2C_REQUEST_DETAIL_NO_TICKET + eventBookingDetailData.getNoOfTickets();

        if(TextUtils.isEmpty(eventBookingDetailData.getEventName())) {
            requestDetails += " | " + AppConstant.B2C_REQUEST_DETAIL_PREF_DATE + DateTimeUtil.shareInstance().formatTimeStamp(eventBookingDetailData.getPreferredDateEpoc(), AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS);
        }

        return requestDetails;*/
    }

    @Override
    protected void updateUI() {
        super.updateUI();
        long epochCreatedDate = eventBookingDetailData.getCreateDateEpoc();
        tvRequestedDate.setText(DateTimeUtil.shareInstance()
                .formatTimeStamp(epochCreatedDate,
                        "dd MMMM yyyy"));
        tvRequestedTime.setText(DateTimeUtil.shareInstance()
                .formatTimeStamp(epochCreatedDate,
                        "hh:mm aa"));
        // Request type
        tvRequestType.setText(AppContext.getSharedInstance()
                .getResources()
                .getString(R.string.text_cancel_request_title_event_request));
        // Request detail header
       /* tvRequestDetailHeader.setText(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_event_request_detail));
        if(eventBookingDetailData != null){
            // Case Id
            tvCaseId.setText(eventBookingDetailData.getBookingItemID());

            // Guests
            if(!TextUtils.isEmpty(eventBookingDetailData.getCity())) {
                addRequestDetailItem(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_city),
                                     eventBookingDetailData.getCity());
            }

            // Room and Suites
            String roomSuitesObj = eventBookingDetailData.getEventDateTime();

            if(roomSuitesObj != null && !TextUtils.isEmpty(roomSuitesObj)){
                addRequestDetailItem(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_reservation_date), roomSuitesObj);
            }*/

        // Request name
        if (CommonUtils.isStringValid(eventBookingDetailData.getEventName()) && CommonUtils.isStringValid(eventBookingDetailData.getCity())) {
            tvRequestName.setText(eventBookingDetailData.getEventName() + ", " + eventBookingDetailData.getCity());
        } else {
            tvRequestName.setText("NONE");
        }
        // Request detail header
        tvRequestDetailHeader.setText(AppContext.getSharedInstance()
                .getResources()
                .getString(R.string.text_cancel_request_event_request_detail));
        if (eventBookingDetailData != null) {
            // Case Id
            tvCaseId.setText(eventBookingDetailData.getBookingItemID());
            if (eventBookingDetailData.getEventName() != null && !TextUtils.isEmpty(eventBookingDetailData.getEventName())) {
                addRequestDetailItem(AppContext.getSharedInstance()
                                .getResources()
                                .getString(R.string.text_cancel_request_event_request_event_name),
                        eventBookingDetailData.getEventName());
            }
            // Guests
            if (!TextUtils.isEmpty(eventBookingDetailData.getCity())) {
                addRequestDetailItem(AppContext.getSharedInstance()
                                .getResources()
                                .getString(R.string.text_cancel_request_city),
                        eventBookingDetailData.getCity());
            }

            // Room and Suites
            String eventDateTime = eventBookingDetailData.getEventDateTime();
            long date = eventBookingDetailData.getEventDateEpoc();
            if (date > 0) {
                addRequestDetailItem(AppContext.getSharedInstance()
                                .getResources()
                                .getString(R.string.text_cancel_request_reservation_date),
                        DateTimeUtil.shareInstance()
                                .formatTimeStamp(date,
                                        "dd MMMM yyyy"));
            }
           /* if(eventDateTime != null && !TextUtils.isEmpty(eventDateTime)){
                addRequestDetailItem(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_reservation_date), DateTimeUtil.shareInstance().convertTime(eventDateTime, AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS, AppConstant.DATE_FORMAT_CONCIERGE_BOOKING));
            }*/
        }

    }

    @Override
    public void onResponse(Call call, Response response) {

    }

    @Override
    public void onFailure(Call call, Throwable t) {

    }

    @Override
    public void onB2CResponse(final B2CBaseResponse response) {
        hideProgressDialog();
        if (response instanceof B2CGetRecentRequestResponse) {
            eventBookingDetailData = new EventBookingDetailData((B2CGetRecentRequestResponse) response);
            updateUI();
        }
    }

    @Override
    public void onB2CResponseOnList(final List<B2CBaseResponse> responseList) {

    }

    @Override
    public void onB2CFailure(final String errorMessage,
                             final String errorCode) {
        hideProgressDialog();
    }
}
