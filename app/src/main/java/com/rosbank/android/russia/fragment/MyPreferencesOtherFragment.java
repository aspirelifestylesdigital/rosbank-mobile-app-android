package com.rosbank.android.russia.fragment;

import android.graphics.Typeface;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.activitys.HomeActivity;
import com.rosbank.android.russia.apiservices.ResponseModel.UpdateOtherPreferencesResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CICallback;
import com.rosbank.android.russia.apiservices.b2c.B2CWSGetPreference;
import com.rosbank.android.russia.apiservices.b2c.B2CWSUpsertPreferenceRequest;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.B2CUpsertPreferenceRequest;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.preference.PreferenceDetailOtherRequest;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.preference.PreferenceDetailRequest;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.model.preference.OtherPreferenceDetailData;
import com.rosbank.android.russia.model.preference.PreferenceData;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.EntranceLock;
import com.rosbank.android.russia.utils.FontUtils;
import com.rosbank.android.russia.utils.StringUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nga.nguyent on 9/27/2016.
 */
public class MyPreferencesOtherFragment extends BaseFragment implements Callback{

    @BindView(R.id.tvTitle)
    protected TextView tvTitle;

    @BindView(R.id.tvOtherTitle)
    protected TextView tvOtherTitle;
    @BindView(R.id.edtOtherContent)
    protected EditText edtOtherContent;
    @BindView(R.id.btnSave)
    Button btnSave;
    @BindView(R.id.btnCancel)
    Button btnCancel;
    @BindView(R.id.container)
    LinearLayout mContainer;
    String cuisineOther;
    OtherPreferenceDetailData otherPreferenceDetailData;
    EntranceLock entranceLock = new EntranceLock();

    @Override
    protected int layoutId() {
        return R.layout.fragment_preference_other;
    }

    @Override
    protected void initView() {
        ButterKnife.bind(this, view);

        CommonUtils.setFontForViewRecursive(mContainer,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
        CommonUtils.setFontForViewRecursive(tvTitle,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        CommonUtils.setFontForViewRecursive(btnSave,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        CommonUtils.setFontForViewRecursive(btnCancel,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        edtOtherContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                cuisineOther = s.toString();
                //Logger.sout(golfCourseNameStr);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        showDialogProgress();
        B2CWSGetPreference b2CWSGetPreference = new B2CWSGetPreference(getPreferenceCallback);
        b2CWSGetPreference.run(null);
    }
    private void updateUI(){
        if(otherPreferenceDetailData != null){
            edtOtherContent.setText(otherPreferenceDetailData.getComment());
        }
    }
    @Override
    protected void bindData() {
        updateCuisinePreferencs();
    }

    @Override
    protected boolean onBack() {
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showLogoApp(false);
            ((HomeActivity) getActivity()).setTitle(getString(R.string.title_mypreference));
        }
        hideRightText();
        showToolbarMenuIcon();

    }

    @OnClick({R.id.btnSave, R.id.btnCancel})
    public void onClick(View v){
        switch (v.getId()){
            case R.id.btnSave:
                //showToast("Save");
                CommonUtils.hideSoftKeyboard(getActivity());
                if(!entranceLock.isClickContinuous()) {
                    saveGolf();
                }
                break;
            case R.id.btnCancel:
                //showToast("Cancel");
                if(!entranceLock.isClickContinuous()) {
                    onBackPress();
                }
                break;
        }
    }

    public void setOther(String other){
        if(StringUtil.isEmpty(other))
            other = "";

        this.cuisineOther = other.trim();
    }

    private void updateCuisinePreferencs(){
        //
        edtOtherContent.setText(cuisineOther);
    }

    private void saveGolf(){

        /*if(!UserItem.isLogined())
        {
            showToast(getString(R.string.text_message_need_login_update_gourmet));
            return;
        }

        showDialogProgress();

        String textOther = edtOtherContent.getText().toString();
        String userId = UserItem.getLoginedId();

        UpdategOtherPreferencesRequest request = new UpdategOtherPreferencesRequest();
        if(!StringUtil.isEmpty(textOther))
            request.setOthers(textOther);

        request.setUserID(userId);

        WSUpdateOtherPreference ws = new WSUpdateOtherPreference();
        ws.setRequest(request);
        ws.run(this);*/
        AppConstant.PREFERENCE_EDIT_TYPE preferenceEditType = otherPreferenceDetailData != null ? AppConstant.PREFERENCE_EDIT_TYPE.UPDATE : AppConstant.PREFERENCE_EDIT_TYPE.ADD;
        PreferenceDetailRequest preferenceDetailRequest = new PreferenceDetailOtherRequest("false");
        switch (preferenceEditType){
            case ADD:
                break;
            case UPDATE:
                preferenceDetailRequest.setMyPreferencesId(otherPreferenceDetailData.getPreferenceId());
                break;
        }
        preferenceDetailRequest.fillData(edtOtherContent.getText().toString().trim());

        B2CUpsertPreferenceRequest upsertPreferenceRequest = new B2CUpsertPreferenceRequest();
        upsertPreferenceRequest.setPreferenceDetails(preferenceDetailRequest);
        B2CWSUpsertPreferenceRequest b2CWSUpsertPreferenceRequest = new B2CWSUpsertPreferenceRequest(preferenceEditType, upsertPreferenceCallback);
        b2CWSUpsertPreferenceRequest.setRequest(upsertPreferenceRequest);
        b2CWSUpsertPreferenceRequest.run(null);
        showDialogProgress();
    }

    @Override
    public void onResponse(Call call, Response response) {
        if(response.body() instanceof UpdateOtherPreferencesResponse){
            UpdateOtherPreferencesResponse rsp = (UpdateOtherPreferencesResponse) response.body();
            if(rsp.isSuccess()){
                onBackPress();
            }
            else {
//                showToast(getString(R.string.text_message_update_gourmet_fail));
            }
        }
        hideDialogProgress();
    }

    @Override
    public void onFailure(Call call, Throwable t) {
        hideDialogProgress();
//        showToast(getString(R.string.text_message_update_gourmet_fail));
    }
    private B2CICallback getPreferenceCallback = new B2CICallback() {
        @Override
        public void onB2CResponse(B2CBaseResponse response) {
            hideDialogProgress();
        }

        @Override
        public void onB2CResponseOnList(List<B2CBaseResponse> responseList) {
            hideDialogProgress();
            otherPreferenceDetailData = (OtherPreferenceDetailData) PreferenceData.findFromList(responseList, AppConstant.PREFERENCE_TYPE.OTHER);
            updateUI();
        }

        @Override
        public void onB2CFailure(String errorMessage, String errorCode) {
            hideDialogProgress();
        }
    };

    private B2CICallback upsertPreferenceCallback = new B2CICallback() {
        @Override
        public void onB2CResponse(B2CBaseResponse response) {
            hideDialogProgress();
            if(response != null && response.isSuccess()){
                onBackPress();
            }else{
//                showToast(getString(R.string.text_server_error_message));
                showDialogMessage("", getString(R.string.text_concierge_request_error));
            }
        }

        @Override
        public void onB2CResponseOnList(List<B2CBaseResponse> responseList) {

        }

        @Override
        public void onB2CFailure(String errorMessage, String errorCode) {
            hideDialogProgress();
//            showToast(getString(R.string.text_server_error_message));
            showDialogMessage("", getString(R.string.text_concierge_request_error));
        }
    };
}
