package com.rosbank.android.russia.model;

import android.text.TextUtils;

import com.google.gson.annotations.Expose;

import java.util.List;

/**
 * Created by ThuNguyen on 10/26/2016.
 */

public class RestaurantRecommendationBookingDetailData {
    @Expose
    private KeyValueObject Occasion;
    @Expose
    private List<KeyValueObject> Cuisines;
    @Expose
    private String Country;
    @Expose
    private KeyValueObject Country4App;
    @Expose
    private String City;
    @Expose
    private String ReservationDate;
    @Expose
    private String EpochReservationDate;
    @Expose
    private String ReservationTime;
    @Expose
    private String MinimumPrice;
    @Expose
    private String MaximumPrice;
    @Expose
    private String NumberOfAdults;
    @Expose
    private String NumberOfKids;
    @Expose
    private String SpecialRequirements;
    @Expose
    private String AttachPhoto;
    @Expose
    private String AttachPhotoUrl;
    @Expose
    private String BookingId;
    @Expose
    private String UserID;
    @Expose
    private String BookingName;
    @Expose
    private String BookingItemId;
    @Expose
    private String GuestName;
    @Expose
    private String UploadPhoto;
    @Expose
    private String UploadPhotoUrl;
    @Expose
    private Boolean Phone;
    @Expose
    private Boolean Email;
    @Expose
    private Boolean Both;
    @Expose
    private String MobileNumber;
    @Expose
    private String EmailAddress;

    public KeyValueObject getOccasion() {
        return Occasion;
    }

    public void setOccasion(KeyValueObject occasion) {
        Occasion = occasion;
    }

    public List<KeyValueObject> getCuisines() {
        return Cuisines;
    }

    public void setCuisines(List<KeyValueObject> cuisines) {
        Cuisines = cuisines;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public KeyValueObject getCountry4App() {
        return Country4App;
    }

    public void setCountry4App(KeyValueObject country4App) {
        Country4App = country4App;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getReservationDate() {
        return ReservationDate;
    }

    public void setReservationDate(String reservationDate) {
        ReservationDate = reservationDate;
    }

    public String getEpochReservationDate() {
        return EpochReservationDate;
    }

    public void setEpochReservationDate(String epochReservationDate) {
        EpochReservationDate = epochReservationDate;
    }

    public String getReservationTime() {
        return ReservationTime;
    }

    public void setReservationTime(String reservationTime) {
        ReservationTime = reservationTime;
    }

    public String getMinimumPrice() {
        return MinimumPrice;
    }

    public void setMinimumPrice(String minimumPrice) {
        MinimumPrice = minimumPrice;
    }

    public String getMaximumPrice() {
        return MaximumPrice;
    }

    public void setMaximumPrice(String maximumPrice) {
        MaximumPrice = maximumPrice;
    }

    public String getNumberOfAdults() {
        return NumberOfAdults;
    }

    public void setNumberOfAdults(String numberOfAdults) {
        NumberOfAdults = numberOfAdults;
    }

    public String getNumberOfKids() {
        return NumberOfKids;
    }

    public void setNumberOfKids(String numberOfKids) {
        NumberOfKids = numberOfKids;
    }

    public String getSpecialRequirements() {
        return SpecialRequirements;
    }

    public void setSpecialRequirements(String specialRequirements) {
        SpecialRequirements = specialRequirements;
    }

    public String getAttachPhoto() {
        return AttachPhoto;
    }

    public void setAttachPhoto(String attachPhoto) {
        AttachPhoto = attachPhoto;
    }

    public String getAttachPhotoUrl() {
        return AttachPhotoUrl;
    }

    public void setAttachPhotoUrl(String attachPhotoUrl) {
        AttachPhotoUrl = attachPhotoUrl;
    }

    public String getBookingId() {
        return BookingId;
    }

    public void setBookingId(String bookingId) {
        BookingId = bookingId;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public String getBookingName() {
        return BookingName;
    }

    public void setBookingName(String bookingName) {
        BookingName = bookingName;
    }

    public String getBookingItemId() {
        return BookingItemId;
    }

    public void setBookingItemId(String bookingItemId) {
        BookingItemId = bookingItemId;
    }

    public String getGuestName() {
        return GuestName;
    }

    public void setGuestName(String guestName) {
        GuestName = guestName;
    }

    public String getUploadPhoto() {
        return UploadPhoto;
    }

    public void setUploadPhoto(String uploadPhoto) {
        UploadPhoto = uploadPhoto;
    }

    public String getUploadPhotoUrl() {
        return UploadPhotoUrl;
    }

    public void setUploadPhotoUrl(String uploadPhotoUrl) {
        UploadPhotoUrl = uploadPhotoUrl;
    }

    public Boolean getPhone() {
        return Phone;
    }

    public void setPhone(Boolean phone) {
        Phone = phone;
    }

    public Boolean getEmail() {
        return Email;
    }

    public void setEmail(Boolean email) {
        Email = email;
    }

    public Boolean getBoth() {
        return Both;
    }

    public void setBoth(Boolean both) {
        Both = both;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public String getEmailAddress() {
        return EmailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        EmailAddress = emailAddress;
    }

    public long getEpocReservationDateInMillis(){
        if(TextUtils.isEmpty(EpochReservationDate) || EpochReservationDate.equalsIgnoreCase("null")){
            return 0;
        }
        return (Long.parseLong(EpochReservationDate) * 1000);
    }
}
