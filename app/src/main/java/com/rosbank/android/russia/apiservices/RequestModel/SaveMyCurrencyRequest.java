package com.rosbank.android.russia.apiservices.RequestModel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;


public class SaveMyCurrencyRequest
        implements Parcelable {
    public String getUserID() {
        return UserID;
    }

    public void setUserID(final String userID) {
        UserID = userID;
    }

    @Expose
    private String UserID;
    @Expose
    private String CurrencyKey;

    public String getCurrencyKey() {
        return CurrencyKey;
    }

    public void setCurrencyKey(final String currencyKey) {
        CurrencyKey = currencyKey;
    }

    public String getCurrencyText() {
        return CurrencyText;
    }

    public void setCurrencyText(final String currencyText) {
        CurrencyText = currencyText;
    }

    @Expose
    private String CurrencyText;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel parcel,
                              final int i) {
        parcel.writeString(this.CurrencyKey);
        parcel.writeString(this.CurrencyText);

    }
    protected SaveMyCurrencyRequest(Parcel in) {
        this.CurrencyKey = in.readString();
        this.CurrencyText = in.readString();
    }
    public SaveMyCurrencyRequest() {
        this.CurrencyKey = "";
        this.CurrencyText = "";
    }

    public static final Creator<SaveMyCurrencyRequest> CREATOR = new Creator<SaveMyCurrencyRequest>() {
        @Override
        public SaveMyCurrencyRequest createFromParcel(Parcel source) {
            return new SaveMyCurrencyRequest(source);
        }

        @Override
        public SaveMyCurrencyRequest[] newArray(int size) {
            return new SaveMyCurrencyRequest[size];
        }
    };
}
