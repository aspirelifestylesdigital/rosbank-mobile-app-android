package com.rosbank.android.russia.helper;

/**
 * Copyright (C) 2015 CreateTrips
 * IClickableUrlSpanListener.java
 * CreateTrips
 *
 * @author nam.tran, 04/18/2015
 * @since v0.3
 */

import android.view.View;

/**
 * The interface of listener used for handling clickable URL in TextView.
 *
 * @author nam.tran, 04/18/2015
 * @since v0.3
 */
public interface IClickableUrlSpanListener {

    /**
     * Listener for click event of an URL.
     *
     * @param view
     *         The TextView contain displayed string.
     * @param url
     *         The URL which is being clicked.
     * @since v0.3
     */
    void onUrlClicked(final View view,
                      final String url);
}
