package com.rosbank.android.russia.fragment;

import com.rosbank.android.russia.BuildConfig;
import com.rosbank.android.russia.R;
import com.rosbank.android.russia.activitys.HomeActivity;
import com.rosbank.android.russia.adapter.ExperiencesDetailGourmetPagerAdapter;
import com.rosbank.android.russia.apiservices.ResponseModel.AddWishListResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.GetExperienceGourmetDetailResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.RemoveWishListResponse;
import com.rosbank.android.russia.apiservices.WSAddToWishList;
import com.rosbank.android.russia.apiservices.WSGetExperienceGourmetDetail;
import com.rosbank.android.russia.apiservices.WSRemoveFromWishList;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.model.ExperienceObject;
import com.rosbank.android.russia.model.Restaurant;
import com.rosbank.android.russia.model.UserItem;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.Fontfaces;
import com.rosbank.android.russia.utils.Logger;
import com.rosbank.android.russia.widgets.ViewPagerWrapContentView;
//import com.bumptech.glide.Glide;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*
 * Created by chau.nguyen on 10/05/2016.
 */
public class ExperiencesDetailFragment extends BaseFragment implements Callback {

    public static final String EXP_DETAIL_DATA = "exp_detail_data";
    public static final String EXP_DETAIL_TYPE = "exp_detail_type";
    public static final String EXP_DETAIL_ID_RESTAURANT = "exp_detail_restaurant";

    @BindView(R.id.srvExpDetail)
    ScrollView scrollView;

    @BindView(R.id.imgExpDetail)
    ImageView imgTitle;

    @BindView(R.id.tvExpDetailTitle)
    TextView tvTitle;

    @BindView(R.id.tvExpDetailDescription)
    TextView tvDescription;

    @BindView(R.id.tvExpDetailWebTitle)
    TextView tvWebTitle;

    @BindView(R.id.tvExpWebLink)
    TextView tvWebLink;

    @BindView(R.id.btnExpDetailBook)
    Button btnBook;

    @BindView(R.id.rlExpDetailWebContent)
    RelativeLayout rlWebContent;

    @BindView(R.id.llExpDetailDescription)
    LinearLayout llContentNormal;

    @BindView(R.id.llExpDetailGourmet)
    LinearLayout llContentGourmet;

    @BindView(R.id.tabLayoutExpDetail)
    TabLayout tabLayoutGourmet;
    @BindView(R.id.vPagerExpDetail)
    ViewPagerWrapContentView viewPagerGourmet;

    @BindView(R.id.imgExpDetailLogo)
    ImageView imgLogo;

    @BindView(R.id.imgExpDetailLike)
    ImageView imgLike;

    //var local
    Parcelable dataDetail;
    String titleBar = "";
    ExperienceSearchFragment.ApiRequest apiRequest;
    String urlWebsiteShow = "";
    Restaurant restaurant;

    public ExperiencesDetailFragment() {

    }

    @Override
    protected int layoutId() {
        return R.layout.fragment_experiences_detail;
    }

    @Override
    protected void initView() {
        setFontType();
        if (getArguments() == null)
            return;

        Object temp = getArguments().get(ExperiencesDetailFragment.EXP_DETAIL_TYPE);
        if (temp != null && temp instanceof ExperienceSearchFragment.ApiRequest) {
            apiRequest = (ExperienceSearchFragment.ApiRequest) temp;
            if (apiRequest == ExperienceSearchFragment.ApiRequest.GOURMET) {
                String restaurantId = getArguments().getString(EXP_DETAIL_ID_RESTAURANT, "");
                if (!restaurantId.isEmpty()) {
                    showDialogProgress();
                    WSGetExperienceGourmetDetail req = new WSGetExperienceGourmetDetail(restaurantId);
                    req.run(this);
                }
            } else {
                dataDetail = getArguments().getParcelable(EXP_DETAIL_DATA);
                if (dataDetail == null)
                    return;
                if (dataDetail instanceof ExperienceObject) {
                    ExperienceObject expObj = (ExperienceObject) dataDetail;
                    titleBar = expObj.getTitle();
                    tvTitle.setText(titleBar);
                    viewNormal(expObj);
                }
            }
        }
    }

    private void viewNormal(ExperienceObject expObj) {
        llContentNormal.setVisibility(View.VISIBLE);
        llContentGourmet.setVisibility(View.GONE);
        imgLogo.setVisibility(View.GONE);
        imgLike.setVisibility(View.GONE);

        CommonUtils.convertHtmlToStr(expObj.getDescription(), tvDescription);

        urlWebsiteShow = expObj.getWebsite();
        tvWebLink.setText(urlWebsiteShow);
        String urlBanner = BuildConfig.WS_ROOT_URL + expObj.getBannerUrl();
        //=========Load Image========
        if (!urlBanner.isEmpty()) {
            if (apiRequest == ExperienceSearchFragment.ApiRequest.ENTERTAINMENT) {
//                Glide.with(this)
//                        .load(urlBanner)
//                        .transform(new CropImageTransformUtil(getContext()))
//                        .crossFade()
//                        .into(imgTitle);
            } else {
//                Glide.with(this)
//                        .load(urlBanner)
//                        .crossFade()
//                        .into(imgTitle);
            }
        }
    }

    enum PageShow {

        Info(R.drawable.item_selector_experience_info, "Info"),
        Location(R.drawable.item_selector_experience_location, "Location"),
        Contact(R.drawable.item_selector_experience_phone, "Contact"),
        Menu(R.drawable.item_selector_experience_menu, "Menu"),
        Picture(R.drawable.item_selector_experience_picture, "Picture");

        private int idResource;
        private String tag;

        PageShow(int idResource, String tag) {
            this.idResource = idResource;
            this.tag = tag;
        }

        public int getIdImg() {
            return idResource;
        }

        public String getTag() {
            return tag;
        }
    }

    private void viewGourmet(Restaurant rest) {
        llContentNormal.setVisibility(View.GONE);
        llContentGourmet.setVisibility(View.VISIBLE);
        imgLogo.setVisibility(View.VISIBLE);
        imgLike.setVisibility(View.VISIBLE);

        //load data
        tvTitle.setText(rest.getRestaurantName());

        if (rest.isVoted()) {
            imgLike.setImageResource(R.drawable.ic_heart_active);
            imgLike.setTag("1");
        } else {
            imgLike.setImageResource(R.drawable.ic_heart);
            imgLike.setTag("0");
        }

//        Glide.with(this)
//                .load(rest.getBackgroundImageUrl())
//                .centerCrop()
//                .crossFade()
//                .into(imgTitle);

        if (rest.getLogoSource().isEmpty()) {
            imgLogo.setVisibility(View.GONE);
        } else {
//            Glide.with(this)
//                    .load(BuildConfig.WS_ROOT_URL + rest.getLogoSource())
//                    .centerCrop()
//                    .crossFade()
//                    .into(imgLogo);
        }

        //tabLayout
        tabLayoutGourmet.setSelectedTabIndicatorColor(ContextCompat.getColor(getContext(), R.color.app_color_dark_bg));

        ExperiencesDetailGourmetPagerAdapter adapter = new ExperiencesDetailGourmetPagerAdapter(getChildFragmentManager());

        List<PageShow> arrPage = new ArrayList<>();
        //default load page Info and Picture
        adapter.addPageInfo(rest);
        arrPage.add(PageShow.Info);

        if (!rest.getAddress().isEmpty() || rest.getBranches().size() > 0) {
            adapter.addPageLocation(rest);
            arrPage.add(PageShow.Location);
        }

//        !rest.getTelephone().isEmpty() ||
        if (!rest.getLink().getUrl().isEmpty() || !rest.getEmail().isEmpty()) {
            adapter.addPageContact(rest);
            arrPage.add(PageShow.Contact);
        }

        if (rest.getMenu().size() > 0) {
            adapter.addPageMenu(rest);
            arrPage.add(PageShow.Menu);
        }

        int heightRemove = tabLayoutGourmet.getHeight() + CommonUtils.convertPxToDp(imgTitle.getHeight(), getContext());
        adapter.addPagePicture(rest, heightRemove);
        arrPage.add(PageShow.Picture);

        viewPagerGourmet.setOffscreenPageLimit(arrPage.size());
        viewPagerGourmet.setAdapter(adapter);
        tabLayoutGourmet.setupWithViewPager(viewPagerGourmet);
        //set icon
        for (int i = 0; i < arrPage.size(); i++) {
            tabLayoutGourmet.getTabAt(i).setIcon(arrPage.get(i).getIdImg()).setTag(arrPage.get(i).getTag());
        }
        viewPagerGourmet.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayoutGourmet));
        tabLayoutGourmet.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPagerGourmet.requestLayout();
                if (PageShow.Picture.getTag().equals(tab.getTag())) {
                    btnBook.setVisibility(View.GONE);
                } else {
                    btnBook.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    private void setFontType() {
        tvTitle.setTypeface(Fontfaces.getFont(getContext(), Fontfaces.FONTLIST.Avenirnext_demibold));
        tvDescription.setTypeface(Fontfaces.getFont(getContext(), Fontfaces.FONTLIST.AVENIR_NEXT_LT_MEDIUM));
        tvWebTitle.setTypeface(Fontfaces.getFont(getContext(), Fontfaces.FONTLIST.Avenirnext_demibold));
        tvWebLink.setTypeface(Fontfaces.getFont(getContext(), Fontfaces.FONTLIST.AVENIR_NEXT_LT_MEDIUM));
        btnBook.setTypeface(Fontfaces.getFont(getContext(), Fontfaces.FONTLIST.Avenirnext_demibold));

        tvTitle.setText("");
        tvDescription.setText("");
        tvDescription.setText("");
        tvWebTitle.setText("");
        tvWebLink.setText("");
    }

    @Override
    protected void bindData() {
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showLogoApp(false);
            ((HomeActivity) getActivity()).setTitle(titleBar);
        }
    }

    @OnClick(R.id.rlExpDetailWebContent)
    public void eventWebContent() {
        if (!urlWebsiteShow.isEmpty()) {
            AboutWebFragment web = new AboutWebFragment();
            Bundle bundle = new Bundle();
            bundle.putString(AboutWebFragment.AboutWebTitle, getString(R.string.experiences_detail_website_page));
            bundle.putString(AboutWebFragment.AboutWebLink, urlWebsiteShow);
            web.setArguments(bundle);
            pushFragment(web, true, true);
        }
    }

    @Override
    public boolean onBack() {
        return false;
    }

    @OnClick(R.id.btnExpDetailBook)
    public void clickBookNow() {
        if (titleBar.isEmpty()) {
//            showToast(getString(R.string.experiences_detail_book_error));
            return;
        }
        sendEmail(titleBar);
    }

    private void sendEmail(String titleName) {
        String[] TO = {getString(R.string.text_experiences_detail_email_to_address)};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse(getString(R.string.text_experiences_detail_email_to)));
        emailIntent.setType(getString(R.string.text_experiences_detail_email_type));
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.text_experiences_detail_email_subject) + titleName);
        emailIntent.putExtra(Intent.EXTRA_TEXT, "");

        try {
            onBackPress();
            startActivity(Intent.createChooser(emailIntent, getString(R.string.text_experiences_detail_send_mail_title)));
        } catch (android.content.ActivityNotFoundException ex) {
//            showToast("There is no email client installed.");
        }
    }

    @OnClick(R.id.imgExpDetailLike)
    public void clickLike() {
        if (restaurant == null)
            return;

        String userId = UserItem.getLoginedId();
        String restId = restaurant.getIDStr();
        //process in response server
        if (restaurant.isVoted()) {
            WSRemoveFromWishList add = new WSRemoveFromWishList();
            add.setUserId(userId);
            add.setItemId(restId);
            add.run(this);
        } else {
            WSAddToWishList add = new WSAddToWishList();
            add.setUserId(userId);
            add.setItemId(restId);
            add.run(this);
        }
        showDialogProgress();
    }

    @Override
    public void onResponse(Call call, Response response) {
        if(getActivity()==null) {
            return;
        }
        if (response.body() instanceof AddWishListResponse) {
            AddWishListResponse rsp = (AddWishListResponse) response.body();
            if (rsp.isSuccess()) {
                if (restaurant != null) {
                    //update server done
                    imgLike.setImageResource(R.drawable.ic_heart_active);
                    restaurant.setAddWishListStatusToCSS(Restaurant.VOTED);
                }
            } else {
//                showToast(rsp.getMessage());
            }
        } else if (response.body() instanceof RemoveWishListResponse) {
            RemoveWishListResponse rsp = (RemoveWishListResponse) response.body();
            if (rsp.isSuccess()) {
                if (restaurant != null) {
                    //remove server
                    imgLike.setImageResource(R.drawable.ic_heart);
                    restaurant.setAddWishListStatusToCSS("");
                }
            } else {
//                showToast(rsp.getMessage());
            }
        } else if (response.body() instanceof GetExperienceGourmetDetailResponse) {
            GetExperienceGourmetDetailResponse rsp = (GetExperienceGourmetDetailResponse) response.body();
            if (rsp.isSuccess()) {

                restaurant = rsp.getData();
                titleBar = restaurant.getRestaurantName();
                tvTitle.setText(titleBar);
                viewGourmet(restaurant);
            } else {
//                showToast(rsp.getMessage());
            }
        }
        hideDialogProgress();
    }

    @Override
    public void onFailure(Call call, Throwable t) {
        Logger.sout(t.getMessage());
        hideDialogProgress();
        if(getActivity()==null) {
            return;
        }
    }
}
