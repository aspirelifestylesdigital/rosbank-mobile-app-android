package com.rosbank.android.russia.dcr.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.application.AppContext;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

/**
 * @author anh.trinh
 * @since v0.1
 */
public class ReadMoreLessView
        extends RelativeLayout {

    private static final int LAYOUT_RESOURCE_ID = R.layout.read_more_view;
    @BindView(R.id.tvReadMore)

    TextView mTvReadMore;
    @BindView(R.id.imgUp)
    ImageView mImgUp;
    @BindView(R.id.imgDown)
    ImageView mImgDown;
    private boolean isTermOfUse = false;
    private boolean isReadMove = false;


    public interface ReadMoreViewListener {
        void onReadMoreChange(boolean isReadmore);
    }

    ReadMoreViewListener readMoreViewListener;

    public void setReadMoreListener(ReadMoreViewListener listener) {
        this.readMoreViewListener = listener;
    }

    /**
     * @param context
     *         The context which view is running on.
     * @return New ReadMoreLessView object.
     * @since v0.1
     */
    public static ReadMoreLessView newInstance(final Context context) {
        if (context == null) {
            return null;
        }

        return new ReadMoreLessView(context);
    }

    /**
     * The constructor with current context.
     *
     * @param context
     *         The context which view is running on.
     * @since v0.1
     */
    public ReadMoreLessView(final Context context) {
        super(context);
        this.initialize();
    }

    /**
     * The constructor with current context.
     *
     * @param context
     *         The context which view is running on.
     * @param attrs
     *         The initialize attributes set.
     * @since v0.1
     */
    public ReadMoreLessView(final Context context,
                            final AttributeSet attrs) {
        super(context,
              attrs);
        this.initialize();
    }

    /**
     * The constructor with current context.
     *
     * @param context
     *         The context which view is running on.
     * @param attrs
     *         The initialize attributes set.
     * @param defStyleAttr
     *         The default style.
     * @since v0.1
     */
    public ReadMoreLessView(final Context context,
                            final AttributeSet attrs,
                            final int defStyleAttr) {
        super(context,
              attrs,
              defStyleAttr);

        this.initialize();
    }

    /**
     * @see View#setEnabled(boolean)
     * @since v0.1
     */
    @Override
    public void setEnabled(final boolean isEnabled) {
        this.setAlpha(isEnabled ?
                      1f :
                      0.5f);
        super.setEnabled(isEnabled);
    }

    /**
     * Reset injected resources created by ButterKnife.
     *
     * @since v0.1
     */
    public void resetInjectedResource() {
        ButterKnife.bind(this);
    }

    /**
     * Initialize UI sub views.
     *
     * @since v0.1
     */
    private void initialize() {

        final LayoutInflater layoutInflater = LayoutInflater.from(this.getContext());
        layoutInflater.inflate(LAYOUT_RESOURCE_ID,
                               this,
                               true);

        ButterKnife.bind(this,
                         this);

           updateData();

    }
    public void setData(boolean isReadMove, boolean isTermOfUse){
        this.isReadMove = isReadMove;
        this.isTermOfUse = isTermOfUse;
        updateData();

    }

private void updateData(){
    if(!isReadMove) {
        mTvReadMore.setText(AppContext.getSharedInstance()
                                      .getResources()
                                      .getString(R.string.text_read_more));
        mImgDown.setVisibility(VISIBLE);
        mImgUp.setVisibility(GONE);
    }else{
        mTvReadMore.setText(AppContext.getSharedInstance().getResources().getString(R.string.text_read_less));
        mImgDown.setVisibility(GONE);
        mImgUp.setVisibility(VISIBLE);
    }
    if(isTermOfUse){
        mTvReadMore.setText(AppContext.getSharedInstance().getResources().getString(R.string.text_terms_of_use));
    }

}
    @OnClick({R.id.layoutReadMore })
    public void onClick(View view) {
    isReadMove = !isReadMove;
        if(readMoreViewListener!= null){
            readMoreViewListener.onReadMoreChange(isReadMove);
        }
        updateData();
    }



}
