package com.rosbank.android.russia.model.preference;

import android.text.TextUtils;

import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CGetPreferenceResponse;
import com.rosbank.android.russia.application.AppConstant;

/**
 * Created by ThuNguyen on 12/13/2016.
 */

public class CarPreferenceDetailData extends PreferenceData{
    private String preferredRentalVehicle;
    private String preferredRentalCompany;
    private String otherPreferredRentalCompany;
    private String loyaltyProgram;
    private String membershipNo;

    public CarPreferenceDetailData(B2CGetPreferenceResponse response){
        super(response);
        preferenceType = AppConstant.PREFERENCE_TYPE.CAR;
        if(response != null){
            preferredRentalVehicle = response.getVALUE();
            preferredRentalCompany = response.getVALUE1();
            otherPreferredRentalCompany = response.getVALUE2();
            loyaltyProgram = response.getVALUE3();
            membershipNo = response.getVALUE4();
        }
    }

    public String getPreferredRentalVehicle() {
        return preferredRentalVehicle;
    }

    public void setPreferredRentalVehicle(String preferredRentalVehicle) {
        this.preferredRentalVehicle = preferredRentalVehicle;
    }

    public String getPreferredRentalCompany() {
        return preferredRentalCompany;
    }

    public void setPreferredRentalCompany(String preferredRentalCompany) {
        this.preferredRentalCompany = preferredRentalCompany;
    }

    public String getOtherPreferredRentalCompany() {
        return otherPreferredRentalCompany;
    }

    public void setOtherPreferredRentalCompany(String otherPreferredRentalCompany) {
        this.otherPreferredRentalCompany = otherPreferredRentalCompany;
    }
    public String getSuggestedRentalCompany(){
        if(!TextUtils.isEmpty(preferredRentalCompany)){
            return preferredRentalCompany;
        }
        return otherPreferredRentalCompany;
    }
    public String getLoyaltyProgram() {
        return loyaltyProgram;
    }

    public void setLoyaltyProgram(String loyaltyProgram) {
        this.loyaltyProgram = loyaltyProgram;
    }

    public String getMembershipNo() {
        return membershipNo;
    }

    public void setMembershipNo(String membershipNo) {
        this.membershipNo = membershipNo;
    }
}
