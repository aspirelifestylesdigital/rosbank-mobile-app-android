package com.rosbank.android.russia.processing.cancelrequest;

import android.text.TextUtils;
import android.view.View;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.apiservices.ResponseModel.CarRentalBookingDetailResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.rosbank.android.russia.apiservices.b2c.B2CWSGetRequestDetail;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.B2CUpsertConciergeRequestRequest;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CGetRecentRequestResponse;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.application.AppContext;
import com.rosbank.android.russia.model.concierge.CarBookingDetailData;
import com.rosbank.android.russia.utils.DateTimeUtil;
import com.rosbank.android.russia.utils.SharedPreferencesUtils;
import com.rosbank.android.russia.utils.StringUtil;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by ThuNguyen on 10/26/2016.
 */

public class CarRentalCancelRequestDetail extends BaseCancelRequestDetail{
    private CarBookingDetailData carBookingDetailData;

    public CarRentalCancelRequestDetail(){}
    public CarRentalCancelRequestDetail(View view) {
        super(view);
    }

    @Override
    public void getRequestDetail() {
        showProgressDialog();
        B2CWSGetRequestDetail b2CWSGetRequestDetail = new B2CWSGetRequestDetail(this);
        b2CWSGetRequestDetail.setValue(myRequestObject.getEpcCaseId(), myRequestObject.getBookingItemID());
        b2CWSGetRequestDetail.run(null);
//        WSGetCarRentalBookingDetail wsGetCarRentalBookingDetail = new WSGetCarRentalBookingDetail();
//        wsGetCarRentalBookingDetail.setBookingId(myRequestObject.getBookingId());
//        wsGetCarRentalBookingDetail.run(this);
        showProgressDialog();
    }

    @Override
    public B2CUpsertConciergeRequestRequest getB2CUpsertConciergeRequestRequest() {
        B2CUpsertConciergeRequestRequest  upsertConciergeRequestRequest = new B2CUpsertConciergeRequestRequest();
        upsertConciergeRequestRequest.setFunctionality(AppConstant.CONCIERGE_FUNCTIONALITY_TYPE.CAR_RENTAL.getValue());
        upsertConciergeRequestRequest.setRequestType(AppConstant.CONCIERGE_REQUEST_TYPE.T_CAR_RENTAL.getValue());
        upsertConciergeRequestRequest.setEmail1((SharedPreferencesUtils.getPreferences(AppConstant.USER_EMAIL_PRE, "")));
        upsertConciergeRequestRequest.setPickUp(carBookingDetailData.getPickupDate());
        upsertConciergeRequestRequest.setSituation("carRental");
        if(!TextUtils.isEmpty(carBookingDetailData.getMobileNumber())) {
            upsertConciergeRequestRequest.setPhoneNumber(carBookingDetailData.getMobileNumber());
        }
        upsertConciergeRequestRequest.setPrefResponse(carBookingDetailData.getPrefResponse());
        // Pickup date
        if(!TextUtils.isEmpty(carBookingDetailData.getPickupDate())) {
            upsertConciergeRequestRequest.setPickupDate(carBookingDetailData.getPickupDate());
        }


        if(myRequestObject != null){
            upsertConciergeRequestRequest.setTransactionID(myRequestObject.getBookingItemID());
        }
        // Request details
        upsertConciergeRequestRequest.setRequestDetails(carBookingDetailData.getRequestDetaiString());
        return upsertConciergeRequestRequest;
    }
    private String combineRequestDetails(){
        String requestDetails = "";
        if(!TextUtils.isEmpty(carBookingDetailData.getConciergeRequestDetail()
                                                    .getSpecialRequirement())){
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_SPECIAL_REQ + carBookingDetailData.getConciergeRequestDetail()
                                                                                               .getSpecialRequirement();
        }
        if(!TextUtils.isEmpty(carBookingDetailData.getReservationName())){
            if(!TextUtils.isEmpty(requestDetails)){
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_RESERVATION_NAME + carBookingDetailData.getReservationName();
        }
        // International license
        if(!StringUtil.isEmpty(requestDetails)){
            requestDetails += " | ";
        }
        requestDetails += AppConstant.B2C_REQUEST_DETAIL_INT_LICENSE + (carBookingDetailData.getInternationalLicense() ? "Yes" : "No");

        // Name of driver
        if(!StringUtil.isEmpty(requestDetails)){
            requestDetails += " | ";
        }
        requestDetails += AppConstant.B2C_REQUEST_DETAIL_DRIVER_NAME + carBookingDetailData.getDriverName();
        // Age of driver
        if(!StringUtil.isEmpty(requestDetails)){
            requestDetails += " | ";
        }
        requestDetails += AppConstant.B2C_REQUEST_DETAIL_DRIVER_AGE + carBookingDetailData.getDriverAge();

        // Preferred vehicle

        if (!StringUtil.isEmpty(carBookingDetailData.getPreferredVehicles())) {

            requestDetails += AppConstant.B2C_REQUEST_DETAIL_PREF_VEHICLE + carBookingDetailData.getPreferredVehicles();
        }
        // Pickup location
        if(!StringUtil.isEmpty(requestDetails)){
            requestDetails += " | ";
        }
        requestDetails += AppConstant.B2C_REQUEST_DETAIL_PICK_LOCATION + carBookingDetailData.getPickupLocation();

        // Drop-off day
        if(!StringUtil.isEmpty(requestDetails)){
            requestDetails += " | ";
        }
        requestDetails += AppConstant.B2C_REQUEST_DETAIL_DROP_OF_DATE + carBookingDetailData.getPickupDate();

        // Drop-off location
        requestDetails += " | " + AppConstant.B2C_REQUEST_DETAIL_DROP_LOCATION + carBookingDetailData.getDropOffLocation();

        // Preferred car rental company
        if (!TextUtils.isEmpty(carBookingDetailData.getPreferredCarRentals())) {
            if(!StringUtil.isEmpty(requestDetails)){
                requestDetails += " | ";
            }

            requestDetails += AppConstant.B2C_REQUEST_DETAIL_PREF_CAR_RENTAL_COMPANY + carBookingDetailData.getPreferredCarRentals();
        }

        if(!StringUtil.isEmpty(carBookingDetailData.getMaximumPrice())){
            if (!TextUtils.isEmpty(requestDetails)) {
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_BUDGET_RANGE_PER_DAY + carBookingDetailData.getConciergeRequestDetail().getBudgetRangepernightCurrency();
        }

        return requestDetails;
    }

    @Override
    protected void updateUI() {
        super.updateUI();
        long epochCreatedDate = carBookingDetailData.getCreateDateEpoc();
        tvRequestedDate.setText(DateTimeUtil.shareInstance().formatTimeStamp(epochCreatedDate, "dd MMMM yyyy"));
        tvRequestedTime.setText(DateTimeUtil.shareInstance().formatTimeStamp(epochCreatedDate, "hh:mm aa"));
        // Request type
        tvRequestType.setText(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_title_car_rental_request));

        // Request name
       // tvRequestName.setText(myRequestObject.getItemTitle());
        tvRequestName.setText(myRequestObject.getReservationName());
        // Request detail header
        tvRequestDetailHeader.setText(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_car_rental_request_detail));

        if(carBookingDetailData != null){
            // Case Id
           tvCaseId.setText(carBookingDetailData.getBookingItemID());

            // Car rental request detail
            // Driver name
            String driverName = carBookingDetailData.getDriverName();
            if(TextUtils.isEmpty(driverName) || driverName.equalsIgnoreCase("null")){
                //addRequestDetailItem("Name of Driver", "N.A");
            }else{
                addRequestDetailItem(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_name_of_driver), driverName);
            }
            // Age of driver
            String driverAge = carBookingDetailData.getDriverAge();
            if(!TextUtils.isEmpty(driverAge) && !driverAge.equalsIgnoreCase("0")){
                addRequestDetailItem(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_age_of_driver), String.valueOf(driverAge));
            }else{
                //addRequestDetailItem("Age of Driver", "N.A");
            }
            // Pick up location
            String pickupLocation = carBookingDetailData.getPickupLocation();
            if(TextUtils.isEmpty(pickupLocation) || pickupLocation.equalsIgnoreCase("null")){
                //addRequestDetailItem("Pick up Location", "N.A");
            }else{
                addRequestDetailItem(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_pick_up_location), pickupLocation);
            }
            // Drop off location
            String dropOffLocation = carBookingDetailData.getDropOffLocation();
            if(TextUtils.isEmpty(dropOffLocation) || dropOffLocation.equalsIgnoreCase("null")){
                //addRequestDetailItem("Drop off Location", "N.A");
            }else{
                addRequestDetailItem(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_drop_off_location), dropOffLocation);
            }
        }
    }

    @Override
    public void onResponse(Call call, Response response) {
        hideProgressDialog();
        if(response != null && response.body() instanceof CarRentalBookingDetailResponse){
            carBookingDetailData = ((CarRentalBookingDetailResponse)response.body()).getData();
            updateUI();
        }
    }

    @Override
    public void onFailure(Call call, Throwable t) {
        hideProgressDialog();
    }
    @Override
    public void onB2CResponse(final B2CBaseResponse response) {
        hideProgressDialog();
        if(response instanceof B2CGetRecentRequestResponse){
            carBookingDetailData = new CarBookingDetailData((B2CGetRecentRequestResponse)response);
            updateUI();
        }

    }

    @Override
    public void onB2CResponseOnList(final List<B2CBaseResponse> responseList) {

    }

    @Override
    public void onB2CFailure(final String errorMessage,
                             final String errorCode) {
        hideProgressDialog();
    }
}
