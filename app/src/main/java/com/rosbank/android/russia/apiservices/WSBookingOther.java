package com.rosbank.android.russia.apiservices;

import com.rosbank.android.russia.apiservices.RequestModel.BookOtherRequest;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CApiProviderClient;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CICallback;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.B2COtherBookingRequest;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CBookingResponse;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Callback;

//Note: switch it to B2C api
public class WSBookingOther
        extends B2CApiProviderClient<B2CBookingResponse> {

    private BookOtherRequest request;

    public WSBookingOther(B2CICallback callback) {
        this.b2CICallback = callback;
    }

    public void booking(final BookOtherRequest request) {
        this.request = request;
    }

    @Override
    protected boolean isUseAuthenticateInApi() {
        return true;
    }

    @Override
    public void runApi() {
        B2COtherBookingRequest b2cRequest = new B2COtherBookingRequest();
        b2cRequest.setPhoneNumber(request.getMobileNumber());
        b2cRequest.setEmailAddress1(request.getEmailAddress());
        b2cRequest.setPrefResponse(request.getPhone() ? "Phone" : (request.getEmail() ? "Email" : "Both"));
        b2cRequest.setSpecialRequirement(request.getWhatCanWeDo());
        //TODO: haven't got submit image

        serviceInterface.bookOther(b2cRequest).enqueue(this);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        Map<String, String> headers = new HashMap<>();

        return headers;
    }

    @Override
    protected void postResponse(B2CBookingResponse response) {
    }

    @Override
    public void run(Callback<B2CBookingResponse> cb) {
        super.checkAuthenticateAndRun();
    }
}
