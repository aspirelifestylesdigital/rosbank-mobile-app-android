package com.rosbank.android.russia.model;

import com.google.gson.annotations.Expose;

/**
 * Created by nga.nguyent on 9/27/2016.
 */
public class FavouriteData {
    @Expose
    private String ItemID;

    public String getItemID() {
        return ItemID;
    }

    public void setItemID(String itemID) {
        ItemID = itemID;
    }
}
