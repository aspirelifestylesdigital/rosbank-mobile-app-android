package com.rosbank.android.russia.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.model.RentalObject;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.FontUtils;

import java.util.ArrayList;

/**
 * Created by anh.trinh on 9/20/2016.
 */
public class SelectionRentalAdapter
        extends ArrayAdapter<RentalObject> {
    private final Context context;
    private final ArrayList<RentalObject> values;
    private ArrayList<RentalObject> selectedData;


    public SelectionRentalAdapter(Context context, ArrayList<RentalObject> values, ArrayList<RentalObject> selectedData) {
        super(context, -1, values);
        this.context = context;
        this.values = values;
        this.selectedData = new ArrayList<>();
        if(selectedData != null && selectedData.size() > 0) {
            this.selectedData.addAll(selectedData);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                                                           .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.item_selection_multiple, parent, false);

        RelativeLayout layoutContain = (RelativeLayout) rowView.findViewById(R.id.layout_contain);
        TextView textView = (TextView) rowView.findViewById(R.id.tv_rental_name);
        CheckBox cbTick = (CheckBox) rowView.findViewById(R.id.cb_selected);
        CommonUtils.setFontForViewRecursive(textView,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
        RentalObject rentalObject = values.get(position);
        textView.setText(rentalObject.getValue());

        if(selectedData!=null && selectedData.contains(rentalObject)) {
            cbTick.setChecked(true);
        }else{
            cbTick.setChecked(false);
        }
        layoutContain.setTag(rentalObject);

        layoutContain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                RentalObject downPressedRentalObj = (RentalObject)view.getTag();
                if(selectedData.contains(downPressedRentalObj)){
                    selectedData.remove(downPressedRentalObj);
                }else{
                    selectedData.add(downPressedRentalObj);
                }
                notifyDataSetChanged();
                /*if(cbTick.isChecked()){
                    cbTick.setChecked(false);
                    if(selectedData!=null && selectedData.size()>0) {
                        for (RentalObject rental : selectedData) {
                            if (rental.getKey()
                                      .equals(rentalObject.getKey())) {
                                selectedData.remove(rental);
                                return;
                            }

                        }
                    }
                }else{
                    cbTick.setChecked(true);
                    boolean isContain = false;
                    if(selectedData== null){
                        selectedData = new ArrayList<RentalObject>();
                    }
                    for (RentalObject rental: selectedData) {
                        if(rental.getKey().equals(rentalObject.getKey())){
                            isContain = true;
                        }
                    }
                    if(!isContain){
                        selectedData.add(rentalObject);
                    }
                }*/
            }
        });


        return rowView;
    }
public ArrayList<RentalObject> getSelectedData(){
    if(selectedData== null){
        return new ArrayList<>();
    }else{
        return selectedData;
    }
}

    @Override
    public int getCount() {
        return values.size();
    }
}

