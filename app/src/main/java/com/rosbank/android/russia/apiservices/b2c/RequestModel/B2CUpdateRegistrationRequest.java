package com.rosbank.android.russia.apiservices.b2c.RequestModel;

import com.google.gson.annotations.Expose;

import com.rosbank.android.russia.BuildConfig;
import com.rosbank.android.russia.apiservices.RequestModel.BaseRequest;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.model.UserItem;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.SharedPreferencesUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ThuNguyen on 10/26/2016.
 */

public class B2CUpdateRegistrationRequest extends BaseRequest {
    @Expose
    private MemberExt Member;
    @Expose
    private List<MemberDetailExt> MemberDetails;

    public B2CUpdateRegistrationRequest build(String firstName, String lastName,
                                              String mobileNumber, String salutation, String postalCode) {
        salutation = CommonUtils.convertSalutationToEnglish(salutation);
        setMember(new MemberExt().build(firstName, lastName, mobileNumber, salutation, postalCode));
        MemberDetails = new ArrayList<>();
        MemberDetails.add(new MemberDetailExt());
        setMemberDetails(MemberDetails);
        return this;
    }

    private class MemberExt {
        @Expose
        private String ConsumerKey;
        @Expose
        private String Email;
        @Expose
        private String FirstName;
        @Expose
        private String Functionality;
        @Expose
        private String LastName;
        @Expose
        private String MemberDeviceId;
        @Expose
        private String Mobile;
        @Expose
        private String OnlineMemberID;
        @Expose
        private String Salutation;
        @Expose
        private String PostalCode;

        public MemberExt() {
            ConsumerKey = UserItem.isVip() ? BuildConfig.B2C_CONSUMER_KEY_PRIVATE : BuildConfig.B2C_CONSUMER_KEY_REGULAR;
            Functionality = "Registration";
            MemberDeviceId = BuildConfig.B2C_MEMBER_DEVICE_ID;
            Email = SharedPreferencesUtils.getPreferences(AppConstant.USER_EMAIL_PRE, "");
            OnlineMemberID = SharedPreferencesUtils.getPreferences(AppConstant.PRE_B2C_ONLINE_MEMBER_ID, "");
        }

        public MemberExt build(String firstName, String lastName, String mobileNumber, String salutation, String postalCode) {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Mobile = mobileNumber;
            this.Salutation = salutation;
            this.PostalCode = postalCode;
            return this;
        }

        public String getConsumerKey() {
            return ConsumerKey;
        }

        public void setConsumerKey(String consumerKey) {
            ConsumerKey = consumerKey;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String email) {
            Email = email;
        }

        public String getFirstName() {
            return FirstName;
        }

        public void setFirstName(String firstName) {
            FirstName = firstName;
        }

        public String getFunctionality() {
            return Functionality;
        }

        public void setFunctionality(String functionality) {
            Functionality = functionality;
        }

        public String getLastName() {
            return LastName;
        }

        public void setLastName(String lastName) {
            LastName = lastName;
        }

        public String getMemberDeviceId() {
            return MemberDeviceId;
        }

        public void setMemberDeviceId(String memberDeviceId) {
            MemberDeviceId = memberDeviceId;
        }

        public String getSalutation() {
            return Salutation;
        }

        public void setSalutation(String salutation) {
            Salutation = salutation;
        }
    }

    private class MemberDetailExt {
        @Expose
        private String VerificationCode;
        @Expose
        private String OnlineMemberDetailId;

        public MemberDetailExt() {
            VerificationCode = UserItem.isVip() ? BuildConfig.B2C_VERIFICATION_CODE_PRIVATE : BuildConfig.B2C_VERIFICATION_CODE_REGULAR;
            OnlineMemberDetailId = SharedPreferencesUtils.getPreferences(AppConstant.USER_ONLINE_MEMBER_DETAIL_ID_PRE, "");
        }

        public String getVeriCode() {
            return VerificationCode;
        }

        public void setVeriCode(String veriCode) {
            VerificationCode = veriCode;
        }

        public String getOnlineMemberDetailId() {
            return OnlineMemberDetailId;
        }

        public void setOnlineMemberDetailId(String onlineMemberDetailId) {
            OnlineMemberDetailId = onlineMemberDetailId;
        }
    }

    public MemberExt getMember() {
        return Member;
    }

    public void setMember(MemberExt member) {
        Member = member;
    }

    public List<MemberDetailExt> getMemberDetails() {
        return MemberDetails;
    }

    public void setMemberDetails(List<MemberDetailExt> memberDetails) {
        MemberDetails = memberDetails;
    }
}
