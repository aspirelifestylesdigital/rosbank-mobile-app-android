package com.rosbank.android.russia.model.preference;

import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CGetPreferenceResponse;
import com.rosbank.android.russia.application.AppConstant;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ThuNguyen on 12/13/2016.
 */

public class PreferenceData implements Serializable{
    private String preferenceId;
    protected AppConstant.PREFERENCE_TYPE preferenceType;

    public PreferenceData(B2CGetPreferenceResponse response){
        if(response != null){
            preferenceId = response.getMYPREFERENCESID();
        }
    }
    public static PreferenceData findFromList(List<B2CBaseResponse> responseList, AppConstant.PREFERENCE_TYPE preferenceType){
        PreferenceData preferenceData = null;
        if(responseList != null && responseList.size() > 0){
            for(B2CBaseResponse b2CBaseResponse : responseList){
                if(b2CBaseResponse instanceof B2CGetPreferenceResponse){
                    B2CGetPreferenceResponse getPreferenceResponse = (B2CGetPreferenceResponse)b2CBaseResponse;
                    if(preferenceType.getValue().equalsIgnoreCase(getPreferenceResponse.getTYPE())){
                        switch (preferenceType){
                            case HOTEL:
                                preferenceData = new HotelPreferenceDetailData(getPreferenceResponse);
                                break;
                            case DINING:
                                preferenceData = new DiningPreferenceDetailData(getPreferenceResponse);
                                break;
                            case CAR:
                                preferenceData = new CarPreferenceDetailData(getPreferenceResponse);
                                break;
                            case GOLF:
                                preferenceData = new GolfPreferenceDetailData(getPreferenceResponse);
                                break;
                            case OTHER:
                                preferenceData = new OtherPreferenceDetailData(getPreferenceResponse);
                                break;
                        }
                        break;
                    }
                }
            }
        }
        return preferenceData;
    }

    public String getPreferenceId() {
        return preferenceId;
    }

    public void setPreferenceId(String preferenceId) {
        this.preferenceId = preferenceId;
    }
}
