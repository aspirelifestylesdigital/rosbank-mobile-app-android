package com.rosbank.android.russia.adapter;

import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.fragment.ConciergeGourmetTab1Fragment;
import com.rosbank.android.russia.fragment.ConciergeGourmetTab2Fragment;
import com.rosbank.android.russia.model.concierge.MyRequestObject;
import com.rosbank.android.russia.model.Restaurant;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

/**
 * Created by nga.nguyent on 10/6/2016.
 */

public class ConciergeGourmetAdapter extends FragmentPagerAdapter {
    BaseFragment[] fragments = new BaseFragment[2];

    public ConciergeGourmetAdapter(FragmentManager fm, Restaurant restaurant) {
        super(fm);
        ConciergeGourmetTab1Fragment tab1 = new ConciergeGourmetTab1Fragment();
        ConciergeGourmetTab2Fragment tab2 = new ConciergeGourmetTab2Fragment();
        fragments[0] = tab1;
        fragments[1] = tab2;
    }

    public ConciergeGourmetAdapter(FragmentManager fm, MyRequestObject myRequest) {
        super(fm);
        fragments = new BaseFragment[1];
        if(myRequest.getBookingRequestType().equals(MyRequestObject.GOURMET_REQUEST)) {
            ConciergeGourmetTab1Fragment tab1 = new ConciergeGourmetTab1Fragment();
            tab1.setRequest(myRequest);
            fragments[0] = tab1;
        }else if(myRequest.getBookingRequestType().equals(MyRequestObject.GOURMET_RECOMMEND_REQUEST)) {
            ConciergeGourmetTab2Fragment tab2 = new ConciergeGourmetTab2Fragment();
            tab2.setRequest(myRequest);
            fragments[0] = tab2;
        }
    }

    @Override
    public Fragment getItem(int position) {
        return fragments[position];
    }

    @Override
    public int getCount() {
        return fragments.length;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
    }
    public void updateDefaultTime(int position){
        if(position == 0){
            ((ConciergeGourmetTab1Fragment)fragments[0]).makeDefaultReservationTime();
        }else{
            ((ConciergeGourmetTab2Fragment)fragments[1]).makeDefaultReservationTime();
        }
    }
}
