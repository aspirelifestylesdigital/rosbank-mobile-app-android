package com.rosbank.android.russia.dcr.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.rosbank.android.russia.R;

import java.util.List;

import com.rosbank.android.russia.dcr.model.DCRImageObject;
import com.rosbank.android.russia.utils.CommonUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

/*
 * Created by anh.trinh on 11/03/2017.
 */
public class DCRGalleryAdapter
        extends PagerAdapter {

    private Activity activity;
    private List<DCRImageObject> listGallery;
    private int width, height;

    public DCRGalleryAdapter(Activity activity, List<DCRImageObject> listGallery, int width, int height) {
        this.activity = activity;
        this.listGallery = listGallery;
        this.width = width;
        this.height = height;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.layout_item_gallery, container, false);

        ImageView imgViewPreview = (ImageView) view.findViewById(R.id.imgGallery);
        if(CommonUtils.isUrlLink(listGallery.get(position).getUrl())) {
            Glide.with(activity).load(listGallery.get(position).getUrl())
                 .thumbnail(0.5f)
                 .centerCrop()
                 .override(width, height)
                 .crossFade()
                 .diskCacheStrategy(DiskCacheStrategy.ALL)
                 .into(imgViewPreview);
        }else{
            if(CommonUtils.isStringValid(listGallery.get(position).getLocalPath())) {
                Drawable drawable = this.activity.getResources()
                                                 .getDrawable(this.activity.getResources()
                                                                           .getIdentifier(listGallery.get(position)
                                                                                                     .getLocalPath(),
                                                                                          "drawable",
                                                                                          this.activity.getPackageName()));
                imgViewPreview.setImageDrawable(drawable);
            }
        }



        container.addView(view);

        return view;
    }

    @Override
    public int getCount() {
        return listGallery.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
