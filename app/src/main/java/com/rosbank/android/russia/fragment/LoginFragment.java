package com.rosbank.android.russia.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.LinkMovementMethod;
import android.text.method.PasswordTransformationMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.activitys.HomeActivity;
import com.rosbank.android.russia.activitys.LoginAcitivy;
import com.rosbank.android.russia.activitys.SplashAcitivy;
import com.rosbank.android.russia.apiservices.ResponseModel.SignInResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CICallback;
import com.rosbank.android.russia.apiservices.b2c.B2CWSLogin;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.B2CLoginRequest;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CLoginResponse;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.helper.DoubleClickListener;
import com.rosbank.android.russia.model.UserItem;
import com.rosbank.android.russia.utils.ColoredUnderlineSpan;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.FontUtils;
import com.rosbank.android.russia.utils.NetworkUtil;
import com.rosbank.android.russia.utils.SharedPreferencesUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTouch;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.rosbank.android.russia.application.AppConstant.USER_VIP_STATUS;

/**
 * Created by anh.trinh on 9/14/2016.
 */
public class LoginFragment
        extends BaseFragment
        implements Callback,
        B2CICallback {
    @BindView(R.id.edtLoginEmail)
    EditText mEdtEmail;
    @BindView(R.id.edtLoginPass)
    EditText mEdtPass;
    @BindView(R.id.edtEntryCode)
    EditText mEdtEntryCode;
    @BindView(R.id.ic_error_entry_code)
    ImageButton mIBtnErrorEntryCode;
    @BindView(R.id.ic_error_email)
    ImageButton mIBtnErrorEmail;
    @BindView(R.id.ic_error_pass)
    ImageButton mIBtnErrorPass;
    @BindView(R.id.ivShowHidePassword)
    ImageView ivShowHidePassword;
    @BindView(R.id.layout_error_message_email)
    RelativeLayout mLayoutErrorEmail;
    @BindView(R.id.layout_error_message_entry_code)
    RelativeLayout mLayoutErrorEntry;
    @BindView(R.id.layout_error_message_pass)
    RelativeLayout mLayoutErrorPass;
    @BindView(R.id.layout_sign_in_email)
    LinearLayout mLayoutSignInEmail;
    @BindView(R.id.layout_sign_in_pass)
    LinearLayout mLayoutSignInPass;
    @BindView(R.id.layout_sign_in_entry_code)
    LinearLayout mLayoutSignInEntryCode;
    @BindView(R.id.layout_error_message)
    RelativeLayout mLayoutErrorMessage;
    /*    @BindView(R.id.tv_get_started)
        TextView mTvGetStarted;*/
    @BindView(R.id.img_mc_logo)
    ImageView mImgMcLogo;
    @BindView(R.id.tv_app_name)
    TextView mTvAppName;
    @BindView(R.id.tvLoginEmail)
    TextView mTvLoginEmail;
    @BindView(R.id.tv_error_email)
    TextView mTvErrorEmail;
    @BindView(R.id.tvLoginPass)
    TextView mTvLoginPass;
    @BindView(R.id.tv_error_pass)
    TextView mTvErrorPass;
    @BindView(R.id.tv_error)
    TextView mTvError;
    @BindView(R.id.img_tool_tip_error)
    ImageView mImgToolTipError;
    @BindView(R.id.tv_note_pass)
    TextView mTvNotePass;
    @BindView(R.id.cbShowPassword)
    CheckBox cbShowPassword;
    @BindView(R.id.tvSignInHint)
    TextView tvSignInHint;
    @BindView(R.id.btn_sign_in)
    Button mBtnSignIn;
    @BindView(R.id.tv_forgot_pass)
    TextView mTvForgotPass;
    @BindView(R.id.tv_sign_up)
    TextView mTvSignUp;
    @BindView(R.id.container)
    LinearLayout mLayoutContainer;
    @BindView(R.id.scrollView)
    ScrollView mScrollView;
    @BindView(R.id.tv_terms_part1)
    TextView mTvTermsOfUse;

    private boolean isShowPassword = false;

    @Override
    protected int layoutId() {
        return R.layout.fragment_login;
    }

    @Override
    protected void initView() {

        setUserAgreementSpannableString();

        mTvError.setText(getString(R.string.text_sign_in_error));
        mTvError.setMovementMethod(LinkMovementMethod.getInstance());

        cbShowPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton,
                                         boolean isChecked) {
                if (!isChecked) {
                    // show password
                    mEdtPass.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    // hide password
                    mEdtPass.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
                mEdtPass.setSelection(mEdtPass.getText()
                        .length());
            }
        });
        CommonUtils.setFontForViewRecursive(mLayoutContainer,
                FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LTPRO_MEDIUM);

        CommonUtils.setFontForViewRecursive(mBtnSignIn,
                FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        CommonUtils.setFontForViewRecursive(mTvAppName,
                FontUtils.FONT_FILE_NAME_GOTHAM_ROUND_MEDIUM);
        CommonUtils.setFontForViewList(FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD,
                cbShowPassword);


        mImgMcLogo.setOnClickListener(new DoubleClickListener() {
            @Override
            public void onSingleClick(final View v) {

            }

            @Override
            public void onDoubleClick(final View v) {
                Intent intent = new Intent(getActivity(),
                        SplashAcitivy.class);
                startActivity(intent);
                getActivity().finish();
                getActivity().overridePendingTransition(R.anim.push_down_in,
                        R.anim.push_down_out);
            }
        });

    }

    @Override
    protected void bindData() {


    }

    @Override
    public boolean onBack() {
        return false;
    }

    @OnClick({R.id.tv_sign_up,
            R.id.tv_forgot_pass,
            R.id.btn_sign_in,
            /*R.id.tv_get_started,*/
            R.id.ic_error_email,
            R.id.ic_error_pass,
            R.id.ic_error_entry_code,
            R.id.tvSignInHint,
            R.id.ivShowHidePassword})
    public void onClick(View view) {
        Activity activity = getActivity();
        switch (view.getId()) {
            case R.id.tv_sign_up:
                if (activity instanceof LoginAcitivy) {
                    /*((LoginAcitivy) activity).loadFragment(new SignUpFragment(),
                                                           null,
                                                           true,
                                                           true,
                                                           false);*/
                    pushFragment(new SignUpFragment(),
                            true,
                            true);
              /*      pushFragment(new FragmentInitialSignUp(),
                            true,
                            true);*/


                }

                break;
            case R.id.tv_forgot_pass:
                if (activity instanceof LoginAcitivy) {
                    /*((LoginAcitivy) activity).loadFragment(new ForgotPasswordFragment(),
                                                           null,
                                                           true,
                                                           true,
                                                           false);*/
                    pushFragment(new ForgotPasswordFragment(),
                            true,
                            true);
                }

                break;
            case R.id.btn_sign_in:

                if (validateInputFields()) {
                    if (!NetworkUtil.getNetworkStatus(getContext())) {
                        showDialogMessage(getString(R.string.text_title_dialog_error),
                                getString(R.string.text_message_dialog_error_no_internet));
                    } else {
                        showDialogProgress();
                        UserItem.saveClientCode(mEdtEntryCode.getText().toString().trim());
                        B2CWSLogin b2CWSLogin = new B2CWSLogin(this);
                        b2CWSLogin.setLoginRequest(new B2CLoginRequest(mEdtEntryCode.getText().toString().trim()).build(
                                mEdtEmail.getText().toString().trim(),
                                mEdtPass.getText().toString().trim(),
                                mEdtEntryCode.getText().toString().trim()
                        ));
                        b2CWSLogin.run(null);
                    }
                }

                break;
      /*      case R.id.tv_get_started:
                Intent intent = new Intent(getActivity(), HomeActivity.class);

                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                getActivity().finish();
                break;*/
            case R.id.ic_error_email:
                mLayoutErrorEmail.setVisibility(View.VISIBLE);

                break;
            case R.id.ic_error_pass:
                mLayoutErrorPass.setVisibility(View.VISIBLE);
                break;
            case R.id.ic_error_entry_code:
                mLayoutErrorEntry.setVisibility(View.VISIBLE);
                break;
            case R.id.ivShowHidePassword:
                if (isShowPassword) {
                    // show password
                    mEdtPass.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    ivShowHidePassword.setImageResource(R.drawable.ic_eyes_show);
                } else {
                    // hide password
                    mEdtPass.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    ivShowHidePassword.setImageResource(R.drawable.ic_eyes_hide);
                }
                isShowPassword = !isShowPassword;
                mEdtPass.setSelection(mEdtPass.getText()
                        .length());
                break;
        }
    }

    private boolean validateInputFields() {
        Boolean isAccepted = true;
        if (!CommonUtils.emailValidator(mEdtEmail.getText()
                .toString())) {
            showMessageErrorEmail();
            isAccepted = false;
        } else {
            hideMessageErrorEmail();
        }

        if ((mEdtPass.getText()
                .toString()
                .trim()
                .length() == 0)) {
            showMessageErrorPass();
            isAccepted = false;

        } else {
            hideMessageErrorPass();

        }
        if (mEdtEntryCode.getText().toString().trim().isEmpty()) {
            showMessageErrorEntryCode();
            isAccepted = false;
        } else {
            hideMessageErrorEntryCode();
        }

        if (!mEdtEntryCode.getText().toString().trim().equals(AppConstant.CLIENT_CODE_REGULAR) && !mEdtEntryCode.getText().toString().trim().equals(AppConstant.CLIENT_CODE_PRIVATE)) {
            showMessageErrorEntryCode();
            isAccepted = false;
        }

        return isAccepted;
    }

    @OnTouch({R.id.edtLoginEmail})
    boolean onInputTouchEmail(final View v,
                              final MotionEvent event) {
        hideMessageErrorEmail();

        return false;
    }

    @OnTouch({R.id.edtLoginPass})
    boolean onInputTouch(final View v,
                         final MotionEvent event) {
        hideMessageErrorPass();

        return false;
    }

    @OnTouch({R.id.edtEntryCode})
    boolean onInputTouchEntryCode(final View v,
                                  final MotionEvent event) {
        hideMessageErrorEntryCode();

        return false;
    }

    private void showErrorMessage() {
        mLayoutErrorMessage.setVisibility(View.VISIBLE);
//        showMessageErrorEmail();
//        showMessageErrorPass();
    }

    private void showMessageErrorEmail() {
        mLayoutSignInEmail.setBackground(getResources().getDrawable(R.drawable.bg_sign_in_circle_stroke_white_red));
        mIBtnErrorEmail.setVisibility(View.VISIBLE);
    }

    private void showMessageErrorPass() {
        mLayoutSignInPass.setBackground(getResources().getDrawable(R.drawable.bg_sign_in_circle_stroke_white_red));
        mIBtnErrorPass.setVisibility(View.VISIBLE);
        ivShowHidePassword.setVisibility(View.GONE);
    }

    private void showMessageErrorEntryCode() {
        mLayoutSignInEntryCode.setBackground(getResources().getDrawable(R.drawable.bg_sign_in_circle_stroke_white_red));
        mIBtnErrorEntryCode.setVisibility(View.VISIBLE);
    }

    private void hideMessageErrorEmail() {
        mLayoutSignInEmail.setBackground(getResources().getDrawable(R.drawable.bg_sign_in_circle_stroke_white_white));
        mLayoutErrorEmail.setVisibility(View.GONE);
        mIBtnErrorEmail.setVisibility(View.GONE);
        mLayoutErrorMessage.setVisibility(View.GONE);
    }

    private void hideMessageErrorEntryCode() {
        mLayoutSignInEntryCode.setBackground(getResources().getDrawable(R.drawable.bg_sign_in_circle_stroke_white_white));
        mLayoutErrorEntry.setVisibility(View.GONE);
        mIBtnErrorEntryCode.setVisibility(View.GONE);
        mLayoutErrorMessage.setVisibility(View.GONE);
    }

    private void hideMessageErrorPass() {
        mLayoutSignInPass.setBackground(getResources().getDrawable(R.drawable.bg_sign_in_circle_stroke_white_white));
        mLayoutErrorPass.setVisibility(View.GONE);
        mIBtnErrorPass.setVisibility(View.GONE);
        mLayoutErrorMessage.setVisibility(View.GONE);
        ivShowHidePassword.setVisibility(View.VISIBLE);
    }


    @Override
    public void onResponse(final Call call,
                           final Response response) {
        if (response.body() instanceof SignInResponse) {
            hideDialogProgress();
            if (getActivity() == null) {
                return;
            }
            SignInResponse signInResponse = ((SignInResponse) response.body());
            if (signInResponse.isSuccess()) {
                if (signInResponse.getData() != null &&
                        signInResponse.getData().getUserItem() != null &&
                        UserItem.isLoginSuccess(signInResponse.getData().getUserItem())) {
                    UserItem.savePreference(signInResponse.getData()
                            .getUserItem(), mEdtEntryCode.getText().toString().trim());
                }
                Intent intent = new Intent(getActivity(),
                        HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

                getActivity().finish();
            } else {
                showErrorMessage();
            }
        }

    }

    @Override
    public void onFailure(final Call call,
                          final Throwable t) {
        hideDialogProgress();
        if (getActivity() == null) {
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putString(ErrorFragment.PRE_ERROR_MESSAGE,
                getString(R.string.text_sign_in_error_message));
        ErrorFragment fragment = new ErrorFragment();
        fragment.setArguments(bundle);
        pushFragment(fragment,
                true,
                true);

    }

    @Override
    public void onB2CResponse(B2CBaseResponse response) {
        hideDialogProgress();
        if (getActivity() == null) {
            return;
        }
        if (response != null && response.isSuccess()) {
            if (UserItem.isLogined()) {
                String loginEmail =
                        SharedPreferencesUtils.getPreferences(AppConstant.USER_EMAIL_PRE,
                                "");
                boolean isForceChangePassword = SharedPreferencesUtils.getPreferences(loginEmail,
                        false);
                if (((B2CLoginResponse) response).isHasForgotPassword()) { // Go to change password
                    SharedPreferencesUtils.removeKey(loginEmail);
                    Bundle bundle = new Bundle();
                    bundle.putString(ChangePasswordFragment.PRE_FORCE_CHANGE_PASS,
                            ChangePasswordFragment.IS_FORCE_CHANGE_PASS);
                    ChangePasswordFragment changePasswordFragment = new ChangePasswordFragment();
                    changePasswordFragment.setArguments(bundle);
                    pushFragment(changePasswordFragment,
                            true,
                            true);
                } else {
                    UserItem.saveClientCode(mEdtEntryCode.getText().toString().trim());
                    Intent intent = new Intent(getActivity(),
                            HomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                    getActivity().finish();
                }
            }
        } else {
            showErrorMessage();
        }
    }

    @Override
    public void onB2CResponseOnList(List<B2CBaseResponse> responseList) {
        if (getActivity() == null) {
            return;
        }
    }

    @Override
    public void onB2CFailure(String errorMessage,
                             String errorCode) { hideDialogProgress();
        if (getActivity() == null) {
            return;
        }
        SharedPreferencesUtils.removeKey(USER_VIP_STATUS);
        showErrorMessage();
    }

    private void setUserAgreementSpannableString() {

        Spannable ss = new SpannableString(mTvTermsOfUse.getText());
        ForegroundColorSpan fcs = new ForegroundColorSpan(getResources().getColor(R.color.white));
        ClickableSpan termsOfUseClickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                ((LoginAcitivy) getActivity()).showWebStatic(getString(R.string.text_title_terms_of_use), getString(R.string.text_path_terms_of_use));
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };

        ClickableSpan privacyPolicyClickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                ((LoginAcitivy) getActivity()).showWebStatic(getString(R.string.text_title_privacy_policy), getString(R.string.text_path_privacy_policy));
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };

        ss.setSpan(termsOfUseClickableSpan, 35, 63, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(privacyPolicyClickableSpan, 66, 94, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(fcs, 35, 63, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(fcs, 66, 94, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(new ColoredUnderlineSpan(mTvTermsOfUse.getText().toString(), R.color.white), 35, 63, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(new ColoredUnderlineSpan(mTvTermsOfUse.getText().toString(), R.color.white), 66, 94, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        mTvTermsOfUse.setText(ss);
        mTvTermsOfUse.setMovementMethod(LinkMovementMethod.getInstance());
        mTvTermsOfUse.setHighlightColor(Color.TRANSPARENT);
    }
}
