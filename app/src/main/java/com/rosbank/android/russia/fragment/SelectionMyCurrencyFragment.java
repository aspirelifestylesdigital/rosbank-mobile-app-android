package com.rosbank.android.russia.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.activitys.HomeActivity;
import com.rosbank.android.russia.adapter.SelectionCurrencyAdapter;
import com.rosbank.android.russia.apiservices.ResponseModel.GetMyCurrencyResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.SaveMyCurrencyResponse;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.model.CurrencyObject;
import com.rosbank.android.russia.utils.SharedPreferencesUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by anh.trinh on 9/13/2016.
 */
public class SelectionMyCurrencyFragment      extends BaseFragment
        implements Callback {

    @Override
    public void onResponse(final Call call,
                           final Response response) {
        if (response.body() instanceof GetMyCurrencyResponse) {
            hideDialogProgress();
            GetMyCurrencyResponse getMyCurrencyResponse = ((GetMyCurrencyResponse) response.body());
            int code = getMyCurrencyResponse.getStatus();
            if (code == 200) {
                dataSelected = getMyCurrencyResponse.getCurrencyObject();
            }
            initCurrencyData();
        }
        if (response.body() instanceof SaveMyCurrencyResponse) {
            hideDialogProgress();
            SaveMyCurrencyResponse saveMyCurrencyResponse =
                    ((SaveMyCurrencyResponse) response.body());
            int code = saveMyCurrencyResponse.getStatus();
            if (code == 200) {
                onBackPress();
            } else {
//                showToast(saveMyCurrencyResponse.getMessage());
            }
        }

    }

    @Override
    public void onFailure(final Call call,
                          final Throwable t) {
        hideDialogProgress();
//        showToast(getString(R.string.text_server_error_message));
        /*Bundle bundle = new Bundle();
        bundle.putString(ErrorHomeFragment.PRE_ERROR_MESSAGE,
                         getString(R.string.text_server_error_message));
        ErrorHomeFragment fragment = new ErrorHomeFragment();
        fragment.setArguments(bundle);
        pushFragment(fragment,
                     true,
                     true);*/
    }

    public interface SelectionCallback {
        void onFinishSelection(Bundle bundle);
    }

    public static final String PRE_SELECTION_DATA = "selection_data";
    public static final String PRE_SELECTION_TYPE = "selection_type";
    public static final String PRE_SELECTION_ROOM = "ROOM";
    public static final String PRE_SELECTION_BED = "BED";
    @BindView(R.id.listview)
    ListView mListview;
    String typeSelection;
    ArrayList<CurrencyObject> data = new ArrayList<CurrencyObject>();
    SelectionCurrencyAdapter adapter;
    CurrencyObject dataSelected = null;
    String mUserID = "";
    SelectionCallback selectionCallback;

    public void setSelectionCallBack(SelectionCallback selectionCallBack) {
        this.selectionCallback = selectionCallBack;
    }


    @Override
    protected int layoutId() {
        return R.layout.fragment_selection_bed_room;
    }

    @Override
    protected void initView() {
        mListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> adapterView,
                                    final View view,
                                    final int i,
                                    final long l) {
                if (i != 0 && i != 4) {
                    // Update to preference
                    SharedPreferencesUtils.setPreferences(AppConstant.PRE_MY_CURRENCY_KEY, data.get(i).getCurrencyKey());
                    SharedPreferencesUtils.setPreferences(AppConstant.PRE_MY_CURRENCY_TEXT, data.get(i).getCurrencyText());

                    adapter.setSelectedData(data.get(i));
                    adapter.notifyDataSetChanged();
                    onBackPress();
                    /*SaveMyCurrencyRequest saveMyCurrencyRequest = new SaveMyCurrencyRequest();
                    CurrencyObject currencyObject = data.get(i);
                    if (!mUserID.equals("")) {
                        saveMyCurrencyRequest.setUserID(mUserID);
                    }
                    if (currencyObject != null) {
                        if (currencyObject.getCurrencyKey() != null) {
                            saveMyCurrencyRequest.setCurrencyKey(currencyObject.getCurrencyKey());
                        }
                        if (currencyObject.getCurrencyText() != null) {
                            saveMyCurrencyRequest.setCurrencyText(currencyObject.getCurrencyText());
                        }
                    }*/
//                    showDialogProgress();
//                    WSSaveMyCurrency wsSaveMyCurrency = new WSSaveMyCurrency();
//                    wsSaveMyCurrency.saveMyCurrency(saveMyCurrencyRequest);
//                    wsSaveMyCurrency.run(SelectionMyCurrencyFragment.this);
                }
            }
        });
        mUserID = SharedPreferencesUtils.getPreferences(AppConstant.USER_ID_PRE,
                                                        "");
        /*if (!mUserID.equals("")) {
            showDialogProgress();
            WSGetMyCurrency wsGetMyCurrency = new WSGetMyCurrency();
            wsGetMyCurrency.getMyCurrency(mUserID);
            wsGetMyCurrency.run(this);
        }*/


    }

    @Override
    protected void bindData() {


    }

    @Override
    public boolean onBack() {
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showLogoApp(false);
            //((HomeActivity) getActivity()).hideToolbarMenuIcon();
            ((HomeActivity) getActivity()).setTitle(getString(R.string.text_title_my_currency));

        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showToolbarMenuIcon();

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater,
                                           container,
                                           savedInstanceState);
        ButterKnife.bind(this,
                         rootView);
        initCurrencyData();
        return rootView;
    }
    private void initCurrencyData() {
        if(getActivity()==null) {
            return;
        }
        CurrencyObject currencyObjectPopular = new CurrencyObject();
        currencyObjectPopular.setCurrencyKey("");
        currencyObjectPopular.setCurrencyText(getString(R.string.text_account_my_currency_popular));
        data.add(currencyObjectPopular);
        CurrencyObject currencyObject17 = new CurrencyObject();
        currencyObject17.setCurrencyKey("РУБЛЬ");
        currencyObject17.setCurrencyText("Russian ruble");
        data.add(currencyObject17);
        CurrencyObject currencyObject = new CurrencyObject();
        currencyObject.setCurrencyKey("USD");
        currencyObject.setCurrencyText("US dollar");
        data.add(currencyObject);
        CurrencyObject currencyObject1 = new CurrencyObject();
        currencyObject1.setCurrencyKey("EUR");
        currencyObject1.setCurrencyText("Euro");
        data.add(currencyObject1);
        CurrencyObject currencyObjectAllCurrency = new CurrencyObject();
        currencyObjectAllCurrency.setCurrencyKey("");
        currencyObjectAllCurrency.setCurrencyText(getString(R.string.text_account_my_currency_all));
        data.add(currencyObjectAllCurrency);
        CurrencyObject currencyObject3 = new CurrencyObject();
        currencyObject3.setCurrencyKey("AUD");
        currencyObject3.setCurrencyText("Australian dollar");
        data.add(currencyObject3);
        CurrencyObject currencyObject5 = new CurrencyObject();
        currencyObject5.setCurrencyKey("CAD");
        currencyObject5.setCurrencyText("Canadian dollar");
        data.add(currencyObject5);
        CurrencyObject currencyObject6 = new CurrencyObject();
        currencyObject6.setCurrencyKey("CNY");
        currencyObject6.setCurrencyText("Chinese yuan");
        data.add(currencyObject6);
        CurrencyObject currencyObject7 = new CurrencyObject();
        currencyObject7.setCurrencyKey("EGP");
        currencyObject7.setCurrencyText("Egyptian pound");
        data.add(currencyObject7);
        CurrencyObject currencyObject8 = new CurrencyObject();
        currencyObject8.setCurrencyKey("HKD");
        currencyObject8.setCurrencyText("Hong Kong dollar");
        data.add(currencyObject8);
        CurrencyObject currencyObject9 = new CurrencyObject();
        currencyObject9.setCurrencyKey("Rs.");
        currencyObject9.setCurrencyText("Indian rupee");
        data.add(currencyObject9);
        CurrencyObject currencyObject10 = new CurrencyObject();
        currencyObject10.setCurrencyKey("Rp");
        currencyObject10.setCurrencyText("Indonesia rupiah");
        data.add(currencyObject10);
        CurrencyObject currencyObject11 = new CurrencyObject();
        currencyObject11.setCurrencyKey("¥");
        currencyObject11.setCurrencyText("Japanese yen");
        data.add(currencyObject11);
        CurrencyObject currencyObject12 = new CurrencyObject();
        currencyObject12.setCurrencyKey("KRW");
        currencyObject12.setCurrencyText("Korean won");
        data.add(currencyObject12);
        CurrencyObject currencyObject23 = new CurrencyObject();
        currencyObject23.setCurrencyKey("KWD");
        currencyObject23.setCurrencyText("Kuwaiti dinar");
        data.add(currencyObject23);
        CurrencyObject currencyObject13 = new CurrencyObject();
        currencyObject13.setCurrencyKey("MYR");
        currencyObject13.setCurrencyText("Malaysia ringgit");
        data.add(currencyObject13);
        CurrencyObject currencyObject14 = new CurrencyObject();
        currencyObject14.setCurrencyKey("MXN");
        currencyObject14.setCurrencyText("Mexican peso");
        data.add(currencyObject14);
        CurrencyObject currencyObject15 = new CurrencyObject();
        currencyObject15.setCurrencyKey("TWD");
        currencyObject15.setCurrencyText("New Taiwan dollar");
        data.add(currencyObject15);
        CurrencyObject currencyObject16 = new CurrencyObject();
        currencyObject16.setCurrencyKey("£");
        currencyObject16.setCurrencyText("Pound sterling");
        data.add(currencyObject16);

        CurrencyObject currencyObject18 = new CurrencyObject();
        currencyObject18.setCurrencyKey("SAR");
        currencyObject18.setCurrencyText("Saudi Arabian riyal");
        data.add(currencyObject18);
        CurrencyObject currencyObject2 = new CurrencyObject();
        currencyObject2.setCurrencyKey("SGD");
        currencyObject2.setCurrencyText("Singapore");
        data.add(currencyObject2);
        CurrencyObject currencyObject19 = new CurrencyObject();
        currencyObject19.setCurrencyKey("ZAR");
        currencyObject19.setCurrencyText("South African rand");
        data.add(currencyObject19);
        CurrencyObject currencyObject20 = new CurrencyObject();
        currencyObject20.setCurrencyKey("SEK");
        currencyObject20.setCurrencyText("Swedish krona");
        data.add(currencyObject20);
        CurrencyObject currencyObject21 = new CurrencyObject();
        currencyObject21.setCurrencyKey("CHF");
        currencyObject21.setCurrencyText("Swiss franc");
        data.add(currencyObject21);
        CurrencyObject currencyObject22 = new CurrencyObject();
        currencyObject22.setCurrencyKey("THB");
        currencyObject22.setCurrencyText("Thai baht");
        data.add(currencyObject22);
        String selectedCurrencyKeyInPref = SharedPreferencesUtils.getPreferences(AppConstant.PRE_MY_CURRENCY_KEY, "");
        String selectedCurrencyTextInPref = SharedPreferencesUtils.getPreferences(AppConstant.PRE_MY_CURRENCY_TEXT, "");
        if(TextUtils.isEmpty(selectedCurrencyKeyInPref)){
            dataSelected = data.get(1);
        }else{
            dataSelected = new CurrencyObject();
            dataSelected.setCurrencyKey(selectedCurrencyKeyInPref);
            dataSelected.setCurrencyText(selectedCurrencyTextInPref);
        }
        adapter = new SelectionCurrencyAdapter(getActivity(),
                                               data,
                                               dataSelected);
        mListview.setAdapter(adapter);
    }


}
