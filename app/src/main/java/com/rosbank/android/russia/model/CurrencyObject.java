package com.rosbank.android.russia.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;


public class CurrencyObject
        implements Parcelable {
    @Expose
    private String CurrencyKey;

    public String getCurrencyKey() {
        return CurrencyKey;
    }

    public void setCurrencyKey(final String currencyKey) {
        CurrencyKey = currencyKey;
    }

    public String getCurrencyText() {
        return CurrencyText;
    }

    public void setCurrencyText(final String currencyText) {
        CurrencyText = currencyText;
    }

    @Expose
    private String CurrencyText;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel parcel,
                              final int i) {
        parcel.writeString(this.CurrencyKey);
        parcel.writeString(this.CurrencyText);

    }
    protected CurrencyObject(Parcel in) {
        this.CurrencyKey = in.readString();
        this.CurrencyText = in.readString();
    }
    public CurrencyObject() {
        this.CurrencyKey = "";
        this.CurrencyText = "";
    }

    public static final Creator<CurrencyObject> CREATOR = new Creator<CurrencyObject>() {
        @Override
        public CurrencyObject createFromParcel(Parcel source) {
            return new CurrencyObject(source);
        }

        @Override
        public CurrencyObject[] newArray(int size) {
            return new CurrencyObject[size];
        }
    };
}
