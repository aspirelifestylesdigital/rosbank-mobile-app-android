package com.rosbank.android.russia.apiservices.b2c;

import com.rosbank.android.russia.BuildConfig;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CApiProviderClient;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CICallback;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CRefreshTokenResponse;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.model.UserItem;
import com.rosbank.android.russia.utils.SharedPreferencesUtils;

import java.util.Map;

import retrofit2.Callback;

public class B2CWSRefreshToken
        extends B2CApiProviderClient<B2CRefreshTokenResponse>{

    public B2CWSRefreshToken(B2CICallback callback){
        this.b2CICallback = callback;
    }
    @Override
    public void run(Callback<B2CRefreshTokenResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }
    @Override
    protected boolean isUseAuthenticateInApi() {
        return false;
    }

    @Override
    protected void runApi() {
        serviceInterface.refreshToken(/*BuildConfig.B2C_CONSUMER_KEY*/UserItem.isVip() ? BuildConfig.B2C_CONSUMER_KEY_PRIVATE : BuildConfig.B2C_CONSUMER_KEY_REGULAR,
                        SharedPreferencesUtils.getPreferences(AppConstant.PRE_B2C_ACCESS_TOKEN, ""),
                        SharedPreferencesUtils.getPreferences(AppConstant.PRE_B2C_REFRESH_TOKEN, "")).enqueue(this);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        return null;
    }

    @Override
    protected void postResponse(B2CRefreshTokenResponse response) {
    }
}
