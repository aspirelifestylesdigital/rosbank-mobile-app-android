package com.rosbank.android.russia.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.rosbank.android.russia.fragment.SplashFourFragment;
import com.rosbank.android.russia.fragment.SplashOneFragment;
import com.rosbank.android.russia.fragment.SplashThreeFragment;

/**
 * Created by anh.trinh on 9/15/2016.
 */
public class SplashPageAdapter extends FragmentPagerAdapter {
    private Context mContext;
    public static int totalPage = 30000;
    public static int actualPage = 3;

    public SplashPageAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }
    @Override
    public Fragment getItem(int position) {
        Fragment f = new Fragment();
        position = position % 3;
        switch (position) {
            case 0:
                f = new SplashOneFragment();
                break;
           /* case 1:
                f = new SplashTwoFragment();
                break;*/
            case 1:
                f = new SplashThreeFragment();
                break;
            case 2:
                f = new SplashFourFragment();
                break;
        }
        return f;
    }
    @Override
    public int getCount() {
        return totalPage;
    }
}
