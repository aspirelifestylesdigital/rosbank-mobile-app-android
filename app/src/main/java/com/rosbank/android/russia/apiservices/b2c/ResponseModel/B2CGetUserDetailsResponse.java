package com.rosbank.android.russia.apiservices.b2c.ResponseModel;

import com.google.gson.annotations.Expose;

import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.rosbank.android.russia.utils.CommonUtils;

import java.util.List;

/**
 * Created by ThuNguyen on 10/26/2016.
 */

public class B2CGetUserDetailsResponse extends B2CBaseResponse{

    public boolean isSuccess(){
        return true; // Because this response didn't define the "Status" or "success" field
    }
    @Expose
    private MemberExt Member;
    @Expose
    private List<MemberDetailExt> MemberDetails;

    public class MemberExt{
        @Expose
        private String EMAIL;
        @Expose
        private String FIRSTNAME;
        @Expose
        private String LASTNAME;
        @Expose
        private String ONLINEMEMBERID;
        @Expose
        private String SALUTATION;
        @Expose
        private String MOBILENUMBER;
        @Expose
        private String MemberRefNo;
        @Expose
        private String PRODUCTNAME;
        @Expose
        private String ACCOUNTTYPE;
        @Expose
        private String ASSITANTNNAME;
        @Expose
        private String BIN;
        @Expose
        private String DOB;
        @Expose
        private String DATEMBR;
        @Expose
        private String MULTITAG;
        @Expose
        private String THANKYOUPOINTS;
        @Expose
        private String Address1;
        @Expose
        private String Address2;
        @Expose
        private String Address3;
        @Expose
        private String City;
        @Expose
        private String State;
        @Expose
        private String Country;
        @Expose
        private String POSTALCODE;
        @Expose
        private String Email;
        @Expose
        private String HomePhone;
        @Expose
        private String MobilePhone;
        @Expose
        private String WorkPhone;
        @Expose
        private String Vip;

        public String getEMAIL() {
            return EMAIL;
        }

        public void setEMAIL(String EMAIL) {
            this.EMAIL = EMAIL;
        }

        public String getFIRSTNAME() {
            return FIRSTNAME;
        }

        public void setFIRSTNAME(String FIRSTNAME) {
            this.FIRSTNAME = FIRSTNAME;
        }

        public String getLASTNAME() {
            return LASTNAME;
        }

        public void setLASTNAME(String LASTNAME) {
            this.LASTNAME = LASTNAME;
        }
        public String getFullName(){
            return FIRSTNAME + " " + LASTNAME;
        }
        public String getONLINEMEMBERID() {
            return ONLINEMEMBERID;
        }

        public void setONLINEMEMBERID(String ONLINEMEMBERID) {
            this.ONLINEMEMBERID = ONLINEMEMBERID;
        }

        public String getSALUTATION() {
            return CommonUtils.convertSalutationToRussian(SALUTATION);
        }

        public void setSALUTATION(String SALUTATION) {
            this.SALUTATION = SALUTATION;
        }

        public String getMemberRefNo() {
            return MemberRefNo;
        }

        public void setMemberRefNo(String memberRefNo) {
            MemberRefNo = memberRefNo;
        }

        public String getMOBILENUMBER() {
            return MOBILENUMBER;
        }

        public void setMOBILENUMBER(String MOBILENUMBER) {
            this.MOBILENUMBER = MOBILENUMBER;
        }

        public String getPRODUCTNAME() {
            return PRODUCTNAME;
        }

        public void setPRODUCTNAME(String PRODUCTNAME) {
            this.PRODUCTNAME = PRODUCTNAME;
        }

        public String getACCOUNTTYPE() {
            return ACCOUNTTYPE;
        }

        public void setACCOUNTTYPE(String ACCOUNTTYPE) {
            this.ACCOUNTTYPE = ACCOUNTTYPE;
        }

        public String getASSITANTNNAME() {
            return ASSITANTNNAME;
        }

        public void setASSITANTNNAME(String ASSITANTNNAME) {
            this.ASSITANTNNAME = ASSITANTNNAME;
        }

        public String getBIN() {
            return BIN;
        }

        public void setBIN(String BIN) {
            this.BIN = BIN;
        }

        public String getDOB() {
            return DOB;
        }

        public void setDOB(String DOB) {
            this.DOB = DOB;
        }

        public String getDATEMBR() {
            return DATEMBR;
        }

        public void setDATEMBR(String DATEMBR) {
            this.DATEMBR = DATEMBR;
        }

        public String getMULTITAG() {
            return MULTITAG;
        }

        public void setMULTITAG(String MULTITAG) {
            this.MULTITAG = MULTITAG;
        }

        public String getTHANKYOUPOINTS() {
            return THANKYOUPOINTS;
        }

        public void setTHANKYOUPOINTS(String THANKYOUPOINTS) {
            this.THANKYOUPOINTS = THANKYOUPOINTS;
        }

        public String getAddress1() {
            return Address1;
        }

        public void setAddress1(String address1) {
            Address1 = address1;
        }

        public String getAddress2() {
            return Address2;
        }

        public void setAddress2(String address2) {
            Address2 = address2;
        }

        public String getAddress3() {
            return Address3;
        }

        public void setAddress3(String address3) {
            Address3 = address3;
        }

        public String getCity() {
            return City;
        }

        public void setCity(String city) {
            City = city;
        }

        public String getState() {
            return State;
        }

        public void setState(String state) {
            State = state;
        }

        public String getCountry() {
            return Country;
        }

        public void setCountry(String country) {
            Country = country;
        }

        public String getPOSTALCODE() {
            return POSTALCODE;
        }

        public void setPOSTALCODE(String POSTALCODE) {
            this.POSTALCODE = POSTALCODE;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String email) {
            Email = email;
        }

        public String getHomePhone() {
            return HomePhone;
        }

        public void setHomePhone(String homePhone) {
            HomePhone = homePhone;
        }

        public String getMobilePhone() {
            return MobilePhone;
        }

        public void setMobilePhone(String mobilePhone) {
            MobilePhone = mobilePhone;
        }

        public String getWorkPhone() {
            return WorkPhone;
        }

        public void setWorkPhone(String workPhone) {
            WorkPhone = workPhone;
        }

        public String getVip() {
            return Vip;
        }

        public void setVip(String vip) {
            Vip = vip;
        }
    }
    public class MemberDetailExt{
        @Expose
        private String DELETE;
        @Expose
        private String ONLINEMEMBERDETAILID;
        @Expose
        private String VeriCode;

        public String getVeriCode() {
            return VeriCode;
        }

        public void setVeriCode(String veriCode) {
            this.VeriCode = veriCode;
        }

        public String getONLINEMEMBERDETAILID() {
            return ONLINEMEMBERDETAILID;
        }

        public void setONLINEMEMBERDETAILID(String ONLINEMEMBERDETAILID) {
            this.ONLINEMEMBERDETAILID = ONLINEMEMBERDETAILID;
        }

        public String getDELETE() {
            return DELETE;
        }

        public void setDELETE(String DELETE) {
            this.DELETE = DELETE;
        }
    }

    public MemberExt getMember() {
        return Member;
    }

    public void setMember(MemberExt member) {
        Member = member;
    }

    public List<MemberDetailExt> getMemberDetails() {
        return MemberDetails;
    }

    public void setMemberDetails(List<MemberDetailExt> memberDetails) {
        MemberDetails = memberDetails;
    }
}
