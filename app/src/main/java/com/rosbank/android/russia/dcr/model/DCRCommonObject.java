package com.rosbank.android.russia.dcr.model;

import com.google.gson.annotations.Expose;

import java.io.Serializable;


public class DCRCommonObject
        implements Serializable {
    public DCRCommonObject(String name) {
            this.name = name;
    }

    @Expose
    private String id;

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
            this.name = name;
    }

    @Expose
    private String name;

    public String getCode() {
        return code;
    }

    public void setCode(final String code) {
        this.code = code;
    }

    @Expose
    private String code;

    @Override
    public boolean equals(Object obj) {
        if (obj != null && obj instanceof DCRCommonObject) {
            DCRCommonObject commonObject = (DCRCommonObject) obj;
            return (name != null && name.equals(commonObject.getName())) ||
                    (id != null && id.equals(commonObject.getId()));
        }
        return false;
    }

}
