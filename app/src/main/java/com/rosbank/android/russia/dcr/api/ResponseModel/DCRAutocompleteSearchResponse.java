package com.rosbank.android.russia.dcr.api.ResponseModel;

import com.rosbank.android.russia.dcr.model.DCRAutocompleteSearchObject;
import com.google.gson.annotations.Expose;

import java.util.List;

/**
 * Created by ThuNguyen on 4/5/2017.
 */

public class DCRAutocompleteSearchResponse extends DCRBaseResponse {
    @Expose
    List<DCRAutocompleteSearchObject> data;

    public List<DCRAutocompleteSearchObject> getData() {
        return data;
    }

    public void setData(List<DCRAutocompleteSearchObject> data) {
        this.data = data;
    }
}
