package com.rosbank.android.russia.apiservices;

import com.rosbank.android.russia.apiservices.RequestModel.UpdateHotelPreferencesRequest;
import com.rosbank.android.russia.apiservices.ResponseModel.UpdateHotelPreferencesResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.ApiProviderClient;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Callback;


public class WSCreateUpdateHotelPreferences
        extends ApiProviderClient<UpdateHotelPreferencesResponse> {
    private UpdateHotelPreferencesRequest request;

    public WSCreateUpdateHotelPreferences(){

    }
    public void updateTransportPreferencesRequest(final UpdateHotelPreferencesRequest request){
        this.request = request;
    }

    @Override
    public void run(Callback<UpdateHotelPreferencesResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }

    @Override
    protected boolean isUseAuthenticateInApi() {
        return true;
    }

    @Override
    public void runApi() {

        serviceInterface.updateHotelPreferencesResponse(request).enqueue(callback);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("User-Agent", "Retrofit-Sample-App");
        return headers;
    }

}
