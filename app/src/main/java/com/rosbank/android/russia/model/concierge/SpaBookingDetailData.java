package com.rosbank.android.russia.model.concierge;

import android.text.TextUtils;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CGetRecentRequestResponse;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.application.AppContext;
import com.rosbank.android.russia.utils.DateTimeUtil;

/**
 * Created by ThuNguyen on 10/26/2016.
 */

public class SpaBookingDetailData
        extends MyRequestObject {


    private String SpaName;
    private String CreateDate;
    private long createDateEpoc;

    public SpaBookingDetailData(B2CGetRecentRequestResponse recentRequestResponse) {
        super(recentRequestResponse);
        CreateDate = recentRequestResponse.getCREATEDDATE();
        createDateEpoc = DateTimeUtil.shareInstance().convertToTimeStampFromGMT(CreateDate, AppConstant.DATE_FORMAT_YYYY_MM_DD_T_HH_MM_SS);
    }
    @Override
    protected void initRequestProperties() {
        // Booking type group
        BookingFilterGroupType = AppConstant.BOOKING_FILTER_TYPE.Spa;

        // Item title
        ItemTitle = conciergeRequestDetail.getSpaName();

        // Item description
       // ItemDescription = conciergeRequestDetail.getSpecialRequirement();
        SpaName =conciergeRequestDetail.getSpaName();
    }

    @Override
    public boolean isUpcoming() {
        return !TextUtils.isEmpty(RequestMode) && !RequestMode.equalsIgnoreCase("cancel");
    }

    @Override
    public boolean isAmendCancelPermitted() {
        return !TextUtils.isEmpty(RequestMode) && !RequestMode.equalsIgnoreCase("cancel");
    }

    @Override
    public boolean isBookNormalType() {
        return true;
    }

    @Override
    public long getRequestStartDateEpoch() {
        return 0;
    }

    @Override
    public long getRequestEndDateEpoch() {
        return 0;
    }

    @Override
    public String getReservationName() {
        return conciergeRequestDetail.getReservationName();
    }

    public String getSpaName() {
        return SpaName;
    }

    public void setSpaName(String spaName) {
        SpaName = spaName;
    }
    public long getCreateDateEpoc() {
        return createDateEpoc;
    }

}
