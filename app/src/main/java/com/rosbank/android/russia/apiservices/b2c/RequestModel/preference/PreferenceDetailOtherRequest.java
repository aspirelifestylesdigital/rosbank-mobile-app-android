package com.rosbank.android.russia.apiservices.b2c.RequestModel.preference;

import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.utils.StringUtil;
import com.google.gson.annotations.Expose;

import java.util.List;

/**
 * Created by Thu Nguyen on 12/12/2016.
 */

public class PreferenceDetailOtherRequest extends PreferenceDetailAddRequest{
    @Expose
    private String Anyothercomments;

    public PreferenceDetailOtherRequest(String Delete) {
        super(Delete, AppConstant.PREFERENCE_TYPE.OTHER.getValue());
    }

    @Override
    public void fillData(Object... values) {
        List<String> valueList = StringUtil.removeAllSpecialCharactersAndFillInList(values);
        if(valueList != null){
            if(valueList.size() > 0){
                Anyothercomments = valueList.get(0);
            }
        }
    }
}
