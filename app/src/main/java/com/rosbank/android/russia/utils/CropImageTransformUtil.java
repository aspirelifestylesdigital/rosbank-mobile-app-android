package com.rosbank.android.russia.utils;

import android.graphics.Bitmap;

//import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
//import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;

/*
 * Created by chau.nguyen on 10/18/2016.
 */
public class CropImageTransformUtil/* extends BitmapTransformation */{

//    public CropImageTransformUtil(Context context) {
//        super(context);
//    }
//
//    @Override
//    protected Bitmap transform(BitmapPool pool, Bitmap toTransform, int outWidth, int outHeight) {
//        return cropImg(toTransform);
//    }

    private Bitmap cropImg(Bitmap source) {
        if (source == null) return null;

        Bitmap temp;
        int x = source.getWidth();
        int y = source.getHeight();
        if (x < y) {
            y = (y / 2);
            temp = Bitmap.createBitmap(source, 0, 0, source.getWidth(), y);
        } else {
            temp = source;
        }
        return temp;
    }

//    @Override
//    public String getId() {
//        return getClass().getName();
//    }
}