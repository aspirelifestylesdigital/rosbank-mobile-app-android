package com.rosbank.android.russia.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.model.Galleries;
//import com.bumptech.glide.Glide;
//import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

/*
 * Created by chau.nguyen on 10/25/2016.
 */
public class ExperiencesDetailGalleryAdapter extends PagerAdapter {

    private Activity activity;
    private List<Galleries> listGallery;
    private int width, height;

    public ExperiencesDetailGalleryAdapter(Activity activity, List<Galleries> listGallery, int width, int height) {
        this.activity = activity;
        this.listGallery = listGallery;
        this.width = width;
        this.height = height;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.layout_item_experience_gourmet_gallery, container, false);

        ImageView imgViewPreview = (ImageView) view.findViewById(R.id.imgGallery);

//        Glide.with(activity).load(listGallery.get(position).getBackgroundImageUrl())
//                .thumbnail(0.5f)
//                .centerCrop()
//                .override(width, height)
//                .crossFade()
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .into(imgViewPreview);

        container.addView(view);

        return view;
    }

    @Override
    public int getCount() {
        return listGallery.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
