package com.rosbank.android.russia.apiservices.ResponseModel;

import com.rosbank.android.russia.apiservices.apis.appimplement.BaseResponse;
import com.rosbank.android.russia.model.HotelRecommendationBookingDetailData;

/**
 * Created by ThuNguyen on 10/26/2016.
 */

public class HotelRecommendationBookingDetailResponse extends BaseResponse {
    private HotelRecommendationBookingDetailData Data;

    public HotelRecommendationBookingDetailData getData() {
        return Data;
    }

    public void setData(HotelRecommendationBookingDetailData data) {
        Data = data;
    }
}
