package com.rosbank.android.russia.model;

import com.google.gson.annotations.Expose;

/**
 * Created by nga.nguyent on 9/22/2016.
 */
public class Menu {
    @Expose
    private String ID;
    @Expose
    private String GroupName;
    @Expose
    private String Name;
    @Expose
    private String Description;
    @Expose
    private String Price;
    @Expose
    private String ImageUrl;
    @Expose
    private String Thumbnail;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getGroupName() {
        return GroupName;
    }

    public void setGroupName(String groupName) {
        GroupName = groupName;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }

    public String getThumbnail() {
        return Thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        Thumbnail = thumbnail;
    }
}
