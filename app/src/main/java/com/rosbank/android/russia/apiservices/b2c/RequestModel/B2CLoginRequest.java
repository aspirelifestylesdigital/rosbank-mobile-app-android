package com.rosbank.android.russia.apiservices.b2c.RequestModel;

import com.rosbank.android.russia.BuildConfig;
import com.rosbank.android.russia.apiservices.RequestModel.BaseRequest;
import com.google.gson.annotations.Expose;

import static com.rosbank.android.russia.application.AppConstant.CLIENT_CODE_REGULAR;

/**
 * Created by ThuNguyen on 10/26/2016.
 */

public class B2CLoginRequest extends BaseRequest{
    @Expose
    private String ConsumerKey;
    @Expose
    private String Email2;
    @Expose
    private String Functionality;
    @Expose
    private String MemberDeviceId;
    @Expose
    private String Password;

    private String clientCode;

    public B2CLoginRequest(String clientCode){
        ConsumerKey = clientCode.equals(CLIENT_CODE_REGULAR) ? BuildConfig.B2C_CONSUMER_KEY_REGULAR : BuildConfig.B2C_CONSUMER_KEY_PRIVATE;
        Functionality = "Login";
        MemberDeviceId = BuildConfig.B2C_MEMBER_DEVICE_ID;
    }
    public B2CLoginRequest build(String email, String password, String clientCode){
        this.Email2 = email;
        this.Password = password;
        this.clientCode = clientCode;
        return this;
    }

}
