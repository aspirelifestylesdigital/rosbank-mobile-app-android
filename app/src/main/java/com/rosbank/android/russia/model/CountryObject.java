package com.rosbank.android.russia.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.application.AppContext;
import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;


public class CountryObject
        implements Parcelable {
    public CountryObject(String countryName){
        CountryName = countryName;
    }
    public String getId() {
        return Id;
    }

    public void setId(final String id) {
        Id = id;
    }

    public String getCountryCode() {
        return CountryCode;
    }

    public void setCountryCode(final String countryCode) {
        CountryCode = countryCode;
    }

    private static List<CountryObject> countryObjectList = new ArrayList<>();
    private static CountryObject defaultCountry = null;
    private static List<CountryObject> createCountryObjectList(){
        if(countryObjectList.size() == 0) {
            String[] countryNameList = AppContext.getSharedInstance().getResources().getStringArray(R.array.country_name_array);
            String[] countryCodeList = AppContext.getSharedInstance().getResources().getStringArray(R.array.country_code_array);
            for(int index = 0; index < countryNameList.length; index++){
                String countryName = countryNameList[index];
                String countryCode = countryCodeList[index];
                CountryObject countryObject = new CountryObject().build(countryCode, countryName);
                countryObjectList.add(countryObject);
                if(countryCode.equalsIgnoreCase("rus")){
                    defaultCountry = countryObject;
                }
            }
        }
        return countryObjectList;
    }
    public static CountryObject getDefaultCountry(){
        if(defaultCountry == null){
            createCountryObjectList();
        }
        return defaultCountry;
    }
    public static CountryObject getCountryObjectFromName(String countryName){
        createCountryObjectList();
        for(CountryObject countryObject : countryObjectList){
            if(countryObject.getCountryName().equalsIgnoreCase(countryName)){
                return countryObject;
            }
        }
        return null;
    }
    public static List<CountryObject> getCountryObjectList(){
        createCountryObjectList();
        return countryObjectList;
    }
    @Expose
    private String CountryCode;
    @Expose
    private String Id;
    @Expose
    private String CountryName;
    @Expose
    private String RegionOfCountry;

    public String getRegionOfCountry() {
        return RegionOfCountry;
    }

    public void setRegionOfCountry(final String regionOfCountry) {
        RegionOfCountry = regionOfCountry;
    }

    public String getCountryName() {
        return CountryName;
    }

    public void setCountryName(final String countryName) {
        CountryName = countryName;
    }

    @Override
    public String toString() {
        return CountryName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel parcel,
                              final int i) {
        parcel.writeString(this.Id);
        parcel.writeString(this.CountryCode);
        parcel.writeString(this.CountryName);
        parcel.writeString(this.RegionOfCountry);

    }
    protected CountryObject(Parcel in) {
        this.Id = in.readString();
        this.CountryCode = in.readString();
        this.CountryName = in.readString();
        this.RegionOfCountry = in.readString();
    }
    public CountryObject() {
        this.Id = "";
        this.CountryCode = "";
        this.CountryName = "";
        this.RegionOfCountry = "";
    }
    public CountryObject build(String countryCode, String countryName){
        this.Id = countryCode;
        this.CountryCode = countryCode;
        this.CountryName = countryName;
        return this;
    }
    public static final Creator<CountryObject> CREATOR = new Creator<CountryObject>() {
        @Override
        public CountryObject createFromParcel(Parcel source) {
            return new CountryObject(source);
        }

        @Override
        public CountryObject[] newArray(int size) {
            return new CountryObject[size];
        }
    };
}
