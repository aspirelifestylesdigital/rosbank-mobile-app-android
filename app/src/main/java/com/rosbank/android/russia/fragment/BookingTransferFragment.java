package com.rosbank.android.russia.fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.activitys.HomeActivity;
import com.rosbank.android.russia.apiservices.ResponseModel.BookTransferResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.CarTransferBookingDetailResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CICallback;
import com.rosbank.android.russia.apiservices.b2c.B2CWSGetRequestDetail;
import com.rosbank.android.russia.apiservices.b2c.B2CWSUpsertConciergeRequest;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.B2CUpsertConciergeRequestRequest;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CGetRecentRequestResponse;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CUpsertConciergeRequestResponse;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.application.AppContext;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.model.CommonObject;
import com.rosbank.android.russia.model.concierge.CarBookingDetailData;
import com.rosbank.android.russia.model.concierge.MyRequestObject;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.DateTimeUtil;
import com.rosbank.android.russia.utils.EntranceLock;
import com.rosbank.android.russia.utils.FontUtils;
import com.rosbank.android.russia.utils.NetworkUtil;
import com.rosbank.android.russia.utils.PhotoPickerUtils;
import com.rosbank.android.russia.utils.StringUtil;
import com.rosbank.android.russia.widgets.AttachePhotoView;
import com.rosbank.android.russia.widgets.ContactView;
import com.rosbank.android.russia.widgets.CustomErrorView;
import com.rosbank.android.russia.widgets.NumberPickerView;

import org.greenrobot.eventbus.EventBus;

import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by anh.trinh on 9/13/2016.
 */
public class BookingTransferFragment
        extends BaseFragment
        implements ContactView.ContactViewListener,
                   SelectionRoom_Bed_Transport_Fragment.SelectionCallback,
                   SelectionFragment.SelectionCallback,
                   Callback, B2CICallback {

    @BindView(R.id.tv_please_select_transport_type)
    TextView mTvPleaseSelectTransportType;
    @BindView(R.id.transport_type_error)
    CustomErrorView mTransportTypeError;
    @BindView(R.id.tv_pick_up_date)
    TextView mTvPickUpDate;
    @BindView(R.id.tv_pick_up_time)
    TextView mTvPickUpTime;
    @BindView(R.id.ervPickupTime)
    CustomErrorView ervPickupTime;
    @BindView(R.id.edt_pick_up_location)
    EditText mEdtPickUpLocation;
    @BindView(R.id.pick_up_location_error)
    CustomErrorView mPickUpLocationError;
    @BindView(R.id.edt_drop_off_loction)
    EditText mEdtDropOffLoction;
    @BindView(R.id.drop_off_loction_error)
    CustomErrorView mDropOffLoctionError;
    @BindView(R.id.picker_no_of_passenger)
    NumberPickerView pickerNoOfPassenger;
    @BindView(R.id.edt_any_special_requirements)
    EditText mEdtAnySpecialRequirements;
    @BindView(R.id.special_req_error)
    CustomErrorView specialReqError;
    @BindView(R.id.photo_attached)
    AttachePhotoView mPhotoAttached;
    @BindView(R.id.btn_submit)
    Button mBtnSubmit;
    @BindView(R.id.btn_cancel)
    Button mBtnCancel;
    @BindView(R.id.layout_bottom_button)
    LinearLayout mLayoutBottomButton;
    @BindView(R.id.container)
    LinearLayout mLayoutContainer;
    @BindView(R.id.scrollView)
    ScrollView mScrollView;
    @BindView(R.id.contact_view)
    ContactView mContactView;
    CommonObject mTransportTypeSelected;
    private Calendar pickCal;

    private MyRequestObject myRequestObject = null;
    private B2CUpsertConciergeRequestRequest upsertConciergeRequestRequest;
    private String uploadedPhotoPath = "";
    private EntranceLock entranceLock = new EntranceLock();
    private boolean isDateTimeSelectedByUser = false;
    private boolean isSubmitClicked = false;

    @Override
    protected int layoutId() {
        return R.layout.fragment_concierge_book_transfer;
    }

    @Override
    protected void initView() {
        CommonUtils.setFontForViewRecursive(mLayoutContainer,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
        CommonUtils.setFontForViewRecursive(mBtnSubmit,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        CommonUtils.setFontForViewRecursive(mBtnCancel,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        CommonUtils.makeFirstCharacterUpperCase(mEdtPickUpLocation, mEdtDropOffLoction);
        specialReqError.setMessage(getString(R.string.special_requirement_error));

        mPhotoAttached.setAttachePhotoCallback(new AttachePhotoView.IAttachePhotoCallback() {
            @Override
            public void onPhotoChanged() {
                // Reset the photo path once the user change photo
                uploadedPhotoPath = "";
            }
        });
        makeDefaultDateTime();
        pickerNoOfPassenger.setMinimum(1);
        mPhotoAttached.setParentFragment(this);
        mContactView.setContactViewListener(this);
        if (getArguments() != null) {
            myRequestObject = (MyRequestObject)getArguments().getSerializable(AppConstant.PRE_REQUEST_OBJECT_DATA);
        }
        if (myRequestObject != null) {
            isDateTimeSelectedByUser = true;
            showDialogProgress();
            B2CWSGetRequestDetail b2CWSGetRequestDetail = new B2CWSGetRequestDetail(this);
            b2CWSGetRequestDetail.setValue(myRequestObject.getEpcCaseId(), myRequestObject.getBookingItemID());
            b2CWSGetRequestDetail.run(null);
        }
    }
    public void makeDefaultDateTime(){
        if(!isDateTimeSelectedByUser) {
            pickCal = Calendar.getInstance();
            pickCal.add(Calendar.DAY_OF_YEAR, 1);
            pickCal.add(Calendar.MINUTE, AppConstant.MINUTE_EXTRA);
            showDateTime();
        }
    }
    private void showDateTime(){
        mTvPickUpDate.setText(DateTimeUtil.shareInstance().formatTimeStamp(pickCal.getTimeInMillis(), AppConstant.DATE_FORMAT_CONCIERGE_BOOKING));
        mTvPickUpTime.setText(DateTimeUtil.shareInstance().formatTimeStamp(pickCal.getTimeInMillis(), AppConstant.TIME_FORMAT_CONCIERGE_BOOKING).toUpperCase());
    }
    @Override
    protected void bindData() {
    }

    @Override
    public boolean onBack() {
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (myRequestObject != null) {
            ((HomeActivity) getActivity()).showLogoApp(false);
            ((HomeActivity) getActivity()).setTitle(getString(R.string.text_title_car_transfer));
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater,
                                           container,
                                           savedInstanceState);
        ButterKnife.bind(this,
                         rootView);


        // Error time
        ervPickupTime.setMessage(getString(R.string.text_error_message_invalid_time));
        return rootView;
    }
    @OnTouch({
            R.id.edt_pick_up_location,
            R.id.edt_drop_off_loction,
            R.id.edt_any_special_requirements})
    boolean onInputTouch(final View v,
                         final MotionEvent event) {

        switch (v.getId()) {
            case R.id.edt_pick_up_location:
                mPickUpLocationError.setVisibility(View.GONE);
                break;
            case R.id.edt_drop_off_loction:
                mDropOffLoctionError.setVisibility(View.GONE);
                break;
            case R.id.edt_any_special_requirements:
                specialReqError.setVisibility(View.GONE);
                break;
        }
        return false;
    }
    @OnClick({R.id.tv_please_select_transport_type,
              R.id.tv_pick_up_date,
              R.id.tv_pick_up_time,
              R.id.btn_submit,
              R.id.btn_cancel})
    public void onClick(View view) {
        CommonUtils.hideSoftKeyboard(getContext());
        switch (view.getId()) {
            case R.id.tv_please_select_transport_type:
                mTransportTypeError.hide();
                Bundle bundle = new Bundle();
                bundle.putString(SelectionRoom_Bed_Transport_Fragment.PRE_SELECTION_TYPE,
                                 SelectionRoom_Bed_Transport_Fragment.PRE_SELECTION_TRANSPORT_TYPE);
                bundle.putString(SelectionRoom_Bed_Transport_Fragment.PRE_HEADER_TITLE, getString(R.string.text_title_transport_type));
                bundle.putParcelable(SelectionRoom_Bed_Transport_Fragment.PRE_SELECTION_DATA,
                                     mTransportTypeSelected);

                SelectionRoom_Bed_Transport_Fragment selectionRoomBedTransportFragment =
                        new SelectionRoom_Bed_Transport_Fragment();
                selectionRoomBedTransportFragment.setArguments(bundle);
                selectionRoomBedTransportFragment.setSelectionCallBack(this);
                pushFragment(selectionRoomBedTransportFragment,
                             true,
                             true);
                break;
            case R.id.tv_pick_up_date:
                ervPickupTime.hide();
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view,
                                                  int year,
                                                  int monthOfYear,
                                                  int dayOfMonth) {
                                if(CommonUtils.validateFutureDate(dayOfMonth, monthOfYear, year, view)) {
                                    pickCal.set(year, monthOfYear, dayOfMonth);
                                    showDateTime();
                                    if (DateTimeUtil.shareInstance().checkLaterThan24Hours(pickCal)) {
                                        ervPickupTime.setVisibility(View.GONE);
                                        isDateTimeSelectedByUser = true;
                                    } else {
                                        ervPickupTime.setVisibility(View.VISIBLE);
                                        isDateTimeSelectedByUser = false;
                                    }
                                }
                            }

                        },
                        pickCal.get(Calendar.YEAR),
                        pickCal.get(Calendar.MONTH),
                        pickCal.get(Calendar.DAY_OF_MONTH));
                Calendar c = Calendar.getInstance();
                c.add(Calendar.DAY_OF_YEAR, 1);
                datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis() - 1000);
                datePickerDialog.show();
                break;
            case R.id.tv_pick_up_time:
                ervPickupTime.hide();
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(getContext(),
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker timePicker,
                                                  int selectedHour,
                                                  int selectedMinute) {
                                pickCal.set(Calendar.HOUR_OF_DAY, selectedHour);
                                pickCal.set(Calendar.MINUTE, selectedMinute);
                                showDateTime();
                                if(DateTimeUtil.shareInstance().checkLaterThan24Hours(pickCal)) {
                                    ervPickupTime.setVisibility(View.GONE);
                                    isDateTimeSelectedByUser = true;
                                }else{
                                    ervPickupTime.setVisibility(View.VISIBLE);
                                    isDateTimeSelectedByUser = false;
                                }
                            }
                        },
                        pickCal.get(Calendar.HOUR_OF_DAY),
                        pickCal.get(Calendar.MINUTE),
                        false);
                mTimePicker.setTitle(getString(R.string.text_concierge_booking_transfre_pick_up_time).replace("*", ""));
                mTimePicker.show();
                break;
            case R.id.btn_submit:
                AppContext.getSharedInstance().track(AppConstant.ANALYTIC_EVENT_CATEGORY_CONCIERGE_REQUEST, AppConstant.ANALYTIC_EVENT_ACTION_SEND_CONCIERGE_REQUEST, AppConstant.ANALYTIC_BOOK_ITEM.TRANSPORTATION.getValue());
                if (validationData() && !entranceLock.isClickContinuous()) {
                    if(NetworkUtil.getNetworkStatus(getContext())) {
                        isSubmitClicked = true;
                    processBookingTransfer();
                    }else{
                        showInternetProblemDialog();
                    }
                }
                break;
            case R.id.btn_cancel:
                if(!entranceLock.isClickContinuous()) {
                    onBackPress();
                }
                break;
        }
    }

    @Override
    public void onFinishSelection(final Bundle bundle) {

        String typeSelection = bundle.getString(SelectionFragment.PRE_SELECTION_TYPE,
                                                "");
        if (typeSelection.equals(SelectionRoom_Bed_Transport_Fragment.PRE_SELECTION_TRANSPORT_TYPE)) {
            mTransportTypeSelected =
                    bundle.getParcelable(SelectionRoom_Bed_Transport_Fragment.PRE_SELECTION_DATA);
            if (mTransportTypeSelected != null) {
                mTvPleaseSelectTransportType.setText(mTransportTypeSelected.getValue());
                mTvPleaseSelectTransportType.setTextColor(ContextCompat.getColor(getContext(), R.color.text_color_active));
            }else{
                mTvPleaseSelectTransportType.setText(getString(R.string.text_concierge_booking_transfer_select_transport_type));
                mTvPleaseSelectTransportType.setTextColor(ContextCompat.getColor(getContext(), R.color.hint_color));
            }
        }
        if (typeSelection.equals(SelectionFragment.PRE_SELECTION_COUNTRY_CODE)) {
            mContactView.setCountryNo(bundle.getString(SelectionFragment.PRE_SELECTION_DATA, ""));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode,
                                         permissions,
                                         grantResults);

        switch (requestCode) {
            case AppConstant.PERMISSION_REQUEST_READ_EXTERNAL:
                if (grantResults.length > 0
                            && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mPhotoAttached.openGallery();
                }
                break;
            case AppConstant.PERMISSION_REQUEST_OPEN_CAMERA:
                if (grantResults.length > 0
                            && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mPhotoAttached.openCamera();
                }
                break;
            case AppConstant.PERMISSION_REQUEST_WRITE_EXTERNAL:
                if (grantResults.length > 0
                            && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mPhotoAttached.openCamera();
                }
                break;
        }

    }

    @Override
    public void onActivityResult(int requestCode,
                                 int resultCode,
                                 Intent data) {
        super.onActivityResult(requestCode,
                               resultCode,
                               data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == PhotoPickerUtils.PICK_IMAGE_REQUEST_CODE || requestCode == PhotoPickerUtils.TAKE_PICTURE_REQUEST_CODE) {
                mPhotoAttached.onActivityResult(requestCode,
                                                data);
            }
        }
    }

    private boolean validationData() {
        boolean result = true;
        if (mTvPleaseSelectTransportType.getText()
                                        .toString()
                                        .trim()
                                        .equals(getString(R.string.text_concierge_booking_transfer_select_transport_type))) {
            mTransportTypeError.fillData(getString(R.string.text_sign_up_error_required_field));
            mTransportTypeError.setVisibility(View.VISIBLE);
            result = false;
        }
        if (!CommonUtils.isStringValid(mEdtPickUpLocation.getText()
                                                         .toString()
                                                         .trim())) {
            mPickUpLocationError.fillData(getString(R.string.text_sign_up_error_required_field));
            mPickUpLocationError.setVisibility(View.VISIBLE);
            result = false;
        }
        if (!CommonUtils.isStringValid(mEdtDropOffLoction.getText()
                                                         .toString()
                                                         .trim())) {
            mDropOffLoctionError.fillData(getString(R.string.text_sign_up_error_required_field));
            mDropOffLoctionError.setVisibility(View.VISIBLE);
            result = false;
        }
        if(StringUtil.containSpecialCharacter(mEdtAnySpecialRequirements.getText().toString())){
            specialReqError.setVisibility(View.VISIBLE);
            result = false;
        }else{
            specialReqError.setVisibility(View.GONE);
        }
        if(!DateTimeUtil.shareInstance().checkLaterThan24Hours(pickCal)){
            result = false;
            ervPickupTime.setVisibility(View.VISIBLE);
        }else{
            ervPickupTime.setVisibility(View.GONE);
        }
        if (!mContactView.validationContact()) {
            result = false;
        }
        return result;
    }

    private void processBookingTransfer() {

        upsertConciergeRequestRequest = new B2CUpsertConciergeRequestRequest();
        upsertConciergeRequestRequest.setFunctionality(AppConstant.CONCIERGE_FUNCTIONALITY_TYPE.CAR_TRANSFER.getValue());
        upsertConciergeRequestRequest.setRequestType(AppConstant.CONCIERGE_REQUEST_TYPE.T_CAR_TRANSFER.getValue());
        upsertConciergeRequestRequest.updateDataFromContactView(mContactView);

        // Pickup date
        upsertConciergeRequestRequest.setPickupDate(DateTimeUtil.shareInstance().formatTimeStamp(pickCal.getTimeInMillis(), AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS));
        upsertConciergeRequestRequest.setPickUp(DateTimeUtil.shareInstance().formatTimeStamp(pickCal.getTimeInMillis(), AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS));

        // Number of Passenger
        upsertConciergeRequestRequest.setNumberOfAdults(pickerNoOfPassenger.getNumber() + "");
        upsertConciergeRequestRequest.setSituation("Transfer");

        // Image path
        if(!TextUtils.isEmpty(uploadedPhotoPath)){
            upsertConciergeRequestRequest.setAttachmentPath(uploadedPhotoPath);
        }else {
            if (mPhotoAttached.getPhotos() != null && mPhotoAttached.getPhotos().size() > 0) {
                String pathImage = mPhotoAttached.getPhotos()
                        .get(0);
                upsertConciergeRequestRequest.setAttachmentPath(pathImage);
            }
        }
        if(myRequestObject != null){
            upsertConciergeRequestRequest.setTransactionID(myRequestObject.getBookingItemID());
        }
        // Request details
        upsertConciergeRequestRequest.setRequestDetails(combineRequestDetails());

        showDialogProgress();
        B2CWSUpsertConciergeRequest b2CWSUpsertConciergeRequest = new B2CWSUpsertConciergeRequest(myRequestObject == null ? AppConstant.CONCIERGE_EDIT_TYPE.ADD : AppConstant.CONCIERGE_EDIT_TYPE.AMEND, this);
        b2CWSUpsertConciergeRequest.setRequest(upsertConciergeRequestRequest);
        b2CWSUpsertConciergeRequest.run(null);
    }
    private String combineRequestDetails(){
        String requestDetails = "";
        if(!TextUtils.isEmpty(mEdtAnySpecialRequirements.getText().toString().trim())){
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_SPECIAL_REQUIREMENTS + StringUtil.removeAllSpecialCharacterAndBreakLine(mEdtAnySpecialRequirements.getText().toString().trim());
        }
        if(!TextUtils.isEmpty(pickerNoOfPassenger.getNumber() + "")){
            if(!TextUtils.isEmpty(requestDetails)){
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_NUMBER_OF_PASSENGERS+ pickerNoOfPassenger.getNumber();
        }
        if(!TextUtils.isEmpty(mContactView.getReservationName())){
            if(!TextUtils.isEmpty(requestDetails)){
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_RESERVATION_NAME+ StringUtil.removeAllSpecialCharacterAndBreakLine(mContactView.getReservationName());
        }

        // Pickup location
        if(!StringUtil.isEmpty(requestDetails)){
            requestDetails += " | ";
        }
        requestDetails += AppConstant.B2C_REQUEST_DETAIL_PICK_LOCATION + StringUtil.removeAllSpecialCharacterAndBreakLine(mEdtPickUpLocation.getText().toString().trim());
        // Dropoff Time - 10 AM "

        // Drop-off location
        if(!StringUtil.isEmpty(requestDetails)){
            requestDetails += " | ";
        }
        requestDetails += AppConstant.B2C_REQUEST_DETAIL_DROP_LOCATION + StringUtil.removeAllSpecialCharacterAndBreakLine(mEdtDropOffLoction.getText().toString().trim());
        requestDetails += " | " + AppConstant.B2C_REQUEST_DETAIL_PREF_TRANSPORT_TYPE + mTvPleaseSelectTransportType.getText().toString();

        return requestDetails;
      /*  String requestDetails = "";
        if(!TextUtils.isEmpty(mEdtAnySpecialRequirements.getText().toString().trim())){
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_SPECIAL_REQ + mEdtAnySpecialRequirements.getText().toString().trim();
        }
        // Pickup location
        if(!StringUtil.isEmpty(requestDetails)){
            requestDetails += " | ";
        }
        requestDetails += AppConstant.B2C_REQUEST_DETAIL_PICK_LOCATION + mEdtPickUpLocation.getText().toString().trim();

        // Drop-off location
        requestDetails += " | " + AppConstant.B2C_REQUEST_DETAIL_DROP_LOCATION + mEdtDropOffLoction.getText().toString().trim();

        // Transport type
        requestDetails += " | " + AppConstant.B2C_REQUEST_DETAIL_PREF_TRANSPORT_TYPE + mTvPleaseSelectTransportType.getText().toString();
        return requestDetails;*/
    }
    @Override
    public void onB2CResponse(B2CBaseResponse response) {
        hideDialogProgress();
        if(getActivity()==null) {
            return;
        }
        if(response instanceof B2CUpsertConciergeRequestResponse){
            if(response != null && response.isSuccess()){
                onBackPress();
                ThankYouFragment thankYouFragment = new ThankYouFragment();
                if (myRequestObject != null) {
//                    Bundle bundle = new Bundle();
//                    bundle.putString(ThankYouFragment.LINE_MAIN,
//                            getString(R.string.text_message_thank_you_amend));
//                    thankYouFragment.setArguments(bundle);
                    EventBus.getDefault().post(AppConstant.CONCIERGE_EDIT_TYPE.AMEND);
                } else {
                    // Track product
                    AppContext.getSharedInstance().track(AppConstant.TRACK_EVENT_PRODUCT_CATEGORY.CAR_TRANSFER.getValue(), AppConstant.TRACK_EVENT_PRODUCT_NAME.GENERIC_BOOK.getValue());
                    EventBus.getDefault().post(AppConstant.CONCIERGE_EDIT_TYPE.ADD);
                }
                pushFragment(thankYouFragment,
                        true,
                        true);
            }else{
                //showDialogMessage("Error", response.getMessage());
                showDialogMessage(getString(R.string.text_title_dialog_error), getString(R.string.text_concierge_request_error));
                if(upsertConciergeRequestRequest != null){
                    uploadedPhotoPath = upsertConciergeRequestRequest.getAttachmentPath();
                }
            }
        }else if(response instanceof B2CGetRecentRequestResponse){
            setDetailData(new CarBookingDetailData((B2CGetRecentRequestResponse)response));
        }
    }

    @Override
    public void onB2CResponseOnList(List<B2CBaseResponse> responseList) {
        if(getActivity()==null) {
            return;
        }
    }

    @Override
    public void onB2CFailure(String errorMessage, String errorCode) {
        hideDialogProgress();
        if(getActivity()==null) {
            return;
        }
        //showDialogMessage("Error", errorMessage);
        if(isSubmitClicked) {
            isSubmitClicked = false;
            showDialogMessage(getString(R.string.text_title_dialog_error), getString(R.string.text_concierge_request_error));
        }
        // Store the uploaded successful photo path
        // So that the upsert failed in the first time, user tried to process for the second time
        // then that is no need to upload again
        if(upsertConciergeRequestRequest != null){
            uploadedPhotoPath = upsertConciergeRequestRequest.getAttachmentPath();
        }
    }
    @Override
    public void onResponse(final Call call,
                           final Response response) {
        if (response.body() instanceof BookTransferResponse) {
            hideDialogProgress();
            if(getActivity()==null) {
                return;
            }
            BookTransferResponse bookTransferResponse = ((BookTransferResponse) response.body());
            int code = bookTransferResponse.getStatus();
            if (code == 200) {
                onBackPress();
                ThankYouFragment thankYouFragment = new ThankYouFragment();
                if (myRequestObject != null) {
                    Bundle bundle = new Bundle();
                    bundle.putString(ThankYouFragment.LINE_MAIN,
                                     getString(R.string.text_message_thank_you_amend));
                    thankYouFragment.setArguments(bundle);
                }
                pushFragment(thankYouFragment,
                             true,
                             true);

            } else {
//                showToast(bookTransferResponse.getMessage());
            }
        }
        if (response.body() instanceof CarTransferBookingDetailResponse) {
            hideDialogProgress();
            if(getActivity()==null) {
                return;
            }
            CarTransferBookingDetailResponse carTransferBookingDetailResponse =
                    ((CarTransferBookingDetailResponse) response.body());
            int code = carTransferBookingDetailResponse.getStatus();
            if (code == 200) {
                setDetailData(carTransferBookingDetailResponse.getData());
            } else {
//                showToast(carTransferBookingDetailResponse.getMessage());
            }
        }


    }

    @Override
    public void onFailure(final Call call,
                          final Throwable t) {
        hideDialogProgress();
        if(getActivity()==null) {
            return;
        }
//        showToast(getString(R.string.text_server_error_message));
    }

    @Override
    public void onSelectCountryClick() {
        Bundle bundle1 = new Bundle();
        bundle1.putString(SelectionFragment.PRE_SELECTION_TYPE,
                          SelectionFragment.PRE_SELECTION_COUNTRY_CODE);
        bundle1.putString(SelectionFragment.PRE_SELECTION_DATA,
                          mContactView.getSelectedCountryCode());
        SelectionFragment fragment1 = new SelectionFragment();
        fragment1.setSelectionCallBack(this);
        fragment1.setArguments(bundle1);
        pushFragment(fragment1,
                     true,
                     true);
    }

    private void setDetailData(CarBookingDetailData data) {

        if(data== null)
            return;
        mEdtPickUpLocation.setText(data.getPickupLocation());
        mEdtDropOffLoction.setText(data.getDropOffLocation());
        if(data.getConciergeRequestDetail()!=null && data.getConciergeRequestDetail().getNoofPassengers()!=null) {
            pickerNoOfPassenger.setNumber(Integer.parseInt(data.getConciergeRequestDetail()
                                                               .getNoofPassengers()));
        }
        mTransportTypeSelected = new CommonObject(data.getTransportType());
        mTvPleaseSelectTransportType.setText(data.getTransportType());
        mTvPleaseSelectTransportType.setTextColor(ContextCompat.getColor(getContext(), R.color.text_color_active));
        mEdtAnySpecialRequirements.setText(data.getConciergeRequestDetail().getSpecialRequirement());
        pickCal = Calendar.getInstance();
        pickCal.setTimeInMillis(data.getPickupDateEpoc());
        showDateTime();

        // Contact view
        mContactView.updateContactView(data);
    }
}
