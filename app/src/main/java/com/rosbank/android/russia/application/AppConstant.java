package com.rosbank.android.russia.application;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by nganguyent on 23/08/2016.
 */
public class AppConstant {
    /**
     * 2
     * Define constant key
     */

    /*public static final String WS_API_URL = "http://113.161.77.24:10302";///api/identity
    public static final String AUTHENTICATE_ACCOUNT_URL = "/auth";

    public static final int WS_USER_SIGN_IN_TASK = 1001;*/

    public static final int PERMISSION_REQUEST_READ_EXTERNAL = 5000;
    public static final int PERMISSION_REQUEST_OPEN_CAMERA = 5001;
    public static final int PERMISSION_REQUEST_WRITE_EXTERNAL = 5002;
    public static final int PERMISSION_REQUEST_LOCATION = 5003;
    public static final int PERMISSION_REQUEST_CALENDAR = 6000;
    public static final int PERMISSION_REQUEST_CALL_REQUEST = 5004;
    public static final long DAY_IN_MILLISECONDS = 86400000;
    public static final int MINUTE_EXTRA = 10;
    public static final int MINUTE_EXTRA_DINING_BOOKING = 30;
    public static final String CLIENT_CODE_REGULAR = "230385";
    public static final String CLIENT_CODE_PRIVATE = "090284";
    public static final String LANGUAGE_SETTINGS = "Language_settings";
    public static final String RUSSIAN_LANGUAGE = "ru";
    public static final String APP_PACKAGE_NAME = "com.rosbank.android.russia";
/*    public static final String LP_APP_INSTALLATION_ID = "3f618eb5-131d-448b-8ada-56911d5173a9";
    public static final String BRAND_ID = "2022139";*/
    public static final String LP_APP_INSTALLATION_ID = "00b370b4-bd8a-48ac-ae2a-23a8dd908f37";
    public static final String BRAND_ID = "11953504";
    public static final String URL_TERMS_OF_USE = "https://infinitebutler.aspirelifestyles.com/TermsofUse";
    public static final String URL_PRIVACY_POLICY = "https://infinitebutler.aspirelifestyles.com/PrivacyPolicy";


    public enum BOOKING_TYPE {
        Restaurant("Restaurant"),
        RestaurantRecommendation("RestaurantRecommendation"),
        Golf("Golf"),
        CarRental("CarRental"),
        CarTransfer("CarTransfer"),
        Hotel("Hotel"),
        HotelRecommendation("HotelRecommendation"),
        Entertainment("Entertainment"),
        Other("Other");
        private static final Map<String, BOOKING_TYPE> enumsByValue = new HashMap<String, BOOKING_TYPE>();

        static {
            for (BOOKING_TYPE type : BOOKING_TYPE.values()) {
                enumsByValue.put(type.value, type);
            }
        }

        public static BOOKING_TYPE getEnum(String value) {
            return enumsByValue.get(value);
        }

        private final String value;

        private BOOKING_TYPE(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public enum BOOKING_FILTER_TYPE {
        All("All"), // For filter
        Restaurant("Restaurant"),
        Golf("Golf"),
        Car("Car"),
        Hotel("Hotel"),
        Entertainment("Entertainment"),
        Spa("Spa"),
        Other("Other");
        private static final Map<String, BOOKING_FILTER_TYPE> enumsByValue = new HashMap<String, BOOKING_FILTER_TYPE>();

        static {
            for (BOOKING_FILTER_TYPE type : BOOKING_FILTER_TYPE.values()) {
                enumsByValue.put(type.value, type);
            }
        }

        public static BOOKING_FILTER_TYPE getEnum(String value) {
            return enumsByValue.get(value);
        }

        private final String value;

        private BOOKING_FILTER_TYPE(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public enum CONCIERGE_EDIT_TYPE {
        ADD("ADD"),
        AMEND("AMEND"),
        CANCEL("CANCEL");
        private static final Map<String, CONCIERGE_EDIT_TYPE> enumsByValue = new HashMap<String, CONCIERGE_EDIT_TYPE>();

        static {
            for (CONCIERGE_EDIT_TYPE type : CONCIERGE_EDIT_TYPE.values()) {
                enumsByValue.put(type.value, type);
            }
        }

        public static CONCIERGE_EDIT_TYPE getEnum(String value) {
            return enumsByValue.get(value);
        }

        private final String value;

        private CONCIERGE_EDIT_TYPE(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public enum PREFERENCE_EDIT_TYPE {
        ADD("ADD"),
        UPDATE("UPDATE");
        private static final Map<String, PREFERENCE_EDIT_TYPE> enumsByValue = new HashMap<String, PREFERENCE_EDIT_TYPE>();

        static {
            for (PREFERENCE_EDIT_TYPE type : PREFERENCE_EDIT_TYPE.values()) {
                enumsByValue.put(type.value, type);
            }
        }

        public static PREFERENCE_EDIT_TYPE getEnum(String value) {
            return enumsByValue.get(value);
        }

        private final String value;

        private PREFERENCE_EDIT_TYPE(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public enum PREFERENCE_TYPE {
        HOTEL("Hotel"),
        DINING("Dining"),
        CAR("Car Rental"),
        GOLF("Golf"),
        OTHER("Other");
        private static final Map<String, PREFERENCE_TYPE> enumsByValue = new HashMap<String, PREFERENCE_TYPE>();

        static {
            for (PREFERENCE_TYPE type : PREFERENCE_TYPE.values()) {
                enumsByValue.put(type.value, type);
            }
        }

        public static PREFERENCE_TYPE getEnum(String value) {
            Set<String> keySet = enumsByValue.keySet();
            for (String key : keySet) {
                if (key.equalsIgnoreCase(value)) {
                    return enumsByValue.get(key);
                }
            }
            return null;
        }

        private final String value;

        private PREFERENCE_TYPE(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public enum CONCIERGE_REQUEST_TYPE {
        RESERVE_TABLE("D RESTAURANT"),
        RECOMMEND_RESTAURANT("D RESTAURANT"),
        BOOK_HOTEL("T HOTEL AND B&B"),
        RECOMMEND_HOTEL("T HOTEL AND B&B"),
        OTHER_REQUEST("Other Request"),
        T_CAR_RENTAL("T CAR RENTAL"),
        T_CAR_TRANSFER("T LIMO AND SEDAN"),
        GOLF("S GOLF"),
        SPA("Spa"),
        RECOMMEND_EVENT("E CONCERT/THEATER"),
        EVENT("E CONCERT/THEATER");

        private static final Map<String, CONCIERGE_REQUEST_TYPE> enumsByValue = new HashMap<String, CONCIERGE_REQUEST_TYPE>();

        static {
            for (CONCIERGE_REQUEST_TYPE type : CONCIERGE_REQUEST_TYPE.values()) {
                enumsByValue.put(type.value, type);
            }
        }

        public static CONCIERGE_REQUEST_TYPE getEnum(String value) {
            Set<String> keySet = enumsByValue.keySet();
            for (String key : keySet) {
                if (key.equalsIgnoreCase(value)) {
                    return enumsByValue.get(key);
                }
            }
            return OTHER_REQUEST;
            //return null;
        }

        private final String value;

        private CONCIERGE_REQUEST_TYPE(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    // Request type of Luxury Card
    /*public enum CONCIERGE_REQUEST_TYPE{
        SEND_FLOWER("Send Flowers"),
        RESERVE_TABLE("D Restaurant"),
        RECOMMEND_RESTAURANT("D Restaurant"),
        BOOK_HOTEL("T HOTEL AND B&B"),
        RECOMMEND_HOTEL("T HOTEL AND B&B"),
        OTHER_REQUEST("O Client Specific"),
        T_CAR_RENTAL("T Limo and Sedan"),
        T_CAR_TRANSFER("T Limo and Sedan"),
        GOLF("S Golf"),
        RECOMMEND_EVENT("E CONCERT/THEATER"),
        EVENT("E CONCERT/THEATER");

        private static final Map<String, CONCIERGE_REQUEST_TYPE> enumsByValue = new HashMap<String, CONCIERGE_REQUEST_TYPE>();
        static {
            for (CONCIERGE_REQUEST_TYPE type : CONCIERGE_REQUEST_TYPE.values()) {
                enumsByValue.put(type.value, type);
            }
        }

        public static CONCIERGE_REQUEST_TYPE getEnum(String value) {
            Set<String> keySet = enumsByValue.keySet();
            for(String key : keySet) {
                if(key.equalsIgnoreCase(value)) {
                    return enumsByValue.get(key);
                }
            }
            return null;
        }
        private final String value;
        private CONCIERGE_REQUEST_TYPE(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }*/
    public enum CONCIERGE_FUNCTIONALITY_TYPE {
        HOTEL("Book a Hotel"),
        HOTEL_RECOMMEND("Recommend a Hotel"),
        DINING("Book a Dining"),
        DINING_RECOMMEND("Recommend a Dining"),
        CAR_RENTAL("CarRental"),
        CAR_TRANSFER("Transfer"),
        GOLF("Book a Golf"),
        ENTERTAINMENT("Book an Event"),
        ENTERTAINMENT_RECOMMEND("Recommend an Event"),
        OTHER("Others");
        private static final Map<String, CONCIERGE_FUNCTIONALITY_TYPE> enumsByValue = new HashMap<String, CONCIERGE_FUNCTIONALITY_TYPE>();

        static {
            for (CONCIERGE_FUNCTIONALITY_TYPE type : CONCIERGE_FUNCTIONALITY_TYPE.values()) {
                enumsByValue.put(type.value, type);
            }
        }

        public static CONCIERGE_FUNCTIONALITY_TYPE getEnum(String value) {
            return enumsByValue.get(value);
        }

        private final String value;

        private CONCIERGE_FUNCTIONALITY_TYPE(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public enum TRACK_EVENT_PRODUCT_NAME {
        GENERIC_BOOK("Generic - Book"),
        GENERIC_RECOMMEND("Generic - Recommend");
        private static final Map<String, TRACK_EVENT_PRODUCT_NAME> enumsByValue = new HashMap<String, TRACK_EVENT_PRODUCT_NAME>();

        static {
            for (TRACK_EVENT_PRODUCT_NAME type : TRACK_EVENT_PRODUCT_NAME.values()) {
                enumsByValue.put(type.value, type);
            }
        }

        public static TRACK_EVENT_PRODUCT_NAME getEnum(String value) {
            return enumsByValue.get(value);
        }

        private final String value;

        private TRACK_EVENT_PRODUCT_NAME(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public enum TRACK_EVENT_PRODUCT_CATEGORY {
        DINING("Dining"),
        RECOMMEND_DINING("Recommend Dining"),
        HOTEL("Hotel"),
        RECOMMEND_HOTEL("Recommend Hotel"),
        GOLF("Golf"),
        CAR_RENTAL("Car Rental"),
        CAR_TRANSFER("Car Transfer"),
        EVENT("Event"),
        RECOMMEND_EVENT("Recommend Event"),
        OTHER("Other");
        private static final Map<String, TRACK_EVENT_PRODUCT_CATEGORY> enumsByValue = new HashMap<String, TRACK_EVENT_PRODUCT_CATEGORY>();

        static {
            for (TRACK_EVENT_PRODUCT_CATEGORY type : TRACK_EVENT_PRODUCT_CATEGORY.values()) {
                enumsByValue.put(type.value, type);
            }
        }

        public static TRACK_EVENT_PRODUCT_CATEGORY getEnum(String value) {
            return enumsByValue.get(value);
        }

        private final String value;

        private TRACK_EVENT_PRODUCT_CATEGORY(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public static final String TRACK_EVENT_PRODUCT_BRAND = "Aspire";
    /*public enum CONCIERGE_FUNCTIONALITY_TYPE{
        HOTEL("Hotel"),
        HOTEL_RECOMMEND("Hotel"),
        DINING("Dining"),
        DINING_RECOMMEND("Dining"),
        CAR_RENTAL("CarRental"),
        CAR_TRANSFER("CarRental"),
        GOLF("Golf"),
        ENTERTAINMENT("Entertainment"),
        ENTERTAINMENT_RECOMMEND("Entertainment"),
        OTHER("Others");
        private static final Map<String, CONCIERGE_FUNCTIONALITY_TYPE> enumsByValue = new HashMap<String, CONCIERGE_FUNCTIONALITY_TYPE>();
        static {
            for (CONCIERGE_FUNCTIONALITY_TYPE type : CONCIERGE_FUNCTIONALITY_TYPE.values()) {
                enumsByValue.put(type.value, type);
            }
        }

        public static CONCIERGE_FUNCTIONALITY_TYPE getEnum(String value) {
            return enumsByValue.get(value);
        }
        private final String value;
        private CONCIERGE_FUNCTIONALITY_TYPE(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }*/
    /**********************************************************************************************/
    // Date format
    public static final String DATE_FORMAT_YYYY_MM_DD_T_HH_MM_SS = "yyyy-MM-dd'T'HH:mm:ss.SSS";
    public static final String DATE_FORMAT_YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_FORMAT_CONCIERGE_BOOKING = "dd MMM yyyy";
    public static final String TIME_FORMAT_CONCIERGE_BOOKING = "hh:mm a";
    /**********************************************************************************************/
    /**
     * DEFINE PREFERENCE KEYS
     */
    public static final String PRE_AUTHOR_TOKEN = "AUTHOR_TOKEN";
    public static final String PRE_AUTHOR_CREATE_AT = "AUTHOR_TOKEN_CREATE_AT";
    public static final String PRE_AUTHOR_EXPIRED_AT = "AUTHOR_TOKEN_EXPIRED_AT";
    public static final String USER_ID_PRE = "USER_ID";
    public static final String USER_ONLINE_MEMBER_DETAIL_ID_PRE = "USER_ONLINE_MEMBER_DETAIL_ID";
    public static final String USER_EMAIL_PRE = "USER_EMAIL";
    public static final String USER_FIRST_TIME_PRE = "USER_FIRST_TIME";
    public static final String USER_FIRST_NAME = "USER_FIRST_NAME";
    public static final String USER_LAST_NAME = "USER_LAST_NAME";
    public static final String USER_FULL_NAME = "USER_FULL_NAME";
    public static final String USER_MOBILE_NUMBER = "USER_MOBILE_NUMBER";
    public static final String USER_CITY = "USER_CITY";
    public static final String USER_COUNTRY = "USER_COUNTRY";
    public static final String USER_ZIPCODE = "USER_ZIPCODE";
    public static final String USER_AVATAR = "USER_AVATAR";
    public static final String USER_SALUTATION = "USER_SALUTATION";
    public static final String PRE_REQUEST_OBJECT_DATA = "object_data";
    public static final String PRE_RESTAURANT_DATA = "restaurant_data";
    public static final String PRE_HOTEL_DATA = "hotel_data";
    public static final String PRE_SPA_DATA = "spa_data";
    public static final String PRE_MY_CURRENCY_KEY = "my_currency_key";
    public static final String PRE_MY_CURRENCY_TEXT = "my_currency_text";

    // Add to find out user vip status
    public static final String USER_VIP_STATUS = "USER_VIP_STATUS";

    // ==========================================================================================
    // ======================== FOR B2C PREFERENCE =================================
    // ==========================================================================================
    public static final String PRE_B2C_TOOLTIP_SHOW = "B2C_TOOLTIP_SHOW";
    public static final String PRE_B2C_ACCESS_TOKEN = "B2C_ACCESS_TOKEN";
    public static final String PRE_B2C_REQUEST_TOKEN = "B2C_REQUEST_TOKEN";
    public static final String PRE_B2C_REFRESH_TOKEN = "B2C_REFRESH_TOKEN";
    public static final String PRE_B2C_EXPIRED_AT = "B2C_EXPIRED_AT";
    public static final String PRE_B2C_EXPIRATION_TIME_DURATION = "B2C_EXPIRATION_TIME_DURATION";
    public static final String PRE_B2C_ONLINE_MEMBER_ID = "B2C_ONLINE_MEMBER_ID";

    // ==========================================================================================
    // ======================== FOR B2C REQUEST DETAIL KEY =================================
    // ==========================================================================================
    /* public static final String B2C_REQUEST_DETAIL_RESTAURANT_NAME = "RestaurantName:";*/
    public static final String B2C_REQUEST_DETAIL_RESTAURANT_NAME = "Name of Restaurant - ";
    /*public static final String B2C_REQUEST_DETAIL_SPECIAL_REQ = "SpecialReq:";*/
    public static final String B2C_REQUEST_DETAIL_SPECIAL_REQ = "SpecialReq - ";
    public static final String B2C_REQUEST_DETAIL_SPECIAL_REQUIREMENTS = "Special Requirement - ";
    public static final String B2C_REQUEST_DETAIL_RESERVATION_NAME = "ReservationName - ";
    public static final String B2C_REQUEST_DETAIL_NUMBER_OF_PASSENGERS = "No of Passengers - ";
    public static final String B2C_REQUEST_DETAIL_COUNTRY = "Country - ";
    public static final String B2C_REQUEST_DETAIL_PREFERED = "If preferred restaurant is not available on selected date & time - ";
    public static final String B2C_REQUEST_DETAIL_CUISINE = "Cuisine:";
    public static final String B2C_REQUEST_DETAIL_TYPE_OF_CUISINE = "Type of Cuisine - ";
    public static final String B2C_REQUEST_DETAIL_AMBIANCE_OCCASION = "Ambiance/Occasion - ";
    public static final String B2C_REQUEST_DETAIL_BUDGET_RANGE_CURRENCY = "Budget Range Currency per person - ";
    /*public static final String B2C_REQUEST_DETAIL_MAX_PRICE = "MaxPrice:";*/
    public static final String B2C_REQUEST_DETAIL_MAX_PRICE = "Budget Range per night Currency - ";
    public static final String B2C_REQUEST_DETAIL_BUDGET_RANGE_PER_DAY = "Budget Range Currency per day - ";
    /*public static final String B2C_REQUEST_DETAIL_MIN_PRICE = "MinPrice:";*/
    public static final String B2C_REQUEST_DETAIL_MIN_PRICE = "MinPrice - ";
    /*public static final String B2C_REQUEST_DETAIL_HOTEL_NAME = "HotelName:";*/
    public static final String B2C_REQUEST_DETAIL_HOTEL_NAME = "Hotel Name - ";
    public static final String B2C_REQUEST_DETAIL_GUEST_NAME = "Guest Name - ";
    public static final String B2C_REQUEST_DETAIL_SITUATION = "Situation:";
    /*public static final String B2C_REQUEST_DETAIL_ROOM_PREF = "RoomPref:";*/
    public static final String B2C_REQUEST_DETAIL_ROOM_PREF = "Room Preference - ";
    /*public static final String B2C_REQUEST_DETAIL_SMOKING_PREF = "SmokingPref:"; */
    public static final String B2C_REQUEST_DETAIL_SMOKING_PREF = "Smoking Preference - ";
    /*public static final String B2C_REQUEST_DETAIL_MEMBERSHIP = "Membership:";*/
    public static final String B2C_REQUEST_DETAIL_MEMBERSHIP = "Membership - ";
    public static final String B2C_REQUEST_DETAIL_MEMBERNO = "Hotel Loyalty Membership Number - ";
    /*public static final String B2C_REQUEST_DETAIL_MEMBERNO = "MemberNo:";*/
    public static final String B2C_REQUEST_DETAIL_PREF_RATING = "PrefRating:";
    public static final String B2C_REQUEST_DETAIL_NO_ROOMS = "NoRooms:";
    //    public static final String B2C_REQUEST_DETAIL_DRIVER_NAME = "DriverName:";
    public static final String B2C_REQUEST_DETAIL_DRIVER_NAME = "Name of driver - ";
    public static final String B2C_REQUEST_DETAIL_DRIVER_AGE = "Age of driver - ";
    //    public static final String B2C_REQUEST_DETAIL_DRIVER_AGE = "DriverAge:";
    /*public static final String B2C_REQUEST_DETAIL_INT_LICENSE = "IntLicense:";*/
    public static final String B2C_REQUEST_DETAIL_INT_LICENSE = "Does the drive hold a valid international driving licence - ";
    //public static final String B2C_REQUEST_DETAIL_PICK_LOCATION = "PickLocation:";
    public static final String B2C_REQUEST_DETAIL_PICK_LOCATION = "Pickup Location - ";
    public static final String B2C_REQUEST_DETAIL_DROP_OF_DATE = "Dropoff Date - ";
    // public static final String B2C_REQUEST_DETAIL_DROP_OF_DATE = "DropoffDate:";
    //public static final String B2C_REQUEST_DETAIL_DROP_LOCATION = "DropLocation:";
    public static final String B2C_REQUEST_DETAIL_DROP_LOCATION = "Dropoff Location - ";
    //    public static final String B2C_REQUEST_DETAIL_PREF_VEHICLE = "PrefVehicle:";
    public static final String B2C_REQUEST_DETAIL_PREF_VEHICLE = "Preferred Vehicle - ";
    //    public static final String B2C_REQUEST_DETAIL_PREF_TRANSPORT_TYPE = "TransportType:";
    public static final String B2C_REQUEST_DETAIL_PREF_TRANSPORT_TYPE = "Transport Type - ";
    //    public static final String B2C_REQUEST_DETAIL_PREF_CAR_RENTAL_COMPANY = "PrefCarRentalComp:";
    public static final String B2C_REQUEST_DETAIL_PREF_CAR_RENTAL_COMPANY = "Preferred car rental company - ";
    public static final String B2C_REQUEST_DETAIL_PAY_TYPE = "PayType:";
    public static final String B2C_REQUEST_DETAIL_DROP_POINT = "DropPoint:";
    public static final String B2C_REQUEST_DETAIL_EXTRA_STOPS = "ExtraStops:";
    public static final String B2C_REQUEST_DETAIL_LIMO_SERVICE = "LimoService:";
    public static final String B2C_REQUEST_DETAIL_EVENT_NAME = "EventName - ";
    /*public static final String B2C_REQUEST_DETAIL_STATE = "State:";*/
    public static final String B2C_REQUEST_DETAIL_STATE = "State - ";
    /* public static final String B2C_REQUEST_DETAIL_EVENT_CATEGORY = "EventCategory:";*/
    public static final String B2C_REQUEST_DETAIL_EVENT_CATEGORY = "EventCategory - ";
    public static final String B2C_REQUEST_DETAIL_PREF_DATE = "PrefDate:";
    public static final String B2C_REQUEST_DETAIL_PREF_SEATING = "PrefSeating:";
    /*public static final String B2C_REQUEST_DETAIL_NO_TICKET = "NoTicket:";*/
    public static final String B2C_REQUEST_DETAIL_NO_TICKET = "No of tickets - ";
    public static final String B2C_REQUEST_DETAIL_GOLF_COURSE_NAME = "GolfCourseName:";
    public static final String B2C_REQUEST_DETAIL_GOLF_HANDICAP = "GolfHandicap:";
    public static final String B2C_REQUEST_DETAIL_PREF_REGION = "PrefRegion:";
    public static final String B2C_REQUEST_DETAIL_SPECIFIC_START_TIME = "SpecificStartTime:";
    public static final String B2C_REQUEST_DETAIL_NO_OF_BALLERS = "NoofBallers:";
    public static final String B2C_REQUEST_DETAIL_HOLES = "Holes:";
    public static final String B2C_REQUEST_DETAIL_NEED_CADDY = "NeedCaddy:";
    public static final String B2C_REQUEST_DETAIL_NEED_BUGGY = "Needbuggy:";
    public static final String B2C_REQUEST_DETAIL_MEMBER_OF_GOLF_CLUB = "MemberofGolfClub:";
    public static final String B2C_REQUEST_DETAIL_CITY = "City - ";
    public static final String B2C_REQUEST_DETAIL_GOLF_COURSE = "Golf Course - ";
    public static final String B2C_REQUEST_DETAIL_DATE_OF_PLAY = "Date of play - ";
    public static final String B2C_REQUEST_DETAIL_TEE_TIME = "Tee time - ";
    public static final String B2C_REQUEST_DETAIL_HOLE_PREFERENCE = "Hole preference - ";
    public static final String B2C_REQUEST_DETAIL_NO_BALLERS = "No ballers - ";
    public static final String B2C_REQUEST_DETAIL_ADDITIONAL_PREFERENCE = "Addition preferences - ";
    public static final String B2C_REQUEST_DETAIL_PREFERRED_GOLF_HANDICAP = "Golf handicap - ";
    public static final String B2C_REQUEST_DETAIL_FOOD_ALLERGIES = "Food allergies - ";

    public static final String B2C_REQUEST_DETAIL_WITH_BREAKFAST = "With Breakfast - ";
    public static final String B2C_REQUEST_DETAIL_WITH_WIFI = "With Wifi - ";
    public static final String B2C_REQUEST_DETAIL_SPA_NAME = "Spa Name - ";
    public static final String B2C_REQUEST_DETAIL_FULL_ADDRESS = "Full Address - ";
    public static final String B2C_REQUEST_DETAIL_PRIVILEGE_ID = "Privilege Id - ";
    public static final String B2C_REQUEST_DETAIL_NUMBER_OF_ROOM = "Numbers Of Room - ";
    public static final String B2C_REQUEST_DETAIL_NOT_AVAILABLE = "Not Available";

    // ==========================================================================================
    // ======================== FOR GOOGLE ANALYTIC =================================
    // ==========================================================================================
    public static final String ANALYTIC_EVENT_CATEGORY_CONCIERGE = "Concierge";
    public static final String ANALYTIC_EVENT_CATEGORY_HOME = "Home";
    public static final String ANALYTIC_EVENT_CATEGORY_CONCIERGE_REQUEST = "Concierge Request";
    public static final String ANALYTIC_EVENT_ACTION_OPEN_MENU = "Open concierge menu";
    public static final String ANALYTIC_EVENT_ACTION_CALL_CONCIERGE = "Call concierge";
    public static final String ANALYTIC_EVENT_ACTION_SEND_REQUEST = "Send request";
    public static final String ANALYTIC_EVENT_ACTION_SEND_CONCIERGE_REQUEST = "Send concierge request";

    public enum ANALYTIC_MENU_ITEM {
        HOME("Home"),
        ACCOUNT("Account"),
        MY_REQUESTS("My Requests"),
        SEND_CONCIERGE_REQUEST("Send Concierge Requests"),
        ABOUT("About"),
        SIGNIN("Sign In"),
        SIGNOUT("Sign Out");
        private static final Map<String, ANALYTIC_MENU_ITEM> enumsByValue = new HashMap<String, ANALYTIC_MENU_ITEM>();

        static {
            for (ANALYTIC_MENU_ITEM type : ANALYTIC_MENU_ITEM.values()) {
                enumsByValue.put(type.value, type);
            }
        }

        public static ANALYTIC_MENU_ITEM getEnum(String value) {
            return enumsByValue.get(value);
        }

        private final String value;

        private ANALYTIC_MENU_ITEM(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public enum ANALYTIC_BOOK_ITEM {
        HOTEL("Travel"),
        DINING("Dining"),
        ENTERTAINMENT("Entertainment"),
        TRANSPORTATION("Transportation"),
        GOLF("Golf"),
        OTHERS("Others");
        private static final Map<String, ANALYTIC_BOOK_ITEM> enumsByValue = new HashMap<String, ANALYTIC_BOOK_ITEM>();

        static {
            for (ANALYTIC_BOOK_ITEM type : ANALYTIC_BOOK_ITEM.values()) {
                enumsByValue.put(type.value, type);
            }
        }

        public static ANALYTIC_BOOK_ITEM getEnum(String value) {
            return enumsByValue.get(value);
        }

        private final String value;

        private ANALYTIC_BOOK_ITEM(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
}
