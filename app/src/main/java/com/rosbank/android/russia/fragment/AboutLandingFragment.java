package com.rosbank.android.russia.fragment;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.activitys.HomeActivity;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.model.UserItem;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.FontUtils;

import butterknife.BindView;
import butterknife.OnClick;

/*
 * Created by chau.nguyen on 10/05/2016.
 */
public class AboutLandingFragment extends BaseFragment {

    @BindView(R.id.rl_about_content)
    RelativeLayout rlAboutContent;
    @BindView(R.id.img_about)
    ImageView imgAbout;
    @BindView(R.id.tv_about_luxury_card)
    TextView tvAspire;
    @BindView(R.id.tv_about_terms)
    TextView tvTerms;
    @BindView(R.id.tv_about_privacy)
    TextView tvPrivacy;
    @BindView(R.id.tv_about_faqs)
    TextView tvFaqs;

    public AboutLandingFragment() {

    }

    @Override
    protected int layoutId() {
        return R.layout.fragment_about_landing;
    }

    @Override
    protected void initView() {
        if (getActivity() != null) {
            if (UserItem.isVip()) {
                tvAspire.setText(getActivity().getResources().getString(R.string.about_hermitage));
            } else {
                tvAspire.setText(getActivity().getResources().getString(R.string.about_rosbank));
            }
        }
        //CommonUtils.setFontForViewRecursive(rlAboutContent,FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD);
        CommonUtils.setFontForTextView(tvAspire, FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        CommonUtils.setFontForTextView(tvTerms, FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        CommonUtils.setFontForTextView(tvPrivacy, FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        CommonUtils.setFontForTextView(tvFaqs, FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
    }

    @Override
    protected void bindData() {

    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showLogoApp(false);
            ((HomeActivity) getActivity()).setTitle(getString(R.string.about_title));
        }
    }

    @Override
    public boolean onBack() {
        return false;
    }

    @OnClick({R.id.tv_about_luxury_card,
            R.id.tv_about_terms,
            R.id.tv_about_privacy,
            R.id.tv_about_faqs, R.id.tv_about_footer})
    public void onClick(View view) {
        switch (view.getId()) {
            /*case R.id.tv_about_luxury_card:
                //pushFragment(new AboutAspireFragment(), true, true);
                openAboutWebFragment(getString(R.string.text_title_about),
                        getString(R.string.text_path_about)
                );
                break;
            case R.id.tv_about_terms:
                openAboutWebFragment(getString(R.string.text_title_luxury_card_concierge),
                        getString(R.string.text_path_terms_of_use)
                );
                break;
            case R.id.tv_about_privacy:
                openAboutWebFragment(getString(R.string.text_title_terms_of_use),
                        getString(R.string.text_path_privacy_policy)
                );
                break;
            case R.id.tv_about_faqs:
                openAboutWebFragment(getString(R.string.about_faqs),
                        getString(R.string.text_path_FAQ)
                );
                break;
            case R.id.tv_about_footer:
                //pushFragment(new AboutAspireFragment(), true, true);
                openAboutWebFragment(getString(R.string.text_title_about),
                                     getString(R.string.text_path_about)
                                    );
                break;*/
            case R.id.tv_about_luxury_card:
                pushFragment(new AboutAspireFragment(), true, true);
                break;
            case R.id.tv_about_terms:
                openAboutWebFragment(getString(R.string.about_terms),
                        getString(R.string.text_path_terms_of_use)
                );
                break;
            case R.id.tv_about_privacy:
                openAboutWebFragment(getString(R.string.about_privacy),
                        getString(R.string.text_path_privacy_policy)
                );
                break;
            case R.id.tv_about_faqs:
                openAboutWebFragment(getString(R.string.about_faqs),
                        getString(R.string.text_path_FAQ)
                );
                break;
        }
    }

    private void openAboutWebFragment(String title, String linkAssetsFile) {
        Bundle bundle = new Bundle();
        bundle.putString(AboutWebFragment.AboutWebTitle, title);
        bundle.putString(AboutWebFragment.AboutWebLink, linkAssetsFile);

        AboutWebFragment fragment = new AboutWebFragment();
        fragment.setArguments(bundle);

        pushFragment(fragment, true, true);
    }
}
