package com.rosbank.android.russia.apiservices.RequestModel;


import com.google.gson.annotations.Expose;

public class FavouriteRequest
        extends BaseRequest {
    @Expose
    private String UserID;
    @Expose
    private String ItemId;

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public String getItemId() {
        return ItemId;
    }

    public void setItemId(String itemId) {
        ItemId = itemId;
    }
}
