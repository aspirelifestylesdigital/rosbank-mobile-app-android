package com.rosbank.android.russia.utils;

import android.text.TextUtils;

import com.rosbank.android.russia.application.AppConstant;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DateTimeUtil {

    private static DateTimeUtil instance;
    private final static long MILLISECS_PER_DAY = 24 * 60 * 60 * 1000;
    private final String ISO_8601 = "yyyy-MM-dd HH:mm:ss";//2015-03-15T20:58:22+00:00
    private final String BIRTHDAY_FORMAT = "mm/dd/yyyy";
    public static final String FORMAT_ALBUM_PERSO_PROFILE = "d MMMM 'at' hh:mm";

    /*public static final String CUSTODY_BOTTLE_DETAIL_FORMAT = "dd MMM yy";
    public static final String CUSTOMER_DETAIL_FORMAT = "dd MMM yy";
    public static final String CUSTODY_SEARCH_FORMAT = "dd MMM yyyy hh:mma";
    public static final String BOTTLE_DETAIL_TITLE_FORMAT = "MM/dd/yyyy' \u2022 'KK:mma";
    public static final String BOTTLE_DETAIL_PAGE_FORMAT = "d MMM. yyyy 'at' hh:mma";*/

    private DateTimeUtil() {
    }

    public static DateTimeUtil shareInstance() {
        if (instance == null) {
            instance = new DateTimeUtil();
        }
        return instance;
    }

    /**
     * =============================================================================================
     * main function of Date utils
     */
    public String convertTime(String time, String fromFormat, String toFormat) {
        if (!TextUtils.isEmpty(time)) {
            SimpleDateFormat sdf = new SimpleDateFormat(fromFormat);
            try {
                Date date = sdf.parse(time);
                sdf.applyPattern(toFormat);
                return sdf.format(date);
            } catch (ParseException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return "";
    }

    /**
     * Convert a timer with format to long timestamp
     * Ex:
     * Time (2015-03-15T20:58:22+00:00) with format (yyyy-MM-ddTHH:mm:ss) -> timestamp milisecond.
     */
    public long convertToTimeStamp(String time, String format) {
        if (!TextUtils.isEmpty(time)) {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            try {
                Date date = sdf.parse(time);
                return date.getTime();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    public long convertToTimeStampFromGMT(String time, String format) {
        if (!TextUtils.isEmpty(time)) {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
            try {
                Date date = sdf.parse(time);
                return date.getTime();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    /**
     * This func help to convert timestamp (milisecond) to format you want.
     * Ex: 344363534537 -> 2015-03-15T20:58:22+00:00 with format (yyyy-MM-ddTHH:mm:ss)
     */
    public String formatTimeStamp(long time, String toFormat) {
        SimpleDateFormat sdf = new SimpleDateFormat(toFormat);
        try {
            Date date = new Date(time);
            return sdf.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * =============================================================================================
     * Compare func
     */

    public int compareDate(String date1, String date2) {
        long d1 = convertToTimeStamp(date1, ISO_8601);
        long d2 = convertToTimeStamp(date1, ISO_8601);
        if (d1 < d2)
            return -1;
        if (d1 > d2)
            return 1;
        return 0;
    }

    public int compareDate(Date date1, Date date2) {
        long d1 = date1.getTime();
        long d2 = date2.getTime();
        if (d1 < d2)
            return -1;
        if (d1 > d2)
            return 1;
        return 0;
    }

    /**
     * 1 minute = 60 seconds
     * 1 hour = 60 x 60 = 3600
     * 1 day = 3600 x 24 = 86400
     * <p>
     * Result array with 5 items: month, week, day, hour, minute, second
     */
    public long[] getDistanceTime(Date startDate, Date endDate) {

        /*long milliStart = convertToTimeStamp(start, ISO_8601);
        long milliEnd = convertToTimeStamp(start, ISO_8601);

        //milliseconds
        long different = milliEnd - milliStart;*/

        return getDistanceTime(startDate.getTime(), endDate.getTime());

    }

    public long[] getDistanceTime(long startTime, long endTime) {
        long different = endTime - startTime;

        /*System.out.println("startDate : " + startDate);
        System.out.println("endDate : " + endDate);
        System.out.println("different : " + different);*/

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;
        long weeksInMilli = daysInMilli * 7;
        long monthsMilli = daysInMilli * 30;//default

        long elapsedMoths = different / monthsMilli;
        //different = different % monthsMilli;

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(endTime);

        int count = 0;
        int m = calendar.get(Calendar.MONTH);
        int y = calendar.get(Calendar.YEAR);

        while (true) {

            calendar.set(Calendar.MONTH, m);
            int days = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
            long milliMonth = days * daysInMilli;

            //System.out.println( (m+1) + " " + days);
            if (different < milliMonth) {
                break;
            }

            different -= milliMonth;
            m -= 1;

            if (m < 0) {
                m = 11;
                y -= 1;
                calendar.set(Calendar.YEAR, y);
            }

            count += 1;
        }

        elapsedMoths = count;

        long elapsedWeeks = different / weeksInMilli;
        different = different % weeksInMilli;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        /*System.out.printf(
                "%d months, %d days, %d hours, %d minutes, %d seconds%n",
                elapsedMoths, elapsedDays,
                elapsedHours, elapsedMinutes, elapsedSeconds);*/

        long[] values = new long[6];
        values[0] = elapsedMoths;
        values[1] = elapsedWeeks;
        values[2] = elapsedDays;
        values[3] = elapsedHours;
        values[4] = elapsedMinutes;
        values[5] = elapsedSeconds;

        return values;

        /**
         * ==============================================================
         * way2
         * */
        /*different = endDate.getTime() - startDate.getTime();
        int day = (int) TimeUnit.MILLISECONDS.toDays(different);
        int hh = (int) (TimeUnit.MILLISECONDS.toHours(different) - TimeUnit.DAYS.toHours(day));
        int mm = (int) (TimeUnit.MILLISECONDS.toMinutes(different) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(different)));

        System.out.printf(
                "%d days, %d hours, %d minutes, %d seconds%n",
                day,
                hh, mm, elapsedSeconds);*/
    }

    public int getMaxTimeInArray(long[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] > 0) {
                return i;
            }
        }
        return array.length - 1;
    }

    public long getDistanceMinute(long startTime, long endTime) {
        long different = startTime - endTime;
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;

        long elapsedMinutes = different / minutesInMilli;

        return elapsedMinutes;
    }

    /**
     * =============================================================================================
     * Extend func for App
     */
    public String convertISO8601ToBirthdayFormat(String time) {
        try {
            return convertTime(time,
                    ISO_8601,
                    BIRTHDAY_FORMAT);
        } catch (Exception ex) {
            ex.printStackTrace();
            return "";
        }
    }

    public String convertTimeStampToISO8601(long time) {
        try {
            return formatTimeStamp(time, ISO_8601);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public String convertTimeStampToPersoProfile(long time) {
        try {
            return formatTimeStamp(time, FORMAT_ALBUM_PERSO_PROFILE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public String convertTimeStampToBirthdayFormat(long time) {
        try {
            return formatTimeStamp(time, BIRTHDAY_FORMAT);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public long convertBirthdayToTimeStamp(String time) {
        try {
            return convertToTimeStamp(time, BIRTHDAY_FORMAT);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int getSignedDiffInDays(String beginDate, String beginDateFormat, String endDate, String endDateFormat) {
        long beginMS = convertToTimeStamp(beginDate, beginDateFormat);
        long endMS = convertToTimeStamp(endDate, endDateFormat);
        long diff = (endMS - beginMS) / (MILLISECS_PER_DAY);
        return Math.max((int) diff, 1);
    }

    public boolean checkLaterThan24Hours(int year, int month, int day, int hour, int minute) {
        Calendar nowCal = Calendar.getInstance();
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day, hour, minute);

        // Check later than 24 hours
        return (calendar.getTimeInMillis() - nowCal.getTimeInMillis()) / (1000 * 60 * 60) >= 24;
    }

    public boolean checkLaterThan24Hours(Calendar calendar) {
        Calendar nowCal = Calendar.getInstance();
        // Check later than 24 hours
        return (calendar.getTimeInMillis() - nowCal.getTimeInMillis()) / (1000 * 60 * 60) >= 24;
    }

    public boolean checkLaterThan24Hours(long milisec) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milisec);
        return checkLaterThan24Hours(calendar);
    }

    public boolean checkIfTimeInThePast(Calendar calendar) {
        Calendar currentCalendar = Calendar.getInstance();
        currentCalendar.add(Calendar.MINUTE, AppConstant.MINUTE_EXTRA_DINING_BOOKING);
        return calendar.getTimeInMillis() <= currentCalendar.getTimeInMillis();
    }

    public long makeToLatestTimeOfADay(long milisec) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milisec);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        return calendar.getTimeInMillis();
    }
}
