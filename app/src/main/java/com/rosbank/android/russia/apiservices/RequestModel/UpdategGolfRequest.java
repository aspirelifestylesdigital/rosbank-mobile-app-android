package com.rosbank.android.russia.apiservices.RequestModel;


import com.google.gson.annotations.Expose;

import java.util.List;

public class UpdategGolfRequest
        extends BaseRequest {
    @Expose
    private String UserID;
    @Expose
    private String CourseName;
    @Expose
    private List<String> TeeTimes;

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public String getCourseName() {
        return CourseName;
    }

    public void setCourseName(String courseName) {
        CourseName = courseName;
    }

    public List<String> getTeeTimes() {
        return TeeTimes;
    }

    public void setTeeTimes(List<String> teeTimes) {
        TeeTimes = teeTimes;
    }
}
