package com.rosbank.android.russia.model.concierge;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CGetRecentRequestResponse;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.model.CountryObject;
import com.rosbank.android.russia.utils.StringUtil;
import com.rosbank.android.russia.widgets.MyRequestsHeaderView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public abstract class MyRequestObject
        implements Serializable {

    private String BookingItemID;
    private String epcCaseId;
    private String MobileNumber;
    private String EmailAddress;
    private String PrefResponse;
    private String RequestStatus;
    protected String RequestMode;

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(final String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(final String lastName) {
        LastName = lastName;
    }

    private String FirstName;
    private String LastName;

    protected AppConstant.CONCIERGE_REQUEST_TYPE BookingRequestType;
    protected AppConstant.BOOKING_FILTER_TYPE BookingFilterGroupType;

    protected String Country;
    protected String City;
    protected String State;

    protected String ItemTitle;
    protected String ItemDescription;

    protected String EventName;
    protected String EventCategory;

    public String getRequestDetaiString() {
        return requestDetaiString;
    }

    public void setRequestDetaiString(final String requestDetaiString) {
        this.requestDetaiString = requestDetaiString;
    }

    protected String requestDetaiString;
    protected ConciergeRequestDetail conciergeRequestDetail;

    public MyRequestObject(B2CGetRecentRequestResponse recentRequestResponse) {
        if (recentRequestResponse != null) {
            BookingItemID = recentRequestResponse.getTransactionID();
            epcCaseId = recentRequestResponse.getEPCCaseID();
            MobileNumber = recentRequestResponse.getPHONENUMBER();
            if (!TextUtils.isEmpty(recentRequestResponse.getEMAILADDRESS1())) {
                EmailAddress = recentRequestResponse.getEMAILADDRESS1();
            } else {
                EmailAddress = recentRequestResponse.getEMAIL1();
            }
            FirstName = recentRequestResponse.getFIRSTNAME();
            LastName = recentRequestResponse.getLASTNAME();
            Country = recentRequestResponse.getCOUNTRY();
            City = recentRequestResponse.getCITY();
            State = recentRequestResponse.getSTATE();
            PrefResponse = recentRequestResponse.getPREFRESPONSE();
            RequestStatus = recentRequestResponse.getREQUESTSTATUS();
            RequestMode = recentRequestResponse.getREQUESTMODE();
            BookingRequestType = AppConstant.CONCIERGE_REQUEST_TYPE.getEnum(recentRequestResponse.getREQUESTTYPE());
            requestDetaiString = recentRequestResponse.getREQUESTDETAILS();
            EventName = recentRequestResponse.getEVENTNAME();
            EventCategory = recentRequestResponse.getEVENTCATEGORY();
            initConciergeRequestDetail(recentRequestResponse.getREQUESTDETAILS());
            initRequestProperties();
        }
    }

    public static List<MyRequestObject> cast(List<B2CBaseResponse> responseList) {
        List<MyRequestObject> myRequestObjectList = new ArrayList<>();
        if (responseList != null && responseList.size() > 0) {
            for (B2CBaseResponse b2CBaseResponse : responseList) {
                if (b2CBaseResponse instanceof B2CGetRecentRequestResponse) {
                    MyRequestObject myRequestObject = null;
                    AppConstant.CONCIERGE_REQUEST_TYPE requestType = AppConstant.CONCIERGE_REQUEST_TYPE.getEnum(((B2CGetRecentRequestResponse) b2CBaseResponse).getREQUESTTYPE());
                    switch (requestType) {
                        case BOOK_HOTEL:
                        case RECOMMEND_HOTEL:
                            myRequestObject = new HotelBookingDetailData((B2CGetRecentRequestResponse) b2CBaseResponse);
                            if (TextUtils.isEmpty(myRequestObject.conciergeRequestDetail.getHotelName())) {
                                myRequestObject.setBookingRequestType(AppConstant.CONCIERGE_REQUEST_TYPE.RECOMMEND_HOTEL);
                            }
                            break;
                        case RESERVE_TABLE:
                        case RECOMMEND_RESTAURANT:
                            myRequestObject = new RestaurantBookingDetailData((B2CGetRecentRequestResponse) b2CBaseResponse);
                            if (TextUtils.isEmpty(myRequestObject.conciergeRequestDetail.getRestaurantName())) {
                                myRequestObject.setBookingRequestType(AppConstant.CONCIERGE_REQUEST_TYPE.RECOMMEND_RESTAURANT);
                            }
                            break;
                        case T_CAR_RENTAL:
                        case T_CAR_TRANSFER:
                            myRequestObject = new CarBookingDetailData((B2CGetRecentRequestResponse) b2CBaseResponse);
                            break;
                        case GOLF:
                            myRequestObject = new GolfBookingDetailData((B2CGetRecentRequestResponse) b2CBaseResponse);
                            break;
                        case EVENT:
                        case RECOMMEND_EVENT:
                            myRequestObject = new EventBookingDetailData((B2CGetRecentRequestResponse) b2CBaseResponse);
                            if (TextUtils.isEmpty(myRequestObject.conciergeRequestDetail.getEventName())) {
                                myRequestObject.setBookingRequestType(AppConstant.CONCIERGE_REQUEST_TYPE.RECOMMEND_EVENT);
                            }
                            break;
                        case OTHER_REQUEST:
                            if (((B2CGetRecentRequestResponse) b2CBaseResponse).getREQUESTDETAILS().contains("Golf Course")) {
                                ((B2CGetRecentRequestResponse) b2CBaseResponse).setREQUESTTYPE(AppConstant.CONCIERGE_REQUEST_TYPE.GOLF.getValue());
                                myRequestObject = new GolfBookingDetailData((B2CGetRecentRequestResponse) b2CBaseResponse);

                            } else {
                                if (((B2CGetRecentRequestResponse) b2CBaseResponse).getREQUESTDETAILS().contains("Spa Name")) {
                                    ((B2CGetRecentRequestResponse) b2CBaseResponse).setREQUESTTYPE(AppConstant.CONCIERGE_REQUEST_TYPE.SPA.getValue());
                                    myRequestObject = new SpaBookingDetailData((B2CGetRecentRequestResponse) b2CBaseResponse);

                                } else {
                                    myRequestObject = new OtherBookingDetailData((B2CGetRecentRequestResponse) b2CBaseResponse);
                                }
                            }

                            break;

                    }
                    myRequestObjectList.add(myRequestObject);
                }
            }
        }
        return myRequestObjectList;
    }

    protected abstract void initRequestProperties();

    public abstract boolean isBookNormalType();

    public abstract long getRequestStartDateEpoch();

    public abstract long getRequestEndDateEpoch();

    public boolean isUpcoming() {
        long startDateEpoc = getRequestStartDateEpoch();
        if (startDateEpoc > 0) {
            return startDateEpoc > System.currentTimeMillis() && (!TextUtils.isEmpty(RequestMode) && !RequestMode.equalsIgnoreCase("cancel"));
        }
        return false;
//        return (TextUtils.isEmpty(RequestMode) || !RequestMode.equalsIgnoreCase("cancel"))&&
//                !TextUtils.isEmpty(RequestStatus) &&
//                (RequestStatus.toLowerCase().contains("open") || RequestStatus.toLowerCase().contains("progress"));
    }

    public boolean isAmendCancelPermitted() {
        long startDateEpoc = getRequestStartDateEpoch();
        return startDateEpoc > Calendar.getInstance().getTimeInMillis() && (!TextUtils.isEmpty(RequestMode) && !RequestMode.equalsIgnoreCase("cancel"));
    }

    protected void initConciergeRequestDetail(String requestDetailStr) {
        String[] requestDetailItems = requestDetailStr.split("\\s*\\|\\s*");
        if (requestDetailItems != null && requestDetailItems.length > 0) {
            JSONObject jsonObject = new JSONObject();
            for (String item : requestDetailItems) {
                String[] keyValue = StringUtil.getKeyValueFromSplitor(item, "-");
                try {
                    if (keyValue != null && keyValue.length == 2) {
                        String key = keyValue[0];
                        if (key != null) {
                            if (key.contains("&")) {
                                key = key.replaceAll("&",
                                        "");
                            }
                            if (key.contains("/")) {
                                key = key.replaceAll("/",
                                        "");
                            }
                            if (key.contains(".")) {
                                key = key.replaceAll("\\.",
                                        "");
                            }
                            if (key.contains(" ")) {
                                key = key.replaceAll(" ",
                                        "");
                            }
                        }
                        jsonObject.put(key, keyValue[1]);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (jsonObject.length() > 0) {
                // Deserialize
                conciergeRequestDetail = new Gson().fromJson(jsonObject.toString(), ConciergeRequestDetail.class);
            }
        }
        if (conciergeRequestDetail == null) { // Implicit empty object
            conciergeRequestDetail = new ConciergeRequestDetail();
        }
    }


    public AppConstant.CONCIERGE_REQUEST_TYPE getBookingRequestType() {
        return BookingRequestType;
    }

    public void setBookingRequestType(AppConstant.CONCIERGE_REQUEST_TYPE bookingRequestType) {
        BookingRequestType = bookingRequestType;
    }

    public AppConstant.BOOKING_FILTER_TYPE getBookingFilterGroupType() {
        return BookingFilterGroupType;
    }

    public void setBookingFilterGroupType(AppConstant.BOOKING_FILTER_TYPE bookingFilterGroupType) {
        BookingFilterGroupType = bookingFilterGroupType;
    }

    public String getFullName() {
        if (FirstName != null && LastName != null) {
            return FirstName + " " + LastName;
        } else {
            if (FirstName != null) {
                return FirstName;
            } else {
                if (LastName != null) {
                    return LastName;
                } else {
                    return "";
                }
            }
        }
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public String getEmailAddress() {
        return EmailAddress;
    }

    public String getCountry() {
        if (Country == null) {
            Country = conciergeRequestDetail.getCountry();
        }
        return Country;
    }

    public String getCity() {
        if (!TextUtils.isEmpty(City)) {
            return City;
        }
        City = conciergeRequestDetail != null ? conciergeRequestDetail.getCity() : null;
        return City;
    }

    public String getState() {
        if (!TextUtils.isEmpty(State)) {
            return State;
        }
        State = conciergeRequestDetail != null ? conciergeRequestDetail.getState() : null;
        return State;
    }

    public String getItemDescription() {
        return ItemDescription;
    }

    public String getBookingItemID() {
        return BookingItemID;
    }

    public void setBookingItemID(final String bookingItemID) {
        BookingItemID = bookingItemID;
    }

    public String getEpcCaseId() {
        return epcCaseId;
    }

    public void setItemTitle(final String itemTitle) {
        ItemTitle = itemTitle;
    }

    public String getItemTitle() {
        return ItemTitle;
    }

    public void setItemDescription(final String itemDescription) {
        ItemDescription = itemDescription;
    }

    public String getCompoundCountryAndCity() {
        String compound = "";
        if (!TextUtils.isEmpty(getCity()) && !getCity().equals(AppConstant.B2C_REQUEST_DETAIL_NOT_AVAILABLE)) {
            compound += getCity();
        }
        if (!TextUtils.isEmpty(getState()) && !getState().equals(AppConstant.B2C_REQUEST_DETAIL_NOT_AVAILABLE)) {
            if (TextUtils.isEmpty(compound)) {
                compound += getState();
            } else {
                compound += ", " + getState();
            }
        }
        if (!TextUtils.isEmpty(getCountry()) && !getCountry().equals(AppConstant.B2C_REQUEST_DETAIL_NOT_AVAILABLE)) {
            if (TextUtils.isEmpty(compound)) {
                compound += getCountry();
            } else {
                compound += ", " + getCountry();
            }
        }
        return compound;
    }

    public abstract String getReservationName();

    public static List<MyRequestObject> filterItems(final List<MyRequestObject> sourceItems,
                                                    final AppConstant.BOOKING_FILTER_TYPE filter,
                                                    final String filterGroup) {
        if (sourceItems == null) {
            return new ArrayList<>();
        }

        final List<MyRequestObject> destinationItems = new ArrayList<>();
        for (final MyRequestObject item : sourceItems) {
            if (MyRequestsHeaderView.PRE_REQUEST_TYPE_ALL_UPCOMING.equals(filterGroup)) {
                if (item.isUpcoming()) {
                    switch (filter) {
                        case All:
                            destinationItems.add(item);
                            break;

                        default:
                            if (item.getBookingFilterGroupType() != null) {
                                if (item.getBookingFilterGroupType().compareTo(filter) == 0) {
                                    destinationItems.add(item);
                                }
                            }
                            break;
                    }

                }
            } else {
                if (!item.isUpcoming()) {
                    switch (filter) {
                        case All:
                            destinationItems.add(item);
                            break;

                        default:
                            if (item.getBookingFilterGroupType() != null) {
                                if (item.getBookingFilterGroupType().compareTo(filter) == 0) {
                                    destinationItems.add(item);
                                }
                            }
                            break;
                    }
                }
            }
        }

        return destinationItems;
    }

    public String getRequestStatus() {
        return RequestStatus;
    }

    public void setRequestStatus(String requestStatus) {
        RequestStatus = requestStatus;
    }

    public String getPrefResponse() {
        return PrefResponse;
    }

    public void setPrefResponse(String prefResponse) {
        PrefResponse = prefResponse;
    }

    public static final String GOURMET_REQUEST = "Restaurant";
    public static final String GOURMET_RECOMMEND_REQUEST = "RestaurantRecommendation";
    public static final String HOTEL_REQUEST = "Hotel";
    public static final String HOTEL_RECOMMEND_REQUEST = "HotelRecommendation";
    public static final String ENTERTAINMENT_REQUEST = "Entertainment";
    public static final String CAR_RENTAL_REQUEST = "CarRental";
    public static final String CAR_TRANSFER_REQUEST = "CarTransfer";
    public static final String GOLF_REQUEST = "Golf";
    public static final String OTHER_REQUEST = "OTHER_REQUEST";
    //  Restaurant,Tour,Travel,Golf,Entertainment,Other,Flight,Hotel,CarRental,CarTransfer,RestaurantRecommendation, HotelRecommendation

    public AppConstant.CONCIERGE_REQUEST_TYPE getBookingRequestEnum() {
        return BookingRequestType;
    }

    public ConciergeRequestDetail getConciergeRequestDetail() {
        return conciergeRequestDetail;
    }

    public String getFullAddress() {
        return conciergeRequestDetail != null ? conciergeRequestDetail.getFullAddress() : "";
    }

    public String getPrivilegeId() {
        return conciergeRequestDetail != null ? conciergeRequestDetail.getPrivilegeId() : "";
    }

    public String getSpaName() {
        return conciergeRequestDetail != null ? conciergeRequestDetail.getSpaName() : "";
    }


}
