package com.rosbank.android.russia.utils;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.TypedValue;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by nga.nguyent on 10/21/2015.
 */
public class PhotoPickerUtils {

    public static final int PICK_IMAGE_REQUEST_CODE = 2011;
    public static final int TAKE_PICTURE_REQUEST_CODE = 2012;

    private static PhotoPickerUtils instance=null;
    private Context context;

    private PhotoPickerUtils(Context context) {
        this.context = context;
    }

    public static PhotoPickerUtils shareInstance(Context context){
        if(instance==null){
            instance = new PhotoPickerUtils(context);
        }
        return instance;
    }

    /**
     * =============================================================================================
     * Camera
     * */
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;

    public interface OnPhotoTypeListener{
        void onCamera();
        void onGallery();
    }

    public void showChooseTypePhotoDialog(final OnPhotoTypeListener listener){

        Resources r = context.getResources();
        int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10/*10dp*/, r.getDisplayMetrics());
        int pLeft = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20/*10dp*/, r.getDisplayMetrics());

        TextView tvCamera = new TextView(context);
        TextView tvGallery = new TextView(context);
        tvCamera.setText("Take Photo");
        tvGallery.setText("Choose from Gallery");
        tvCamera.setPadding(pLeft,pLeft, pLeft, px);
        tvGallery.setPadding(pLeft,px, pLeft, px);

        LinearLayout view = new LinearLayout(context);
        view.setOrientation(LinearLayout.VERTICAL);

        view.addView(tvCamera);
        view.addView(tvGallery);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("ATTACH PHOTO");
        //builder.setMessage("ATTACH PHOTO");
        builder.setView(view);
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        final Dialog dialog = builder.show();

        tvCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener!=null){
                    listener.onCamera();
                }
                dialog.dismiss();
            }
        });
        tvGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener!=null){
                    listener.onGallery();
                }
                dialog.dismiss();
            }
        });
    }

    /** Check if this device has a camera */
    public boolean checkCameraHardware() {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

    public void openPickImage(Fragment fragment, int PICK_IMAGE_REQUEST){
        /*
        http://stackoverflow.com/questions/8960072/onactivityresult-with-launchmode-singletask
        */
        Intent intent = new Intent();
        // Show only images, no videos or anything else
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        /*intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);*/

        // Always show the chooser (if there are multiple options available)
        fragment.startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    public Uri openCamera(Fragment fragment, int CAMERA_REQUEST){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        // create a file to save the image
        Uri fileUri = getOutputMediaFileUri(PhotoPickerUtils.MEDIA_TYPE_IMAGE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri); // set the image file name

        // start the image capture Intent
        fragment.startActivityForResult(intent, CAMERA_REQUEST);

        return fileUri;
    }

    public void showDialogCameraNotFound(){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Not found camera on your device");
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    /** Create a file Uri for saving an image or video */
    public Uri getOutputMediaFileUri(int type){
        File f = getOutputMediaFile(type);
        return Uri.fromFile(f);
    }

    /** Create a File for saving an image or video */
    private File getOutputMediaFile(int type){
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "Krowdis");
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                System.out.println("failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile = null;
        if (type == MEDIA_TYPE_IMAGE){
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_"+ timeStamp + ".jpg");
        } else if(type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "VID_"+ timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }

    /**
     * =============================================================================================
     * Get absolute path from uri of image.
     * */

    public String getRealPathFromURI(Uri uri){

        int sdk = Build.VERSION.SDK_INT;
        String realPath = "";

        //realPath = getRealPathFromURIAllVer(uri);
        // SDK < API11
        if (sdk < 11)
            realPath = getRealPathFromURI_BelowAPI11(uri);
        else if (sdk < 19)
            // SDK >= 11 && SDK < 19
            realPath = getRealPathFromURI_API11to18(uri);
        else {
            // SDK >= 19 (Android 4.4)
            realPath = getRealPathFromURI_API19(uri);
        }

        return realPath;
    }

    /**
     * This is util using for get real path of image in gallery
     * */
    @SuppressLint("NewApi")
    public String getRealPathFromURI_API19(Uri uri) {
        if (uri.getHost().contains("com.android.providers.media")) {
            String filePath = "";
            String wholeID = DocumentsContract.getDocumentId(uri);

            // Split at colon, use second item in the array
            String[] ids = wholeID.split(":");
            String id;
            if (ids.length == 1)
                id = wholeID.split(":")[0];
            else
                id = wholeID.split(":")[1];

            String[] column = {MediaStore.Images.Media.DATA};

            // where id is equal to
            String sel = MediaStore.Images.Media._ID + "=?";

            Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    column, sel, new String[]{id}, null);

            int columnIndex = cursor.getColumnIndex(column[0]);

            if (cursor.moveToFirst()) {
                filePath = cursor.getString(columnIndex);
            }
            cursor.close();
            return filePath;
        }
        return getRealPathFromURI_BelowAPI11(uri);
    }


    @SuppressLint("NewApi")
    public String getRealPathFromURI_API11to18(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        String result = null;

        CursorLoader cursorLoader = new CursorLoader(
                context,
                contentUri, proj, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();

        if (cursor != null) {
            int column_index =
                    cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            result = cursor.getString(column_index);
        }
        return result;
    }

    public String getRealPathFromURI_BelowAPI11(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(contentUri, null/*proj*/, null, null, null);

        if (cursor == null)
            return null;

        int column_index
                = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();

        String s=cursor.getString(column_index);
        cursor.close();
        return s;
    }

    /**
     * =============================================================================================
     * Detach bitmap and resize
     * */
    public Bitmap getBitmapResize(String filePath, int width, int height) {
        int reqHeight = height;
        int reqWidth = width;

        BitmapFactory.Options options = new BitmapFactory.Options();

        // First decode with inJustDecodeBounds=true to check dimensions
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(filePath, options);
    }

    private int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {

        final int height = options.outHeight;
        final int width = options.outWidth;

        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;

        }
        return inSampleSize;
    }

    /**
     * =============================================================================================
     * Save bitmap function
     * */
    public void saveBitmapExternalStorageDirectory(Bitmap bitmap){
        try {
            String path = Environment.getExternalStorageDirectory().toString();
            File file = new File(path, "capture" + ".jpg"); // the File to save to

            OutputStream fOut = new FileOutputStream(file);

            // saving the Bitmap to a file compressed as a JPEG with 85% compression rate
            bitmap.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
            fOut.flush();

            // do not forget to close the stream
            fOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * save internal app
     * */
    private String internalFileName = "image.jpg";
    public String saveBitmapLocalPrivate(Bitmap bitmap) {
        String fileName = internalFileName;//no .png or .jpg needed
        try {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            FileOutputStream fo = context.openFileOutput(fileName, Context.MODE_PRIVATE);
            fo.write(bytes.toByteArray());
            // remember close file output
            fo.close();
        } catch (Exception e) {
            e.printStackTrace();
            fileName = null;
        }
        return fileName;
    }

    public Bitmap getBitmapLocalPrivate(){
        Bitmap bitmap = null;
        try {
            bitmap = BitmapFactory.decodeStream(context.openFileInput(internalFileName));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return bitmap;
    }

    public File getBitmapLocalPrivateFile(){

        try {
            //bitmap = BitmapFactory.decodeStream(context.openFileInput(internalFileName));
            File f = new File(context.getFilesDir(), internalFileName);
            if(f.exists()){
                return f;
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public Bitmap getBimap(Uri uri){
        Bitmap bitmap = null;
        try {
            bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    /**
     * save in external local
     * */
    public String saveBitmapExternal(Bitmap bitmap, String fileName) {

        try {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

            File f = new File( context.getExternalCacheDir(), fileName );

            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            // remember close file output
            fo.close();

            return f.getAbsolutePath();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public File getBitmapExternalFile(String fileName){

        try {
            File f = new File( context.getExternalCacheDir(), fileName );
            if(f.exists()){
                return f;
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public Bitmap getBitmapFromPath(String path){
        File imagefile = new File(path);
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(imagefile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        Bitmap bm = BitmapFactory.decodeStream(fis);
        return bm;
    }

}
