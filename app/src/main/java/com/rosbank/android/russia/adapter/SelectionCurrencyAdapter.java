package com.rosbank.android.russia.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.model.CurrencyObject;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.FontUtils;

import java.util.ArrayList;

/**
 * Created by anh.trinh on 9/20/2016.
 */
public class SelectionCurrencyAdapter
        extends ArrayAdapter<CurrencyObject> {
    private final Context context;
    private final ArrayList<CurrencyObject> values;
    private CurrencyObject selectedData;


    public SelectionCurrencyAdapter(Context context, ArrayList<CurrencyObject> values, CurrencyObject selectedData) {
        super(context, -1, values);
        this.context = context;
        this.values = values;
        this.selectedData =selectedData;
    }
    public void setSelectedData(CurrencyObject selectedData){
        this.selectedData = selectedData;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                                                           .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.item_selection, parent, false);
        RelativeLayout layoutContain = (RelativeLayout) rowView.findViewById(R.id.layout_contain);
        TextView textView = (TextView) rowView.findViewById(R.id.tv_selection_name);
        ImageView imgTick = (ImageView) rowView.findViewById(R.id.ic_selection_tick);
        CommonUtils.setFontForViewRecursive(textView,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
        textView.setText(getCurrencyDisplay(values.get(position)));

        if(selectedData!=null &&selectedData.getCurrencyKey()!=null && selectedData.getCurrencyKey().equals(values.get(position).getCurrencyKey())){
            imgTick.setVisibility(View.VISIBLE);
        }else{
            imgTick.setVisibility(View.GONE);
        }
        if(position==0 || position==4){
            layoutContain.setBackgroundColor(getContext().getResources().getColor(R.color.my_currency_background_item));
        }
        return rowView;
    }
    private String getCurrencyDisplay(CurrencyObject currencyObject){
        String result="";
        if(currencyObject!=null){
            if(currencyObject.getCurrencyText()!=null){
                result += currencyObject.getCurrencyText();
                if(currencyObject.getCurrencyKey()!=null && !currencyObject.getCurrencyKey().equals("")){
                    result+=" ("+ currencyObject.getCurrencyKey()+")";
                }
            }

        }
        return result;
    }

    @Override
    public int getCount() {
        return values.size();
    }
}

