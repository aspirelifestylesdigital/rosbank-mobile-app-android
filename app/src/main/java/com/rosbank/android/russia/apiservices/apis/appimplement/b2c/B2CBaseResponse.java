package com.rosbank.android.russia.apiservices.apis.appimplement.b2c;

import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.internal.LinkedTreeMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by ThuNguyen on 11/3/2016.
 */

public abstract class B2CBaseResponse {
    @Expose
    public Object Status;
    @Expose
    public Object status;
    @Expose
    public Object success;
    @Expose
    public String HttpStatusCode;
    @Expose
    public String Message;
    @Expose
    public Object message;

    private String errorCode;
    private String actualMessage;

    public boolean isSuccess(){
        return isTextInSuccess(Status) || isTextInSuccess(success) || isTextInSuccess(status) || isTextInSuccess(HttpStatusCode);
    }
    public String getMessage(){
        if(!TextUtils.isEmpty(actualMessage)){
            return actualMessage;
        }
        String tempMessage = Message;
        if(TextUtils.isEmpty(Message)){
            if(message != null && message instanceof List){
                Object firstItem = ((List)message).get(0);
                if(firstItem instanceof LinkedTreeMap) {
                    LinkedTreeMap textLinkedTreeMap = (LinkedTreeMap) firstItem;
                    actualMessage = textLinkedTreeMap.get("message").toString();
                    errorCode = textLinkedTreeMap.get("code").toString();
                }
            }

        }
        if(!TextUtils.isEmpty(tempMessage) && TextUtils.isEmpty(actualMessage)) {
            try {
                JSONArray jsonArray = new JSONArray(tempMessage);
                if (jsonArray != null && jsonArray.length() > 0) {
                    JSONObject messageObj = jsonArray.getJSONObject(0);
                    if (messageObj != null) {
                        actualMessage = messageObj.optString("message");
                        errorCode = messageObj.optString("code");
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return actualMessage;
    }
    public String getErrorCode(){
        getMessage();
        return errorCode;

    }
    private boolean isTextInSuccess(Object text){
        boolean isSuccess = false;
        if(text != null) {
            if (text instanceof String) {
                String textStr = (String)text;
                isSuccess = (!TextUtils.isEmpty(textStr) &&
                        (textStr.equalsIgnoreCase("true") || textStr.equalsIgnoreCase("valid")
                                || textStr.equalsIgnoreCase("success") || textStr.equalsIgnoreCase("201")));
            }else if(text instanceof LinkedTreeMap){
                LinkedTreeMap textLinkedTreeMap = (LinkedTreeMap)text;
                if(textLinkedTreeMap.containsKey("message")) {
                    actualMessage = textLinkedTreeMap.get("message").toString();
                }
                if(textLinkedTreeMap.containsKey("success")) {
                    String successIn = textLinkedTreeMap.get("success").toString();
                    isSuccess = (!TextUtils.isEmpty(successIn) &&
                            (successIn.equalsIgnoreCase("true") || successIn.equalsIgnoreCase("valid")
                                    || successIn.equalsIgnoreCase("success") || successIn.equalsIgnoreCase("201")));
                }
            }
        }
        return isSuccess;
    }
}
