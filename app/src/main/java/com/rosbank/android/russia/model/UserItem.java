package com.rosbank.android.russia.model;

import com.google.gson.annotations.Expose;

import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CGetUserDetailsResponse;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.SharedPreferencesUtils;
import com.rosbank.android.russia.utils.StringUtil;

import android.os.Parcel;
import android.os.Parcelable;

import static com.rosbank.android.russia.activitys.HomeActivity.CAMPAIGN_SAVED;
import static com.rosbank.android.russia.application.AppConstant.*;


public class UserItem
        implements Parcelable {
    @Expose
    private String Email;
    @Expose
    private String Password;
    @Expose
    private String UserID;
    private String OnLineMemberDetailId;
    @Expose
    private String AvatarURL;
    @Expose
    private String AvatarId;
    @Expose
    private String AvatarPostedFile;
    @Expose
    private String Salutation;
    @Expose
    private String FirstName;
    @Expose
    private String LastName;
    @Expose
    private String FullName;
    @Expose
    private String DateOfBirth;
    @Expose
    private String EpochDateOfBirth;
    @Expose
    private String CountryOfBirth;
    @Expose
    private String MobileNumber;
    @Expose
    private String IsActive;
    @Expose
    private String HasForgotPassword;
    @Expose
    private String ZipCode;
    @Expose
    private String InterestMusics;
    @Expose
    private String InterestCuisines;
    @Expose
    private String InterestBrands;
    @Expose
    private String NameOnCard;
    @Expose
    private String MemberCardID;
    @Expose
    private String CardExpiryDate;
    @Expose
    private String UserTier;

    @Expose
    private String ProductType;
    /*@Expose
    private String LoyalProgrammes;*/
    @Expose
    private String CurrentCountry;
    @Expose
    private String CurrentCity;
    @Expose
    private String CurrentRegionCode;
    @Expose
    private String CurrentLatitude;
    @Expose
    private String CurrentLongitude;
    @Expose
    private String Country;

    @Expose
    private String VipStatus;

    public String getEmail() {
        return Email;
    }

    public void setEmail(final String email) {
        Email = email;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(final String password) {
        Password = password;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(final String userID) {
        UserID = userID;
    }

    public String getSalutation() {
        return Salutation;
    }

    public void setSalutation(final String salutation) {
        Salutation = salutation;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(final String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(final String lastName) {
        LastName = lastName;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(final String fullName) {
        FullName = fullName;
    }

    public String getDateOfBirth() {
        return DateOfBirth;
    }

    public void setDateOfBirth(final String dateOfBirth) {
        DateOfBirth = dateOfBirth;
    }

    public String getEpochDateOfBirth() {
        return EpochDateOfBirth;
    }

    public void setEpochDateOfBirth(final String epochDateOfBirth) {
        EpochDateOfBirth = epochDateOfBirth;
    }

    public String getCountryOfBirth() {
        return CountryOfBirth;
    }

    public void setCountryOfBirth(final String countryOfBirth) {
        CountryOfBirth = countryOfBirth;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(final String mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public String getIsActive() {
        return IsActive;
    }

    public void setIsActive(final String isActive) {
        IsActive = isActive;
    }

    public String getHasForgotPassword() {
        return HasForgotPassword;
    }

    public void setHasForgotPassword(final String hasForgotPassword) {
        HasForgotPassword = hasForgotPassword;
    }

    public String getZipCode() {
        return ZipCode;
    }

    public void setZipCode(final String zipCode) {
        ZipCode = zipCode;
    }

    public String getInterestMusics() {
        return InterestMusics;
    }

    public void setInterestMusics(final String interestMusics) {
        InterestMusics = interestMusics;
    }

    public String getInterestCuisines() {
        return InterestCuisines;
    }

    public void setInterestCuisines(final String interestCuisines) {
        InterestCuisines = interestCuisines;
    }

    public String getInterestBrands() {
        return InterestBrands;
    }

    public void setInterestBrands(final String interestBrands) {
        InterestBrands = interestBrands;
    }

    public String getNameOnCard() {
        return NameOnCard;
    }

    public void setNameOnCard(final String nameOnCard) {
        NameOnCard = nameOnCard;
    }

    public String getMemberCardID() {
        return MemberCardID;
    }

    public void setMemberCardID(final String memberCardID) {
        MemberCardID = memberCardID;
    }

    public String getCardExpiryDate() {
        return CardExpiryDate;
    }

    public void setCardExpiryDate(final String cardExpiryDate) {
        CardExpiryDate = cardExpiryDate;
    }

    public String getUserTier() {
        return UserTier;
    }

    public void setUserTier(final String userTier) {
        UserTier = userTier;
    }

    public String getProductType() {
        return ProductType;
    }

    public void setProductType(final String productType) {
        ProductType = productType;
    }

    public String getCurrentCountry() {
        return CurrentCountry;
    }

    public void setCurrentCountry(final String currentCountry) {
        CurrentCountry = currentCountry;
    }

    public String getCurrentCity() {
        return CurrentCity;
    }

    public void setCurrentCity(final String currentCity) {
        CurrentCity = currentCity;
    }

    public String getCurrentRegionCode() {
        return CurrentRegionCode;
    }

    public void setCurrentRegionCode(final String currentRegionCode) {
        CurrentRegionCode = currentRegionCode;
    }

    public String getCurrentLatitude() {
        return CurrentLatitude;
    }

    public void setCurrentLatitude(final String currentLatitude) {
        CurrentLatitude = currentLatitude;
    }

    public String getCurrentLongitude() {
        return CurrentLongitude;
    }

    public void setCurrentLongitude(final String currentLongitude) {
        CurrentLongitude = currentLongitude;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(final String country) {
        Country = country;
    }

    public String getAvatarURL() {
        return AvatarURL;
    }

    public void setAvatarURL(String avatarURL) {
        AvatarURL = avatarURL;
    }

    public String getAvatarId() {
        return AvatarId;
    }

    public void setAvatarId(String avatarId) {
        AvatarId = avatarId;
    }

    public String getAvatarPostedFile() {
        return AvatarPostedFile;
    }

    public void setAvatarPostedFile(String avatarPostedFile) {
        AvatarPostedFile = avatarPostedFile;
    }

    public String getVipStatus() {
        return VipStatus;
    }

    public void setVipStatus(String vipStatus) {
        VipStatus = vipStatus;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest,
                              int flags) {
        dest.writeString(this.UserID);
        dest.writeString(this.Email);
    }

    public UserItem() {
    }

    public UserItem(B2CGetUserDetailsResponse userResponse) {
        if (userResponse != null && userResponse.getMember() != null) {
            B2CGetUserDetailsResponse.MemberExt memberExt = userResponse.getMember();
            UserID = SharedPreferencesUtils.getPreferences(PRE_B2C_ONLINE_MEMBER_ID, "");
            Email = memberExt.getEMAIL();
            FirstName = memberExt.getFIRSTNAME();
            LastName = memberExt.getLASTNAME();
            FullName = memberExt.getFullName();
            Salutation = memberExt.getSALUTATION();
            ZipCode = memberExt.getPOSTALCODE();
            Country = memberExt.getCountry();
            CurrentCity = memberExt.getCity();
            MobileNumber = memberExt.getMOBILENUMBER();
            VipStatus = memberExt.getVip();
        }
        if (userResponse != null && userResponse.getMemberDetails() != null && userResponse.getMemberDetails().size() > 0) {
            OnLineMemberDetailId = userResponse.getMemberDetails().get(0).getONLINEMEMBERDETAILID();
        }
    }

    protected UserItem(Parcel in) {
        this.UserID = in.readString();
        this.Email = in.readString();
    }

    public static final Creator<UserItem> CREATOR = new Creator<UserItem>() {
        @Override
        public UserItem createFromParcel(Parcel source) {
            return new UserItem(source);
        }

        @Override
        public UserItem[] newArray(int size) {
            return new UserItem[size];
        }
    };

    public static boolean isLoginSuccess(UserItem user) {
        if (user == null)
            return false;

        if (StringUtil.isEmpty(user.getEmail()) || StringUtil.isEmpty(user.getUserID()))
            return false;

        return true;
    }

    public static void savePreference(UserItem user, String clientCode) {
        SharedPreferencesUtils.setPreferences(USER_EMAIL_PRE, user.getEmail());
        SharedPreferencesUtils.setPreferences(USER_ID_PRE, user.getUserID());
        SharedPreferencesUtils.setPreferences(USER_ONLINE_MEMBER_DETAIL_ID_PRE, user.getOnLineMemberDetailId());
        SharedPreferencesUtils.setPreferences(USER_FIRST_NAME, user.getFirstName());
        SharedPreferencesUtils.setPreferences(USER_LAST_NAME, user.getLastName());
        SharedPreferencesUtils.setPreferences(USER_FULL_NAME, user.getFullName());
        SharedPreferencesUtils.setPreferences(USER_MOBILE_NUMBER, user.getMobileNumber());
        SharedPreferencesUtils.setPreferences(USER_COUNTRY, user.getCountry());
        SharedPreferencesUtils.setPreferences(USER_CITY, user.getCurrentCity());
        SharedPreferencesUtils.setPreferences(USER_ZIPCODE, user.getZipCode());
        SharedPreferencesUtils.setPreferences(USER_AVATAR, user.getAvatarURL());
        SharedPreferencesUtils.setPreferences(USER_SALUTATION, user.getSalutation());
        // Add USER_VIP_STATUS to find out user status
        SharedPreferencesUtils.setPreferences(USER_VIP_STATUS, clientCode);
    }

    public static void saveClientCode(String clientCode){
        SharedPreferencesUtils.setPreferences(USER_VIP_STATUS, clientCode);
    }

    // add isVip() to find out user status
    public static boolean isVip() {
        String vip = SharedPreferencesUtils.getPreferences(USER_VIP_STATUS, "");
        return !vip.isEmpty() && !vip.equals(CLIENT_CODE_REGULAR);
    }

    public static boolean isLogined() {
        String email = SharedPreferencesUtils.getPreferences(USER_EMAIL_PRE, "");
        String userId = SharedPreferencesUtils.getPreferences(USER_ID_PRE, "");
        if (StringUtil.isEmpty(email) || StringUtil.isEmpty(userId))
//            return true; // must be return false
            return false;
        return true;
    }

    public static String getLoginedId() {
        String userId = SharedPreferencesUtils.getPreferences(USER_ID_PRE, "");
        return userId;
    }

    public static String getLoginedFirstName() {
        String userId = SharedPreferencesUtils.getPreferences(USER_FIRST_NAME, "");
        return userId;
    }

    public static String getLoginedLastName() {
        String userId = SharedPreferencesUtils.getPreferences(USER_LAST_NAME, "");
        return userId;
    }

    public static String getLoginedSalutation() {
        String salutation = SharedPreferencesUtils.getPreferences(USER_SALUTATION, "");
        return salutation;
    }

    public static String getAvatarUrl() {

        return SharedPreferencesUtils.getPreferences(USER_AVATAR, "");
    }

    public static void logOut() {
        SharedPreferencesUtils.removeKey(USER_ID_PRE);
        SharedPreferencesUtils.removeKey(USER_EMAIL_PRE);
        SharedPreferencesUtils.removeKey(USER_VIP_STATUS);
        SharedPreferencesUtils.removeKey(CAMPAIGN_SAVED);
        // Also clear all preference
        CommonUtils.resetManageUserPreference();
    }

    public static String getLoginedFullName() {
        String userId = SharedPreferencesUtils.getPreferences(USER_FULL_NAME, "");
        return userId;
    }

    public static String getLoginedCountry() {
        String userId = SharedPreferencesUtils.getPreferences(USER_COUNTRY, "");
        return userId;
    }

    public static String getLoginedCity() {
        String userId = SharedPreferencesUtils.getPreferences(USER_CITY, "");
        return userId;
    }

    public static String getLoginedZipCode() {
        String userId = SharedPreferencesUtils.getPreferences(USER_ZIPCODE, "");
        return userId;
    }

    public static String getLoginedMobileBumber() {
        String userId = SharedPreferencesUtils.getPreferences(USER_MOBILE_NUMBER, "");
        return userId;
    }

    public static String getLoginedEmail() {
        String userId = SharedPreferencesUtils.getPreferences(USER_EMAIL_PRE, "");
        return userId;
    }

    public String getOnLineMemberDetailId() {
        return OnLineMemberDetailId;
    }

    public void setOnLineMemberDetailId(String onLineMemberDetailId) {
        OnLineMemberDetailId = onLineMemberDetailId;
    }
}
