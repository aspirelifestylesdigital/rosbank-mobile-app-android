package com.rosbank.android.russia.apiservices.b2c;

import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CApiProviderClient;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CICallback;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.B2CForgotPasswordRequest;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CForgotPasswordResponse;

import java.util.Map;

import retrofit2.Callback;

public class B2CWSForgotPassword
        extends B2CApiProviderClient<B2CForgotPasswordResponse>{
    private B2CForgotPasswordRequest request;
    public B2CWSForgotPassword(B2CICallback callback){
        this.b2CICallback = callback;
    }
    public void setRequest(B2CForgotPasswordRequest b2CForgotPasswordRequest){
        request = b2CForgotPasswordRequest;
    }
    @Override
    public void run(Callback<B2CForgotPasswordResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }
    @Override
    protected boolean isUseAuthenticateInApi() {
        return false;
    }

    @Override
    protected void runApi() {
        serviceInterface.forgotPassword(request).enqueue(this);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        return null;
    }

    @Override
    protected void postResponse(B2CForgotPasswordResponse response) {
    }
}
