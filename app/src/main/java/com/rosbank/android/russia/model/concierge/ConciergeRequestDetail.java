package com.rosbank.android.russia.model.concierge;

import com.rosbank.android.russia.utils.StringUtil;
import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by ThuNguyen on 11/16/2016.
 */

public class ConciergeRequestDetail implements Serializable{
    @Expose
    private String TypeofCuisine;
    @Expose
    private String AmbianceOccasion;

    public String getBudgetRangeCurrencyperperson() {
        return BudgetRangeCurrencyperperson;
    }

    public void setBudgetRangeCurrencyperperson(final String budgetRangeCurrencyperperson) {
        BudgetRangeCurrencyperperson = budgetRangeCurrencyperperson;
    }

    public String getTypeofCuisine() {
        return TypeofCuisine;
    }

    public void setTypeofCuisine(final String typeofCuisine) {
        TypeofCuisine = typeofCuisine;
    }

    public String getAmbianceOccasion() {
        return AmbianceOccasion;
    }

    public void setAmbianceOccasion(final String ambianceOccasion) {
        AmbianceOccasion = ambianceOccasion;
    }

    @Expose
    private String BudgetRangeCurrencyperperson;
    @Expose
    private String NameofRestaurant;
    @Expose
    private String SpecialRequirement;
    @Expose
    private String Cuisine;
    @Expose
    private String MaxPrice;
    @Expose
    private String MinPrice;
    @Expose
    private String Situation;
    @Expose
    private String HotelName;
    @Expose
    private String RoomPref;
    @Expose
    private String Membership;
    @Expose
    private String MemberNo;
    @Expose
    private String PrefRating;
    @Expose
    private String Nameofdriver;
    @Expose
    private String Ageofdriver;
    @Expose
    private String IntLicense;
    @Expose
    private String PickupLocation;
    @Expose
    private String DropoffDate;
    @Expose
    private String DropoffLocation;
    @Expose
    private String PreferredVehicle;
    @Expose
    private String Preferredcarrentalcompany;
    @Expose
    private String PayType;
    @Expose
    private String DropPoint;
    @Expose
    private String ExtraStops;
    @Expose
    private String TransportType;
    @Expose
    private String EventName;
    @Expose
    private String State;
    @Expose
    private String EventCategory;
    @Expose
    private String PrefDate;
    @Expose
    private String PrefSeating;
    @Expose
    private String NoTicket;
    @Expose
    private String GolfCourseName;
    @Expose
    private String GolfHandicap;
    @Expose
    private String PrefRegion;
    @Expose
    private String SpecificStartTime;
    @Expose
    private String NoofBallers;
    @Expose
    private String Holes;
    @Expose
    private String NeedCaddy;
    @Expose
    private String Needbuggy;
    @Expose
    private String MemberofGolfClub;
    @Expose
    private String GuestName;
    @Expose
    private String HotelLoyaltyMembershipNumber;
    @Expose
    private String RoomPreference;

    @Expose
    private String SmokingPreference;
    @Expose
    private String BudgetRangepernightCurrency;
    @Expose
    private String Doesthedriveholdavalidinternationaldrivinglicence;
    @Expose
    private String City;
    @Expose
    private String GolfCourse;
@Expose
    private String FoodAllergies;
    @Expose
    private String PreferredGolfHandicap;
    @Expose
    private String AdditionalPreference;

    public String getDateofplay() {
        return Dateofplay;
    }

    public void setDateofplay(final String dateofplay) {
        Dateofplay = dateofplay;
    }

    public String getTeetime() {
        return Teetime;
    }

    public void setTeetime(final String teetime) {
        Teetime = teetime;
    }

    @Expose
    private String Dateofplay;
    @Expose
    private String Teetime;

    public String getCity() {
        return City;
    }

    public void setCity(final String city) {
        City = city;
    }

    public String getGolfCourse() {
        return GolfCourse;
    }

    public void setGolfCourse(final String golfCourse) {
        GolfCourse = golfCourse;
    }

    public String getNoballers() {
        return Noballers;
    }

    public void setNoballers(final String noballers) {
        Noballers = noballers;
    }

    public String getHolepreference() {
        return Holepreference;
    }

    public void setHolepreference(final String holepreference) {
        Holepreference = holepreference;
    }

    @Expose
    private String Noballers ;
    @Expose
    private String Holepreference;


    public String getNoofPassengers() {
        return NoofPassengers;
    }

    public void setNoofPassengers(final String noofPassengers) {
        NoofPassengers = noofPassengers;
    }

    @Expose
    private String NoofPassengers;

    public String getBudgetRangeCurrencyperday() {
        return BudgetRangeCurrencyperday;
    }

    public void setBudgetRangeCurrencyperday(final String budgetRangeCurrencyperday) {
        BudgetRangeCurrencyperday = budgetRangeCurrencyperday;
    }

    public String getDoesthedriveholdavalidinternationaldrivinglicence() {
        return Doesthedriveholdavalidinternationaldrivinglicence;
    }

    public void setDoesthedriveholdavalidinternationaldrivinglicence(final String doesthedriveholdavalidinternationaldrivinglicence) {
        Doesthedriveholdavalidinternationaldrivinglicence =
                doesthedriveholdavalidinternationaldrivinglicence;
    }

    @Expose
    private String BudgetRangeCurrencyperday;


    public String getNooftickets() {
        return Nooftickets;
    }

    public void setNooftickets(final String nooftickets) {
        Nooftickets = nooftickets;
    }

    @Expose
    private String Nooftickets;
    public String getBudgetRangepernightCurrency() {
        return BudgetRangepernightCurrency;
    }

    public void setBudgetRangepernightCurrency(final String budgetRangepernightCurrency) {
        BudgetRangepernightCurrency = budgetRangepernightCurrency;
    }


    public String getNameofRestaurant() {
        return NameofRestaurant;
    }

    public void setNameofRestaurant(final String nameofRestaurant) {
        NameofRestaurant = nameofRestaurant;
    }

    public void setSituation(final String situation) {
        Situation = situation;
    }

    public String getGuestName() {
        return GuestName;
    }

    public void setGuestName(final String guestName) {
        GuestName = guestName;
    }

    public String getHotelLoyaltyMembershipNumber() {
        return HotelLoyaltyMembershipNumber;
    }

    public void setHotelLoyaltyMembershipNumber(final String hotelLoyaltyMembershipNumber) {
        HotelLoyaltyMembershipNumber = hotelLoyaltyMembershipNumber;
    }

    public String getRoomPreference() {
        return RoomPreference;
    }

    public void setRoomPreference(final String roomPreference) {
        RoomPreference = roomPreference;
    }


    public String getReservationName() {
        return ReservationName;
    }

    public void setReservationName(final String reservationName) {
        ReservationName = reservationName;
    }

    @Expose
    private String ReservationName;

    public String getCountry() {
        return Country;
    }

    public void setCountry(final String country) {
        Country = country;
    }

    public String getIfpreferredrestaurantisnotavailableonselecteddatetime() {
        return Ifpreferredrestaurantisnotavailableonselecteddatetime;
    }

    public void setIfpreferredrestaurantisnotavailableonselecteddatetime(final String ifpreferredrestaurantisnotavailableonselecteddatetime) {
        Ifpreferredrestaurantisnotavailableonselecteddatetime =
                ifpreferredrestaurantisnotavailableonselecteddatetime;
    }

    @Expose
    private String Country;
    @Expose
    private String Ifpreferredrestaurantisnotavailableonselecteddatetime;

    public String getRestaurantName() {
        return NameofRestaurant;
    }

    public void setRestaurantName(String restaurantName) {
        NameofRestaurant = restaurantName;
    }

    public String getSpecialRequirement() {
        return SpecialRequirement;
    }

    public void setSpecialRequirement(String specialRequirement) {
        SpecialRequirement = specialRequirement;
    }

    public String getCuisine() {
        return Cuisine;
    }

    public void setCuisine(String cuisine) {
        Cuisine = cuisine;
    }

    public String getMaxPrice() {
        return MaxPrice;
    }

    public void setMaxPrice(String maxPrice) {
        MaxPrice = maxPrice;
    }

    public String getMinPrice() {
        return MinPrice;
    }

    public void setMinPrice(String minPrice) {
        MinPrice = minPrice;
    }

    public String getHotelName() {
        return HotelName;
    }

    public void setHotelName(String hotelName) {
        HotelName = hotelName;
    }

    public String getRoomPref() {
        return RoomPref;
    }

    public void setRoomPref(String roomPref) {
        RoomPref = roomPref;
    }

    public String getSmokingPref() {
        return SmokingPreference;
    }

    public void setSmokingPref(String smokingPref) {
        SmokingPreference = smokingPref;
    }

    public String getMembership() {
        return Membership;
    }

    public void setMembership(String membership) {
        Membership = membership;
    }

    public String getMemberNo() {
        return MemberNo;
    }

    public void setMemberNo(String memberNo) {
        MemberNo = memberNo;
    }

    public String getPrefRating() {
        return PrefRating;
    }

    public void setPrefRating(String prefRating) {
        PrefRating = prefRating;
    }

    public String getNameofdriver() {
        return Nameofdriver;
    }

    public void setNameofdriver(String nameofdriver) {
        Nameofdriver = nameofdriver;
    }

    public String getAgeofdriver() {
        return Ageofdriver;
    }

    public void setAgeofdriver(String ageofdriver) {
        Ageofdriver = ageofdriver;
    }

    public String getIntLicense() {
        return IntLicense;
    }

    public void setIntLicense(String intLicense) {
        IntLicense = intLicense;
    }

    public String getPickupLocation() {
        return PickupLocation;
    }

    public void setPickupLocation(String pickupLocation) {
        PickupLocation = pickupLocation;
    }

    public String getDropoffDate() {
        return DropoffDate;
    }

    public void setDropoffDate(String dropoffDate) {
        DropoffDate = dropoffDate;
    }

    public String getDropoffLocation() {
        return DropoffLocation;
    }

    public void setDropoffLocation(String dropoffLocation) {
        DropoffLocation = dropoffLocation;
    }

    public String getSituation() {
        return Situation;
    }

    public String getPreferredVehicle() {
        return PreferredVehicle;
    }

    public void setPreferredVehicle(String preferredVehicle) {
        PreferredVehicle = preferredVehicle;
    }

    public String getPayType() {
        return PayType;
    }

    public void setPayType(String payType) {
        PayType = payType;
    }

    public String getDropPoint() {
        return DropPoint;
    }

    public void setDropPoint(String dropPoint) {
        DropPoint = dropPoint;
    }

    public String getExtraStops() {
        return ExtraStops;
    }

    public void setExtraStops(String extraStops) {
        ExtraStops = extraStops;
    }

    public String getTransportType() {
        return TransportType;
    }

    public void setTransportType(String transportType) {
        TransportType = transportType;
    }

    public String getEventName() {
        return EventName;
    }

    public void setEventName(String eventName) {
        EventName = eventName;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getEventCategory() {
        return EventCategory;
    }

    public void setEventCategory(String eventCategory) {
        EventCategory = eventCategory;
    }

    public String getPrefDate() {
        return PrefDate;
    }

    public void setPrefDate(String prefDate) {
        PrefDate = prefDate;
    }

    public String getPrefSeating() {
        return PrefSeating;
    }

    public void setPrefSeating(String prefSeating) {
        PrefSeating = prefSeating;
    }

    public String getNoTicket() {
        return NoTicket;
    }

    public void setNoTicket(String noTicket) {
        NoTicket = noTicket;
    }

    public String getGolfCourseName() {
        if(GolfCourseName!=null && !StringUtil.isEmpty(GolfCourseName)) {
            return GolfCourseName;
        }else{
            return GolfCourse;
        }
    }

    public void setGolfCourseName(String golfCourseName) {
        GolfCourseName = golfCourseName;
    }

    public String getGolfHandicap() {
        return GolfHandicap;
    }

    public void setGolfHandicap(String golfHandicap) {
        GolfHandicap = golfHandicap;
    }

    public String getPrefRegion() {
        return PrefRegion;
    }

    public void setPrefRegion(String prefRegion) {
        PrefRegion = prefRegion;
    }

    public String getSpecificStartTime() {
        return SpecificStartTime;
    }

    public void setSpecificStartTime(String specificStartTime) {
        SpecificStartTime = specificStartTime;
    }

    public String getNoofBallers() {
        return NoofBallers;
    }

    public void setNoofBallers(String noofBallers) {
        NoofBallers = noofBallers;
    }

    public String getHoles() {
        return Holes;
    }

    public void setHoles(String holes) {
        Holes = holes;
    }

    public String getNeedCaddy() {
        return NeedCaddy;
    }

    public void setNeedCaddy(String needCaddy) {
        NeedCaddy = needCaddy;
    }

    public String getNeedbuggy() {
        return Needbuggy;
    }

    public void setNeedbuggy(String needbuggy) {
        Needbuggy = needbuggy;
    }

    public String getMemberofGolfClub() {
        return MemberofGolfClub;
    }

    public void setMemberofGolfClub(String memberofGolfClub) {
        MemberofGolfClub = memberofGolfClub;
    }

    public String getPreferredcarrentalcompany() {
        return Preferredcarrentalcompany;
    }

    public void setPreferredcarrentalcompany(String preferredcarrentalcompany) {
        Preferredcarrentalcompany = preferredcarrentalcompany;
    }
    public String getFoodAllergies() {
        return FoodAllergies;
    }

    public void setFoodAllergies(String foodAllergies) {
        FoodAllergies = foodAllergies;
    }

    public String getPreferredGolfHandicap() {
        return PreferredGolfHandicap;
    }

    public void setPreferredGolfHandicap(String preferredGolfHandicap) {
        PreferredGolfHandicap = preferredGolfHandicap;
    }

    public String getAdditionalPreference() {
        return AdditionalPreference;
    }

    public void setAdditionalPreference(String additionalPreference) {
        AdditionalPreference = additionalPreference;
    }

    public String getWithBreakfast() {
        return WithBreakfast;
    }

    public void setWithBreakfast(final String withBreakfast) {
        WithBreakfast = withBreakfast;
    }

    public String getWithWifi() {
        return WithWifi;
    }

    public void setWithWifi(final String withWifi) {
        WithWifi = withWifi;
    }

    @Expose
    private String WithBreakfast;
    @Expose
    private String WithWifi;

    public String getFullAddress() {
        return FullAddress;
    }

    public void setFullAddress(final String fullAddress) {
        FullAddress = fullAddress;
    }

    public String getPrivilegeId() {
        return PrivilegeId;
    }

    public void setPrivilegeId(final String privilegeId) {
        PrivilegeId = privilegeId;
    }

    @Expose
    private String FullAddress;
    @Expose
    private String PrivilegeId;

    public String getSpaName() {
        return SpaName;
    }

    public void setSpaName(final String spaName) {
        SpaName = spaName;
    }

    @Expose
    private String SpaName;

    public String getNumbersOfRoom() {
        return NumbersOfRoom;
    }

    public void setNumbersOfRoom(final String numbersOfRoom) {
        NumbersOfRoom = numbersOfRoom;
    }

    @Expose
    private String NumbersOfRoom;
}
