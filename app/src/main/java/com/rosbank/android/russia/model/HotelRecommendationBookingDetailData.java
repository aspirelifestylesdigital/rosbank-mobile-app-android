package com.rosbank.android.russia.model;

import android.text.TextUtils;

import com.google.gson.annotations.Expose;

/**
 * Created by ThuNguyen on 10/26/2016.
 */

public class HotelRecommendationBookingDetailData {
    @Expose
    private String CheckInDate;
    @Expose
    private String EpochCheckInDate;
    @Expose
    private String CheckOutDate;
    @Expose
    private String EpochCheckOutDate;
    @Expose
    private String MinimumPrice;
    @Expose
    private String MaximumPrice;
    @Expose
    private CommonObject StarRating;
    @Expose
    private CommonObject RoomType;
    @Expose
    private String Country;
    @Expose
    private CommonObject Country4App;
    @Expose
    private String City;
    @Expose
    private String NumberOfAdults;
    @Expose
    private String NumberOfKids;
    @Expose
    private Boolean SmokingPreference;
    @Expose
    private String SpecialRequirements;
    @Expose
    private String AttachPhoto;
    @Expose
    private String AttachPhotoUrl;
    @Expose
    private String BookingId;
    @Expose
    private String UserID;
    @Expose
    private String BookingName;
    @Expose
    private String BookingItemId;
    @Expose
    private String GuestName;
    @Expose
    private String UploadPhoto;
    @Expose
    private String UploadPhotoUrl;
    @Expose
    private Boolean Phone;
    @Expose
    private Boolean Email;
    @Expose
    private Boolean Both;
    @Expose
    private String MobileNumber;
    @Expose
    private String EmailAddress;

    public String getCheckInDate() {
        return CheckInDate;
    }

    public void setCheckInDate(String checkInDate) {
        CheckInDate = checkInDate;
    }

    public String getEpochCheckInDate() {
        return EpochCheckInDate;
    }

    public void setEpochCheckInDate(String epochCheckInDate) {
        EpochCheckInDate = epochCheckInDate;
    }

    public String getCheckOutDate() {
        return CheckOutDate;
    }

    public void setCheckOutDate(String checkOutDate) {
        CheckOutDate = checkOutDate;
    }

    public String getEpochCheckOutDate() {
        return EpochCheckOutDate;
    }

    public void setEpochCheckOutDate(String epochCheckOutDate) {
        EpochCheckOutDate = epochCheckOutDate;
    }

    public String getMinimumPrice() {
        return MinimumPrice;
    }

    public void setMinimumPrice(String minimumPrice) {
        MinimumPrice = minimumPrice;
    }

    public String getMaximumPrice() {
        return MaximumPrice;
    }

    public void setMaximumPrice(String maximumPrice) {
        MaximumPrice = maximumPrice;
    }

    public CommonObject getStarRating() {
        return StarRating;
    }

    public void setStarRating(CommonObject starRating) {
        StarRating = starRating;
    }

    public CommonObject getRoomType() {
        return RoomType;
    }

    public void setRoomType(CommonObject roomType) {
        RoomType = roomType;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public CommonObject getCountry4App() {
        return Country4App;
    }

    public void setCountry4App(CommonObject country4App) {
        Country4App = country4App;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getNumberOfAdults() {
        return NumberOfAdults;
    }

    public void setNumberOfAdults(String numberOfAdults) {
        NumberOfAdults = numberOfAdults;
    }

    public String getNumberOfKids() {
        return NumberOfKids;
    }

    public void setNumberOfKids(String numberOfKids) {
        NumberOfKids = numberOfKids;
    }

    public Boolean getSmokingPreference() {
        return SmokingPreference;
    }

    public void setSmokingPreference(Boolean smokingPreference) {
        SmokingPreference = smokingPreference;
    }

    public String getSpecialRequirements() {
        return SpecialRequirements;
    }

    public void setSpecialRequirements(String specialRequirements) {
        SpecialRequirements = specialRequirements;
    }

    public String getAttachPhoto() {
        return AttachPhoto;
    }

    public void setAttachPhoto(String attachPhoto) {
        AttachPhoto = attachPhoto;
    }

    public String getAttachPhotoUrl() {
        return AttachPhotoUrl;
    }

    public void setAttachPhotoUrl(String attachPhotoUrl) {
        AttachPhotoUrl = attachPhotoUrl;
    }

    public String getBookingId() {
        return BookingId;
    }

    public void setBookingId(String bookingId) {
        BookingId = bookingId;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public String getBookingName() {
        return BookingName;
    }

    public void setBookingName(String bookingName) {
        BookingName = bookingName;
    }

    public String getBookingItemId() {
        return BookingItemId;
    }

    public void setBookingItemId(String bookingItemId) {
        BookingItemId = bookingItemId;
    }

    public String getGuestName() {
        return GuestName;
    }

    public void setGuestName(String guestName) {
        GuestName = guestName;
    }

    public String getUploadPhoto() {
        return UploadPhoto;
    }

    public void setUploadPhoto(String uploadPhoto) {
        UploadPhoto = uploadPhoto;
    }

    public String getUploadPhotoUrl() {
        return UploadPhotoUrl;
    }

    public void setUploadPhotoUrl(String uploadPhotoUrl) {
        UploadPhotoUrl = uploadPhotoUrl;
    }

    public Boolean getPhone() {
        return Phone;
    }

    public void setPhone(Boolean phone) {
        Phone = phone;
    }

    public Boolean getEmail() {
        return Email;
    }

    public void setEmail(Boolean email) {
        Email = email;
    }

    public Boolean getBoth() {
        return Both;
    }

    public void setBoth(Boolean both) {
        Both = both;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public String getEmailAddress() {
        return EmailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        EmailAddress = emailAddress;
    }
    public String getGuestDisplay(){
        String guests = "";
        Integer adult = Integer.parseInt(NumberOfAdults);
        if(adult >= 2){
            guests += adult + " Adults";
        }else{
            guests += adult + " Adult";
        }
        Integer kid = Integer.parseInt(NumberOfKids);
        if(kid > 0){
            if(kid == 1){
                guests += " & 1 Kid";
            }else{
                guests += " & " + guests + " Kids";
            }
        }
        return guests;
    }
    public long getEpocCheckInDateInMillis(){
        if(TextUtils.isEmpty(EpochCheckInDate) || EpochCheckInDate.equalsIgnoreCase("null")){
            return 0;
        }
        return (Long.parseLong(EpochCheckInDate) * 1000);
    }
    public long getEpocCheckOutDateInMillis(){
        if(TextUtils.isEmpty(EpochCheckOutDate) || EpochCheckOutDate.equalsIgnoreCase("null")){
            return 0;
        }
        return (Long.parseLong(EpochCheckOutDate) * 1000);
    }
}
