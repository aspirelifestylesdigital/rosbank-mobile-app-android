package com.rosbank.android.russia.apiservices.ResponseModel;

import com.rosbank.android.russia.apiservices.apis.appimplement.BaseResponse;
import com.rosbank.android.russia.model.Restaurant;
import com.google.gson.annotations.Expose;


public class GetExperienceGourmetDetailResponse extends BaseResponse {

    @Expose
    private Restaurant Data;

    public Restaurant getData() {
        return Data;
    }

    public void setData(Restaurant data) {
        Data = data;
    }
}
