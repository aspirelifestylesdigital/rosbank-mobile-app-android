package com.rosbank.android.russia.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger {

    public static enum LEVEL {
        all, none, debug, warn, info, error
    }

    private static String LOGNAME = "Logger";
    //
    private static LEVEL level = LEVEL.all;
    private static String filename = null;
    private static boolean writeToSD = false;

    public static void setLevel(LEVEL _level) {
        level = _level;
    }

    public static void sout(String msg) {
        if (level == LEVEL.all || level == LEVEL.debug) {
            System.out.println(msg);
            writeToSDLog("[System.Out] - "+msg);
        }
    }

    public static void d(String tag, String msg) {
        if (level == LEVEL.all || level == LEVEL.debug) {
            android.util.Log.d(tag, msg);
            writeToSDLog("[Debug] - "+msg);
        }
    }

    public static void i(String tag, String msg) {
        if (level == LEVEL.all || level == LEVEL.debug || level == LEVEL.warn) {
            android.util.Log.i(tag, msg);
            writeToSDLog("[Info] - "+msg);
        }
    }

    public static void w(String tag, String msg) {
        if (level == LEVEL.all || level == LEVEL.debug || level == LEVEL.info
                || level == LEVEL.warn) {
            android.util.Log.w(tag, msg);
            writeToSDLog("[Warn] - "+msg);
        }
    }

    public static void e(String tag, String msg) {
        if (level == LEVEL.all || level == LEVEL.debug) {
            android.util.Log.e(tag, msg);
            writeToSDLog("[Error] - "+msg);
        }
    }

    /**
     * CONFIGURATION FOR WRITE LOG FILE
     * =============================================
     * */

    /**
     * Set file name for Log writer
     * */
    public static void setFileStorage(String _filename) {
        writeToSD = true;
        if(_filename==null || _filename.trim().length()==0)
            filename = null;
        else
            filename = _filename;
    }

    public static void visibleFileStorage() {
        writeToSD = true;
    }

    public static void disableFileStorage() {
        writeToSD = false;
    }

    private static void writeToSDLog(String message) {

        if (!writeToSD) {
            return;
        }

        try {

            filename = (filename == null) ? "application.log" : filename;

            File logfile = new File("/sdcard/" + filename);
            if (!logfile.exists())
                logfile.createNewFile();

            FileWriter logwriter = new FileWriter(logfile, true);
            PrintWriter out = new PrintWriter(logwriter);

            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            out.println("[" + df.format(new Date()) + "]" + " - " + message);
            out.close();

        } catch (IOException e) {
//            android.util.Log.e(LOGNAME, "Unable to write debug file "
//                    + e.getMessage() + ". Logging has been disabled");
            writeToSD = false;
        }

    }

//    public static String readLog() {
//        StringBuffer sb = new StringBuffer();
//
//        try {
//            filename = (filename == null) ? "application.log" : filename;
//            File logFile = new File("/sdcard/" + filename);
//            if (!logFile.exists())
//                return "LogFile does not exist.";
//
//            FileReader f = new FileReader(logFile);
//            BufferedReader in = new BufferedReader(f);
//
//            Boolean end = false;
//
//            while (!end) {
//                String s = in.readLine();
//                if (s == null){
//                    end = true;
//                }else{
//                    sb.append(s);
//                    sb.append("\n");
//                }
//
//            }
//            in.close();
//        } catch (Exception ex) {
//            Logger.e(LOGNAME, "Could not read file " + ex.getMessage());
//        }
//
//        return sb.toString();
//    }

//    public static void eraseLogFile(){
//        filename = (filename == null) ? "application.log" : filename;
//        File logFile = new File("/sdcard/" + filename);
//        if (logFile.exists())
//            logFile.delete();
//    }
//
//    public static String getStackTrace(Throwable t) {
//        StringWriter sw = new StringWriter();
//        PrintWriter pw = new PrintWriter(sw, true);
//        t.printStackTrace(pw);
//        pw.flush();
//        sw.flush();
//        return sw.toString();
//    }

}
