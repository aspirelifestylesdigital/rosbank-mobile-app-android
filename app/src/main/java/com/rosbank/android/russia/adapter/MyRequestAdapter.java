package com.rosbank.android.russia.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.model.concierge.MyRequestObject;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.FontUtils;
import com.rosbank.android.russia.views.LoadingViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/*
 * Created by anh.trinh on 10/24/2016.
 */
public class MyRequestAdapter
        extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public interface MyRequestListener {
        void onAmendClick(MyRequestObject object);

        void onCancelClick(MyRequestObject object);

        void onCalendarClick(MyRequestObject object);
    }

    private final int ITEM_VIEW_TYPE_BASIC = 0;
    private final int ITEM_VIEW_TYPE_LOADING = 1;


    private List<MyRequestObject> mData;
    private List<MyRequestObject> mDisplayItems = new ArrayList<>();
    private final MyRequestListener listener;
    private Context mContext;

    public MyRequestAdapter(List<MyRequestObject> listDTO,
                            MyRequestListener listener,
                            Context context) {
        this.mData = listDTO;
        this.mDisplayItems = new ArrayList<>();
        this.listener = listener;
        this.mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        if (viewType == ITEM_VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext())
                                      .inflate(R.layout.layout_footer_loadmore,
                                               parent,
                                               false);
            return new LoadingViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext())
                                      .inflate(R.layout.item_my_request,
                                               parent,
                                               false);
            return new ViewHolder(view,
                                  this.mContext);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder,
                                 int position) {
        if (getItemViewType(position) == ITEM_VIEW_TYPE_BASIC) {
            MyRequestObject item = mDisplayItems.get(position);
            ((ViewHolder) holder).bind(position,
                                       item,
                                       listener);
        } else {
            //view loading
        }

    }

    @Override
    public int getItemCount() {
        return (mDisplayItems == null) ?
               0 :
               mDisplayItems.size();
    }

    public List<MyRequestObject> getData() {
        return mDisplayItems;
    }

    public List<MyRequestObject> getDisPlayData() {
        return mDisplayItems;
    }

    //========== Config load more ===============

    /**
     * check is correct have view loading (show Process)
     *
     * @return true: show | false: hide
     */
    private boolean isShowLoading() {
        return (mDisplayItems.size() > 1 && mDisplayItems.contains(null));
    }

    /**
     * Visible view loading process pause load more
     */
    public void addViewLoading() {
        if (!isShowLoading()) {
            mDisplayItems.add(null);
            notifyItemInserted(getItemCount() - 1);
        }
    }

    /**
     * Hide view loading
     */
    public void removeViewLoading() {
        if (isShowLoading()) {
            int itemLoad = mDisplayItems.indexOf(null);
            if (itemLoad != -1) {
                mDisplayItems.remove(itemLoad);
                notifyItemRemoved(itemLoad);
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        return mDisplayItems.get(position) != null ?
               ITEM_VIEW_TYPE_BASIC :
               ITEM_VIEW_TYPE_LOADING;
    }

    public int getRealSize() {
        if (mDisplayItems == null || mDisplayItems.size() <= 1) {
            return 0;
        }
        int n = mDisplayItems.size();
        if (isShowLoading()) {
            n -= 1;//loading item
        }
        if (n < 0) {
            n = 0;
        }
        return n;
    }
    //========== End Config load more ===============

    //========== Location User ===============


    public List<MyRequestObject> getDisplayItems() {
        return mDisplayItems;
    }

    public void filter(AppConstant.BOOKING_FILTER_TYPE query, final String filterGroup) {
        mDisplayItems.clear();

        // Return empty list when filter string is empty.
        mDisplayItems.addAll(MyRequestObject.filterItems(mData,
                                                             query,filterGroup));
        notifyDataSetChanged();
    }


    class ViewHolder
            extends RecyclerView.ViewHolder {

        @BindView(R.id.ic_request_type)
        ImageView mIcRequestType;
        @BindView(R.id.ic_calendar)
        ImageView mIcCalendar;
        @BindView(R.id.tv_request_title)
        TextView mTvRequestTitle;
        @BindView(R.id.tv_request_description)
        TextView mTvRequestDescription;
        @BindView(R.id.llRequestStatus)
        protected View llRequestStatus;
        @BindView(R.id.tv_request_status)
        TextView mTvRequestStatus;
        @BindView(R.id.tv_request_city_country)
        TextView tvRequestCountryCity;
        @BindView(R.id.tv_request_amend)
        TextView mTvRequestAmend;
        @BindView(R.id.tv_request_cancel)
        TextView mTvRequestCancel;
        @BindView(R.id.layout_contain)
        RelativeLayout mLayoutContain;
        @BindView(R.id.layout_button)
        RelativeLayout mLayoutButton;
        int pos;
        MyRequestListener listener;
        MyRequestObject object;
        Context mContext;

        @OnClick({R.id.ic_calendar,
                  R.id.tv_request_amend,
                  R.id.tv_request_cancel})
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.ic_calendar:
                    if (listener != null) {
                        listener.onCalendarClick(object);
                    }
                    break;
                case R.id.tv_request_amend:
                    if (listener != null) {
                        listener.onAmendClick(object);
                    }
                    break;
                case R.id.tv_request_cancel:
                    if (listener != null) {
                        listener.onCancelClick(object);
                    }
                    break;
            }
        }

        ViewHolder(View itemView,
                   Context context) {
            super(itemView);
            ButterKnife.bind(this,
                             itemView);
            CommonUtils.setFontForViewRecursive(mLayoutContain,
                                                FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
            CommonUtils.setFontForTextView(mTvRequestTitle,
                    FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
            mContext = context;
        }

        protected void bind(int position,
                            final MyRequestObject dto,
                            final MyRequestListener listener) {
            this.pos = position;
            //parent view set event click
            itemView.setTag(dto);
            this.listener = listener;
            this.object = dto;
            switch (this.object.getBookingFilterGroupType()) {
                case Restaurant:
                    mIcRequestType.setImageResource(R.drawable.dining_circle);
                   // mTvRequestDate.setText(getDateTimeFomat(object));
                    break;
                case Hotel:
                    mIcRequestType.setImageResource(R.drawable.hotel_circle);
                    //mTvRequestDate.setText(getDateFomat(object));
                    break;
                case Entertainment:
                    mIcRequestType.setImageResource(R.drawable.events_circle);
                    //mTvRequestDate.setText(getDateTimeFomat(object));
                    break;
                case Golf:
                    mIcRequestType.setImageResource(R.drawable.golf_circle);
                    //mTvRequestDate.setText(getDateTimeFomat(object));
                    break;
                case Car:
                    mIcRequestType.setImageResource(R.drawable.car_circle);
                    //mTvRequestDate.setText(getDateTimeFomat(object));
                    break;
                case Spa:
                    mIcRequestType.setImageResource(R.drawable.spa_request_sel);
                    break;
                case Other:
                    mIcRequestType.setImageResource(R.drawable.other_request_sel);
                    //mTvRequestDate.setText(getDateTimeFomat(object));
                    break;
            }

            if (CommonUtils.isStringValid(object.getItemTitle())) {
                mTvRequestTitle.setText(object.getItemTitle());
                mTvRequestTitle.setVisibility(View.VISIBLE);
            } else {
                mTvRequestTitle.setVisibility(View.GONE);
            }
            if (CommonUtils.isStringValid(object.getRequestStatus())) {
                mTvRequestStatus.setText(object.getRequestStatus());
                llRequestStatus.setVisibility(View.VISIBLE);
            } else {
                llRequestStatus.setVisibility(View.GONE);
            }
            if (CommonUtils.isStringValid(object.getCompoundCountryAndCity())) {
                tvRequestCountryCity.setText(object.getCompoundCountryAndCity());
                tvRequestCountryCity.setVisibility(View.VISIBLE);
            } else {
                tvRequestCountryCity.setVisibility(View.GONE);
            }
            if (CommonUtils.isStringValid(object.getItemDescription())) {
                mTvRequestDescription.setText(object.getItemDescription());
                mTvRequestDescription.setVisibility(View.VISIBLE);
            } else {
                mTvRequestDescription.setVisibility(View.GONE);
            }

            if(object.isUpcoming()) {
                if(object.getBookingFilterGroupType() == AppConstant.BOOKING_FILTER_TYPE.Other) {
                    mIcCalendar.setVisibility(View.GONE);
                }else{
                    mIcCalendar.setVisibility(View.VISIBLE);
                }
                /*if(MyRequestsHeaderView.PRE_REQUEST_TYPE_OTHER.equals(object.getTypeGroup())){
                    mIcCalendar.setVisibility(View.GONE);
                }*/
            }else{
                mIcCalendar.setVisibility(View.GONE);
            }
            if(object.isAmendCancelPermitted()){
                mLayoutButton.setVisibility(View.VISIBLE);
            }else{
                mLayoutButton.setVisibility(View.GONE);
            }
        }
    }
}
