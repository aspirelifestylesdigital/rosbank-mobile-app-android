package com.rosbank.android.russia.apiservices;

import com.rosbank.android.russia.apiservices.RequestModel.SignUpRequest;
import com.rosbank.android.russia.apiservices.ResponseModel.SignUpResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.ApiProviderClient;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Callback;


public class WSUserSignUp
        extends ApiProviderClient<SignUpResponse> {
   /* private String mSalutation;
    private String mFirstName;
    private String mLastName;
    private String mPhone;
    private String mEmail;
    private String mPassword;
    private String mConfirmPassword;
    private Boolean mAcceptTermAndPolicy;
    private String mCreditCardNumber;*/
    private SignUpRequest signUpRequest;

    public WSUserSignUp(){

    }
    public void signUp(final SignUpRequest request){
        this.signUpRequest = request;
      /*  mSalutation =request.getSalutation();
        mFirstName = request.getFIRSTNAME();
        mLastName = request.getLASTNAME();
        mPhone = request.getPhone();
        mEmail = request.getEmail();
        mCreditCardNumber = request.getCreditCardNumber();
        mPassword = request.getPassword();
        mConfirmPassword = request.getConfirmPassword();
        mAcceptTermAndPolicy = request.getAcceptTermAndPolicy();*/

    }


    public void signUp(String salutation, String firstName, String lastName, String phone, String email, String creditCardNumber, String password, String confirmPassword, Boolean acceptTermAndPolicy){
      /*  mSalutation =salutation;
        mFirstName = firstName;
        mLastName = lastName;
        mPhone = phone;
         mEmail = email;
        mCreditCardNumber = creditCardNumber;
         mPassword = password;
         mConfirmPassword = confirmPassword;
         mAcceptTermAndPolicy = acceptTermAndPolicy;*/
    }

    @Override
    public void run(Callback<SignUpResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }

    @Override
    protected boolean isUseAuthenticateInApi() {
        return true;
    }

    @Override
    public void runApi() {
       /* SignUpRequest request = new SignUpRequest();
        request.setSalutation(mSalutation);
        request.setFIRSTNAME(mFirstName);
        request.setLASTNAME(mLastName);
        request.setPhone(mPhone);
        request.setEmail(mEmail);
        request.setCreditCardNumber(mCreditCardNumber);
        request.setPassword(mPassword);
        request.setConfirmPassword(mConfirmPassword);
        request.setAcceptTermAndPolicy(mAcceptTermAndPolicy);
        serviceInterface.userSignUp(request).enqueue(callback);*/
        serviceInterface.userSignUp(signUpRequest).enqueue(callback);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("User-Agent", "Retrofit-Sample-App");
        return headers;
    }

}
