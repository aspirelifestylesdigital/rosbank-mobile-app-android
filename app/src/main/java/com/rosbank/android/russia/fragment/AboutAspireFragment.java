package com.rosbank.android.russia.fragment;

import android.graphics.Typeface;
import android.widget.ImageView;
import android.widget.TextView;

import com.rosbank.android.russia.BuildConfig;
import com.rosbank.android.russia.R;
import com.rosbank.android.russia.activitys.HomeActivity;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.model.UserItem;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.FontUtils;

import butterknife.BindView;

/*
 * Created by chau.nguyen on 10/05/2016.
 */
public class AboutAspireFragment extends BaseFragment {

    @BindView(R.id.tv_aspire_welcome)
    TextView tvAspireWelcome;

    @BindView(R.id.tvAspireTitle)
    TextView tvAspireTitle;

    @BindView(R.id.tvAspireLine1)
    TextView tvAspireLine1;

    @BindView(R.id.tvAspireLine2)
    TextView tvAspireLine2;

    @BindView(R.id.tvAspireVersion)
    TextView tvAspireVersion;

    @BindView(R.id.tvAspireVersionContent)
    TextView tvAspireVersionNumber;

    @BindView(R.id.img_aspire_logo)
    ImageView ivAspireLogo;

    public AboutAspireFragment() {

    }

    @Override
    protected int layoutId() {
        return R.layout.fragment_about_aspire;
    }

    @Override
    protected void initView() {

        if (getActivity() != null) {
            if (UserItem.isVip()) {
                ivAspireLogo.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.hermitage_logo_2));
            } else {
                ivAspireLogo.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.rosbank_logo));
            }
        }

        CommonUtils.setFontForViewRecursive(tvAspireWelcome,
                FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        CommonUtils.setFontForViewRecursive(tvAspireTitle,
                FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        CommonUtils.setFontForViewRecursive(tvAspireLine1,
                FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
        CommonUtils.setFontForViewRecursive(tvAspireLine2,
                FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
        CommonUtils.setFontForViewRecursive(tvAspireVersion,
                FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        CommonUtils.setFontForViewRecursive(tvAspireVersionNumber,
                FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
        tvAspireVersionNumber.setText(BuildConfig.VERSION_NAME);
    }

    @Override
    protected void bindData() {

    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showLogoApp(false);
            ((HomeActivity) getActivity()).setTitle(getString(R.string.about_title));
        }
    }

    @Override
    public boolean onBack() {
        return false;
    }

}
