package com.rosbank.android.russia.dcr;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.dcr.model.DCRPrivilegeObject;
import com.rosbank.android.russia.dcr.widgets.ItemPrivilegeDetailInfo;
import com.rosbank.android.russia.dcr.widgets.ItemPrivilegeDetailInfoLocation;
import com.rosbank.android.russia.dcr.widgets.ItemPrivilegeDetailInfoPrivileges;
import com.rosbank.android.russia.fragment.BookHotelFragment;
import com.rosbank.android.russia.model.UserItem;
import com.rosbank.android.russia.utils.CommonUtils;

import android.os.Bundle;
import android.view.View;

import butterknife.OnClick;

/*
 * Created by anh.trinh on 11/03/2017.
 */
public class PrivilegeDetailHotelFragment
        extends PrivilegeDetailBaseFragment {


    public PrivilegeDetailHotelFragment() {

    }

    @Override
    protected void fillDataCenter(DCRPrivilegeObject privilegeObject) {
        mDcrPrivilegeObject = privilegeObject;
        if (mDcrPrivilegeObject == null) {
            return;
        }

        tvSummary.setVisibility(View.GONE);
        boolean isShowLine = false;
       /* if(!isShowLine) {
            isShowLine = CommonUtils.isStringValid(privilegeObject.getSummary());
        }*/
        if (CommonUtils.isStringValid(privilegeObject.getDescription())) {

            ItemPrivilegeDetailInfo itemDescription = new ItemPrivilegeDetailInfo(getContext());
            itemDescription.setData("",
                                    0,
                                    privilegeObject.getDescription(),
                                    0,
                                    "",isShowLine
                                    ,
                                    true);
            layoutPrivilegeInfo.addView(itemDescription);
        }
        if(!isShowLine) {
            isShowLine = CommonUtils.isStringValid(privilegeObject.getDescription());
        }


        //PrivilegeDescription
        if(!isShowLine) {
            isShowLine = privilegeObject.getTypes() != null && privilegeObject.getTypes()
                    .size() > 0 && CommonUtils.isStringValid(privilegeObject.getTypes()
                    .get(0)
                    .getName());
        }

        String privileges = privilegeObject.getStringPrivilegeDescription();
        if ( CommonUtils.isStringValid(privileges)) {
            ItemPrivilegeDetailInfoPrivileges itemPrivilegeDescription =
                    new ItemPrivilegeDetailInfoPrivileges(getContext());
            itemPrivilegeDescription.setPrivileges(privileges,
                    privilegeObject.getTerms_and_conditions(),
                    isShowLine);
            layoutPrivilegeInfo.addView(itemPrivilegeDescription);
        }else{
            if(CommonUtils.isStringValid(privilegeObject.getTerms_and_conditions())) {

                ItemPrivilegeDetailInfo itemPrivilegeDescription =
                        new ItemPrivilegeDetailInfo(getContext());
                itemPrivilegeDescription.setData("",
                        0,
                        "",
                        0,
                        privilegeObject.getTerms_and_conditions(),
                        isShowLine,
                        true);
                layoutPrivilegeInfo.addView(itemPrivilegeDescription);
            }
        }

        if (CommonUtils.isStringValid(privilegeObject.getAddress())) {

            ItemPrivilegeDetailInfoLocation
                    itemLocation = new ItemPrivilegeDetailInfoLocation(getContext());
            itemLocation.setExpandAsDefault(false);
            itemLocation.setAddress(
                    privilegeObject.getAddress(),isShowLine);
            itemLocation.setPrivilegeDetailInfoListener(new ItemPrivilegeDetailInfoLocation.ItemPrivilegeDetailInfoListener() {
                @Override
                public void onTextInfoClick() {
                    if (mDcrPrivilegeObject == null) {
                        return;
                    }
                    openMap(mDcrPrivilegeObject.getLatitude(),
                            mDcrPrivilegeObject.getLongitude(),
                            mDcrPrivilegeObject.getAddress());
                }
            });
            layoutPrivilegeInfo.addView(itemLocation);

        }

        tvVisitWebsite.setText(getString(R.string.privileges_detail_visit_hotel_web_service));
    }

    @Override
    @OnClick({R.id.btnPrivilegeDetailBook})
    protected void onPrivilegeDetailBookClick(View view) {
        super.onPrivilegeDetailBookClick(view);
        if (UserItem.isLogined()) {
            BaseFragment fragment = new BookHotelFragment();
            Bundle bundle = new Bundle();
            if (mDcrPrivilegeObject != null) {
                bundle.putSerializable(AppConstant.PRE_HOTEL_DATA,
                                       mDcrPrivilegeObject);
            }
            fragment.setArguments(bundle);
            pushFragment(fragment,
                         true,
                         true);
        }
    }
}
