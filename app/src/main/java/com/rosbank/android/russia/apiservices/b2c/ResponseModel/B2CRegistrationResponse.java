package com.rosbank.android.russia.apiservices.b2c.ResponseModel;

import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.google.gson.annotations.Expose;

import java.util.List;


public class B2CRegistrationResponse
        extends B2CBaseResponse {
    @Expose
    public List<String> OnlineMemberDetailIDs;

    @Expose
    public String OnlineMemberID;

    public List<String> getOnlineMemberDetailIDs() {
        return OnlineMemberDetailIDs;
    }

    public void setOnlineMemberDetailIDs(List<String> onlineMemberDetailIDs) {
        OnlineMemberDetailIDs = onlineMemberDetailIDs;
    }

    public String getOnlineMemberID() {
        return OnlineMemberID;
    }

    public void setOnlineMemberID(String onlineMemberID) {
        OnlineMemberID = onlineMemberID;
    }
}
