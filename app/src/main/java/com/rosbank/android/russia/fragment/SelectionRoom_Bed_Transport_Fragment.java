package com.rosbank.android.russia.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.activitys.HomeActivity;
import com.rosbank.android.russia.adapter.SelectionRoom_Bed_Transport_Adapter;
import com.rosbank.android.russia.apiservices.ResponseModel.BedPreferencesResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.RoomTypePreferencesResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.TransportTypesResponse;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.model.CommonObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by anh.trinh on 9/13/2016.
 */
public class SelectionRoom_Bed_Transport_Fragment
        extends BaseFragment implements Callback {

    @Override
    public void onResponse(final Call call,
                           final Response response) {
        if (response.body() instanceof RoomTypePreferencesResponse) {
            hideDialogProgress();
            if(getActivity()==null) {
                return;
            }

            RoomTypePreferencesResponse roomTypePreferencesResponse = ((RoomTypePreferencesResponse) response.body());
            int code = roomTypePreferencesResponse.getStatus();
            if (code == 200) {
                data.clear();
                data.addAll(roomTypePreferencesResponse.getData());
                adapter = new SelectionRoom_Bed_Transport_Adapter(getActivity(),
                                                                  data,
                                                                  dataSelected);
                mListview.setAdapter(adapter);
            }else{
//                showToast(roomTypePreferencesResponse.getMessage());
            }
        }
        if (response.body() instanceof BedPreferencesResponse) {
            hideDialogProgress();
            if(getActivity()==null) {
                return;
            }

            BedPreferencesResponse bedPreferencesResponse = ((BedPreferencesResponse) response.body());
            int code = bedPreferencesResponse.getStatus();
            if (code == 200) {
                data.clear();
                data.addAll(bedPreferencesResponse.getData());
                adapter = new SelectionRoom_Bed_Transport_Adapter(getActivity(),
                                                                  data,
                                                                  dataSelected);
                mListview.setAdapter(adapter);
            }else{
//                showToast(bedPreferencesResponse.getMessage());
            }
        }
        if (response.body() instanceof TransportTypesResponse) {
            hideDialogProgress();
            if(getActivity()==null) {
                return;
            }

            TransportTypesResponse transportTypesResponse = ((TransportTypesResponse) response.body());
            int code = transportTypesResponse.getStatus();
            if (code == 200) {
                data.clear();
                data.addAll(transportTypesResponse.getData());
                adapter = new SelectionRoom_Bed_Transport_Adapter(getActivity(),
                                                                  data,
                                                                  dataSelected);
                mListview.setAdapter(adapter);
            }else{
//                showToast(transportTypesResponse.getMessage());
            }
        }
    }

    @Override
    public void onFailure(final Call call,
                          final Throwable t) {
        hideDialogProgress();
        if(getActivity()==null) {
            return;
        }

//        showToast(getString(R.string.text_server_error_message));
        /*Bundle bundle = new Bundle();
        bundle.putString(ErrorHomeFragment.PRE_ERROR_MESSAGE,
                         getString(R.string.text_server_error_message));
        ErrorHomeFragment fragment = new ErrorHomeFragment();
        fragment.setArguments(bundle);
        pushFragment(fragment,
                     true,
                     true);*/
    }

    public interface SelectionCallback {
        void onFinishSelection(Bundle bundle);
    }

    public static final String PRE_SELECTION_DATA = "selection_data";
    public static final String PRE_SELECTION_TYPE = "selection_type";
    public static final String PRE_SELECTION_ROOM = "ROOM";
    public static final String PRE_SELECTION_BED = "BED";
    public static final String PRE_SELECTION_TRANSPORT_TYPE = "TRANSPORT_TYPE";
    public static final String PRE_HEADER_TITLE = "header_title";

    @BindView(R.id.listview)
    ListView mListview;
    String typeSelection;
    ArrayList<CommonObject> data = new ArrayList<CommonObject>();
    SelectionRoom_Bed_Transport_Adapter adapter;
    CommonObject dataSelected = null;
    String headerTitle;

    SelectionCallback selectionCallback;

    public void setSelectionCallBack(SelectionCallback selectionCallBack) {
        this.selectionCallback = selectionCallBack;
    }


    @Override
    protected int layoutId() {
        return R.layout.fragment_selection_bed_room;
    }

    @Override
    protected void initView() {
        mListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> adapterView,
                                    final View view,
                                    final int i,
                                    final long l) {
                if (selectionCallback != null) {
                    Bundle bundle = new Bundle();
                    bundle.putString(PRE_SELECTION_TYPE,
                                     typeSelection);
                    if(dataSelected ==null ||!data.get(i).getValue().equals(dataSelected.getValue())) {
                    bundle.putParcelable(PRE_SELECTION_DATA,
                                     data.get(i));
                    }
                    selectionCallback.onFinishSelection(bundle);
                }
                onBackPress();

            }
        });
        if (getArguments() != null) {
            typeSelection = getArguments().getString(PRE_SELECTION_TYPE,
                                                     "");
            dataSelected = getArguments().getParcelable(PRE_SELECTION_DATA);
            headerTitle = getArguments().getString(PRE_HEADER_TITLE);
            String[] resource;
            if (typeSelection == PRE_SELECTION_ROOM) {
                /*if (getActivity() instanceof HomeActivity) {
                    ((HomeActivity) getActivity()).setTitle(getString(R.string.text_title_room_type));

                }*/
                /*showDialogProgress();
                WSRoomTypePreferences wsRoomTypePreferences = new WSRoomTypePreferences();
                wsRoomTypePreferences.run(this);*/
                data.clear();
                data.addAll(getHotelRoomList());
                adapter = new SelectionRoom_Bed_Transport_Adapter(getActivity(),
                        data,
                        dataSelected);
                mListview.setAdapter(adapter);

            } else {
                if (typeSelection == PRE_SELECTION_BED) {
                    /*if (getActivity() instanceof HomeActivity) {
                        ((HomeActivity) getActivity()).setTitle(getString(R.string.text_title_bed_type));

                    }*/
                    /*showDialogProgress();
                    WSBedPreferences wsBedPreferences = new WSBedPreferences();
                    wsBedPreferences.run(this);*/
                    data.clear();
                    data.addAll(getHotelBedList());
                    adapter = new SelectionRoom_Bed_Transport_Adapter(getActivity(),
                            data,
                            dataSelected);
                    mListview.setAdapter(adapter);
                }else{
                    if (typeSelection == PRE_SELECTION_TRANSPORT_TYPE) {
                        /*if (getActivity() instanceof HomeActivity) {
                            ((HomeActivity) getActivity()).setTitle(getString(R.string.text_title_transport_type));

                        }*/
                        /*showDialogProgress();
                        WSGetTransportTypes wsGetTransportTypes = new WSGetTransportTypes();
                        wsGetTransportTypes.run(this);*/
                        data.clear();
                        data.addAll(getCarTransferList());
                        adapter = new SelectionRoom_Bed_Transport_Adapter(getActivity(),
                                data,
                                dataSelected);
                        mListview.setAdapter(adapter);
                    }
                }
            }
        }
    }
    private List<CommonObject> getCarTransferList(){
        List<CommonObject> carTransferObjList = new ArrayList<>();
        String[]carTransferArr = getResources().getStringArray(R.array.transport_type_array);
        for(int index = 0; index < carTransferArr.length; index++){
            CommonObject commonObject = new CommonObject();
            commonObject.setKey(String.valueOf(index));
            commonObject.setValue(carTransferArr[index]);
            carTransferObjList.add(commonObject);
        }
        return carTransferObjList;
    }
    private List<CommonObject> getHotelRoomList(){
        List<CommonObject> hotelRoomObjList = new ArrayList<>();
        String[] hotelRoomArr = getResources().getStringArray(R.array.room_preference_array);
        for(int index = 0; index < hotelRoomArr.length; index++){
            CommonObject commonObject = new CommonObject();
            commonObject.setKey(String.valueOf(index));
            commonObject.setValue(hotelRoomArr[index]);
            hotelRoomObjList.add(commonObject);
        }
        return hotelRoomObjList;
    }
    private List<CommonObject> getHotelBedList(){
        List<CommonObject> hotelBedObjList = new ArrayList<>();
        String[] hotelBedArr = getResources().getStringArray(R.array.bed_preference_array);
        for(int index = 0; index < hotelBedArr.length; index++){
            CommonObject commonObject = new CommonObject();
            commonObject.setKey(String.valueOf(index));
            commonObject.setValue(hotelBedArr[index]);
            hotelBedObjList.add(commonObject);
        }
        return hotelBedObjList;
    }
    @Override
    protected void bindData() {

    }

    @Override
    public boolean onBack() {
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!TextUtils.isEmpty(headerTitle)){
            setTitle(headerTitle);
        }else {
        setTitle(getString(R.string.text_title_preference));
        }
        showLogoApp(false);
        hideToolbarMenuIcon();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showToolbarMenuIcon();


        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater,
                                           container,
                                           savedInstanceState);
        ButterKnife.bind(this,
                         rootView);
        return rootView;
    }

}
