package com.rosbank.android.russia.utils;

/**
 * Created by anh.trinh on 9/19/2016.
 */
public class FontUtils {
    // Define constant font
    public static final String FONT_FILE_NAME_AVENIR_NEXT= "fonts/Avenir Next.ttc.ttf";
    public static final String FONT_FILE_NAME_AVENIR_NEXT_LTPRO_ULTLT= "fonts/AvenirNextLTPro-UltLt.otf";
    public static final String FONT_FILE_NAME_AVENIR_NEXT_LTPRO_REGULAR= "fonts/AvenirNextLTPro-Regular.otf";
    public static final String FONT_FILE_NAME_AVENIR_NEXT_LTPRO_MEDIUM= "fonts/AvenirNextLTPro-Medium.otf";
    public static final String FONT_FILE_NAME_AVENIR_NEXT_LT_ULTLT= "fonts/AvenirNextLTPro-Regular.otf";
    public static final String FONT_FILE_NAME_AVENIR_NEXT_LT_ROMAN= "fonts/AvenirLTStd-Roman.otf";

    // Use LTPro to display Russian
    //public static final String FONT_FILE_NAME_AVENIR_NEXT_LT_ULTLT= "fonts/AvenirLTStd-Light.otf";
    //public static final String FONT_FILE_NAME_AVENIR_NEXT_LT_ROMAN= "fonts/AvenirLTStd-Roman.otf";
    // public static final String FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM= "fonts/AvenirLTStd-Medium.otf";
    //  public static final String FONT_FILE_NAME_AVENIR_NEXT_LTPRO_DEMI= "fonts/AvenirNextLTPro-Demi.otf";
    // public static final String FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD= "fonts/avenirnext-demibold.ttf";

    public static final String FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM= "fonts/AvenirNextLTPro-Medium.otf";
    public static final String FONT_FILE_NAME_GOTHAM_ROUND_MEDIUM= "fonts/gotham-rounded-medium.otf";
    public static final String FONT_FILE_NAME_AVENIR_NEXT_LTPRO_DEMI= "fonts/AvenirNextLTPro-Demi.otf";
    public static final String FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD= "fonts/AvenirNextLTPro-Demi.otf";


}
