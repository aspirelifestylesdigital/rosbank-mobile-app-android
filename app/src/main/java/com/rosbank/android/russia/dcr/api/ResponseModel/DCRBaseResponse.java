package com.rosbank.android.russia.dcr.api.ResponseModel;

import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.internal.LinkedTreeMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by ThuNguyen on 11/3/2016.
 */

public abstract class DCRBaseResponse {

    @Expose
    public boolean success;
    @Expose
    public String message;
    @Expose
    public List<DCRErrorMessageResponse> errors;


    public boolean isSuccess(){
        return isTextInSuccess(message) || isTextInSuccess(success) ;
    }
    public String getMessage(){

        return message;
    }
    public String getErrorCode(){
        String errorCode="";
        if(errors!=null && errors.size()>0){
            if(errors.get(0)!=null && errors.get(0).getErrorCode()!=null)
            errorCode = errors.get(0).getErrorCode();
        }

        return errorCode;
    }
    private boolean isTextInSuccess(Object text){
        boolean isSuccess = false;
        if(text != null) {
            if (text instanceof String) {
                String textStr = (String)text;
                isSuccess = (!TextUtils.isEmpty(textStr) &&
                        (textStr.equalsIgnoreCase("true") || textStr.equalsIgnoreCase("valid")
                                || textStr.equalsIgnoreCase("success") || textStr.equalsIgnoreCase("201")));
            }else if(text instanceof LinkedTreeMap){
                LinkedTreeMap textLinkedTreeMap = (LinkedTreeMap)text;
                if(textLinkedTreeMap.containsKey("message")) {
                    message = textLinkedTreeMap.get("message").toString();
                }
                if(textLinkedTreeMap.containsKey("success")) {
                    String successIn = textLinkedTreeMap.get("success").toString();
                    isSuccess = (!TextUtils.isEmpty(successIn) &&
                            (successIn.equalsIgnoreCase("true") || successIn.equalsIgnoreCase("valid")
                                    || successIn.equalsIgnoreCase("success") || successIn.equalsIgnoreCase("201")));
                }
            }
        }
        return isSuccess;
    }
}
