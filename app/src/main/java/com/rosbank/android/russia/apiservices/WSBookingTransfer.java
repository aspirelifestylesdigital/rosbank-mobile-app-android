package com.rosbank.android.russia.apiservices;

import android.support.annotation.NonNull;

import com.rosbank.android.russia.apiservices.RequestModel.BookTransferRequest;
import com.rosbank.android.russia.apiservices.ResponseModel.BookTransferResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.ApiProviderClient;
import com.rosbank.android.russia.utils.StringUtil;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Callback;


public class WSBookingTransfer
        extends ApiProviderClient<BookTransferResponse> {

    private BookTransferRequest request;

    public WSBookingTransfer() {

    }

    public void booking(final BookTransferRequest request) {
        this.request = request;

    }

    @Override
    public void run(Callback<BookTransferResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }

    @Override
    protected boolean isUseAuthenticateInApi() {
        return true;
    }

    @Override
    public void runApi() {
        RequestBody partBookingId = createPartFromString(request.getBookingId() == null ?
                                                         "" :
                                                         request.getBookingId());
        RequestBody partUserID = createPartFromString(request.getUserID() == null ?
                                                      "" :
                                                      request.getUserID());
        RequestBody partPhone = createPartFromString(request.getPhone() == null ?
                                                     "" :
                                                     request.getPhone() + "");
        RequestBody partEmail = createPartFromString(request.getEmail() == null ?
                                                     "" :
                                                     request.getEmail() + "");
        RequestBody partBoth = createPartFromString(request.getBoth() == null ?
                                                    "" :
                                                    request.getBoth() + "");
        RequestBody partMobileNumber = createPartFromString(request.getMobileNumber() == null ?
                                                            "" :
                                                            request.getMobileNumber() + "");
        RequestBody partEmailAddress = createPartFromString(request.getEmailAddress() == null ?
                                                            "" :
                                                            request.getEmailAddress() + "");
        RequestBody partTransportType = createPartFromString(request.getTransportType() == null ?
                                                          "" :
                                                          request.getTransportType() + "");
        RequestBody partEpochPickupDate =
                createPartFromString(request.getEpochPickupDate() == null ?
                                     "" :
                                     request.getEpochPickupDate() + "");
        RequestBody partPickupTime = createPartFromString(request.getPickupTime() == null ?
                                                          "" :
                                                          request.getPickupTime() + "");
        RequestBody partPickupLocation = createPartFromString(request.getPickupLocation() == null ?
                                                              "" :
                                                              request.getPickupLocation() + "");
        RequestBody partDropOffLocation =
                createPartFromString(request.getDropOffLocation() == null ?
                                     "" :
                                     request.getDropOffLocation() + "");


        RequestBody partNbOfPassenger = createPartFromString(request.getNbOfPassenger() == null ?
                                                            "" :
                                                            request.getNbOfPassenger() + "");
        RequestBody partSpecialRequirements =
                createPartFromString(request.getSpecialRequirements() == null ?
                                     "" :
                                     request.getSpecialRequirements() + "");

        MultipartBody.Part partUploadPhoto = null;
        if (request.getUploadPhoto() != null) {
            File file = new File(request.getUploadPhoto());
            if (!StringUtil.isEmpty(request.getUploadPhoto()) && file.exists()) {
                RequestBody requestFile =
                        RequestBody.create(MediaType.parse("multipart/form-data"),
                                           file);
                partUploadPhoto =
                        MultipartBody.Part.createFormData("picture",
                                                          file.getName(),
                                                          requestFile);
            } else {

            }
        }


        serviceInterface.bookTransfer(partBookingId,
                                    partUserID,
                                    partUploadPhoto,
                                    partPhone,
                                    partEmail,
                                    partBoth,
                                    partMobileNumber,
                                    partEmailAddress,
                                    partTransportType,
                                    partEpochPickupDate,
                                    partPickupTime,
                                    partPickupLocation,
                                    partDropOffLocation,
                                    partNbOfPassenger,
                                    partSpecialRequirements
                                   )
                        .enqueue(callback);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("User-Agent",
                    "Retrofit-Sample-App");
        return headers;
    }

    @NonNull
    private RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(
                MediaType.parse("multipart/form-data"),
                descriptionString);
    }


}
