package com.rosbank.android.russia.application;

import android.annotation.TargetApi;
import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.analytics.ecommerce.Product;
import com.google.android.gms.analytics.ecommerce.ProductAction;
import com.liveperson.infra.InitLivePersonProperties;
import com.liveperson.infra.MonitoringInitParams;
import com.liveperson.infra.callbacks.InitLivePersonCallBack;
import com.liveperson.messaging.sdk.api.LivePerson;
import com.rosbank.android.russia.R;
import com.rosbank.android.russia.utils.Logger;
import com.rosbank.android.russia.utils.SharedPreferencesUtils;

import java.util.Locale;

/**
 * Created by nganguyent on 10/08/2016.
 */
public class AppContext extends Application /*MultiDexApplication*/ {
    private static AppContext sSharedInstance = null;
    private Tracker mTracker;
    // private static String language = "ru";

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        /*MultiDex.install(this);*/

        SharedPreferencesUtils.init(getBaseContext());
        Logger.visibleFileStorage();
    }

    public static Context setLocale(Context context, String language) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return updateResources(context, language);
        }

        return updateResourcesLegacy(context, language);
    }

    public void changeLocale(String language) {
        final Resources res = getResources();
        final Configuration conf = res.getConfiguration();
        if (language == null || language.length() == 0) {
            conf.locale = Locale.getDefault();
        } else {
            conf.locale = new Locale(language);
        }

        res.updateConfiguration(conf, null);
    }

    public static AppContext getSharedInstance() {
        return AppContext.sSharedInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        changeLocale(SharedPreferencesUtils.getPreferences(AppConstant.LANGUAGE_SETTINGS, AppConstant.RUSSIAN_LANGUAGE));
        sSharedInstance= this;
        GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
        analytics.setLocalDispatchPeriod(5);
        // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
        mTracker = analytics.newTracker(R.xml.global_tracker);
        // Start a new session with the hit.
        mTracker.send(new HitBuilders.EventBuilder()
                .setNewSession()
                .build());
        initLivePerson();
    }

    public void track(String eventCategory, String eventAction, String eventLabel) {
        if (TextUtils.isEmpty(eventLabel)) {
            mTracker.send(new HitBuilders.EventBuilder()
                    .setCategory(eventCategory)
                    .setAction(eventAction)
                    .setValue(1)
                    .build());
        } else {
            mTracker.send(new HitBuilders.EventBuilder()
                    .setCategory(eventCategory)
                    .setAction(eventAction)
                    .setLabel(eventLabel)
                    .setValue(1)
                    .build());
        }
    }

    public void track(String productCategory, String productName) {
        Product product = new Product().setQuantity(1).setCategory(productCategory).setName(productName).setBrand(AppConstant.TRACK_EVENT_PRODUCT_BRAND);
        HitBuilders.EventBuilder builder = new HitBuilders.EventBuilder();
        builder.setProductAction(new ProductAction(ProductAction.ACTION_CHECKOUT).setCheckoutStep(1));
        builder.addProduct(product);
        builder.addImpression(product, "ConciergeRequest");
        mTracker.send(builder.build());

    }

    private void initLivePerson() {
        MonitoringInitParams monitoringInitParams = new MonitoringInitParams(AppConstant.LP_APP_INSTALLATION_ID);
        LivePerson.initialize(setLocale(this,AppConstant.RUSSIAN_LANGUAGE), new InitLivePersonProperties(AppConstant.BRAND_ID, AppConstant.APP_PACKAGE_NAME, monitoringInitParams, new InitLivePersonCallBack() {
            @Override
            public void onInitSucceed() {
                Log.d(AppContext.class.getSimpleName(), "LivePerson initialized");
            }

            @Override
            public void onInitFailed(Exception e) {
                Log.d(AppContext.class.getSimpleName(), "LivePerson initialize error " + e.getMessage());
            }
        }));
    }

    @TargetApi(Build.VERSION_CODES.N)
    private static Context updateResources(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Configuration configuration = context.getResources().getConfiguration();
        configuration.setLocale(locale);

        return context.createConfigurationContext(configuration);
    }

    @SuppressWarnings("deprecation")
    private static Context updateResourcesLegacy(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Resources resources = context.getResources();

        Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;

        resources.updateConfiguration(configuration, resources.getDisplayMetrics());

        return context;
    }
}
