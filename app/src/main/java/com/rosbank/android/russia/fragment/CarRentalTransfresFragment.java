package com.rosbank.android.russia.fragment;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.activitys.HomeActivity;
import com.rosbank.android.russia.apiservices.ResponseModel.UpdateTransportPreferencesResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CICallback;
import com.rosbank.android.russia.apiservices.b2c.B2CWSGetPreference;
import com.rosbank.android.russia.apiservices.b2c.B2CWSUpsertPreferenceRequest;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.B2CUpsertPreferenceRequest;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.preference.PreferenceDetailCarRequest;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.preference.PreferenceDetailRequest;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.model.LoyalProgramObject;
import com.rosbank.android.russia.model.RentalObject;
import com.rosbank.android.russia.model.preference.CarPreferenceDetailData;
import com.rosbank.android.russia.model.preference.PreferenceData;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.EntranceLock;
import com.rosbank.android.russia.utils.FontUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by anh.trinh on 9/13/2016.
 */
public class CarRentalTransfresFragment
        extends BaseFragment
        implements SelectionRentalFragment.SelectionCallback,
                   Callback {
    public static final String PRE_VEHICLE_DATA = "vehicle_data";
    public static final String PRE_RENTAL_COMPANY_DATA = "rental_company_data";
    public static final String PRE_OTHER_RENTAL_COMPANY_DATA = "other_rental_company";
    public static final String PRE_LOYAL_PROGRAM_CAR_RENTAL_DATA = "loyal_program_car_rental_data";

    @BindView(R.id.tv_car_rental_transfers)
    TextView mTvCarRentalTransfers;
    @BindView(R.id.line_1)
    View mLine;
    @BindView(R.id.tv_please_select_vehicle)
    TextView mTvPleaseSelectVehicle;
    @BindView(R.id.edt_preferred_rental_company)
    EditText edtPreferredRentalCompany;
    @BindView(R.id.edt_rental_company_name)
    EditText mEdtRentalCompanyName;
    @BindView(R.id.tv_we_would_like_to_help)
    TextView mTvWeWouldLikeToHelp;
    @BindView(R.id.tv_name_of_loyalty_program)
    TextView mTvNameOfLoyaltyProgram;
    @BindView(R.id.edt_name_of_program)
    EditText mEdtNameOfProgram;
    @BindView(R.id.tv_membership_no)
    TextView mTvMembershipNo;
    @BindView(R.id.edt_membership_number)
    EditText mEdtMembershipNumber;
    @BindView(R.id.btn_cancel)
    Button mBtnCancel;
    @BindView(R.id.btn_save)
    Button mBtnSave;
    @BindView(R.id.layout_bottom_button)
    LinearLayout mLayoutBottomButton;
    @BindView(R.id.container)
    LinearLayout mLayoutContainer;
    @BindView(R.id.scrollView)
    ScrollView scrollView;

    ArrayList<RentalObject> mVehicleSelected;
    ArrayList<RentalObject> mCompanySelected;
    String mOtherRentalCompany = "";
    LoyalProgramObject mLoyalProgramCarRental;

    EntranceLock entranceLock = new EntranceLock();
    CarPreferenceDetailData carPreferenceDetailData;
    @Override
    protected int layoutId() {
        return R.layout.fragment_car_rental_and_transfers;
    }

    @Override
    protected void initView() {
        CommonUtils.setFontForViewRecursive(mLayoutContainer,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
        CommonUtils.setFontForViewRecursive(mTvCarRentalTransfers,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        CommonUtils.setFontForViewRecursive(mBtnSave,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        CommonUtils.setFontForViewRecursive(mBtnCancel,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        /*if (getArguments() != null) {
            mOtherRentalCompany = getArguments().getString(PRE_OTHER_RENTAL_COMPANY_DATA,"");
            mVehicleSelected = getArguments().getParcelableArrayList(PRE_VEHICLE_DATA);
            mCompanySelected = getArguments().getParcelableArrayList(PRE_RENTAL_COMPANY_DATA);

            if(!mOtherRentalCompany.equals(""))
            {
                mEdtRentalCompanyName.setText(mOtherRentalCompany);
            }
            updateVehicle();
            updateRentalCompany();
            mLoyalProgramCarRental = getArguments().getParcelable(PRE_LOYAL_PROGRAM_CAR_RENTAL_DATA);
            if(mLoyalProgramCarRental!=null){
                mEdtNameOfProgram.setText(mLoyalProgramCarRental.getName());
                mEdtMembershipNumber.setText(mLoyalProgramCarRental.getMembershipNumber());
            }
        }*/
        showDialogProgress();
        B2CWSGetPreference b2CWSGetPreference = new B2CWSGetPreference(getPreferenceCallback);
        b2CWSGetPreference.run(null);
    }
    private void updateUI(){
        if(carPreferenceDetailData != null){
            if(!TextUtils.isEmpty(carPreferenceDetailData.getPreferredRentalVehicle())){
                String[] selectedRentalVehicleArr = carPreferenceDetailData.getPreferredRentalVehicle().split("\\s*,\\s*");
                if(selectedRentalVehicleArr != null && selectedRentalVehicleArr.length > 0){
                    mVehicleSelected = new ArrayList<>();
                    for(String selectedRentalVehicle : selectedRentalVehicleArr){
                        String vehicleRu =CommonUtils.getRusiaPreferredVehicleString(selectedRentalVehicle);
                        if(CommonUtils.isStringValid(vehicleRu)){
                            if(!carPreferenceDetailData.getPreferredRentalVehicle().contains(vehicleRu)) {
                                mVehicleSelected.add(new RentalObject(vehicleRu));
                            }
                        }else {
                            mVehicleSelected.add(new RentalObject(selectedRentalVehicle));
                        }
                    }
                }
                updateVehicle();
            }
            if(!TextUtils.isEmpty(carPreferenceDetailData.getPreferredRentalCompany())){
                String[] selectedCompanyArr = carPreferenceDetailData.getPreferredRentalCompany().split("\\s*,\\s*");
                if(selectedCompanyArr != null && selectedCompanyArr.length > 0){
                    mCompanySelected = new ArrayList<>();
                    for(String selectedCompany : selectedCompanyArr){
                        String companyRu =CommonUtils.getRusiaPreferredRentalString(selectedCompany);
                        if(CommonUtils.isStringValid(companyRu)) {
                            if(!carPreferenceDetailData.getOtherPreferredRentalCompany().contains(companyRu)) {
                                mCompanySelected.add(new RentalObject(companyRu));
                            }
                        }else{
                            mCompanySelected.add(new RentalObject(selectedCompany));
                        }
                    }
                }
                updateRentalCompany();
            }
            mEdtRentalCompanyName.setText(carPreferenceDetailData.getOtherPreferredRentalCompany());
            mEdtNameOfProgram.setText(carPreferenceDetailData.getLoyaltyProgram());
            mEdtMembershipNumber.setText(carPreferenceDetailData.getMembershipNo());
        }
    }
    @Override
    protected void bindData() {


    }

    @Override
    public boolean onBack() {
        return false;
    }


    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater,
                                           container,
                                           savedInstanceState);
        ButterKnife.bind(this,
                         rootView);

        return rootView;
    }

    @OnClick({R.id.tv_please_select_vehicle,
              R.id.btn_cancel,
              R.id.btn_save})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_please_select_vehicle:
                Bundle bundle = new Bundle();
                bundle.putString(SelectionRentalFragment.PRE_SELECTION_TYPE,
                                 SelectionRentalFragment.PRE_SELECTION_VERHICLE_TYPE);
                if(mVehicleSelected!=null) {
                    bundle.putParcelableArrayList(SelectionRentalFragment.PRE_SELECTION_DATA,
                                                  mVehicleSelected);
                }
                SelectionRentalFragment fragment = new SelectionRentalFragment();
                fragment.setSelectionCallBack(this);
                bundle.putString(SelectionRentalFragment.PRE_SELECTION_TITLE, getString(R.string.text_concierge_booking_rental_preferred_verhicle));
                fragment.setArguments(bundle);
                pushFragment(fragment,
                             true,
                             true);
                break;
            /*case R.id.tv_please_select_rental_company:
                Bundle bundle1 = new Bundle();
                bundle1.putString(SelectionRentalFragment.PRE_SELECTION_TYPE,
                                  SelectionRentalFragment.PRE_SELECTION_RENTAL_CONUNTRY);
                if(mCompanySelected!=null) {
                    bundle1.putParcelableArrayList(SelectionRentalFragment.PRE_SELECTION_DATA,
                                                   mCompanySelected);
                }
                SelectionRentalFragment fragment1 = new SelectionRentalFragment();
                fragment1.setSelectionCallBack(this);
                fragment1.setArguments(bundle1);
                pushFragment(fragment1,
                             true,
                             true);
                break;*/
            case R.id.btn_cancel:
                if(!entranceLock.isClickContinuous()) {
                    onBackPress();
                }
                break;
            case R.id.btn_save:
                if(!entranceLock.isClickContinuous()) {
                    processUpdateCarRentalAndTranfres();
                }
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showLogoApp(false);
            ((HomeActivity) getActivity()).setTitle(getString(R.string.text_title_my_preferences));
            ((HomeActivity) getActivity()).showRightTextTitle(false,
                                                              null);
        }
    }

    @Override
    public void onFinishSelection(final Bundle bundle) {

        String typeSelection = bundle.getString(SelectionFragment.PRE_SELECTION_TYPE,
                                                "");
        if (typeSelection.equals(SelectionRentalFragment.PRE_SELECTION_VERHICLE_TYPE)) {
            mVehicleSelected =
                    bundle.getParcelableArrayList(SelectionRentalFragment.PRE_SELECTION_DATA);
            updateVehicle();
        } else {
            if (typeSelection.equals(SelectionRentalFragment.PRE_SELECTION_RENTAL_CONUNTRY)) {
                mCompanySelected =
                        bundle.getParcelableArrayList(SelectionRentalFragment.PRE_SELECTION_DATA);
                updateRentalCompany();
            }
        }
    }

    private void updateVehicle() {
        if(mVehicleSelected== null || mVehicleSelected.size()== 0){
            mTvPleaseSelectVehicle.setText(getString(R.string.text_please_select_vehicle));
            mTvPleaseSelectVehicle.setTextColor(ContextCompat.getColor(getContext(), R.color.hint_color));
            return;
        }
        String vehicle = "";
        if (mVehicleSelected != null) {
            for (int i = 0; i < mVehicleSelected.size(); i++) {
                RentalObject rentalObject = mVehicleSelected.get(i);
                if (i == 0) {
                    vehicle += rentalObject.getValue();
                } else {
                    vehicle += ", " + rentalObject.getValue();

                }
            }
            mTvPleaseSelectVehicle.setText(vehicle);
            mTvPleaseSelectVehicle.setTextColor(ContextCompat.getColor(getContext(), R.color.text_color_active));

        }
    }

    private void updateRentalCompany() {
//        if(mCompanySelected== null || mCompanySelected.size()== 0){
//            mTvPleaseSelectRentalCompany.setText(getString(R.string.text_please_enter_rental_company));
//            mTvPleaseSelectRentalCompany.setTextColor(ContextCompat.getColor(getContext(), R.color.hint_color));
//            return;
//        }
        String company = "";
        if (mCompanySelected != null) {
            for (int i = 0; i < mCompanySelected.size(); i++) {
                RentalObject rentalObject = mCompanySelected.get(i);
                if (i == 0) {
                    company += rentalObject.getValue();
                } else {
                    company += ", " + rentalObject.getValue();

                }
            }
            edtPreferredRentalCompany.setText(company);
            //mTvPleaseSelectRentalCompany.setTextColor(ContextCompat.getColor(getContext(), R.color.text_color_active));
        }
    }

    private void processUpdateCarRentalAndTranfres() {
        /*String mUserID = SharedPreferencesUtils.getPreferences(AppConstant.USER_ID_PRE,
                                                               "");
        UpdateTransportPreferencesRequest updateTransportPreferencesRequest =
                new UpdateTransportPreferencesRequest();
        updateTransportPreferencesRequest.setUserID(mUserID);
        if (mVehicleSelected != null && mVehicleSelected.size() > 0) {
            ArrayList<String> vehicle = new ArrayList<>();
            for (int i = 0; i < mVehicleSelected.size(); i++) {
                vehicle.add(mVehicleSelected.get(i)
                                            .getKey());
            }
            updateTransportPreferencesRequest.setVehicles(vehicle);
        }
        if (mCompanySelected != null && mCompanySelected.size() > 0) {
            ArrayList<String> copanies = new ArrayList<>();
            for (int i = 0; i < mCompanySelected.size(); i++) {
                copanies.add(mCompanySelected.get(i)
                                             .getKey());
            }
            updateTransportPreferencesRequest.setCompanies(copanies);
        }
        if (mEdtRentalCompanyName != null && !CommonUtils.stringIsEmpty(mEdtRentalCompanyName.getText()
                                                                                             .toString()
                                                                                             .trim())&& !mEdtRentalCompanyName.getText()
                                                                                                                             .toString()
                                                                                                                             .trim().equals("")) {
            updateTransportPreferencesRequest.setOthersRentalCompany(mEdtRentalCompanyName.getText()
                                                                                          .toString()
                                                                                          .trim());
        }
        LoyaltyObject loyaltyObject = new LoyaltyObject();
        if (mEdtNameOfProgram != null && !CommonUtils.stringIsEmpty(mEdtNameOfProgram.getText()
                                                                                     .toString()
                                                                                     .trim())&& !mEdtNameOfProgram.getText()
                                                                                                                      .toString()
                                                                                                                      .trim().equals("")) {
            loyaltyObject.setName(mEdtNameOfProgram.getText()
                                                   .toString()
                                                   .trim());
        }
        if (mEdtMembershipNumber != null && !CommonUtils.stringIsEmpty(mEdtMembershipNumber.getText()
                                                                                           .toString()
                                                                                           .trim())&& !mEdtMembershipNumber.getText()
                                                                                                                        .toString()
                                                                                                                        .trim().equals("")) {
            loyaltyObject.setNumber(mEdtMembershipNumber.getText()
                                                        .toString()
                                                        .trim());
        }
        ArrayList<LoyaltyObject> loyaltyObjects = new ArrayList<>();
        loyaltyObjects.add(loyaltyObject);

        updateTransportPreferencesRequest.setLoyaltyPrograms(loyaltyObjects);

        showDialogProgress();
        WSCreateUpdateTransportPreferences wsCreateUpdateTransportPreferences =
                new WSCreateUpdateTransportPreferences();
        wsCreateUpdateTransportPreferences.updateTransportPreferencesRequest(updateTransportPreferencesRequest);
        wsCreateUpdateTransportPreferences.run(this);*/
        AppConstant.PREFERENCE_EDIT_TYPE preferenceEditType = carPreferenceDetailData != null ? AppConstant.PREFERENCE_EDIT_TYPE.UPDATE : AppConstant.PREFERENCE_EDIT_TYPE.ADD;
        PreferenceDetailRequest preferenceDetailRequest = new PreferenceDetailCarRequest("false");
        switch (preferenceEditType){
            case ADD:
                break;
            case UPDATE:
                preferenceDetailRequest.setMyPreferencesId(carPreferenceDetailData.getPreferenceId());
                break;
        }
        preferenceDetailRequest.fillData(CommonUtils.getStringArrWithSeparator(mVehicleSelected, ","), edtPreferredRentalCompany.getText().toString().trim(),
                                        mEdtRentalCompanyName.getText().toString().trim(), mEdtNameOfProgram.getText().toString().trim(),
                                        mEdtMembershipNumber.getText().toString().trim());

        B2CUpsertPreferenceRequest upsertPreferenceRequest = new B2CUpsertPreferenceRequest();
        upsertPreferenceRequest.setPreferenceDetails(preferenceDetailRequest);
        B2CWSUpsertPreferenceRequest b2CWSUpsertPreferenceRequest = new B2CWSUpsertPreferenceRequest(preferenceEditType, upsertPreferenceCallback);
        b2CWSUpsertPreferenceRequest.setRequest(upsertPreferenceRequest);
        b2CWSUpsertPreferenceRequest.run(null);
        showDialogProgress();
    }

    @Override
    public void onResponse(final Call call,
                           final Response response) {
        if (response.body() instanceof UpdateTransportPreferencesResponse) {
            hideDialogProgress();
            if(getActivity()==null) {
                return;
            }
            UpdateTransportPreferencesResponse myPreferenceResponse =
                    ((UpdateTransportPreferencesResponse) response.body());
            int code = myPreferenceResponse.getStatus();
            if (code == 200) {
                onBackPress();
            }else{
//                showToast(myPreferenceResponse.getMessage());
            }
        }

    }

    @Override
    public void onFailure(final Call call,
                          final Throwable t) {
        hideDialogProgress();
        if(getActivity()==null) {
            return;
        }
//        showToast(getString(R.string.text_server_error_message));
       /* Bundle bundle = new Bundle();
        bundle.putString(ErrorHomeFragment.PRE_ERROR_MESSAGE,
                         getString(R.string.text_server_error_message));
        ErrorHomeFragment fragment = new ErrorHomeFragment();
        fragment.setArguments(bundle);
        pushFragment(fragment,
                     true,
                     true);*/

    }
    private B2CICallback getPreferenceCallback = new B2CICallback() {
        @Override
        public void onB2CResponse(B2CBaseResponse response) {
            hideDialogProgress();
            if(getActivity()==null) {
                return;
            }
        }

        @Override
        public void onB2CResponseOnList(List<B2CBaseResponse> responseList) {
            hideDialogProgress();
            if(getActivity()==null) {
                return;
            }
            carPreferenceDetailData = (CarPreferenceDetailData) PreferenceData.findFromList(responseList, AppConstant.PREFERENCE_TYPE.CAR);
            updateUI();
        }

        @Override
        public void onB2CFailure(String errorMessage, String errorCode) {
            hideDialogProgress();
            if(getActivity()==null) {
                return;
            }
        }
    };

    private B2CICallback upsertPreferenceCallback = new B2CICallback() {
        @Override
        public void onB2CResponse(B2CBaseResponse response) {
            hideDialogProgress();
            if(getActivity()==null) {
                return;
            }
            if(response != null && response.isSuccess()){
                onBackPress();
            }else{
//                showToast(getString(R.string.text_server_error_message));
                showDialogMessage("", getString(R.string.text_concierge_request_error));
            }
        }

        @Override
        public void onB2CResponseOnList(List<B2CBaseResponse> responseList) {
            if(getActivity()==null) {
                return;
            }
        }

        @Override
        public void onB2CFailure(String errorMessage, String errorCode) {
            hideDialogProgress();
            if(getActivity()==null) {
                return;
            }
//            showToast(getString(R.string.text_server_error_message));
            showDialogMessage("", getString(R.string.text_concierge_request_error));
        }
    };
}
