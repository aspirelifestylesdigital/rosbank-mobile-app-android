package com.rosbank.android.russia.fragment;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.activitys.HomeActivity;
import com.rosbank.android.russia.apiservices.ResponseModel.RatingPreferencesResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.SmokingRoomPreferencesResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.UpdateHotelPreferencesResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CICallback;
import com.rosbank.android.russia.apiservices.b2c.B2CWSGetPreference;
import com.rosbank.android.russia.apiservices.b2c.B2CWSUpsertPreferenceRequest;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.B2CUpsertPreferenceRequest;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.preference.PreferenceDetailHotelRequest;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.preference.PreferenceDetailRequest;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.model.CommonObject;
import com.rosbank.android.russia.model.LoyalProgramObject;
import com.rosbank.android.russia.model.preference.HotelPreferenceDetailData;
import com.rosbank.android.russia.model.preference.PreferenceData;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.EntranceLock;
import com.rosbank.android.russia.utils.FontUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by anh.trinh on 9/13/2016.
 */
public class HotelMyPreferencesFragment
        extends BaseFragment
        implements SelectionRoom_Bed_Transport_Fragment.SelectionCallback,
                   Callback {
    public static final String PRE_ROOM_TYPE_DATA = "room_type_data";
    public static final String PRE_BED_DATA = "bed_data";
    public static final String PRE_SMOKING_ROOM_DATA = "smoking_room_data";
    public static final String PRE_LOYAL_PROGRAM_HOTEL_DATA = "loyal_program_hotel_data";
    public static final String PRE_RATING_DATA = "rating_data";

    @BindView(R.id.tv_hotel_title)
    TextView mTvHotelTitle;
    @BindView(R.id.line_1)
    View line1;
    @BindView(R.id.tv_preferred_star_rating)
    TextView mTvPreferredStarRating;
    @BindView(R.id.ratingbar)
    RatingBar mRatingbar;
    @BindView(R.id.tv_room_preference)
    TextView mTvRoomPreference;
    @BindView(R.id.tv_please_select_room)
    TextView mTvPleaseSelectRoom;
    @BindView(R.id.tv_bed_preference)
    TextView mTvBedPreference;
    @BindView(R.id.tv_please_select_bed)
    TextView mTvPleaseSelectBed;
    @BindView(R.id.tv_prefer_a_smoking_room)
    TextView mTvPreferASmokingRoom;
    @BindView(R.id.tb_switch)
    ToggleButton mCbSwitch;
    @BindView(R.id.edtAnyOtherPreferences)
    EditText edtAnyOtherPreferences;
    @BindView(R.id.tv_we_would_like_to_help)
    TextView mTvWeWouldLikeToHelp;
    @BindView(R.id.tv_name_of_loyalty_program)
    TextView mTvNameOfLoyaltyProgram;
    @BindView(R.id.edt_name_of_program)
    EditText mEdtNameOfProgram;
    @BindView(R.id.tv_membership_no)
    TextView mTvMembershipNo;
    @BindView(R.id.edt_membership_number)
    EditText mEdtMembershipNumber;
    @BindView(R.id.btn_cancel)
    Button mBtnCancel;
    @BindView(R.id.btn_save)
    Button mBtnSave;
    @BindView(R.id.layout_bottom_button)
    LinearLayout mLayoutBottomButton;
    @BindView(R.id.container)
    LinearLayout mLayoutContainer;
    @BindView(R.id.scrollView)
    ScrollView mScrollView;

    ArrayList<CommonObject> mRoomType;
    ArrayList<CommonObject> mBedType;
    CommonObject mRating;
    CommonObject mSmoking;
    CommonObject mRoomTypeSelected;
    CommonObject mBedTypeSelected;
    LoyalProgramObject mLoyalProgramHotel;
    ArrayList<CommonObject> mRatingList;
    ArrayList<CommonObject> mSmokingList;

    HotelPreferenceDetailData hotelPreferenceDetailData;
    EntranceLock entranceLock = new EntranceLock();
    @Override
    protected int layoutId() {
        return R.layout.fragment_hotel_preferences;
    }

    @Override
    protected void initView() {
        CommonUtils.setFontForViewRecursive(mLayoutContainer,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
        CommonUtils.setFontForViewRecursive(mTvHotelTitle,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        CommonUtils.setFontForViewRecursive(mBtnSave,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        CommonUtils.setFontForViewRecursive(mBtnCancel,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        /*if (getArguments() != null) {
            mRoomType = getArguments().getParcelableArrayList(PRE_ROOM_TYPE_DATA);
            mBedType = getArguments().getParcelableArrayList(PRE_BED_DATA);
            mRating = getArguments().getParcelable(PRE_RATING_DATA);
            mSmoking = getArguments().getParcelable(PRE_SMOKING_ROOM_DATA);
            mLoyalProgramHotel = getArguments().getParcelable(PRE_LOYAL_PROGRAM_HOTEL_DATA);
            setData();

        }
        WSRatingPreferences wsRatingPreferences = new WSRatingPreferences();
        wsRatingPreferences.run(this);
        WSSmokingRoomPreferences wsSmokingRoomPreferences = new WSSmokingRoomPreferences();
        wsSmokingRoomPreferences.run(this);*/
        // Get hotel for first init
        showDialogProgress();
        B2CWSGetPreference b2CWSGetPreference = new B2CWSGetPreference(getPreferenceCallback);
        b2CWSGetPreference.run(null);

    }

    private void setData() {
        if (mRoomType != null && mRoomType.size() > 0) {
            mRoomTypeSelected = mRoomType.get(0);
            if (mRoomTypeSelected != null) {

                mTvPleaseSelectRoom.setText(mRoomTypeSelected.getValue());
                mTvPleaseSelectRoom.setTextColor(ContextCompat.getColor(getContext(), R.color.text_color_active));
            } else {
                mTvPleaseSelectRoom.setText(getString(R.string.text_hotel_please_select_room_preference));
                mTvPleaseSelectRoom.setTextColor(ContextCompat.getColor(getContext(), R.color.hint_color));
            }
        } else {
            mTvPleaseSelectRoom.setText(getString(R.string.text_hotel_please_select_room_preference));
            mTvPleaseSelectRoom.setTextColor(ContextCompat.getColor(getContext(), R.color.hint_color));
        }
        if (mBedType != null && mBedType.size() > 0) {
            mBedTypeSelected = mBedType.get(0);
            if (mBedTypeSelected != null) {
                mTvPleaseSelectBed.setText(mBedTypeSelected.getValue());
                mTvPleaseSelectBed.setTextColor(ContextCompat.getColor(getContext(), R.color.text_color_active));
            } else {
                mTvPleaseSelectBed.setText(getString(R.string.text_hotel_please_select_bed_preference));
                mTvPleaseSelectBed.setTextColor(ContextCompat.getColor(getContext(), R.color.hint_color));
            }
        } else {
            mTvPleaseSelectBed.setText(getString(R.string.text_hotel_please_select_bed_preference));
            mTvPleaseSelectBed.setTextColor(ContextCompat.getColor(getContext(), R.color.hint_color));
        }
        if (mRating != null && mRating.getValue() != null) {
            if (mRating.getValue()
                       .contains("1")) {
                mRatingbar.setRating(1);
            }
            if (mRating.getValue()
                       .contains("2")) {
                mRatingbar.setRating(2);
            }
            if (mRating.getValue()
                       .contains("3")) {
                mRatingbar.setRating(3);
            }
            if (mRating.getValue()
                       .contains("4")) {
                mRatingbar.setRating(4);
            }
            if (mRating.getValue()
                       .contains("5")) {
                mRatingbar.setRating(5);
            }
        }
        if (mSmoking != null && mSmoking.getValue() != null) {
            if (mSmoking.getValue()
                        .equals(getString(R.string.text_yes))) {
                mCbSwitch.setChecked(true);
            } else {
                mCbSwitch.setChecked(false);
            }
        }
        if (mLoyalProgramHotel != null) {
            mEdtNameOfProgram.setText(mLoyalProgramHotel.getName());
            mEdtMembershipNumber.setText(mLoyalProgramHotel.getMembershipNumber());
        }

    }

    private void updateUI(){
        if(getActivity()==null) {
            return;
        }
        if(hotelPreferenceDetailData != null){
            if(hotelPreferenceDetailData.getRating() > 0){
                mRatingbar.setRating(hotelPreferenceDetailData.getRating());
            }
            if(!TextUtils.isEmpty(hotelPreferenceDetailData.getRoomPreference())){
                String roomRu =CommonUtils.getRusiaRoomPreferenceString(hotelPreferenceDetailData.getRoomPreference());
                if(CommonUtils.isStringValid(roomRu)){
                    mRoomTypeSelected =
                            new CommonObject(roomRu);
                    mTvPleaseSelectRoom.setText(roomRu);
                }else {
                    mRoomTypeSelected =
                            new CommonObject(hotelPreferenceDetailData.getRoomPreference());
                    mTvPleaseSelectRoom.setText(hotelPreferenceDetailData.getRoomPreference());
                }

                mTvPleaseSelectRoom.setTextColor(ContextCompat.getColor(getContext(), R.color.text_color_active));
            }
            if(!TextUtils.isEmpty(hotelPreferenceDetailData.getBedPreference())){
                String bedRu =CommonUtils.getRusianBedPreferenceString(hotelPreferenceDetailData.getBedPreference());
                if(CommonUtils.isStringValid(bedRu)){
                    mBedTypeSelected =
                            new CommonObject(bedRu);
                    mTvPleaseSelectBed.setText(bedRu);
                }else {
                    mBedTypeSelected = new CommonObject(hotelPreferenceDetailData.getBedPreference());
                    mTvPleaseSelectBed.setText(hotelPreferenceDetailData.getBedPreference());
                }


                mTvPleaseSelectBed.setTextColor(ContextCompat.getColor(getContext(), R.color.text_color_active));
            }
            mCbSwitch.setChecked(hotelPreferenceDetailData.isSmokingPreference());
            mEdtNameOfProgram.setText(hotelPreferenceDetailData.getLoyaltyProgram());
            mEdtMembershipNumber.setText(hotelPreferenceDetailData.getMemberShipNo());
            edtAnyOtherPreferences.setText(hotelPreferenceDetailData.getAdditionalPreference());
        }
    }
    @Override
    protected void bindData() {
    }

    @Override
    public boolean onBack() {
        return false;
    }


    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater,
                                           container,
                                           savedInstanceState);
        ButterKnife.bind(this,
                         rootView);
        return rootView;
    }

    @OnClick({R.id.tv_please_select_room,
              R.id.tv_please_select_bed,
              R.id.btn_cancel,
              R.id.btn_save})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_please_select_room:
                Bundle bundle = new Bundle();
bundle.putString(SelectionRoom_Bed_Transport_Fragment.PRE_HEADER_TITLE, getString(R.string.text_hotel_room_preference));
                bundle.putString(SelectionRoom_Bed_Transport_Fragment.PRE_SELECTION_TYPE,
                                 SelectionRoom_Bed_Transport_Fragment.PRE_SELECTION_ROOM);
                bundle.putParcelable(SelectionRoom_Bed_Transport_Fragment.PRE_SELECTION_DATA,
                                     mRoomTypeSelected);

                SelectionRoom_Bed_Transport_Fragment selectionRoomBedTransportFragment =
                        new SelectionRoom_Bed_Transport_Fragment();
                selectionRoomBedTransportFragment.setArguments(bundle);
                selectionRoomBedTransportFragment.setSelectionCallBack(this);
                pushFragment(selectionRoomBedTransportFragment,
                             true,
                             true);
                break;
            case R.id.tv_please_select_bed:
                Bundle bundleBed = new Bundle();
bundleBed.putString(SelectionRoom_Bed_Transport_Fragment.PRE_HEADER_TITLE, getString(R.string.text_hotel_bed_preference));
                bundleBed.putString(SelectionRoom_Bed_Transport_Fragment.PRE_SELECTION_TYPE,
                                    SelectionRoom_Bed_Transport_Fragment.PRE_SELECTION_BED);
                bundleBed.putParcelable(SelectionRoom_Bed_Transport_Fragment.PRE_SELECTION_DATA,
                                        mBedTypeSelected);

                SelectionRoom_Bed_Transport_Fragment
                        selectionBedFragment = new SelectionRoom_Bed_Transport_Fragment();
                selectionBedFragment.setArguments(bundleBed);
                selectionBedFragment.setSelectionCallBack(this);
                pushFragment(selectionBedFragment,
                             true,
                             true);
                break;
            case R.id.btn_cancel:
                if(!entranceLock.isClickContinuous()) {
                    onBackPress();
                }
                break;
            case R.id.btn_save:
                if(!entranceLock.isClickContinuous()) {
                    processUpdateHotelPreferences();
                }
                break;
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showLogoApp(false);
            ((HomeActivity) getActivity()).setTitle(getString(R.string.text_title_my_preferences));
            ((HomeActivity) getActivity()).showRightTextTitle(false,
                                                              null);
        }
    }

    @Override
    public void onFinishSelection(final Bundle bundle) {

        String typeSelection = bundle.getString(SelectionRoom_Bed_Transport_Fragment.PRE_SELECTION_TYPE,
                                                "");
        if (typeSelection.equals(SelectionRoom_Bed_Transport_Fragment.PRE_SELECTION_ROOM)) {
            mRoomTypeSelected =
                    bundle.getParcelable(SelectionRoom_Bed_Transport_Fragment.PRE_SELECTION_DATA);
            if (mRoomTypeSelected != null) {
                mTvPleaseSelectRoom.setText(mRoomTypeSelected.getValue());
                mTvPleaseSelectRoom.setTextColor(ContextCompat.getColor(getContext(), R.color.text_color_active));
            }else{
                mTvPleaseSelectRoom.setText(R.string.text_hotel_please_select_room_preference);
                mTvPleaseSelectRoom.setTextColor(ContextCompat.getColor(getContext(), R.color.hint_color));
            }
        } else {
            if (typeSelection.equals(SelectionRoom_Bed_Transport_Fragment.PRE_SELECTION_BED)) {
                mBedTypeSelected =
                        bundle.getParcelable(SelectionRoom_Bed_Transport_Fragment.PRE_SELECTION_DATA);
                if (mBedTypeSelected != null) {
                    mTvPleaseSelectBed.setText(mBedTypeSelected.getValue());
                    mTvPleaseSelectBed.setTextColor(ContextCompat.getColor(getContext(), R.color.text_color_active));
                }else{
                    mTvPleaseSelectBed.setText(R.string.text_hotel_please_select_bed_preference);
                    mTvPleaseSelectBed.setTextColor(ContextCompat.getColor(getContext(), R.color.hint_color));
                }
            }
        }

    }

    @Override
    public void onResponse(final Call call,
                           final Response response) {
        if (response.body() instanceof RatingPreferencesResponse) {
            hideDialogProgress();
            if(getActivity()==null) {
                return;
            }
            RatingPreferencesResponse ratingPreferencesResponse =
                    ((RatingPreferencesResponse) response.body());
            int code = ratingPreferencesResponse.getStatus();
            if (code == 200) {
                if (mRatingList == null) {
                    mRatingList = new ArrayList<>();
                }
                mRatingList.clear();
                mRatingList.addAll(ratingPreferencesResponse.getData());
            } else {
//                showToast(ratingPreferencesResponse.getMessage());
            }
        }
        if (response.body() instanceof SmokingRoomPreferencesResponse) {
            hideDialogProgress();
            if(getActivity()==null) {
                return;
            }
            SmokingRoomPreferencesResponse smokingRoomPreferencesResponse =
                    ((SmokingRoomPreferencesResponse) response.body());
            int code = smokingRoomPreferencesResponse.getStatus();
            if (code == 200) {
                if (mSmokingList == null) {
                    mSmokingList = new ArrayList<>();
                }
                mSmokingList.clear();
                mSmokingList.addAll(smokingRoomPreferencesResponse.getData());
            } else {
//                showToast(smokingRoomPreferencesResponse.getMessage());
            }
        }
        if (response.body() instanceof UpdateHotelPreferencesResponse) {
            hideDialogProgress();
            if(getActivity()==null) {
                return;
            }
            UpdateHotelPreferencesResponse updateHotelPreferencesResponse =
                    ((UpdateHotelPreferencesResponse) response.body());
            int code = updateHotelPreferencesResponse.getStatus();
            if (code == 200) {
                onBackPress();
            } else {
//                showToast(updateHotelPreferencesResponse.getMessage());
            }
        }
    }

    @Override
    public void onFailure(final Call call,
                          final Throwable t) {
        hideDialogProgress();
        if(getActivity()==null) {
            return;
        }
//        showToast(getString(R.string.text_server_error_message));
       /* Bundle bundle = new Bundle();
        bundle.putString(ErrorHomeFragment.PRE_ERROR_MESSAGE,
                         getString(R.string.text_server_error_message));
        ErrorHomeFragment fragment = new ErrorHomeFragment();
        fragment.setArguments(bundle);
        pushFragment(fragment,
                     true,
                     true);*/

    }

    private void processUpdateHotelPreferences() {
        /*String mUserID = SharedPreferencesUtils.getPreferences(AppConstant.USER_ID_PRE,
                                                               "");
        UpdateHotelPreferencesRequest updateHotelPreferencesRequest =
                new UpdateHotelPreferencesRequest();
        updateHotelPreferencesRequest.setUserID(mUserID);
        for (CommonObject commonObject : mRatingList) {
            int rating = (int) mRatingbar.getRating();
            if (commonObject.getValue()
                            .contains(String.valueOf(rating))) {
                updateHotelPreferencesRequest.setRating(commonObject.getKey());
            }
        }
        if (mRoomTypeSelected != null) {
            ArrayList<String> room = new ArrayList<>();
            room.add(mRoomTypeSelected.getKey());
            updateHotelPreferencesRequest.setRoomTypes(room);
        }
        if (mBedTypeSelected != null) {
            ArrayList<String> beds = new ArrayList<>();
            beds.add(mBedTypeSelected.getKey());
            updateHotelPreferencesRequest.setBedTypes(beds);
        }
        for (CommonObject commonObject : mSmokingList) {
            if (mCbSwitch.isChecked()) {
                if (commonObject.getValue()
                                .equals("Yes")) {

                    updateHotelPreferencesRequest.setSmokingRoom(commonObject.getKey());
                }
            } else {
                if (commonObject.getValue()
                                .equals("No")) {

                    updateHotelPreferencesRequest.setSmokingRoom(commonObject.getKey());
                }
            }
        }
        LoyaltyObject loyaltyObject = new LoyaltyObject();
        if (mEdtNameOfProgram != null && !CommonUtils.stringIsEmpty(mEdtNameOfProgram.getText()
                                                                                     .toString()
                                                                                     .trim()) && !mEdtNameOfProgram.getText()
                                                                                                                   .toString()
                                                                                                                   .trim()
                                                                                                                   .equals("")) {
            loyaltyObject.setName(mEdtNameOfProgram.getText()
                                                   .toString()
                                                   .trim());
        }
        if (mEdtMembershipNumber != null && !CommonUtils.stringIsEmpty(mEdtMembershipNumber.getText()
                                                                                           .toString()
                                                                                           .trim()) && !mEdtMembershipNumber.getText()
                                                                                                                            .toString()
                                                                                                                            .trim()
                                                                                                                            .equals("")) {
            loyaltyObject.setNumber(mEdtMembershipNumber.getText()
                                                        .toString()
                                                        .trim());
        }
        ArrayList<LoyaltyObject> loyaltyObjects = new ArrayList<>();
        loyaltyObjects.add(loyaltyObject);

        updateHotelPreferencesRequest.setLoyaltyPrograms(loyaltyObjects);

        showDialogProgress();
        WSCreateUpdateHotelPreferences wsCreateUpdateHotelPreferences =
                new WSCreateUpdateHotelPreferences();
        wsCreateUpdateHotelPreferences.updateTransportPreferencesRequest(updateHotelPreferencesRequest);
        wsCreateUpdateHotelPreferences.run(this);*/
        AppConstant.PREFERENCE_EDIT_TYPE preferenceEditType = hotelPreferenceDetailData != null ? AppConstant.PREFERENCE_EDIT_TYPE.UPDATE : AppConstant.PREFERENCE_EDIT_TYPE.ADD;
        PreferenceDetailRequest preferenceDetailRequest = new PreferenceDetailHotelRequest("false");
        switch (preferenceEditType){
            case ADD:
                break;
            case UPDATE:
                preferenceDetailRequest.setMyPreferencesId(hotelPreferenceDetailData.getPreferenceId());
                break;
        }
        preferenceDetailRequest.fillData((int)mRatingbar.getRating(), mRoomTypeSelected != null ? mRoomTypeSelected.getValue() : "", mBedTypeSelected != null ? mBedTypeSelected.getValue() : "",
                mCbSwitch.isChecked() ? "true" : "false", mEdtNameOfProgram.getText().toString().trim(), mEdtMembershipNumber.getText().toString().trim(), edtAnyOtherPreferences.getText().toString().trim());

        B2CUpsertPreferenceRequest upsertPreferenceRequest = new B2CUpsertPreferenceRequest();
        upsertPreferenceRequest.setPreferenceDetails(preferenceDetailRequest);
        B2CWSUpsertPreferenceRequest b2CWSUpsertPreferenceRequest = new B2CWSUpsertPreferenceRequest(preferenceEditType, upsertPreferenceCallback);
        b2CWSUpsertPreferenceRequest.setRequest(upsertPreferenceRequest);
        b2CWSUpsertPreferenceRequest.run(null);
        showDialogProgress();
    }

    private B2CICallback getPreferenceCallback = new B2CICallback() {
        @Override
        public void onB2CResponse(B2CBaseResponse response) {
            hideDialogProgress();
            if(getActivity()==null) {
                return;
            }
        }

        @Override
        public void onB2CResponseOnList(List<B2CBaseResponse> responseList) {
            hideDialogProgress();
            if(getActivity()==null) {
                return;
            }
            hotelPreferenceDetailData = (HotelPreferenceDetailData)PreferenceData.findFromList(responseList, AppConstant.PREFERENCE_TYPE.HOTEL);
            updateUI();
        }

        @Override
        public void onB2CFailure(String errorMessage, String errorCode) {
            hideDialogProgress();
            if(getActivity()==null) {
                return;
            }
        }
    };

    private B2CICallback upsertPreferenceCallback = new B2CICallback() {
        @Override
        public void onB2CResponse(B2CBaseResponse response) {
            hideDialogProgress();
            if(getActivity()==null) {
                return;
            }
            if(response != null && response.isSuccess()){
                onBackPress();
            }else{
//                showToast(getString(R.string.text_server_error_message));
                showDialogMessage("", getString(R.string.text_concierge_request_error));
            }
        }

        @Override
        public void onB2CResponseOnList(List<B2CBaseResponse> responseList) {
            if(getActivity()==null) {
                return;
            }
        }

        @Override
        public void onB2CFailure(String errorMessage, String errorCode) {
            hideDialogProgress();
            if(getActivity()==null) {
                return;
            }
//            showToast(getString(R.string.text_server_error_message));
            showDialogMessage("", getString(R.string.text_concierge_request_error));
        }

    };
}
