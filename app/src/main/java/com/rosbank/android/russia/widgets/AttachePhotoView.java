package com.rosbank.android.russia.widgets;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.utils.Logger;
import com.rosbank.android.russia.utils.PhotoPickerUtils;
import com.rosbank.android.russia.utils.SelfPermissionUtils;
import com.rosbank.android.russia.utils.StringUtil;
//import com.bumptech.glide.Glide;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by nga.nguyent on 10/7/2016.
 */

public class AttachePhotoView extends LinearLayout {
    public interface IAttachePhotoCallback{
        void onPhotoChanged();
    }
    @BindView(R.id.tvAttachedPhoto)
    TextView tvPhotoAttached;
    @BindView(R.id.imgCamera)
    ImageView imgCamera;
    @BindView(R.id.rclView)
    RecyclerView rclView;

    private PhotoAdapter adapter;
    private Fragment parentFragment;
    private Uri uriTakePicture;
    private IAttachePhotoCallback attachePhotoCallback;

    public AttachePhotoView(Context context) {
        super(context);
        initialView();
    }

    public AttachePhotoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialView();
    }

    public AttachePhotoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialView();
    }

    private void initialView() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.layout_attach_photo, this, false);
        this.addView(view);

        ButterKnife.bind(this, this);

        //
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rclView.setLayoutManager(layoutManager);

        adapter = new PhotoAdapter();
        rclView.setAdapter(adapter);

        updatePhotoNumber();
    }

    public void setParentFragment(Fragment parentFragment) {
        this.parentFragment = parentFragment;
    }
    public void setAttachePhotoCallback(IAttachePhotoCallback attachePhotoCallback){this.attachePhotoCallback = attachePhotoCallback;}
    public void onActivityResult(int requestCode, Intent data) {
        Uri targetUri;
        String path = "";
        if (requestCode == PhotoPickerUtils.PICK_IMAGE_REQUEST_CODE) {
            targetUri = data.getData();
            path = PhotoPickerUtils.shareInstance(getContext()).getRealPathFromURI(targetUri);
        } else {
            targetUri = uriTakePicture;
            if (targetUri != null) {
                path = targetUri.getPath();
                uriTakePicture = null;
            }
        }

        if (StringUtil.isEmpty(path)) {
            Toast.makeText(getContext(), "Can't get image in this place.", Toast.LENGTH_SHORT).show();
        } else {
            adapter.addPhoto(path);
            adapter.notifyDataSetChanged();
        }
        updatePhotoNumber();
        if(attachePhotoCallback != null){
            attachePhotoCallback.onPhotoChanged();
        }
    }

    public List<String> getPhotos() {
        return adapter.data;
    }

    public void addUrl(String url) {
        if (url != null && url.trim().length() > 0) {
            adapter.addPhoto(url);
            updatePhotoNumber();
            adapter.notifyDataSetChanged();
        }
    }

    @OnClick(R.id.imgCamera)
    void onCameraClick() {
        if (parentFragment == null) {
            return;
        }

        choosePhotoType();
    }

    public void choosePhotoType() {
        PhotoPickerUtils.shareInstance(getContext()).showChooseTypePhotoDialog(new PhotoPickerUtils.OnPhotoTypeListener() {
            @Override
            public void onCamera() {
                //Logger.sout("camera nhe");
                openCamera();
            }

            @Override
            public void onGallery() {
                openGallery();
            }
        });
    }

    private void updatePhotoNumber() {
        int n = adapter.data.size();
        String text = getContext().getString(R.string.text_concierge_booking_hotel_attached_photo, n);
        tvPhotoAttached.setText(text);
        if (n == 0) {
            imgCamera.setVisibility(VISIBLE);
        } else {
            imgCamera.setVisibility(GONE);
        }
    }

    public void openGallery() {
        if (SelfPermissionUtils.getInstance().checkOrRequestReadExternalStoragePermission(parentFragment, AppConstant.PERMISSION_REQUEST_READ_EXTERNAL)) {
            PhotoPickerUtils.shareInstance(getContext()).openPickImage(parentFragment, PhotoPickerUtils.PICK_IMAGE_REQUEST_CODE);
        }
    }

    public void openCamera() {
        PhotoPickerUtils pickerUtils = PhotoPickerUtils.shareInstance(getContext());
        if (!pickerUtils.checkCameraHardware()) {
            pickerUtils.showDialogCameraNotFound();
        } else {
            if (SelfPermissionUtils.getInstance().checkOrRequestCameraPermission(parentFragment, AppConstant.PERMISSION_REQUEST_OPEN_CAMERA)) {
                if (SelfPermissionUtils.getInstance().checkOrRequestWriteExternalStoragePermission(parentFragment, AppConstant.PERMISSION_REQUEST_WRITE_EXTERNAL)) {
                    uriTakePicture = PhotoPickerUtils.shareInstance(getContext()).openCamera(parentFragment, PhotoPickerUtils.TAKE_PICTURE_REQUEST_CODE);
                }
            }
        }
    }

    /**
     * =============================================================
     */
    class PhotoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        List<String> data = new ArrayList<>();

        public void addPhoto(String path) {
            data.add(path);
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = View.inflate(parent.getContext(), R.layout.layout_photo_item, null);
            PhotoHolder holder = new PhotoHolder(view);
            return holder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ((PhotoHolder) holder).setData(data.get(position), position);
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        class PhotoHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.imgPhoto)
            ImageView imgPhoto;
            @BindView(R.id.imgClose)
            ImageView imgClose;

            String path;
            int pos;

            public PhotoHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            public void setData(String path, int pos) {
                this.path = path;
                this.pos = pos;

                if (path.startsWith("http")) {
                    //server
//                    Glide.with(itemView.getContext())
//                            .load(path)
//                            .crossFade()
//                            .transform(new CropImageTransformUtil(itemView.getContext()))
//                            .into(imgPhoto);

                    if (SelfPermissionUtils.getInstance().checkOrRequestWriteExternalStoragePermission(parentFragment, AppConstant.PERMISSION_REQUEST_WRITE_EXTERNAL)) {
                        String fileName = String.valueOf((new Date()).getTime());
                        if(saveFile(imgPhoto.getDrawingCache(false), fileName)) {
                            data.set(pos, fileName);
                        }
                    }

                } else {
                    //local
                    int s = getContext().getResources().getDimensionPixelSize(R.dimen.attch_photo_item_size);
                    Bitmap bmp = PhotoPickerUtils.shareInstance(getContext()).getBitmapResize(path, s, s);
                    imgPhoto.setImageBitmap(bmp);
                }
            }

            @OnClick(R.id.imgPhoto)
            void onPhotoClick() {
                Logger.sout("photo click");
            }

            @OnClick(R.id.imgClose)
            void onCloseClick() {
                //Logger.sout("close click");
                data.remove(pos);
                adapter.notifyDataSetChanged();
                updatePhotoNumber();
            }

        }

    }

    private Boolean saveFile(Bitmap bitmap, String filename){
        try {
            FileOutputStream out = null;
            try {
                out = new FileOutputStream(filename);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
                // PNG is a lossless format, the compression factor (100) is ignored
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return true;
            }
        }catch (Exception e){
            return false;
        }
    }

}
