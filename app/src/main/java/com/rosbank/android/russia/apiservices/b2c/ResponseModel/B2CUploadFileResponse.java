package com.rosbank.android.russia.apiservices.b2c.ResponseModel;

import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.google.gson.annotations.Expose;

import java.util.List;


public class B2CUploadFileResponse
        extends B2CBaseResponse {
    @Expose
    public List<String> UploadedFiles;

    public List<String> getUploadedFiles() {
        return UploadedFiles;
    }

    public void setUploadedFiles(List<String> uploadedFiles) {
        UploadedFiles = uploadedFiles;
    }
}
