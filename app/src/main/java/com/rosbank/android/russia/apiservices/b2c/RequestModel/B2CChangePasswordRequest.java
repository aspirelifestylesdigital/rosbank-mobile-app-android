package com.rosbank.android.russia.apiservices.b2c.RequestModel;

import com.rosbank.android.russia.BuildConfig;
import com.rosbank.android.russia.apiservices.RequestModel.BaseRequest;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.model.UserItem;
import com.rosbank.android.russia.utils.SharedPreferencesUtils;
import com.google.gson.annotations.Expose;

/**
 * Created by ThuNguyen on 10/26/2016.
 */

public class B2CChangePasswordRequest extends BaseRequest{
    @Expose
    private String ConsumerKey;
    @Expose
    private String Email2;
    @Expose
    private String Functionality;
    @Expose
    private String NewPassword;
    @Expose
    private String OldPassword;

    public B2CChangePasswordRequest(){
        //ConsumerKey = BuildConfig.B2C_CONSUMER_KEY;
        ConsumerKey = UserItem.isVip() ? BuildConfig.B2C_CONSUMER_KEY_PRIVATE : BuildConfig.B2C_CONSUMER_KEY_REGULAR;
        Email2 = SharedPreferencesUtils.getPreferences(AppConstant.USER_EMAIL_PRE, "");
        Functionality = "ChangePassword";

    }
    public B2CChangePasswordRequest build(String newPassword, String oldPassword){
        this.NewPassword = newPassword;
        this.OldPassword = oldPassword;
        return this;
    }

}
