package com.rosbank.android.russia.model;

import com.google.gson.annotations.Expose;

import java.util.List;

/**
 * Created by nga.nguyent on 9/22/2016.
 */
public class GourmetResponeData {
    @Expose
    private int TotalRecords;
    @Expose
    private int CurrentPage;
    @Expose
    private int NbOfRecordsReturn;
    @Expose
    private List<Restaurant> ListData;

    public int getTotalRecords() {
        return TotalRecords;
    }

    public void setTotalRecords(int totalRecords) {
        TotalRecords = totalRecords;
    }

    public int getCurrentPage() {
        return CurrentPage;
    }

    public void setCurrentPage(int currentPage) {
        CurrentPage = currentPage;
    }

    public int getNbOfRecordsReturn() {
        return NbOfRecordsReturn;
    }

    public void setNbOfRecordsReturn(int nbOfRecordsReturn) {
        NbOfRecordsReturn = nbOfRecordsReturn;
    }

    public List<Restaurant> getListData() {
        return ListData;
    }

    public void setListData(List<Restaurant> listData) {
        ListData = listData;
    }
}
