package com.rosbank.android.russia.processing.cancelrequest;

import android.text.TextUtils;
import android.view.View;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.apiservices.ResponseModel.CarTransferBookingDetailResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.rosbank.android.russia.apiservices.b2c.B2CWSGetRequestDetail;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.B2CUpsertConciergeRequestRequest;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CGetRecentRequestResponse;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.application.AppContext;
import com.rosbank.android.russia.model.concierge.CarBookingDetailData;
import com.rosbank.android.russia.utils.DateTimeUtil;
import com.rosbank.android.russia.utils.SharedPreferencesUtils;
import com.rosbank.android.russia.utils.StringUtil;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by ThuNguyen on 10/26/2016.
 */

public class CarTransferCancelRequestDetail extends BaseCancelRequestDetail{
    CarBookingDetailData carTransferBookingDetailData;

    public CarTransferCancelRequestDetail(){}
    public CarTransferCancelRequestDetail(View view) {
        super(view);
    }

    @Override
    protected void updateUI() {
        super.updateUI();
        long epochCreatedDate = carTransferBookingDetailData.getCreateDateEpoc();
        tvRequestedDate.setText(DateTimeUtil.shareInstance().formatTimeStamp(epochCreatedDate, "dd MMMM yyyy"));
        tvRequestedTime.setText(DateTimeUtil.shareInstance().formatTimeStamp(epochCreatedDate, "hh:mm aa"));
        // Request type
       // tvRequestType.setText(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_title_car_rental_request));

        // Request name
       // tvRequestName.setText(myRequestObject.getItemTitle());
        //tvRequestName.setText(myRequestObject.getReservationName());

        // Request detail header
        //tvRequestDetailHeader.setText(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_car_rental_request_detail));
        tvRequestType.setText(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_title_transfer_request));

        // Request name
        tvRequestName.setText(myRequestObject.getReservationName());

        // Request detail header
        tvRequestDetailHeader.setText(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_transfer_request_detail));

        if(carTransferBookingDetailData != null) {
            // Case id
            tvCaseId.setText(carTransferBookingDetailData.getBookingItemID());

            // Pick up location
            String pickupLocation = carTransferBookingDetailData.getPickupLocation();
            if(TextUtils.isEmpty(pickupLocation) || pickupLocation.equalsIgnoreCase("null")){
                //addRequestDetailItem("Pick up Location", "N.A");
            }else{
                addRequestDetailItem(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_pick_up_location_transfer), pickupLocation);
            }
            // Drop off location
            String dropOffLocation = carTransferBookingDetailData.getDropOffLocation();
            if(TextUtils.isEmpty(dropOffLocation) || dropOffLocation.equalsIgnoreCase("null")){
                //addRequestDetailItem("Drop off Location", "N.A");
            }else{
                addRequestDetailItem(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_drop_off_location_transfer), dropOffLocation);
            }
        }
    }

    @Override
    public void getRequestDetail() {
        showProgressDialog();
        B2CWSGetRequestDetail b2CWSGetRequestDetail = new B2CWSGetRequestDetail(this);
        b2CWSGetRequestDetail.setValue(myRequestObject.getEpcCaseId(), myRequestObject.getBookingItemID());
        b2CWSGetRequestDetail.run(null);
        /*WSGetCarTransferBookingDetail carTransferBookingDetail = new WSGetCarTransferBookingDetail();
        carTransferBookingDetail.setBookingId(myRequestObject.getBookingId());
        carTransferBookingDetail.run(this);
        showProgressDialog();*/
    }

    @Override
    public B2CUpsertConciergeRequestRequest getB2CUpsertConciergeRequestRequest() {
        B2CUpsertConciergeRequestRequest  upsertConciergeRequestRequest = new B2CUpsertConciergeRequestRequest();
        upsertConciergeRequestRequest.setFunctionality(AppConstant.CONCIERGE_FUNCTIONALITY_TYPE.CAR_TRANSFER.getValue());
        upsertConciergeRequestRequest.setRequestType(AppConstant.CONCIERGE_REQUEST_TYPE.T_CAR_TRANSFER.getValue());
        upsertConciergeRequestRequest.setEmail1((SharedPreferencesUtils.getPreferences(AppConstant.USER_EMAIL_PRE, "")));
        upsertConciergeRequestRequest.setSituation("carRental");
        if(!TextUtils.isEmpty(carTransferBookingDetailData.getMobileNumber())) {
            upsertConciergeRequestRequest.setPhoneNumber(carTransferBookingDetailData.getMobileNumber());
        }
        upsertConciergeRequestRequest.setPrefResponse(carTransferBookingDetailData.getPrefResponse());

        // Pickup date
        if(!TextUtils.isEmpty(carTransferBookingDetailData.getPickupDate())) {
            upsertConciergeRequestRequest.setPickupDate(carTransferBookingDetailData.getPickupDate());
            upsertConciergeRequestRequest.setPickUp(carTransferBookingDetailData.getPickupDate());
        }
        if(!TextUtils.isEmpty(carTransferBookingDetailData.getPickupDate())) {
            upsertConciergeRequestRequest.setPickupDate(carTransferBookingDetailData.getPickupDate());
        }


        // Number of Passenger
        upsertConciergeRequestRequest.setNumberOfAdults(String.valueOf(carTransferBookingDetailData.getNumberOfAdults()));


        if(myRequestObject != null){
            upsertConciergeRequestRequest.setTransactionID(myRequestObject.getBookingItemID());
        }
        // Request details
        upsertConciergeRequestRequest.setRequestDetails(carTransferBookingDetailData.getRequestDetaiString());

        return upsertConciergeRequestRequest;
    }
    private String combineRequestDetails(){
        String requestDetails = "";
        if(!TextUtils.isEmpty(carTransferBookingDetailData.getConciergeRequestDetail()
                                                  .getSpecialRequirement())){
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_SPECIAL_REQ + carTransferBookingDetailData.getConciergeRequestDetail()
                                                                                                       .getSpecialRequirement();
        }
        if(!TextUtils.isEmpty(carTransferBookingDetailData.getConciergeRequestDetail().getNoofPassengers())){
            if(!TextUtils.isEmpty(requestDetails)){
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_NUMBER_OF_PASSENGERS+ carTransferBookingDetailData.getConciergeRequestDetail().getNoofPassengers();
        }
        if(!TextUtils.isEmpty(carTransferBookingDetailData.getReservationName())){
            if(!TextUtils.isEmpty(requestDetails)){
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_RESERVATION_NAME+ carTransferBookingDetailData.getReservationName();
        }
        // Pickup location
        if(!StringUtil.isEmpty(requestDetails)){
            requestDetails += " | ";
        }
        requestDetails += AppConstant.B2C_REQUEST_DETAIL_PICK_LOCATION + carTransferBookingDetailData.getPickupLocation();

        // Drop-off location
        requestDetails += " | " + AppConstant.B2C_REQUEST_DETAIL_DROP_LOCATION + carTransferBookingDetailData.getDropOffLocation();

        // Transport type
        requestDetails += " | " + AppConstant.B2C_REQUEST_DETAIL_PREF_TRANSPORT_TYPE + carTransferBookingDetailData.getTransportType();
        return requestDetails;
    }
    @Override
    public void onResponse(Call call, Response response) {
        hideProgressDialog();
        if(response != null && response.body() instanceof CarTransferBookingDetailResponse){
            //carTransferBookingDetailData = ((CarTransferBookingDetailResponse)response.body()).getData();
            updateUI();
        }
    }

    @Override
    public void onFailure(Call call, Throwable t) {
        hideProgressDialog();
    }
    @Override
    public void onB2CResponse(final B2CBaseResponse response) {
        hideProgressDialog();
        if(response instanceof B2CGetRecentRequestResponse){
           carTransferBookingDetailData = new CarBookingDetailData((B2CGetRecentRequestResponse)response);
            updateUI();
        }

    }

    @Override
    public void onB2CResponseOnList(final List<B2CBaseResponse> responseList) {

    }

    @Override
    public void onB2CFailure(final String errorMessage,
                             final String errorCode) {
        hideProgressDialog();

    }
}
