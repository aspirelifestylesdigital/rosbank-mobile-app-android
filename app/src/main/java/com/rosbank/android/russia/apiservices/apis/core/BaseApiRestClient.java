package com.rosbank.android.russia.apiservices.apis.core;

import android.text.TextUtils;

import com.rosbank.android.russia.apiservices.ResponseModel.AuthenticateResponse;
import com.rosbank.android.russia.utils.Logger;
import com.rosbank.android.russia.utils.RemoveNullStringFactory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import okhttp3.CipherSuite;
import okhttp3.ConnectionSpec;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.TlsVersion;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public abstract class BaseApiRestClient<T>/* implements Callback<T>*/ {

    /*public interface ApiCallback{
        public void onNoInternetConnection();
        public void onComplete(BaseResponse response);
        public void onFailed(BaseErrorResponse error);
        public void onAuthenticateFailed(BaseErrorResponse error);
    }*/

    protected static final String TAG = "API";
    public static final int HTTP_CONNECT_TIMEOUT = 60;
    public static final int HTTP_READ_TIMEOUT = 180;

    protected Gson sGson;
    protected OkHttpClient sOkHttpClient;
    protected Retrofit rtfAdapter;
    protected List<BaseApiRestClient> mListRequests;
    //protected ApiCallback apiCallback;
    protected Callback<T> callback;
    protected String originalMessageErrorWhenFailed;
    protected String messageErrorWhenFailed;
    protected String errorCodeWhenFailed;
    /**
     * Define: abstract method
     * */
    //TODO: init restadapter for each API system with difference base url
    protected abstract void initRestAdapter();
    protected abstract boolean isAuthenticateValidate();
    protected abstract void runSignAuthenticate();
    protected abstract void authenticateSuccess(AuthenticateResponse token);
    protected abstract String getAuthorizeToken();

    //client implement
    public abstract void run(Callback<T> cb);
    protected abstract boolean isUseAuthenticateInApi();
    protected abstract void runApi();
    protected abstract Map<String, String> prepareHeaders();

    public BaseApiRestClient() {
        mListRequests = new ArrayList<>();

        /*synchronized (this) {
            initParser();
        }*/

        initParser();
        initRestAdapter();
    }

    protected void initParser() {
        sGson = new GsonBuilder()
                .setDateFormat(DateFormat.FULL, DateFormat.FULL)
                .registerTypeAdapter(Date.class, new JsonSerializer<Date>() {
                    @Override
                    public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext
                            context) {
                        return src == null ? null : new JsonPrimitive(src.getTime() / 1000);
                    }
                })
                .registerTypeAdapterFactory(new RemoveNullStringFactory())
                .excludeFieldsWithoutExposeAnnotation()
                //.serializeNulls()  //Turn it on when using staging server
                //.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();

        sOkHttpClient = configureClient();
        if (sOkHttpClient == null) {
            sOkHttpClient = createBaseHttpClient()
                            .build();
        }

    }

    private OkHttpClient.Builder createBaseHttpClient(){
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .connectTimeout(HTTP_CONNECT_TIMEOUT, TimeUnit.SECONDS)//connect timeout
                .readTimeout(HTTP_READ_TIMEOUT, TimeUnit.SECONDS)//read timeout
                ;
        return builder;
    }

    private OkHttpClient configureClient() {
         /*final TrustManager[] certs = new TrustManager[]{new X509TrustManager() {

            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }

            @Override
            public void checkServerTrusted(final X509Certificate[] chain,
                                           final String authType) throws
                                                                  CertificateException {

            }

            @Override
            public void checkClientTrusted(final X509Certificate[] chain,
                                           final String authType) throws CertificateException {
            }
        }};

        SSLContext ctx = null;
        try {
            ctx = SSLContext.getInstance("TLS");
            ctx.init(null, certs, new SecureRandom());
        } catch (final java.security.GeneralSecurityException ex) {
            Logger.e(TAG, ex.getMessage());
        }*/
        try {
            /*TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            trustManagerFactory.init((KeyStore) null);
            TrustManager[] trustManagers = trustManagerFactory.getTrustManagers();
            if (trustManagers.length != 1 || !(trustManagers[0] instanceof X509TrustManager)) {
                throw new IllegalStateException("Unexpected default trust managers:" + Arrays.toString(trustManagers));
            }
            X509TrustManager trustManager = (X509TrustManager) trustManagers[0];
            okHttpBuilder.sslSocketFactory(new TLSSocketFactory(), trustManager);*/
//            ConnectionSpec spec = new ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
//                    .tlsVersions(TlsVersion.TLS_1_1, TlsVersion.TLS_1_2, TlsVersion.TLS_1_0)
//                    .cipherSuites(
//                            CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
//                            CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
//                            CipherSuite.TLS_DHE_RSA_WITH_AES_128_GCM_SHA256,
//                            CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA,
//                            CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256)
//                    .build();
//            TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(
//                    TrustManagerFactory.getDefaultAlgorithm());
//            trustManagerFactory.init((KeyStore) null);
//            TrustManager[] trustManagers = trustManagerFactory.getTrustManagers();
//            if (trustManagers.length != 1 || !(trustManagers[0] instanceof X509TrustManager)) {
//                throw new IllegalStateException("Unexpected default trust managers:"
//                        + Arrays.toString(trustManagers));
//            }
//            X509TrustManager trustManager = (X509TrustManager) trustManagers[0];

//            final HostnameVerifier hostnameVerifier = new HostnameVerifier() {
//                @Override
//                public boolean verify(final String hostname,
//                                      final SSLSession session) {
//                    return true;
//                }
//            };
            HttpLoggingInterceptor logInterceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
                @Override
                public void log(String message) {
                    if(!TextUtils.isEmpty(message) && message.contains("\"message1\"")){
                        originalMessageErrorWhenFailed = message;
                    }
                    Logger.sout(message);
                }

            });
            logInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            // Define the interceptor, add authentication headers
            Interceptor interceptor = new Interceptor() {
                @Override
                public okhttp3.Response intercept(Chain chain) throws IOException {
                    //add header default
                    Request.Builder builder = chain.request().newBuilder();
                    //builder.addHeader("User-Agent", "Retrofit-Sample-App");
                    if(isUseAuthenticateInApi()){
                        builder.addHeader("Authorization", getAuthorizeToken());
                    }

                    //get header from client
                    final Map<String, String> headers = prepareHeaders();

                    if ((headers != null) && !headers.isEmpty()) {
                        for (final Map.Entry<String, String> item : headers.entrySet()) {
                            builder.addHeader(item.getKey(), item.getValue());
                        }
                    }

                    Request newRequest = builder.build();
                    return chain.proceed(newRequest);
                }
            };
//
//            SSLContext sslContext = SSLContext.getInstance("TLS");
//            sslContext.init(null, new TrustManager[] { trustManager }, null);
//            OkHttpClient.Builder builder = createBaseHttpClient();
//            builder.hostnameVerifier(hostnameVerifier)
//                    .sslSocketFactory(ctx.getSocketFactory())
//                    .addInterceptor(logInterceptor)//log
//                    .addInterceptor(interceptor)//header
//            ;
            OkHttpClient.Builder builder = createBaseHttpClient();
            OkHttpClient client  = builder
                  //  .sslSocketFactory(new TLSSocketFactory(), trustManager)
                  //  .connectionSpecs(Collections.singletonList(spec))
                    .protocols(Arrays.asList(Protocol.HTTP_1_1))
                    .addInterceptor(logInterceptor)//log
                    .addInterceptor(interceptor)
                    .hostnameVerifier(new HostnameVerifier() {
                        @Override
                        public boolean verify(String hostname, SSLSession session) {
                            return true;
                        }
                    })
                    .build();
            return client;
            //return okHttpClient;
        }
        catch ( Exception e ) {

        }
//        catch (KeyManagementException e) {
//            e.printStackTrace();
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        } catch (KeyStoreException e) {
//            e.printStackTrace();
//        }
        return null;
    }

    protected void checkAuthenticateAndRun(){
        if(isUseAuthenticateInApi()){
            synchronized (mListRequests) {
                if ( !isAuthenticateValidate() ) {
                    if (mListRequests.size()==0) {
                        mListRequests.add(this);
                        runSignAuthenticate();
                    } else {
                        mListRequests.add(this);
                    }
                } else {
                    runApi();
                }
            }
        }
        else{
            runApi();
        }
    }

    /**
     * send callback runSignAuthenticate error to implement class
     * */
    protected void postAuthenticateSuccess() {
        synchronized (mListRequests) {
            while (mListRequests.size()>0){
                BaseApiRestClient api = mListRequests.get(0);
                //
                mListRequests.remove(0);
                //recall api.
                api.run(api.callback);
            }
        }
    }

    protected void postAuthenticateError(Call<AuthenticateResponse> call, Response<AuthenticateResponse> response) {
        synchronized (mListRequests) {
            while (mListRequests.size()>0){
                mListRequests.get(0).callback.onResponse(call, response);
                mListRequests.remove(0);
            }
        }
    }

    protected void postAuthenticateError(Call<AuthenticateResponse> call, Throwable t) {
        synchronized (mListRequests) {
            while (mListRequests.size()>0){
                mListRequests.get(0).callback.onFailure(call, t);
                mListRequests.remove(0);
            }
        }
    }
    public String getMessageErrorFromLog(){
        if(!TextUtils.isEmpty(messageErrorWhenFailed)){
            return messageErrorWhenFailed;
        }
        if(!TextUtils.isEmpty(originalMessageErrorWhenFailed)){
            try {
                JSONObject jsonObject = new JSONObject(originalMessageErrorWhenFailed);
                if(jsonObject != null){
                    JSONArray messageJsonArr = jsonObject.optJSONArray("message");
                    if(messageJsonArr != null && messageJsonArr.length() > 0){
                        JSONObject messageJsonObj = messageJsonArr.getJSONObject(0);
                        messageErrorWhenFailed = messageJsonObj.optString("message");
                        errorCodeWhenFailed = messageJsonObj.optString("code");
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return messageErrorWhenFailed;
    }
    public String getErrorCodeFromLog(){
        return errorCodeWhenFailed;
    }
    /**
     * ============================================================
     * RETROFIT CALLBACK
     * */
    /*@Override
    public void onResponse(Call<T> call, Response<T> response) {
        int code = response.code();
        if (code == 200){
            BaseResponse result = null;
            if(response.body() != null) {
                result = (BaseResponse) response.body();
            }
            else{
                result = new BaseResponse();
                result.setStatus(code);
                result.setMessage(BaseResponse.BODY_NULL);
            }
            //
            postResponse(result);

        } else {
            BaseResponse result = new BaseResponse();
            if(response.body()!=null){
                result = (BaseResponse) response.body();
            }
            else if(response.errorBody()!=null){
                try {
                    Converter<ResponseBody, BaseResponse> errorConverter =
                            rtfAdapter.responseBodyConverter(BaseResponse.class, new Annotation[0]);
                    result = errorConverter.convert(response.errorBody());
                } catch (Exception e) {
                    Logger.e(TAG, e.getMessage1());
                }
            }

            if(result==null){
                result = new BaseResponse();
                result.setStatus(code);
                result.setMessage(BaseResponse.BODY_NULL);
            }

            //
            postResponse(result);
        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        //error like no internet connection.
        BaseErrorResponse error = new BaseErrorResponse();
        error.setStatus(400);
        error.setMessage(t.getMessage1());
        error.setThrowable(t);
        //
        postError(error);
    }

    *//**
     * send callback api success to implement class
     * *//*
    protected void postResponse(BaseResponse object) {
        //EventBus.getDefault().post(object);
        if(apiCallback!=null){
            apiCallback.onComplete(object);
        }
    }

    protected void postError(BaseErrorResponse response) {
        //EventBus.getDefault().post(object);
        if(apiCallback!=null){
            apiCallback.onFailed(response);
        }
    }

    protected void postNoInternet() {
        //EventBus.getDefault().post(object);
        if(apiCallback!=null){
            apiCallback.onNoInternetConnection();
        }
    }*/
    /**
     * END RETROFIT CALLBACK
     * ============================================================
     * */
}
