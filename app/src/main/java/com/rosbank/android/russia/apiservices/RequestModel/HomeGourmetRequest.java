package com.rosbank.android.russia.apiservices.RequestModel;


import com.google.gson.annotations.Expose;

public class HomeGourmetRequest
        extends BaseRequest {
    @Expose
    private String UserID;
    @Expose
    private int Page;
    @Expose
    private int RecordPerPage;

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public int getPage() {
        return Page;
    }

    public void setPage(int page) {
        Page = page;
    }

    public int getRecordPerPage() {
        return RecordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        RecordPerPage = recordPerPage;
    }
}