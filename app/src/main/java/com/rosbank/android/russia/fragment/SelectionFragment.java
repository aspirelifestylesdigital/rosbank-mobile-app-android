package com.rosbank.android.russia.fragment;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.activitys.HomeActivity;
import com.rosbank.android.russia.adapter.SelectionAdapter;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.FontUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by anh.trinh on 9/13/2016.
 */
public class SelectionFragment
        extends BaseFragment {
    @BindView(R.id.header)
    View mToolbar;
    @BindView(R.id.search_country)
    SearchView mSearchView;

    public interface SelectionCallback {
        void onFinishSelection(Bundle bundle);
    }

    public static final String PRE_SELECTION_DATA = "selection_data";
    public static final String PRE_SELECTION_TYPE = "selection_type";
    public static final String PRE_SELECTION_SPECIFIC_STATE = "selection_state";

    public static final String PRE_SELECTION_SALUTATION = "SALUTATION";
    public static final String PRE_SELECTION_COUNTRY_CODE = "COUNTRY CODE";
    public static final String PRE_SELECTION_OCCASION = "OCCASION";
    public static final String PRE_SELECTION_CUISINE = "CUISINE";
    public static final String PRE_SELECTION_EVENT_CATEGORY = "EVENT CATEGORY";
    public static final String PRE_SELECTION_STATE = "STATE";

    @BindView(R.id.ic_back)
    ImageView mIcBack;
    @BindView(R.id.tv_tool_bar_title)
    TextView mTvToolBarTitle;
    @BindView(R.id.listview)
    ListView mListview;
    String typeSelection;
    ArrayList<String> data = new ArrayList<String>();
    SelectionAdapter adapter;
    String dataSelected = "";
    String specificStateSelected = "";

    SelectionCallback selectionCallback;

    public void setSelectionCallBack(SelectionCallback selectionCallBack) {
        this.selectionCallback = selectionCallBack;
    }


    @Override
    protected int layoutId() {
        return R.layout.fragment_selection;
    }

    @Override
    protected void initView() {
        CommonUtils.setFontForViewRecursive(mTvToolBarTitle,
                FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        mListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> adapterView,
                                    final View view,
                                    final int i,
                                    final long l) {
                if (selectionCallback != null) {
                    Bundle bundle = new Bundle();
                    bundle.putString(PRE_SELECTION_TYPE,
                            typeSelection);
                    if(!data.get(i).equals(dataSelected)) {
                    bundle.putString(PRE_SELECTION_DATA,
                            data.get(i));
                    }
                    selectionCallback.onFinishSelection(bundle);
                }
                onBackPress();

            }
        });
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(final String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(final String newText) {
                ImageView searchViewIcon = (ImageView)mSearchView.findViewById(android.support.v7.appcompat.R.id.search_mag_icon);
                searchViewIcon.setVisibility(TextUtils.isEmpty(newText) ? View.VISIBLE : View.GONE);
                adapter.getFilter().filter(newText);
                return false;
            }
        });
        mSearchView.setIconifiedByDefault(false);
        CommonUtils.clearFocusLineSearchView(mSearchView);
    }

    @Override
    protected void bindData() {
        if (getArguments() != null) {
            typeSelection = getArguments().getString(PRE_SELECTION_TYPE,
                    "");
            dataSelected = getArguments().getString(PRE_SELECTION_DATA,
                    "");
            specificStateSelected = getArguments().getString(PRE_SELECTION_SPECIFIC_STATE, "");

            String[] resource;
            if (typeSelection.equalsIgnoreCase(PRE_SELECTION_SALUTATION)) {
                mSearchView.setVisibility(View.GONE);
                mTvToolBarTitle.setText(getString(R.string.text_title_salutation));
                if (getActivity() instanceof HomeActivity) {
                    ((HomeActivity) getActivity()).setTitle(getString(R.string.text_title_salutation));

                }
                resource = getResources().getStringArray(R.array.salutation_arrays);
                data.clear();
                for (String s1 : resource) {
                    data.add(s1);
                }
            } else if (typeSelection.equalsIgnoreCase(PRE_SELECTION_COUNTRY_CODE)) {
                mSearchView.setVisibility(View.VISIBLE);
                mSearchView.setQueryHint(getString(R.string.enter_country_name_query_hint));
                mTvToolBarTitle.setText(getString(R.string.text_title_country_code));
                if (getActivity() instanceof HomeActivity) {
                    ((HomeActivity) getActivity()).setTitle(getString(R.string.text_title_country_code));

                }
                resource = getResources().getStringArray(R.array.country_arrays);
                data.clear();
                for (String s1 : resource) {
                    data.add(s1);
                }
            } else if(typeSelection.equalsIgnoreCase(PRE_SELECTION_OCCASION)){
                mSearchView.setVisibility(View.GONE);
                mTvToolBarTitle.setText(getString(R.string.occasion_text));
                if (getActivity() instanceof HomeActivity) {
                    ((HomeActivity) getActivity()).setTitle(getString(R.string.occasion_text));

                }
                resource = getResources().getStringArray(R.array.occasion_array);
                data.clear();
                for (String s1 : resource) {
                    data.add(s1);
                }
            }else if(typeSelection.equalsIgnoreCase(PRE_SELECTION_CUISINE)) {
                mSearchView.setVisibility(View.GONE);
                mTvToolBarTitle.setText(getString(R.string.cuisine_text));
                if (getActivity() instanceof HomeActivity) {
                    ((HomeActivity) getActivity()).setTitle(getString(R.string.cuisine_text));

                }
                resource = getResources().getStringArray(R.array.cuisine_array);
                data.clear();
                for (String s1 : resource) {
                    data.add(s1);
                }
            }else if(typeSelection.equalsIgnoreCase(PRE_SELECTION_EVENT_CATEGORY)) {
                mSearchView.setVisibility(View.GONE);
                mTvToolBarTitle.setText(getString(R.string.text_event_category));
                if (getActivity() instanceof HomeActivity) {
                    ((HomeActivity) getActivity()).setTitle(getString(R.string.text_event_category));

                }
                resource = getResources().getStringArray(R.array.event_category_array);
                data.clear();
                for (String s1 : resource) {
                    data.add(s1);
                }
            }else if(typeSelection.equalsIgnoreCase(PRE_SELECTION_STATE)) {
                mSearchView.setVisibility(View.VISIBLE);
                mSearchView.setQueryHint(getString(R.string.enter_state_name_query_hint));
                mTvToolBarTitle.setText(getString(R.string.text_state));
                if (getActivity() instanceof HomeActivity) {
                    ((HomeActivity) getActivity()).setTitle(getString(R.string.text_state));
                }

                resource = getResources().getStringArray(R.array.us_states_array);
                if(specificStateSelected != null && specificStateSelected.equalsIgnoreCase("can")){
                    resource = getResources().getStringArray(R.array.canada_states_array);
                }
                data.clear();
                for (String s1 : resource) {
                    data.add(s1);
                }
            }

        }
        adapter = new SelectionAdapter(getActivity(),
                data,
                dataSelected);
        mListview.setAdapter(adapter);
    }

    @Override
    public boolean onBack() {
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showLogoApp(false);
            mToolbar.setVisibility(View.GONE);
            ((HomeActivity) getActivity()).hideToolbarMenuIcon();

        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showToolbarMenuIcon();
            mToolbar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater,
                container,
                savedInstanceState);
        ButterKnife.bind(this,
                rootView);
        return rootView;
    }

    @OnClick(R.id.ic_back)
    public void onClick() {
        onBackPress();
    }
}
