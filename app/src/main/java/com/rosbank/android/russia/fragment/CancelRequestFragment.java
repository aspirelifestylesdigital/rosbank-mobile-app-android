package com.rosbank.android.russia.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CICallback;
import com.rosbank.android.russia.apiservices.b2c.B2CWSUpsertConciergeRequest;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.B2CUpsertConciergeRequestRequest;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CGetRecentRequestResponse;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CUpsertConciergeRequestResponse;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.model.concierge.MyRequestObject;
import com.rosbank.android.russia.processing.cancelrequest.BaseCancelRequestDetail;
import com.rosbank.android.russia.processing.cancelrequest.CarRentalCancelRequestDetail;
import com.rosbank.android.russia.processing.cancelrequest.CarTransferCancelRequestDetail;
import com.rosbank.android.russia.processing.cancelrequest.EventCancelRequestDetail;
import com.rosbank.android.russia.processing.cancelrequest.EventRecommendCancelRequestDetail;
import com.rosbank.android.russia.processing.cancelrequest.GolfCancelRequestDetail;
import com.rosbank.android.russia.processing.cancelrequest.GourmetCancelRequestDetail;
import com.rosbank.android.russia.processing.cancelrequest.GourmetRecommendationCancelRequestDetail;
import com.rosbank.android.russia.processing.cancelrequest.HotelCancelRequestDetail;
import com.rosbank.android.russia.processing.cancelrequest.HotelRecommendationCancelRequestDetail;
import com.rosbank.android.russia.processing.cancelrequest.OtherCancelRequestDetail;
import com.rosbank.android.russia.utils.StringUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ThuNguyen on 10/25/2016.
 */

public class CancelRequestFragment extends BaseFragment implements Callback ,
                                                                   B2CICallback {

    public static final String REQUEST_ITEM_OBJECT = "request_item_object";
    private BaseCancelRequestDetail cancelRequestDetailProcessing;
    private B2CGetRecentRequestResponse requestDetail;
    private MyRequestObject requestObj;

    public static void openCancelRequest(BaseFragment baseFragment, MyRequestObject myRequestObject){
        Bundle bundleDetail = new Bundle();
        bundleDetail.putSerializable(REQUEST_ITEM_OBJECT, myRequestObject);

        CancelRequestFragment detailFr = new CancelRequestFragment();
        detailFr.setArguments(bundleDetail);
        baseFragment.pushFragment(detailFr, true, true);
    }
    @Override
    protected int layoutId() {
        return R.layout.fragment_cancel_request;
    }

    @Override
    protected void initView() {
        view.setVisibility(View.GONE);
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle(getString(R.string.text_title_cancel_request));
    }

    @Override
    protected void bindData() {
        requestObj =(MyRequestObject) getArguments().getSerializable(REQUEST_ITEM_OBJECT);

        if(requestObj != null && requestObj.getBookingRequestEnum() != null){
            switch (requestObj.getBookingRequestEnum()){
                case BOOK_HOTEL:
                case RECOMMEND_HOTEL:
                    if(requestObj.isBookNormalType()) {
                        cancelRequestDetailProcessing = new HotelCancelRequestDetail(view);
                    }else {
                        cancelRequestDetailProcessing = new HotelRecommendationCancelRequestDetail(view);
                    }
                    break;
                case RESERVE_TABLE:
                case RECOMMEND_RESTAURANT:
                    if(requestObj.isBookNormalType()) {
                        cancelRequestDetailProcessing = new GourmetCancelRequestDetail(view);
                    }else {
                        cancelRequestDetailProcessing = new GourmetRecommendationCancelRequestDetail(view);
                    }
                    break;
                case T_CAR_RENTAL:
                case T_CAR_TRANSFER:
                    if(requestObj.isBookNormalType()) {
                        cancelRequestDetailProcessing = new CarRentalCancelRequestDetail(view);
                    }else {
                        cancelRequestDetailProcessing = new CarTransferCancelRequestDetail(view);
                    }
                    break;
                case GOLF:
                    cancelRequestDetailProcessing = new GolfCancelRequestDetail(view);
                    break;
                case EVENT:
                case RECOMMEND_EVENT:
                    if(requestObj.isBookNormalType()){
                        cancelRequestDetailProcessing = new EventCancelRequestDetail(view);
                    }else{
                        cancelRequestDetailProcessing = new EventRecommendCancelRequestDetail(view);
                    }
                    break;
                case OTHER_REQUEST:
                case SPA:
                    cancelRequestDetailProcessing = new OtherCancelRequestDetail(view);
                    break;
            }
        }else{
            showDialogMessage(getString(R.string.text_title_dialog_error), getString(R.string.text_cancel_request_error_message), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    onBackPress();
                }
            }, new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                }
            });
        }
        if(cancelRequestDetailProcessing != null){
            cancelRequestDetailProcessing.setMyRequestObject(requestObj);
            cancelRequestDetailProcessing.setCancelRequestActionListener(new BaseCancelRequestDetail.ICancelRequestActionListener() {
                @Override
                public void onCancelRequestClick(B2CUpsertConciergeRequestRequest myRequestObject) {
                   processCancelRequest(myRequestObject);
                }

                @Override
                public void onCallConciergeClick(MyRequestObject myRequestObject) {

                }
            });
            cancelRequestDetailProcessing.getRequestDetail();
        }
    }

    @Override
    protected boolean onBack() {
        return false;
    }


    @Override
    public void onResponse(Call call, Response response) {
        hideDialogProgress();
        if(getActivity()==null) {
            return;
        }
        onBackPress();

        ThankYouFragment thankYouFragment = new ThankYouFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ThankYouFragment.LINE_BOLD, " ");
        bundle.putString(ThankYouFragment.LINE_MAIN, getString(R.string.text_message_thank_you_cancel_request));
        thankYouFragment.setArguments(bundle);
        pushFragment(thankYouFragment,
                true,
                true);
    }

    @Override
    public void onFailure(Call call, Throwable t) {
        hideDialogProgress();
        String msg = t.getMessage();
        if(StringUtil.isEmpty(msg)){
            msg = getString(R.string.api_failed_message);
        }
        showDialogMessage("", msg);
    }
    @Override
    public void onB2CResponse(B2CBaseResponse response) {
        hideDialogProgress();
        if(getActivity()==null) {
            return;
        }
        if(response instanceof B2CUpsertConciergeRequestResponse){
            if(response != null && response.isSuccess()) {
                EventBus.getDefault().post(AppConstant.CONCIERGE_EDIT_TYPE.CANCEL);
                hideDialogProgress();
                onBackPress();

                ThankYouFragment thankYouFragment = new ThankYouFragment();
                Bundle bundle = new Bundle();
                bundle.putString(ThankYouFragment.TYPE_CANCEL,"cancel");
                bundle.putString(ThankYouFragment.LINE_MAIN,
                                 getString(R.string.text_message_thank_you_cancel_request));
                thankYouFragment.setArguments(bundle);
                pushFragment(thankYouFragment,
                             true,
                             true);

            }else {
//                showToast(getString(R.string.text_server_error_message));
                showDialogMessage("", getString(R.string.text_concierge_request_error));

            }
        }else if(response instanceof B2CGetRecentRequestResponse){
            requestDetail = (B2CGetRecentRequestResponse)response;
        }
    }

    @Override
    public void onB2CResponseOnList(List<B2CBaseResponse> responseList) {
        if(getActivity()==null) {
            return;
        }
    }

    @Override
    public void onB2CFailure(String errorMessage, String errorCode) {
        hideDialogProgress();
        if(getActivity()==null) {
            return;
        }
//        showToast(getString(R.string.text_server_error_message));
        showDialogMessage("", getString(R.string.text_concierge_request_error));

    }
    private void processCancelRequest(B2CUpsertConciergeRequestRequest upsertConciergeRequestRequest){
        showDialogProgress();

        B2CWSUpsertConciergeRequest b2CWSUpsertConciergeRequest = new B2CWSUpsertConciergeRequest(AppConstant.CONCIERGE_EDIT_TYPE.CANCEL, this);
        b2CWSUpsertConciergeRequest.setRequest(upsertConciergeRequestRequest);
        b2CWSUpsertConciergeRequest.run(null);
    }
}
