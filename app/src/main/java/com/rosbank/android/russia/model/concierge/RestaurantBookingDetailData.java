package com.rosbank.android.russia.model.concierge;

import android.text.TextUtils;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CGetRecentRequestResponse;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.application.AppContext;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.DateTimeUtil;

/**
 * Created by ThuNguyen on 10/26/2016.
 */

public class RestaurantBookingDetailData extends MyRequestObject {
    private String NameOfRestaurant;
    private String ReservationDate;
    private int NumberOfAdults;
    private int NumberOfKids;
    private int NumberOfChildren;
    private String Cuisine;
    private String Occasion;
    private String MinPrice;
    private String MaxPrice;
    private long reservationDateEpoc;
    private String CreateDate;
    private long createDateEpoc;
    private String FoodAllergies;

    public RestaurantBookingDetailData(B2CGetRecentRequestResponse recentRequestResponse) {
        super(recentRequestResponse);
        ReservationDate = recentRequestResponse.getEVENTDATE();
        if(!TextUtils.isEmpty(conciergeRequestDetail.getSituation())){
            Occasion = conciergeRequestDetail.getSituation();
        }else{
            Occasion = recentRequestResponse.getSituation();
        }
        if(!TextUtils.isEmpty(recentRequestResponse.getNUMBEROFADULTS())) {
            try {
                NumberOfAdults = Integer.parseInt(recentRequestResponse.getNUMBEROFADULTS());
            }catch (Exception e){}

        }
        if(!TextUtils.isEmpty(recentRequestResponse.getNUMBEROFCHILDREN())) {
            try {
                NumberOfKids = Integer.parseInt(recentRequestResponse.getNUMBEROFCHILDREN());
                NumberOfChildren = Integer.parseInt(recentRequestResponse.getNUMBEROFCHILDREN());
            }catch (Exception e){}

        }
        CreateDate = recentRequestResponse.getCREATEDDATE();
        createDateEpoc = DateTimeUtil.shareInstance().convertToTimeStampFromGMT(CreateDate, AppConstant.DATE_FORMAT_YYYY_MM_DD_T_HH_MM_SS);

        // Booking history
        reservationDateEpoc = DateTimeUtil.shareInstance().convertToTimeStamp(ReservationDate, AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS);


        // Item title
        ItemTitle = conciergeRequestDetail.getRestaurantName();
        if(TextUtils.isEmpty(ItemTitle)){
            ItemTitle = AppContext.getSharedInstance().getResources().getString(R.string.text_get_dinning_recommedation);
        }
        // Item description
        ItemDescription = AppContext.getSharedInstance().getResources().getString(R.string.text_booking_detail_reservation_date) +
                DateTimeUtil.shareInstance().convertTime(ReservationDate, AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS, AppContext.getSharedInstance().getResources().getString(R.string.text_my_request_reservation_date_format));
    }

    @Override
    protected void initRequestProperties() {
        // Mapping properties
        NameOfRestaurant = conciergeRequestDetail.getRestaurantName();
        Cuisine = conciergeRequestDetail.getCuisine();
        MinPrice = conciergeRequestDetail.getMinPrice();
        MaxPrice = conciergeRequestDetail.getMaxPrice();
        FoodAllergies = conciergeRequestDetail.getFoodAllergies();
        // Booking type group
        BookingFilterGroupType = AppConstant.BOOKING_FILTER_TYPE.Restaurant;

    }

    @Override
    public boolean isBookNormalType() {
        return !TextUtils.isEmpty(NameOfRestaurant);
    }

    @Override
    public long getRequestStartDateEpoch() {
        return reservationDateEpoc;
    }

    @Override
    public long getRequestEndDateEpoch() {
        return 0;
    }

    public long getReservationDateEpoc(){
        return reservationDateEpoc;
    }
    public String getNameOfRestaurant() {
        return NameOfRestaurant;
    }

    public void setNameOfRestaurant(String nameOfRestaurant) {
        NameOfRestaurant = nameOfRestaurant;
    }

    public String getReservationDate() {
        return ReservationDate;
    }

    public void setReservationDate(String reservationDate) {
        ReservationDate = reservationDate;
    }

    public String getReservationName() {
        if(CommonUtils.isStringValid(getFullName())){
            return getFullName();
        }else{
            return conciergeRequestDetail.getReservationName();
        }

    }

    public int getNumberOfAdults() {
        return NumberOfAdults;
    }

    public void setNumberOfAdults(int numberOfAdults) {
        NumberOfAdults = numberOfAdults;
    }

    public int getNumberOfKids() {
        return NumberOfKids;
    }

    public void setNumberOfKids(int numberOfKids) {
        NumberOfKids = numberOfKids;
    }

    public int getNumberOfChildren() {
        return NumberOfChildren;
    }

    public void setNumberOfChildren(int numberOfChildren) {
        NumberOfChildren = numberOfChildren;
    }

    public String getMinPrice() {
        return MinPrice;
    }

    public void setMinPrice(String minPrice) {
        MinPrice = minPrice;
    }

    public String getMaxPrice() {
        if(!CommonUtils.isStringValid(MaxPrice)&& conciergeRequestDetail!=null && conciergeRequestDetail.getBudgetRangeCurrencyperperson()!=null){
            MaxPrice = conciergeRequestDetail.getBudgetRangeCurrencyperperson();
        }
        return MaxPrice;
    }

    public void setMaxPrice(String maxPrice) {
        MaxPrice = maxPrice;
    }

    public String getCuisine() {
        if(!CommonUtils.isStringValid(Cuisine) && conciergeRequestDetail!=null && conciergeRequestDetail.getTypeofCuisine()!=null){
            Cuisine = conciergeRequestDetail.getTypeofCuisine();
        }
        return Cuisine;
    }

    public void setCuisine(String cuisine) {
        Cuisine = cuisine;
    }

    public String getOccasion() {
        if(!CommonUtils.isStringValid(Occasion)&& conciergeRequestDetail!=null && conciergeRequestDetail.getIfpreferredrestaurantisnotavailableonselecteddatetime()!=null){
            Occasion = conciergeRequestDetail.getIfpreferredrestaurantisnotavailableonselecteddatetime();
        }
        if(!CommonUtils.isStringValid(Occasion) && conciergeRequestDetail!=null && conciergeRequestDetail.getAmbianceOccasion()!=null){
            Occasion = conciergeRequestDetail.getAmbianceOccasion();
        }
        return Occasion;
    }

    public void setOccasion(String occasion) {
        Occasion = occasion;
    }
    public long getCreateDateEpoc() {
        return createDateEpoc;
    }

    public void setFoodAllergies(String foodAllergies) {
        FoodAllergies = foodAllergies;
    }

    public String getFoodAllergies() {
        return FoodAllergies;
    }
}
