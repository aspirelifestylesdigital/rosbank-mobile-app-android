package com.rosbank.android.russia.widgets;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/*
 * Created by anh.trinh on 03/28/2017.
 */
public class ViewPagerSpalshView
        extends ViewPager {
    float mStartDragX;
    Context context;
    public static boolean enabled;

    public ViewPagerSpalshView(Context context) {
        super(context);
    }

    public ViewPagerSpalshView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
            return super.onTouchEvent(event);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev)
    {
            float x = ev.getX();
            switch (ev.getAction())
            {
                case MotionEvent.ACTION_DOWN:
                    mStartDragX = x;

                    break;
                case MotionEvent.ACTION_MOVE:
                    if (mStartDragX < x - 50)//100 value velocity
                    {
                        if(getCurrentItem()%3>0) {
                            //Left scroll
                            return super.onInterceptTouchEvent(ev);
                        }else{
                            return false;
                        }
                    }
                    else
                    if (mStartDragX > x + 50)
                    {
                        //Right scroll
                        return super.onInterceptTouchEvent(ev);
                    }
                    break;
            }


        return  super.onInterceptTouchEvent(ev);

    }

}
