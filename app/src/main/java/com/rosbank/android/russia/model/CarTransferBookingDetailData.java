package com.rosbank.android.russia.model;

import com.google.gson.annotations.Expose;

/**
 * Created by ThuNguyen on 10/26/2016.
 */

public class CarTransferBookingDetailData {
    @Expose
    private CommonObject TransportType;
    @Expose
    private String PickupDate;
    @Expose
    private String EpochPickupDate;
    @Expose
    private String PickupTime;
    @Expose
    private String PickupLocation;
    @Expose
    private String DropOffLocation;
    @Expose
    private Integer NbOfPassenger;
    @Expose
    private String SpecialRequirements;
    @Expose
    private String BookingId;
    @Expose
    private String UserID;
    @Expose
    private String BookingName;
    @Expose
    private String BookingItemId;
    @Expose
    private String GuestName;
    @Expose
    private String UploadPhoto;
    @Expose
    private String UploadPhotoUrl;
    @Expose
    private Boolean Phone;
    @Expose
    private Boolean Email;
    @Expose
    private Boolean Both;
    @Expose
    private String MobileNumber;
    @Expose
    private String EmailAddress;

    public CommonObject getTransportType() {
        return TransportType;
    }

    public void setTransportType(CommonObject transportType) {
        TransportType = transportType;
    }

    public String getPickupDate() {
        return PickupDate;
    }

    public void setPickupDate(String pickupDate) {
        PickupDate = pickupDate;
    }

    public String getEpochPickupDate() {
        return EpochPickupDate;
    }

    public void setEpochPickupDate(String epochPickupDate) {
        EpochPickupDate = epochPickupDate;
    }

    public String getPickupTime() {
        return PickupTime;
    }

    public void setPickupTime(String pickupTime) {
        PickupTime = pickupTime;
    }

    public String getPickupLocation() {
        return PickupLocation;
    }

    public void setPickupLocation(String pickupLocation) {
        PickupLocation = pickupLocation;
    }

    public String getDropOffLocation() {
        return DropOffLocation;
    }

    public void setDropOffLocation(String dropOffLocation) {
        DropOffLocation = dropOffLocation;
    }

    public Integer getNbOfPassenger() {
        return NbOfPassenger;
    }

    public void setNbOfPassenger(Integer nbOfPassenger) {
        NbOfPassenger = nbOfPassenger;
    }

    public String getSpecialRequirements() {
        return SpecialRequirements;
    }

    public void setSpecialRequirements(String specialRequirements) {
        SpecialRequirements = specialRequirements;
    }

    public String getBookingId() {
        return BookingId;
    }

    public void setBookingId(String bookingId) {
        BookingId = bookingId;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public String getBookingName() {
        return BookingName;
    }

    public void setBookingName(String bookingName) {
        BookingName = bookingName;
    }

    public String getBookingItemId() {
        return BookingItemId;
    }

    public void setBookingItemId(String bookingItemId) {
        BookingItemId = bookingItemId;
    }

    public String getGuestName() {
        return GuestName;
    }

    public void setGuestName(String guestName) {
        GuestName = guestName;
    }

    public String getUploadPhoto() {
        return UploadPhoto;
    }

    public void setUploadPhoto(String uploadPhoto) {
        UploadPhoto = uploadPhoto;
    }

    public String getUploadPhotoUrl() {
        return UploadPhotoUrl;
    }

    public void setUploadPhotoUrl(String uploadPhotoUrl) {
        UploadPhotoUrl = uploadPhotoUrl;
    }

    public Boolean getPhone() {
        return Phone;
    }

    public void setPhone(Boolean phone) {
        Phone = phone;
    }

    public Boolean getEmail() {
        return Email;
    }

    public void setEmail(Boolean email) {
        Email = email;
    }

    public Boolean getBoth() {
        return Both;
    }

    public void setBoth(Boolean both) {
        Both = both;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public String getEmailAddress() {
        return EmailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        EmailAddress = emailAddress;
    }
}
