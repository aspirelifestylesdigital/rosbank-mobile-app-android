package com.rosbank.android.russia.apiservices.ResponseModel;

import com.rosbank.android.russia.apiservices.apis.appimplement.BaseResponse;
import com.rosbank.android.russia.model.RestaurantRecommendationBookingDetailData;

/**
 * Created by ThuNguyen on 10/26/2016.
 */

public class RestaurantRecommendationBookingDetailResponse extends BaseResponse{
    private RestaurantRecommendationBookingDetailData Data;

    public RestaurantRecommendationBookingDetailData getData() {
        return Data;
    }

    public void setData(RestaurantRecommendationBookingDetailData data) {
        Data = data;
    }
}
