package com.rosbank.android.russia.application.coreactivitys;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import com.rosbank.android.russia.R;

public abstract class BaseActivityNonNavigationview
        extends AbstractActivity{

    @Override
    protected int layoutContainerId() {
        return R.id.base_layout_container;
    }

    @Override
    public void setTitle(String title) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_nonnavigationview);

        //
        parentContainer = (FrameLayout) findViewById(R.id.base_layout_container);

        /*Locale locale = new Locale(AppContext.getLanguage());
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getApplicationContext().getResources()
                               .updateConfiguration(config,
                                                    null);*/
        int layoutId = activityLayoutId();
        if(layoutId > 0){
            LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(layoutId, null);

            parentContainer.addView(layout);
        }

        initView();
    }

    @Override
    public void openNavMenu() {

    }

    @Override
    public void closeNavMenu() {

    }

    @Override
    public void showFloatingActionButton(){

    }

    @Override
    public void hideFloatingActionButton(){

    }
}
