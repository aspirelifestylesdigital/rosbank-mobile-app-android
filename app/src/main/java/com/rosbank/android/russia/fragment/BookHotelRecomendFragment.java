package com.rosbank.android.russia.fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.activitys.HomeActivity;
import com.rosbank.android.russia.apiservices.ResponseModel.BookHotelResponse;
import com.rosbank.android.russia.apiservices.ResponseModel.HotelBookingDetailResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CICallback;
import com.rosbank.android.russia.apiservices.b2c.B2CWSGetPreference;
import com.rosbank.android.russia.apiservices.b2c.B2CWSGetRequestDetail;
import com.rosbank.android.russia.apiservices.b2c.B2CWSUpsertConciergeRequest;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.B2CUpsertConciergeRequestRequest;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CGetRecentRequestResponse;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CUpsertConciergeRequestResponse;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.application.AppContext;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.model.CommonObject;
import com.rosbank.android.russia.model.concierge.HotelBookingDetailData;
import com.rosbank.android.russia.model.concierge.MyRequestObject;
import com.rosbank.android.russia.model.preference.HotelPreferenceDetailData;
import com.rosbank.android.russia.model.preference.PreferenceData;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.DateTimeUtil;
import com.rosbank.android.russia.utils.EntranceLock;
import com.rosbank.android.russia.utils.FontUtils;
import com.rosbank.android.russia.utils.NetworkUtil;
import com.rosbank.android.russia.utils.PhotoPickerUtils;
import com.rosbank.android.russia.utils.StringUtil;
import com.rosbank.android.russia.widgets.AttachePhotoView;
import com.rosbank.android.russia.widgets.ContactView;
import com.rosbank.android.russia.widgets.CountryCityRequestView;
import com.rosbank.android.russia.widgets.CustomErrorView;
import com.rosbank.android.russia.widgets.NumberPickerView;

import org.greenrobot.eventbus.EventBus;

import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by anh.trinh on 9/13/2016.
 */
public class BookHotelRecomendFragment
        extends BaseFragment
        implements ContactView.ContactViewListener,
                   SelectionFragment.SelectionCallback,
                   SelectionRoom_Bed_Transport_Fragment.SelectionCallback,
                   SelectionCountryOrCityFragment.SelectionCallback,
                   Callback,
                   B2CICallback {
    @BindView(R.id.edt_maximum_budget)
    EditText mEdtMaximumBudget;
    @BindView(R.id.tv_check_in_date)
    TextView mTvCheckInDate;
    @BindView(R.id.tv_check_out_date)
    TextView mTvCheckOutDate;
    @BindView(R.id.countryCityView)
    CountryCityRequestView countryCityRequestView;
    @BindView(R.id.picker_adults)
    NumberPickerView mPickerAdults;
    @BindView(R.id.picker_kids)
    NumberPickerView mPickerKids;
    @BindView(R.id.edt_loyalty_program)
    EditText mEdtLoyaltyProgram;
    @BindView(R.id.edt_membership_no)
    EditText edtMembershipNo;
    @BindView(R.id.tv_please_select_room)
    TextView mTvPleaseSelectRoom;
    @BindView(R.id.room_preference_error)
    CustomErrorView roomPreferenceErrorView;
    @BindView(R.id.tb_switch)
    ToggleButton mTbSwitch;
    @BindView(R.id.edt_any_special_requirements)
    EditText mEdtAnySpecialRequirements;
    @BindView(R.id.special_req_error)
    CustomErrorView specialReqError;
    @BindView(R.id.photo_attached)
    AttachePhotoView mPhotoAttached;
    /* @BindView(R.id.img_attached)
     ImageView mImgAttached;*/
    @BindView(R.id.btn_cancel)
    Button mBtnCancel;
    @BindView(R.id.btn_submit)
    Button mBtnSubmit;
    @BindView(R.id.layout_bottom_button)
    LinearLayout mLayoutBottomButton;
    @BindView(R.id.container)
    LinearLayout mLayoutContainer;
    @BindView(R.id.scrollView)
    ScrollView mScrollView;
    @BindView(R.id.contact_view)
    ContactView mContactView;

    CommonObject mRoomTypeSelected;

    private Calendar checkInCalendar;
    private Calendar checkOutCalendar;

    private MyRequestObject myRequestObject = null;
    private HotelBookingDetailData detailData = null;
    private HotelPreferenceDetailData hotelPreferenceDetailData;
    private B2CUpsertConciergeRequestRequest upsertConciergeRequestRequest;
    private String uploadedPhotoPath = "";
    private boolean isDateSelectedByAction = false;
    private boolean isSubmitClicked = false;

    private EntranceLock entranceLock = new EntranceLock();

    @Override
    protected int layoutId() {
        return R.layout.fragment_concierge_book_hotel_recomend;
    }

    @Override
    protected void initView() {

        CommonUtils.setFontForViewRecursive(mLayoutContainer,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
        CommonUtils.setFontForViewRecursive(mBtnSubmit,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        CommonUtils.setFontForViewRecursive(mBtnCancel,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        CommonUtils.makeFirstCharacterUpperCase(mEdtLoyaltyProgram,
                                                edtMembershipNo);
        mPickerAdults.setMinimum(1);
        mPickerKids.setMinimum(0);
        makeDefaultDate();
        specialReqError.setMessage(getString(R.string.special_requirement_error));
        mPhotoAttached.setParentFragment(this);
        mPhotoAttached.setAttachePhotoCallback(new AttachePhotoView.IAttachePhotoCallback() {
            @Override
            public void onPhotoChanged() {
                // Reset the photo path once the user change photo
                uploadedPhotoPath = "";
            }
        });
        mContactView.setContactViewListener(this);
        if (getArguments() != null) {
            myRequestObject =
                    (MyRequestObject) getArguments().getSerializable(AppConstant.PRE_REQUEST_OBJECT_DATA);
        }
        if (myRequestObject != null) {
            isDateSelectedByAction = true;
            showDialogProgress();
            B2CWSGetRequestDetail b2CWSGetRequestDetail = new B2CWSGetRequestDetail(this);
            b2CWSGetRequestDetail.setValue(myRequestObject.getEpcCaseId(),
                                           myRequestObject.getBookingItemID());
            b2CWSGetRequestDetail.run(null);
        } else { // Load default preferences
            B2CWSGetPreference b2CWSGetPreference = new B2CWSGetPreference(getPreferenceCallback);
            b2CWSGetPreference.run(null);
        }
    }

    public void makeDefaultDate() {
        if (!isDateSelectedByAction) {
            checkInCalendar = Calendar.getInstance();
            checkInCalendar.add(Calendar.DAY_OF_YEAR,
                                1);
            checkInCalendar.set(Calendar.HOUR_OF_DAY,
                                23);
            checkInCalendar.set(Calendar.MINUTE,
                                59);
            checkInCalendar.set(Calendar.SECOND,
                                59);

            checkOutCalendar = Calendar.getInstance();
            checkOutCalendar.add(Calendar.DAY_OF_YEAR,
                                 2);

            showDate();
        }
    }

    private void showDate() {
        mTvCheckInDate.setText(DateTimeUtil.shareInstance()
                                           .formatTimeStamp(checkInCalendar.getTimeInMillis(),
                                                            AppConstant.DATE_FORMAT_CONCIERGE_BOOKING));
        mTvCheckOutDate.setText(DateTimeUtil.shareInstance()
                                            .formatTimeStamp(checkOutCalendar.getTimeInMillis(),
                                                             AppConstant.DATE_FORMAT_CONCIERGE_BOOKING));
    }

    @Override
    protected void bindData() {
    }

    @Override
    public boolean onBack() {
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((HomeActivity) getActivity()).showLogoApp(false);
        ((HomeActivity) getActivity()).setTitle(getString(R.string.concierge_item_hotel));
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater,
                                           container,
                                           savedInstanceState);
        ButterKnife.bind(this,
                         rootView);
        return rootView;
    }

    @OnTouch({R.id.tv_please_select_room,
              R.id.edt_any_special_requirements})
    boolean onInputTouch(final View v,
                         final MotionEvent event) {

        switch (v.getId()) {
            case R.id.tv_please_select_room:
                roomPreferenceErrorView.setVisibility(View.GONE);
                break;
            case R.id.edt_any_special_requirements:
                specialReqError.setVisibility(View.GONE);
                break;
        }
        return false;
    }

    @OnClick({R.id.tv_please_select_room,
              R.id.tv_check_in_date,
              R.id.tv_check_out_date,
              R.id.btn_cancel,
              R.id.btn_submit})
    public void onClick(View view) {
        CommonUtils.hideSoftKeyboard(getContext());
        switch (view.getId()) {
            case R.id.tv_please_select_room:
                Bundle bundle = new Bundle();
                bundle.putString(SelectionRoom_Bed_Transport_Fragment.PRE_SELECTION_TYPE,
                                 SelectionRoom_Bed_Transport_Fragment.PRE_SELECTION_ROOM);
                bundle.putString(SelectionRoom_Bed_Transport_Fragment.PRE_HEADER_TITLE,
                                 getString(R.string.text_hotel_room_preference));
                bundle.putParcelable(SelectionRoom_Bed_Transport_Fragment.PRE_SELECTION_DATA,
                                     mRoomTypeSelected);

                SelectionRoom_Bed_Transport_Fragment selectionRoomBedTransportFragment =
                        new SelectionRoom_Bed_Transport_Fragment();
                selectionRoomBedTransportFragment.setArguments(bundle);
                selectionRoomBedTransportFragment.setSelectionCallBack(this);
                pushFragment(selectionRoomBedTransportFragment,
                             true,
                             true);
                break;
            case R.id.tv_check_in_date:
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                                                                         new DatePickerDialog.OnDateSetListener() {

                                                                             @Override
                                                                             public void onDateSet(DatePicker view,
                                                                                                   int year,
                                                                                                   int monthOfYear,
                                                                                                   int dayOfMonth) {
                                                                                 if (CommonUtils.validateFutureDate(dayOfMonth,
                                                                                                                    monthOfYear,
                                                                                                                    year,
                                                                                                                    view)) {
                                                                                     isDateSelectedByAction =
                                                                                             true;
                                                                                     checkInCalendar.set(year,
                                                                                                         monthOfYear,
                                                                                                         dayOfMonth);
                                                                                     checkInCalendar.set(Calendar.HOUR_OF_DAY,
                                                                                                         23);
                                                                                     checkInCalendar.set(Calendar.MINUTE,
                                                                                                         59);
                                                                                     checkInCalendar.set(Calendar.SECOND,
                                                                                                         59);
                                                                                     // Change checkout as checkin if it is less than
                                                                                     if (checkOutCalendar.getTimeInMillis() < (checkInCalendar.getTimeInMillis() + AppConstant.DAY_IN_MILLISECONDS)) {
                                                                                         checkOutCalendar.set(year,
                                                                                                              monthOfYear,
                                                                                                              dayOfMonth);
                                                                                         checkOutCalendar.add(Calendar.DAY_OF_YEAR,
                                                                                                              1);
                                                                                     }
                                                                                     showDate();
                                                                                 }

                                                                             }

                                                                         },
                                                                         checkInCalendar.get(Calendar.YEAR),
                                                                         checkInCalendar.get(Calendar.MONTH),
                                                                         checkInCalendar.get(Calendar.DAY_OF_MONTH));

                Calendar c = Calendar.getInstance();
                c.add(Calendar.DAY_OF_YEAR,
                      1);
//                c.set(Calendar.HOUR_OF_DAY, 0);
//                c.set(Calendar.MINUTE, 0);
//                c.set(Calendar.SECOND, 0);
                datePickerDialog.getDatePicker()
                                .setMinDate(c.getTimeInMillis() - 1000);
                datePickerDialog.show();
                break;

            case R.id.tv_check_out_date:
                DatePickerDialog datePickerDialogOut = new DatePickerDialog(getContext(),
                                                                            new DatePickerDialog.OnDateSetListener() {

                                                                                @Override
                                                                                public void onDateSet(DatePicker view,
                                                                                                      int year,
                                                                                                      int monthOfYear,
                                                                                                      int dayOfMonth) {
                                                                                    if (CommonUtils.validateFutureDate(dayOfMonth,
                                                                                                                       monthOfYear,
                                                                                                                       year,
                                                                                                                       view)) {
                                                                                        isDateSelectedByAction =
                                                                                                true;
                                                                                        checkOutCalendar.set(year,
                                                                                                             monthOfYear,
                                                                                                             dayOfMonth);
                                                                                        showDate();
                                                                                    }
                                                                                }
                                                                            },
                                                                            checkOutCalendar.get(Calendar.YEAR),
                                                                            checkOutCalendar.get(Calendar.MONTH),
                                                                            checkOutCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialogOut.getDatePicker()
                                   .setMinDate(checkInCalendar.getTimeInMillis() + AppConstant.DAY_IN_MILLISECONDS - 1000);
                datePickerDialogOut.show();
                break;
            case R.id.btn_cancel:
                if (!entranceLock.isClickContinuous()) {
                    onBackPress();
                }
                break;
            case R.id.btn_submit:
                AppContext.getSharedInstance().track(AppConstant.ANALYTIC_EVENT_CATEGORY_CONCIERGE_REQUEST, AppConstant.ANALYTIC_EVENT_ACTION_SEND_CONCIERGE_REQUEST, AppConstant.ANALYTIC_BOOK_ITEM.HOTEL.getValue());
                if (validationData() && !entranceLock.isClickContinuous()) {
                    if (NetworkUtil.getNetworkStatus(getContext())) {
                        isSubmitClicked = true;
                        processBooking();
                    } else {
                        showInternetProblemDialog();
                    }
                }

                break;
        }
    }

    @Override
    public void onResponse(final Call call,
                           final Response response) {
        if (response.body() instanceof BookHotelResponse) {
            hideDialogProgress();
            if(getActivity()==null) {
                return;
            }
            BookHotelResponse bookHotelResponse = ((BookHotelResponse) response.body());
            int code = bookHotelResponse.getStatus();
            if (code == 200) {
                onBackPress();

                ThankYouFragment thankYouFragment = new ThankYouFragment();
                if (myRequestObject != null) {
                  /*  Bundle bundle = new Bundle();
                    bundle.putString(ThankYouFragment.LINE_MAIN,
                                     getString(R.string.text_message_thank_you_amend));
                    thankYouFragment.setArguments(bundle);*/
                    EventBus.getDefault().post(AppConstant.CONCIERGE_EDIT_TYPE.AMEND);
                } else {
                    // Track product
                    AppContext.getSharedInstance().track(AppConstant.TRACK_EVENT_PRODUCT_CATEGORY.RECOMMEND_HOTEL.getValue(), AppConstant.TRACK_EVENT_PRODUCT_NAME.GENERIC_RECOMMEND.getValue());
                    EventBus.getDefault().post(AppConstant.CONCIERGE_EDIT_TYPE.ADD);
                }
                pushFragment(thankYouFragment,
                             true,
                             true);

            } else {
//                showToast(bookHotelResponse.getMessage());
            }
        }
        if (response.body() instanceof HotelBookingDetailResponse) {
            hideDialogProgress();
            if(getActivity()==null) {
                return;
            }
            HotelBookingDetailResponse hotelBookingDetailResponse =
                    ((HotelBookingDetailResponse) response.body());
            int code = hotelBookingDetailResponse.getStatus();
            if (code == 200) {

                detailData = hotelBookingDetailResponse.getData();
                setDetailData(detailData);
            } else {
//                showToast(hotelBookingDetailResponse.getMessage());
            }
        }


    }

    @Override
    public void onFailure(final Call call,
                          final Throwable t) {
        hideDialogProgress();
        if(getActivity()==null) {
            return;
        }
//        showToast(getString(R.string.text_server_error_message));
    }

    @Override
    public void onFinishSelection(final Bundle bundle) {

        String typeSelection = bundle.getString(SelectionFragment.PRE_SELECTION_TYPE,
                                                "");

        if (typeSelection.equals(SelectionFragment.PRE_SELECTION_COUNTRY_CODE)) {
            mContactView.setCountryNo(bundle.getString(SelectionFragment.PRE_SELECTION_DATA,
                                                       ""));
        }
        if (typeSelection.equals(SelectionRoom_Bed_Transport_Fragment.PRE_SELECTION_ROOM)) {
            mRoomTypeSelected =
                    bundle.getParcelable(SelectionRoom_Bed_Transport_Fragment.PRE_SELECTION_DATA);
            if (mRoomTypeSelected != null) {
                mTvPleaseSelectRoom.setText(mRoomTypeSelected.getValue());
                mTvPleaseSelectRoom.setTextColor(ContextCompat.getColor(getContext(),
                                                                        R.color.text_color_active));
            } else {
                mTvPleaseSelectRoom.setText(R.string.text_hotel_please_select_room_preference);
                mTvPleaseSelectRoom.setTextColor(ContextCompat.getColor(getContext(),
                                                                        R.color.hint_color));
            }
            roomPreferenceErrorView.setVisibility(View.GONE);
        }
    }

    private boolean validationData() {
        boolean result = true;
        if (!countryCityRequestView.isValidated()) {
            result = false;
        }
        if (mTvPleaseSelectRoom.getText()
                               .toString()
                               .equalsIgnoreCase(getString(R.string.text_hotel_please_select_room_preference))) {
            roomPreferenceErrorView.fillData(getString(R.string.text_sign_up_error_required_field));
            roomPreferenceErrorView.setVisibility(View.VISIBLE);
            result = false;
        } else {
            roomPreferenceErrorView.setVisibility(View.GONE);
        }
        if (StringUtil.containSpecialCharacter(mEdtAnySpecialRequirements.getText()
                                                                         .toString())) {
            specialReqError.setVisibility(View.VISIBLE);
            result = false;
        } else {
            specialReqError.setVisibility(View.GONE);
        }
        if (!mContactView.validationContact()) {
            result = false;
        }
        return result;
    }

    private void processBooking() {
        upsertConciergeRequestRequest = new B2CUpsertConciergeRequestRequest();
        upsertConciergeRequestRequest.setFunctionality(AppConstant.CONCIERGE_FUNCTIONALITY_TYPE.HOTEL_RECOMMEND.getValue());
        upsertConciergeRequestRequest.setRequestType(AppConstant.CONCIERGE_REQUEST_TYPE.RECOMMEND_HOTEL.getValue());
       /* upsertConciergeRequestRequest.setCity(edtCity.getText().toString().trim());
        upsertConciergeRequestRequest.setCountry(mTvSelectCountry.getText().toString().trim());*/
        upsertConciergeRequestRequest.setNumberOfAdults(String.valueOf(mPickerAdults.getNumber()));
        if (mPickerKids.getNumber() > 0) {
            upsertConciergeRequestRequest.setNumberOfKids(String.valueOf(mPickerKids.getNumber()));
            upsertConciergeRequestRequest.setNumberOfChildren(String.valueOf(mPickerKids.getNumber()));
        }
        upsertConciergeRequestRequest.updateDataFromCountryCityView(countryCityRequestView);

        upsertConciergeRequestRequest.updateDataFromContactView(mContactView);

        // Check in date
        upsertConciergeRequestRequest.setStartDate(DateTimeUtil.shareInstance()
                                                               .formatTimeStamp(checkInCalendar.getTimeInMillis(),
                                                                                AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS));
        upsertConciergeRequestRequest.setDelivery(DateTimeUtil.shareInstance()
                                                              .formatTimeStamp(checkInCalendar.getTimeInMillis(),
                                                                               AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS));
        upsertConciergeRequestRequest.setCheckIn(DateTimeUtil.shareInstance()
                                                             .formatTimeStamp(checkInCalendar.getTimeInMillis(),
                                                                              AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS));
        upsertConciergeRequestRequest.setCheckindate(DateTimeUtil.shareInstance().formatTimeStamp(checkInCalendar.getTimeInMillis(), AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS));

        // Check out date
        upsertConciergeRequestRequest.setEndDate(DateTimeUtil.shareInstance()
                                                             .formatTimeStamp(checkOutCalendar.getTimeInMillis(),
                                                                              AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS));
        upsertConciergeRequestRequest.setCheckOut(DateTimeUtil.shareInstance()
                                                              .formatTimeStamp(checkOutCalendar.getTimeInMillis(),
                                                                               AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS));
        upsertConciergeRequestRequest.setCheckoutdate(DateTimeUtil.shareInstance().formatTimeStamp(checkOutCalendar.getTimeInMillis(), AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS));

        // Image path
        if (!TextUtils.isEmpty(uploadedPhotoPath)) {
            upsertConciergeRequestRequest.setAttachmentPath(uploadedPhotoPath);
        } else {
            if (mPhotoAttached.getPhotos() != null && mPhotoAttached.getPhotos()
                                                                    .size() > 0) {
                String pathImage = mPhotoAttached.getPhotos()
                                                 .get(0);
                upsertConciergeRequestRequest.setAttachmentPath(pathImage);
            }
        }
        if (myRequestObject != null) {
            upsertConciergeRequestRequest.setTransactionID(myRequestObject.getBookingItemID());
        }
        // Request details
        upsertConciergeRequestRequest.setRequestDetails(combineRequestDetails());
        upsertConciergeRequestRequest.setSituation("hotel");

        showDialogProgress();
        B2CWSUpsertConciergeRequest b2CWSUpsertConciergeRequest =
                new B2CWSUpsertConciergeRequest(myRequestObject == null ?
                                                AppConstant.CONCIERGE_EDIT_TYPE.ADD :
                                                AppConstant.CONCIERGE_EDIT_TYPE.AMEND,
                                                this);
        b2CWSUpsertConciergeRequest.setRequest(upsertConciergeRequestRequest);
        b2CWSUpsertConciergeRequest.run(null);

    }

    private String combineRequestDetails() {
        String requestDetails = "";
        if (!TextUtils.isEmpty(mEdtAnySpecialRequirements.getText()
                                                         .toString()
                                                         .trim())) {
            requestDetails +=
                    AppConstant.B2C_REQUEST_DETAIL_SPECIAL_REQUIREMENTS + StringUtil.removeAllSpecialCharacterAndBreakLine(mEdtAnySpecialRequirements.getText()
                                                                                                                                            .toString()
                                                                                                                                            .trim());
        }
        if (!TextUtils.isEmpty(mEdtMaximumBudget.getText().toString())) {
            if (!TextUtils.isEmpty(requestDetails)) {
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_MAX_PRICE + StringUtil.removeAllSpecialCharacterAndBreakLine(mEdtMaximumBudget.getText().toString());
        }else {
            if (!TextUtils.isEmpty(requestDetails)) {
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_MAX_PRICE + "0";
        }

        if (!TextUtils.isEmpty(mContactView.getReservationName())) {
            if (!TextUtils.isEmpty(requestDetails)) {
                requestDetails += " | ";
            }
            requestDetails +=
                    AppConstant.B2C_REQUEST_DETAIL_GUEST_NAME + StringUtil.removeAllSpecialCharacterAndBreakLine(mContactView.getReservationName());
        }

        if (!TextUtils.isEmpty(mEdtLoyaltyProgram.getText()
                                                 .toString())) {
            if (!TextUtils.isEmpty(requestDetails)) {
                requestDetails += " | ";
            }
            requestDetails +=
                    AppConstant.B2C_REQUEST_DETAIL_MEMBERSHIP + StringUtil.removeAllSpecialCharacterAndBreakLine(mEdtLoyaltyProgram.getText()
                                                                                                                                   .toString());
        }
        if (!TextUtils.isEmpty(edtMembershipNo.getText()
                                              .toString())) {
            if (!TextUtils.isEmpty(requestDetails)) {
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_MEMBERNO + edtMembershipNo.getText()
                                                                                       .toString();
        }
        if (!TextUtils.isEmpty(countryCityRequestView.getCountry())) {
            if (!TextUtils.isEmpty(requestDetails)) {
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_COUNTRY + countryCityRequestView.getCountry();
        }
        if (mRoomTypeSelected != null) {
            if (!TextUtils.isEmpty(mRoomTypeSelected.getValue())) {
                if (!TextUtils.isEmpty(requestDetails)) {
                    requestDetails += " | ";
                }
                requestDetails +=
                        AppConstant.B2C_REQUEST_DETAIL_ROOM_PREF + mRoomTypeSelected.getValue();
            }
        }
        if (!TextUtils.isEmpty(requestDetails)) {
            requestDetails += " | ";
        }
        requestDetails += AppConstant.B2C_REQUEST_DETAIL_SMOKING_PREF + (mTbSwitch.isChecked() ?
                                                                         getString(R.string.text_yes) :
                                                                         getString(R.string.text_no));
        if (!TextUtils.isEmpty(countryCityRequestView.getState())) {
            if (!TextUtils.isEmpty(requestDetails)) {
                requestDetails += " | ";
            }
            requestDetails +=
                    AppConstant.B2C_REQUEST_DETAIL_STATE + countryCityRequestView.getState();
        }
        if (hotelPreferenceDetailData != null && !TextUtils.isEmpty(hotelPreferenceDetailData.getAdditionalPreference())) {
            if (!TextUtils.isEmpty(requestDetails)) {
                requestDetails += " | ";
            }
            requestDetails +=
                    AppConstant.B2C_REQUEST_DETAIL_ADDITIONAL_PREFERENCE + hotelPreferenceDetailData.getAdditionalPreference();
        }
        return requestDetails;
        /*String requestDetails = "";
        if(!TextUtils.isEmpty(mEdtAnySpecialRequirements.getText().toString().trim())){
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_SPECIAL_REQ + mEdtAnySpecialRequirements.getText().toString().trim();
        }
        if(!TextUtils.isEmpty(mEdtMaximumBudget.getText().toString())){
            if(!TextUtils.isEmpty(requestDetails)){
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_MAX_PRICE + mEdtMaximumBudget.getText().toString();
        }

        if(!TextUtils.isEmpty(mEdtLoyaltyProgram.getText().toString())){
            if(!TextUtils.isEmpty(requestDetails)){
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_MEMBERSHIP + mEdtLoyaltyProgram.getText().toString();
        }
        if(!TextUtils.isEmpty(edtMembershipNo.getText().toString())){
            if(!TextUtils.isEmpty(requestDetails)){
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_MEMBERNO + edtMembershipNo.getText().toString();
        }
        if(mRoomTypeSelected != null){
            if(!TextUtils.isEmpty(mRoomTypeSelected.getValue())){
                if (!TextUtils.isEmpty(requestDetails)) {
                    requestDetails += " | ";
                }
                requestDetails += AppConstant.B2C_REQUEST_DETAIL_ROOM_PREF + mRoomTypeSelected.getValue();
            }
        }
        if (!TextUtils.isEmpty(requestDetails)) {
            requestDetails += " | ";
        }
        requestDetails += AppConstant.B2C_REQUEST_DETAIL_SMOKING_PREF + (mTbSwitch.isChecked() ? "true" : "false");
        return requestDetails;*/
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode,
                                         permissions,
                                         grantResults);

        switch (requestCode) {
            case AppConstant.PERMISSION_REQUEST_READ_EXTERNAL:
                if (grantResults.length > 0
                            && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mPhotoAttached.openGallery();
                }
                break;
            case AppConstant.PERMISSION_REQUEST_OPEN_CAMERA:
                if (grantResults.length > 0
                            && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mPhotoAttached.openCamera();
                }
                break;
            case AppConstant.PERMISSION_REQUEST_WRITE_EXTERNAL:
                if (grantResults.length > 0
                            && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mPhotoAttached.openCamera();
                }
                break;
        }

    }

    @Override
    public void onActivityResult(int requestCode,
                                 int resultCode,
                                 Intent data) {
        super.onActivityResult(requestCode,
                               resultCode,
                               data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == PhotoPickerUtils.PICK_IMAGE_REQUEST_CODE || requestCode == PhotoPickerUtils.TAKE_PICTURE_REQUEST_CODE) {
                mPhotoAttached.onActivityResult(requestCode,
                                                data);
            }
        }
    }

    @Override
    public void onSelectCountryClick() {
        Bundle bundle1 = new Bundle();
        bundle1.putString(SelectionFragment.PRE_SELECTION_TYPE,
                          SelectionFragment.PRE_SELECTION_COUNTRY_CODE);
        bundle1.putString(SelectionFragment.PRE_SELECTION_DATA,
                          mContactView.getSelectedCountryCode());
        SelectionFragment fragment1 = new SelectionFragment();
        fragment1.setSelectionCallBack(this);
        fragment1.setArguments(bundle1);
        pushFragment(fragment1,
                     true,
                     true);
    }

    private void setDetailData(HotelBookingDetailData data) {
        if(getActivity()==null) {
            return;
        }
        mEdtMaximumBudget.setText(data.getMaxPrice());
        countryCityRequestView.setData(data);
        mTvPleaseSelectRoom.setText(data.getRoomType());
        mTvPleaseSelectRoom.setTextColor(ContextCompat.getColor(getContext(),
                                                                R.color.text_color_active));
        mRoomTypeSelected = new CommonObject(data.getRoomType());

        mTbSwitch.setChecked(data.getSmokingPreference());
        mPickerAdults.setNumber(data.getNumberOfAdults());
        mPickerKids.setNumber(data.getNumberOfKids());
        mEdtAnySpecialRequirements.setText(data.getConciergeRequestDetail()
                                               .getSpecialRequirement());
        mEdtLoyaltyProgram.setText(data.getLoyaltyProgrammeMembership());
        edtMembershipNo.setText(data.getMemberNo());

        checkInCalendar = Calendar.getInstance();
        checkInCalendar.setTimeInMillis(data.getCheckInDateEpoc());
        checkOutCalendar = Calendar.getInstance();
        checkOutCalendar.setTimeInMillis(data.getCheckOutDateEpoc());
        showDate();

        // Update contact view
        mContactView.updateContactView(data);

    }

    private void updateUIFromDefaultPreference(HotelPreferenceDetailData hotelPreferenceDetailData) {
        if (hotelPreferenceDetailData != null) {
            // Room type
            String roomPreference = hotelPreferenceDetailData.getRoomPreference();
            if (!TextUtils.isEmpty(roomPreference)) {
                String roomRu =CommonUtils.getRusiaRoomPreferenceString(roomPreference);
                if(CommonUtils.isStringValid(roomRu)){
                    mRoomTypeSelected =
                            new CommonObject(roomRu);
                }else {
                    mRoomTypeSelected = new CommonObject(roomPreference);
                }
                mTvPleaseSelectRoom.setText(roomPreference);
                mTvPleaseSelectRoom.setTextColor(ContextCompat.getColor(getContext(),
                                                                        R.color.text_color_active));
            }
            // Smoking
            mTbSwitch.setChecked(hotelPreferenceDetailData.isSmokingPreference());

            // Loyalty program
            String loyaltyProgram = hotelPreferenceDetailData.getLoyaltyProgram();
            if (!TextUtils.isEmpty(loyaltyProgram)) {
                mEdtLoyaltyProgram.setText(loyaltyProgram);
            }
            // Membership no
            String memberShipNo = hotelPreferenceDetailData.getMemberShipNo();
            if (!TextUtils.isEmpty(memberShipNo)) {
                edtMembershipNo.setText(memberShipNo);
            }
        }
    }

    @Override
    public void onB2CResponse(B2CBaseResponse response) {
        hideDialogProgress();
        if(getActivity()==null) {
            return;
        }
        if (response instanceof B2CUpsertConciergeRequestResponse) {
            if (response != null && response.isSuccess()) {
                onBackPress();

                ThankYouFragment thankYouFragment = new ThankYouFragment();
                if (myRequestObject != null) {
//                    Bundle bundle = new Bundle();
//                    bundle.putString(ThankYouFragment.LINE_MAIN,
//                            getString(R.string.text_message_thank_you_amend));
//                    thankYouFragment.setArguments(bundle);
                    EventBus.getDefault().post(AppConstant.CONCIERGE_EDIT_TYPE.AMEND);
                }else{
                    // Track product
                    AppContext.getSharedInstance().track(AppConstant.TRACK_EVENT_PRODUCT_CATEGORY.RECOMMEND_HOTEL.getValue(), AppConstant.TRACK_EVENT_PRODUCT_NAME.GENERIC_RECOMMEND.getValue());
                    EventBus.getDefault().post(AppConstant.CONCIERGE_EDIT_TYPE.ADD);
                }
                pushFragment(thankYouFragment,
                             true,
                             true);
            } else {
                showDialogMessage(getString(R.string.text_title_dialog_error),
                                  getString(R.string.text_concierge_request_error));
                if (upsertConciergeRequestRequest != null) {
                    uploadedPhotoPath = upsertConciergeRequestRequest.getAttachmentPath();
                }
            }
        } else if (response instanceof B2CGetRecentRequestResponse) {
            detailData = new HotelBookingDetailData((B2CGetRecentRequestResponse) response);
            setDetailData(detailData);
        }
    }

    @Override
    public void onB2CResponseOnList(List<B2CBaseResponse> responseList) {
        if(getActivity()==null) {
            return;
        }
    }

    @Override
    public void onB2CFailure(String errorMessage,
                             String errorCode) {
        hideDialogProgress();
        if(getActivity()==null) {
            return;
        }
        if (isSubmitClicked) {
            isSubmitClicked = false;
            showDialogMessage(getString(R.string.text_title_dialog_error),
                              getString(R.string.text_concierge_request_error));
        }
        // Store the uploaded successful photo path
        // So that the upsert failed in the first time, user tried to process for the second time
        // then that is no need to upload again
        if (upsertConciergeRequestRequest != null) {
            uploadedPhotoPath = upsertConciergeRequestRequest.getAttachmentPath();
        }
    }

    private B2CICallback getPreferenceCallback = new B2CICallback() {
        @Override
        public void onB2CResponse(B2CBaseResponse response) {
            if(getActivity()==null) {
                return;
            }
        }

        @Override
        public void onB2CResponseOnList(List<B2CBaseResponse> responseList) {
            if (getActivity() != null) {
                hotelPreferenceDetailData =
                        (HotelPreferenceDetailData) PreferenceData.findFromList(responseList,
                                                                                AppConstant.PREFERENCE_TYPE.HOTEL);
                updateUIFromDefaultPreference(hotelPreferenceDetailData);
            }
        }

        @Override
        public void onB2CFailure(String errorMessage,
                                 String errorCode) {
            if(getActivity()==null) {
                return;
            }
        }
    };
}
