package com.rosbank.android.russia.apiservices.b2c.ResponseModel;

import android.text.TextUtils;

import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.rosbank.android.russia.model.concierge.ConciergeRequestDetail;
import com.rosbank.android.russia.utils.StringUtil;
import com.google.gson.Gson;
import com.google.gson.annotations.Expose;

import org.json.JSONException;
import org.json.JSONObject;


public class B2CGetRecentRequestResponse
        extends B2CBaseResponse {
    @Expose
    private Integer RowNumber;
    @Expose
    private String TransactionID;
    @Expose
    private String VeriCode;
    @Expose
    private String EPCCaseID;
    @Expose
    private String ATTACHMENTPATH;
    @Expose
    private String STARTDATE;
    @Expose
    private String ENDDATE;
    @Expose
    private String COUNTRY;
    @Expose
    private String CITY;
    @Expose
    private String STATE;
    @Expose
    private String FIRSTNAME;
    @Expose
    private String LASTNAME;
    @Expose
    private String Situation;
    @Expose
    private String CREATEDDATE;
    @Expose
    private String DELIVERY;
    @Expose
    private String EMAILADDRESS1;
    @Expose
    private String EMAIL1;
    @Expose
    private String EVENTNAME;
    @Expose
    private String EVENTCATEGORY;
    @Expose
    private String FUNCTIONALITY;
    @Expose
    private String NUMBEROFADULTS;
    @Expose
    private String NUMBEROFCHILDREN;
    @Expose
    private String PHONENUMBER;
    @Expose
    private String PICKUPDATE;
    @Expose
    private String DROPOFFDATE;
    @Expose
    private String EVENTDATE;
    @Expose
    private String PREFRESPONSE;
    @Expose
    private String REQUESTDETAILS;
    @Expose
    private String REQUESTMODE;
    @Expose
    private String REQUESTSTATUS;
    @Expose
    private String REQUESTTYPE;
    @Expose
    private String CHECKOUT;
    @Expose
    private String CHECKIN;
    @Expose
    private String CHECKOUTDATE;
    @Expose
    private String CHECKINDATE;
    @Expose
    private String NUMBEROFKIDS;
    @Expose
    private String PICKUP;

    @Expose
    private String VERIFICATIONCODE;

    public String getVERIFICATIONCODE() {
        return VERIFICATIONCODE;
    }

    public void setVERIFICATIONCODE(final String VERIFICATIONCODE) {
        this.VERIFICATIONCODE = VERIFICATIONCODE;
    }

    public String getEVENTCATEGORY() {
        return EVENTCATEGORY;
    }

    public void setEVENTCATEGORY(String EVENTCATEGORY) {
        this.EVENTCATEGORY = EVENTCATEGORY;
    }

    public String getCHECKOUTDATE() {
        return CHECKOUTDATE;
    }

    public void setCHECKOUTDATE(String CHECKOUTDATE) {
        this.CHECKOUTDATE = CHECKOUTDATE;
    }

    public String getCHECKINDATE() {
        return CHECKINDATE;
    }

    public void setCHECKINDATE(String CHECKINDATE) {
        this.CHECKINDATE = CHECKINDATE;
    }

    public String getPICKUP() {
        return PICKUP;
    }

    public void setPICKUP(final String PICKUP) {
        this.PICKUP = PICKUP;
    }


    public boolean isSuccess(){
        if(!TextUtils.isEmpty(TransactionID)){
            return true;
        }
        return super.isSuccess();
    }
    public String getATTACHMENTPATH() {
        return ATTACHMENTPATH;
    }

    public void setATTACHMENTPATH(String ATTACHMENTPATH) {
        this.ATTACHMENTPATH = ATTACHMENTPATH;
    }

    public String getSTARTDATE() {
        return STARTDATE;
    }

    public void setSTARTDATE(String STARTDATE) {
        this.STARTDATE = STARTDATE;
    }

    public String getENDDATE() {
        return ENDDATE;
    }

    public void setENDDATE(String ENDDATE) {
        this.ENDDATE = ENDDATE;
    }

    public String getCOUNTRY() {

        return COUNTRY;
    }

    public String getCITY() {
        return CITY;
    }

    public void setCITY(String CITY) {
        this.CITY = CITY;
    }

    public String getSTATE() {
        return STATE;
    }

    public void setSTATE(String STATE) {
        this.STATE = STATE;
    }

    public String getCREATEDDATE() {
        return CREATEDDATE;
    }

    public void setCREATEDDATE(String CREATEDDATE) {
        this.CREATEDDATE = CREATEDDATE;
    }

    public String getDELIVERY() {
        return DELIVERY;
    }

    public void setDELIVERY(String DELIVERY) {
        this.DELIVERY = DELIVERY;
    }

    public String getEMAILADDRESS1() {
        return EMAILADDRESS1;
    }

    public void setEMAILADDRESS1(String EMAILADDRESS1) {
        this.EMAILADDRESS1 = EMAILADDRESS1;
    }

    public String getEMAIL1() {
        return EMAIL1;
    }

    public String getEPCCaseID() {
        return EPCCaseID;
    }

    public void setEPCCaseID(String EPCCaseID) {
        this.EPCCaseID = EPCCaseID;
    }

    public String getEVENTNAME() {
        return EVENTNAME;
    }

    public void setEVENTNAME(String EVENTNAME) {
        this.EVENTNAME = EVENTNAME;
    }

    public String getFUNCTIONALITY() {
        return FUNCTIONALITY;
    }

    public void setFUNCTIONALITY(String FUNCTIONALITY) {
        this.FUNCTIONALITY = FUNCTIONALITY;
    }

    public String getNUMBEROFADULTS() {
        return NUMBEROFADULTS;
    }

    public void setNUMBEROFADULTS(String NUMBEROFADULTS) {
        this.NUMBEROFADULTS = NUMBEROFADULTS;
    }

    public String getNUMBEROFCHILDREN() {
        if(NUMBEROFCHILDREN==null){
            return NUMBEROFKIDS;
        }else {
            return NUMBEROFCHILDREN;
        }
    }

    public void setNUMBEROFCHILDREN(String NUMBEROFCHILDREN) {
        this.NUMBEROFCHILDREN = NUMBEROFCHILDREN;
    }

    public String getPHONENUMBER() {
        return PHONENUMBER;
    }

    public void setPHONENUMBER(String PHONENUMBER) {
        this.PHONENUMBER = PHONENUMBER;
    }

    public String getPICKUPDATE() {
        if(PICKUPDATE== null){
            PICKUPDATE = PICKUP;
        }
        return PICKUPDATE;
    }

    public void setPICKUPDATE(String PICKUPDATE) {
        this.PICKUPDATE = PICKUPDATE;
    }

    public String getPREFRESPONSE() {
        return PREFRESPONSE;
    }

    public void setPREFRESPONSE(String PREFRESPONSE) {
        this.PREFRESPONSE = PREFRESPONSE;
    }

    public String getREQUESTDETAILS() {
        return REQUESTDETAILS;
    }

    public void setREQUESTDETAILS(String REQUESTDETAILS) {
        this.REQUESTDETAILS = REQUESTDETAILS;
    }

    public String getREQUESTMODE() {
        return REQUESTMODE;
    }

    public void setREQUESTMODE(String REQUESTMODE) {
        this.REQUESTMODE = REQUESTMODE;
    }

    public String getREQUESTSTATUS() {
        return REQUESTSTATUS;
    }

    public void setREQUESTSTATUS(String REQUESTSTATUS) {
        this.REQUESTSTATUS = REQUESTSTATUS;
    }

    public String getREQUESTTYPE() {
        return REQUESTTYPE;
    }

    public void setREQUESTTYPE(String REQUESTTYPE) {
        this.REQUESTTYPE = REQUESTTYPE;
    }

    public Integer getRowNumber() {
        return RowNumber;
    }

    public void setRowNumber(Integer rowNumber) {
        RowNumber = rowNumber;
    }

    public String getTransactionID() {
        return TransactionID;
    }

    public void setTransactionID(String transactionID) {
        TransactionID = transactionID;
    }

    public String getVeriCode() {
        return VeriCode;
    }

    public void setVeriCode(String veriCode) {
        this.VeriCode = veriCode;
    }

    public void setCOUNTRY(String COUNTRY) {
        this.COUNTRY = COUNTRY;
    }

    public String getFIRSTNAME() {
        return FIRSTNAME;
    }

    public void setFIRSTNAME(String FIRSTNAME) {
        this.FIRSTNAME = FIRSTNAME;
    }

    public String getLASTNAME() {
        return LASTNAME;
    }

    public void setLASTNAME(String LASTNAME) {
        this.LASTNAME = LASTNAME;
    }

    public String getEVENTDATE() {
        if(EVENTDATE == null){
            EVENTDATE = DELIVERY;
        }
        return EVENTDATE;
    }

    public void setEVENTDATE(String EVENTDATE) {
        this.EVENTDATE = EVENTDATE;
    }

    public String getDROPOFFDATE() {
        return DROPOFFDATE;
    }

    public void setDROPOFFDATE(String DROPOFFDATE) {
        this.DROPOFFDATE = DROPOFFDATE;
    }

    public String getSituation() {
        return Situation;
    }

    public void setSituation(String situation) {
        Situation = situation;
    }
    public String getNUMBEROFKIDS() {
        return NUMBEROFKIDS;
    }

    public void setNUMBEROFKIDS(final String NUMBEROFKIDS) {
        this.NUMBEROFKIDS = NUMBEROFKIDS;
    }

    public void setEMAIL1(final String EMAIL1) {
        this.EMAIL1 = EMAIL1;
    }

    public String getCHECKOUT() {
        return CHECKOUT;
    }

    public void setCHECKOUT(final String CHECKOUT) {
        this.CHECKOUT = CHECKOUT;
    }

    public String getCHECKIN() {
        return CHECKIN;
    }

    public void setCHECKIN(final String CHECKIN) {
        this.CHECKIN = CHECKIN;
    }
    private ConciergeRequestDetail initConciergeRequestDetail(String requestDetailStr){
        ConciergeRequestDetail conciergeRequestDetail= new ConciergeRequestDetail();
        String[] requestDetailItems = requestDetailStr.split("\\s*\\|\\s*");
        if(requestDetailItems != null && requestDetailItems.length > 0) {
            JSONObject jsonObject = new JSONObject();
            for(String item : requestDetailItems){
                String[] keyValue = StringUtil.getKeyValueFromSplitor(item, "");
                try {
                    if(keyValue != null && keyValue.length == 2) {
                        jsonObject.put(keyValue[0], keyValue[1]);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if(jsonObject.length() > 0){
                // Deserialize
                conciergeRequestDetail = new Gson().fromJson(jsonObject.toString(), ConciergeRequestDetail.class);
            }
        }

        return conciergeRequestDetail;
    }
}
