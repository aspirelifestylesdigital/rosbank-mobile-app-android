package com.rosbank.android.russia.apiservices.b2c;

import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CApiProviderClient;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CICallback;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.B2CGetRecentRequestRequest;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CGetRecentRequestResponse;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.utils.SharedPreferencesUtils;

import java.util.List;
import java.util.Map;

import retrofit2.Callback;

public class B2CWSGetRecentRequest
        extends B2CApiProviderClient<List<B2CGetRecentRequestResponse>>{

    public B2CWSGetRecentRequest(B2CICallback callback){
        this.b2CICallback = callback;
    }
    private B2CGetRecentRequestRequest request;
    public void setPage(int page){
        request = new B2CGetRecentRequestRequest().build(page);
    }
    @Override
    public void run(Callback<List<B2CGetRecentRequestResponse>> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }
    @Override
    protected boolean isUseAuthenticateInApi() {
        return true;
    }

    @Override
    protected void runApi() {
        request.setAccessToken(SharedPreferencesUtils.getPreferences(AppConstant.PRE_B2C_ACCESS_TOKEN, ""));
        serviceInterface.getRecentRequests(request).enqueue(this);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        return null;
    }

    @Override
    protected void postResponse(List<B2CGetRecentRequestResponse> response) {
    }
}
