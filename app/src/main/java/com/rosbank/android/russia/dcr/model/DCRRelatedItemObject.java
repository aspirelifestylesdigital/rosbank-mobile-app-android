package com.rosbank.android.russia.dcr.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

import java.io.Serializable;


public class DCRRelatedItemObject
        implements Serializable {
    public DCRRelatedItemObject(String name){
        this.name = name;
    }
    @Expose
    private String id;

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    @Expose
    private String name;

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public DCRCommonObject getType() {
        return type;
    }

    public void setType(final DCRCommonObject type) {
        this.type = type;
    }

    @Expose
    private String description;
    @Expose
    private DCRCommonObject type;

    @Override
    public boolean equals(Object obj) {
        if(obj != null && obj instanceof DCRRelatedItemObject){
            DCRRelatedItemObject commonObject = (DCRRelatedItemObject)obj;
            return (name != null && name.equals(commonObject.getName())) ||
                    (id != null && id.equals(commonObject.getId()));
        }
        return false;
    }

}
