package com.rosbank.android.russia.push;

import android.app.IntentService;
import android.content.Intent;


import com.google.firebase.iid.FirebaseInstanceId;
import com.liveperson.messaging.sdk.api.LivePerson;
import com.rosbank.android.russia.application.AppConstant;


/**
 * Created by nirni on 11/20/16.
 */

public class FirebaseRegistrationIntentService extends IntentService {

    public static final String TAG = FirebaseRegistrationIntentService.class.getSimpleName();

    public FirebaseRegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        String token = FirebaseInstanceId.getInstance().getToken();
        String account = AppConstant.BRAND_ID;
        String appID = AppConstant.APP_PACKAGE_NAME;
        LivePerson.registerLPPusher(account, appID, token);
        // Notify UI that registration has completed, so the progress indicator can be hidden.
        // Intent registrationComplete = new Intent(REGISTRATION_COMPLETE);
        // LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }
}
