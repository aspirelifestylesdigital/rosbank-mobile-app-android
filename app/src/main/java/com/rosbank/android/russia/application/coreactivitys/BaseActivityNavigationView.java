package com.rosbank.android.russia.application.coreactivitys;

import android.app.Fragment;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.utils.StringUtil;

import java.util.List;

public abstract class BaseActivityNavigationView
        extends AbstractActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    protected Toolbar toolbar;
    protected FloatingActionButton fab;
    protected NavigationView navigationView;
    protected DrawerLayout drawer;
    //============
    protected ImageView mIvLogoAppToolbar;
    protected ImageView mIvBack;
    protected TextView mTvTitle;
    protected TextView mTvRightTitle;

    protected abstract void onToolbarOpened(View drawerView);
    protected abstract void onToolbarClosed(View drawerView);

    @Override
    protected int layoutContainerId() {
        return R.id.base_layout_container;
    }

    @Override
    public void setTitle(String title) {
        mTvTitle.setText(title);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_navigationview);
        //
        //ButterKnife.bind(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mIvLogoAppToolbar = (ImageView) findViewById(R.id.ivLogoAppToolbar);
        mIvBack = (ImageView) findViewById(R.id.ic_back);
        mTvTitle = (TextView) findViewById(R.id.tv_tool_bar_title);
        mTvRightTitle = (TextView) findViewById(R.id.tv_right);
         /**
         * initial toolbar & navigationview
         * */
        //setup toolbar display menu icon on right conner.
        toolbar.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this,
                drawer,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close) {
            @Override
            public void onDrawerOpened(final View drawerView) {
                //showOrHideSignInOutItem();
                onToolbarOpened(drawerView);
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                onToolbarClosed(drawerView);
                super.onDrawerClosed(drawerView);
            }
        };

        drawer.setDrawerListener(toggle);
        toggle.syncState();

        toolbar.setNavigationIcon(R.drawable.menu);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerOpen(Gravity.RIGHT)) {
                    drawer.closeDrawer(Gravity.RIGHT);
                } else {
                    drawer.openDrawer(Gravity.RIGHT);
                }
            }
        });

        //
        navigationView.setNavigationItemSelectedListener(this);

        /**
         * setup view
         * */
        parentContainer = (FrameLayout) findViewById(R.id.base_layout_container);

        //
        int layoutId = activityLayoutId();
        if (layoutId > 0) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(layoutId,
                                           null);

            parentContainer.addView(layout);
        }

        //
        initView();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {
            boolean handled = false;
            android.support.v4.app.Fragment f = getCurrentFragment() ;
                if(f instanceof BaseFragment) {
                    handled = ((BaseFragment)f).onBack();
                    if(!handled) {
                        super.onBackPressed();
                    }else{
                        ((BaseFragment) f).onBackPress();
                }
            }else {
                    super.onBackPressed();
                }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.END);

        return true;
    }

    @Override
    public void openNavMenu() {
        //TODO: call activity open navigation menu
        if (!drawer.isDrawerOpen(Gravity.RIGHT)) {
            drawer.openDrawer(Gravity.RIGHT);
        }
    }

    @Override
    public void closeNavMenu() {
        //TODO: call activity close navigation menu
        if (drawer.isDrawerOpen(Gravity.RIGHT)) {
            drawer.closeDrawer(Gravity.RIGHT);
        }
    }

    public void hideToolbarMenuIcon(){
        /*int hCount = navigationView.getHeaderCount();
        if(hCount>0)
            navigationView.getHeaderView(0).setVisibility(View.GONE);*/

        //toolbar.setVisibility(View.GONE);
        toolbar.setNavigationIcon(null);
        closeNavMenu();
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    public void showToolbarMenuIcon(){
        /*int hCount = navigationView.getHeaderCount();
        if(hCount>0)
            navigationView.getHeaderView(0).setVisibility(View.VISIBLE);*/

        //toolbar.setVisibility(View.VISIBLE);
        toolbar.setNavigationIcon(R.drawable.menu);
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);

    }

    public void showRightText(String text, View.OnClickListener onClickListener){
            if(!StringUtil.isEmpty(text)){
                mTvRightTitle.setText(text);
            }
            mTvRightTitle.setVisibility(View.VISIBLE);
            mTvRightTitle.setOnClickListener(onClickListener);
    }

    public void hideRightText(){
        mTvRightTitle.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showFloatingActionButton(){
        fab.show();
    }

    @Override
    public void hideFloatingActionButton(){
        fab.hide();
    }

}
