package com.rosbank.android.russia.dcr.api;

import android.text.TextUtils;


import com.rosbank.android.russia.BuildConfig;
import com.rosbank.android.russia.apiservices.apis.core.BaseApiRestClient;
import com.rosbank.android.russia.dcr.api.ResponseModel.DCRBaseResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public abstract class DCRApiProviderClient<T> extends BaseApiRestClient<T>
        implements Callback<T>{

    protected DCRApiInterface serviceInterface;
    protected DCRICallback dcrICallback;
    protected T callbackResponse;
    protected boolean isCallbackInstantly = true; // Some flows need to call several APIs then complete
    public DCRApiProviderClient() {
        super();
    }

    @Override
    protected void initRestAdapter() {
        rtfAdapter = new Retrofit.Builder()
                .baseUrl(BuildConfig.WS_DCR_ROOT_URL)
                .client(sOkHttpClient)
                .addConverterFactory(GsonConverterFactory.create(sGson))
                .build();
        serviceInterface = rtfAdapter.create(DCRApiInterface.class);
    }

    @Override
    protected boolean isAuthenticateValidate() {
        return false;
    }

    protected abstract void postResponse(T response);
    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        callbackResponse = response.body();
        if(callbackResponse != null){
            if(callbackResponse instanceof DCRBaseResponse) {
                if (((DCRBaseResponse) callbackResponse).isSuccess()) {
                    postResponse(callbackResponse);
                    if (isCallbackInstantly) {
                        dcrICallback.onDCRResponse((DCRBaseResponse) callbackResponse);
                    }
                } else {
                    if (dcrICallback != null) {
                        dcrICallback.onDCRFailure(((DCRBaseResponse) callbackResponse).getMessage(), ((DCRBaseResponse) callbackResponse).getErrorCode());
                    }
                }
            }else if(callbackResponse instanceof List){
                List responseList = (List)callbackResponse;
                if(responseList.size() > 0){
                    dcrICallback.onDCRResponseOnList((List)callbackResponse);
                }else{
                    dcrICallback.onDCRFailure("", "");
                }
            }
        }else{
            if(dcrICallback != null){
                dcrICallback.onDCRFailure(getMessageErrorFromLog(), getErrorCodeFromLog());
            }
        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        if(dcrICallback != null){
            dcrICallback.onDCRFailure(getMessageErrorFromLog(), getErrorCodeFromLog());
        }
    }
    public String getMessageErrorFromLog(){
        if(!TextUtils.isEmpty(messageErrorWhenFailed)){
            return messageErrorWhenFailed;
        }
        if(!TextUtils.isEmpty(originalMessageErrorWhenFailed)){
            try {
                JSONObject jsonObject = new JSONObject(originalMessageErrorWhenFailed);
                if(jsonObject != null){
                    JSONArray messageJsonArr = jsonObject.optJSONArray("errors");
                    if(messageJsonArr != null && messageJsonArr.length() > 0){
                        JSONObject messageJsonObj = messageJsonArr.getJSONObject(0);
                        messageErrorWhenFailed = messageJsonObj.optString("message");
                        errorCodeWhenFailed = messageJsonObj.optString("code");
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return messageErrorWhenFailed;
    }
    public String getErrorCodeFromLog(){
        return errorCodeWhenFailed;
    }
    @Override
    protected Map<String, String> prepareHeaders() {
        Map<String, String> map = new HashMap<>();
        map.put("Content-Type", "application/json");
        map.put("Ocp-Apim-Subscription-Key", BuildConfig.DCR_SUBSCRIPTION_KEY);
        return map;
    }
}