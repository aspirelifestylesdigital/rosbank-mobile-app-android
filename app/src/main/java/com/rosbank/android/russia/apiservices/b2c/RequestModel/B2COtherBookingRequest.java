package com.rosbank.android.russia.apiservices.b2c.RequestModel;

/**
 * Created by anh.tran on 11/4/2016.
 */

public class B2COtherBookingRequest extends B2CBookingRequest {

    public B2COtherBookingRequest() {
        super("Other Request");
    }

    @Override
    public void buildRequestDetails() {
        String details = SPECIAL_REQUIREMENT + getSpecialRequirement();
        setRequestDetails(details);
    }


}
