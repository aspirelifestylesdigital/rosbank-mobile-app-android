package com.rosbank.android.russia.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.adapter.TeeTimesAdapter;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by IT on 12/14/2016.
 */

public class TeeTimeFragment extends BaseFragment {
    public static final String PRE_SELECTION_DATA = "selection_data";
    private static final int TIME_START = 6;
    private static final int TIME_END = 17;

    public interface SelectionCallback {
        void onFinishSelection(Bundle bundle);
    }

    @BindView(R.id.lvTeeTimes)
    ListView lvTeeTimes;

    private SelectionCallback selectionCallback;
    private List<String> teeTimes = new ArrayList<>();
    private String selectedData;
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater,
                container,
                savedInstanceState);
        ButterKnife.bind(this,
                rootView);
        return rootView;
    }
    private void initTeeTimes(){
        teeTimes.clear();
        for(int index = TIME_START; index <= TIME_END; index++){
            String amPm = "AM";
            int hour = index;
            if(index >= 12){
                amPm = "PM";
                if(index > 12){
                    hour = index % 12;
                }
            }
            teeTimes.add(hour + ":00 " + amPm);
        }
    }
    public void setSelectionCallback(SelectionCallback selectionCallback){
        this.selectionCallback = selectionCallback;
    }
    public void setSelectedData(String selectedData){
        this.selectedData = selectedData;
    }
    @Override
    protected int layoutId() {
        return R.layout.fragment_tee_times;
    }

    @Override
    protected void initView() {
        initTeeTimes();
        lvTeeTimes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> adapterView,
                                    final View view,
                                    final int i,
                                    final long l) {
                if (selectionCallback != null) {
                    Bundle bundle = new Bundle();
                    if(!teeTimes.get(i).equals(selectedData)) {
                    bundle.putSerializable(PRE_SELECTION_DATA,
                            teeTimes.get(i));
                    }
                    selectionCallback.onFinishSelection(bundle);
                }
                onBackPress();

            }
        });
        TeeTimesAdapter teeTimesAdapter = new TeeTimesAdapter(getContext(), teeTimes, selectedData);
        lvTeeTimes.setAdapter(teeTimesAdapter);
    }

    @Override
    protected void bindData() {

    }

    @Override
    protected boolean onBack() {
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle(getString(R.string.text_tee_times));
        showLogoApp(false);
        hideToolbarMenuIcon();
    }
}
