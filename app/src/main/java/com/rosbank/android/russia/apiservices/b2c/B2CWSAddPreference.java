package com.rosbank.android.russia.apiservices.b2c;

import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CApiProviderClient;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CICallback;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.B2CUpsertPreferenceRequest;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CUpsertPreferenceResponse;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.utils.SharedPreferencesUtils;

import java.util.Map;

import retrofit2.Callback;

public class B2CWSAddPreference
        extends B2CApiProviderClient<B2CUpsertPreferenceResponse>{

    public B2CWSAddPreference(B2CICallback callback){
        this.b2CICallback = callback;
    }
    private B2CUpsertPreferenceRequest request;
    public void setRequest(B2CUpsertPreferenceRequest b2CUpsertPreferenceRequest){
        request = b2CUpsertPreferenceRequest;
    }
    @Override
    public void run(Callback<B2CUpsertPreferenceResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }
    @Override
    protected boolean isUseAuthenticateInApi() {
        return true;
    }

    @Override
    protected void runApi() {
        request.getMember().setAccessToken(SharedPreferencesUtils.getPreferences(AppConstant.PRE_B2C_ACCESS_TOKEN, ""));
        serviceInterface.addPreferences(request).enqueue(this);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        return null;
    }

    @Override
    protected void postResponse(B2CUpsertPreferenceResponse response) {
    }
}
