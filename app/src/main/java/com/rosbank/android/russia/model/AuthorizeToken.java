package com.rosbank.android.russia.model;


import com.rosbank.android.russia.utils.CommonUtils;
import com.google.gson.annotations.Expose;

public class AuthorizeToken {
    @Expose
    private String AuthorizeToken;
    @Expose
    private String CreatedAt;
    @Expose
    private String ExpiredAt;

    public String getAuthorizeToken() {
        return AuthorizeToken;
    }

    public void setAuthorizeToken(String authorizeToken) {
        AuthorizeToken = authorizeToken;
    }

    public String getCreatedAt() {
        return CreatedAt;
    }

    public void setCreatedAt(String createdAt) {
        CreatedAt = createdAt;
    }

    public String getExpiredAt() {
        return ExpiredAt;
    }

    public void setExpiredAt(String expiredAt) {
        ExpiredAt = expiredAt;
    }

    public static Boolean isExpired(String createAt, String expiredAt) {
        long lCreate = Long.parseLong(createAt);
        long lExpired = Long.parseLong(expiredAt);

        return isExpired(lCreate, lExpired);
    }

    public static Boolean isExpired(long createAt, long expiredAt) {
        long temp = CommonUtils.getGMTTime();
        long curr = System.currentTimeMillis();
        if (expiredAt <= temp)
            return true;
        return false;

    }

}