package com.rosbank.android.russia.dcr.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

import java.io.Serializable;


public class DCROpeningHourObject
        implements Serializable {

    public String getOpening_from_day() {
        return opening_from_day;
    }

    public void setOpening_from_day(final String opening_from_day) {
        this.opening_from_day = opening_from_day;
    }

    public String getOpening_to_day() {
        return opening_to_day;
    }

    public void setOpening_to_day(final String opening_to_day) {
        this.opening_to_day = opening_to_day;
    }

    public String getOpening_from_hour() {
        return opening_from_hour;
    }

    public void setOpening_from_hour(final String opening_from_hour) {
        this.opening_from_hour = opening_from_hour;
    }

    public String getOpening_to_hour() {
        if(opening_to_hour.contains("24:00")){
            opening_to_hour = "23:59";
        }
        return opening_to_hour;
    }

    public void setOpening_to_hour(final String opening_to_hour) {
        this.opening_to_hour = opening_to_hour;
    }


    @Expose
    private String opening_from_day;



    @Expose
    private String opening_to_day;
    @Expose
    private String opening_from_hour;



    @Expose
    private String opening_to_hour;


    @Override
    public boolean equals(Object obj) {
        if(obj != null && obj instanceof DCROpeningHourObject){
            DCROpeningHourObject commonObject = (DCROpeningHourObject)obj;
            return (opening_from_day != null && opening_from_day.equals(commonObject.getOpening_from_day())) ||
                    (opening_from_hour != null && opening_from_hour.equals(commonObject.getOpening_from_hour()));
        }
        return false;
    }

    public DCROpeningHourObject() {
        this.opening_from_day= "";
        this.opening_from_hour = "";
        this.opening_to_day = "";
        this.opening_to_hour = "";
    }
    public DCROpeningHourObject(String openingFromDay, String openingFromHour, String openingToDay, String openingToHour) {
        this.opening_from_day= openingFromDay;
        this.opening_from_hour = openingFromHour;
        this.opening_to_day = openingToDay;
        this.opening_to_hour = openingToHour;
    }

}
