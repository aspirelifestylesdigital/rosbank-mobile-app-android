package com.rosbank.android.russia.apiservices.ResponseModel;

import com.rosbank.android.russia.apiservices.apis.appimplement.BaseResponse;
import com.rosbank.android.russia.model.concierge.HotelBookingDetailData;

/**
 * Created by ThuNguyen on 10/26/2016.
 */

public class HotelBookingDetailResponse extends BaseResponse {
    private HotelBookingDetailData Data;

    public HotelBookingDetailData getData() {
        return Data;
    }

    public void setData(HotelBookingDetailData data) {
        Data = data;
    }
}
