package com.rosbank.android.russia.dcr.api;


import com.rosbank.android.russia.apiservices.ResponseModel.AuthenticateResponse;
import com.rosbank.android.russia.dcr.api.RequestModel.DCRGetPrivilegeDetailRequest;
import com.rosbank.android.russia.dcr.api.ResponseModel.DCRGetPrivilegeDetailResponse;

import java.util.Map;

import retrofit2.Callback;

public class DCRWSGetPrivilegeDetail
        extends DCRApiProviderClient<DCRGetPrivilegeDetailResponse> {

    public DCRWSGetPrivilegeDetail(DCRICallback callback){
        this.dcrICallback = callback;
    }
    private DCRGetPrivilegeDetailRequest request = new DCRGetPrivilegeDetailRequest();
    public void setRequest(DCRGetPrivilegeDetailRequest dcrBaseRequest){
        request = dcrBaseRequest;
    }

    @Override
    protected void runSignAuthenticate() {

    }

    @Override
    protected void authenticateSuccess(final AuthenticateResponse token) {

    }

    @Override
    protected String getAuthorizeToken() {
        return null;
    }

    @Override
    public void run(Callback<DCRGetPrivilegeDetailResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }

    @Override
    protected boolean isUseAuthenticateInApi() {
        return false;
    }

    @Override
    protected void runApi() {
        serviceInterface.getPrivilegeDetail(request.toMap()).enqueue(this);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        return super.prepareHeaders() ;
    }

    @Override
    protected void postResponse(DCRGetPrivilegeDetailResponse response) {
    }
}
