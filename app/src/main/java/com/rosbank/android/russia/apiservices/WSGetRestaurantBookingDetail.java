package com.rosbank.android.russia.apiservices;

import com.rosbank.android.russia.apiservices.ResponseModel.RestaurantBookingDetailResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.ApiProviderClient;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Callback;

/**
 * Created by ThuNguyen on 9/29/2016.
 */

public class WSGetRestaurantBookingDetail extends ApiProviderClient<RestaurantBookingDetailResponse> {
    private String bookingId;
    public void setBookingId(String bookingId){
        this.bookingId = bookingId;
    }
    @Override
    public void run(Callback<RestaurantBookingDetailResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }

    @Override
    protected boolean isUseAuthenticateInApi() {
        return true;
    }

    @Override
    protected void runApi() {
        serviceInterface.getRestaurantBookingDetail(bookingId).enqueue(callback);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        //return null;
        Map<String, String> headers = new HashMap<>();
        //headers.put("User-Agent", "Retrofit-Sample-App");
        return headers;
    }
}
