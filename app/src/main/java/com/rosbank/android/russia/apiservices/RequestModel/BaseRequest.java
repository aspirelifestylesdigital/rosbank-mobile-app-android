package com.rosbank.android.russia.apiservices.RequestModel;


public class BaseRequest {
    public String mAuthorizeToken;

    public void setAuthorizeToken(String token) {
        this.mAuthorizeToken = token;
    }

    public String getAuthorizeToken() {
        return mAuthorizeToken;
    }
}
