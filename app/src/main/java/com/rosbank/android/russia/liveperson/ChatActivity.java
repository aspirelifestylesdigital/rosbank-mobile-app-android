package com.rosbank.android.russia.liveperson;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.liveperson.infra.BadArgumentException;
import com.liveperson.infra.CampaignInfo;
import com.liveperson.infra.ConversationViewParams;
import com.liveperson.infra.ICallback;
import com.liveperson.infra.LPAuthenticationParams;
import com.liveperson.infra.messaging_ui.fragment.ConversationFragment;
import com.liveperson.messaging.sdk.api.LivePerson;
import com.liveperson.messaging.sdk.api.model.ConsumerProfile;
import com.rosbank.android.russia.R;
import com.rosbank.android.russia.activitys.HomeActivity;
import com.rosbank.android.russia.application.coreactivitys.AbstractActivity;
import com.rosbank.android.russia.model.UserItem;
import com.rosbank.android.russia.push.FCMUtils;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.SharedPreferencesUtils;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.rosbank.android.russia.activitys.HomeActivity.CAMPAIGN_ID;
import static com.rosbank.android.russia.activitys.HomeActivity.CAMPAIGN_SAVED;
import static com.rosbank.android.russia.activitys.HomeActivity.ENGAGEMENT_CONTEXT_ID;
import static com.rosbank.android.russia.activitys.HomeActivity.ENGAGEMENT_ID;
import static com.rosbank.android.russia.activitys.HomeActivity.SESSION_ID;

public class ChatActivity extends AbstractActivity implements LivepersonIntentHandler.ChatActivityCallback {

    private static final String TAG = ChatActivity.class.getSimpleName();
    private static final String LIVEPERSON_FRAGMENT = "liveperson_fragment";
    public static final int DIALOG_CLEAR_HISTORY = 1;
    public static final int DIALOG_MARK_AS_URGENT = 2;
    public static final int DIALOG_MARK_AS_NOT_URGENT = 3;
    public static final int DIALOG_MARK_AS_RESOLVED = 4;
    public static final int DIALOG_RESOLVE_CONVERSATION = 5;

    private ConversationFragment mConversationFragment;
    private Menu mMenu;
    private LivepersonIntentHandler mIntentsHandler;
    @BindView(R.id.toolbarChat)
    Toolbar toolbar;
    private ToolbarViews toolbarViews;
    private String agentName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
        }

        toolbarViews = new ToolbarViews();
        ButterKnife.bind(toolbarViews, toolbar);
        setTitle(getString(R.string.text_title_live_chat));
        mIntentsHandler = new LivepersonIntentHandler(this);
        setUserProfile();
        setCampaignInfo();
    }

    @Override
    protected int activityLayoutId() {
        return R.layout.activity_chat;
    }

    @Override
    protected int layoutContainerId() {
        return R.id.chat_container;
    }

    static class ToolbarViews {
        @BindView(R.id.tv_tool_bar_title)
        TextView mTvTitle;
    }

    private void setUserProfile() {
        FCMUtils.handleGCMRegistration(this);
        ConsumerProfile consumerProfile = new ConsumerProfile.Builder()
                .setFirstName(UserItem.getLoginedFirstName())
                .setLastName(UserItem.getLoginedLastName())
                .setPhoneNumber(UserItem.getLoginedMobileBumber() + "|" + UserItem.getLoginedEmail())
                .setAvatarUrl(UserItem.getAvatarUrl())
                .build();
        LivePerson.setUserProfile(consumerProfile);
    }

    private void initConversationFragment(CampaignInfo campaignInfo) {

        mConversationFragment = (ConversationFragment) getSupportFragmentManager().findFragmentByTag(LIVEPERSON_FRAGMENT);
        Log.d(ChatActivity.class.getSimpleName(), "initFragment. mConversationFragment = " + mConversationFragment);
        if (mConversationFragment == null) {
            String authCode = "";
            String publicKey = "";
            LPAuthenticationParams authParams = new LPAuthenticationParams();
            authParams.setAuthKey(authCode);
            authParams.addCertificatePinningKey(publicKey);
            ConversationViewParams conversationViewParams = new ConversationViewParams();
            conversationViewParams.setReadOnlyMode(false);
            if (campaignInfo != null) conversationViewParams.setCampaignInfo(campaignInfo);

            mConversationFragment = (ConversationFragment) LivePerson.getConversationFragment(authParams, conversationViewParams);

            // Pending intent for image foreground service
            Intent notificationIntent = new Intent(this, HomeActivity.class);
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
            LivePerson.setImageServicePendingIntent(pendingIntent);

            // Notification builder for image upload foreground service
            Notification.Builder uploadBuilder = new Notification.Builder(this.getApplicationContext());
            Notification.Builder downloadBuilder = new Notification.Builder(this.getApplicationContext());
            uploadBuilder.setContentTitle("Uploading image")
                    .setSmallIcon(android.R.drawable.arrow_up_float)
                    .setContentIntent(pendingIntent)
                    .setProgress(0, 0, true);

            downloadBuilder.setContentTitle("Downloading image")
                    .setSmallIcon(android.R.drawable.arrow_down_float)
                    .setContentIntent(pendingIntent)
                    .setProgress(0, 0, true);

            LivePerson.setImageServiceUploadNotificationBuilder(uploadBuilder);
            LivePerson.setImageServiceDownloadNotificationBuilder(downloadBuilder);
            attachFragment();
        } else {
            attachFragment();
        }
    }

    private void setCampaignInfo() {

        if (isCampaignSaved() || isEngagementExists()) {

            String currentCampaignID = SharedPreferencesUtils.getPreferences(CAMPAIGN_ID, "campaignID");
            String currentEngagementID = SharedPreferencesUtils.getPreferences(ENGAGEMENT_ID, "engagementID");
            String currentEngagementContextID = SharedPreferencesUtils.getPreferences(ENGAGEMENT_CONTEXT_ID, "engagementContextID");
            String sessionID = SharedPreferencesUtils.getPreferences(SESSION_ID, "sessionID");
            String visitorID = SharedPreferencesUtils.getPreferences(HomeActivity.VISITOR_ID, "visitorID");

            CampaignInfo campaignInfo;
            try {
                campaignInfo = new CampaignInfo(
                        Long.valueOf(currentCampaignID),
                        Long.valueOf(currentEngagementID),
                        currentEngagementContextID,
                        sessionID,
                        visitorID);
                initConversationFragment(campaignInfo);
            } catch (BadArgumentException e) {
                e.printStackTrace();
            }
        } else {
            initConversationFragment(null);
        }
    }

    public boolean isCampaignSaved() {
        return SharedPreferencesUtils.getPreferences(CAMPAIGN_SAVED, false);
    }

    public boolean isEngagementExists() {
        return !Objects.equals(SharedPreferencesUtils.getPreferences(ENGAGEMENT_ID, "0"), "0");
    }

    private void attachFragment() {
        clearAllFragments();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.addToBackStack(mConversationFragment.getClass().getName());
        ft.replace(layoutContainerId(), mConversationFragment, ConversationFragment.class.getSimpleName()).commitAllowingStateLoss();
        setTitle(getString(R.string.text_title_live_chat));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mConversationFragment != null) {
            attachFragment();
        }
        removeChatNotifications();
        if (agentName != null && !agentName.isEmpty()) {
            setTitle(agentName.toUpperCase());
        }
        if (!mIntentsHandler.getIsConversationActive()){
            refreshToolbarAndMenu();
        }
    }

    private void removeChatNotifications() {
        NotificationManager nManager = ((NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE));
        if (nManager != null) {
            nManager.cancelAll();
        }
    }

    @Override
    public void onBackPressed() {
        if (mConversationFragment == null || !mConversationFragment.onBackPressed()) {
            super.onBackPressed();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_chat, menu);
        mMenu = menu;
        changeOptionsMenuIcon();
        return true;
    }

    private void changeOptionsMenuIcon() {
        Drawable drawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.menu);
        toolbar.setOverflowIcon(drawable);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        ChatMenuDialog chatMenuDialog;
        switch (id) {
            case R.id.clear_history:
                // check if the history is resolved,if not skip the clear command and notify the user.
                LivePerson.checkActiveConversation(new ICallback<Boolean, Exception>() {
                    @Override
                    public void onSuccess(Boolean aBoolean) {
                        ChatMenuDialog chatMenuDialog;
                        if (!aBoolean) {
                            chatMenuDialog = ChatMenuDialog.newInstance(R.string.clear_history, R.string.alert_clear_history, DIALOG_CLEAR_HISTORY);
                            chatMenuDialog.show(getSupportFragmentManager(), "clear");
                        } else {
                            chatMenuDialog = ChatMenuDialog.newInstance(R.string.clear_history, R.string.resolve_conversation_first, DIALOG_RESOLVE_CONVERSATION);
                            chatMenuDialog.show(getSupportFragmentManager(), "resolveFirstAlert");
                        }
                    }

                    @Override
                    public void onError(Exception e) {
                        Log.e(TAG, e.getMessage());
                    }
                });

                break;
            case R.id.mark_as_resolved:
                chatMenuDialog = ChatMenuDialog.newInstance(R.string.resolve_conversation, R.string.alert_resolve_chat, DIALOG_MARK_AS_RESOLVED);
                chatMenuDialog.show(getSupportFragmentManager(), "resolve");
                break;
/*            case R.id.mark_as_urgent:
                if (item.getTitle().toString().equalsIgnoreCase(getString(R.string.mark_as_urgent))) {
                    chatMenuDialog = ChatMenuDialog.newInstance(R.string.mark_as_urgent, R.string.alert_mark_as_urgent, DIALOG_MARK_AS_URGENT);
                    chatMenuDialog.show(getSupportFragmentManager(), "urgent");
                } else {
                    chatMenuDialog = ChatMenuDialog.newInstance(R.string.dismiss_urgency, R.string.alert_dismiss_urgent, DIALOG_MARK_AS_NOT_URGENT);
                    chatMenuDialog.show(getSupportFragmentManager(), "notUrgent");
                }
                break;*/
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
    /*    if (mIntentsHandler.getIsConversationUrgent()) {
            menu.getItem(0).setTitle(getResources().getString(R.string.dismiss_urgency));
        } else {
            menu.getItem(0).setTitle(getResources().getString(R.string.mark_as_urgent));
        }*/

        if (mIntentsHandler.getIsConversationActive()) {
            menu.setGroupEnabled(R.id.grp_urgent, true);
        } else {
            menu.setGroupEnabled(R.id.grp_urgent, false);
        }
        menu.setGroupEnabled(R.id.grp_clear, true);
        return true;
    }

    public void markAsUrgent() {

        LivePerson.markConversationAsUrgent();
        mIntentsHandler.setIsConversationUrgent(true);
    }

    public void markAsNotUrgent() {
        LivePerson.markConversationAsNormal();
        mIntentsHandler.setIsConversationUrgent(false);
    }

    private void refreshToolbarAndMenu() {
        invalidateOptionsMenu();
        setTitle(getString(R.string.text_title_live_chat));
        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
        }
    }


    @Override
    protected void initView() {

    }

    @Override
    public void openNavMenu() {

    }

    @Override
    public void closeNavMenu() {

    }

    @Override
    public void setTitle(String title) {
        toolbarViews.mTvTitle.setText(title);
    }

    @Override
    public void showFloatingActionButton() {

    }

    @Override
    public void hideFloatingActionButton() {

    }

    @Override
    public void onCsatDismissed() {
        refreshToolbarAndMenu();
    }

    @Override
    public void finishChatScreen() {
        refreshToolbarAndMenu();
    }

    @Override
    public void onCsatLaunched() {
        CommonUtils.hideSoftKeyboard(this);
        setTitle("");
        this.agentName = "";
        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_grey);
        }
        if (mMenu != null) {
            mMenu.close();
            mMenu.clear();
        }
    }

    @Override
    public void setAgentName(String agentName) {
        this.agentName = agentName;
        setTitle(agentName.toUpperCase());
    }

    @Override
    public void onConversationResolved() {

    }
}
