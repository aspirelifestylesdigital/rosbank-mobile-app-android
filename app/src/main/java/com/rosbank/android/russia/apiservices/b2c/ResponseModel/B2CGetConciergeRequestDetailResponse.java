package com.rosbank.android.russia.apiservices.b2c.ResponseModel;

import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.google.gson.annotations.Expose;


public class B2CGetConciergeRequestDetailResponse
        extends B2CBaseResponse {
    @Expose
    public String ATTACHMENTPATH;
    @Expose
    public String CHECKIN;
    @Expose
    public String CHECKINDATE;
    @Expose
    public String CHECKOUT;
    @Expose
    public String CHECKOUTDATE;
    @Expose
    public String CITY;
    @Expose
    public String CREATEDDATE;
    @Expose
    public String DELIVERY;
    @Expose
    public String EMAILADDRESS1;
    @Expose
    public String EPCCaseID;
    @Expose
    public String EVENTNAME;
    @Expose
    public String FUNCTIONALITY;
    @Expose
    public String NUMBEROFADULTS;
    @Expose
    public String NUMBEROFKIDS;
    @Expose
    public String NUMBEROFCHILDREN;
    @Expose
    public String PHONENUMBER;
    @Expose
    public String PICKUP;
    @Expose
    public String PREFRESPONSE;
    @Expose
    public String REQUESTDETAILS;
    @Expose
    public String REQUESTMODE;
    @Expose
    public String REQUESTSTATUS;
    @Expose
    public String REQUESTTYPE;
    @Expose
    public String TransactionID;
    @Expose
    public String VeriCode;

    public String getCHECKINDATE() {
        return CHECKINDATE;
    }

    public void setCHECKINDATE(String CHECKINDATE) {
        this.CHECKINDATE = CHECKINDATE;
    }

    public String getCHECKOUTDATE() {
        return CHECKOUTDATE;
    }

    public void setCHECKOUTDATE(String CHECKOUTDATE) {
        this.CHECKOUTDATE = CHECKOUTDATE;
    }

    public String getNUMBEROFCHILDREN() {
        return NUMBEROFCHILDREN;
    }

    public void setNUMBEROFCHILDREN(String NUMBEROFCHILDREN) {
        this.NUMBEROFCHILDREN = NUMBEROFCHILDREN;
    }

    public String getATTACHMENTPATH() {
        return ATTACHMENTPATH;
    }

    public void setATTACHMENTPATH(String ATTACHMENTPATH) {
        this.ATTACHMENTPATH = ATTACHMENTPATH;
    }

    public String getCHECKIN() {
        return CHECKIN;
    }

    public void setCHECKIN(String CHECKIN) {
        this.CHECKIN = CHECKIN;
    }

    public String getCHECKOUT() {
        return CHECKOUT;
    }

    public void setCHECKOUT(String CHECKOUT) {
        this.CHECKOUT = CHECKOUT;
    }

    public String getCITY() {
        return CITY;
    }

    public void setCITY(String CITY) {
        this.CITY = CITY;
    }

    public String getCREATEDDATE() {
        return CREATEDDATE;
    }

    public void setCREATEDDATE(String CREATEDDATE) {
        this.CREATEDDATE = CREATEDDATE;
    }

    public String getDELIVERY() {
        return DELIVERY;
    }

    public void setDELIVERY(String DELIVERY) {
        this.DELIVERY = DELIVERY;
    }

    public String getEMAILADDRESS1() {
        return EMAILADDRESS1;
    }

    public void setEMAILADDRESS1(String EMAILADDRESS1) {
        this.EMAILADDRESS1 = EMAILADDRESS1;
    }

    public String getEPCCaseID() {
        return EPCCaseID;
    }

    public void setEPCCaseID(String EPCCaseID) {
        this.EPCCaseID = EPCCaseID;
    }

    public String getEVENTNAME() {
        return EVENTNAME;
    }

    public void setEVENTNAME(String EVENTNAME) {
        this.EVENTNAME = EVENTNAME;
    }

    public String getFUNCTIONALITY() {
        return FUNCTIONALITY;
    }

    public void setFUNCTIONALITY(String FUNCTIONALITY) {
        this.FUNCTIONALITY = FUNCTIONALITY;
    }

    public String getNUMBEROFADULTS() {
        return NUMBEROFADULTS;
    }

    public void setNUMBEROFADULTS(String NUMBEROFADULTS) {
        this.NUMBEROFADULTS = NUMBEROFADULTS;
    }

    public String getNUMBEROFKIDS() {
        return NUMBEROFKIDS;
    }

    public void setNUMBEROFKIDS(String NUMBEROFKIDS) {
        this.NUMBEROFKIDS = NUMBEROFKIDS;
    }

    public String getPHONENUMBER() {
        return PHONENUMBER;
    }

    public void setPHONENUMBER(String PHONENUMBER) {
        this.PHONENUMBER = PHONENUMBER;
    }

    public String getPICKUP() {
        return PICKUP;
    }

    public void setPICKUP(String PICKUP) {
        this.PICKUP = PICKUP;
    }

    public String getPREFRESPONSE() {
        return PREFRESPONSE;
    }

    public void setPREFRESPONSE(String PREFRESPONSE) {
        this.PREFRESPONSE = PREFRESPONSE;
    }

    public String getREQUESTDETAILS() {
        return REQUESTDETAILS;
    }

    public void setREQUESTDETAILS(String REQUESTDETAILS) {
        this.REQUESTDETAILS = REQUESTDETAILS;
    }

    public String getREQUESTMODE() {
        return REQUESTMODE;
    }

    public void setREQUESTMODE(String REQUESTMODE) {
        this.REQUESTMODE = REQUESTMODE;
    }

    public String getREQUESTSTATUS() {
        return REQUESTSTATUS;
    }

    public void setREQUESTSTATUS(String REQUESTSTATUS) {
        this.REQUESTSTATUS = REQUESTSTATUS;
    }

    public String getREQUESTTYPE() {
        return REQUESTTYPE;
    }

    public void setREQUESTTYPE(String REQUESTTYPE) {
        this.REQUESTTYPE = REQUESTTYPE;
    }

    public String getTransactionID() {
        return TransactionID;
    }

    public void setTransactionID(String transactionID) {
        TransactionID = transactionID;
    }

    public String getVeriCode() {
        return VeriCode;
    }

    public void setVeriCode(String veriCode) {
        this.VeriCode = veriCode;
    }
}
