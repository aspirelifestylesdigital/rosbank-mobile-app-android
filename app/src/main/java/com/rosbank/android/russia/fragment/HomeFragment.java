package com.rosbank.android.russia.fragment;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.activitys.HomeActivity;
import com.rosbank.android.russia.adapter.CommonLandingAdapter;
import com.rosbank.android.russia.adapter.HomeAdapter;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CICallback;
import com.rosbank.android.russia.apiservices.b2c.B2CWSUpsertConciergeRequest;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.B2CUpsertConciergeRequestRequest;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CUpsertConciergeRequestResponse;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.application.AppContext;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.dcr.PrivilegesFragment;
import com.rosbank.android.russia.dcr.model.DCRPrivilegeObject;
import com.rosbank.android.russia.interfaces.OnRecyclerViewItemClickListener;
import com.rosbank.android.russia.model.CommonLandingObject;
import com.rosbank.android.russia.model.ConciergeRequestObject;
import com.rosbank.android.russia.model.UserItem;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.EntranceLock;
import com.rosbank.android.russia.utils.Fontfaces;
import com.rosbank.android.russia.utils.NetworkUtil;
import com.rosbank.android.russia.utils.StringUtil;
import com.rosbank.android.russia.widgets.CustomErrorView;

import org.greenrobot.eventbus.EventBus;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.view.View.GONE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

/**
 * Created by anh.trinh on 9/13/2016.
 */
public class HomeFragment extends BaseFragment implements HomeAdapter.OnHomePageClickListener
        , B2CICallback {

    @BindView(R.id.svHome)
    NestedScrollView svHome;
    @BindView(R.id.homeViewBorderLayout)
    View homeViewBorderLayout;
    @BindView(R.id.homeSearchLayout)
    View homeSearchLayout;

    @BindView(R.id.rlArrow)
    View rlArrow;
    @BindView(R.id.imgArrowDown)
    ImageView imgArrowDown;
    @BindView(R.id.homeExperiencesRv)
    RecyclerView homeExperiencesRv;

    EntranceLock entranceLock = new EntranceLock();
    HomeSearchViewHolder homeSearchViewHolder;

    @Override
    protected int layoutId() {
        return R.layout.fragment_home;
    }

    @Override
    protected void initView() {
        homeSearchViewHolder = new HomeSearchViewHolder(homeSearchLayout);
        homeViewBorderLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            boolean isFirstTime;

            @Override
            public void onGlobalLayout() {
                if (homeViewBorderLayout.getHeight() > 0 && !isFirstTime) {
                    isFirstTime = true;
                    homeSearchLayout.getLayoutParams().height = homeViewBorderLayout.getHeight();
                    Log.d("ThuNguyen", "height = " + homeViewBorderLayout.getHeight());
                }
            }
        });

        initExperienceRv();

        svHome.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                int scrollY = svHome.getScrollY(); // For ScrollView
                // Log.d("ThuNguyen", "ScrollY = " + scrollY);
                if (scrollY != 0) {
                    setVisibilityForArrow(INVISIBLE);
                } else {
                    setVisibilityForArrow(VISIBLE);
                }
            }
        });
        rlArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onExpandClick(true);
            }
        });

    }

    public void makeArrowDance() {
        Animation mAnimation = new TranslateAnimation(
                TranslateAnimation.ABSOLUTE, 0f,
                TranslateAnimation.ABSOLUTE, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.4f);
        mAnimation.setDuration(500);
        mAnimation.setRepeatCount(-1);
        mAnimation.setRepeatMode(Animation.REVERSE);
        mAnimation.setInterpolator(new LinearInterpolator());
        imgArrowDown.setAnimation(mAnimation);
        imgArrowDown.invalidate();
    }

    public void resetDance() {
        imgArrowDown.setAnimation(null);
    }

    public void setVisibilityForArrow(int visibility) {
        rlArrow.setVisibility(visibility);
    }

    @Override
    protected void bindData() {
        homeSearchViewHolder.updateWelcom();
    }

    @Override
    public boolean onBack() {
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        showLogoApp(true);
        setSoftInputModeAdjustResize();
        updateWelcomText();
        resetPlacehoder();
        makeArrowDance();
        scrollToTop();
        ((HomeActivity) getActivity()).showFab(true);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        resetDance();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
        }
    }

    @Override
    public void onExpandClick(boolean expanded) {
        svHome.smoothScrollBy(0, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 200, getResources().getDisplayMetrics()));
    }

    @Override
    public void onFavouriteClick(int position) {

    }

    @Override
    public void onChatbotSubmit(String text) {
        AppContext.getSharedInstance().track(AppConstant.ANALYTIC_EVENT_CATEGORY_HOME, AppConstant.ANALYTIC_EVENT_ACTION_SEND_CONCIERGE_REQUEST, AppConstant.ANALYTIC_EVENT_CATEGORY_HOME);
        if (UserItem.isLogined()) {
            this.processBooking(text);
        } else {
            ((HomeActivity) getActivity()).showDialogRequiredSignIn();
        }
    }

    @Override
    public void showDialogNoConnection() {
        showDialogMessage(getString(R.string.text_title_dialog_error), getString(R.string.text_message_dialog_error_no_internet));
    }

    public void scrollToTop() {
        svHome.smoothScrollTo(0, 0);
    }

    public void updateWelcomText() {
        if (homeSearchViewHolder != null) {
            homeSearchViewHolder.updateWelcom();
        }
    }

    public void resetPlacehoder() {
        if (homeSearchViewHolder != null) {
            homeSearchViewHolder.resetPlaceHolder();
        }
    }

    private void processBooking(String text) {
        /*BookOtherRequest request = new BookOtherRequest();
        request.setWhatCanWeDo(text
                .toString()
                .trim());
        request.setPhone(true);
        request.setEmail(true);
        request.setBoth(true);
        request.setMobileNumber(UserItem.getLoginedMobileBumber());
        request.setEmailAddress(UserItem.getLoginedEmail());

        showDialogProgress();
        WSBookingOther wsBookingOther = new WSBookingOther(this);
        wsBookingOther.booking(request);
        wsBookingOther.run(this);*/
        B2CUpsertConciergeRequestRequest upsertConciergeRequestRequest = new B2CUpsertConciergeRequestRequest();
        upsertConciergeRequestRequest.setFunctionality(AppConstant.CONCIERGE_FUNCTIONALITY_TYPE.OTHER.getValue());
        upsertConciergeRequestRequest.setRequestType(AppConstant.CONCIERGE_REQUEST_TYPE.OTHER_REQUEST.getValue());
        upsertConciergeRequestRequest.setSituation("Situation");

        upsertConciergeRequestRequest.updateDefaultPersonalInformation();
        // Request details
        upsertConciergeRequestRequest.setRequestDetails(combineRequestDetails(text));

        showDialogProgress();
        B2CWSUpsertConciergeRequest b2CWSUpsertConciergeRequest = new B2CWSUpsertConciergeRequest(AppConstant.CONCIERGE_EDIT_TYPE.ADD, this);
        b2CWSUpsertConciergeRequest.setRequest(upsertConciergeRequestRequest);
        b2CWSUpsertConciergeRequest.run(null);
    }

    private String combineRequestDetails(String text) {
        String requestDetails = AppConstant.B2C_REQUEST_DETAIL_SPECIAL_REQ + text;
        if (!TextUtils.isEmpty(UserItem.getLoginedFullName())) {
            if (!TextUtils.isEmpty(requestDetails)) {
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_RESERVATION_NAME + StringUtil.removeAllSpecialCharacterAndBreakLine(UserItem.getLoginedFullName());
        }
        return requestDetails;
    }

    @Override
    public void onB2CResponse(B2CBaseResponse response) {
        if (getActivity() == null) {
            return;
        }
        if (response instanceof B2CUpsertConciergeRequestResponse) {
            if (response.isSuccess()) {
                //Reset Placeholder
                resetPlacehoder();
                scrollToTop();
                //goto thank you page
                ThankYouFragment thankYouFragment = new ThankYouFragment();
                EventBus.getDefault().post(AppConstant.CONCIERGE_EDIT_TYPE.ADD);
                pushFragment(thankYouFragment,
                        true,
                        true);
            } else {
//                showToast(response.getMessage());
                showDialogMessage("", getString(R.string.text_concierge_request_error));
            }
        }
        hideDialogProgress();
    }

    @Override
    public void onB2CResponseOnList(List<B2CBaseResponse> responseList) {
        if (getActivity() == null) {
            return;
        }
    }

    @Override
    public void onB2CFailure(String errorMessage, String errorCode) {
        hideDialogProgress();
        if (getActivity() == null) {
            return;
        }
        showDialogMessage("", getString(R.string.text_concierge_request_error));
    }

    public class HomeSearchViewHolder {
        @BindView(R.id.tvWelcome)
        public TextView tvWelcome;
        @BindView(R.id.tvHello)
        public TextView tvHello;
        @BindView(R.id.tvPlaceHolder)
        public TextView tvPlaceHolder;
        @BindView(R.id.etMessage)
        public EditText edtMessage;
        @BindView(R.id.etMessageError)
        public CustomErrorView etMessageError;
        @BindView(R.id.btnSubmit)
        public Button btnSubmit;
        @BindView(R.id.btnCancel)
        public Button btnCancel;

        public HomeSearchViewHolder(View view) {
            ButterKnife.bind(this, view);
            tvWelcome.setTypeface(Fontfaces.getFont(getContext(), Fontfaces.FONTLIST.AVENIR_NEXT_LT_MEDIUM));
            //tvWelcomeName.setTypeface(Fontfaces.getFont(context, Fontfaces.FONTLIST.AVENIR_NEXT_LT_MEDIUM));
            tvHello.setTypeface(Fontfaces.getFont(getContext(), Fontfaces.FONTLIST.AvenirLTStd_Light));
            btnSubmit.setTypeface(Fontfaces.getFont(getContext(), Fontfaces.FONTLIST.AvenirLTStd_Heavy));
            btnCancel.setTypeface(Fontfaces.getFont(getContext(), Fontfaces.FONTLIST.AvenirLTStd_Heavy));
            tvPlaceHolder.setTypeface(Fontfaces.getFont(getContext(),
                    Fontfaces.FONTLIST.AVENIR_NEXT_LT_MEDIUM));
            etMessageError.fillData(getContext().getString(R.string.text_sign_up_error_required_field));

            //edtMessage.setImeOptions(EditorInfo.IME_ACTION_DONE);
            //edtMessage.setImeActionLabel("AAAA", KeyEvent.KEYCODE_ENTER);

            updateWelcom();
            rotate(0);
            edtMessage.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(final View v,
                                          final boolean hasFocus) {
                    if (!hasFocus && edtMessage.getText().toString().trim().equals("")) {
                        resetPlacehoder();
                    } else {
                        edtMessage.setCursorVisible(true);
                        tvPlaceHolder.setAnimation(null);
                        tvPlaceHolder.setVisibility(View.GONE);
                        etMessageError.setVisibility(GONE);
                    }
                }
            });
        }

        public void rotate(int count) {

            //
            final int nextCount = count < 3 ?
                    count + 1 :
                    0;
            String[] strings = {getContext().getString(R.string.text_can_you_recommend),
                    getContext().getString(R.string.text_book_me_a_private_jet),
                    getContext().getString(R.string.text_book_a_private_chef),
                    getContext().getString(R.string.text_are_there_any_hidden)};
            tvPlaceHolder.setText(strings[count]);
            tvPlaceHolder.setAnimation(null);
            final Animation animationFadeIn = AnimationUtils.loadAnimation(getContext(),
                    R.anim.fade_in);
            final Animation animationFadeOut = AnimationUtils.loadAnimation(getContext(),
                    R.anim.fade_out);


            animationFadeIn.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {

                    new CountDownTimer(4000,
                            1000) {

                        public void onTick(long millisUntilFinished) {

                        }

                        public void onFinish() {
                            if (tvPlaceHolder.getVisibility() == View.VISIBLE) {
                                tvPlaceHolder.setAnimation(null);
                                tvPlaceHolder.startAnimation(animationFadeOut);
                            }
                        }
                    }.start();

                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            animationFadeOut.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {

                    if (nextCount >= 0) {
                        {
                            if (tvPlaceHolder.getVisibility() == View.VISIBLE) {
                                rotate(nextCount);
                            }
                        }
                    }

                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            animationFadeIn.setRepeatCount(0);
            animationFadeOut.setRepeatCount(0);
            tvPlaceHolder.setAnimation(null);
            tvPlaceHolder.startAnimation(animationFadeIn);
        }

        public void resetPlaceHolder() {
            edtMessage.setText("");
            edtMessage.clearFocus();
            edtMessage.setCursorVisible(false);
            etMessageError.setVisibility(GONE);
            tvPlaceHolder.setVisibility(View.VISIBLE);
            CommonUtils.hideSoftKeyboard(getContext(), edtMessage);
            rotate(0);
        }

        public void updateWelcom() {
            if (UserItem.isLogined()) {
                //tvWelcomeName.setText(UserItem.getLoginedFirstName() + " " + UserItem.getLoginedLastName());
                tvWelcome.setText(getContext().getString(R.string.text_welcome) + " " + UserItem.getLoginedFirstName() + " " + UserItem.getLoginedLastName());
                tvWelcome.setVisibility(View.VISIBLE);
            } else {
                String wl = getContext().getString(R.string.text_welcome).replace(",", "") + getContext().getString(R.string.text_guest);
                tvWelcome.setText(wl);
                tvWelcome.setVisibility(View.VISIBLE);
            }
        }

        public void setData() {
            updateWelcom();
        }

        @OnClick(R.id.btnSubmit)
        void onSubmitClick() {

            if (UserItem.isLogined()) {
                String text = edtMessage.getText()
                        .toString()
                        .trim();
                if (StringUtil.isEmpty(text)) {
                    tvPlaceHolder.setAnimation(null);
                    etMessageError.setVisibility(View.VISIBLE);
                    tvPlaceHolder.setVisibility(GONE);


                    return;
                }
                if (!NetworkUtil.getNetworkStatus(getContext())) {
                    showDialogNoConnection();
                    return;
                }
                if (!entranceLock.isClickContinuous()) {
                    HomeFragment.this.onChatbotSubmit(text);
                }

               /* String text = edtMessage.getText().toString().trim();
                if (StringUtil.isEmpty(text)) {
                    tvPlaceHolder.setAnimation(null);
                    etMessageError.setVisibility(View.VISIBLE);
                    tvPlaceHolder.setVisibility(GONE);
                    return;
                }
                etMessageError.setVisibility(GONE);

                edtMessage.setText("");*/
            } else {
                ((HomeActivity) getContext()).showDialogRequiredSignIn();
            }
        }

        @OnClick(R.id.btnCancel)
        void onCancelClick() {
            resetPlaceHolder();
        }
       /* @OnTouch({R.id.etMessage})
        boolean onInputTouchMessage(final View v,
                                  final MotionEvent event) {
          *//*  edtMessage.setCursorVisible(true);
            tvPlaceHolder.setAnimation(null);
            tvPlaceHolder.setVisibility(View.GONE);
            etMessageError.setVisibility(GONE);*//*

            return false;
        }*/

        public void onMenuClick() {

        }

        public void clearInputText() {
            edtMessage.setText("");
        }

    }

    private void initExperienceRv() {
        CommonLandingAdapter adapter = new CommonLandingAdapter(createDataConcierge(), (position, view) -> {
            if (UserItem.isLogined()) {
                if (view.getTag() instanceof CommonLandingObject) {
                    switch (((CommonLandingObject) view.getTag()).getImgId()) {
                        case R.drawable.img_experiences_item_gourmet:
                            //openPrivilegesFragment(DCRPrivilegeObject.PRIVILEGE_TYPE_RESTAURANT);
                            pushFragment(new ConciergeGourmetFragment(), true, true);
                            break;
                        case R.drawable.img_experiences_item_spa:
                            openPrivilegesFragment(DCRPrivilegeObject.PRIVILEGE_TYPE_SPA);
                            break;
                        case R.drawable.img_concierge_service_hotel:
                            // openPrivilegesFragment(DCRPrivilegeObject.PRIVILEGE_TYPE_HOTEL);
                            pushFragment(new BookingHotelFragment(), true, true);
                            break;
                        case R.drawable.img_concierge_service_entertainment:
                            pushFragment(new BookingEventFragment(), true, true);
                            break;
                        case R.drawable.img_concierge_service_rental:
                            pushFragment(new BookingCarRentalTransferFragment(), true, true);
                            break;
                        case R.drawable.img_concierge_service_golf:
                            pushFragment(new BookingGolfFragment(), true, true);
                            break;
                        case R.drawable.img_concierge_service_others:
                            pushFragment(new BookingOtherFragment(), true, true);
                            break;
                        default:
                            break;
                    }
                }
            } else {
                ((HomeActivity) getActivity()).showDialogRequiredSignIn();
            }
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(view.getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        homeExperiencesRv.setNestedScrollingEnabled(false);
        homeExperiencesRv.setLayoutManager(layoutManager);
        homeExperiencesRv.setAdapter(adapter);
    }

    private List<CommonLandingObject> createDataConcierge() {
        List<CommonLandingObject> data = new ArrayList<>();
        data.add(new CommonLandingObject(getString(R.string.privileges_title_dinning_item), R.drawable.img_experiences_item_gourmet));
        data.add(new CommonLandingObject(getString(R.string.privileges_title_hotel_item), R.drawable.img_concierge_service_hotel));
        //data.add(new CommonLandingObject(getString(R.string.privileges_title_spa_item), R.drawable.img_experiences_item_spa));
        data.add(new CommonLandingObject(getString(R.string.privileges_title_entertainment_item), R.drawable.img_concierge_service_entertainment));
        data.add(new CommonLandingObject(getString(R.string.privileges_title_rental_item), R.drawable.img_concierge_service_rental));
        data.add(new CommonLandingObject(getString(R.string.privileges_title_golf_item), R.drawable.img_concierge_service_golf));
        data.add(new CommonLandingObject(getString(R.string.privileges_title_other_item), R.drawable.img_concierge_service_others));
        return data;
    }

    private void openPrivilegesFragment(String privilegeType) {
        Bundle bundle = new Bundle();
        bundle.putString(PrivilegesFragment.PRIVILEGE_TYPE, privilegeType);

        PrivilegesFragment fragment = new PrivilegesFragment();
        fragment.setArguments(bundle);
        pushFragment(fragment, true, true);
    }
}
