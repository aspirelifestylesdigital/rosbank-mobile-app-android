package com.rosbank.android.russia.dcr.model;

import com.rosbank.android.russia.dcr.api.ResponseModel.DCRBaseResponse;
import com.rosbank.android.russia.model.BaseModel;
import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ThuNguyen on 4/5/2017.
 */

public class DCRAutocompleteSearchObject extends BaseModel implements Serializable {
    @Expose
    private String id;
    @Expose
    private String name;
    @Expose
    private DCRCommonObject item_type;
    @Expose
    private DCRCommonObject address_country;
    @Expose
    private DCRCommonObject address_state;
    @Expose
    private DCRCommonObject address_city;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DCRCommonObject getItem_type() {
        return item_type;
    }

    public void setItem_type(DCRCommonObject item_type) {
        this.item_type = item_type;
    }

    public DCRCommonObject getAddress_country() {
        return address_country;
    }

    public void setAddress_country(DCRCommonObject address_country) {
        this.address_country = address_country;
    }

    public DCRCommonObject getAddress_state() {
        return address_state;
    }

    public void setAddress_state(DCRCommonObject address_state) {
        this.address_state = address_state;
    }

    public DCRCommonObject getAddress_city() {
        return address_city;
    }

    public void setAddress_city(DCRCommonObject address_city) {
        this.address_city = address_city;
    }

    public static List<DCRAutocompleteSearchObject> cast(List<DCRBaseResponse> responseList){
        List<DCRAutocompleteSearchObject> dcrAutocompleteSearchObjectList = new ArrayList<>();
        if(responseList != null && responseList.size() > 0){

        }
        return dcrAutocompleteSearchObjectList;
    }

    @Override
    public String toString() {
        return name;
    }
}
