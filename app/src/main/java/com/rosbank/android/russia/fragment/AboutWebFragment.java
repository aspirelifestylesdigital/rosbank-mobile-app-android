package com.rosbank.android.russia.fragment;

import android.annotation.TargetApi;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.webkit.GeolocationPermissions;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.activitys.HomeActivity;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.utils.IntentUtil;

import butterknife.BindView;

/*
 * Created by chau.nguyen on 10/05/2016.
 */
public class AboutWebFragment
        extends BaseFragment {


    public static final String AboutWebTitle = "webTitle";
    public static final String AboutWebLink = "webLink";
    public static final String IS_MAP = "is_map";

    @BindView(R.id.webView_about_common)
    WebView aboutWebView;

    String titleName = "", linkWeb = "";
    boolean isMap = false;
    private static final int REQUEST_FINE_LOCATION = 0;
   // private String mGeolocationOrigin;
   // private GeolocationPermissions.Callback mGeolocationCallback;

    public class GeoWebViewClient
            extends WebViewClient {
        @Override
        public void onPageStarted(WebView view,
                                  String url,
                                  Bitmap favicon) {
            showDialogProgress();
            super.onPageStarted(view,
                                url,
                                favicon);
        }

        @Override
        public void onPageFinished(WebView view,
                                   String url) {
            hideDialogProgress();
            super.onPageFinished(view,
                                 url);
        }
    }


    /**
     * WebChromeClient subclass handles UI-related calls
     * Note: think chrome as in decoration, not the Chrome browser
     */
    public class GeoWebChromeClient
            extends WebChromeClient {
        /* @Override
         public void onGeolocationPermissionsShowPrompt(String origin,
                                                        GeolocationPermissions.Callback callback) {*/
        // Geolocation permissions coming from this app's Manifest will only be valid for devices with API_VERSION < 23.
        // On API 23 and above, we must check for permission, and possibly ask for it.
            /*final String permission = Manifest.permission.ACCESS_FINE_LOCATION;
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
                        ContextCompat.checkSelfPermission(getActivity(),
                                                          permission) == PackageManager.PERMISSION_GRANTED) {
                // we're on SDK < 23 OR user has already granted permission
                callback.invoke(origin,
                                true,
                                false);
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                                                                        permission)) {
                    // user has denied this permission before and selected [/] DON'T ASK ME AGAIN
                    // TODO Best Practice: show an AlertDialog explaining why the user could allow this permission, then ask again
                } else {
                    // ask the user for permissions
                    ActivityCompat.requestPermissions(getActivity(),
                                                      new String[]{permission},
                                                      REQUEST_FINE_LOCATION);
                    mGeolocationOrigin = origin;
                    mGeolocationCallback = callback;
                }
            }
        }*/
        @Override
        public void onGeolocationPermissionsShowPrompt(String origin,
                                                       GeolocationPermissions.Callback callback) {
            // Always grant permission since the app itself requires location
            // permission and the user has therefore already granted it
            callback.invoke(origin,
                            true,
                            false);
        }
    }

    public AboutWebFragment() {

    }

    @Override
    public void onBackPress() {
        if(aboutWebView.canGoBack()){
            aboutWebView.goBack();
        }else{
            super.onBackPress();
        }
    }

    @Override
    protected int layoutId() {
        return R.layout.fragment_about_common_web;
    }

    @Override
    protected void initView() {
        aboutWebView.getSettings()
                    .setJavaScriptEnabled(true);
    }

    @Override
    protected void bindData() {
        titleName = getArguments().getString(AboutWebTitle,
                                             "");
        linkWeb = getArguments().getString(AboutWebLink,
                                           "");
        isMap = getArguments().getBoolean(IS_MAP,
                                          false);
        if (isMap) {
            loadPermissions(android.Manifest.permission.ACCESS_COARSE_LOCATION,
                            REQUEST_FINE_LOCATION);
            aboutWebView.getSettings()
                        .setJavaScriptCanOpenWindowsAutomatically(true);
            aboutWebView.getSettings()
                        .setBuiltInZoomControls(true);
            aboutWebView.setWebViewClient(new GeoWebViewClient());

            aboutWebView.getSettings()
                        .setJavaScriptEnabled(true);
            aboutWebView.getSettings()
                        .setGeolocationEnabled(true);
            aboutWebView.setWebChromeClient(new GeoWebChromeClient());
            aboutWebView.loadUrl(linkWeb);
            if (getActivity() instanceof HomeActivity) {
                ((HomeActivity) getActivity()).hideFloatingActionButton();
            }
        } else {
            aboutWebView.loadUrl(linkWeb);
            aboutWebView.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(final WebView view,
                                                        final String url) {
                    if (url.startsWith("mailto:")) {
                        String mail = url.replace("mailto:",
                                                  "");
                        IntentUtil intent = IntentUtil.getInstance(getActivity());
                        intent.sendMail(mail);
                        return true;
                    } else {
                        return super.shouldOverrideUrlLoading(view,
                                                              url);
                    }
                }

                @TargetApi(21)
                @Override

                public boolean shouldOverrideUrlLoading(final WebView view,
                                                        final WebResourceRequest request) {
                    if (request.getUrl()
                               .getPath()
                               .toString()
                               .startsWith("mailto:")) {
                        String mail = request.getUrl()
                                             .getPath()
                                             .replace("mailto:",
                                                      "");
                        IntentUtil intent = IntentUtil.getInstance(getActivity());
                        intent.sendMail(mail);
                        return true;
                    } else {
                        return super.shouldOverrideUrlLoading(view,
                                                              request);
                    }
                }
                @Override
                public void onPageStarted(WebView view,
                                          String url,
                                          Bitmap favicon) {
                    showDialogProgress();
                    super.onPageStarted(view,
                                        url,
                                        favicon);
                }

                @Override
                public void onPageFinished(WebView view,
                                           String url) {
                    hideDialogProgress();
                    super.onPageFinished(view,
                                         url);
                }
            });
        }
    }

    private void loadPermissions(String perm,
                                 int requestCode) {
        if (ContextCompat.checkSelfPermission(getActivity(),
                                              perm) != PackageManager.PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                                                                     perm)) {
                ActivityCompat.requestPermissions(getActivity(),
                                                  new String[]{perm},
                                                  requestCode);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showLogoApp(false);
            ((HomeActivity) getActivity()).setTitle(titleName);
        }
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(isMap) {
            if (getActivity() instanceof HomeActivity) {
                ((HomeActivity) getActivity()).showFloatingActionButton();
            }
        }
    }

    @Override
    public boolean onBack() {
        return aboutWebView.canGoBack();
    }
   /* @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_FINE_LOCATION:
                boolean allow = false;
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // user has allowed these permissions
                    allow = true;
                }
                if (mGeolocationCallback != null) {
                    mGeolocationCallback.invoke(mGeolocationOrigin, allow, false);
                }
                break;
        }
    }*/


}

