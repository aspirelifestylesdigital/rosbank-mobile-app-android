package com.rosbank.android.russia.apiservices.RequestModel;


import com.google.gson.annotations.Expose;

public class BookOtherRequest
        extends BaseRequest {
    @Expose
    private String BookingId = "";
    @Expose
    private String UserID = "";
    @Expose
    private String UploadPhoto = "";
    @Expose
    private boolean Phone = false;
    @Expose
    private Boolean Email = false;
    @Expose
    private Boolean Both = false;
    @Expose
    private String MobileNumber = "";
    @Expose
    private String EmailAddress = "";

    public String getWhatCanWeDo() {
        return WhatCanWeDo;
    }

    public void setWhatCanWeDo(final String whatCanWeDo) {
        WhatCanWeDo = whatCanWeDo;
    }

    @Expose
    private String WhatCanWeDo = "";



    public String getBookingId() {
        return BookingId;
    }

    public void setBookingId(final String bookingId) {
        BookingId = bookingId;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(final String userID) {
        UserID = userID;
    }
    public String getUploadPhoto() {
        return UploadPhoto;
    }

    public void setUploadPhoto(final String uploadPhoto) {
        UploadPhoto = uploadPhoto;
    }

    public Boolean getPhone() {
        return Phone;
    }

    public void setPhone(final Boolean phone) {
        Phone = phone;
    }

    public Boolean getEmail() {
        return Email;
    }

    public void setEmail(final Boolean email) {
        Email = email;
    }

    public Boolean getBoth() {
        return Both;
    }

    public void setBoth(final Boolean both) {
        Both = both;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(final String mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public String getEmailAddress() {
        return EmailAddress;
    }

    public void setEmailAddress(final String emailAddress) {
        EmailAddress = emailAddress;
    }


}
