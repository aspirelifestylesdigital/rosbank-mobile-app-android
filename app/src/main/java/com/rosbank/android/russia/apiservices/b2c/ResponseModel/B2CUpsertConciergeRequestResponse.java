package com.rosbank.android.russia.apiservices.b2c.ResponseModel;

import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.google.gson.annotations.Expose;


public class B2CUpsertConciergeRequestResponse
        extends B2CBaseResponse {
    @Expose
    private String transactionId;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }
}
