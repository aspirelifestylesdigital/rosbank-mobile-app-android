package com.rosbank.android.russia.apiservices.b2c.RequestModel;

import com.google.gson.annotations.Expose;

import com.rosbank.android.russia.BuildConfig;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.model.UserItem;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.SharedPreferencesUtils;

/**
 * Created by anh.tran on 11/4/2016.
 */

public abstract class B2CBookingRequest {
    @Expose
    private String AccessToken;
    @Expose
    private String ConsumerKey;
    @Expose
    private String EPCClientCode;
    @Expose
    private String EPCProgramCode;
    @Expose
    private String OnlineMemberId;
    @Expose
    private String AttachmentPath;
    @Expose
    private String EmailAddress1;
    @Expose
    private String FirstName;
    @Expose
    private String LastName;
    @Expose
    private String Functionality;
    @Expose
    private String PhoneNumber;
    @Expose
    private String PrefResponse;
    @Expose
    private String RequestDetails;
    @Expose
    private String RequestType;
    @Expose
    private String Salutation;
    @Expose
    private String Source;
    @Expose
    private String VeriCode;

    private String SpecialRequirement;

    public static final String SPECIAL_REQUIREMENT = "Special Requirements - ";
    public static final String COUNTRY = "Country - ";

    public B2CBookingRequest(String function) {
        Source = "Mobile App";
        VeriCode = UserItem.isVip() ? BuildConfig.B2C_VERIFICATION_CODE_PRIVATE : BuildConfig.B2C_VERIFICATION_CODE_REGULAR;
        AccessToken = SharedPreferencesUtils.getPreferences(AppConstant.PRE_B2C_ACCESS_TOKEN, "");
        ConsumerKey = UserItem.isVip() ? BuildConfig.B2C_CONSUMER_KEY_PRIVATE : BuildConfig.B2C_CONSUMER_KEY_REGULAR;
        Functionality = function;
        RequestType = function;
        OnlineMemberId = SharedPreferencesUtils.getPreferences(AppConstant.PRE_B2C_ONLINE_MEMBER_ID, "");
        EPCClientCode = UserItem.isVip() ? BuildConfig.B2C_EPC_CLIENT_CODE_PRIVATE : BuildConfig.B2C_EPC_CLIENT_CODE_REGULAR;
        EPCProgramCode = UserItem.isVip() ? BuildConfig.B2C_EPC_PROGRAM_CODE_PRIVATE : BuildConfig.B2C_EPC_PROGRAM_CODE_REGULAR;
        FirstName = UserItem.getLoginedFirstName();
        LastName = UserItem.getLoginedLastName();
        Salutation = CommonUtils.convertSalutationToEnglish(UserItem.getLoginedSalutation());
    }

    abstract public void buildRequestDetails();

    public String getAccessToken() {
        return AccessToken;
    }

    public void setAccessToken(String accessToken) {
        AccessToken = accessToken;
    }

    public String getConsumerKey() {
        return ConsumerKey;
    }

    public void setConsumerKey(String consumerKey) {
        ConsumerKey = consumerKey;
    }

    public String getEPCClientCode() {
        return EPCClientCode;
    }

    public void setEPCClientCode(String EPCClientCode) {
        this.EPCClientCode = EPCClientCode;
    }

    public String getEPCProgramCode() {
        return EPCProgramCode;
    }

    public void setEPCProgramCode(String EPCProgramCode) {
        this.EPCProgramCode = EPCProgramCode;
    }

    public String getOnlineMemberId() {
        return OnlineMemberId;
    }

    public void setOnlineMemberId(String onlineMemberId) {
        OnlineMemberId = onlineMemberId;
    }

    public String getAttachmentPath() {
        return AttachmentPath;
    }

    public void setAttachmentPath(String attachmentPath) {
        AttachmentPath = attachmentPath;
    }

    public String getEmailAddress1() {
        return EmailAddress1;
    }

    public void setEmailAddress1(String emailAddress1) {
        EmailAddress1 = emailAddress1;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getFunctionality() {
        return Functionality;
    }

    public void setFunctionality(String functionality) {
        Functionality = functionality;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

    public String getPrefResponse() {
        return PrefResponse;
    }

    public void setPrefResponse(String prefResponse) {
        PrefResponse = prefResponse;
    }

    public String getRequestDetails() {
        return RequestDetails;
    }

    public void setRequestDetails(String requestDetails) {
        RequestDetails = requestDetails;
    }

    public String getRequestType() {
        return RequestType;
    }

    public void setRequestType(String requestType) {
        RequestType = requestType;
    }

    public String getSalutation() {
        return Salutation;
    }

    public void setSalutation(String salutation) {
        Salutation = salutation;
    }

    public String getSource() {
        return Source;
    }

    public void setSource(String source) {
        Source = source;
    }

    public String getVeriCode() {
        return VeriCode;
    }

    public void setVeriCode(String veriCode) {
        VeriCode = veriCode;
    }

    public String getSpecialRequirement() {
        return SpecialRequirement;
    }

    public void setSpecialRequirement(String specialRequirement) {
        SpecialRequirement = specialRequirement;
    }
}
