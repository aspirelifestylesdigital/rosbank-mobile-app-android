package com.rosbank.android.russia.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.fragment.BookHotelFragment;
import com.rosbank.android.russia.fragment.BookHotelRecomendFragment;
import com.rosbank.android.russia.model.concierge.MyRequestObject;

/**
 * Created by anh.trinh on 9/15/2016.
 */
public class TabBookingHotelAdapter
        extends FragmentPagerAdapter {
    private Context mContext;
    BaseFragment[] fragments = new BaseFragment[2];
    public TabBookingHotelAdapter(Context context, FragmentManager fm, Bundle bundle) {
        super(fm);
        mContext = context;
        BookHotelFragment bookHotelFragment = new BookHotelFragment();
        BookHotelRecomendFragment bookHotelRecomendFragment = new BookHotelRecomendFragment();
        if (bundle != null) {
            MyRequestObject myRequestObject = bundle.getParcelable(AppConstant.PRE_REQUEST_OBJECT_DATA);

            if(myRequestObject!=null && myRequestObject.getBookingRequestType().equals(MyRequestObject.HOTEL_REQUEST)) {

                bookHotelFragment.setArguments(bundle);
            }else{
                if(myRequestObject!=null && myRequestObject.getBookingRequestType().equals(MyRequestObject.HOTEL_RECOMMEND_REQUEST)) {
                    bookHotelRecomendFragment.setArguments(bundle);
                }
            }
        }

        fragments[0] = bookHotelFragment;
        fragments[1] = bookHotelRecomendFragment;
    }
    @Override
    public Fragment getItem(int position) {
        return fragments[position];
    }
    @Override
    public int getCount() {
        return fragments.length;
    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
    }

    public void updateReservationTime(int position){
        if(position == 0){
            ((BookHotelFragment)fragments[0]).makeDefaultDate();
        }else{
            ((BookHotelRecomendFragment)fragments[1]).makeDefaultDate();
        }
    }

}
