package com.rosbank.android.russia.model.concierge;

import android.text.TextUtils;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CGetRecentRequestResponse;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.application.AppContext;
import com.rosbank.android.russia.utils.DateTimeUtil;

/**
 * Created by ThuNguyen on 10/26/2016.
 */

public class OtherBookingDetailData extends MyRequestObject {
    private String WhatCanWeDo;
    private String CreateDate;
    private long createDateEpoc;

    public OtherBookingDetailData(B2CGetRecentRequestResponse recentRequestResponse) {
        super(recentRequestResponse);
        CreateDate = recentRequestResponse.getCREATEDDATE();
        createDateEpoc = DateTimeUtil.shareInstance().convertToTimeStampFromGMT(CreateDate, AppConstant.DATE_FORMAT_YYYY_MM_DD_T_HH_MM_SS);
    }
    @Override
    protected void initRequestProperties() {
        // Booking type group
        BookingFilterGroupType = AppConstant.BOOKING_FILTER_TYPE.Other;

        // Item title
        ItemTitle = AppContext.getSharedInstance().getResources().getString(R.string.text_other_request);

        // Item description
        ItemDescription = conciergeRequestDetail.getSpecialRequirement();
        WhatCanWeDo = ItemDescription;
    }

    @Override
    public boolean isUpcoming() {
        return !TextUtils.isEmpty(RequestMode) && !RequestMode.equalsIgnoreCase("cancel");
    }

    @Override
    public boolean isAmendCancelPermitted() {
        return !TextUtils.isEmpty(RequestMode) && !RequestMode.equalsIgnoreCase("cancel");
    }

    @Override
    public boolean isBookNormalType() {
        return true;
    }

    @Override
    public long getRequestStartDateEpoch() {
        return 0;
    }

    @Override
    public long getRequestEndDateEpoch() {
        return 0;
    }

    @Override
    public String getReservationName() {
        return conciergeRequestDetail.getReservationName();
    }

    public String getWhatCanWeDo() {
        return WhatCanWeDo;
    }

    public void setWhatCanWeDo(String whatCanWeDo) {
        WhatCanWeDo = whatCanWeDo;
    }
    public long getCreateDateEpoc() {
        return createDateEpoc;
    }


}
