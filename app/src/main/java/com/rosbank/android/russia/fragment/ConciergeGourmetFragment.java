package com.rosbank.android.russia.fragment;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.adapter.ConciergeGourmetAdapter;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.model.Restaurant;
import com.rosbank.android.russia.model.concierge.MyRequestObject;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.FontUtils;

import butterknife.BindView;

import static android.view.View.GONE;

/**
 * Created by nga.nguyent on 10/6/2016.
 */

public class ConciergeGourmetFragment extends BaseFragment{

    enum GOURMETREQUEST{
        CONCIERGE,
        CHATBOT
    }

    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.viewPager)
    ViewPager viewPager;

    private ConciergeGourmetAdapter adapter;
    private GOURMETREQUEST requestFrom = GOURMETREQUEST.CONCIERGE;
    private Restaurant restaurant;

    @Override
    protected int layoutId() {
        return R.layout.fragment_concierge_gourmet;
    }

    @Override
    protected void initView() {

        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.gourmet_tab1)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.gourmet_tab2)));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        CommonUtils.setFontForViewRecursive(tabLayout,
                FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);

        Bundle bundle =  getArguments();
        MyRequestObject myRequestObject;
        if (bundle != null &&  (myRequestObject = bundle.getParcelable(AppConstant.PRE_REQUEST_OBJECT_DATA))!=null) {
            adapter = new ConciergeGourmetAdapter(getChildFragmentManager(), myRequestObject);
            tabLayout.setVisibility(GONE);
        } else {
            adapter = new ConciergeGourmetAdapter(getChildFragmentManager(), restaurant);
            viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

            tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab LayoutTab) {
                    CommonUtils.hideSoftKeyboard(getContext(), view);
                    viewPager.setCurrentItem(LayoutTab.getPosition());
                    adapter.updateDefaultTime(LayoutTab.getPosition());
                }

                @Override
                public void onTabUnselected(TabLayout.Tab LayoutTab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab LayoutTab) {

                }
            });
        }

        viewPager.setAdapter(adapter);
    }

    @Override
    protected void bindData() {
        showLogoApp(false);
        setTitle(getString(R.string.concierge_item_gourmet));
        adapter.notifyDataSetChanged();
    }

    @Override
    protected boolean onBack() {
        return false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        int currentItemPos = viewPager.getCurrentItem();
        adapter.getItem(currentItemPos).onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        int currentItemPos = viewPager.getCurrentItem();
        adapter.getItem(currentItemPos).onActivityResult(requestCode, resultCode, data);
    }

    public void setRequestFrom(GOURMETREQUEST requestFrom) {
        this.requestFrom = requestFrom;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }
}
