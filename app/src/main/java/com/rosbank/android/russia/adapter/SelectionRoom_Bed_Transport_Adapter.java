package com.rosbank.android.russia.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.model.CommonObject;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.FontUtils;

import java.util.ArrayList;

/**
 * Created by anh.trinh on 9/20/2016.
 */
public class SelectionRoom_Bed_Transport_Adapter
        extends ArrayAdapter<CommonObject> {
    private final Context context;
    private final ArrayList<CommonObject> values;
    private final CommonObject selectedData;


    public SelectionRoom_Bed_Transport_Adapter(Context context, ArrayList<CommonObject> values, CommonObject selectedData) {
        super(context, -1, values);
        this.context = context;
        this.values = values;
        this.selectedData =selectedData;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                                                           .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.item_selection, parent, false);
        TextView textView = (TextView) rowView.findViewById(R.id.tv_selection_name);
        ImageView imgTick = (ImageView) rowView.findViewById(R.id.ic_selection_tick);
        CommonUtils.setFontForViewRecursive(textView,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
        textView.setText(values.get(position).getValue());
        if(selectedData!=null && selectedData.getValue()!=null && selectedData.getValue().equals(values.get(position).getValue())){
            imgTick.setVisibility(View.VISIBLE);
        }else{
            imgTick.setVisibility(View.GONE);
        }
        return rowView;
    }

    @Override
    public int getCount() {
        return values.size();
    }
}

