package com.rosbank.android.russia.views;

import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;

import java.util.List;
import java.util.UUID;

public class ParentItem implements ParentListItem {

    private UUID mId;
    private List<ChildItem> mChildItemList;

    private String mTitle;


    public ParentItem() {
        mId = UUID.randomUUID();
    }

    public UUID getId() {
        return mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public void setChildItemList(List<ChildItem> list) {
        mChildItemList = list;
    }

    @Override
    public List<ChildItem> getChildItemList() {
        return mChildItemList;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }
}
