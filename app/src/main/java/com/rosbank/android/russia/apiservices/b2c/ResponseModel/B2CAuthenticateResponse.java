package com.rosbank.android.russia.apiservices.b2c.ResponseModel;

import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;


public class B2CAuthenticateResponse
        extends B2CBaseResponse {

    private String message;
    private boolean success;

    public void setMessage(String message){this.message = message;}
    public String getMessage(){return  message;}

    public B2CAuthenticateResponse(String message, boolean success){
        this.message = message;
        this.success = success;
    }
    @Override
    public boolean isSuccess() {
        return success;
    }
}
