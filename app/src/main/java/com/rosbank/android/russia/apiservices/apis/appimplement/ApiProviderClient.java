package com.rosbank.android.russia.apiservices.apis.appimplement;

import com.rosbank.android.russia.BuildConfig;
import com.rosbank.android.russia.apiservices.WSAuthenticate;
import com.rosbank.android.russia.apiservices.apis.core.BaseApiRestClient;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.apiservices.ResponseModel.AuthenticateResponse;
import com.rosbank.android.russia.model.AuthorizeToken;
import com.rosbank.android.russia.utils.Logger;
import com.rosbank.android.russia.utils.SharedPreferencesUtils;
import com.rosbank.android.russia.utils.StringUtil;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public abstract class ApiProviderClient<T> extends BaseApiRestClient<T> {

    protected ApiInterface serviceInterface;

    public ApiProviderClient() {
        super();
    }

    @Override
    protected void initRestAdapter() {
        rtfAdapter = new Retrofit.Builder()
                .baseUrl(BuildConfig.WS_ROOT_URL)
                .client(sOkHttpClient)
                .addConverterFactory(GsonConverterFactory.create(sGson))
                .build();
        //
        serviceInterface = rtfAdapter.create(ApiInterface.class);
    }

    @Override
    protected boolean isAuthenticateValidate() {
        String token = SharedPreferencesUtils.getPreferences(AppConstant.PRE_AUTHOR_TOKEN, "");
        if(StringUtil.isEmpty(token))
            return false;

        String createAt = SharedPreferencesUtils.getPreferences(AppConstant.PRE_AUTHOR_CREATE_AT, "");
        String expiredAt = SharedPreferencesUtils.getPreferences(AppConstant.PRE_AUTHOR_EXPIRED_AT, "");

        return !AuthorizeToken.isExpired(createAt, expiredAt);
    }

    @Override
    protected void runSignAuthenticate() {
        /*Callback<AuthenticateResponse> cb = new Callback<AuthenticateResponse>() {
            @Override
            public void onResponse(Call<AuthenticateResponse> call, Response<AuthenticateResponse> response) {
                AuthenticateResponse authen = response.body();
                if (response.code()==200 && authen!=null && authen.isSuccess()) {
                    //authen success -> save token
                    authenticateSuccess(authen);
                    //
                    postAuthenticateSuccess();
                } else {
                    //
                    postAuthenticateError(call, response);
                }
            }

            @Override
            public void onFailure(Call<AuthenticateResponse> call, Throwable t) {
                postAuthenticateError(call, t);
            }
        };

        //authenticate("admin", "Admin123!@#$", cb);
        authenticate(BuildConfig.AUTHEN_USERNAME, BuildConfig.AUTHEN_PASSWORD, cb);*/

        WSAuthenticate authen = new WSAuthenticate();
        authen.run(new Callback<AuthenticateResponse>() {
            @Override
            public void onResponse(Call<AuthenticateResponse> call, Response<AuthenticateResponse> response) {
                AuthenticateResponse authen = response.body();
                if (response.code()==200 && authen!=null && authen.isSuccess()) {
                    //authen success -> save token
                    authenticateSuccess(authen);
                    //
                    postAuthenticateSuccess();
                } else {
                    //
                    postAuthenticateError(call, response);
                }
            }

            @Override
            public void onFailure(Call<AuthenticateResponse> call, Throwable t) {
                postAuthenticateError(call, t);
            }
        });

        /*Authenticate wsAuthenticate = new Authenticate();
        wsAuthenticate.run(new ApiCallback() {
            @Override
            public void onNoInternetConnection() {
                System.out.println("authen no internet");
                postNoInternet();
            }

            @Override
            public void onComplete(BaseResponse response) {
                System.out.println("authen success");
                token = getAuthorizeToken();
                postAuthenticateSuccess();
            }

            @Override
            public void onFailed(BaseErrorResponse error) {
                System.out.println("authen error");
                postAuthenticateError(error);
            }

            @Override
            public void onAuthenticateFailed(BaseErrorResponse error) {
                System.out.println("authen Failed");
                postAuthenticateError(error);
            }
        });*/

    }

    @Override
    protected void authenticateSuccess(AuthenticateResponse authen) {
        // TODO: [9/16/2016] after sign authenticate with server successful you need save to reuse.
        SharedPreferencesUtils.setPreferences(AppConstant.PRE_AUTHOR_TOKEN, authen.getData().getAuthorizeToken());
        SharedPreferencesUtils.setPreferences(AppConstant.PRE_AUTHOR_CREATE_AT, authen.getData().getCreatedAt());
        SharedPreferencesUtils.setPreferences(AppConstant.PRE_AUTHOR_EXPIRED_AT, authen.getData().getExpiredAt());
    }

    @Override
    protected String getAuthorizeToken() {
        //return "aspire.auth 515A49683533474F2B716835434E6E7733454E31336741304D6C2B41673051744A7573367562766F583648484D4378586533764A505348424244386A626D4C51327633786B76625967725556507571707944463030386A4F55564E33764F34745757466C4172346438786D38686E7A622F656366676B4F4D6539314975482F7952674D59342F5351384A702B43794C6E3149692F66773D3D";
        String authenToken = SharedPreferencesUtils.getPreferences(AppConstant.PRE_AUTHOR_TOKEN, "");
        Logger.sout(authenToken);
        return authenToken;
    }

    /**
     * DEFINE API BELOW
     * ===============================================
     * */
    public void authenticate(String username, String password, Callback<AuthenticateResponse> cb){
        serviceInterface.authenticate(username, password).enqueue(cb);
    }

}