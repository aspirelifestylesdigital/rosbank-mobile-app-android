package com.rosbank.android.russia.apiservices;

import com.rosbank.android.russia.apiservices.RequestModel.GetListMyRequestRequest;
import com.rosbank.android.russia.apiservices.ResponseModel.GetListMyRequestResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.ApiProviderClient;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Callback;


public class WSGetMyRequests
        extends ApiProviderClient<GetListMyRequestResponse> {

    private GetListMyRequestRequest request;

    public WSGetMyRequests(){

    }

    public void getMyRequests(GetListMyRequestRequest request){
        this.request = request;
    }

    @Override
    public void run(Callback<GetListMyRequestResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }

    @Override
    protected boolean isUseAuthenticateInApi() {
        return true;
    }

    @Override
    public void runApi() {
        serviceInterface.getMyRequest(request).enqueue(callback);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("User-Agent", "Retrofit-Sample-App");
        return headers;
    }

}
