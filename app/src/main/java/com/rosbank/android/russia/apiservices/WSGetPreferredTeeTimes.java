package com.rosbank.android.russia.apiservices;

import com.rosbank.android.russia.apiservices.ResponseModel.PreferrelTeeTimeResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.ApiProviderClient;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Callback;

/**
 * Created by nga.nguyent on 9/29/2016.
 */

public class WSGetPreferredTeeTimes extends ApiProviderClient<PreferrelTeeTimeResponse> {
    @Override
    public void run(Callback<PreferrelTeeTimeResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }

    @Override
    protected boolean isUseAuthenticateInApi() {
        return true;
    }

    @Override
    protected void runApi() {
        serviceInterface.getPreferredTeeTimes().enqueue(callback);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        //return null;
        Map<String, String> headers = new HashMap<>();
        headers.put("User-Agent", "Retrofit-Sample-App");
        return headers;
    }
}
