package com.rosbank.android.russia.dcr.api.ResponseModel;

import com.rosbank.android.russia.dcr.model.DCRPrivilegeObject;
import com.google.gson.annotations.Expose;

import java.util.List;


public class DCRGetPrivilegesResponse
        extends DCRBaseResponse {
    @Expose
    List<DCRPrivilegeObject> data;

    public List<DCRPrivilegeObject> getData() {
        return data;
    }

    public void setData(List<DCRPrivilegeObject> data) {
        this.data = data;
    }
}
