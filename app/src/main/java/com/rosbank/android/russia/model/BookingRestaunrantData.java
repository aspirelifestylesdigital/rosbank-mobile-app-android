package com.rosbank.android.russia.model;

import com.google.gson.annotations.Expose;


public class BookingRestaunrantData {
    @Expose
    private String BookingId;
    @Expose
    private String BookingName;

    public String getBookingId() {
        return BookingId;
    }

    public void setBookingId(String bookingId) {
        BookingId = bookingId;
    }

    public String getBookingName() {
        return BookingName;
    }

    public void setBookingName(String bookingName) {
        BookingName = bookingName;
    }
}
