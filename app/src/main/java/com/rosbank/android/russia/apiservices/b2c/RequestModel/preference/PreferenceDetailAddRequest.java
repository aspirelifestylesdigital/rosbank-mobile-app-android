package com.rosbank.android.russia.apiservices.b2c.RequestModel.preference;

/**
 * Created by Thu Nguyen on 12/12/2016.
 */

public abstract class PreferenceDetailAddRequest extends PreferenceDetailRequest{

    public PreferenceDetailAddRequest(String Delete, String Type) {
        super(Delete, Type);
    }
}
