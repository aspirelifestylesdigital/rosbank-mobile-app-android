package com.rosbank.android.russia.fragment;

import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.activitys.HomeActivity;
import com.rosbank.android.russia.adapter.SelectDataAdapter;
import com.rosbank.android.russia.apiservices.RequestModel.ExperienceGourmetRequest;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.model.KeyValueObject;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.FontUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/*
 * Created by chau.nguyen on 10/05/2016.
 */
public class ExperiencesFilterFragment extends BaseFragment implements SelectDataFragment.OnSelectDataListener{

    @BindView(R.id.llFilterContent)
    LinearLayout llFilterContent;

    @BindView(R.id.edtZipCode)
    EditText edtZipCode;

    @BindView(R.id.tgeBtnRange1)
    ToggleButton tgeBtnRange1;

    @BindView(R.id.tgeBtnRange2)
    ToggleButton tgeBtnRange2;

    @BindView(R.id.tgeBtnRange3)
    ToggleButton tgeBtnRange3;

    @BindView(R.id.tgeBtnRange4)
    ToggleButton tgeBtnRange4;

    @BindView(R.id.tvCuisineSelected)
    TextView tvCuisineSelected;

    @BindView(R.id.ratingStar)
    RatingBar ratingBarStar;

    @BindView(R.id.tvOccasionsSelected)
    TextView tvOccasionsSelected;

    @BindView(R.id.btnApply)
    Button btnApply;

    @BindView(R.id.btnClear)
    Button btnClear;

    //variable local
    private List<KeyValueObject> cuisineSelectList;
    private List<KeyValueObject> occasionsSelectList;
    private ExperienceSearchFragment.EventFilterPage filterCallSearch;

    public ExperiencesFilterFragment() {

    }

    @Override
    protected int layoutId() {
        return R.layout.fragment_experiences_filter;
    }

    @Override
    protected void initView() {
        CommonUtils.setFontForViewRecursive(llFilterContent, FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
    }

    @Override
    protected void bindData() {

    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showLogoApp(false);
            ((HomeActivity) getActivity()).setTitle("");
        }
    }

    @Override
    public boolean onBack() {
        return false;
    }

    @Override
    public void onSelectedData(SelectDataFragment.APIDATA type, List<KeyValueObject> list) {
        //after select cuisine type response
        if(type == SelectDataFragment.APIDATA.CUISINE_GOURMET){
            cuisineSelectList = list;
            tvCuisineSelected.setText(getValueSelected(cuisineSelectList));

        }else if (type == SelectDataFragment.APIDATA.OCCASION){
            occasionsSelectList = list;
            tvOccasionsSelected.setText(getValueSelected(occasionsSelectList));

        }
    }

    @OnClick(R.id.tvCuisineSelected)
    public void eventCuisineSelect() {
        SelectDataFragment cuisine = new SelectDataFragment();
        cuisine.setDataType(SelectDataFragment.APIDATA.CUISINE_GOURMET);
        cuisine.setChoiceMode(SelectDataAdapter.CHOICEMODE.MULTI);
        cuisine.setOnSelectListener(this);
        if (cuisineSelectList != null) {
            cuisine.setCuisineSelected(cuisineSelectList);
        }
        cuisine.setHeaderTitle(getString(R.string.cuisine_preferences));
        pushFragment(cuisine, true, true);
    }

    @OnClick(R.id.tvOccasionsSelected)
    public void eventOccasionsSelect(){
        SelectDataFragment occasion = new SelectDataFragment();
        occasion.setDataType(SelectDataFragment.APIDATA.OCCASION);
        occasion.setChoiceMode(SelectDataAdapter.CHOICEMODE.SINGLE);
        occasion.setOnSelectListener(this);
        if (occasionsSelectList != null) {
            occasion.setCuisineSelected(occasionsSelectList);
        }
        occasion.setHeaderTitle(getString(R.string.occasion_text));
        pushFragment(occasion, true, true);
    }

    /**
     * String TextView Selected
    */
    private String getValueSelected(List<KeyValueObject> list){
        String text = getString(R.string.hint_please_select);
        if(list != null && list.size() > 0){
            text = "";
            for (int i = 0; i < list.size(); i++) {
                if (text.length() > 0) {
                    text += ", ";
                }
                text += list.get(i).getValue();
            }
        }
        return text;
    }
    /**
     * return list Ids Cuisine Type selected
     */
    private List<String> getCuisine() {
        List<String> ids = new ArrayList<>();
        if (cuisineSelectList != null) {
            for (int i = 0; i < cuisineSelectList.size(); i++) {
                KeyValueObject cuisine = cuisineSelectList.get(i);
                ids.add(cuisine.getValue());
            }
        }
        return ids;
    }

    private List<String> getOccasions() {
        List<String> ids = new ArrayList<>();
        if (occasionsSelectList != null) {
            for (int i = 0; i < occasionsSelectList.size(); i++) {
                KeyValueObject cuisine = occasionsSelectList.get(i);
                ids.add(cuisine.getValue());
            }
        }
        return ids;
    }

    @OnClick(R.id.btnClear)
    public void eventClear() {

        edtZipCode.setText("");

        tgeBtnRange1.setChecked(false);
        tgeBtnRange2.setChecked(false);
        tgeBtnRange3.setChecked(false);
        tgeBtnRange4.setChecked(false);

        if(cuisineSelectList != null)
            cuisineSelectList.clear();
        tvCuisineSelected.setText(getString(R.string.hint_please_select));

        ratingBarStar.setRating(0.0f);

        if(occasionsSelectList != null)
            occasionsSelectList.clear();
        tvOccasionsSelected.setText(getString(R.string.hint_please_select));

    }

    @OnClick(R.id.btnApply)
    public void eventApply() {
        if(filterCallSearch == null)
            return;

        boolean haveCondition = false;

        String zipCode = edtZipCode.getText().toString().trim();
        if(!zipCode.isEmpty()){
            haveCondition = true;
        }

        List<String> priceRange = new ArrayList<>();

        if(tgeBtnRange1.isChecked()){
            priceRange.add(getString(R.string.experiences_filter_price_range_1));
            haveCondition = true;
        }
        if(tgeBtnRange2.isChecked()){
            priceRange.add(getString(R.string.experiences_filter_price_range_2));
            haveCondition = true;
        }
        if(tgeBtnRange3.isChecked()){
            priceRange.add(getString(R.string.experiences_filter_price_range_3));
            haveCondition = true;
        }
        if(tgeBtnRange4.isChecked()){
            priceRange.add(getString(R.string.experiences_filter_price_range_4));
            haveCondition = true;
        }

        List<String> cuisine = getCuisine();
        if(cuisine.size()>0){
            haveCondition = true;
        }

        int rang = (int) ratingBarStar.getRating();
        if(rang > 0){
            haveCondition = true;
        }

        List<String> occasion = getOccasions();
        if(occasion.size()>0){
            haveCondition = true;
        }

        if(haveCondition){
            ExperienceGourmetRequest request = new ExperienceGourmetRequest();
            request.setZipcode(zipCode);
            request.setPriceRange(priceRange);
            request.setCuisines(cuisine);
            request.setRating(String.valueOf(rang)) ;
            request.setOccasions(occasion);
            filterCallSearch.callFilter(request);
            eventClear();
        }else{
            filterCallSearch.callNoFilter();
        }

        onBackPress();
    }

    public void setFilterCallSearch(ExperienceSearchFragment.EventFilterPage filterCallSearch) {
        this.filterCallSearch = filterCallSearch;
    }
}
