package com.rosbank.android.russia.utils;

import android.text.TextPaint;
import android.text.style.CharacterStyle;

import java.lang.reflect.Method;

public class ColoredUnderlineSpan extends CharacterStyle {

    private final int mColor;
    private String mResource;

    public ColoredUnderlineSpan(String resource, final int color) {
        this.mResource = resource;
        this.mColor = color;
    }


    @Override
    public void updateDrawState(TextPaint tp) {
        try {
            final Method method = TextPaint.class.getMethod(mResource, Integer.TYPE, Float.TYPE);
            method.invoke(tp, mColor, 10.0f);
        } catch (Exception e) {
            tp.setUnderlineText(true);
        }
    }
}
