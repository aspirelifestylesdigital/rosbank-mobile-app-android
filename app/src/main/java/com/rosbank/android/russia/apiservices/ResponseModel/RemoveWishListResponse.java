package com.rosbank.android.russia.apiservices.ResponseModel;

import com.rosbank.android.russia.apiservices.apis.appimplement.BaseResponse;
import com.rosbank.android.russia.model.FavouriteData;
import com.google.gson.annotations.Expose;


public class RemoveWishListResponse
        extends BaseResponse {
    @Expose
    FavouriteData Data;

    public FavouriteData getData() {
        return Data;
    }

    public void setData(FavouriteData data) {
        Data = data;
    }
}
