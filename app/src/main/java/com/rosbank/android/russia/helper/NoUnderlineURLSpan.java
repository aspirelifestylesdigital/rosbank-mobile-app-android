package com.rosbank.android.russia.helper;

/**
 * Copyright (C) 2015 CreateTrips
 * NoUnderlineURLSpan.java
 * CreateTrips
 *
 * @author nam.tran, 06/22/2015
 * @since v0.6
 */

import android.text.TextPaint;
import android.text.style.URLSpan;
import android.view.View;

/**
 * Custom class to hide the underline links.
 *
 * @author nam.tran, 06/22/2015
 * @since v0.6
 */
public class NoUnderlineURLSpan
        extends URLSpan {

    private final IClickableUrlSpanListener mListener;

    /**
     * Constructor with URL.
     *
     * @param url
     *         The URL of this spannable object.
     * @since v0.6
     */
    public NoUnderlineURLSpan(final String url) {
        super(url);
        this.mListener = null;
    }

    /**
     * Constructor with URL.
     *
     * @param url
     *         The URL of this spannable object.
     * @param listener
     *         The custom listener for span.
     * @since v0.6
     */
    public NoUnderlineURLSpan(final String url,
                              final IClickableUrlSpanListener listener) {
        super(url);
        this.mListener = listener;
    }

    /**
     * @see URLSpan#updateDrawState(TextPaint)
     * @since v0.6
     */
    @Override
    public void updateDrawState(final TextPaint ds) {
        super.updateDrawState(ds);
        ds.setUnderlineText(false);
    }

    /**
     * @see URLSpan#onClick(View)
     * @since v0.6
     */
    @Override
    public void onClick(final View widget) {
        if (this.mListener != null) {
            this.mListener.onUrlClicked(widget,
                                        getURL());
        } else {
            super.onClick(widget);
        }
    }
}
