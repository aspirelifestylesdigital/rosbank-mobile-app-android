package com.rosbank.android.russia.model;

/*
 * Created by chau.nguyen on 10/5/2016.
 */

public class CommonLandingObject {

    private String title;

    private int imgId;

    public CommonLandingObject(String title, int imgId) {
        this.title = title;
        this.imgId = imgId;
    }

    public String getTitle() {
        return title;
    }

    public int getImgId() {
        return imgId;
    }
}
