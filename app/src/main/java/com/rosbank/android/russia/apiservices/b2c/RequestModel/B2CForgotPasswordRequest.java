package com.rosbank.android.russia.apiservices.b2c.RequestModel;

import com.rosbank.android.russia.BuildConfig;
import com.rosbank.android.russia.apiservices.RequestModel.BaseRequest;
import com.google.gson.annotations.Expose;

import static com.rosbank.android.russia.application.AppConstant.CLIENT_CODE_REGULAR;

/**
 * Created by ThuNguyen on 10/26/2016.
 */

public class B2CForgotPasswordRequest extends BaseRequest{
    @Expose
    private String ConsumerKey;
    @Expose
    private String Email2;
    @Expose
    private String Functionality;

    private String clientCode;

    public B2CForgotPasswordRequest(String clientCode){
        ConsumerKey = clientCode.equals(CLIENT_CODE_REGULAR) ? BuildConfig.B2C_CONSUMER_KEY_REGULAR : BuildConfig.B2C_CONSUMER_KEY_PRIVATE;
        //ConsumerKey = BuildConfig.B2C_CONSUMER_KEY;
        Functionality = "ForgotPassword";
    }
    public B2CForgotPasswordRequest build(String email, String clientCode){
        Email2 = email;
        this.clientCode = clientCode;
        return this;
    }
}
