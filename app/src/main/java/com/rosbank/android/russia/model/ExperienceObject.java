package com.rosbank.android.russia.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;


public class ExperienceObject implements Parcelable {

    @Expose
    private String Title;
    @Expose
    private String BannerUrl;
    @Expose
    private String Description;
    @Expose
    private String Website;

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getBannerUrl() {
        return BannerUrl;
    }

    public void setBannerUrl(String bannerUrl) {
        BannerUrl = bannerUrl;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getWebsite() {
        return Website;
    }

    public void setWebsite(String website) {
        Website = website;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel parcel, final int i) {
        parcel.writeString(this.Title);
        parcel.writeString(this.BannerUrl);
        parcel.writeString(this.Description);
        parcel.writeString(this.Website);

    }
    private ExperienceObject(Parcel in) {
        this.Title = in.readString();
        this.BannerUrl = in.readString();
        this.Description = in.readString();
        this.Website = in.readString();
    }
    public ExperienceObject() {
        this.Title = "";
        this.BannerUrl = "";
        this.Description = "";
        this.Website = "";
    }

    public static final Creator<ExperienceObject> CREATOR = new Creator<ExperienceObject>() {
        @Override
        public ExperienceObject createFromParcel(Parcel source) {
            return new ExperienceObject(source);
        }

        @Override
        public ExperienceObject[] newArray(int size) {
            return new ExperienceObject[size];
        }
    };
}
