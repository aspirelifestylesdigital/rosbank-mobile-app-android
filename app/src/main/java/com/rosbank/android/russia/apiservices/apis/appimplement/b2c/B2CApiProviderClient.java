package com.rosbank.android.russia.apiservices.apis.appimplement.b2c;

import com.rosbank.android.russia.BuildConfig;
import com.rosbank.android.russia.apiservices.ResponseModel.AuthenticateResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.ApiInterface;
import com.rosbank.android.russia.apiservices.apis.core.BaseApiRestClient;
import com.rosbank.android.russia.apiservices.b2c.B2CWSAuthenticate;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.utils.Logger;
import com.rosbank.android.russia.utils.SharedPreferencesUtils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public abstract class B2CApiProviderClient<T> extends BaseApiRestClient<T> implements Callback<T>{

    protected ApiInterface serviceInterface;
    protected B2CICallback b2CICallback;
    protected T callbackResponse;
    protected boolean isCallbackInstantly = true; // Some flows need to call several APIs then complete
    public B2CApiProviderClient() {
        super();
    }

    @Override
    protected void initRestAdapter() {
        rtfAdapter = new Retrofit.Builder()
                .baseUrl(BuildConfig.WS_B2C_ROOT_URL)
                .client(sOkHttpClient)
                .addConverterFactory(GsonConverterFactory.create(sGson))
                .build();
        serviceInterface = rtfAdapter.create(ApiInterface.class);
    }

    @Override
    protected boolean isAuthenticateValidate() {
        long expiredAt = SharedPreferencesUtils.getPreferences(AppConstant.PRE_B2C_EXPIRED_AT, 0L); // Milli second
        long currentTimeMillis = System.currentTimeMillis();
        return expiredAt > currentTimeMillis;
    }

    @Override
    protected void runSignAuthenticate() {
        /*WSAuthenticate authen = new WSAuthenticate();
        authen.run(new Callback<AuthenticateResponse>() {
            @Override
            public void onResponse(Call<AuthenticateResponse> call, Response<AuthenticateResponse> response) {
                AuthenticateResponse authen = response.body();
                if (response.code()==200 && authen!=null && authen.isSuccess()) {
                    //authen success -> save token
                    authenticateSuccess(authen);
                    //
                    postAuthenticateSuccess();
                } else {
                    //
                    postAuthenticateError(call, response);
                }
            }

            @Override
            public void onFailure(Call<AuthenticateResponse> call, Throwable t) {
                postAuthenticateError(call, t);
            }
        });*/
        B2CWSAuthenticate b2CWSAuthenticate = new B2CWSAuthenticate();
        b2CWSAuthenticate.run(new B2CICallback() {
            @Override
            public void onB2CResponse(B2CBaseResponse response) {
                if(response != null){
                    if(response.isSuccess()) {
                        postAuthenticateSuccess();
                    }else{
                        postAuthenticateError(response.getMessage(), response.getErrorCode());
                    }
                }else{
                    postAuthenticateError(getMessageErrorFromLog(), null);
                }
            }

            @Override
            public void onB2CResponseOnList(List<B2CBaseResponse> responseList) {

            }

            @Override
            public void onB2CFailure(String errorMessage, String errorCode) {
                postAuthenticateError(getMessageErrorFromLog(), getErrorCodeFromLog());
            }
        });
    }
    @Override
    protected void postAuthenticateSuccess() {
        synchronized (mListRequests) {
            while (mListRequests.size()>0){
                BaseApiRestClient api = mListRequests.get(0);
                //
                mListRequests.remove(0);
                //recall api.
                api.run(null);
            }
        }
    }
    protected void postAuthenticateError(String errorMessage, String errorCode) {
        synchronized (mListRequests) {
            while (mListRequests.size()>0){
                ((B2CApiProviderClient)mListRequests.get(0)).b2CICallback.onB2CFailure(errorMessage, errorCode);
                mListRequests.remove(0);
            }
        }
    }
    @Override
    protected void authenticateSuccess(AuthenticateResponse authen) {
        // TODO: [9/16/2016] after sign authenticate with server successful you need save to reuse.
        SharedPreferencesUtils.setPreferences(AppConstant.PRE_AUTHOR_TOKEN, authen.getData().getAuthorizeToken());
        SharedPreferencesUtils.setPreferences(AppConstant.PRE_AUTHOR_CREATE_AT, authen.getData().getCreatedAt());
        SharedPreferencesUtils.setPreferences(AppConstant.PRE_AUTHOR_EXPIRED_AT, authen.getData().getExpiredAt());
    }

    @Override
    protected String getAuthorizeToken() {
        //return "aspire.auth 515A49683533474F2B716835434E6E7733454E31336741304D6C2B41673051744A7573367562766F583648484D4378586533764A505348424244386A626D4C51327633786B76625967725556507571707944463030386A4F55564E33764F34745757466C4172346438786D38686E7A622F656366676B4F4D6539314975482F7952674D59342F5351384A702B43794C6E3149692F66773D3D";
        String authenToken = SharedPreferencesUtils.getPreferences(AppConstant.PRE_AUTHOR_TOKEN, "");
        Logger.sout(authenToken);
        return authenToken;
    }
    protected abstract void postResponse(T response);
    /**
     * DEFINE API BELOW
     * ===============================================
     * */
    public void authenticate(String username, String password, Callback<AuthenticateResponse> cb){
        serviceInterface.authenticate(username, password).enqueue(cb);
    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        callbackResponse = response.body();
        if(callbackResponse != null){
            if(callbackResponse instanceof B2CBaseResponse) {
                if (((B2CBaseResponse) callbackResponse).isSuccess()) {
                    postResponse(callbackResponse);
                    if (isCallbackInstantly) {
                        b2CICallback.onB2CResponse((B2CBaseResponse) callbackResponse);
                    }
                } else {
                    if(((B2CBaseResponse)callbackResponse).getErrorCode().equalsIgnoreCase("Err401")){
                        mListRequests.add(this);
                        runSignAuthenticate();
                    }else {
                        if (b2CICallback != null) {
                            b2CICallback.onB2CFailure(((B2CBaseResponse) callbackResponse).getMessage(), ((B2CBaseResponse) callbackResponse).getErrorCode());
                        }
                    }
                }
            }else if(callbackResponse instanceof List){
                List responseList = (List)callbackResponse;
                if(responseList.size() > 0){
                    b2CICallback.onB2CResponseOnList((List)callbackResponse);
                }else{
                    b2CICallback.onB2CFailure("", "");
                }
            }
        }else{
            if(b2CICallback != null){
                b2CICallback.onB2CFailure(getMessageErrorFromLog(), getErrorCodeFromLog());
            }
        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        if(b2CICallback != null){
            b2CICallback.onB2CFailure(getMessageErrorFromLog(), getErrorCodeFromLog());
        }
    }
}