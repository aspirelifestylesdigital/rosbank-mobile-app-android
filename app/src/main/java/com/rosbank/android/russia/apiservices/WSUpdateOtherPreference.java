package com.rosbank.android.russia.apiservices;

import com.rosbank.android.russia.apiservices.RequestModel.UpdategOtherPreferencesRequest;
import com.rosbank.android.russia.apiservices.ResponseModel.UpdateOtherPreferencesResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.ApiProviderClient;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Callback;


public class WSUpdateOtherPreference
        extends ApiProviderClient<UpdateOtherPreferencesResponse> {

    UpdategOtherPreferencesRequest request;

    public WSUpdateOtherPreference(){

    }

    public UpdategOtherPreferencesRequest getRequest() {
        return request;
    }

    public void setRequest(UpdategOtherPreferencesRequest request) {
        this.request = request;
    }

    @Override
    public void run(Callback<UpdateOtherPreferencesResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }

    @Override
    protected boolean isUseAuthenticateInApi() {
        return true;
    }

    @Override
    public void runApi() {
        if(request==null)
            return;

        serviceInterface.updateOtherPreferences(request).enqueue(callback);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("User-Agent", "Retrofit-Sample-App");
        return headers;
    }

}
