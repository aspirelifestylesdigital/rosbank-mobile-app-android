package com.rosbank.android.russia.dcr.api.ResponseModel;

import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.internal.LinkedTreeMap;

/**
 * Created by ThuNguyen on 11/3/2016.
 */

public abstract class DCRErrorMessageResponse {

    @Expose
    public String code;
    @Expose
    public String message;

    public String getMessage(){

        return message;
    }
    public String getErrorCode(){

        return code;
    }

}
