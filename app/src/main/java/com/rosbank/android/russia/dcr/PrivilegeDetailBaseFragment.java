package com.rosbank.android.russia.dcr;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.activitys.HomeActivity;
import com.rosbank.android.russia.activitys.LoginAcitivy;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.dcr.api.DCRICallback;
import com.rosbank.android.russia.dcr.api.DCRWSGetPrivilegeDetail;
import com.rosbank.android.russia.dcr.api.RequestModel.DCRGetPrivilegeDetailRequest;
import com.rosbank.android.russia.dcr.api.ResponseModel.DCRBaseResponse;
import com.rosbank.android.russia.dcr.api.ResponseModel.DCRGetPrivilegeDetailResponse;
import com.rosbank.android.russia.dcr.model.DCRPrivilegeObject;
import com.rosbank.android.russia.dcr.widgets.ExpandableTextView;
import com.rosbank.android.russia.dcr.widgets.GalleryView;
import com.rosbank.android.russia.model.UserItem;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.FontUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/*
 * Created by anh.trinh on 11/03/2017.
 */
public class PrivilegeDetailBaseFragment
        extends BaseFragment
        implements DCRICallback {


    public static final String PRIVILEGE_TYPE = "privilege_type";
    public static final String PRIVILEGE_DATA = "data";
    @BindView(R.id.llPrivilegeDetailContent)
    RelativeLayout llPrivilegeDetailContent;
    @BindView(R.id.tvPrivilegeName)
    TextView tvPrivilegeName;
    @BindView(R.id.tvSummary)
    ExpandableTextView tvSummary;
    @BindView(R.id.layoutPrivilegeInfo)
    protected LinearLayout layoutPrivilegeInfo;
    @BindView(R.id.tvVisitWebsite)
    TextView tvVisitWebsite;
    @BindView(R.id.tvImageCopyRight)
    TextView tvImageCopyRight;
    @BindView(R.id.srvExpDetail)
    ScrollView srvExpDetail;
    @BindView(R.id.btnPrivilegeDetailBook)
    Button btnPrivilegeDetailBook;
    @BindView(R.id.layoutButtonBook)
    LinearLayout layoutButtonBook;
    @BindView(R.id.layoutVisitWebsite)
    LinearLayout layoutVisitWebsite;
    @BindView(R.id.fabPrivilege)
    FloatingActionButton fabPrivilege;

    protected String titleName = "";
    protected String privilegeType = "";

    protected String id = "";

    protected DCRPrivilegeObject mDcrPrivilegeObject;

    @BindView(R.id.gallery)
    GalleryView mGalleries;

    public PrivilegeDetailBaseFragment() {

    }

    @Override
    protected int layoutId() {
        return R.layout.fragment_privilege_detail;
    }

    @Override
    protected void initView() {
        CommonUtils.setFontForViewRecursive(llPrivilegeDetailContent,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
        CommonUtils.setFontForViewRecursive(btnPrivilegeDetailBook,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        CommonUtils.setFontForViewRecursive(tvPrivilegeName,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);

        tvImageCopyRight.setTypeface(tvImageCopyRight.getTypeface(),
                                     Typeface.ITALIC);
        if (getArguments() != null) {
            mDcrPrivilegeObject =
                    (DCRPrivilegeObject) getArguments().getSerializable(PRIVILEGE_DATA);
            titleName = mDcrPrivilegeObject.getName();
            id = mDcrPrivilegeObject.getId();
            privilegeType = getArguments().getString(PRIVILEGE_TYPE,
                                                     "");
            if (CommonUtils.isStringValid(titleName)) {
                ((HomeActivity) getActivity()).setTitle(titleName);
            }
            if(privilegeType.equals(DCRPrivilegeObject.PRIVILEGE_TYPE_SPA)) {
                fillData(mDcrPrivilegeObject);
            }else {
                if (CommonUtils.isStringValid(id)) {
                    showDialogProgress();
                    DCRGetPrivilegeDetailRequest dcrGetPrivilegeDetailRequest =
                            new DCRGetPrivilegeDetailRequest();
                    dcrGetPrivilegeDetailRequest.setIds(id);
                    DCRWSGetPrivilegeDetail dcrwsGetPrivilegeDetail =
                            new DCRWSGetPrivilegeDetail(this);
                    dcrwsGetPrivilegeDetail.setRequest(dcrGetPrivilegeDetailRequest);
                    dcrwsGetPrivilegeDetail.run(null);
                    llPrivilegeDetailContent.setVisibility(View.INVISIBLE);
                }
            }

        }

    }

    @Override
    protected void bindData() {


    }

    protected void fillDataHeader(DCRPrivilegeObject privilegeObject) {
        mDcrPrivilegeObject = privilegeObject;
        if (mDcrPrivilegeObject == null) {
            return;
        }
        if (CommonUtils.isStringValid(privilegeObject.getName())) {
            tvPrivilegeName.setText(privilegeObject.getName());
        }
        if (CommonUtils.isStringValid(privilegeObject.getSummary())) {
            tvSummary.setText(privilegeObject.getSummary());
        } else {
            tvSummary.setVisibility(View.GONE);
        }
    }

    protected void fillDataFooter(DCRPrivilegeObject privilegeObject) {
        mDcrPrivilegeObject = privilegeObject;
        if (mDcrPrivilegeObject == null) {
            return;
        }
        if (CommonUtils.isStringValid(privilegeObject.getWebsite_url())) {
            layoutVisitWebsite.setVisibility(View.VISIBLE);
        } else {
            layoutVisitWebsite.setVisibility(View.GONE);
        }
        if (CommonUtils.isStringValid(privilegeObject.getName())) {
            tvImageCopyRight.setText(getString(R.string.privilege_detail_text_copy_right_by_format,
                                               privilegeObject.getName()));
        } else {
            tvImageCopyRight.setVisibility(View.GONE);
        }
        if (privilegeObject.getImages() != null && privilegeObject.getImages()
                                                                  .size() > 0) {
            mGalleries.setData(privilegeObject.getImages());
        }
        llPrivilegeDetailContent.setVisibility(View.VISIBLE);

    }

    protected void fillDataCenter(DCRPrivilegeObject privilegeObject) {

    }

    protected void fillData(DCRPrivilegeObject privilegeObject) {
        fillDataHeader(privilegeObject);
        fillDataCenter(privilegeObject);
        fillDataFooter(privilegeObject);
    }

    private DCRGetPrivilegeDetailResponse convertedPrivelegeObject(DCRGetPrivilegeDetailResponse privilegeDetailResponse) {
       DCRGetPrivilegeDetailResponse convertedPrivilegeObject = privilegeDetailResponse;
       if (convertedPrivilegeObject.getData().get(0).getAddress_country().getName().equalsIgnoreCase("Россия")){
           convertedPrivilegeObject.getData().get(0).getAddress_country().setName("Российская Федерация");
       }
       return convertedPrivilegeObject;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showLogoApp(false);
        }
    }

    @Override
    public boolean onBack() {
        return false;
    }


    @Override
    public void onDCRResponse(final DCRBaseResponse response) {
        hideDialogProgress();
        if (getActivity() == null) {
            return;
        }
        if (response instanceof DCRGetPrivilegeDetailResponse) {
            DCRGetPrivilegeDetailResponse privilegeObject = (DCRGetPrivilegeDetailResponse) response;
            if (privilegeObject != null && privilegeObject.getData() != null && privilegeObject.getData().size() > 0) {
                fillData(convertedPrivelegeObject(privilegeObject).getData().get(0));
            }
        }
    }

    @Override
    public void onDCRResponseOnList(final List<DCRBaseResponse> responseList) {
        hideDialogProgress();

    }

    @Override
    public void onDCRFailure(final String errorMessage,
                             final String errorCode) {
        hideDialogProgress();

    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater,
                                           container,
                                           savedInstanceState);
        ButterKnife.bind(this,
                         rootView);
        return rootView;
    }

    @OnClick({R.id.tvVisitWebsite})
    public void onWebClick(View view) {
        if (mDcrPrivilegeObject != null && CommonUtils.isStringValid(mDcrPrivilegeObject.getWebsite_url())) {

            Bundle bundle = new Bundle();
            bundle.putString(PrivilegeDetailWebFragment.AboutWebTitle,
                             titleName);
            bundle.putString(PrivilegeDetailWebFragment.AboutWebLink,
                             mDcrPrivilegeObject.getWebsite_url());
            bundle.putString(PrivilegesFragment.PRIVILEGE_TYPE,
                             privilegeType);
            if (mDcrPrivilegeObject != null) {
                bundle.putSerializable(PrivilegeDetailWebFragment.PRIVILEGE_DATA,
                                       mDcrPrivilegeObject);
            }
            PrivilegeDetailWebFragment fragment = new PrivilegeDetailWebFragment();
            fragment.setArguments(bundle);

            pushFragment(fragment,
                         true,
                         true);
        }
    }


    @OnClick({R.id.btnPrivilegeDetailBook})
    protected void onPrivilegeDetailBookClick(View view) {
        if (!UserItem.isLogined()) {
            showDialogRequiredSignIn();
            return;
        }
    }

    public void showDialogRequiredSignIn() {
        CommonUtils.showDialogRequiredSignIn(getContext(),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog,
                                        final int which) {
                        Intent intent = new Intent(getContext(),
                                LoginAcitivy.class);
                        startActivity(intent);
                    }
                },
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog,
                                        final int which) {

                    }
                });


    }

}
