package com.rosbank.android.russia.apiservices.b2c;

import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CApiProviderClient;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CICallback;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.B2CChangePasswordRequest;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CChangePasswordResponse;

import java.util.Map;

import retrofit2.Callback;

public class B2CWSChangePassword
        extends B2CApiProviderClient<B2CChangePasswordResponse>{
    private B2CChangePasswordRequest request;
    public B2CWSChangePassword(B2CICallback callback){
        this.b2CICallback = callback;
    }
    public void setRequest(B2CChangePasswordRequest b2CChangePasswordRequest){
        request = b2CChangePasswordRequest;
    }
    @Override
    public void run(Callback<B2CChangePasswordResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }
    @Override
    protected boolean isUseAuthenticateInApi() {
        return true;
    }

    @Override
    protected void runApi() {
        serviceInterface.changePassword(request).enqueue(this);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        return null;
    }

    @Override
    protected void postResponse(B2CChangePasswordResponse response) {
    }
}
