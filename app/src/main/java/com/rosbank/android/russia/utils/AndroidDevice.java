package com.rosbank.android.russia.utils;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Point;
import android.os.Build;
import android.provider.Settings.Secure;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;


/**
 * Multi screen: http://developer.android.com/training/multiscreen/index.html<br>
 * Desing icon: http://developer.android.com/design/style/iconography.html<br>
 * */
public class AndroidDevice {

	/**
	 * Get screen type: small, normal, large<br>
	 * Configuration.SCREENLAYOUT_SIZE_XLARGE<br>
	 * Configuration.SCREENLAYOUT_SIZE_LARGE<br>
	 * Configuration.SCREENLAYOUT_SIZE_NORMAL<br>
	 * Configuration.SCREENLAYOUT_SIZE_SMALL
	 * */
	public static int getScreenType(Context context){
		
		int screenSize = context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK;

		switch(screenSize) {
			case Configuration.SCREENLAYOUT_SIZE_XLARGE:
				System.out.println("SCREENLAYOUT_SIZE_XLARGE");
			break;
		    case Configuration.SCREENLAYOUT_SIZE_LARGE:
				System.out.println("SCREENLAYOUT_SIZE_LARGE");
		        break;
		    case Configuration.SCREENLAYOUT_SIZE_NORMAL:
				System.out.println("SCREENLAYOUT_SIZE_NORMAL");
		        break;
		    case Configuration.SCREENLAYOUT_SIZE_SMALL:
				System.out.println("SCREENLAYOUT_SIZE_SMALL");
		        break;
		    default:
				System.out.println("Screen size is neither xlarg, large, normal or small");
		    	
		}
		
		return screenSize;
		
	}
	
	/**
	 * Get the screen size (w, h) pixel
	 * @return int[2] = {width, height}
	 * */
	public static int[] getScreenSizeDemension(Context context){
		
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		
		int[] result = new int[2];
		int width = 0;
		int height = 0;


		if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB_MR2) {
			/*
			 * getSize was introduced (in API level 13)
			 * 
			 * */
		
			Point size = new Point();
			display.getSize(size);
			width = size.x;
			height = size.y;
			
		}
		else{
			width = display.getWidth();  // deprecated
			height = display.getHeight();  // deprecated
		}

		System.out.println("Screen size: " + width + "-" + height);
		
		result[0] = width;
		result[1] = height;

		return result;
		
	}
	
	private static float[]getDenstity(Context context){
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		DisplayMetrics metrics = new DisplayMetrics();
		display.getMetrics(metrics);
		
		float density = metrics.density;
		int densityDpi = metrics.densityDpi;
		
		if (densityDpi== DisplayMetrics.DENSITY_XHIGH) {
			System.out.println("DENSITY_XHIGHT..., Density  is " + density + " - " + String.valueOf(densityDpi) + "dpi");
        }
		else if (densityDpi== DisplayMetrics.DENSITY_HIGH) {
			System.out.println("DENSITY_HIGH... Density number is " + density + " - " + String.valueOf(densityDpi) + "dpi");
        }
        else if (densityDpi== DisplayMetrics.DENSITY_MEDIUM) {
			System.out.println("DENSITY_MEDIUM... Density number is " + density + " - " + String.valueOf(densityDpi) + "dpi");
        }
        else if (densityDpi== DisplayMetrics.DENSITY_LOW) {
			System.out.println("DENSITY_LOW... Density number is " + density + " - " + String.valueOf(densityDpi) + "dpi");
        }
        else if (densityDpi == DisplayMetrics.DENSITY_XXHIGH) {
			System.out.println("DENSITY_XXHIGHT... Density number is " + density + " - "
					+ String.valueOf(densityDpi) + "dpi");
		}
       /* else if (densityDpi == DisplayMetrics.DENSITY_XXXHIGH) {
			LogUtil.systemOut("DENSITY_XHIGHT... Density is " + density + " - "
					+ String.valueOf(densityDpi) + "dpi");
		} */
        else {
			System.out.println("Density is neither XHIGH, HIGH, MEDIUM OR LOW.  Density is " + density + " - " + String.valueOf(densityDpi));
        }
		
		float[] value = new float[2];
		value[0] = metrics.density;
		value[1] = metrics.densityDpi;
		
		return value;
	}
	
	/**
	 * @return
	 * ~640dpi - xxhdpi:<br>
	 * ~480dpi - xxhdpi:<br>
	 * ~320dpi - xhdpi<br>
	 * ~240dpi - hdpi<br>
	 * ~160dpi - mdpi(baseline)<br> 
	 * ~120dpi - ldpi 
	 * */
	public static int GetDensityDpi(Context context){
		
		float[]value = getDenstity(context);
		if (value==null) {
			return 0;
		}
		
        return (int)value[1];
	}
	
	/**
	 * @return
	 * xxhdpi:<br>
	 * xxhdpi:<br>
	 * xhdpi: 2.0<br>
	 * hdpi: 1.5<br>
	 * mdpi: 1.0 (baseline)<br> 
	 * ldpi: 0.75 
	 * */
	public static float GetDensity(Context context){
		
		float[]value = getDenstity(context);
		if (value==null) {
			return 0;
		}
		
        return value[0];
	}
	
	public static String GetDeviceName(){
		//android.os.Build.MANUFACTURER;
		String name = android.os.Build.MODEL;
		String manufacturer = android.os.Build.MANUFACTURER;
		System.out.println(name);
		System.out.println(manufacturer);
		return name;
	}
	
	/**	 * 
	 * This method converts dp unit to equivalent pixels, depending on device density. 
	 * 
	 * @param dp A value in dp (density independent pixels) unit. Which we need to convert into pixels
	 * @param context Context to get resources and device specific display metrics
	 * @return A float value to represent px equivalent to dp depending on device density
	 */
	public static int dpToPx(int dp, Context context) {
		int dpi = GetDensityDpi(context);
        return (int) (dp * ((float) dpi / (float) 160));
    }
	
	/**
	 * This method converts device specific pixels to density independent pixels.
	 * 
	 * @param px A value in px (pixels) unit. Which we need to convert into db
	 * @param context Context to get resources and device specific display metrics
	 * @return A float value to represent dp equivalent to px value
	 */
	public static float pixelsToDp(float px, Context context){
		int dpi = GetDensityDpi(context);
	    float dp = px / (dpi / 160f);
	    return dp;
	}
	
	public static String GetDeviceId(Context mContext) {
        return Secure.getString(mContext.getContentResolver(), Secure.ANDROID_ID);
    }
	
}
