package com.rosbank.android.russia.apiservices.ResponseModel;

import com.rosbank.android.russia.apiservices.apis.appimplement.BaseResponse;
import com.rosbank.android.russia.model.BookingRestaunrantData;


public class BookRestaurantResponse
        extends BaseResponse {
    private BookingRestaunrantData Data;

    public BookingRestaunrantData getData() {
        return Data;
    }

    public void setData(BookingRestaunrantData data) {
        this.Data = data;
    }
}
