package com.rosbank.android.russia.fragment;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.activitys.HomeActivity;
import com.rosbank.android.russia.adapter.CommonLandingAdapter;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.interfaces.OnRecyclerViewItemClickListener;
import com.rosbank.android.russia.model.CommonLandingObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/*
 * Created by chau.nguyen on 10/05/2016.
 */
public class ConciergeServiceLandingFragment extends BaseFragment {

    @BindView(R.id.rcvCommonLanding)
    RecyclerView rcvConcierge;

    public ConciergeServiceLandingFragment() {

    }

    @Override
    protected int layoutId() {
        return R.layout.fragment_common_landing;
    }

    @Override
    protected void initView() {
        CommonLandingAdapter adapter = new CommonLandingAdapter(createDataConcierge(), new OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(int position, View view) {
                //after click Concierge Service Item
                if (view.getTag() instanceof CommonLandingObject) {
                    switch (((CommonLandingObject) view.getTag()).getImgId()) {
                        case R.drawable.img_concierge_service_hotel:
                            pushFragment(new BookingHotelFragment(), true, true);
                            break;
                        case R.drawable.img_concierge_service_gourmet:
                            pushFragment(new ConciergeGourmetFragment(), true, true);
                            break;
                        case R.drawable.img_concierge_service_rental:
                            pushFragment(new BookingCarRentalTransferFragment(), true, true);
                            break;
                        case R.drawable.img_concierge_service_golf:
                            pushFragment(new BookingGolfFragment(), true, true);
                            break;
                        case R.drawable.img_concierge_service_entertainment:
                            pushFragment(new BookingEventFragment(), true, true);
                            break;
                        case R.drawable.img_concierge_service_others:
                            pushFragment(new BookingOtherFragment(), true, true);
                            break;
                        default:
                            break;
                    }
                }
            }
        });
        LinearLayoutManager layoutManager = new LinearLayoutManager(view.getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rcvConcierge.setLayoutManager(layoutManager);
        rcvConcierge.setAdapter(adapter);
    }

    private List<CommonLandingObject> createDataConcierge() {
        List<CommonLandingObject> data = new ArrayList<>();
        data.add(new CommonLandingObject(getString(R.string.concierge_item_gourmet), R.drawable.img_concierge_service_gourmet));
        data.add(new CommonLandingObject(getString(R.string.concierge_item_hotel), R.drawable.img_concierge_service_hotel));
        data.add(new CommonLandingObject(getString(R.string.concierge_item_entertainment), R.drawable.img_concierge_service_entertainment));
        data.add(new CommonLandingObject(getString(R.string.concierge_item_car_rental), R.drawable.img_concierge_service_rental));
        data.add(new CommonLandingObject(getString(R.string.concierge_item_golf), R.drawable.img_concierge_service_golf));
        data.add(new CommonLandingObject(getString(R.string.concierge_item_others), R.drawable.img_concierge_service_others));
        return data;
    }

    @Override
    protected void bindData() {

    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showLogoApp(false);
            ((HomeActivity) getActivity()).setTitle(getString(R.string.concierge_bar_title));
        }
    }

    @Override
    public boolean onBack() {
        return false;
    }
}
