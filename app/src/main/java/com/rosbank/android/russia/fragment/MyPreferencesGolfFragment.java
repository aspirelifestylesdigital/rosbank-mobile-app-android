package com.rosbank.android.russia.fragment;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.activitys.HomeActivity;
import com.rosbank.android.russia.apiservices.ResponseModel.UpdateGolfResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CICallback;
import com.rosbank.android.russia.apiservices.b2c.B2CWSGetPreference;
import com.rosbank.android.russia.apiservices.b2c.B2CWSUpsertPreferenceRequest;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.B2CUpsertPreferenceRequest;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.preference.PreferenceDetailGolfRequest;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.preference.PreferenceDetailRequest;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.model.KeyValueObject;
import com.rosbank.android.russia.model.preference.GolfPreferenceDetailData;
import com.rosbank.android.russia.model.preference.PreferenceData;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.EntranceLock;
import com.rosbank.android.russia.utils.FontUtils;
import com.rosbank.android.russia.utils.Logger;
import com.rosbank.android.russia.widgets.NumberPickerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nga.nguyent on 9/27/2016.
 */
public class MyPreferencesGolfFragment
        extends BaseFragment
        implements SelectDataFragment.OnSelectDataListener,
                   Callback {

    @BindView(R.id.tvTitle)
    protected TextView tvTitle;
    @BindView(R.id.edtGolfCourse)
    protected EditText edtGolfCourse;
    @BindView(R.id.tvTeeTime)
    protected TextView tvTeeTime;
    @BindView(R.id.golfHandicapNumberPick)
    NumberPickerView golfHandicapNumberPick;
    @BindView(R.id.container)
    LinearLayout mContainer;
    @BindView(R.id.btnSave)
    Button btnSave;
    @BindView(R.id.btnCancel)
    Button btnCancel;

    List<KeyValueObject> teeTimesPreferences = new ArrayList<>();
    GolfPreferenceDetailData golfPreferenceDetailData;
    EntranceLock entranceLock = new EntranceLock();

    @Override
    protected int layoutId() {
        return R.layout.fragment_preference_golf;
    }

    @Override
    protected void initView() {
        ButterKnife.bind(this,
                         view);

        golfHandicapNumberPick.setMinimum(0);
        CommonUtils.setFontForViewRecursive(mContainer,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
        CommonUtils.setFontForViewRecursive(tvTitle,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        CommonUtils.setFontForViewRecursive(btnSave,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        CommonUtils.setFontForViewRecursive(btnCancel,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);

        showDialogProgress();
        B2CWSGetPreference b2CWSGetPreference = new B2CWSGetPreference(getPreferenceCallback);
        b2CWSGetPreference.run(null);
    }

    private void updateUI() {
        if (getActivity() == null) {
            return;
        }
        if (golfPreferenceDetailData != null) {
            if (!TextUtils.isEmpty(golfPreferenceDetailData.getTeeTimes())) {
                String[] selectedTeeTimesArr = golfPreferenceDetailData.getTeeTimes()
                                                                       .split("\\s*,\\s*");
                if (selectedTeeTimesArr != null && selectedTeeTimesArr.length > 0) {
                    teeTimesPreferences = new ArrayList<>();
                    for (String selectedTeetime : selectedTeeTimesArr) {
                        KeyValueObject keyValueObject = new KeyValueObject(selectedTeetime,
                                                                           selectedTeetime);
                        teeTimesPreferences.add(keyValueObject);
                    }
                }
            }
            updateGolfPreferences();
        }
    }

    @Override
    protected void bindData() {
        updateGolfPreferences();
    }

    @Override
    protected boolean onBack() {
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showLogoApp(false);
            ((HomeActivity) getActivity()).setTitle(getString(R.string.title_mypreference));
        }
        hideRightText();
        showToolbarMenuIcon();

    }

    @OnClick({R.id.tvTeeTime,
              R.id.edtGolfCourse,
              R.id.btnSave,
              R.id.btnCancel})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvTeeTime:
                //showToast("Cuisine");
                /*SelectDataFragment teeTimeFrag = new SelectDataFragment();
                teeTimeFrag.setDataType(SelectDataFragment.APIDATA.PREFERRED_TEE_TIME);
                teeTimeFrag.setOnSelectListener(this);
                if(teeTimesPreferences !=null) {
                    teeTimeFrag.setCuisineSelected(teeTimesPreferences);
                }
                pushFragment(teeTimeFrag, true, true);*/
                TeeTimeFragment teeTimeFragment = new TeeTimeFragment();
                teeTimeFragment.setSelectedData((teeTimesPreferences != null && teeTimesPreferences.size() > 0) ?
                                                teeTimesPreferences.get(0)
                                                                   .getValue() :
                                                null);
                teeTimeFragment.setSelectionCallback(new TeeTimeFragment.SelectionCallback() {
                    @Override
                    public void onFinishSelection(Bundle bundle) {
                        if (teeTimesPreferences == null) {
                            teeTimesPreferences = new ArrayList<KeyValueObject>();
                        }
                        teeTimesPreferences.clear();
                        String teeTime =
                                (String) bundle.getSerializable(TeeTimeFragment.PRE_SELECTION_DATA);
                        if (!TextUtils.isEmpty(teeTime)) {
                            teeTimesPreferences.add(new KeyValueObject(teeTime,
                                                                       teeTime));
                        }
                        updateGolfPreferences();
                    }
                });
                pushFragment(teeTimeFragment,
                             true,
                             true);
                break;
            case R.id.edtGolfCourse:
                //showToast("Other");
                break;
            case R.id.btnSave:
                //showToast("Save");
                CommonUtils.hideSoftKeyboard(getActivity());
                if (!entranceLock.isClickContinuous()) {
                    saveGolf();
                }
                break;
            case R.id.btnCancel:
                //showToast("Cancel");
                if (!entranceLock.isClickContinuous()) {
                    onBackPress();
                }
                break;
        }
    }

    @Override
    public void onSelectedData(SelectDataFragment.APIDATA APIDATA,
                               List<KeyValueObject> list) {
        Logger.sout("selectedddd " + list.size());
        teeTimesPreferences = list;
        //
        //updateGolfPreferences();
    }

    private void updateGolfPreferences() {
        String text = "";
        if (teeTimesPreferences != null) {
            for (int i = 0; i < teeTimesPreferences.size(); i++) {
                if (text.length() > 0) {
                    text += ", ";
                }
                text += teeTimesPreferences.get(i)
                                           .getValue();
            }
        }

        if (text.length() == 0) {
            text = getString(R.string.text_please_select_preferred_tee_times);
            tvTeeTime.setTextColor(ContextCompat.getColor(getContext(),
                                                          R.color.hint_color));
        } else {
            tvTeeTime.setTextColor(ContextCompat.getColor(getContext(),
                                                          R.color.text_color_active));
        }

        tvTeeTime.setText(text);
        edtGolfCourse.setText(golfPreferenceDetailData != null ?
                              golfPreferenceDetailData.getCourseName() :
                              "");
        if (golfPreferenceDetailData != null) {
            if (!TextUtils.isEmpty(golfPreferenceDetailData.getPreferredGolfHandicap())) {
                try {
                    golfHandicapNumberPick.setNumber(Integer.parseInt(golfPreferenceDetailData.getPreferredGolfHandicap()));
                } catch (NumberFormatException e) {

                }
            }
        }
    }

    private void saveGolf() {

        /*if(!UserItem.isLogined())
        {
            showToast(getString(R.string.text_message_need_login_update_gourmet));
            return;
        }

        showDialogProgress();

        String textOther = edtGolfCourse.getText().toString();
        List<String>ids = getCuisineIds();
        String userId = UserItem.getLoginedId();

        UpdategGolfRequest request = new UpdategGolfRequest();
        if(!StringUtil.isEmpty(textOther))
            request.setCourseName(textOther);
        if(ids.size()>0)
            request.setTeeTimes(ids);
        request.setUserID(userId);

        WSUpdateGolfPreference ws = new WSUpdateGolfPreference();
        ws.setRequest(request);
        ws.run(this);*/
        AppConstant.PREFERENCE_EDIT_TYPE preferenceEditType = golfPreferenceDetailData != null ?
                                                              AppConstant.PREFERENCE_EDIT_TYPE.UPDATE :
                                                              AppConstant.PREFERENCE_EDIT_TYPE.ADD;
        PreferenceDetailRequest preferenceDetailRequest = new PreferenceDetailGolfRequest("false");
        switch (preferenceEditType) {
            case ADD:
                break;
            case UPDATE:
                preferenceDetailRequest.setMyPreferencesId(golfPreferenceDetailData.getPreferenceId());
                break;
        }
        preferenceDetailRequest.fillData(edtGolfCourse.getText()
                                                      .toString()
                                                      .trim(),
                                         CommonUtils.getStringArrWithSeparator(teeTimesPreferences,
                                                                               ","),
                                         String.valueOf(golfHandicapNumberPick.getNumber()));

        B2CUpsertPreferenceRequest upsertPreferenceRequest = new B2CUpsertPreferenceRequest();
        upsertPreferenceRequest.setPreferenceDetails(preferenceDetailRequest);
        B2CWSUpsertPreferenceRequest b2CWSUpsertPreferenceRequest =
                new B2CWSUpsertPreferenceRequest(preferenceEditType,
                                                 upsertPreferenceCallback);
        b2CWSUpsertPreferenceRequest.setRequest(upsertPreferenceRequest);
        b2CWSUpsertPreferenceRequest.run(null);
        showDialogProgress();
    }

    private List<String> getCuisineIds() {
        List<String> ids = new ArrayList<>();

        if (teeTimesPreferences != null) {
            for (int i = 0; i < teeTimesPreferences.size(); i++) {
                KeyValueObject cuisine = teeTimesPreferences.get(i);
                //
                ids.add(cuisine.getKey());
            }
        }

        return ids;
    }

    @Override
    public void onResponse(Call call,
                           Response response) {
        if (getActivity() == null) {
            return;
        }
        if (response.body() instanceof UpdateGolfResponse) {
            UpdateGolfResponse rsp = (UpdateGolfResponse) response.body();
            if (rsp.isSuccess()) {
                onBackPress();
            } else {
//                showToast(getString(R.string.text_message_update_gourmet_fail));
            }
        }
        hideDialogProgress();
    }

    @Override
    public void onFailure(Call call,
                          Throwable t) {
        hideDialogProgress();
        if (getActivity() == null) {
            return;
        }
//        showToast(getString(R.string.text_message_update_gourmet_fail));
    }

    private B2CICallback getPreferenceCallback = new B2CICallback() {
        @Override
        public void onB2CResponse(B2CBaseResponse response) {
            hideDialogProgress();
            if (getActivity() == null) {
                return;
            }
        }

        @Override
        public void onB2CResponseOnList(List<B2CBaseResponse> responseList) {
            hideDialogProgress();
            if (getActivity() == null) {
                return;
            }
            golfPreferenceDetailData =
                    (GolfPreferenceDetailData) PreferenceData.findFromList(responseList,
                                                                           AppConstant.PREFERENCE_TYPE.GOLF);
            updateUI();
        }

        @Override
        public void onB2CFailure(String errorMessage,
                                 String errorCode) {
            hideDialogProgress();
            if (getActivity() == null) {
                return;
            }
        }
    };

    private B2CICallback upsertPreferenceCallback = new B2CICallback() {
        @Override
        public void onB2CResponse(B2CBaseResponse response) {
            hideDialogProgress();
            if (getActivity() == null) {
                return;
            }
            if (response != null && response.isSuccess()) {
                onBackPress();
            } else {
//                showToast(getString(R.string.text_server_error_message));
                showDialogMessage("",
                                  getString(R.string.text_concierge_request_error));
            }
        }

        @Override
        public void onB2CResponseOnList(List<B2CBaseResponse> responseList) {
            if (getActivity() == null) {
                return;
            }
        }

        @Override
        public void onB2CFailure(String errorMessage,
                                 String errorCode) {
            hideDialogProgress();
            if (getActivity() == null) {
                return;
            }
//            showToast(getString(R.string.text_server_error_message));
            showDialogMessage("",
                              getString(R.string.text_concierge_request_error));
        }

    };
}
