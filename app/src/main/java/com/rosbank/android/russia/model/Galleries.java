package com.rosbank.android.russia.model;

import com.google.gson.annotations.Expose;

/**
 * Created by nga.nguyent on 9/22/2016.
 */
public class Galleries {
    @Expose
    private String CaptionDescription;
    @Expose
    private String CaptionTitle;
    @Expose
    private String BackgroundImageUrl;
    @Expose
    private String IsVideo;
    @Expose
    private String VideoLink;
    @Expose
    private String ImageAlt;
    @Expose
    private String Thumbnail;

    public String getCaptionDescription() {
        return CaptionDescription;
    }

    public void setCaptionDescription(String captionDescription) {
        CaptionDescription = captionDescription;
    }

    public String getCaptionTitle() {
        return CaptionTitle;
    }

    public void setCaptionTitle(String captionTitle) {
        CaptionTitle = captionTitle;
    }

    public String getBackgroundImageUrl() {
        return BackgroundImageUrl;
    }

    public void setBackgroundImageUrl(String backgroundImageUrl) {
        BackgroundImageUrl = backgroundImageUrl;
    }

    public String getIsVideo() {
        return IsVideo;
    }

    public void setIsVideo(String isVideo) {
        IsVideo = isVideo;
    }

    public String getVideoLink() {
        return VideoLink;
    }

    public void setVideoLink(String videoLink) {
        VideoLink = videoLink;
    }

    public String getImageAlt() {
        return ImageAlt;
    }

    public void setImageAlt(String imageAlt) {
        ImageAlt = imageAlt;
    }

    public String getThumbnail() {
        return Thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        Thumbnail = thumbnail;
    }
}