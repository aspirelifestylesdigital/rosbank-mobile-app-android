package com.rosbank.android.russia.dcr.api;


import com.rosbank.android.russia.apiservices.ResponseModel.AuthenticateResponse;
import com.rosbank.android.russia.dcr.api.RequestModel.DCRGetPrivilegesRequest;
import com.rosbank.android.russia.dcr.api.ResponseModel.DCRGetPrivilegesResponse;

import java.util.Map;

import retrofit2.Callback;

public class DCRWSGetPrivileges
        extends DCRApiProviderClient<DCRGetPrivilegesResponse> {

    public DCRWSGetPrivileges(DCRICallback callback){
        this.dcrICallback = callback;
    }
    private DCRGetPrivilegesRequest request = new DCRGetPrivilegesRequest();
    public void setRequest(DCRGetPrivilegesRequest dcrBaseRequest){
        request = dcrBaseRequest;
    }

    @Override
    protected void runSignAuthenticate() {

    }

    @Override
    protected void authenticateSuccess(final AuthenticateResponse token) {

    }

    @Override
    protected String getAuthorizeToken() {
        return null;
    }

    @Override
    public void run(Callback<DCRGetPrivilegesResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }

    @Override
    protected boolean isUseAuthenticateInApi() {
        return false;
    }

    @Override
    protected void runApi() {
           /*api/v1/items/search?language=en&item_type=privilege&term=training%20sessions&term_
    operator=OR&exact_match=false&sort=Name&facets=Type HTTP/1.1
    Host: aspiredigitalapitest.azure-api.net/dcr
    Ocp-Apim-Subscription-Key: KhXfp555U_hCIUxdJns0A23h59lhm9qqU4jfF-VMd7e*/
        serviceInterface.getPrivileges(request.toMap()).enqueue(this);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        return super.prepareHeaders() ;
    }

    @Override
    protected void postResponse(DCRGetPrivilegesResponse response) {
    }
}
