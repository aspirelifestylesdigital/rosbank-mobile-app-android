package com.rosbank.android.russia.apiservices;

import com.rosbank.android.russia.apiservices.ResponseModel.GetMyPreferenceResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.ApiProviderClient;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Callback;


public class WSGetMyPreferences
        extends ApiProviderClient<GetMyPreferenceResponse> {
    //
    private String mUserID;

    public WSGetMyPreferences(){

    }

    public void getMyPreferences(String userId){
        mUserID = userId;
    }

    @Override
    public void run(Callback<GetMyPreferenceResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }

    @Override
    protected boolean isUseAuthenticateInApi() {
        return true;
    }

    @Override
    public void runApi() {
        serviceInterface.getMyPreferences(mUserID).enqueue(callback);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("User-Agent", "Retrofit-Sample-App");
        return headers;
    }

}
