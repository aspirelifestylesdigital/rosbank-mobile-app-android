package com.rosbank.android.russia.model.concierge;

import android.text.TextUtils;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CGetRecentRequestResponse;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.application.AppContext;
import com.rosbank.android.russia.utils.DateTimeUtil;

/**
 * Created by ThuNguyen on 10/26/2016.
 */

public class CarBookingDetailData extends MyRequestObject {
    private String DriverName;
    private String DriverAge;
    private boolean InternationalLicense;
    private String PickupDate;
    private String PickupLocation;
    private String DropOffDate;
    private String DropOffLocation;
    private String PreferredVehicles;
    private String PreferredCarRentals;
    private String MinimumPrice;
    private String MaximumPrice;
    private String TransportType;
    private int NumberOfAdults;
    private String CreateDate;
    private long createDateEpoc;

    private long pickupDateEpoc;
    private long dropOffDateEpoc;

    public int getNumberOfPassenger() {
        return NumberOfPassenger;
    }

    public void setNumberOfPassenger(final int numberOfPassenger) {
        NumberOfPassenger = numberOfPassenger;
    }

    private int NumberOfPassenger;

    public CarBookingDetailData(B2CGetRecentRequestResponse recentRequestResponse) {
        super(recentRequestResponse);
        PickupDate = recentRequestResponse.getPICKUP();
        if(!TextUtils.isEmpty(recentRequestResponse.getNUMBEROFADULTS())) {
            try {
                NumberOfAdults = Integer.parseInt(recentRequestResponse.getNUMBEROFADULTS());
            }catch (Exception e){}

        }
        CreateDate = recentRequestResponse.getCREATEDDATE();
        createDateEpoc = DateTimeUtil.shareInstance().convertToTimeStampFromGMT(CreateDate, AppConstant.DATE_FORMAT_YYYY_MM_DD_T_HH_MM_SS);
        // Booking history
        pickupDateEpoc = DateTimeUtil.shareInstance().convertToTimeStamp(PickupDate, AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS);

        dropOffDateEpoc = DateTimeUtil.shareInstance().convertToTimeStamp(DropOffDate, AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS);

        // Item title
        if(!TextUtils.isEmpty(DriverName)) {

           // ItemTitle = "Car rental";
            ItemTitle = AppContext.getSharedInstance().getResources().getString(R.string.text_car_rental);
        }else{
           // ItemTitle = "Transfer by " + getConciergeRequestDetail().getTransportType();
            ItemTitle = /* AppContext.getSharedInstance().getResources().getString(R.string.text_transfer_by)+ */getConciergeRequestDetail().getTransportType();
        }
        ItemDescription =  AppContext.getSharedInstance().getResources().getString(R.string.text_pick_up)+ getPickupLocation();
        ItemDescription += "\n"+ AppContext.getSharedInstance().getResources().getString(R.string.text_pick_up_date)+ DateTimeUtil.shareInstance().convertTime(PickupDate, AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS,
                AppContext.getSharedInstance().getResources().getString(R.string.text_my_request_reservation_date_format));
        ItemDescription += "\n"+ AppContext.getSharedInstance().getResources().getString(R.string.text_drop_off) + getDropOffLocation();
        if(!TextUtils.isEmpty(DropOffDate)){
            ItemDescription += "\n" + AppContext.getSharedInstance().getResources().getString(R.string.text_drop_off_date) +DateTimeUtil.shareInstance().convertTime(DropOffDate, AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS,
                    AppContext.getSharedInstance().getResources().getString(R.string.text_my_request_reservation_date_format));
        }
    }
    @Override
    protected void initRequestProperties() {
        // Mapping properties
        DriverName = conciergeRequestDetail.getNameofdriver();
        DriverAge = conciergeRequestDetail.getAgeofdriver();
        InternationalLicense = conciergeRequestDetail.getDoesthedriveholdavalidinternationaldrivinglicence() != null && (conciergeRequestDetail.getDoesthedriveholdavalidinternationaldrivinglicence().equalsIgnoreCase("yes") || conciergeRequestDetail.getDoesthedriveholdavalidinternationaldrivinglicence().equalsIgnoreCase("true"));
        PickupLocation = conciergeRequestDetail.getPickupLocation();
        DropOffLocation = conciergeRequestDetail.getDropoffLocation();
        DropOffDate = conciergeRequestDetail.getDropoffDate();
        PreferredVehicles = conciergeRequestDetail.getPreferredVehicle();
        PreferredCarRentals = conciergeRequestDetail.getPreferredcarrentalcompany();
        TransportType = conciergeRequestDetail.getTransportType();
        if(!TextUtils.isEmpty(conciergeRequestDetail.getNoofPassengers())) {
            try {
                NumberOfAdults = Integer.parseInt(conciergeRequestDetail.getNoofPassengers());
            }catch (Exception e){}

        }
        // Booking type group
        BookingFilterGroupType = AppConstant.BOOKING_FILTER_TYPE.Car;
    }

    @Override
    public boolean isBookNormalType() {
        return !TextUtils.isEmpty(DriverName);
    }

    @Override
    public long getRequestStartDateEpoch() {
        return pickupDateEpoc;
    }

    @Override
    public long getRequestEndDateEpoch() {
        if(!TextUtils.isEmpty(DriverName)){ // Car rental
            return dropOffDateEpoc;
        }
        return 0;
    }

    @Override
    public String getReservationName() {
        return conciergeRequestDetail.getReservationName();
    }

    public long getPickupDateEpoc() {
        return pickupDateEpoc;
    }

    public void setPickupDateEpoc(long pickupDateEpoc) {
        this.pickupDateEpoc = pickupDateEpoc;
    }

    public long getDropOffDateEpoc() {
        return dropOffDateEpoc;
    }

    public void setDropOffDateEpoc(long dropOffDateEpoc) {
        this.dropOffDateEpoc = dropOffDateEpoc;
    }

    public String getDriverName() {
        return DriverName;
    }

    public void setDriverName(String driverName) {
        DriverName = driverName;
    }

    public String getDriverAge() {
        return DriverAge;
    }

    public void setDriverAge(String driverAge) {
        DriverAge = driverAge;
    }

    public boolean getInternationalLicense() {
        return InternationalLicense;
    }

    public void setInternationalLicense(boolean internationalLicense) {
        InternationalLicense = internationalLicense;
    }

    public String getPickupDate() {
        return PickupDate;
    }

    public void setPickupDate(String pickupDate) {
        PickupDate = pickupDate;
    }

    public String getPickupLocation() {
        return PickupLocation;
    }

    public void setPickupLocation(String pickupLocation) {
        PickupLocation = pickupLocation;
    }

    public String getDropOffDate() {
        return DropOffDate;
    }

    public void setDropOffDate(String dropOffDate) {
        DropOffDate = dropOffDate;
    }

    public String getDropOffLocation() {
        return DropOffLocation;
    }

    public void setDropOffLocation(String dropOffLocation) {
        DropOffLocation = dropOffLocation;
    }

    public String getMinimumPrice() {
        return MinimumPrice;
    }

    public void setMinimumPrice(String minimumPrice) {
        MinimumPrice = minimumPrice;
    }

    public String getMaximumPrice() {
        return MaximumPrice;
    }

    public void setMaximumPrice(String maximumPrice) {
        MaximumPrice = maximumPrice;
    }

    public String getTransportType() {
        return TransportType;
    }

    public void setTransportType(String transportType) {
        TransportType = transportType;
    }

    public int getNumberOfAdults() {
        return NumberOfAdults;
    }

    public void setNumberOfAdults(int numberOfAdults) {
        NumberOfAdults = numberOfAdults;
    }

    public String getPreferredVehicles() {
        return PreferredVehicles;
    }

    public void setPreferredVehicles(String preferredVehicles) {
        PreferredVehicles = preferredVehicles;
    }

    public String getPreferredCarRentals() {
        return PreferredCarRentals;
    }

    public void setPreferredCarRentals(String preferredCarRentals) {
        PreferredCarRentals = preferredCarRentals;
    }
    public long getCreateDateEpoc() {
        return createDateEpoc;
    }

}
