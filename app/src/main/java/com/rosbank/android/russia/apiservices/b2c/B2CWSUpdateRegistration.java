package com.rosbank.android.russia.apiservices.b2c;

import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CApiProviderClient;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CICallback;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.B2CUpdateRegistrationRequest;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CUpdateRegistrationResponse;

import java.util.Map;

import retrofit2.Callback;

public class B2CWSUpdateRegistration
        extends B2CApiProviderClient<B2CUpdateRegistrationResponse>{

    B2CUpdateRegistrationRequest updateRegistrationRequest;

    public B2CWSUpdateRegistration(B2CICallback callback){
        this.b2CICallback = callback;
    }
    @Override
    public void run(Callback<B2CUpdateRegistrationResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }
    public void setRegistrationRequest(B2CUpdateRegistrationRequest registrationRequest){
        this.updateRegistrationRequest = registrationRequest;
    }
    @Override
    protected boolean isUseAuthenticateInApi() {
        return true;
    }

    @Override
    protected void runApi() {
        serviceInterface.updateRegistration(updateRegistrationRequest).enqueue(this);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        return null;
    }

    @Override
    protected void postResponse(B2CUpdateRegistrationResponse response) {
    }
}
