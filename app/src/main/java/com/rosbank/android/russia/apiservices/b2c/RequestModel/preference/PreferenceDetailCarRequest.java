package com.rosbank.android.russia.apiservices.b2c.RequestModel.preference;

import com.google.gson.annotations.Expose;

import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.utils.StringUtil;

import java.util.List;

/**
 * Created by Thu Nguyen on 12/12/2016.
 */

public class PreferenceDetailCarRequest extends PreferenceDetailAddRequest{
    @Expose
    private String PreferredRentalVehicle;
    @Expose
    private String PreferredRentalCompany;
    @Expose
    private String OtherPreferredRentalCompany;
    @Expose
    private String NameofLoyaltyProgram;
    @Expose
    private String MembershipNo;

    public PreferenceDetailCarRequest(String Delete) {
        super(Delete, AppConstant.PREFERENCE_TYPE.CAR.getValue());
    }

    @Override
    public void fillData(Object... values) {
        List<String> valueList = StringUtil.removeAllSpecialCharactersAndFillInList(values);
        if(valueList != null){
            if(valueList.size() > 0){
                PreferredRentalVehicle = valueList.get(0);
            }
            if(valueList.size() > 1){
                PreferredRentalCompany = valueList.get(1);
            }
            if(valueList.size() > 2){
                OtherPreferredRentalCompany = valueList.get(2);
            }
            if(valueList.size() > 3){
                NameofLoyaltyProgram = valueList.get(3);
            }
            if(valueList.size() > 4){
                MembershipNo = valueList.get(4);
            }

        }
    }
}
