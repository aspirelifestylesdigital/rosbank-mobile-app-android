package com.rosbank.android.russia.dcr.widgets;

import android.content.Context;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.helper.HtmlTagHandler;
import com.rosbank.android.russia.helper.urlimage.URLImageParser;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.FontUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/*
 * Created by anh.tirnh on 04/04/2017.
 */
public class ItemPrivilegeDetailInfoPrivileges
        extends RelativeLayout {
    private static final int LAYOUT_RESOURCE_ID = R.layout.item_privilege_detail_info;
    @BindView(R.id.ic_left)
    ImageView icLeft;
    @BindView(R.id.ic_right)
    ImageView icRight;
    @BindView(R.id.layoutInfo)
    LinearLayout layoutInfo;
    @BindView(R.id.layout_contain)
    LinearLayout mLayoutContain;



    public interface ItemPrivilegeDetailInfoListener {
        void onTextInfoClick();
    }

    @BindView(R.id.line)
    View line;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvInfo)
    ExpandableTextView tvInfo;
    @BindView(R.id.tvInfo2)
    TextView tvInfo2;// This is for text unable to expand
    //    @BindView(R.id.readMore)
//    ReadMoreLessView readMore;
    @BindView(R.id.tvTermOfUse)
    ExpandableTextView tvTermOfUse;
    ItemPrivilegeDetailInfoListener mPrivilegeDetailInfoListener;

    public void setPrivilegeDetailInfoListener(ItemPrivilegeDetailInfoListener listener) {
        this.mPrivilegeDetailInfoListener = listener;
    }

    private String mTitle = "";
    private String mDescription = "";
    private String mTermofuse = "";
    private int mDrawableTitle;
    private int mDrawableDescriptionRight;
    private boolean isShowLine = true;
    private boolean isHTML = false;
    private boolean isExpandAsDefault = false;

    /**
     * @param context
     *         The context which view is running on.
     * @return New ItemPrivilegeDetailInfo object.
     * @since v0.1
     */
    public static ItemPrivilegeDetailInfoPrivileges newInstance(final Context context) {
        if (context == null) {
            return null;
        }

        return new ItemPrivilegeDetailInfoPrivileges(context);
    }

    /**
     * The constructor with current context.
     *
     * @param context
     *         The context which view is running on.
     * @since v0.1
     */
    public ItemPrivilegeDetailInfoPrivileges(final Context context) {
        super(context);
        this.initialize();
    }

    /**
     * The constructor with current context.
     *
     * @param context
     *         The context which view is running on.
     * @param attrs
     *         The initialize attributes set.
     * @since v0.1
     */
    public ItemPrivilegeDetailInfoPrivileges(final Context context,
                                             final AttributeSet attrs) {
        super(context,
              attrs);
        this.initialize();
    }

    /**
     * The constructor with current context.
     *
     * @param context
     *         The context which view is running on.
     * @param attrs
     *         The initialize attributes set.
     * @param defStyleAttr
     *         The default style.
     * @since v0.1
     */
    public ItemPrivilegeDetailInfoPrivileges(final Context context,
                                             final AttributeSet attrs,
                                             final int defStyleAttr) {
        super(context,
              attrs,
              defStyleAttr);

        this.initialize();
    }

    /**
     * @see View#setEnabled(boolean)
     * @since v0.1
     */
    @Override
    public void setEnabled(final boolean isEnabled) {
        this.setAlpha(isEnabled ?
                      1f :
                      0.5f);
        super.setEnabled(isEnabled);
    }

    /**
     * Reset injected resources created by ButterKnife.
     *
     * @since v0.1
     */
    public void resetInjectedResource() {
        ButterKnife.bind(this);
    }

    public void setExpandAsDefault(boolean isExpandAsDefault) {
        this.isExpandAsDefault = isExpandAsDefault;
    }

    /**
     * Initialize UI sub views.
     *
     * @since v0.1
     */
    private void initialize() {

        final LayoutInflater layoutInflater = LayoutInflater.from(this.getContext());
        layoutInflater.inflate(LAYOUT_RESOURCE_ID,
                               this,
                               true);

        ButterKnife.bind(this,
                         this);

    }

    public void setPrivileges(String privileges,String termOfUse,
                           boolean isShowLine) {
        this.setData(getContext().getString(R.string.privileges_detail_title_privileges),
                     R.drawable.ic_info_active,
                     privileges,
                     0,
                     termOfUse,
                     isShowLine,
                     true);;

    }

    public void setData(String title,
                        int drawableTitle,
                        String description,
                        int drawableDescriptionRight,
                        String termOfUse) {
        this.setData(title,
                     drawableTitle,
                     description,
                     drawableDescriptionRight,
                     termOfUse,
                     true);

    }

    public void setData(String title,
                        int drawableTitle,
                        String description,
                        int drawableDescriptionRight,
                        String termOfUse,
                        boolean isShowLine) {
        this.mTitle = title;
        this.mDescription = description;
        this.mDrawableTitle = drawableTitle;
        this.mTermofuse = termOfUse;
        this.mDrawableDescriptionRight = drawableDescriptionRight;
        this.isShowLine = isShowLine;
        resetData();

    }

    public void setData(String title,
                        int drawableTitle,
                        String description,
                        int drawableDescriptionRight,
                        String termOfUse,
                        boolean isShowLine,
                        boolean isHTML) {
        this.mTitle = title;
        this.mDescription = description;
        this.mTermofuse = termOfUse;
        this.mDrawableTitle = drawableTitle;
        this.mDrawableDescriptionRight = drawableDescriptionRight;
        this.isShowLine = isShowLine;
        this.isHTML = isHTML;
        resetData();

    }

    private void resetData() {
        CommonUtils.setFontForViewRecursive(tvTitle,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
//        CommonUtils.setFontForViewRecursive(tvInfo,
//                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
//        CommonUtils.setFontForViewRecursive(tvInfo2,
//                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
//        CommonUtils.setFontForViewRecursive(tvTermOfUse,
//                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
        line.setVisibility(this.isShowLine ?
                           VISIBLE :
                           GONE);
//        termOfUse.setData(false,
//                          true);
//        tvInfo.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//                if (tvInfo.getLineCount() > 10) {
//                    tvInfo.setMaxLines(10);
//                    readMore.setVisibility(View.VISIBLE);
//                } else {
//                    readMore.setVisibility(View.GONE);
//                }
//            }
//        });
        if (mDrawableTitle > 0) {
            icLeft.setImageResource(mDrawableTitle);
            icLeft.setVisibility(VISIBLE);
        } else {
            icLeft.setVisibility(GONE);
        }
        if (mDrawableDescriptionRight > 0) {
            icRight.setImageResource(mDrawableDescriptionRight);
            icRight.setVisibility(VISIBLE);
        } else {
            icRight.setVisibility(GONE);
        }
        if (CommonUtils.isStringValid(mTitle)) {
            tvTitle.setText(mTitle);
            tvTitle.setVisibility(VISIBLE);

        } else {
            tvTitle.setVisibility(GONE);
        }
        if (CommonUtils.isStringValid(mDescription)) {
            if (isExpandAsDefault) {
                tvInfo.setVisibility(VISIBLE);
                tvInfo2.setVisibility(GONE);
                //tvInfo.setText(mDescription);
                if (mDescription.contains("<img")) {
                    mDescription = CommonUtils.appendTwoLineForImageTag(mDescription);
                    tvInfo.setmMaxCollapsedLines(12);
                    tvInfo.setText(Html.fromHtml(mDescription,
                                                 new URLImageParser(tvInfo,
                                                                    getContext(),
                                                                    new URLImageParser.IImageParserCallback() {
                                                                        @Override
                                                                        public void onImageParserDone() {
                                                                            line.setVisibility(VISIBLE);
                                                                        }
                                                                    }),
                                                 new HtmlTagHandler()));
                    tvInfo.getTextView()
                          .setMovementMethod(LinkMovementMethod.getInstance());
                    tvInfo.setVisibility(GONE);
                    line.setVisibility(GONE);
                } else {
                    mDescription = CommonUtils.replaceStringForHTMLDisplay(mDescription);
                    CommonUtils.convertHtmlToStr(mDescription,
                                                 tvInfo);
                }

            } else {
                tvInfo.setVisibility(GONE);
                tvInfo2.setVisibility(VISIBLE);
                mDescription = CommonUtils.replaceStringForHTMLDisplay(mDescription);
                CommonUtils.convertHtmlToStr(mDescription,
                                             tvInfo2);

            }

//            if (isHTML) {
//
//                CommonUtils.convertHtmlToStr(
//                        mDescription,
//                        tvInfo.getTextView());
//
//            } else {
//                tvInfo.setText(mDescription);
//            }
//            if (mDrawableTitle > 0) {
//                tvInfo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_none,
//                                                               0,
//                                                               mDrawableDescriptionRight,
//                                                               0);
//            } else {
//                tvInfo.setCompoundDrawablesWithIntrinsicBounds(0,
//                                                               0,
//                                                               mDrawableDescriptionRight,
//                                                               0);
//            }

        } else {
            tvInfo.setVisibility(GONE);
            tvInfo2.setVisibility(GONE);
//            readMore.setVisibility(View.GONE);
        }
        if (CommonUtils.isStringValid(mTermofuse)) {
            tvTermOfUse.setVisibility(VISIBLE);
            CommonUtils.convertHtmlToStr(mTermofuse,
                                         tvTermOfUse);
        } else {
            tvTermOfUse.setVisibility(GONE);
        }
//        readMore.setReadMoreListener(new ReadMoreLessView.ReadMoreViewListener() {
//            @Override
//            public void onReadMoreChange(final boolean isReadmore) {
//                if (isReadmore) {
//                    tvInfo.setMaxLines(1000);
//                } else {
//                    tvInfo.setMaxLines(10);
//                }
//                tvInfo.requestLayout();
//                tvInfo.invalidate();
//            }
//        });
//        termOfUse.setReadMoreListener(new ReadMoreLessView.ReadMoreViewListener() {
//            @Override
//            public void onReadMoreChange(final boolean isReadmore) {
//                if (isReadmore) {
//                    tvTitleTermOfUse.setVisibility(View.VISIBLE);
//                    tvTermOfUse.setVisibility(View.VISIBLE);
//                } else {
//                    tvTitleTermOfUse.setVisibility(View.GONE);
//                    tvTermOfUse.setVisibility(View.GONE);
//                }
//            }
//        });

    }

    @OnClick(R.id.layout_contain)
    public void onClick() {
        if (mPrivilegeDetailInfoListener != null) {
            mPrivilegeDetailInfoListener.onTextInfoClick();
        }
    }

    public void addViewToInfo(View view) {
        layoutInfo.setVisibility(VISIBLE);
        layoutInfo.addView(view);
    }
}