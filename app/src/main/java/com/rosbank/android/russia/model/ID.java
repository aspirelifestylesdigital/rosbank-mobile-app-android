package com.rosbank.android.russia.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

/**
 * Created by nga.nguyent on 9/22/2016.
 */
public class ID implements Parcelable {
    @Expose
    private String Guid;
    @Expose
    private boolean IsGlobalNullId;
    @Expose
    private boolean IsNull;

    public String getGuid() {
        return Guid;
    }

    public void setGuid(String guid) {
        Guid = guid;
    }

    public boolean isGlobalNullId() {
        return IsGlobalNullId;
    }

    public void setGlobalNullId(boolean globalNullId) {
        IsGlobalNullId = globalNullId;
    }

    public boolean isNull() {
        return IsNull;
    }

    public void setNull(boolean aNull) {
        IsNull = aNull;
    }

    @Override
    public int describeContents() {
        return 0;
    }
    protected ID(Parcel in) {
        this.Guid= in.readString();
        this.IsGlobalNullId= in.readByte() != 0;
        this.IsNull= in.readByte() != 0;
    }
    @Override
    public void writeToParcel(final Parcel parcel,
                              final int i) {
        parcel.writeString(this.Guid);
        parcel.writeByte((byte) (this.IsGlobalNullId ? 1 : 0));
        parcel.writeByte((byte) (this.IsNull ? 1 : 0));

    }
    public static final Creator<ID> CREATOR = new Creator<ID>() {
        @Override
        public ID createFromParcel(Parcel source) {
            return new ID(source);
        }

        @Override
        public ID[] newArray(int size) {
            return new ID[size];
        }
    };
}
