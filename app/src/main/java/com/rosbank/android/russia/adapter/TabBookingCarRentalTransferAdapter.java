package com.rosbank.android.russia.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.fragment.BookingRentalFragment;
import com.rosbank.android.russia.fragment.BookingTransferFragment;
import com.rosbank.android.russia.model.concierge.MyRequestObject;

/**
 * Created by anh.trinh on 9/15/2016.
 */
public class TabBookingCarRentalTransferAdapter
        extends FragmentPagerAdapter {
    private Context mContext;
    BaseFragment[] fragments = new BaseFragment[2];
    public TabBookingCarRentalTransferAdapter(Context context, FragmentManager fm, Bundle bundle) {
        super(fm);
        mContext = context;
        BookingRentalFragment bookingRentalFragment = new BookingRentalFragment();
        BookingTransferFragment bookingTransferFragment = new BookingTransferFragment();
        if (bundle != null) {
            MyRequestObject myRequestObject = bundle.getParcelable(AppConstant.PRE_REQUEST_OBJECT_DATA);

            if(myRequestObject!=null && myRequestObject.getBookingRequestType().equals(MyRequestObject.CAR_RENTAL_REQUEST)) {

                bookingRentalFragment.setArguments(bundle);
            }else{
                if(myRequestObject!=null && myRequestObject.getBookingRequestType().equals(MyRequestObject.CAR_TRANSFER_REQUEST)) {
                    bookingTransferFragment.setArguments(bundle);
                }
            }
        }
        fragments[0] = bookingRentalFragment;
        fragments[1] = bookingTransferFragment;
    }
    @Override
    public Fragment getItem(int position) {
        return fragments[position];
    }
    @Override
    public int getCount() {
        return fragments.length;
    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
    }

    public void makeDefaultDateTime(int position){
        if(position == 0){
            ((BookingRentalFragment)fragments[0]).makeDefaultDateTime();
        }else{
            ((BookingTransferFragment)fragments[1]).makeDefaultDateTime();
        }
    }

}
