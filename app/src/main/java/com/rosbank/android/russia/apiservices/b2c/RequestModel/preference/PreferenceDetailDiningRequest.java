package com.rosbank.android.russia.apiservices.b2c.RequestModel.preference;

import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.utils.StringUtil;
import com.google.gson.annotations.Expose;

import java.util.List;

/**
 * Created by Thu Nguyen on 12/12/2016.
 */

public class PreferenceDetailDiningRequest extends PreferenceDetailAddRequest{
    @Expose
    private String CuisinePreferences;
    @Expose
    private String OtherCuisine;
    @Expose
    private String Value2; // Force to map API parameter :(

    public PreferenceDetailDiningRequest(String Delete) {
        super(Delete, AppConstant.PREFERENCE_TYPE.DINING.getValue());
    }

    @Override
    public void fillData(Object... values) {
        List<String> valueList = StringUtil.removeAllSpecialCharactersAndFillInList(values);
        if(valueList != null){
            if(valueList.size() > 0){
                CuisinePreferences = valueList.get(0);
            }
            if(valueList.size() > 1){
                OtherCuisine = valueList.get(1);
            }
            if(valueList.size() > 2){
                Value2 = valueList.get(2);
            }
        }
    }
}
