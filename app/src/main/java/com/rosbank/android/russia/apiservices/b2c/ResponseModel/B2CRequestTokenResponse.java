package com.rosbank.android.russia.apiservices.b2c.ResponseModel;

import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.google.gson.annotations.Expose;


public class B2CRequestTokenResponse
        extends B2CBaseResponse {
    @Expose
    private String RequestToken;
    @Expose
    private String ConsumerKey;
    @Expose
    private String ExpirationTime;

    public String getRequestToken() {
        return RequestToken;
    }

    public void setRequestToken(String requestToken) {
        RequestToken = requestToken;
    }

    public String getConsumerKey() {
        return ConsumerKey;
    }

    public void setConsumerKey(String consumerKey) {
        ConsumerKey = consumerKey;
    }

    public String getExpirationTime() {
        return ExpirationTime;
    }

    public void setExpirationTime(String expirationTime) {
        ExpirationTime = expirationTime;
    }
}
