package com.rosbank.android.russia.model;

import com.rosbank.android.russia.application.AppConstant;

/**
 * Created by ThuNguyen on 12/19/2016.
 */

public class ConciergeRequestObject {
    private AppConstant.BOOKING_FILTER_TYPE bookingType;
    private String bookingName;
    private int imageRes;

    public ConciergeRequestObject(AppConstant.BOOKING_FILTER_TYPE bookingType, String bookingName, int imageRes){
        this.bookingType = bookingType;
        this.bookingName = bookingName;
        this.imageRes = imageRes;
    }

    public AppConstant.BOOKING_FILTER_TYPE getBookingType() {
        return bookingType;
    }

    public void setBookingType(AppConstant.BOOKING_FILTER_TYPE bookingType) {
        this.bookingType = bookingType;
    }

    public String getBookingName() {
        return bookingName;
    }

    public void setBookingName(String bookingName) {
        this.bookingName = bookingName;
    }

    public int getImageRes() {
        return imageRes;
    }

    public void setImageRes(int imageRes) {
        this.imageRes = imageRes;
    }
}
