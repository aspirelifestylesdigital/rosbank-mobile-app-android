package com.rosbank.android.russia.apiservices.RequestModel;


import com.google.gson.annotations.Expose;

public class BookGolfRequest
        extends BaseRequest {
    @Expose
    private String BookingId = "";
    @Expose
    private String UserID = "";
    @Expose
    private String UploadPhoto = "";
    @Expose
    private boolean Phone = false;
    @Expose
    private Boolean Email = false;
    @Expose
    private Boolean Both = false;
    @Expose
    private String MobileNumber = "";
    @Expose
    private String EmailAddress = "";
    @Expose
    private String GolfCourseName = "";
    @Expose
    private String Date = "";
    @Expose
    private String StartTime = "";
    @Expose
    private String NbOfHours = "";
    @Expose
    private String Country = "";
    @Expose
    private String City = "";
    @Expose
    private Integer NumberOfAdults = 0;
    @Expose
    private String SpecialRequirements = "";

    public Integer getNumberOfAdults() {
        return NumberOfAdults;
    }

    public String getGolfCourseName() {
        return GolfCourseName;
    }

    public String getDate() {
        return Date;
    }

    public String getStartTime() {
        return StartTime;
    }

    public String getNbOfHours() {
        return NbOfHours;
    }

    public String getCountry() {
        return Country;
    }

    public String getCity() {
        return City;
    }



    public void setNumberOfAdults(final Integer numberOfAdults) {
        NumberOfAdults = numberOfAdults;
    }

    public void setGolfCourseName(final String golfCourseName) {
        GolfCourseName = golfCourseName;
    }

    public void setDate(final String date) {
        Date = date;
    }

    public void setStartTime(final String startTime) {
        StartTime = startTime;
    }

    public void setNbOfHours(final String nbOfHours) {
        NbOfHours = nbOfHours;
    }

    public void setCountry(final String country) {
        Country = country;
    }

    public void setCity(final String city) {
        City = city;
    }
    public String getSpecialRequirements() {
        return SpecialRequirements;
    }

    public void setSpecialRequirements(final String specialRequirements) {
        SpecialRequirements = specialRequirements;
    }

    public String getBookingId() {
        return BookingId;
    }

    public void setBookingId(final String bookingId) {
        BookingId = bookingId;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(final String userID) {
        UserID = userID;
    }
    public String getUploadPhoto() {
        return UploadPhoto;
    }

    public void setUploadPhoto(final String uploadPhoto) {
        UploadPhoto = uploadPhoto;
    }

    public Boolean getPhone() {
        return Phone;
    }

    public void setPhone(final Boolean phone) {
        Phone = phone;
    }

    public Boolean getEmail() {
        return Email;
    }

    public void setEmail(final Boolean email) {
        Email = email;
    }

    public Boolean getBoth() {
        return Both;
    }

    public void setBoth(final Boolean both) {
        Both = both;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(final String mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public String getEmailAddress() {
        return EmailAddress;
    }

    public void setEmailAddress(final String emailAddress) {
        EmailAddress = emailAddress;
    }


}
