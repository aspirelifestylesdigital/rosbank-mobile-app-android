package com.rosbank.android.russia.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.fragment.MyRequestHistoryFragment;
import com.rosbank.android.russia.fragment.MyRequestUpComingFragment;

/**
 * Created by anh.trinh on 9/15/2016.
 */
public class TabMyRequestAdapter
        extends FragmentPagerAdapter {
    private Context mContext;
    BaseFragment[] fragments = new BaseFragment[2];
    public TabMyRequestAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
        fragments[0] = new MyRequestUpComingFragment();
        fragments[1] = new MyRequestHistoryFragment();
    }
    @Override
    public Fragment getItem(int position) {
        return fragments[position];
    }
    @Override
    public int getCount() {
        return fragments.length;
    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
    }



}
