package com.rosbank.android.russia.fragment;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.adapter.ExperiencesDetailAdapter;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.interfaces.OnRecyclerViewItemClickListener;
import com.rosbank.android.russia.model.ExperiencesDetailObject;
import com.rosbank.android.russia.model.Menu;
import com.rosbank.android.russia.model.Restaurant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;

/*
 * Created by chau.nguyen on 10/20/2016.
 */
public class ExperiencesDetailGourmetMenuFragment extends BaseFragment {

    @BindView(R.id.rcvDetailCommon)
    RecyclerView rcv;
    List<ExperiencesDetailObject> listExpDetailObj;

    private Restaurant restaurant;

    public ExperiencesDetailGourmetMenuFragment() {
        listExpDetailObj = new ArrayList<>();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int layoutId() {
        return R.layout.fragment_experiences_detail_gourmet_common;
    }

    @Override
    protected void initView() {
        if (restaurant != null) {

            if (restaurant.getMenu().size() > 0) {

                Map<String, List<Menu>> arrMap = new HashMap<>();
                for (Menu menu : restaurant.getMenu()) {
                    String groupName = menu.getGroupName();
                    if (arrMap.get(groupName) == null) {
                        arrMap.put(groupName, new ArrayList<Menu>());
                    }
                    arrMap.get(groupName).add(menu);
                }

                for ( String groupNameData : arrMap.keySet() ) {

                    List<Menu> list = arrMap.get(groupNameData);
                    //add Header have content
                    if(!groupNameData.isEmpty()){
                        ExperiencesDetailObject groupHeader = new ExperiencesDetailObject(groupNameData);
                        groupHeader.setRowShow(ExperiencesDetailAdapter.ROW_SHOW.MENU);
                        listExpDetailObj.add(groupHeader);
                    }

                    //add SubMain
                    for (Menu menu : list) {
                        ExperiencesDetailObject obj = new ExperiencesDetailObject(ExperiencesDetailAdapter.ROW_SHOW.MENU);
                        obj.setTypeRowView(ExperiencesDetailAdapter.ITEM_VIEW_COMMON);
                        obj.setMenu(menu);
                        listExpDetailObj.add(obj);
                    }
                }

            }

            ExperiencesDetailAdapter adapter = new ExperiencesDetailAdapter(listExpDetailObj, new OnRecyclerViewItemClickListener() {
                @Override
                public void onItemClick(int position, View view) {
                    /*if (view.getTag() instanceof ExperiencesDetailObject) {

                        ExperiencesDetailObject obj = (ExperiencesDetailObject) view.getTag();

                    }*/
                }
            });

            LinearLayoutManager layoutManager = new LinearLayoutManager(view.getContext());
            layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            rcv.setLayoutManager(layoutManager);
            rcv.setAdapter(adapter);
        }
    }

    @Override
    protected void bindData() {

    }

    @Override
    protected boolean onBack() {
        return false;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }
}