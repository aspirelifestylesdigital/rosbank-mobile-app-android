package com.rosbank.android.russia.apiservices.RequestModel;

public class AuthenticateRequest {
    public String mUserID;
    public String mPassword;

    public AuthenticateRequest(String userID, String password){
        this.mUserID = userID;
        this.mPassword = password;
    }
}
