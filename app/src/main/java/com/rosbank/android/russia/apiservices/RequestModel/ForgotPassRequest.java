package com.rosbank.android.russia.apiservices.RequestModel;


import com.google.gson.annotations.Expose;

public class ForgotPassRequest
        extends BaseRequest {
    @Expose
    private String Email;

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

}
