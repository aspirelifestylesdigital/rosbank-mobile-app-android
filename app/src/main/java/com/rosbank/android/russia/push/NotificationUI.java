package com.rosbank.android.russia.push;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.liveperson.infra.model.PushMessage;
import com.liveperson.messaging.sdk.api.LivePerson;
import com.rosbank.android.russia.R;
import com.rosbank.android.russia.liveperson.ChatActivity;

import java.util.List;

import static android.app.NotificationManager.IMPORTANCE_HIGH;

/**
 * ***** Sample app class - Not related to Messaging SDK *****
 * <p>
 * Used as an example of how to create push notification in terms of UI.
 * As best practise each host app needs to handle the push notifications UI implementation.
 */
public class NotificationUI {

    private static final String TAG = NotificationUI.class.getSimpleName();
    public static final int NOTIFICATION_ID = 143434567;
    public static final String PUSH_NOTIFICATION = "push_notification";


    @SuppressLint("RestrictedApi")
    public static void showNotification(Context ctx, PushMessage pushMessage) {

        NotificationCompat.Builder builder;
        NotificationChannel notificationChannel;

        // If we're running on Android O we create a notification channel
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            builder = new NotificationCompat.Builder(ctx);
            builder.setContentTitle(ctx.getString(R.string.app_name));
        } else {
            notificationChannel = new NotificationChannel(PUSH_NOTIFICATION, "Push Notification", NotificationManager.IMPORTANCE_DEFAULT);
            notificationChannel.setImportance(IMPORTANCE_HIGH);
            getNotificationManager(ctx).createNotificationChannel(notificationChannel);
            builder = new NotificationCompat.Builder(ctx, PUSH_NOTIFICATION);

        }


        builder.setContentIntent(getPendingIntent(ctx)).
                setContentText(pushMessage.getMessage()).
                setAutoCancel(true).
                setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS).
                setSmallIcon(R.drawable.ic_notification_icon).
                setColor(ContextCompat.getColor(ctx, R.color.app_color_light_bg)).
                //setLargeIcon(drawableToBitmap(ContextCompat.getDrawable(ctx, R.mipmap.ic_rosbank))).
                setNumber(pushMessage.getCurrentUnreadMessgesCounter());
                /*.setStyle(new Notification.InboxStyle()
                        .addLine(pushMessage.getFrom())
                        .addLine(pushMessage.getBrandId())
                        .addLine(pushMessage.getConversationId())
                        .addLine(pushMessage.getBackendService())
                        .addLine(pushMessage.getCollapseKey())
                        .addLine("Unread messages : " + LivePerson.
                                getNumUnreadMessages(pushMessage.getBrandId()))

                );*/

        if (Build.VERSION.SDK_INT >= 21) {
            builder = builder.
                    setCategory(Notification.CATEGORY_MESSAGE).
                    setPriority(Notification.PRIORITY_HIGH);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int smallIconViewId = ctx.getResources().getIdentifier("right_icon", "id", android.R.class.getPackage().getName());

            if (smallIconViewId != 0) {
                if (builder.getContentView() != null)
                    builder.getContentView().setViewVisibility(smallIconViewId, View.INVISIBLE);

                if (builder.getHeadsUpContentView() != null)
                    builder.getHeadsUpContentView().setViewVisibility(smallIconViewId, View.INVISIBLE);

                if (builder.getBigContentView() != null)
                    builder.getBigContentView().setViewVisibility(smallIconViewId, View.INVISIBLE);
            }
        }

        getNotificationManager(ctx).notify(NOTIFICATION_ID, builder.build());
    }

    public static void hideNotification(Context ctx) {
        getNotificationManager(ctx).cancel(NOTIFICATION_ID);

    }

    public static Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if (bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    private static NotificationManager getNotificationManager(Context ctx) {
        return (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
    }


    private static PendingIntent getPendingIntent(Context ctx) {
        // there was chatmessagnging here changed by nishant
        Intent showIntent = new Intent(ctx, ChatActivity.class);
        showIntent.putExtra(PUSH_NOTIFICATION, true);

        return PendingIntent.getActivity(ctx, 0, showIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    }


    /************************ Example of app Icon Badge - For Samsung *******************************/

    public static void setBadge(Context context, int count) {
        String launcherClassName = getLauncherClassName(context);
        if (launcherClassName == null) {
            return;
        }
        Intent intent = new Intent("android.intent.action.BADGE_COUNT_UPDATE");
        intent.putExtra("badge_count", count);
        intent.putExtra("badge_count_package_name", context.getPackageName());
        intent.putExtra("badge_count_class_name", launcherClassName);
        context.sendBroadcast(intent);
    }

    public static String getLauncherClassName(Context context) {

        PackageManager pm = context.getPackageManager();

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);

        List<ResolveInfo> resolveInfos = pm.queryIntentActivities(intent, 0);
        for (ResolveInfo resolveInfo : resolveInfos) {
            String pkgName = resolveInfo.activityInfo.applicationInfo.packageName;
            if (pkgName.equalsIgnoreCase(context.getPackageName())) {
                return resolveInfo.activityInfo.name;
            }
        }
        return null;
    }


    /**
     * Listen to changes in unread messages counter and updating app icon badge
     */
    public static class BadgeBroadcastReceiver extends BroadcastReceiver {

        public BadgeBroadcastReceiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            int unreadCounter = intent.getIntExtra(LivePerson.ACTION_LP_UPDATE_NUM_UNREAD_MESSAGES_EXTRA, 0);
            NotificationUI.setBadge(context, unreadCounter);
        }
    }
}
