package com.rosbank.android.russia.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.rosbank.android.russia.fragment.ExperiencesDetailGourmetGalleryFragment;
import com.rosbank.android.russia.fragment.ExperiencesDetailGourmetInfoFragment;
import com.rosbank.android.russia.fragment.ExperiencesDetailGourmetLocationFragment;
import com.rosbank.android.russia.fragment.ExperiencesDetailGourmetMenuFragment;
import com.rosbank.android.russia.fragment.ExperiencesDetailGourmetPhoneFragment;
import com.rosbank.android.russia.model.Restaurant;

import java.util.ArrayList;
import java.util.List;

/*
 * Created by chau.nguyen on 10/25/2016.
 */

public class ExperiencesDetailGourmetPagerAdapter extends FragmentPagerAdapter {
    private final List<Fragment> mFragmentList = new ArrayList<>();

    public ExperiencesDetailGourmetPagerAdapter(FragmentManager manager) {
        super(manager);
    }

    public void addPageInfo(Restaurant restaurant){
        ExperiencesDetailGourmetInfoFragment tabInfo = new ExperiencesDetailGourmetInfoFragment();
        tabInfo.setRestaurant(restaurant);
        mFragmentList.add(tabInfo);
    }

    public void addPageLocation(Restaurant restaurant){
        ExperiencesDetailGourmetLocationFragment tabLocation = new ExperiencesDetailGourmetLocationFragment();
        tabLocation.setRestaurant(restaurant);
        mFragmentList.add(tabLocation);
    }

    public void addPageContact(Restaurant restaurant){
        ExperiencesDetailGourmetPhoneFragment tabContact = new ExperiencesDetailGourmetPhoneFragment();
        tabContact.setRestaurant(restaurant);
        mFragmentList.add(tabContact);
    }

    public void addPageMenu(Restaurant restaurant){
        ExperiencesDetailGourmetMenuFragment tabMenu = new ExperiencesDetailGourmetMenuFragment();
        tabMenu.setRestaurant(restaurant);
        mFragmentList.add(tabMenu);
    }

    public void addPagePicture(Restaurant restaurant, int heightTabLayout){
        ExperiencesDetailGourmetGalleryFragment tabPicture = new ExperiencesDetailGourmetGalleryFragment();
        tabPicture.setHeightTabLayout(heightTabLayout);
        tabPicture.setRestaurant(restaurant);
        mFragmentList.add(tabPicture);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return null;
    }
}
