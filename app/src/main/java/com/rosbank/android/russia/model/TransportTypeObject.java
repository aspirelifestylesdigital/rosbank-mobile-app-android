package com.rosbank.android.russia.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;


public class TransportTypeObject
        implements Parcelable {
    @Expose
    private String Key;

    public String getKey() {
        return Key;
    }

    public void setKey(final String key) {
        Key = key;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(final String value) {
        Value = value;
    }

    @Expose
    private String Value;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel parcel,
                              final int i) {
        parcel.writeString(this.Key);
        parcel.writeString(this.Value);

    }
    protected TransportTypeObject(Parcel in) {
        this.Key = in.readString();
        this.Value = in.readString();
    }
    public static final Creator<TransportTypeObject> CREATOR = new Creator<TransportTypeObject>() {
        @Override
        public TransportTypeObject createFromParcel(Parcel source) {
            return new TransportTypeObject(source);
        }

        @Override
        public TransportTypeObject[] newArray(int size) {
            return new TransportTypeObject[size];
        }
    };
}
