package com.rosbank.android.russia.model;


import com.rosbank.android.russia.adapter.ExperiencesDetailAdapter;

/*
 * Created by chau.nguyen on 10/26/2016.
 */
public class ExperiencesDetailObject {

    private int typeRowView = ExperiencesDetailAdapter.ITEM_VIEW_NORMAL;
    private ExperiencesDetailAdapter.ROW_SHOW rowShow;

    private String content = "";
    private String content2 = "";
    private Branches branches;
    private Menu menu;


    public ExperiencesDetailObject(String content, ExperiencesDetailAdapter.ROW_SHOW rowShow) {
        this.content = content;
        this.rowShow = rowShow;
    }

    /**
     * add header
    */
    public ExperiencesDetailObject(String content) {
        this.content = content;
        typeRowView = ExperiencesDetailAdapter.ITEM_VIEW_HEADER;
    }

    public ExperiencesDetailObject(ExperiencesDetailAdapter.ROW_SHOW rowShow) {
        this.rowShow = rowShow;
    }

    public String getContent2() {
        return content2;
    }

    public void setContent2(String content2) {
        this.content2 = content2;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public ExperiencesDetailAdapter.ROW_SHOW getRowShow() {
        return rowShow;
    }

    public void setRowShow(ExperiencesDetailAdapter.ROW_SHOW rowShow) {
        this.rowShow = rowShow;
    }

    public int getTypeRowView() {
        return typeRowView;
    }

    public void setTypeRowView(int typeRowView) {
        this.typeRowView = typeRowView;
    }

    public Branches getBranches() {
        return branches;
    }

    public void setBranches(Branches branches) {
        this.branches = branches;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }
}
