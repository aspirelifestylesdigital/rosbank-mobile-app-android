package com.rosbank.android.russia.apiservices.ResponseModel;

import com.rosbank.android.russia.apiservices.apis.appimplement.BaseResponse;
import com.rosbank.android.russia.model.concierge.GolfBookingDetailData;

/**
 * Created by ThuNguyen on 10/26/2016.
 */

public class GolfBookingDetailResponse extends BaseResponse{
    GolfBookingDetailData Data;

    public GolfBookingDetailData getData() {
        return Data;
    }

    public void setData(GolfBookingDetailData data) {
        Data = data;
    }
}
