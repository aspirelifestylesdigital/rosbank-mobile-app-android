package com.rosbank.android.russia.fragment;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.activitys.HomeActivity;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.FontUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by anh.trinh on 9/13/2016.
 */
public class ErrorHomeFragment
        extends BaseFragment {

    public static final String PRE_ERROR_MESSAGE = "error_message";

    @BindView(R.id.tv_error_message)
    TextView mTvErrorMessage;
    @BindView(R.id.btn_ok)
    Button mBtnOk;

    @Override
    protected int layoutId() {
        return R.layout.fragment_error_home;
    }

    @Override
    protected void initView() {

        CommonUtils.setFontForViewRecursive(mTvErrorMessage,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);

        CommonUtils.setFontForViewRecursive(mBtnOk,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        Bundle bundle = getArguments();
        if(bundle!=null) {
            String errorMessage = getArguments().getString(PRE_ERROR_MESSAGE,
                                                           "");

            mTvErrorMessage.setText(errorMessage);
        }

    }

    @Override
    protected void bindData() {


    }

    @Override
    public void onResume() {

        super.onResume();
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showLogoApp(false);
            ((HomeActivity) getActivity()).hideToolbarMenuIcon();
            ((HomeActivity) getActivity()).setTitle(getString(R.string.text_title_error));


        }
    }

    @Override
    public boolean onBack() {
        return false;
    }


    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater,
                                           container,
                                           savedInstanceState);
        ButterKnife.bind(this,
                         rootView);
        return rootView;
    }

    @OnClick({
              R.id.btn_ok})
    public void onClick(View view) {
        switch (view.getId()) {
               case R.id.btn_ok:
                onBackPress();
                break;
        }
    }
}
