package com.rosbank.android.russia.apiservices.b2c;

import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CApiProviderClient;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CICallback;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.B2CRegistrationRequest;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CRegistrationResponse;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.SharedPreferencesUtils;

import java.util.List;
import java.util.Map;
import retrofit2.Callback;

public class B2CWSRegistration
        extends B2CApiProviderClient<B2CRegistrationResponse>{

    B2CRegistrationRequest registrationRequest;

    public B2CWSRegistration(B2CICallback callback){
        this.b2CICallback = callback;
        isCallbackInstantly = false;
    }
    @Override
    public void run(Callback<B2CRegistrationResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }
    public void setRegistrationRequest(B2CRegistrationRequest registrationRequest){
        this.registrationRequest = registrationRequest;
    }
    @Override
    protected boolean isUseAuthenticateInApi() {
        return false;
    }

    @Override
    protected void runApi() {
        serviceInterface.registration(registrationRequest).enqueue(this);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        return null;
    }

    @Override
    protected void postResponse(B2CRegistrationResponse response) {
// Save online member id here
        SharedPreferencesUtils.setPreferences(AppConstant.PRE_B2C_ONLINE_MEMBER_ID, response.getOnlineMemberID());

        // Begin authentication
        B2CWSAuthenticate wsAuthenticate = new B2CWSAuthenticate();
        wsAuthenticate.run(new B2CICallback() {
            @Override
            public void onB2CResponse(B2CBaseResponse response) {
                if (b2CICallback != null) {
                    if(response != null) {
                        if(response.isSuccess()) {
                            // Get user detail
                            B2CWSGetUserDetails b2CWSGetUserDetails = new B2CWSGetUserDetails(new B2CICallback() {
                                @Override
                                public void onB2CResponse(B2CBaseResponse response) {
                                    if (response != null && response.isSuccess()) {
                                        b2CICallback.onB2CResponse(callbackResponse);
                                    } else {
                                        b2CICallback.onB2CResponse(null);
                                    }
                                }

                                @Override
                                public void onB2CResponseOnList(List<B2CBaseResponse> responseList) {

                                }

                                @Override
                                public void onB2CFailure(String errorMessage, String errorCode) {

                                }
                            });
                            b2CWSGetUserDetails.run(null);
                        }else{
                            // Reset all manage user preference
                            CommonUtils.resetManageUserPreference();
                            b2CICallback.onB2CFailure(getMessageErrorFromLog(), getErrorCodeFromLog());
                        }
                    }else{
                        // Reset all manage user preference
                        CommonUtils.resetManageUserPreference();
                        b2CICallback.onB2CFailure(getMessageErrorFromLog(), getErrorCodeFromLog());
                    }
                }
            }

            @Override
            public void onB2CResponseOnList(List<B2CBaseResponse> responseList) {

            }

            @Override
            public void onB2CFailure(String errorMessage, String errorCode) {
                b2CICallback.onB2CFailure(errorMessage, errorCode);
            }
        });
    }
}
