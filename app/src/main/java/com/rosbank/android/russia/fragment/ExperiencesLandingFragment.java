package com.rosbank.android.russia.fragment;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.activitys.HomeActivity;
import com.rosbank.android.russia.adapter.CommonLandingAdapter;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.dcr.PrivilegesFragment;
import com.rosbank.android.russia.dcr.model.DCRPrivilegeObject;
import com.rosbank.android.russia.interfaces.OnRecyclerViewItemClickListener;
import com.rosbank.android.russia.model.CommonLandingObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/*
 * Created by chau.nguyen on 10/05/2016.
 */
public class ExperiencesLandingFragment extends BaseFragment {

    @BindView(R.id.rcvCommonLanding)
    RecyclerView rcvResult;

    public ExperiencesLandingFragment() {

    }

    @Override
    protected int layoutId() {
        return R.layout.fragment_common_landing;
    }

    @Override
    protected void initView() {
        CommonLandingAdapter adapter = new CommonLandingAdapter(createDataConcierge(), new OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(int position, View view) {
                //after click Concierge Service Item
                if (view.getTag() instanceof CommonLandingObject) {
                    switch (((CommonLandingObject) view.getTag()).getImgId()) {
                        case R.drawable.img_experiences_item_gourmet:
                            openPrivilegesFragment(DCRPrivilegeObject.PRIVILEGE_TYPE_RESTAURANT);
                            /*openExpSearchFragment(
                                    getString(R.string.experiences_item_gourmet),
                                    ExperienceSearchFragment.ApiRequest.GOURMET
                            );*/
                            break;
                        case R.drawable.img_experiences_item_entertainment:
                           /* openExpSearchFragment(
                                    getString(R.string.experiences_item_entertainment),
                                    ExperienceSearchFragment.ApiRequest.ENTERTAINMENT
                            );*/

                            break;
                        case R.drawable.img_experiences_item_spa:
                           /* openExpSearchFragment(
                                    getString(R.string.experiences_item_lifestyle),
                                    ExperienceSearchFragment.ApiRequest.LIFESTYLE
                            );*/
                           openPrivilegesFragment(DCRPrivilegeObject.PRIVILEGE_TYPE_SPA);
                            break;
                        case R.drawable.img_concierge_service_hotel:
                           /* openExpSearchFragment(
                                    getString(R.string.experiences_item_travel),
                                    ExperienceSearchFragment.ApiRequest.TRAVEL
                            );*/
                            openPrivilegesFragment(DCRPrivilegeObject.PRIVILEGE_TYPE_HOTEL);
                            break;
                        default:
                            break;
                    }
                }
            }
        });
        LinearLayoutManager layoutManager = new LinearLayoutManager(view.getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rcvResult.setLayoutManager(layoutManager);
        rcvResult.setAdapter(adapter);
    }

    private List<CommonLandingObject> createDataConcierge() {
        List<CommonLandingObject> data = new ArrayList<>();
        data.add(new CommonLandingObject(getString(R.string.privileges_title_dinning_item), R.drawable.img_experiences_item_gourmet));
        data.add(new CommonLandingObject(getString(R.string.privileges_title_hotel_item), R.drawable.img_concierge_service_hotel));
        data.add(new CommonLandingObject(getString(R.string.privileges_title_spa_item), R.drawable.img_experiences_item_spa));
//        data.add(new CommonLandingObject(getString(R.string.experiences_item_gourmet), R.drawable.img_experiences_item_gourmet));
//        data.add(new CommonLandingObject(getString(R.string.experiences_item_entertainment), R.drawable.img_experiences_item_entertainment));
//        data.add(new CommonLandingObject(getString(R.string.experiences_item_lifestyle), R.drawable.img_experiences_item_lifestyle));
//        data.add(new CommonLandingObject(getString(R.string.experiences_item_travel), R.drawable.img_experiences_item_travel));
        return data;
    }

    @Override
    protected void bindData() {

    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showLogoApp(false);
            ((HomeActivity) getActivity()).setTitle(getString(R.string.experiences_title));
        }
    }

    @Override
    public boolean onBack() {
        return false;
    }

    /*private void openExpSearchFragment(String title, ExperienceSearchFragment.ApiRequest apiRequest){
        Bundle bundle = new Bundle();
        bundle.putString(ExperienceSearchFragment.TitleSearch,title);

        ExperienceSearchFragment fragment = new ExperienceSearchFragment();
        fragment.setApiRequest(apiRequest);
        fragment.setArguments(bundle);

        pushFragment(fragment, true, true);
    }*/
    private void openPrivilegesFragment(String privilegeType){
        Bundle bundle = new Bundle();
        bundle.putString(PrivilegesFragment.PRIVILEGE_TYPE, privilegeType);

        PrivilegesFragment fragment = new PrivilegesFragment();
        fragment.setArguments(bundle);

        pushFragment(fragment, true, true);
    }
}
