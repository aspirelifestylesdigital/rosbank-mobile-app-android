package com.rosbank.android.russia.adapter;

import android.content.Context;
import android.os.Parcelable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.model.CityObject;
import com.rosbank.android.russia.model.CountryObject;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.FontUtils;

import java.util.ArrayList;

/**
 * Created by anh.trinh on 9/20/2016.
 */
public class SelectionCountryOrCityAdapter
        extends ArrayAdapter<Parcelable> implements Filterable {
    private final Context context;
    private final ArrayList<Parcelable> values;
    private ArrayList<Parcelable> filterValueList;
    private CountryObject selectedCountry;
    private CityObject selectedCity;
    private boolean isCountry;
    private CustomFilter mFilter;

    public SelectionCountryOrCityAdapter(Context context, ArrayList<Parcelable> values, ArrayList<Parcelable> filterValues, Parcelable selectedData) {
        super(context, -1, values);
        this.context = context;
        this.values = values;
        this.mFilter = new CustomFilter(this);
        if(values!=null && values.size()>0){
            if(values.get(0) instanceof  CountryObject){
                isCountry = true;
            }
            else {
                isCountry = false;
            }
        }

        if(selectedData instanceof CountryObject){
            selectedCountry = (CountryObject) selectedData;
        }
        else if(selectedData instanceof  CityObject){
            selectedCity = (CityObject) selectedData;
        }
        filterValueList = filterValues;
        filterValueList.addAll(this.values);
    }
    public void filter(String text){
        filterValueList.clear();
        if(TextUtils.isEmpty(text) || TextUtils.isEmpty(text.trim())){
            filterValueList.addAll(values);
        }else {
            for (Parcelable parcelable : values) {
                if (parcelable.toString().toLowerCase().startsWith(text.toLowerCase().trim())) {
                    filterValueList.add(parcelable);
                }
            }
        }
        notifyDataSetChanged();
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.item_selection, parent, false);
        TextView textView = (TextView) rowView.findViewById(R.id.tv_selection_name);
        ImageView imgTick = (ImageView) rowView.findViewById(R.id.ic_selection_tick);

        imgTick.setVisibility(View.GONE);

        CommonUtils.setFontForViewRecursive(textView,
                FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);

        if (isCountry) {
            CountryObject countryObject = (CountryObject)filterValueList.get(position);
            textView.setText(countryObject.getCountryName());
            if(selectedCountry != null) {
                if (selectedCountry.getCountryName().equalsIgnoreCase(countryObject.getCountryName()))
                    imgTick.setVisibility(View.VISIBLE);
            }
        }
        else {
            CityObject cityObject = (CityObject) filterValueList.get(position);
            textView.setText(cityObject.getCityName());
            if (selectedCity != null) {
                if (selectedCity.getCityName().equalsIgnoreCase(cityObject.getCityName()))
                    imgTick.setVisibility(View.VISIBLE);
            }
        }
        return rowView;
    }

    @Override
    public int getCount() {
        return filterValueList.size();
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    private class CustomFilter extends Filter {
        private SelectionCountryOrCityAdapter mAdapter;

        private CustomFilter(SelectionCountryOrCityAdapter mAdapter) {
            super();
            this.mAdapter = mAdapter;
        }
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            filterValueList.clear();
            final FilterResults results = new FilterResults();
            if (constraint.length() == 0) {
                filterValueList.addAll(values);
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();
                for (final Parcelable obj : values) {
                    if (obj.toString().toLowerCase().startsWith(filterPattern)) {
                        filterValueList.add(obj);
                    }
                }
            }
            results.values = filterValueList;
            results.count = filterValueList.size();
            return results;
        }
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mAdapter.notifyDataSetChanged();
        }
    }
}

