package com.rosbank.android.russia.apiservices.b2c;

import com.rosbank.android.russia.BuildConfig;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CApiProviderClient;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CICallback;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CAccessTokenResponse;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.model.UserItem;
import com.rosbank.android.russia.utils.SharedPreferencesUtils;

import java.util.Map;

import retrofit2.Callback;



public class B2CWSAccessToken
        extends B2CApiProviderClient<B2CAccessTokenResponse> {

    public B2CWSAccessToken(B2CICallback callback) {
        this.b2CICallback = callback;
    }

    @Override
    public void run(Callback<B2CAccessTokenResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }

    @Override
    protected boolean isUseAuthenticateInApi() {
        return false;
    }

    @Override
    protected void runApi() {
        serviceInterface.accessToken(UserItem.isVip() ? BuildConfig.B2C_CONSUMER_KEY_PRIVATE : BuildConfig.B2C_CONSUMER_KEY_REGULAR /*BuildConfig.B2C_CONSUMER_KEY*/,
                UserItem.isVip() ? BuildConfig.B2C_CALL_BACK_URL_RPB : BuildConfig.B2C_CALL_BACK_URL_RBA  /*BuildConfig.B2C_CALL_BACK_URL*/,
                SharedPreferencesUtils.getPreferences(AppConstant.PRE_B2C_REQUEST_TOKEN, ""),
                SharedPreferencesUtils.getPreferences(AppConstant.PRE_B2C_ONLINE_MEMBER_ID, "")).enqueue(this);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        return null;
    }

    @Override
    protected void postResponse(B2CAccessTokenResponse response) {
        SharedPreferencesUtils.setPreferences(AppConstant.PRE_B2C_ACCESS_TOKEN, response.getAccessToken());
        SharedPreferencesUtils.setPreferences(AppConstant.PRE_B2C_REFRESH_TOKEN, response.getRefreshToken());
        long expirationTimeInMiliSecond = Long.parseLong(response.getExpirationTime()) * 1000;
        SharedPreferencesUtils.setPreferences(AppConstant.PRE_B2C_EXPIRATION_TIME_DURATION, expirationTimeInMiliSecond);
        SharedPreferencesUtils.setPreferences(AppConstant.PRE_B2C_EXPIRED_AT, System.currentTimeMillis() + expirationTimeInMiliSecond);
    }
}
