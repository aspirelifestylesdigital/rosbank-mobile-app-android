package com.rosbank.android.russia.fragment;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;

/**
 * Created by anh.trinh on 9/13/2016.
 */
public class SplashThreeFragment
        extends BaseFragment {

    @Override
    protected int layoutId() {
        return R.layout.layout_splash_three;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void bindData() {
       /* if(getActivity() instanceof SplashAcitivy){
            ((SplashAcitivy)getActivity()).setInfo(getResources().getString(R.string.text_title_splash_three),getResources().getString(R.string.text_description_splash_three));
        }*/

    }

    @Override
    public boolean onBack() {
        return false;
    }


}
