package com.rosbank.android.russia.apiservices;

import com.rosbank.android.russia.apiservices.RequestModel.UpdateMyProfileRequest;
import com.rosbank.android.russia.apiservices.apis.appimplement.ApiProviderClient;
import com.rosbank.android.russia.apiservices.apis.appimplement.BaseResponse;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Callback;


public class WSUpdateMyProfile
        extends ApiProviderClient<BaseResponse> {

    private UpdateMyProfileRequest updateMyProfileRequest;

    public WSUpdateMyProfile(){

    }
    public void updateMyProfile(final UpdateMyProfileRequest request){
        this.updateMyProfileRequest = request;


    }




    @Override
    public void run(Callback<BaseResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }

    @Override
    protected boolean isUseAuthenticateInApi() {
        return true;
    }

    @Override
    public void runApi() {

        serviceInterface.updateMyProfile(updateMyProfileRequest).enqueue(callback);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("User-Agent", "Retrofit-Sample-App");
        return headers;
    }

}
