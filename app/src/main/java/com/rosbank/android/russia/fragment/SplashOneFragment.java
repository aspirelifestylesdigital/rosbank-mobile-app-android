package com.rosbank.android.russia.fragment;

import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;
import android.widget.VideoView;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;

import butterknife.BindView;

/**
 * Created by anh.trinh on 9/13/2016.
 */
public class SplashOneFragment
        extends BaseFragment {

    int pausePosition;

    @BindView(R.id.video)
    VideoView mVideo;

    @Override
    protected int layoutId() {
        return R.layout.layout_splash_one;
    }

    @Override
    protected void initView() {

        //  mVideo.requestFocus();


    }

    @Override
    protected void bindData() {
        if(mVideo!=null) {
            mVideo.setVideoURI(Uri.parse("android.resource://" + getActivity().getPackageName() + "/" + R.raw.frying_1));
            mVideo.setMediaController(null);
            mVideo.start();

            mVideo.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    Log.d("ThuNguyen", "Playback finished");
                    mVideo.start();
                }
            });
            mVideo.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    Log.d("splash", "error"+what);
                    return false;
                }
            });
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        pausePosition = mVideo.getCurrentPosition(); //stopPosition is an int
        if (mVideo.isPlaying())
            mVideo.pause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mVideo != null) {
            mVideo.seekTo(pausePosition);
            mVideo.start();
        }
    }

    @Override
    public boolean onBack() {
        return false;
    }


}
