package com.rosbank.android.russia.apiservices.ResponseModel;

import com.rosbank.android.russia.apiservices.apis.appimplement.BaseResponse;
import com.rosbank.android.russia.model.ExperienceObject;
import com.google.gson.annotations.Expose;

import java.util.List;


public class GetExperienceLifeStyleResponse extends BaseResponse {

    @Expose
    private List<ExperienceObject> Data;

    public List<ExperienceObject> getData() {
        return Data;
    }

    public void setData(List<ExperienceObject> data) {
        this.Data = data;
    }
}
