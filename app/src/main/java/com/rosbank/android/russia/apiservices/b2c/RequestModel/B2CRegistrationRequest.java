package com.rosbank.android.russia.apiservices.b2c.RequestModel;

import com.google.gson.annotations.Expose;
import com.rosbank.android.russia.BuildConfig;
import com.rosbank.android.russia.apiservices.RequestModel.BaseRequest;
import com.rosbank.android.russia.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;

import static com.rosbank.android.russia.application.AppConstant.CLIENT_CODE_REGULAR;

/**
 * Created by ThuNguyen on 10/26/2016.
 */

public class B2CRegistrationRequest extends BaseRequest {
    @Expose
    private MemberExt Member;
    @Expose
    private List<MemberDetailExt> MemberDetails;

    public B2CRegistrationRequest build(String email, String firstName, String lastName,
                                        String mobileNumber, String password, String salutation, String zipCode, String clientCode) {
        salutation = CommonUtils.convertSalutationToEnglish(salutation);
        setMember(new MemberExt(clientCode).build(email, firstName, lastName, mobileNumber, password, salutation, zipCode));
        MemberDetails = new ArrayList<>();
        MemberDetails.add(new MemberDetailExt(clientCode));
        setMemberDetails(MemberDetails);
        return this;
    }

    private class MemberExt {
        @Expose
        private String ConsumerKey;
        @Expose
        private String Email;
        @Expose
        private String FirstName;
        @Expose
        private String Functionality;
        @Expose
        private String LastName;
        @Expose
        private String MemberDeviceId;
        @Expose
        private String MobileNumber;
        @Expose
        private String Password;
        @Expose
        private String Salutation;
        @Expose
        private String PostalCode;

        public MemberExt(String clientCode) {
            ConsumerKey = clientCode.equals(CLIENT_CODE_REGULAR) ? BuildConfig.B2C_CONSUMER_KEY_REGULAR : BuildConfig.B2C_CONSUMER_KEY_PRIVATE;
            //ConsumerKey = BuildConfig.B2C_CONSUMER_KEY;
            Functionality = "Registration";
            MemberDeviceId = BuildConfig.B2C_MEMBER_DEVICE_ID;
        }

        public MemberExt build(String email, String firstName, String lastName, String mobileNumber, String password, String salutation, String zipCode) {
            this.Email = email;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.MobileNumber = mobileNumber;
            this.Password = password;
            this.Salutation = salutation;
            this.PostalCode = zipCode;
            return this;
        }

        public String getConsumerKey() {
            return ConsumerKey;
        }

        public void setConsumerKey(String consumerKey) {
            ConsumerKey = consumerKey;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String email) {
            Email = email;
        }

        public String getFirstName() {
            return FirstName;
        }

        public void setFirstName(String firstName) {
            FirstName = firstName;
        }

        public String getFunctionality() {
            return Functionality;
        }

        public void setFunctionality(String functionality) {
            Functionality = functionality;
        }

        public String getLastName() {
            return LastName;
        }

        public void setLastName(String lastName) {
            LastName = lastName;
        }

        public String getMemberDeviceId() {
            return MemberDeviceId;
        }

        public void setMemberDeviceId(String memberDeviceId) {
            MemberDeviceId = memberDeviceId;
        }

        public String getMobileNumber() {
            return MobileNumber;
        }

        public void setMobileNumber(String mobileNumber) {
            MobileNumber = mobileNumber;
        }

        public String getPassword() {
            return Password;
        }

        public void setPassword(String password) {
            Password = password;
        }

        public String getSalutation() {
            return Salutation;
        }

        public void setSalutation(String salutation) {
            Salutation = salutation;
        }

    }

    private class MemberDetailExt {
        @Expose
        private String VerificationCode;

        public MemberDetailExt(String clientCode) {
            VerificationCode = clientCode.equals(CLIENT_CODE_REGULAR) ? BuildConfig.B2C_VERIFICATION_CODE_REGULAR : BuildConfig.B2C_VERIFICATION_CODE_PRIVATE;
        }

        public String getVeriCode() {
            return VerificationCode;
        }

        public void setVeriCode(String veriCode) {
            VerificationCode = veriCode;
        }
    }

    public MemberExt getMember() {
        return Member;
    }

    public void setMember(MemberExt member) {
        Member = member;
    }

    public List<MemberDetailExt> getMemberDetails() {
        return MemberDetails;
    }

    public void setMemberDetails(List<MemberDetailExt> memberDetails) {
        MemberDetails = memberDetails;
    }
}
