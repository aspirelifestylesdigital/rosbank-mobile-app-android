package com.rosbank.android.russia.fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.rosbank.android.russia.R;
import com.rosbank.android.russia.activitys.HomeActivity;
import com.rosbank.android.russia.apiservices.ResponseModel.RestaurantBookingDetailResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.rosbank.android.russia.apiservices.apis.appimplement.b2c.B2CICallback;
import com.rosbank.android.russia.apiservices.b2c.B2CWSGetPreference;
import com.rosbank.android.russia.apiservices.b2c.B2CWSGetRequestDetail;
import com.rosbank.android.russia.apiservices.b2c.B2CWSUpsertConciergeRequest;
import com.rosbank.android.russia.apiservices.b2c.RequestModel.B2CUpsertConciergeRequestRequest;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CGetRecentRequestResponse;
import com.rosbank.android.russia.apiservices.b2c.ResponseModel.B2CUpsertConciergeRequestResponse;
import com.rosbank.android.russia.application.AppConstant;
import com.rosbank.android.russia.application.AppContext;
import com.rosbank.android.russia.application.coreactivitys.BaseFragment;
import com.rosbank.android.russia.model.KeyValueObject;
import com.rosbank.android.russia.model.UserItem;
import com.rosbank.android.russia.model.concierge.MyRequestObject;
import com.rosbank.android.russia.model.concierge.RestaurantBookingDetailData;
import com.rosbank.android.russia.model.preference.DiningPreferenceDetailData;
import com.rosbank.android.russia.model.preference.PreferenceData;
import com.rosbank.android.russia.utils.CommonUtils;
import com.rosbank.android.russia.utils.DateTimeUtil;
import com.rosbank.android.russia.utils.EntranceLock;
import com.rosbank.android.russia.utils.FontUtils;
import com.rosbank.android.russia.utils.NetworkUtil;
import com.rosbank.android.russia.utils.PhotoPickerUtils;
import com.rosbank.android.russia.utils.StringUtil;
import com.rosbank.android.russia.widgets.AttachePhotoView;
import com.rosbank.android.russia.widgets.ContactView;
import com.rosbank.android.russia.widgets.CountryCityRequestView;
import com.rosbank.android.russia.widgets.CustomErrorView;
import com.rosbank.android.russia.widgets.NumberPickerView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTouch;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nga.nguyent on 10/6/2016.
 */

public class ConciergeGourmetTab2Fragment
        extends BaseFragment
        implements ContactView.ContactViewListener,
        SelectionFragment.SelectionCallback,
        SelectionCountryOrCityFragment.SelectionCallback,
        SelectDataFragment.OnSelectDataListener
        ,
        Callback,
        B2CICallback {
    @BindView(R.id.container)
    LinearLayout mLayoutContainer;
    @BindView(R.id.edt_maximum_budget)
    EditText edtMaximumBudget;
    @BindView(R.id.tvReservationDateValue)
    TextView tvReservationDateValue;
    @BindView(R.id.tvReservationTimeValue)
    TextView tvReservationTimeValue;
    @BindView(R.id.countryCityView)
    CountryCityRequestView countryCityRequestView;
    @BindView(R.id.tvCuisineValue)
    TextView tvCuisineValue;
    @BindView(R.id.tv_occasion)
    TextView tvOccasion;
    @BindView(R.id.picker_adults)
    NumberPickerView npvAdults;
    @BindView(R.id.picker_kids)
    NumberPickerView npvKids;
    @BindView(R.id.edtSpecialRequirements)
    EditText edtSpecialRequirements;
    @BindView(R.id.special_req_error)
    CustomErrorView specialReqError;

    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.btn_cancel)
    Button btnCancel;
    @BindView(R.id.ervReservationTime)
    CustomErrorView ervReservationTime;
    @BindView(R.id.country_error)
    CustomErrorView ervCountry;
    @BindView(R.id.ervCuisine)
    CustomErrorView ervCuisine;
    @BindView(R.id.photo_attached)
    AttachePhotoView mPhotoAttached;
    @BindView(R.id.contact_view)
    ContactView mContactView;

    private Calendar reservationCal;
    List<KeyValueObject> cuisinePreferences = new ArrayList<>();
    MyRequestObject myRequestObject;
    DiningPreferenceDetailData diningPreferenceDetailData;
    private B2CUpsertConciergeRequestRequest upsertConciergeRequestRequest;
    private String uploadedPhotoPath = "";
    private EntranceLock entranceLock = new EntranceLock();
    private boolean isDateSelectedByUser = false;
    private boolean isSubmitClicked = false;

    @Override
    protected int layoutId() {
        return R.layout.fragment_concierge_gourmet_tab2;
    }

    @Override
    protected void initView() {
        mPhotoAttached.setParentFragment(this);
        mPhotoAttached.setAttachePhotoCallback(new AttachePhotoView.IAttachePhotoCallback() {
            @Override
            public void onPhotoChanged() {
                // Reset the photo path once the user change photo
                uploadedPhotoPath = "";
            }
        });
        CommonUtils.setFontForViewRecursive(mLayoutContainer,
                FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
        CommonUtils.setFontForViewRecursive(btnSubmit,
                FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        CommonUtils.setFontForViewRecursive(btnCancel,
                FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);

        makeDefaultReservationTime();
        mContactView.setPhoneNumberHint(R.string.text_concierge_booking_dining_mobile_number_hint);
        mContactView.setContactViewListener(this);

        /**
         * Reset error view
         * */
        ervReservationTime.setMessage(getString(R.string.text_error_message_invalid_time));
        ervCountry.setMessage(getString(R.string.text_sign_up_error_required_field));
        ervCuisine.setMessage(getString(R.string.text_sign_up_error_required_field));
        specialReqError.setMessage(getString(R.string.special_requirement_error));

        ervReservationTime.hide();
        ervCountry.hide();
        ervCuisine.hide();

        npvAdults.setNumber(1);
        npvAdults.setMinimum(1);

        if (getArguments() != null) {
            myRequestObject =
                    (MyRequestObject) getArguments().getSerializable(AppConstant.PRE_REQUEST_OBJECT_DATA);
        }
        if (myRequestObject != null) {
            isDateSelectedByUser = true;
            showDialogProgress();
            B2CWSGetRequestDetail b2CWSGetRequestDetail = new B2CWSGetRequestDetail(this);
            b2CWSGetRequestDetail.setValue(myRequestObject.getEpcCaseId(),
                    myRequestObject.getBookingItemID());
            b2CWSGetRequestDetail.run(null);
        } else { // Load default preferences
            showDialogProgress();
            B2CWSGetPreference b2CWSGetPreference = new B2CWSGetPreference(getPreferenceCallback);
            b2CWSGetPreference.run(null);
        }
    }

    @Override
    protected void bindData() {

    }

    @Override
    protected boolean onBack() {
        return false;
    }

    public void makeDefaultReservationTime() {
        if (!isDateSelectedByUser) {
            reservationCal = Calendar.getInstance();
            reservationCal.add(Calendar.DAY_OF_YEAR, 0);
            reservationCal.add(Calendar.MINUTE, AppConstant.MINUTE_EXTRA_DINING_BOOKING);

            showReservationDateTime();
        }
    }

    public void setRequest(MyRequestObject request) {
        myRequestObject = request;

    }

    public void resetCalendarTimePlus30Min() {
        Locale.setDefault(new Locale("en"));
        Calendar currentCalendar = Calendar.getInstance();
        currentCalendar.add(Calendar.MINUTE, AppConstant.MINUTE_EXTRA_DINING_BOOKING);
        reservationCal.set(Calendar.HOUR, currentCalendar.get(Calendar.HOUR));
        reservationCal.set(Calendar.MINUTE, currentCalendar.get(Calendar.MINUTE));
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode,
                permissions,
                grantResults);

        switch (requestCode) {
            case AppConstant.PERMISSION_REQUEST_READ_EXTERNAL:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mPhotoAttached.openGallery();
                }
                break;
            case AppConstant.PERMISSION_REQUEST_OPEN_CAMERA:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mPhotoAttached.openCamera();
                }
                break;
            case AppConstant.PERMISSION_REQUEST_WRITE_EXTERNAL:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mPhotoAttached.openCamera();
                }
                break;
        }

    }

    @Override
    public void onActivityResult(int requestCode,
                                 int resultCode,
                                 Intent data) {
        super.onActivityResult(requestCode,
                resultCode,
                data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == PhotoPickerUtils.PICK_IMAGE_REQUEST_CODE || requestCode == PhotoPickerUtils.TAKE_PICTURE_REQUEST_CODE) {
                mPhotoAttached.onActivityResult(requestCode,
                        data);
            }
        }
    }

    @OnTouch({R.id.edtSpecialRequirements})
    boolean onInputTouch(final View v,
                         final MotionEvent event) {

        switch (v.getId()) {
            case R.id.edtSpecialRequirements:
                specialReqError.setVisibility(View.GONE);
                break;
        }
        return false;
    }

    @OnClick({R.id.tvReservationDateValue,
            R.id.tvReservationTimeValue})
    void onDateTimeClick(View v) {
        CommonUtils.hideSoftKeyboard(getContext());
        switch (v.getId()) {
            case R.id.tvReservationDateValue:
                showDatePicker();
                ervReservationTime.hide();
                break;
            case R.id.tvReservationTimeValue:
                showTimePicker();
                ervReservationTime.hide();
                break;
        }
    }

    @OnClick({R.id.tvCuisineValue,
            R.id.tv_occasion})
    void onCountryCityClick(View view) {
        switch (view.getId()) {
            case R.id.tvCuisineValue:
                ervCuisine.hide();
                /*Bundle bundle = new Bundle();
                bundle.putString(SelectionFragment.PRE_SELECTION_TYPE,
                                 SelectionFragment.PRE_SELECTION_CUISINE);
                if (!tvCuisineValue.getText()
                                   .toString()
                                   .equalsIgnoreCase(getString(R.string.text_concierge_booking_restaurant_hint_cuisine))) {
                    bundle.putString(SelectionFragment.PRE_SELECTION_DATA,
                                     tvCuisineValue.getText()
                                                   .toString());
                }
                SelectionFragment fragment = new SelectionFragment();
                fragment.setSelectionCallBack(this);
                fragment.setArguments(bundle);
                pushFragment(fragment, true, true);*/
                SelectDataFragment cuisine = new SelectDataFragment();
                cuisine.setOnSelectListener(this);
                if (cuisinePreferences != null) {
                    cuisine.setCuisineSelected(cuisinePreferences);
                }
                cuisine.setHeaderTitle(getString(R.string.cuisine_preferences));
                pushFragment(cuisine,
                        true,
                        true);
                break;
            case R.id.tv_occasion:
                Bundle bundle = new Bundle();
                bundle.putString(SelectionFragment.PRE_SELECTION_TYPE,
                        SelectionFragment.PRE_SELECTION_OCCASION);
                if (!tvOccasion.getText()
                        .toString()
                        .equalsIgnoreCase(getString(R.string.hint_occasion_text))) {
                    bundle.putString(SelectionFragment.PRE_SELECTION_DATA,
                            tvOccasion.getText()
                                    .toString());
                }
                SelectionFragment fragment = new SelectionFragment();
                fragment.setSelectionCallBack(this);
                fragment.setArguments(bundle);
                pushFragment(fragment,
                        true,
                        true);
                break;

        }
    }

    private void showDatePicker() {
        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view,
                                          int year,
                                          int monthOfYear,
                                          int dayOfMonth) {
                        if (CommonUtils.validateFutureDate(dayOfMonth,
                                monthOfYear,
                                year,
                                view)) {
                            reservationCal.set(year,
                                    monthOfYear,
                                    dayOfMonth);
                            showReservationDateTime();
                    /*        if (DateTimeUtil.shareInstance()
                                    .checkLaterThan24Hours(reservationCal)) {
                                ervReservationTime.setVisibility(View.GONE);*/
                            isDateSelectedByUser =
                                    true;
                        /*    } else {
                                ervReservationTime.setVisibility(View.VISIBLE);
                                isDateSelectedByUser =
                                        false;
                            }*/
                        }
                    }
                },
                reservationCal.get(Calendar.YEAR),
                reservationCal.get(Calendar.MONTH),
                reservationCal.get(Calendar.DAY_OF_MONTH));
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_YEAR, 0);
        datePickerDialog.getDatePicker()
                .setMinDate(c.getTimeInMillis() - 1000);
        datePickerDialog.show();
    }

    private void showTimePicker() {
        TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(),
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view,
                                          int hourOfDay,
                                          int minute) {
                        reservationCal.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        reservationCal.set(Calendar.MINUTE, minute);
                        if (DateTimeUtil.shareInstance().checkIfTimeInThePast(reservationCal)) {
                            resetCalendarTimePlus30Min();
                        }
                        isDateSelectedByUser = true;
                        showReservationDateTime();
                    }
                },
                reservationCal.get(Calendar.HOUR_OF_DAY),
                reservationCal.get(Calendar.MINUTE), false);
        timePickerDialog.setTitle(getString(R.string.text_reservation_time).replace("*", ""));
        timePickerDialog.show();
    }

    private void showReservationDateTime() {
        tvReservationDateValue.setText(DateTimeUtil.shareInstance()
                .formatTimeStamp(reservationCal.getTimeInMillis(),
                        AppConstant.DATE_FORMAT_CONCIERGE_BOOKING));
        tvReservationTimeValue.setText(DateTimeUtil.shareInstance()
                .formatTimeStamp(reservationCal.getTimeInMillis(),
                        AppConstant.TIME_FORMAT_CONCIERGE_BOOKING)
                .toUpperCase());
    }

    private void updateCuisineValue() {
        String text = "";
        if (cuisinePreferences != null) {
            for (int i = 0; i < cuisinePreferences.size(); i++) {
                if (text.length() > 0) {
                    text += ", ";
                }
                text += cuisinePreferences.get(i)
                        .getValue();
            }
        }

        if (text.length() == 0) {
            text = getString(R.string.text_concierge_booking_restaurant_hint_cuisine);
            tvCuisineValue.setTextColor(ContextCompat.getColor(getContext(),
                    R.color.hint_color));
        } else {
            tvCuisineValue.setTextColor(ContextCompat.getColor(getContext(),
                    R.color.text_color_active));
        }

        tvCuisineValue.setText(text);
    }

    @Override
    public void onSelectedData(SelectDataFragment.APIDATA APIDATA,
                               List<KeyValueObject> list) {
        //Logger.sout("selectedddd " + list.size() );
        cuisinePreferences = list;
        updateCuisineValue();
    }

    @Override
    public void onFinishSelection(Bundle bundle) {

        String typeSelection = bundle.getString(SelectionFragment.PRE_SELECTION_TYPE,
                "");

        if (typeSelection.equals(SelectionFragment.PRE_SELECTION_COUNTRY_CODE)) {
            mContactView.setCountryNo(bundle.getString(SelectionFragment.PRE_SELECTION_DATA,
                    ""));
        } else if (typeSelection.equalsIgnoreCase(SelectionFragment.PRE_SELECTION_OCCASION)) {
            String selectedOccasion = bundle.getString(SelectionFragment.PRE_SELECTION_DATA);
            if (!TextUtils.isEmpty(selectedOccasion)) {
                tvOccasion.setText(selectedOccasion);
                tvOccasion.setTextColor(ContextCompat.getColor(getContext(),
                        R.color.text_color_active));
            } else {
                tvOccasion.setText(R.string.hint_occasion_text);
                tvOccasion.setTextColor(ContextCompat.getColor(getContext(),
                        R.color.hint_color));
            }
        } else if (typeSelection.equalsIgnoreCase(SelectionFragment.PRE_SELECTION_CUISINE)) {
            tvCuisineValue.setText(bundle.getString(SelectionFragment.PRE_SELECTION_DATA));
            tvCuisineValue.setTextColor(ContextCompat.getColor(getContext(),
                    R.color.text_color_active));
        }
    }

    @OnClick(R.id.btn_submit)
    void onNextClick() {
        AppContext.getSharedInstance()
                .track(AppConstant.ANALYTIC_EVENT_CATEGORY_CONCIERGE_REQUEST,
                        AppConstant.ANALYTIC_EVENT_ACTION_SEND_CONCIERGE_REQUEST,
                        AppConstant.ANALYTIC_BOOK_ITEM.DINING.getValue());
        if (!entranceLock.isClickContinuous()) {
            submit();
        }
    }

    @OnClick(R.id.btn_cancel)
    void onCancelClick() {
        if (!entranceLock.isClickContinuous()) {
            onBackPress();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ((HomeActivity) getActivity()).showLogoApp(false);
        ((HomeActivity) getActivity()).setTitle(getString(R.string.concierge_item_gourmet));
    }

    private void submit() {
        boolean invalid = false;

        if (DateTimeUtil.shareInstance().checkIfTimeInThePast(reservationCal)) {
            resetCalendarTimePlus30Min();
        }
        if (tvCuisineValue.getText()
                .toString()
                .equalsIgnoreCase(getString(R.string.text_concierge_booking_restaurant_hint_cuisine))) {
            ervCuisine.show();
            invalid = true;
        } else {
            ervCuisine.hide();
        }
        if (!countryCityRequestView.isValidated()) {
            invalid = true;
        }
        if (StringUtil.containSpecialCharacter(edtSpecialRequirements.getText()
                .toString())) {
            specialReqError.setVisibility(View.VISIBLE);
            invalid = true;
        } else {
            specialReqError.setVisibility(View.GONE);
        }
        if (!mContactView.validationContact()) {
            invalid = true;
        }

        if (invalid) {
            return;
        }

        //call api update
        if (!UserItem.isLogined()) {
            //pop to root.
            return;
        }
        if (!NetworkUtil.getNetworkStatus(getContext())) {
            showInternetProblemDialog();
            return;
        }
        isSubmitClicked = true;
        upsertConciergeRequestRequest = new B2CUpsertConciergeRequestRequest();
        upsertConciergeRequestRequest.setFunctionality(AppConstant.CONCIERGE_FUNCTIONALITY_TYPE.DINING_RECOMMEND.getValue());
        upsertConciergeRequestRequest.setRequestType(AppConstant.CONCIERGE_REQUEST_TYPE.RECOMMEND_RESTAURANT.getValue());

        // Event date
        upsertConciergeRequestRequest.setEventDate(DateTimeUtil.shareInstance()
                .formatTimeStamp(reservationCal.getTimeInMillis(),
                        AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS));
        upsertConciergeRequestRequest.setDelivery(DateTimeUtil.shareInstance()
                .formatTimeStamp(reservationCal.getTimeInMillis(),
                        AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS));
        if (!tvOccasion.getText().toString().equalsIgnoreCase(getString(R.string.hint_occasion_text))) {

            upsertConciergeRequestRequest.setSituation(tvOccasion.getText().toString());
        } else {
            upsertConciergeRequestRequest.setSituation("Situation");
        }

        upsertConciergeRequestRequest.updateDataFromCountryCityView(countryCityRequestView);

        upsertConciergeRequestRequest.setNumberOfAdults(String.valueOf(npvAdults.getNumber()));
        if (npvKids.getNumber() > 0) {
            upsertConciergeRequestRequest.setNumberOfKids(String.valueOf(npvKids.getNumber()));
            upsertConciergeRequestRequest.setNumberOfChildren(String.valueOf(npvKids.getNumber()));
        }
        upsertConciergeRequestRequest.updateDataFromContactView(mContactView);
        // Image path
        /*if(!TextUtils.isEmpty(uploadedPhotoPath)){
            upsertConciergeRequestRequest.setAttachmentPath(uploadedPhotoPath);
        }else {
            if (atpView.getPhotos() != null && atpView.getPhotos().size() > 0) {
                String pathImage = atpView.getPhotos()
                        .get(0);
                upsertConciergeRequestRequest.setAttachmentPath(pathImage);
            }
        }*/
        if (myRequestObject != null) {
            upsertConciergeRequestRequest.setTransactionID(myRequestObject.getBookingItemID());
        }
        // Request details
        upsertConciergeRequestRequest.setRequestDetails(combineRequestDetails());

        showDialogProgress();
        B2CWSUpsertConciergeRequest b2CWSUpsertConciergeRequest =
                new B2CWSUpsertConciergeRequest(myRequestObject == null ?
                        AppConstant.CONCIERGE_EDIT_TYPE.ADD :
                        AppConstant.CONCIERGE_EDIT_TYPE.AMEND,
                        this);
        b2CWSUpsertConciergeRequest.setRequest(upsertConciergeRequestRequest);
        b2CWSUpsertConciergeRequest.run(null);
    }

    private String combineRequestDetails() {
       /* String requestDetails = "";
        if(!TextUtils.isEmpty(edtSpecialRequirements.getText().toString().trim())){
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_SPECIAL_REQ + edtSpecialRequirements.getText().toString().trim();
        }
        if(!TextUtils.isEmpty(edtMaximumBudget.getText().toString())){
            if(!TextUtils.isEmpty(requestDetails)){
                requestDetails += " | ";
            }
            requestDetails += AppConstant.B2C_REQUEST_DETAIL_MAX_PRICE + edtMaximumBudget.getText().toString();
        }
        if(!TextUtils.isEmpty(requestDetails)){
            requestDetails += " | ";
        }
        requestDetails += AppConstant.B2C_REQUEST_DETAIL_CUISINE + tvCuisineValue.getText().toString();
        return requestDetails;*/
        String requestDetails = "";
        if (!TextUtils.isEmpty(edtSpecialRequirements.getText()
                .toString()
                .trim())) {
            requestDetails +=
                    AppConstant.B2C_REQUEST_DETAIL_SPECIAL_REQUIREMENTS + StringUtil.removeAllSpecialCharacterAndBreakLine(edtSpecialRequirements.getText()
                            .toString()
                            .trim());
        }
        if (!TextUtils.isEmpty(mContactView.getReservationName())) {
            if (!TextUtils.isEmpty(requestDetails)) {
                requestDetails += " | ";
            }
            requestDetails +=
                    AppConstant.B2C_REQUEST_DETAIL_RESERVATION_NAME + StringUtil.removeAllSpecialCharacterAndBreakLine(mContactView.getReservationName());
        }
        if (!TextUtils.isEmpty(tvCuisineValue.getText()
                .toString())) {
            if (!TextUtils.isEmpty(requestDetails)) {
                requestDetails += " | ";
            }
            requestDetails +=
                    AppConstant.B2C_REQUEST_DETAIL_TYPE_OF_CUISINE + tvCuisineValue.getText()
                            .toString();
        }

        if (!tvOccasion.getText()
                .toString()
                .equalsIgnoreCase(getString(R.string.hint_occasion_text))) {
            if (!TextUtils.isEmpty(requestDetails)) {
                requestDetails += " | ";
            }
            requestDetails +=
                    AppConstant.B2C_REQUEST_DETAIL_AMBIANCE_OCCASION + tvOccasion.getText()
                            .toString();
        }
        if (!TextUtils.isEmpty(countryCityRequestView.getState())) {
            if (!TextUtils.isEmpty(requestDetails)) {
                requestDetails += " | ";
            }
            requestDetails +=
                    AppConstant.B2C_REQUEST_DETAIL_STATE + countryCityRequestView.getState();
        }
        if (!TextUtils.isEmpty(countryCityRequestView.getCountry())) {
            if (!TextUtils.isEmpty(requestDetails)) {
                requestDetails += " | ";
            }
            requestDetails +=
                    AppConstant.B2C_REQUEST_DETAIL_COUNTRY + countryCityRequestView.getCountry();
        }
        if (!TextUtils.isEmpty(edtMaximumBudget.getText()
                .toString())) {
            if (!TextUtils.isEmpty(requestDetails)) {
                requestDetails += " | ";
            }
            requestDetails +=
                    AppConstant.B2C_REQUEST_DETAIL_BUDGET_RANGE_CURRENCY + edtMaximumBudget.getText()
                            .toString();
        }
        if (diningPreferenceDetailData != null && !TextUtils.isEmpty(diningPreferenceDetailData.getFoodAllergies())) {
            if (!TextUtils.isEmpty(requestDetails)) {
                requestDetails += " | ";
            }
            requestDetails +=
                    AppConstant.B2C_REQUEST_DETAIL_FOOD_ALLERGIES + diningPreferenceDetailData.getFoodAllergies();
        }

        return requestDetails;
    }

    @Override
    public void onSelectCountryClick() {
        Bundle bundle1 = new Bundle();
        bundle1.putString(SelectionFragment.PRE_SELECTION_TYPE,
                SelectionFragment.PRE_SELECTION_COUNTRY_CODE);
        bundle1.putString(SelectionFragment.PRE_SELECTION_DATA,
                mContactView.getSelectedCountryCode());
        SelectionFragment fragment1 = new SelectionFragment();
        fragment1.setSelectionCallBack(this);
        fragment1.setArguments(bundle1);
        pushFragment(fragment1,
                true,
                true);
    }

    @Override
    public void onB2CResponse(B2CBaseResponse response) {
        hideDialogProgress();
        if (getActivity() == null) {
            return;
        }
        if (response instanceof B2CUpsertConciergeRequestResponse) {
            if (response != null && response.isSuccess()) {
                onBackPress();

                ThankYouFragment thankYouFragment = new ThankYouFragment();
                if (myRequestObject != null) {
//                    Bundle bundle = new Bundle();
//                    bundle.putString(ThankYouFragment.LINE_MAIN,
//                            getString(R.string.text_message_thank_you_amend));
//                    thankYouFragment.setArguments(bundle);
                    EventBus.getDefault()
                            .post(AppConstant.CONCIERGE_EDIT_TYPE.AMEND);
                } else {
                    EventBus.getDefault()
                            .post(AppConstant.CONCIERGE_EDIT_TYPE.ADD);
                    // Track product
                    AppContext.getSharedInstance()
                            .track(AppConstant.TRACK_EVENT_PRODUCT_CATEGORY.RECOMMEND_DINING.getValue(),
                                    AppConstant.TRACK_EVENT_PRODUCT_NAME.GENERIC_RECOMMEND.getValue());
                }
                pushFragment(thankYouFragment,
                        true,
                        true);
            } else {
                //showDialogMessage("Error", response.getMessage());
                showDialogMessage(getString(R.string.text_title_dialog_error),
                        getString(R.string.text_concierge_request_error));
                if (upsertConciergeRequestRequest != null) {
                    uploadedPhotoPath = upsertConciergeRequestRequest.getAttachmentPath();
                }
            }

        } else if (response instanceof B2CGetRecentRequestResponse) {
            showRequestDetails(new RestaurantBookingDetailData((B2CGetRecentRequestResponse) response));
        }
    }

    @Override
    public void onB2CResponseOnList(List<B2CBaseResponse> responseList) {
        if (getActivity() == null) {
            return;
        }
    }

    @Override
    public void onB2CFailure(String errorMessage,
                             String errorCode) {
        hideDialogProgress();
        if (getActivity() == null) {
            return;
        }
        if (isSubmitClicked) {
            isSubmitClicked = false;
            showDialogMessage(getString(R.string.text_title_dialog_error),
                    getString(R.string.text_concierge_request_error));
        }
        //showDialogMessage("Error", errorMessage);
        // Store the uploaded successful photo path
        // So that the upsert failed in the first time, user tried to process for the second time
        // then that is no need to upload again
        if (upsertConciergeRequestRequest != null) {
            uploadedPhotoPath = upsertConciergeRequestRequest.getAttachmentPath();
        }
    }

    @Override
    public void onResponse(Call call,
                           Response response) {
        if (getActivity() == null) {
            return;
        }
        if (response.body() instanceof RestaurantBookingDetailResponse) {
            showRequestDetails(((RestaurantBookingDetailResponse) response.body()).getData());
        }
        hideDialogProgress();
    }

    @Override
    public void onFailure(Call call,
                          Throwable t) {
        hideDialogProgress();
        if (getActivity() == null) {
            return;
        }
        showToast(getString(R.string.text_server_error_message));
    }

    private void showRequestDetails(RestaurantBookingDetailData data) {
        if (getActivity() == null) {
            return;
        }
        if (data == null) {
            return;
        }
        countryCityRequestView.setData(data);
        edtMaximumBudget.setText(data.getMaxPrice());
        if (!TextUtils.isEmpty(data.getCuisine())) {
            String[] selectedCuisineArr = data.getCuisine()
                    .split("\\s*,\\s*");
            if (selectedCuisineArr != null && selectedCuisineArr.length > 0) {
                cuisinePreferences = new ArrayList<>();
                for (String cuisine : selectedCuisineArr) {
                    cuisinePreferences.add(new KeyValueObject(cuisine,
                            cuisine));
                }
            }
            updateCuisineValue();
        }
        if (!TextUtils.isEmpty(data.getOccasion())) {
            tvOccasion.setText(data.getOccasion());
            tvOccasion.setTextColor(ContextCompat.getColor(getContext(),
                    R.color.text_color_active));
        }
        npvAdults.setNumber(data.getNumberOfAdults());
        npvKids.setNumber(data.getNumberOfKids());
        edtSpecialRequirements.setText(data.getConciergeRequestDetail()
                .getSpecialRequirement());

        reservationCal = Calendar.getInstance();
        reservationCal.setTimeInMillis(data.getReservationDateEpoc());
        showReservationDateTime();

        // Update on contact view
        mContactView.updateContactView(data);
    }

    private void updateUIFromDefaultPreference(DiningPreferenceDetailData diningPreferenceDetailData) {
        if (getActivity() == null) {
            return;
        }
        if (diningPreferenceDetailData != null) {
            if (!TextUtils.isEmpty(diningPreferenceDetailData.getCuisinePreference())) {
                String[] selectedCuisineArr = diningPreferenceDetailData.getCuisinePreference()
                        .split("\\s*,\\s*");
                if (selectedCuisineArr != null && selectedCuisineArr.length > 0) {
                    for (String cuisine : selectedCuisineArr) {
                        String cussineRu = CommonUtils.getRusianCussineString(cuisine);
                        if (CommonUtils.isStringValid(cussineRu)) {
                            if (!diningPreferenceDetailData.getCuisinePreference()
                                    .contains(cussineRu)) {
                                KeyValueObject keyValueObject = new KeyValueObject(cussineRu,
                                        cussineRu);
                                cuisinePreferences.add(keyValueObject);
                            }
                        } else {
                            KeyValueObject keyValueObject = new KeyValueObject(cuisine,
                                    cuisine);
                            cuisinePreferences.add(keyValueObject);
                        }
                    }
                }

            }
            if (cuisinePreferences == null || cuisinePreferences.size() == 0) {
                KeyValueObject keyValueObject = new KeyValueObject("Русская",
                        "Русская");
                cuisinePreferences.add(keyValueObject);
            }
            String otherCuisine = diningPreferenceDetailData.getOtherPreference();
            if (!TextUtils.isEmpty(otherCuisine) && !cuisinePreferences.contains(otherCuisine)) {
                cuisinePreferences.add(new KeyValueObject(otherCuisine,
                        otherCuisine));
            }

        }
        if (cuisinePreferences == null || cuisinePreferences.size() == 0) {
            KeyValueObject keyValueObject = new KeyValueObject("Русская",
                    "Русская");
            cuisinePreferences.add(keyValueObject);
        }
        updateCuisineValue();
    }

    private B2CICallback getPreferenceCallback = new B2CICallback() {
        @Override
        public void onB2CResponse(B2CBaseResponse response) {
            if (getActivity() == null) {
                return;
            }
        }

        @Override
        public void onB2CResponseOnList(List<B2CBaseResponse> responseList) {
            if (getActivity() != null) {
                hideDialogProgress();
                if (getActivity() == null) {
                    return;
                }
                diningPreferenceDetailData =
                        (DiningPreferenceDetailData) PreferenceData.findFromList(responseList,
                                AppConstant.PREFERENCE_TYPE.DINING);
                updateUIFromDefaultPreference(diningPreferenceDetailData);
            }
        }

        @Override
        public void onB2CFailure(String errorMessage,
                                 String errorCode) {
            hideDialogProgress();
            if (getActivity() == null) {
                return;
            }
        }
    };
}
