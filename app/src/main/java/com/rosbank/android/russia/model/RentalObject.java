package com.rosbank.android.russia.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;


public class RentalObject
        implements Parcelable {
    public RentalObject(){}
    public RentalObject(String value){
        Value = value;}
    @Expose
    private String Key;

    public String getKey() {
        return Key;
    }

    public void setKey(final String key) {
        Key = key;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(final String value) {
        Value = value;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof RentalObject){
            return ((RentalObject)obj).getValue().equals(Value);
        }
        return false;
    }

    @Override
    public String toString() {
        return Value;
    }

    @Expose
    private String Value;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel parcel,
                              final int i) {
        parcel.writeString(this.Key);
        parcel.writeString(this.Value);

    }
    protected RentalObject(Parcel in) {
        this.Key = in.readString();
        this.Value = in.readString();
    }
    public static final Creator<RentalObject> CREATOR = new Creator<RentalObject>() {
        @Override
        public RentalObject createFromParcel(Parcel source) {
            return new RentalObject(source);
        }

        @Override
        public RentalObject[] newArray(int size) {
            return new RentalObject[size];
        }
    };
}
